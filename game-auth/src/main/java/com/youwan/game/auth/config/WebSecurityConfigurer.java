package com.youwan.game.auth.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.youwan.game.common.security.handler.MobileLoginSuccessHandler;
import com.youwan.game.common.security.mobile.MobileSecurityConfigurer;
import com.youwan.game.common.security.service.GameUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

/**
 * @author lion
 * @date 2018/6/22
 * 认证相关配置
 */
@Primary
@Order(90)
@Configuration
public class 	WebSecurityConfigurer extends WebSecurityConfigurerAdapter {
	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	private ClientDetailsService clientDetailsService;
	@Autowired
	private GameUserDetailsService userDetailsService;
	@Lazy
	@Autowired
	private AuthorizationServerTokenServices defaultAuthorizationServerTokenServices;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.formLogin()
			//登录URL,指定登录页的路径
			.loginPage("/token/login")
			//登录拦截URL
			.loginProcessingUrl("/token/form")
			.and()
			//添加不验证的请求
			.authorizeRequests()
		     //添加URL不通过验证,
			.antMatchers(
				"/token/**",
				//获取第三方社交
				"/socialconfig/getConfig/**",
				"/downconfig/configByChannelId/**",
				//接口信息
				"/api/**/**",
				//前端公告显示
				"/bulletin/**",
				//前端下载
				"/download/**",
				//微信分享防封
				"/share/**",
				//前端支付界面
				"/pay/**",
				//静态资源
				"/css/**", "/js/**", "/jquery/**", "/images/**",
				//支付配置请求接口
                "/payconfig/payChannel/**",
				//渠道信息请求接口
                "/tchannel/channelInfo/**",
				//获取支付配置信息接口
				"/payconfig/info/**",
				//处理短链接
				"/shorturl/createShortUrl/",
				//获取支付订单
				"/sysfile/config/","/sysfile/qrcode/","/sysfile/defineQrCode/",
				"/cashorder/createOrder/","/syssms/sendSms/", "/sysversionconfig/config/",
				"/actuator/**",
				"/mobile/**"
				//前端下载界面
					).permitAll()
			//登录后可访问
			.anyRequest().authenticated()
			//关闭打开的csrf保护
			.and().csrf().disable()
			//添加配置
			.apply(mobileSecurityConfigurer());
	}

	/**
	 * 不拦截静态资源
	 * 此处配置所有不需要拦截的资源
	 * @param web
	 */
	@Override
	public void configure(WebSecurity web) {
		web.ignoring().antMatchers("/css/**", "/js/**", "/jquery/**", "/images/**");
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public AuthenticationSuccessHandler mobileLoginSuccessHandler() {
		return MobileLoginSuccessHandler.builder()
			.objectMapper(objectMapper)
			.clientDetailsService(clientDetailsService)
			.passwordEncoder(passwordEncoder())
			.defaultAuthorizationServerTokenServices(defaultAuthorizationServerTokenServices).build();
	}

	@Bean
	public MobileSecurityConfigurer mobileSecurityConfigurer() {
		MobileSecurityConfigurer mobileSecurityConfigurer = new MobileSecurityConfigurer();
		mobileSecurityConfigurer.setMobileLoginSuccessHandler(mobileLoginSuccessHandler());
		mobileSecurityConfigurer.setUserDetailsService(userDetailsService);
		return mobileSecurityConfigurer;
	}


	/**
	 * @return PasswordEncoder
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
		return passwordEncoder;
	}

}
