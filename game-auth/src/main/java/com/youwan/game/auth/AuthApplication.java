package com.youwan.game.auth;

import com.youwan.game.common.security.annotation.EnableGameFeignClients;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;


/**
 * @author lion
 * @date 2018年06月21日
 * 认证授权中心
 */
@SpringCloudApplication
@EnableGameFeignClients
public class AuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthApplication.class, args);
	}
}
