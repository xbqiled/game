package com.youwan.game.auth.handler;


import com.youwan.game.common.security.handler.AbstractAuthenticationFailureEvenHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;


/**
 * @author lion
 * @date 2018/10/8
 */
@Slf4j
@Component
public class AuthenticationFailureEvenHandler extends AbstractAuthenticationFailureEvenHandler {

	/**
	 * 处理登录失败方法
	 * <p>
	 *
	 * @param authenticationException 登录的authentication 对象
	 * @param authentication          登录的authenticationException 对象
	 */
	@Override
	public void handle(AuthenticationException authenticationException, Authentication authentication) {
		//System.err.println(authentication.getCredentials());
		log.info("用户：{}.{} 登录失败，异常：{}", authentication.getPrincipal(),
				authentication.getCredentials(), authenticationException.getLocalizedMessage());
	}
}
