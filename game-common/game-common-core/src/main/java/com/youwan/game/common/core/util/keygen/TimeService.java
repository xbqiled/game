

package com.youwan.game.common.core.util.keygen;

/**
 * Time service.
 *
 * @author zhangliang
 */
public class TimeService {

    /**
     * Get current millis.
     *
     * @return current millis
     */
    public long getCurrentMillis() {
        return System.currentTimeMillis();
    }
}
