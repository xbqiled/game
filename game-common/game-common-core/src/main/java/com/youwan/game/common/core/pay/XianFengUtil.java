package com.youwan.game.common.core.pay;


import cn.hutool.core.date.DateUtil;
import com.youwan.game.common.core.util.SignUtil;

import java.math.BigDecimal;
import java.util.*;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;
import static com.youwan.game.common.core.constant.UrlConstant.FEONT_SYSTM_URL;

public class XianFengUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/xianFeng";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "";

    public static Map<String, String> packaData(String... params) {
        String merchantKey = params[6];
        SortedMap<String, String> map = new TreeMap<>();
        //商户编号
        map.put("number",  params[0]);
        //订单号
        map.put("order_no", params[1]);
        //金额
        map.put("amount", new BigDecimal(params[2]).toString());
        //订单建立时间
        map.put("created_at", DateUtil.now());
        //会员账号
        map.put("user_name",  params[8]);
        //支付类型
        map.put("payment_type", params[3]);

        //返回类型
        map.put("content_type", "html");
        //回调地址
        map.put("notify_url",  API_SYSTM_URL + NOTIFY_URL);

        StringBuffer buffer = new StringBuffer();
        Set<Map.Entry<String, String>> set = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
            buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }
        String signSrc = buffer.append("key=").append(merchantKey).toString();
        map.put("sign", SignUtil.encryption(signSrc).toUpperCase());
        //回调界面
        map.put("return_url", FEONT_SYSTM_URL + CALL_BACK_URL + params[1]);
        return  map;
    }

}
