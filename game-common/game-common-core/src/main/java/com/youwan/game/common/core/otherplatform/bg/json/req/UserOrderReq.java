package com.youwan.game.common.core.otherplatform.bg.json.req;


import lombok.Data;

import java.io.Serializable;


@Data
public class UserOrderReq implements Serializable {

    private String id;
    private String method = "open.user.order.sum";
    private Params params;
    private String jsonrpc;

    @Data
    public class Params implements Serializable {
        private String random;

        private String sn;
        private String sign;
        private String loginId;
        private String nickname;
        private String agentLoginId;
    }

}
