package com.youwan.game.common.core.jsonmodel;


import lombok.Data;

import java.io.Serializable;


@Data
public class RequestOperateAccount implements Serializable {

    private Integer accountID;
    private Integer optType;

    private OptParam optParam;

    @Data
    public class OptParam {
        private Integer thirdType;
    }

}
