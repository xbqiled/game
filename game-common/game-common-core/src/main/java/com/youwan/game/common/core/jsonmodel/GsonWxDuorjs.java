package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonWxDuorjs implements Serializable {

    private Integer betPeopleCnt;

    private Integer totalBet;
    
}
