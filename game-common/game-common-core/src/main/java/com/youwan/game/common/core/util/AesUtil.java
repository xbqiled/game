package com.youwan.game.common.core.util;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;

public class AesUtil {

    //Data Fields
    private String inputText;
    private String secretKey;
    private String initialVector;
    private int keySize;

    //Default Constructor
    public AesUtil(String inputText, String secretKey, String initialVector, int keySize) {
        try {
            setKeySize(keySize);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setInputText(inputText);
        setSecretKey(secretKey);
        setInitialVector(initialVector);
    }

    //Encapsulation
    public String getInputText() {
        return inputText;
    }

    public void setInputText(String inputText) {
        this.inputText = inputText;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = lengthPadding(secretKey, (getKeySize() / 8));
    }

    public String getInitialVector() {
        return initialVector;
    }

    public void setInitialVector(String initialVector) {
        this.initialVector = lengthPadding(initialVector, 16);
    }

    public int getKeySize() {
        return keySize;
    }

    public void setKeySize(int keySize) throws Exception {
        if (keySize == 128 || keySize == 192 || keySize == 256) {
            this.keySize = keySize;
        } else {
            throw new Exception("Not a valid size! only 128, 192 and 256 accepted.");
        }
    }

    //Methods
    private String lengthPadding(String str, int toLength) {
        int fromLength = str.length();
        if (fromLength < toLength && fromLength != 0) {
            for (int i = 0; i < (toLength - fromLength); i++) {
                str += "\0";
            }
        } else if (fromLength == 0) {
            str = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
        } else {
            str = str.substring(0, toLength);
        }
        return str;
    }

    public String encryptToString() throws Exception {
        try {
            IvParameterSpec initialVectorSpec = new IvParameterSpec(initialVector.getBytes(StandardCharsets.UTF_8));
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(StandardCharsets.UTF_8), "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, initialVectorSpec);
            byte[] encrypted = cipher.doFinal(inputText.getBytes());
            return java.util.Base64.getEncoder().encodeToString(encrypted);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String decryptToString() throws Exception {
        try {
            IvParameterSpec initialVectorSpec = new IvParameterSpec(initialVector.getBytes(StandardCharsets.UTF_8));
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(StandardCharsets.UTF_8), "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, initialVectorSpec);
            byte[] original = cipher.doFinal(java.util.Base64.getDecoder().decode(inputText));
            return new String(original);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String encrypt(String sSrc, String sKey, String cKey) throws Exception {
        if (sKey == null) {
            System.out.print("Key为空null");
            return null;
        }
        // 判断Key是否为16位
        if (sKey.length() != 16) {
            System.out.print("Key长度不是16位");
            return null;
        }
        byte[] raw = sKey.getBytes("utf-8");
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");//"算法/模式/补码方式"
        IvParameterSpec iv = new IvParameterSpec(cKey.getBytes());//使用CBC模式，需要一个向量iv，可增加加密算法的强度
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
        byte[] encrypted = cipher.doFinal(sSrc.getBytes());
        return new Base64().encode(encrypted);//此处使用BASE64做转码功能，同时能起到2次加密的作用。
    }

    public static void main(String[] args) throws Exception{
        String s = encrypt("admin","Vn7GQ21MSLRCdvAH","Vn7GQ21MSLRCdvAH");
        System.err.println(s);
    }
}