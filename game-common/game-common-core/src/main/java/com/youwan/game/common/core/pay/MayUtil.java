package com.youwan.game.common.core.pay;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.google.gson.Gson;
import com.youwan.game.common.core.pay.json.ResultMay;
import com.youwan.game.common.core.util.MD5;

import java.math.BigDecimal;
import java.util.*;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;

public class MayUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/may";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "";


    public static Map<String, String> packaData(String... params) {
        String memberNumber = params[0]; //商户号
        String memberOrderNumber = params[1]; //商户订单号
        String tradeAmount = params[2]; //交易金额
        String defrayalType = params[3]; //支付方式        String defrayalType = params[3]; //支付方式
        String gongyao = params[9];   //商户公钥
        ORDER_URL = params[4];

        SortedMap<String, String> map = new TreeMap<>();
        map.put("mch_id", memberNumber);
        map.put("mch_number", memberOrderNumber);
        map.put("pay_mode", defrayalType);
        map.put("total", new BigDecimal(tradeAmount).multiply(new BigDecimal(100)).toString()); // 订单金额（单位：分）
        map.put("source_ip", "127.0.0.1");
        map.put("body", "Onlinepay");

        map.put("notify_url", API_SYSTM_URL + NOTIFY_URL);
        map.put("nonce_str", IdUtil.fastSimpleUUID());//随机字符串，不长于 32 位

        Set<Map.Entry<String, String>> set = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();
        StringBuffer buffer = new StringBuffer();

        while (iterator.hasNext()) {
            Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
            buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }

        String signSrc = buffer.append("key=").append(gongyao).toString();
        map.put("sign", MD5.encryption(signSrc).toUpperCase());
        return map;
    }


    public static ResultMay doPay(Map map) {
        String bodyStr = new Gson().toJson(map);
        String resultStr =  HttpUtil.post(ORDER_URL, bodyStr);

        System.out.println(resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  ResultMay.class);
        }
        return  null;
    }

}
