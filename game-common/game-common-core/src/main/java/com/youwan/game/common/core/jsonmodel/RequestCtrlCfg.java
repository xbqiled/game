package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class RequestCtrlCfg implements Serializable {

    Integer sixInterval;

    Integer eightInterval;

    Integer twelveInterval;

    Integer globalInterval;

    Integer killAllInterval;
}
