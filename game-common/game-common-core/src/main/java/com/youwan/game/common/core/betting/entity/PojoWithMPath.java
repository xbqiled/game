package com.youwan.game.common.core.betting.entity;

import lombok.Data;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-04-21.
 */
@Data
public abstract class PojoWithMPath {
    protected Long pid = 0L;
    protected String m_path;
}
