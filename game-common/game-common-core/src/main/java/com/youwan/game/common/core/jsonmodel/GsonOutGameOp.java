package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonOutGameOp implements Serializable {

    Integer ret;

    String msg;

    Integer outerType;

    Integer coinOpType;

    Integer coinOpNum;

    Integer accountId;

    Integer coinChangeNum;

    Long leftCoin;
}
