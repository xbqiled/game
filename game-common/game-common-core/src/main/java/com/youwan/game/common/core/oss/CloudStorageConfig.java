package com.youwan.game.common.core.oss;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 云存储配置信息
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2017-03-25 16:12
 */
public class CloudStorageConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    //类型 1：七牛  2：阿里云
    @Getter
    @Setter
    private Integer type;

    //七牛绑定的域名
    @Getter
    @Setter
    private String qiniuDomain;
    //七牛路径前缀
    @Getter
    @Setter
    private String qiniuPrefix;
    //七牛ACCESS_KEY
    @Getter
    @Setter
    private String qiniuAccessKey;
    //七牛SECRET_KEY
    @Getter
    @Setter
    private String qiniuSecretKey;
    //七牛存储空间名
    @Getter
    @Setter
    private String qiniuBucketName;

    //阿里云绑定的域名
    @Getter
    @Setter
    private String aliyunDomain;
    //阿里云路径前缀
    @Getter
    @Setter
    private String aliyunPrefix;
    //阿里云EndPoint
    @Getter
    @Setter
    private String aliyunEndPoint;
    //阿里云AccessKeyId
    @Getter
    @Setter
    private String aliyunAccessKeyId;
    //阿里云AccessKeySecret
    @Getter
    @Setter
    private String aliyunAccessKeySecret;
    //阿里云BucketName
    @Getter
    @Setter
    private String aliyunBucketName;
}
