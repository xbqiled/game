package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonCashAli implements Serializable {

    private String payId;
    private String realName;

}
