package com.youwan.game.common.core.jsonmodel;


import lombok.Data;

import java.io.Serializable;


@Data
public class GsonDeviceInfo  implements Serializable {

    private  String osVersion;
    private  String appVersion;

    private  String colorDepth;
    private  String deviceMemory;
    private  String deviceType;

    private  String fingerprint;
    private  String hardwareConcurrency;
    private  String language;

    private  String netWork;
    private  String screenHeight;
    private  String screenWidth;

    private  String userAgent;
}
