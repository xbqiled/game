package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonSwitch implements Serializable {

    private Integer functionType;

    private Integer openType;

}
