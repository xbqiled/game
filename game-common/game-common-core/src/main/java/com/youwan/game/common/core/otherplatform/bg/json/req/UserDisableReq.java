package com.youwan.game.common.core.otherplatform.bg.json.req;

import lombok.Data;

import java.io.Serializable;


@Data
public class UserDisableReq implements Serializable {

    private String id;
    private String method = "open.user.disable";
    private Params params;
    private String jsonrpc;

    @Data
    public static class Params implements Serializable {
        private String random;

        private String sn;
        private String digest;
        private String loginId;
    }
    

}
