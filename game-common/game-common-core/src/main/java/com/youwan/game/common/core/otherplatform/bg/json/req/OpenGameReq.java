package com.youwan.game.common.core.otherplatform.bg.json.req;

import lombok.Data;

import java.io.Serializable;


@Data
public class OpenGameReq implements Serializable {

    private String id;
    private String method = "open.video.game.url";
    private Params params;
    private String jsonrpc = "2.0";

    @Data
    public static class Params implements Serializable {
        private String random;
        private String digest;

        private String sn;
        private String loginId;
        private String locale = "zh_CN";

        private Integer isMobileUrl;
        private Integer isHttpsUrl;
    }
}
