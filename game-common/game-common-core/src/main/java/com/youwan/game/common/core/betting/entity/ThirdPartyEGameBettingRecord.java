package com.youwan.game.common.core.betting.entity;

import lombok.Data;

import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;


/**
 *
 * 第三方电子游戏投注记录
 */
@Data
public class ThirdPartyEGameBettingRecord extends ThirdPartyRecord {

	// 游戏类型
	public static final int TYPE_AG = 1;
	public static final int TYPE_MG = 3;
	public static final int TYPE_BBIN = 2;
	public static final int TYPE_CQ9 = 9;
	public static final int TYPE_ISB = 96;
	public static final int TYPE_MW = 95;
	public static final int TYPE_TTG = 92;
	public static final int TYPE_JDB = 11;
	public static final int TYPE_EBET = 13;

	/**
	 * 订单ID
	 */
	@Id
	private String orderId;

	/**
	 * 下注code
	 */
	private String bettingCode;

	/**
	 * 游戏类型，1=AG，2=MG，3=BBin
	 */
	private Integer type;

	/**
	 * 平台类型
	 */
	private String platformType;
	/**
	 * 用户id
	 */
	private Long accountId;
	/**
	 * 用户账号
	 */
	private String account;

	/**
	 * 站点ID
	 */
	private Long stationId;
	/**
	 * 集群ID
	 */
	private Long serverId;
	/**
	 * 第三方账号ID
	 */
	private Long thirdMemberId;
	/**
	 * 第三方账号
	 */
	private String thirdMemberAccount;
	/**
	 * 下注前金额
	 */
	private BigDecimal beforeMoney;
	/**
	 * 下注金额
	 */
	private BigDecimal bettingMoney;
	/**
	 * 实际下注金额
	 */
	private BigDecimal realBettingMoney;

	/**
	 * 下注时间
	 */
	private Date bettingTime;
	/**
	 * 下注时间
	 */
	private Date bettingTimeGmt4;
	/**
	 * 盈利金额
	 */
	private BigDecimal winMoney;
	/**
	 * 游戏类型
	 */
	private String gameType;
	/**
	 * 游戏类型code
	 */
	private String gameTypeCode;
	/**
	 * 玩法类型
	 */
	private String playType;
	/**
	 * 游戏code
	 */
	private String gameCode;
	/**
	 * 桌号
	 */
	private String tableCode;
	/**
	 * 赔率
	 */
	private BigDecimal odds;
	/**
	 * 创建时间
	 */
	private Date createDatetime;
	/**
	 * 币种
	 */
	private String currency;
	/**
	 * 登录IP
	 */
	private String loginIp;
	/**
	 * 第三方扩展信息
	 */
	private String thirdExtInfo;
	/**
	 * 局号
	 */
	private String round;

	/**
	 *
	 */
	private String slotType;
	/**
	 * 上级账号
	 */
	private String parents;

    /**
     * 返水金额
	 */
	private BigDecimal rakebackMoney;
	/**
	 * 返水赔率
	 */
	private BigDecimal rakebackRate;

	/**
	 * 返水说明
	 */
	private String rakebackDesc;

	/**
	 *
	 */
	private BigDecimal rakebackDrawNeed;

	/**
	 *
	 */
	private BigDecimal jackpotComm;// 捕鱼彩池 的交收

	/**
	 * AG游戏 1 为电子桌面游戏 0 为非电子桌面游戏
	 * 
	 * @return
	 */
	private Integer gameCategory;

}
