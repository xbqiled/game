package com.youwan.game.common.core.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class OrderIdUtil {

    private static long tmpID = 0;
    private static boolean tmpIDlocked = false;

    public static Long getOrdereId() {
        long ltime = 0;
        while (true) {
            if(tmpIDlocked == false) {
                tmpIDlocked = true;
                //当前：（年、月、日、时、分、秒、毫秒）*10000
                ltime = Long.valueOf(new SimpleDateFormat("yyMMddhhmmssSSS").format(new Date())) * 10000;
                if(tmpID < ltime) {
                    tmpID = ltime;
                } else {
                    tmpID = tmpID + 1;
                    ltime = tmpID;
                }
                tmpIDlocked = false;
                return ltime;
            }
        }
    }

    public static void main(String [] args) {
        try {
            String aa = MD5.encryption("12345678");
            System.out.println(aa);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
