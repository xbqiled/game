package com.youwan.game.common.core.jsonmodel;


import lombok.Data;

import java.io.Serializable;


@Data
public class GsonUserGame implements Serializable {

    private int ret;

    private int[] list;

    private String msg;

}
