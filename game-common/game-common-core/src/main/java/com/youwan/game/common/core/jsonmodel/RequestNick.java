package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;

@Data
public class RequestNick implements Serializable {

    Integer openType;
    String channel;
    Integer award;
}

