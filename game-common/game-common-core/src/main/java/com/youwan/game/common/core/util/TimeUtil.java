package com.youwan.game.common.core.util;


import org.apache.commons.lang.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class TimeUtil {
    public static final int DEFAULT = 0;
    public static final int YM = 1;
    public static final int YMR_SLASH = 11;
    public static final int NO_SLASH = 2;
    public static final int YM_NO_SLASH = 3;
    public static final int DATE_TIME = 4;
    public static final int DATE_TIME_NO_SLASH = 5;
    public static final int DATE_HM = 6;
    public static final int TIME = 7;
    public static final int HM = 8;
    public static final int LONG_TIME = 9;
    public static final int SHORT_TIME = 10;
    public static final int DATE_TIME_LINE = 12;

    public static String dateToStr(Date date, String pattern) {
        if ((date == null) || (date.equals("")))
            return null;
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        return formatter.format(date);
    }

    public static String dateToStr(Date date) {
        return dateToStr(date, "yyyy/MM/dd");
    }

    public static String dateToStr(Date date, int type) {
        switch (type) {
            case 0:
                return dateToStr(date);
            case 1:
                return dateToStr(date, "yyyy/MM");
            case 2:
                return dateToStr(date, "yyyyMMdd");
            case 11:
                return dateToStr(date, "yyyy-MM-dd");
            case 3:
                return dateToStr(date, "yyyyMM");
            case 4:
                return dateToStr(date, "yyyy/MM/dd HH:mm:ss");
            case 5:
                return dateToStr(date, "yyyyMMddHHmmss");
            case 6:
                return dateToStr(date, "yyyy/MM/dd HH:mm");
            case 7:
                return dateToStr(date, "HH:mm:ss");
            case 8:
                return dateToStr(date, "HH:mm");
            case 9:
                return dateToStr(date, "HHmmss");
            case 10:
                return dateToStr(date, "HHmm");
            case 12:
                return dateToStr(date, "yyyy-MM-dd HH:mm:ss");
        }
        throw new IllegalArgumentException("Type undefined : " + type);
    }

    public static Integer stringToUnixTimeStamp(String strDate, String pattern) {
        if (StringUtils.isBlank(strDate)) {
            return null;
        }
        DateTimeFormatter fmt = DateTimeFormat.forPattern(pattern);
        return Math.toIntExact(fmt.parseLocalDateTime(strDate).toDate().getTime() / 1000);
    }

    public static void main(String[] args) {
        Integer s = stringToUnixTimeStamp("2020-04-01", "yyyy-MM-dd");
        System.err.println(s);
    }
}