package com.youwan.game.common.core.otherplatform.bg.json.resp;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class UserTransferResp implements Serializable {

    private String id;
    private Float result;
    private Map<Object, Object> error;

    private String jsonrpc;

}