package com.youwan.game.common.core.jsonmodel;


import lombok.Data;

import java.io.Serializable;

@Data
public class GsonLuckRouletteOptions implements Serializable {

    private Integer award;
    private Integer pos;
    private Integer rate;
    private Integer res;
    private String name;
    private Integer value;

}
