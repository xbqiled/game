package com.youwan.game.common.core.jsonmodel;


import lombok.Data;

import java.io.Serializable;


@Data
public class RequestMail implements Serializable {
    private Mail mail;
    private SendCondition sendCondition;

    @Data
    public class Mail {
        private String title;

        private String content;
        private long sendDate;
        private String sender;

        private RewardList [] rewardList;

        @Data
        public class RewardList {
            int id;
            int num;
        }
    }

    @Data
    public class SendCondition {
        private int sendType;
        private int channelList [];
        private int toUserList [];
    }

}
