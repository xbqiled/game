package com.youwan.game.common.core.pay;


import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.youwan.game.common.core.pay.json.ResultMao;
import com.youwan.game.common.core.util.IPUtils;
import com.youwan.game.common.core.util.SignUtil;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;
import static com.youwan.game.common.core.constant.UrlConstant.FEONT_SYSTM_URL;

public class HongYunTongUtil {


    //回调地址
    private static String NOTIFY_URL = "/notify/hongYunTong";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "http://service.88wu1i81r19.ofxtw.com/InterfaceV4/CreatePayOrder/";


    public static Map<String, String> packaData(String... params) {
        Map<String,String> map = new HashMap<>();
        String memberNumber = params[0]; //商户号
        String memberOrderNumber = params[1]; //商户订单号
        String tradeAmount = params[2]; //交易金额
        String defrayalType = params[3]; //支付方式
        String payGateway =params[4]; //支付网关
        ORDER_URL = payGateway;
        String key = params[6];  //商户秘钥
        String ip = params[11];  //商户秘钥
        map.put("MerchantId",memberNumber);
        map.put("Amount",new BigDecimal(tradeAmount).setScale(2).toString());
        map.put("MerchantUniqueOrderId",memberOrderNumber);
        map.put("PayTypeId",defrayalType);
        map.put("Ip", ip);
        map.put("NotifyUrl", API_SYSTM_URL + NOTIFY_URL);
        map.put("ReturnUrl", FEONT_SYSTM_URL + CALL_BACK_URL + memberOrderNumber);
//        String str = SignUtil.formatBizQueryParaMap(map,false);
//        map.put("sign", SignUtil.encryption(str));
        return map;
    }

    public static String doPay(Map map) {
        String resultStr = HttpRequest.post(ORDER_URL).form(map).timeout(10000).execute().body();
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return resultStr;
        }
        return null;
    }

}
