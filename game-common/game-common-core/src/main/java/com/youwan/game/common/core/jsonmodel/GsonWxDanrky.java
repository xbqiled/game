package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonWxDanrky implements Serializable {

    private Integer maxWin;

    private Integer min;

    private Integer max;
    
}
