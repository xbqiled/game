package com.youwan.game.common.core.jsonmodel;


import lombok.Data;

import java.io.Serializable;

@Data
public class RequestCancelOutGameOp implements Serializable {
    private Integer outerType;
    private Integer beforeCoinOpType;
    private Integer beforeCoinOpNum;
    private Integer accountId;
    private String orderId;
}
