package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;

@Data
public class GsonCashBank implements Serializable {
    private String bank;
    private String subBank;

    private String payId;
    private String realName;
}
