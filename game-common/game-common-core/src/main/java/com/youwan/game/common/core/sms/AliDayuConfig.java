package com.youwan.game.common.core.sms;


import lombok.Getter;
import lombok.Setter;


public class AliDayuConfig extends  SmsConfig {

    /**
     * 应用ID
     */
    @Getter
    @Setter
    private String aliAccessKey;

    /**
     * 应用秘钥
     */
    @Getter
    @Setter
    private String aliSecretKey;

    /**
     * 签名
     */
    @Getter
    @Setter
    private String aliSignName;

}
