package com.youwan.game.common.core.pay.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class ResultNiuNiu  implements Serializable {


    private Integer code;

    private String http_url;

    private String sign;

    private String msg;
}
