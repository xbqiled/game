package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-11.
 */
@Data
public class RequestTakeRebate implements Serializable {

    private Long accountID;
    private Double money;
    private String bid;
}
