package com.youwan.game.common.core.jsonmodel;


import lombok.Data;

import java.io.Serializable;

@Data
public class GsonWxFjhs implements Serializable {

    private Integer gameId;

    private Integer fourColorMinBet;

    private Integer fourColorMaxBet;

    private Integer kingMaxBet;
}
