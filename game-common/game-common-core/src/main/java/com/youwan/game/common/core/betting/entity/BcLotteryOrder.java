package com.youwan.game.common.core.betting.entity;

import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 彩票投注记录
 */
@Data
public class BcLotteryOrder extends ThirdPartyRecord implements Serializable {

	public static final int tz_status_end = 1;// 投注结束
	public static final int tz_status_betting = 3;// 投注中
	public static final String joint_status_cg = "1";// 常规投注
	public static final String joint_status_hm = "2";// 合买投注

	public static final String SYS_ALL = "sysAll";// 所有系统彩
	public static final String SYS_NO_SF = "sysNoSF";// 所有系统彩不含十分六合
	public static final String COMMON = "common";// 官方彩
	public static final String OFFICIALLHC = "officialLHC";// 官方六合彩
	
	public static final Integer cancel_switch_on = 1;//开启撤单
	public static final Integer cancel_switch_off = 0;//关闭撤单


	/**
	 * 订单号
	 */
	@Id
	private String orderId;

	/**
	 * 会员账号名
	 */
	private String account;


	private Long accountId;

	/**
	 * 彩票编号
	 */
	private String lotCode;

	/**
	 * 彩种类型，1=系统彩，2=时时彩，3=pk10，4=排列三，5=11选5，6=香港彩，7=PC蛋蛋
	 */
	private Integer lotType;

	/**
	 * 彩票期号
	 */
	private String qiHao;

	/**
	 * 玩法编码
	 */
	private String playCode;

	/**
	 * 玩法名称
	 */
	private String playName;

	/**
	 * 站点ID
	 */
	private Long stationId;

	/**
	 * 购买的号码
	 */
	private String haoMa;

	/**
	 * 购买时间
	 */
	private Date createTime;

	/**
	 * 开奖时间
	 */
	private Date openTime;

	/**
	 * 购买注数
	 */
	private Integer buyZhuShu;

	/**
	 * 中奖注数
	 */
	private Integer winZhuShu;

	/**
	 * 购买倍数
	 */
	private Integer multiple;

	/**
	 * 购买金额
	 */
	private BigDecimal buyMoney;

	/**
	 * 中奖金额
	 */
	private BigDecimal winMoney;

	/**
	 * 状态 1等待开奖 2已中奖 3未中奖 4撤单 5派奖回滚成功 6回滚异常的 7开奖异常
	 */
	private Integer status;

	/**
	 * 模式 1元 10角 100分
	 */
	private Integer model;

	/**
	 * 追号标识，不是追号＝1，中奖继续＝2，中奖中止＝追号第一期的order_id
	 */
	private String zhuiHao;

	/**
	 * 会员返水状态 （1，还未返水 2，已经返水,还未到账 3，返水已经回滚 ,4 反水已经到账 ）多级表示返点(1，还未返点 2，已经返点 3，返点已经回滚)
	 */
	private Integer rollBackStatus;

	/**
	 * 多级表示返点(1，还未返点 2，已经返点 3，返点已经回滚)
	 */
	private Integer agentRebate;

	/**
	 * 六合彩赔率表的id
	 */
	private Long markSixId;

	/**
	 * 开奖时间跟封盘时间差，单位秒
	 */
	private Integer ago;
	/**
	 * 上级用户id
	 */
	private String parents;
	/**
	 * 赔率
	 */
	private BigDecimal odds;
	/**
	 * 1常规(追号)下注，合买=合买方案编号
	 */
	private String jointPurchase;

	/**
	 * v4版本房间号标识
	 */
	private Long roomId;

	/**
	 * 日志对应ID
	 */
	private Long logId;
	
	/**
	 * 群组名称
	 */
	private String qzGroupName;
	
	/**
	 * 投注IP
	 */
	private String betIp;
	/**
	 * 会员备注
	 */
	private String remark;

	private String agentName;

	/**
	 * 开奖号码
	 */
	private String lotteryHaoMa;

	/**
	 * 彩种名称
	 * 
	 * @return
	 */
	private String lotName;

	/**
	 * 返水比例
	 * 
	 * @return
	 */
	private String rollBackRate;

	/**
	 * 返水金额
	 * 
	 * @return
	 */
	private BigDecimal rollBackMoney;

	/**
	 * 盈亏输赢
	 * 
	 * @return
	 */
	private BigDecimal yingKui;

	/**
	 * 彩种奖金
	 * 
	 * @return
	 */
	private BigDecimal minBonusOdds;

	/**
	 * 彩种图标
	 * 
	 * @return
	 */
	private String lotUrl;

	private String imgUrl;

}
