package com.youwan.game.common.core.push.jpush.model;


import com.youwan.game.common.core.push.PushResponse;
import lombok.Data;


@Data
public class JPushResponse extends PushResponse {

}
