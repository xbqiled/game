package com.youwan.game.common.core.constant;


/**
 * <B> 定义消息队列类型</B>
 */
public interface MqQueueConstant {

    /**
     * log rabbit队列名称
     */
    String LOG_QUEUE = "log";

    /**
     * 发送短信验证码队列
     */
    String MOBILE_CODE_QUEUE = "mobile_code_queue";

    /**
     * 短信服务状态队列
     *
     */
    String MOBILE_SERVICE_STATUS_CHANGE = "mobile_service_status_change";

    /**
     * 邮件服务状态队列
     */
    String EMAIL_SERVICE_STATUS_CHANGE = "email_service_status_change";

}
