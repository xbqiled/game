package com.youwan.game.common.core.jsonmodel;


import lombok.Data;

import java.io.Serializable;


@Data
public class RequestRecharge implements Serializable {
    private Integer accountID;
    private String orderId;
    private Integer amount;
    private Integer code;
    private String msg;
    private Integer type;
    private Double rate;
    private Integer isManual;
    private Double damaMulti;
}
