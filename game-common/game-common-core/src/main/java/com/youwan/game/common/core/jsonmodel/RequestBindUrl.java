package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

@Data
public class RequestBindUrl {
    private Integer opType;
    private String url;
    private String channel;
    private Long accountID;
}
