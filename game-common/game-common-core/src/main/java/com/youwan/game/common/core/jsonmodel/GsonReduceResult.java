package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;

@Data
public class GsonReduceResult implements Serializable {

    private int ret;
    private String msg;
    private Long realCancel;

    public GsonReduceResult(int ret) {
        this.ret = ret;
    }


}
