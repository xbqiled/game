package com.youwan.game.common.core.jsonmodel;


import lombok.Data;

import java.io.Serializable;

@Data
public class RequestOutGameOp implements Serializable {
    private Integer outerType;
    private Integer coinOpType;
    private Integer coinOpNum;
    private Integer accountId;
    private String orderId;
    private Integer reason;
}
