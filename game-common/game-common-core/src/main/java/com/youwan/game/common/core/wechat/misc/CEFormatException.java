package com.youwan.game.common.core.wechat.misc;

import java.io.IOException;

public class CEFormatException extends IOException {

    public CEFormatException(String s) {
        super(s);
    }
}
