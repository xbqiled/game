package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonLuckRoulette implements Serializable {

    private Integer award;
    private Integer pos;
    private Integer rate;
    private Integer res;

}
