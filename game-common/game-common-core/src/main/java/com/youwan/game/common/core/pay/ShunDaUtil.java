package com.youwan.game.common.core.pay;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONUtil;
import com.google.gson.Gson;
import com.youwan.game.common.core.pay.json.ResultShunDa;
import com.youwan.game.common.core.util.MD5;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.*;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;
import static com.youwan.game.common.core.constant.UrlConstant.FEONT_SYSTM_URL;


@Slf4j
public class ShunDaUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/shunDa";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "";


    public static Map<String, String> packaData(String... params) {
        String memberNumber =params[0]; //商户号
        String memberOrderNumber =params[1]; //商户订单号
        String tradeAmount =params[2]; //交易金额

        String defrayalType =params[3]; //支付方式
        String payGateway =params[4]; //支付网关
        String commodityBody =params[5]; //商品信息

        String miyao =params[6];  //商户秘钥
        String bankCode =params[7];   //银行代码
        String userId =params[8];   // url传来的userid

        String gongyao =params[9];   //商户公钥
        String appId = params[10];   //
        ORDER_URL = params[4];

        SortedMap<String, String> map = new TreeMap<>();
        map.put("mchId", memberNumber);
        map.put("appId", appId);
        map.put("mchOrderNo", memberOrderNumber);

        map.put("productId", defrayalType);
        map.put("currency", "cny");
        map.put("amount", new BigDecimal(tradeAmount).multiply(new BigDecimal(100)).toString()); // 订单金额（单位：分）

        map.put("returnUrl", FEONT_SYSTM_URL + CALL_BACK_URL + params[1]);
        map.put("notifyUrl", API_SYSTM_URL + NOTIFY_URL);
        map.put("subject", "xxpay1");
        map.put("body", "xxpay1");

        StringBuffer buffer = new StringBuffer();
        Set<Map.Entry<String, String>> set = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();

        while (iterator.hasNext()) {
            Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
            buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }
        String signSrc = buffer.append("key=").append(miyao).toString();
        map.put("sign", MD5.encryption(signSrc).toUpperCase());
        return map;
    }



    public static ResultShunDa doPay(Map map) {
        String resultStr = HttpRequest.post(ORDER_URL).form(map).timeout(10000).execute().body();
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  ResultShunDa.class);
        }
        return  null;
    }

}
