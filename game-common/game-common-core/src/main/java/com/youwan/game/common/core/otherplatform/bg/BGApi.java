package com.youwan.game.common.core.otherplatform.bg;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.google.gson.Gson;
import com.youwan.game.common.core.otherplatform.bg.json.req.*;
import com.youwan.game.common.core.otherplatform.bg.json.resp.*;
import com.youwan.game.common.core.util.Base64;
import com.youwan.game.common.core.util.MD5Util;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;


@Slf4j
public class BGApi {

    private static String getSecretCode(String agentPassword) {
        return Base64.encode(org.apache.commons.codec.digest.DigestUtils.sha1(agentPassword));
    }

    private static String getBalanceSignStr(String random, String sn, String loginId , String secretCode) {
        return MD5Util.md5(random + sn + loginId + secretCode);
    }

    private static String getTransferSignStr(String random, String sn,  String loginId ,  String amount, String secretCode) {
        return MD5Util.md5(random + sn + loginId + amount + secretCode);
    }

    public static String getSignStr(String random, String sn, String secretCode) {
       return MD5Util.md5(random + sn +  secretCode);
    }

    public static String getOrderSignStr(String random, String sn, String orderId, String secretCode) {
        return MD5Util.md5(random + sn + orderId + secretCode);
    }

    public static String getQuerySignStr(String random, String sn, String secretKey) {
        return MD5Util.md5(random + sn +  secretKey);
    }

    public static String getUserSignStr(String random, String sn, String userId, String secretCode) {
        return MD5Util.md5(random + sn + userId +  secretCode);
    }

    /**
     * <B>创建代理账号</B>
     */
    public static CreateAgentResp createAgent(String url, String sn, String loginId, String password) {
        CreateAgentReq createAgentReq = new CreateAgentReq();
        String body = new Gson().toJson(createAgentReq);

        String resultStr = HttpUtil.post(url, body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, CreateAgentResp.class);
        }
        return null;
    }


    /**
     * <B>创建用户账号</B>
     */
    public static CreateUserResp createUser(BGPlatfromConfig bgPlatfromConfig, String userId, String nickName) {
        CreateUserReq createUserReq = new CreateUserReq();
        String id = IdUtil.simpleUUID();
        createUserReq.setId(id);
        CreateUserReq.Params params = new CreateUserReq.Params();

        params.setLoginId(userId);
        params.setNickname(nickName);
        params.setAgentLoginId(bgPlatfromConfig.getAgentAccount());

        params.setSn(bgPlatfromConfig.getSn());
        params.setRandom(id);
        params.setDigest(getSignStr(id, bgPlatfromConfig.getSn(), getSecretCode(bgPlatfromConfig.getAgentPassword())));
        createUserReq.setParams(params);

        String body = new Gson().toJson(createUserReq);
        String resultStr = HttpUtil.post(bgPlatfromConfig.getApiUrl(), body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, CreateUserResp.class);
        }
        return null;
    }

    /**
     * <B>用户额度转换</B>
     * @param bgPlatfromConfig
     * @return
     */
    public static UserTransferResp userBalanceTransfer(BGPlatfromConfig bgPlatfromConfig, String userId, String amount, String orderId) {
       try {
           UserTransferReq userTransferReq = new UserTransferReq();
           String id = IdUtil.simpleUUID();

           userTransferReq.setId(id);
           UserTransferReq.Params params = new UserTransferReq.Params();
           params.setSn(bgPlatfromConfig.getSn());

           params.setRandom(id);
           params.setDigest(getTransferSignStr(id, bgPlatfromConfig.getSn(), userId, amount, getSecretCode(bgPlatfromConfig.getAgentPassword())));
           params.setLoginId(userId);

           params.setAmount(amount);
           params.setBizId(orderId);
           userTransferReq.setParams(params);

           String body = new Gson().toJson(userTransferReq);
           log.debug("bg转账请求Json:" + body);
           String resultStr = HttpUtil.post(bgPlatfromConfig.getApiUrl(), body);
           log.debug("bg转账返回Json:" + resultStr);
           if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
               return new Gson().fromJson(resultStr, UserTransferResp.class);
           }
       } catch (Exception e) {
           return null;
       }
        return null;
    }


    /**
     * <B>获取用户余额</B>
     * @return
     */
    public static UserBalanceResp userBalance(BGPlatfromConfig bgPlatfromConfig, String userId) {
        UserBalanceReq userBalanceReq = new UserBalanceReq();
        String id = IdUtil.simpleUUID();

        userBalanceReq.setId(id);
        UserBalanceReq.Params params = new UserBalanceReq.Params();
        params.setRandom(id);

        params.setLoginId(userId);
        params.setSn(bgPlatfromConfig.getSn());
        params.setDigest(getBalanceSignStr(id, bgPlatfromConfig.getSn(),  userId, getSecretCode(bgPlatfromConfig.getAgentPassword())));
        userBalanceReq.setParams(params);

        String body = new Gson().toJson(userBalanceReq);
        log.debug("bg获取余额请求Json:" + body);
        String resultStr = HttpUtil.post(bgPlatfromConfig.getApiUrl(), body);
        log.debug("bg获取余额返回Json:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, UserBalanceResp.class);
        }
        return null;
    }


    /**
     * <B>打开游戏地址</B>
     */
    public static OpenGameResp openGame(BGPlatfromConfig bgPlatfromConfig, String channelId, String userId, Integer isMobileUrl, Integer isHttpsUrl) {
        OpenGameReq openGameReq = new OpenGameReq();
        String id = IdUtil.simpleUUID();

        openGameReq.setId(id);
        OpenGameReq.Params params = new OpenGameReq.Params();
        params.setDigest(getBalanceSignStr(id, bgPlatfromConfig.getSn(),  userId, getSecretCode(bgPlatfromConfig.getAgentPassword())));
        params.setIsHttpsUrl(isHttpsUrl);

        params.setIsMobileUrl(isMobileUrl);
        params.setLoginId(userId);
        params.setSn(bgPlatfromConfig.getSn());

        params.setRandom(id);
        openGameReq.setParams(params);
        String body = new Gson().toJson(openGameReq);

        String resultStr = HttpUtil.post(bgPlatfromConfig.getApiUrl(), body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, OpenGameResp.class);
        }
        return null;
    }


    public static OrderQueryResp orderQuery (BGPlatfromConfig bgPlatfromConfig, String userId, Date startTime ,
        Date endTime, Long pageIndex, Long pageSize, Integer gameId, String gameName, Long issueId, Long orderId) {
        OrderQueryReq orderQueryReq = new OrderQueryReq();
        String id = IdUtil.simpleUUID();

        orderQueryReq.setId(id);
        OrderQueryReq.Params params = new OrderQueryReq.Params();
        params.setRandom(id);

        params.setSn(bgPlatfromConfig.getSn());
        params.setSign(getQuerySignStr(id, bgPlatfromConfig.getSn(), bgPlatfromConfig.getSecretKey()));
        params.setPageIndex(pageIndex);

        params.setPageSize(pageSize);
        if (StrUtil.isNotEmpty(userId)) {
            Integer [] userArray = {Integer.valueOf(userId)};
            params.setUserIds(userArray);
        }

        if (startTime != null) {
            params.setStartTime(DateUtil.formatDateTime(startTime));
        }

        if (endTime != null) {
            params.setEndTime(DateUtil.formatDateTime(endTime));
        }

        if (gameId != null) {
            params.setGameId(gameId);
        }

        if (gameName != null) {
            params.setGameName(gameName);
        }

        if (issueId != null) {
            params.setIssueId(issueId);
        }

        if (orderId != null) {
            params.setOrderId(orderId);
        }

        orderQueryReq.setParams(params);
        String body = new Gson().toJson(orderQueryReq);

        String resultStr = HttpUtil.post(bgPlatfromConfig.getApiUrl(), body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, OrderQueryResp.class);
        }

        return null;
    }




    public static AgentQueryResp agentQuery (BGPlatfromConfig bgPlatfromConfig, String userId, Date startTime ,
                                             Date endTime, Long pageIndex, Long pageSize, Integer gameId,
                                             String gameName, Long issueId, Long orderId) {
        try {
            AgentQueryReq agentQueryReq = new AgentQueryReq();
            String id = IdUtil.simpleUUID();

            agentQueryReq.setId(id);
            AgentQueryReq.Params params = new AgentQueryReq.Params();
            params.setRandom(id);

            params.setSn(bgPlatfromConfig.getSn());
            params.setDigest(getSignStr(id, bgPlatfromConfig.getSn(), getSecretCode(bgPlatfromConfig.getAgentPassword())));
            params.setPageIndex(pageIndex);

            params.setPageSize(pageSize);
            params.setAgentLoginId(bgPlatfromConfig.getAgentAccount());

            if (StrUtil.isNotEmpty(userId)) {
                String [] loginIdArr = {userId};
                params.setLoginIds(loginIdArr);
            }

            if (startTime != null) {
                params.setStartTime(DateUtil.formatDateTime(startTime));
            }

            if (endTime != null) {
                params.setEndTime(DateUtil.formatDateTime(endTime));
            }

            if (gameId != null) {
                params.setGameId(gameId);
            }

            if (gameName != null) {
                params.setGameName(gameName);
            }

            if (issueId != null) {
                params.setIssueId(issueId);
            }

            if (orderId != null) {
                params.setOrderId(orderId);
            }

            agentQueryReq.setParams(params);
            String body = new Gson().toJson(agentQueryReq);
            log.info("请求打码量==============>" + body);
            String resultStr = HttpUtil.post(bgPlatfromConfig.getApiUrl(), body);
            log.info("请求打码量返回==============>" + resultStr);
            if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
                return new Gson().fromJson(resultStr, AgentQueryResp.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        String s = "{\"id\":\"0ce315bd5ad943eba1f8f9d741f55fc6\",\"result\":{\"total\":6,\"stats\":{\"validAmountTotal\":128.25," +
                "\"aAmountTotal\":204.25,\"userCount\":1,\"paymentTotal\":72.25,\"bAmountTotal_\":132,\"bAmountTotal\":-132,\"paymentTotal_\":72" +
                ".25,\"validBetTotal\":128.25,\"validAmountTotal2\":128.25},\"pageIndex\":1,\"pageSize\":10000,\"etag\":null,\"page\":1," +
                "\"items\":[{\"tranId\":null,\"aAmount\":0,\"loginId\":\"cac1098302\",\"orderId\":6894569042,\"moduleName\":\"视讯\"," +
                "\"orderStatus\":4,\"playId\":\"268435458\",\"uid\":237895902,\"orderTime\":\"2020-07-31 23:48:07\",\"gameName\":\"百家乐\"," +
                "\"payment\":-28,\"sn\":\"ls00\",\"bAmount\":-28,\"moduleId\":2,\"noComm\":null,\"gameId\":1,\"playNameEn\":\"Player\"," +
                "\"issueId\":\"BGB06200731618\",\"playName\":\"下注闲赢\",\"userId\":237895902,\"validAmount\":28,\"gameNameEn\":\"Baccarat\"," +
                "\"fromIp\":\"123.147.246.208\",\"tableId\":\"B06\",\"orderFrom\":5,\"betContent\":null,\"validBet\":28," +
                "\"lastUpdateTime\":\"2020-07-31 23:48:37\"},{\"tranId\":null,\"aAmount\":39,\"loginId\":\"cac1098302\",\"orderId\":6894565067," +
                "\"moduleName\":\"视讯\",\"orderStatus\":2,\"playId\":\"268435457\",\"uid\":237895902,\"orderTime\":\"2020-07-31 23:45:55\"," +
                "\"gameName\":\"百家乐\",\"payment\":19,\"sn\":\"ls00\",\"bAmount\":-20,\"moduleId\":2,\"noComm\":null,\"gameId\":1," +
                "\"playNameEn\":\"Banker\",\"issueId\":\"BGB06200731615\",\"playName\":\"下注庄赢\",\"userId\":237895902,\"validAmount\":19," +
                "\"gameNameEn\":\"Baccarat\",\"fromIp\":\"123.147.246.208\",\"tableId\":\"B06\",\"orderFrom\":5,\"betContent\":null," +
                "\"validBet\":19,\"lastUpdateTime\":\"2020-07-31 23:46:17\"},{\"tranId\":null,\"aAmount\":39,\"loginId\":\"cac1098302\"," +
                "\"orderId\":6894563726,\"moduleName\":\"视讯\",\"orderStatus\":2,\"playId\":\"268435457\",\"uid\":237895902," +
                "\"orderTime\":\"2020-07-31 23:45:04\",\"gameName\":\"百家乐\",\"payment\":19,\"sn\":\"ls00\",\"bAmount\":-20,\"moduleId\":2," +
                "\"noComm\":null,\"gameId\":1,\"playNameEn\":\"Banker\",\"issueId\":\"BGB06200731614\",\"playName\":\"下注庄赢\",\"userId\":237895902," +
                "\"validAmount\":19,\"gameNameEn\":\"Baccarat\",\"fromIp\":\"123.147.246.208\",\"tableId\":\"B06\",\"orderFrom\":5," +
                "\"betContent\":null,\"validBet\":19,\"lastUpdateTime\":\"2020-07-31 23:45:30\"},{\"tranId\":null,\"aAmount\":58," +
                "\"loginId\":\"cac1098302\",\"orderId\":6894548439,\"moduleName\":\"视讯\",\"orderStatus\":2,\"playId\":\"268435458\"," +
                "\"uid\":237895902,\"orderTime\":\"2020-07-31 23:42:41\",\"gameName\":\"百家乐\",\"payment\":29,\"sn\":\"ls00\",\"bAmount\":-29," +
                "\"moduleId\":2,\"noComm\":null,\"gameId\":1,\"playNameEn\":\"Player\",\"issueId\":\"BGB06200731611\",\"playName\":\"下注闲赢\"," +
                "\"userId\":237895902,\"validAmount\":29,\"gameNameEn\":\"Baccarat\",\"fromIp\":\"123.147.246.208\",\"tableId\":\"B06\"," +
                "\"orderFrom\":5,\"betContent\":null,\"validBet\":29,\"lastUpdateTime\":\"2020-07-31 23:42:56\"},{\"tranId\":null,\"aAmount\":44" +
                ".85,\"loginId\":\"cac1098302\",\"orderId\":6894543258,\"moduleName\":\"视讯\",\"orderStatus\":2,\"playId\":\"268435457\"," +
                "\"uid\":237895902,\"orderTime\":\"2020-07-31 23:40:11\",\"gameName\":\"百家乐\",\"payment\":21.85,\"sn\":\"ls00\",\"bAmount\":-23," +
                "\"moduleId\":2,\"noComm\":null,\"gameId\":1,\"playNameEn\":\"Banker\",\"issueId\":\"BGA0120073166F\",\"playName\":\"下注庄赢\"," +
                "\"userId\":237895902,\"validAmount\":21.85,\"gameNameEn\":\"Baccarat\",\"fromIp\":\"123.147.246.208\",\"tableId\":\"A01\"," +
                "\"orderFrom\":5,\"betContent\":null,\"validBet\":21.85,\"lastUpdateTime\":\"2020-07-31 23:40:40\"},{\"tranId\":null,\"aAmount\":23" +
                ".4,\"loginId\":\"cac1098302\",\"orderId\":6894536227,\"moduleName\":\"视讯\",\"orderStatus\":2,\"playId\":\"268435457\"," +
                "\"uid\":237895902,\"orderTime\":\"2020-07-31 23:39:27\",\"gameName\":\"百家乐\",\"payment\":11.4,\"sn\":\"ls00\",\"bAmount\":-12," +
                "\"moduleId\":2,\"noComm\":null,\"gameId\":1,\"playNameEn\":\"Banker\",\"issueId\":\"BGA0120073166E\",\"playName\":\"下注庄赢\"," +
                "\"userId\":237895902,\"validAmount\":11.4,\"gameNameEn\":\"Baccarat\",\"fromIp\":\"123.147.246.208\",\"tableId\":\"A01\"," +
                "\"orderFrom\":5,\"betContent\":null,\"validBet\":11.4,\"lastUpdateTime\":\"2020-07-31 23:39:46\"}]},\"error\":null,\"jsonrpc\":\"2" +
                ".0\"}";
        boolean ret= JSONUtil.isJson(s);
        System.err.println(ret);
        AgentQueryResp resp = new Gson().fromJson(s, AgentQueryResp.class);
        System.err.println(resp);


    }

    /**
     * <B>获取用户信息</B>
     * @param userId
     * @return
     */
    public static UserDetailsResp getUserDetails(BGPlatfromConfig bgPlatfromConfig, String userId) {
        UserDetailsReq UserDetailsReq = new UserDetailsReq();
        String id = IdUtil.simpleUUID();

        UserDetailsReq.setId(id);
        UserDetailsReq.Params params = new UserDetailsReq.Params();
        params.setDigest(getBalanceSignStr(id, bgPlatfromConfig.getSn(),  userId, getSecretCode(bgPlatfromConfig.getAgentPassword())));
        params.setLoginId(userId);
        params.setSn(bgPlatfromConfig.getSn());

        params.setRandom(id);
        UserDetailsReq.setParams(params);
        String body = new Gson().toJson(UserDetailsReq);

        String resultStr = HttpUtil.post(bgPlatfromConfig.getApiUrl(), body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, UserDetailsResp.class);
        }
        return null;
    }



    public static UserRoundResp getUserRound(BGPlatfromConfig bgPlatfromConfig, String round, String startTime, String endTime,
                                             Long pageIndex, Long pageSize) {
        UserRoundReq userRoundReq = new UserRoundReq();
        String id = IdUtil.simpleUUID();

        userRoundReq.setId(id);
        UserRoundReq.Params params = new UserRoundReq.Params();
        params.setSign(getQuerySignStr(id, bgPlatfromConfig.getSn(), bgPlatfromConfig.getSecretKey()));

        params.setRandom(id);
        params.setSn(bgPlatfromConfig.getSn());
        params.setStartTime(startTime);
        params.setEndTime(endTime);

        params.setPageIndex(pageIndex);
        params.setPageSize(pageSize);

        if (StrUtil.isNotEmpty(round)) {
            params.setRound(round);
        }

        userRoundReq.setParams(params);
        String body = new Gson().toJson(userRoundReq);

        String resultStr = HttpUtil.post(bgPlatfromConfig.getApiUrl(), body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, UserRoundResp.class);
        }
        return null;
    }

    public static UserEnableResp userEnable(BGPlatfromConfig bgPlatfromConfig, String userId) {
        UserEnableReq userEnableReq = new UserEnableReq();
        String id = IdUtil.simpleUUID();

        userEnableReq.setId(id);
        UserEnableReq.Params params = new UserEnableReq.Params();
        params.setDigest(getUserSignStr(id, bgPlatfromConfig.getSn(), userId,  getSecretCode(bgPlatfromConfig.getAgentPassword())));

        params.setRandom(id);
        params.setSn(bgPlatfromConfig.getSn());
        params.setLoginId(userId);
        userEnableReq.setParams(params);

        String body = new Gson().toJson(userEnableReq);
        String resultStr = HttpUtil.post(bgPlatfromConfig.getApiUrl(), body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, UserEnableResp.class);
        }
        return null;
    }


    public static UserDisableResp userDisable(BGPlatfromConfig bgPlatfromConfig, String userId) {
        UserDisableReq userDisableReq = new UserDisableReq();
        String id = IdUtil.simpleUUID();

        userDisableReq.setId(id);
        UserDisableReq.Params params = new UserDisableReq.Params();
        params.setDigest(getUserSignStr(id, bgPlatfromConfig.getSn(), userId, getSecretCode(bgPlatfromConfig.getAgentPassword())));

        params.setRandom(id);
        params.setSn(bgPlatfromConfig.getSn());
        params.setLoginId(userId);
        userDisableReq.setParams(params);

        String body = new Gson().toJson(userDisableReq);
        String resultStr = HttpUtil.post(bgPlatfromConfig.getApiUrl(), body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, UserDisableResp.class);
        }
        return null;
    }




    /**
     * <B>用户登出</B>
     */
    public static UserLogoutResp userLogout(BGPlatfromConfig bgPlatfromConfig, String userId) {
        UserLogoutReq userLogoutReq = new UserLogoutReq();
        String id = IdUtil.simpleUUID();

        userLogoutReq.setId(id);
        UserLogoutReq.Params params = new UserLogoutReq.Params();
        params.setSn(bgPlatfromConfig.getSn());

        params.setRandom(id);
        params.setDigest(getSignStr(id, bgPlatfromConfig.getSn(), getSecretCode(bgPlatfromConfig.getAgentPassword())));
        params.setLoginId(userId);

        userLogoutReq.setParams(params);
        String body = new Gson().toJson(userLogoutReq);

        String resultStr = HttpUtil.post(bgPlatfromConfig.getApiUrl(), body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, UserLogoutResp.class);
        }
        return null;
    }

    /**
     * <B>获取订单信息</B>
     */
    public static OrderDetailResp getOrderDetail(BGPlatfromConfig bgPlatfromConfig, String orderId) {
        OrderDetailReq orderDetailReq = new OrderDetailReq();
        String id = IdUtil.simpleUUID();

        orderDetailReq.setId(id);
        OrderDetailReq.Params params = new OrderDetailReq.Params();
        params.setSn(bgPlatfromConfig.getSn());

        params.setRandom(id);
        params.setSign(getOrderSignStr(id, bgPlatfromConfig.getSn(), orderId, bgPlatfromConfig.getSecretKey()));
        params.setOrderId(orderId);

        orderDetailReq.setParams(params);
        String body = new Gson().toJson(orderDetailReq);
        String resultStr = HttpUtil.post(bgPlatfromConfig.getApiUrl(), body);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, OrderDetailResp.class);
        }
        return null;
    }


    /**
     * <B>获取用户红包余额</B>
     * @param bgPlatfromConfig
     * @param userId
     * @return
     */
    public static UserRedPacketBalanceResp getUserRedPacketBalance(BGPlatfromConfig bgPlatfromConfig, String userId,  String startTime, String endTime) {
        UserRedPacketBalanceReq userRedPacketBalanceReq = new UserRedPacketBalanceReq();
        String id = IdUtil.simpleUUID();

        userRedPacketBalanceReq.setId(id);
        UserRedPacketBalanceReq.Params params = new UserRedPacketBalanceReq.Params();
        params.setSn(bgPlatfromConfig.getSn());

        params.setUserId(userId);
        params.setStartTime(startTime);
        params.setEndTime(endTime);

        userRedPacketBalanceReq.setParams(params);
        String body = new Gson().toJson(userRedPacketBalanceReq);
        String resultStr = HttpUtil.post(bgPlatfromConfig.getApiUrl(), body);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, UserRedPacketBalanceResp.class);
        }
        return null;
    }

    /**
     * <B>获取用户红包纪录</B>
     * @param bgPlatfromConfig
     * @param
     * @return
     */
    public static UserRedPacketResp getUserRedPacketRecord(BGPlatfromConfig bgPlatfromConfig, Long pageIndex, Long pageSize, Date startTime, Date endTime) {
        UserRedPacketReq userRedPacketReq = new UserRedPacketReq();
        String id = IdUtil.simpleUUID();

        userRedPacketReq.setId(id);
        UserRedPacketReq.Params params = new UserRedPacketReq.Params();
        params.setSn(bgPlatfromConfig.getSn());

        params.setRandom(id);
        params.setPageIndex(pageIndex);
        params.setPageSize(pageSize);

        params.setSign(getQuerySignStr(id, bgPlatfromConfig.getSn(), bgPlatfromConfig.getSecretKey()));
        if (startTime != null) {
            params.setStartTime(DateUtil.formatDateTime(startTime));
        }

        if (endTime != null) {
            params.setEndTime(DateUtil.formatDateTime(endTime));
        }

        userRedPacketReq.setParams(params);
        String body = new Gson().toJson(userRedPacketReq);
        String resultStr = HttpUtil.post(bgPlatfromConfig.getApiUrl(), body);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, UserRedPacketResp.class);
        }
        return null;
    }

    /**
     * <B>获取用户红包兑换纪录</B>
     * @param bgPlatfromConfig
     * @return
     */
    public static UserRedPacketExchangeResp getUserRedPacketExchange(BGPlatfromConfig bgPlatfromConfig, Long pageIndex, Long pageSize, Date startTime, Date endTime) {
        UserRedPacketExchangeReq userRedPacketExchangeReq = new UserRedPacketExchangeReq();
        String id = IdUtil.simpleUUID();

        userRedPacketExchangeReq.setId(id);
        UserRedPacketExchangeReq.Params params = new UserRedPacketExchangeReq.Params();
        params.setSn(bgPlatfromConfig.getSn());

        params.setRandom(id);
        params.setPageIndex(pageIndex);
        params.setPageSize(pageSize);
        params.setSign(getQuerySignStr(id, bgPlatfromConfig.getSn(), bgPlatfromConfig.getSecretKey()));
        if (startTime != null) {
            params.setStartTime(DateUtil.formatDateTime(startTime));
        }

        if (endTime != null) {
            params.setEndTime(DateUtil.formatDateTime(endTime));
        }

        userRedPacketExchangeReq.setParams(params);
        String body = new Gson().toJson(userRedPacketExchangeReq);
        String resultStr = HttpUtil.post(bgPlatfromConfig.getApiUrl(), body);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, UserRedPacketExchangeResp.class);
        }
        return null;
    }

}
