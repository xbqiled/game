package com.youwan.game.common.core.sms;


/**
 * <B>定义短信执行接口</B>
 */
public interface SmsMessageService {

    /**
     * 执行入口
     *
     * @param mobileMsgTemplate 信息
     */
     void execute(MobileMsgTemplate mobileMsgTemplate, SmsConfig smsConfig);

    /**
     * 数据校验
     *
     * @param mobileMsgTemplate 信息
     */
    void check(MobileMsgTemplate mobileMsgTemplate);

    /**
     * 业务处理
     *
     * @param mobileMsgTemplate 信息
     * @return boolean
     */
    boolean process(MobileMsgTemplate mobileMsgTemplate, SmsConfig config);

    /**
     * 失败处理
     *
     * @param mobileMsgTemplate 信息
     */
    void fail(MobileMsgTemplate mobileMsgTemplate);



}
