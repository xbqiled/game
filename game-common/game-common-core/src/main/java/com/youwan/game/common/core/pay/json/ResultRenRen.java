package com.youwan.game.common.core.pay.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class ResultRenRen  implements Serializable {


    private int Code;

    private String ErrMsg;

    private String Result;

    private String Sign;

}
