package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class RequestGameConfig implements Serializable {

    private String channelKey;
    private Integer nodeIds [];
}
