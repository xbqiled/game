package com.youwan.game.common.core.pay;

import com.youwan.game.common.core.util.MD5;

import java.math.BigDecimal;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;
import static com.youwan.game.common.core.constant.UrlConstant.FEONT_SYSTM_URL;

public class YangGuangUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/yangGuang";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "";

    public static Map<String, String> packaData(String... params) {
        String memberNumber = params[0]; //商户号
        String memberOrderNumber = params[1]; //商户订单号
        String tradeAmount = params[2]; //交易金额

        String defrayalType = params[3]; //支付方式
        String payGateway = params[4]; //支付网关

        String miyao = params[6];  //商户秘钥
        String userId = params[8];   // url传来的userid

        String gongyao = params[9];   //商户公钥
        ORDER_URL = payGateway;

        SortedMap<String, String> map = new TreeMap<String,String>();
        //商户id
        map.put("shopAccountId", memberNumber);
        //商户自己平台用户ID
        map.put("shopUserId", userId);
        //订单金额
        map.put("amountInString", new BigDecimal(tradeAmount).setScale(2).toString());

        map.put("payChannel", defrayalType);
        map.put("shopNo", memberOrderNumber);

        map.put("shopCallbackUrl", API_SYSTM_URL + NOTIFY_URL);
        map.put("returnUrl", FEONT_SYSTM_URL + CALL_BACK_URL + memberOrderNumber);

        map.put("target", "1");
        map.put("apiVersion", "2");
        String signSrc = map.get("shopAccountId") + "&" + map.get("shopUserId") + "&" + map.get("amountInString") + "&" + map.get("shopNo") + "&" + map.get("payChannel") + "&" + gongyao;
        map.put("sign", MD5.encryption(signSrc).toLowerCase());
        return map;
    }

}
