package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonSignAwardCfg implements Serializable {
    private Long beginDay;
    private Long endDay;
    private Long award;
    private Long additional;
    private Long betLimit;
}
