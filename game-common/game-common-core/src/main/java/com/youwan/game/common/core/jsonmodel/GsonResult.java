package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonResult implements Serializable {

    private int ret;
    private String msg;
    private Long rebateCoin;
}
