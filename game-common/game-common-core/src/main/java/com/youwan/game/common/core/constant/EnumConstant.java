package com.youwan.game.common.core.constant;


public class EnumConstant {

    public static  String PayTypeValueOf(String value) {
        String description = "";
        for (PAY_TYPE payTypeEnum : PAY_TYPE.values()) {
            if (payTypeEnum.getValue().equals(value)) {
                description = payTypeEnum.getDescription();
                break;
            }
        }
        return description;
    }

    public enum URL_STATUS {
        URL_ZC(1),

        URL_SX(2),

        URL_LH(3);

        private int value;

        URL_STATUS(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }


    public enum PAY_TYPE_HAND {
        BCL(0),

        PAYURL(1),

        PAGE(2),

        CASHIER(3),

        WEB(4);

        private int value;

        PAY_TYPE_HAND(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }



    public enum REPLACE_POSITION {
        REPLACE_URL(0),

        REPLACE_HEAD(1),

        REPLACE_BODY(2);

        private int value;

        REPLACE_POSITION(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }




    /**
     * 请求方式
     */
    public enum  REQUEST_TYPE{
        POST_REQUEST(0),

        GET_REQUEST(1);

        private int value;

        REQUEST_TYPE(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }



    /**
     * 审批类型
     */
    public enum  APPROVE_STATUS{
        APPLY_STATUS(0),

        SUCCESS_STATUS(1),

        FAIL_STATUS(2);

        private int value;

        APPROVE_STATUS(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }


    public enum HANDLE_TYPE {
        /**
         * 目录
         */
        HAND(0, "手动处理"),

        SYSTEM(1, "系统处理"),

        RECHARGE(2, "手动上分"),

        CANCEL(3, "撤销支付"),

        REQUEST(4, "手动扣款");
        /**
         * 类型
         */
        private int value;
        /**
         * 描述
         */
        private String description;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        HANDLE_TYPE(int value, String description) {
            this.value = value;
            this.description = description;
        }
    }


    public enum PAY_TYPE {
        /**
         * 目录
         */
        PAYTYPE0("0", "微信公众账号"),

        PAYTYPE1("1", "微信扫码支付"),

        PAYTYPE2("2", "微信网页支付"),

        PAYTYPE3("3", "微信app支付"),

        PAYTYPE4("4", "微信转账支付"),

        PAYTYPE5("5", "支付宝扫码"),

        PAYTYPE6("6", "支付宝APP"),

        PAYTYPE7("7", "支付宝网页"),

        PAYTYPE8("8", "支付宝转账"),

        PAYTYPE9("9", "QQ钱包Wap"),

        PAYTYPE10("10", "QQ钱包手机扫码"),

        PAYTYPE11("11", "QQ转账支付"),

        PAYTYPE12("12", "银联二维码"),

        PAYTYPE13("13", "网银B2B支付"),

        PAYTYPE14("14", "网银B2C支付"),

        PAYTYPE15("15", "无卡快捷支付"),

        PAYTYPE16("16", "银联H5快捷"),

        PAYTYPE17("17", "银行卡转账"),

        PAYTYPE18("18", "手动赠送");

        PAY_TYPE(String value, String description) {
            this.value = value;
            this.description = description;
        }
        /**
         * 类型
         */
        private String value;
        /**
         * 描述
         */
        private String description;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

}



