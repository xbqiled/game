package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;



@Data
public class RequestGameMaint  implements Serializable {
      private int operatorType;
      private Integer gameList[];
      private int start;
      private int finish;
}
