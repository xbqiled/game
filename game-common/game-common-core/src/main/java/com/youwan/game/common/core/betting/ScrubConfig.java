package com.youwan.game.common.core.betting;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * @author terry.jiang[taoj555@163.com] on 2020-10-02.
 */
@Data
public class ScrubConfig implements Serializable {

    private Double qp;
    private Double egame;
    private Double live;
    private Double lottery;
    private Double sport;
    private Double esport;
}
