package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

@Data
public class GsonDayShare {
    private Integer totalDay;
    private Long additionAward;
}
