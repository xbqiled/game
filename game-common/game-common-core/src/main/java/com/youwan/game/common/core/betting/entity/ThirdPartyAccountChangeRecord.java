package com.youwan.game.common.core.betting.entity;

import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-08.
 */
@Data
public class ThirdPartyAccountChangeRecord extends ThirdPartyRecord implements Serializable {

    @Id
    private Long id;

    private Long accountId;

    /**
     * 变动前金额
     */
    private BigDecimal beforeMoney;
    /**
     * 变动后金额
     */
    private BigDecimal afterMoney;
    /**
     * 变动金额
     */
    private BigDecimal money;


    private BigDecimal fee;

    /**
     * 帐变类型
     */
    private Long type;

    private Long stationId;

    private Long createUserId;

    private Date createDatetime;

    private String remark;
    /**
     * 对应订单号
     */
    private String orderId;

    private String parents;// 用户的层级关系

    private Date bizDatetime;// 账变对应的订单时间

    private String account;

    //操作者id
    private Long operatorId;

    //操作员账号
    private String operatorName;
}
