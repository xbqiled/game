package com.youwan.game.common.core.otherplatform.bg.json.resp;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


@Data
public class OrderQueryResp implements Serializable {

    public String id;
    public OrderData result;

    @Data
    public static class OrderData {
        public Long total;
        public Long pageIndex;
        public Long pageSize;

        public String etag;
        public List<BgBetOrder> items;
        public BetStats stats;

        @Data
        public static class BetStats {
            public Float validAmountTotal;
            public Float aAmountTotal;
            public Float userCount;
            public Float paymentTotal;
            public Float bAmountTotal_;
            public Float bAmountTotal;
            public Float validBetTotal;
            public Float paymentTotal_;
        }

        @Data
        public static class BgBetOrder {
            public long orderId;
            public long tranId;

            public String sn;
            public long uid;
            public String loginId;

            public int moduleId;
            public String moduleName;
            public int gameId;

            public String gameName;
            public int orderStatus;
            public float bAmount;

            public float aAmount;
            public int orderFrom;
            public Date orderTime;

            public Date lastUpdateTime;
            public String fromIp;
            public String issueId;

            public String  playId;
            public String playName;
            public String playNameEn;

            public float validBet;
            public float payment;
        }
    }


}
