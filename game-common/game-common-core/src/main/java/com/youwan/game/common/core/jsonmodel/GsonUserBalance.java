package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonUserBalance implements Serializable {

    private Integer ret;

    private String msg;

    private Integer outerType;

    private Integer accountId;

    private Integer curCoin;

}
