package com.youwan.game.common.core.pay;

import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.URLUtil;
import com.google.gson.Gson;
import com.youwan.game.common.core.util.AesUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;


public class XinTianDiUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/xinTianDi";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success";
    //下单接口
    private static String ORDER_URL = "";


    public static Map<String, String> packaData(String... params) {
        String memberNumber = params[0]; //商户号
        String memberOrderNumber = params[1]; //商户订单号
        String tradeAmount = params[2]; //交易金额

        String defrayalType = params[3]; //支付方式
        String miyao = params[6];  //商户秘钥
        String appId = params[10];
        ORDER_URL = params[4];


        SortedMap<String, String> map = new TreeMap<>();
        map.put("mer_code", memberNumber);
        //json
        Map<String, Object> resultMap = new HashMap<>();

        //是否手机版
        resultMap.put("is_mobile", 1);
        //支付类型
        resultMap.put("class_code", defrayalType);
        //支付金额
        resultMap.put("amount", new Integer(tradeAmount));
        //订单号
        resultMap.put("order_number", memberOrderNumber);
        //银行编码
        resultMap.put("bank_code", "HF");
        //银行编码
//            resultMap.put("pay_id", appId);
        //用户IP
        resultMap.put("ip", "127.0.0.1");
        //回调地址
        resultMap.put("push_url", API_SYSTM_URL + NOTIFY_URL);

        String json = new Gson().toJson(resultMap);
        AesUtil aes = new AesUtil(json,miyao, miyao,128);
        // RES 加密
        String pay_data = "";
        try {
            pay_data = aes.encryptToString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        map.put("pay_data", URLUtil.decode(pay_data, "UTF-8"));
        return map;
    }

}
