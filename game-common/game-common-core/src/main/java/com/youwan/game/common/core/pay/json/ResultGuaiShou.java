package com.youwan.game.common.core.pay.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class ResultGuaiShou implements Serializable {

    private Boolean result;
    private Data data;
    private Integer errcode;

    @lombok.Data
    public class Data implements Serializable {
        private String payment_code;
    }

}
