package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class RequstUserBalance implements Serializable {

    private Integer outerType;

    private Integer accountId;
}
