package com.youwan.game.common.core.betting;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-08-27.
 */
public enum ThirdPartyPlatform {
    AG(1),
    MG(3),
    QT(4),
    ALLBET(5),
    PT(6),
    BBIN(2),
    OG(7),
    DS(8),
    CQ9(9),
    IBC(10),
    JDB(11),
    KY(12),
    TTG(92),
    MW(95),
    ISB(96),
    VR(97),
    BG(98),
    M8H(991),
    M8(99),
    EBET(13),
    TH(14),
    LH(15),
    KX(16),
    SYS(0);
    private int value;

    public int getValue() {
        return this.value;
    }

    private ThirdPartyPlatform(int value) {
        this.value = value;
    }

    public static ThirdPartyPlatform getPlatform(int platform) {
        ThirdPartyPlatform[] ps = ThirdPartyPlatform.values();
        for (int i = 0; i < ps.length; i++) {
            ThirdPartyPlatform p = ps[i];
            if (p.value == platform) {
                return p;
            }
        }
        return null;
    }

    public static List<ThirdPartyPlatform> getPtsByTs(List<String> types) {
        if (types == null || types.isEmpty()) {
            return null;
        }
        List<ThirdPartyPlatform> pts = new ArrayList<ThirdPartyPlatform>();
        ThirdPartyPlatform[] ps = ThirdPartyPlatform.values();
        for (ThirdPartyPlatform platform : ps) {
            for (String type : types) {
                if (platform.name().equalsIgnoreCase(type.trim())) {
                    pts.add(platform);
                    break;
                }
            }
        }
        return pts;
    }

    public static Integer getIdByType(String type) {
        ThirdPartyPlatform[] ps = ThirdPartyPlatform.values();
        for (int i = 0; i < ps.length; i++) {
            ThirdPartyPlatform p = ps[i];
            if (p.name().equalsIgnoreCase(type)) {
                return p.value;
            }
        }
        return null;
    }

    public static List<Integer> getIdsByTyeps(String types) {
        if (StringUtils.isEmpty(types)) {
            return null;
        }
        String[] tlst = types.split(",");
        if (tlst == null || tlst.length == 0) {
            return null;
        }
        List<Integer> ids = new ArrayList<Integer>();
        Integer id = null;
        for (int i = 0; i < tlst.length; i++) {
            id = getIdByType(tlst[i]);
            if (Objects.nonNull(id)) {
                ids.add(id);
            }
        }
        return ids;
    }
}
