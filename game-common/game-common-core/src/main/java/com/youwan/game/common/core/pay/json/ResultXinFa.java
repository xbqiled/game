package com.youwan.game.common.core.pay.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class ResultXinFa implements Serializable {

    private String merchNo;
    private String stateCode;
    private String msg;

    private String orderNo;
    private String qrcodeUrl;
    private String sign;

}
