package com.youwan.game.common.core.util;

import org.apache.commons.codec.binary.Base64;

import java.math.BigDecimal;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

/**
 * 经典的数字签名算法RSA
 * 数字签名
 * @author kongqz
 * */
public class RSACoder {
    //数字签名，密钥算法
    public static final String KEY_ALGORITHM="RSA";

    /**
     * 数字签名
     * 签名/验证算法
     * */
    public static final String SIGNATURE_ALGORITHM="MD5withRSA";

    /**
     * RSA密钥长度，RSA算法的默认密钥长度是1024
     * 密钥长度必须是64的倍数，在512到65536位之间
     * */
    private static final int KEY_SIZE=1024;
    //公钥
    private static final String PUBLIC_KEY="RSAPublicKey";

    //私钥
    private static final String PRIVATE_KEY="RSAPrivateKey";

    /**
     * 初始化密钥对
     * @return Map 甲方密钥的Map
     * */
    public static Map<String,Object> initKey() throws Exception{
        //实例化密钥生成器
        KeyPairGenerator keyPairGenerator=KeyPairGenerator.getInstance(KEY_ALGORITHM);
        //初始化密钥生成器
        keyPairGenerator.initialize(KEY_SIZE);
        //生成密钥对
        KeyPair keyPair=keyPairGenerator.generateKeyPair();
        //甲方公钥
        RSAPublicKey publicKey=(RSAPublicKey) keyPair.getPublic();
        //甲方私钥
        RSAPrivateKey privateKey=(RSAPrivateKey) keyPair.getPrivate();
        //将密钥存储在map中
        Map<String,Object> keyMap=new HashMap<String,Object>();
        keyMap.put(PUBLIC_KEY, publicKey);
        keyMap.put(PRIVATE_KEY, privateKey);
        return keyMap;

    }


    /**
     * 签名
     * @param privateKey 密钥
     * @return byte[] 数字签名
     * */
    public static byte[] sign(byte[] data,byte[] privateKey) throws Exception{

        //取得私钥
        PKCS8EncodedKeySpec pkcs8KeySpec=new PKCS8EncodedKeySpec(privateKey);
        KeyFactory keyFactory=KeyFactory.getInstance(KEY_ALGORITHM);
        //生成私钥
        PrivateKey priKey=keyFactory.generatePrivate(pkcs8KeySpec);
        //实例化Signature
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
        //初始化Signature
        signature.initSign(priKey);
        //更新
        signature.update(data);
        return signature.sign();
    }
    /**
     * 校验数字签名
     * @param data 待校验数据
     * @param publicKey 公钥
     * @param sign 数字签名
     * @return boolean 校验成功返回true，失败返回false
     * */
    public static boolean verify(byte[] data,byte[] publicKey,byte[] sign) throws Exception{
        //转换公钥材料
        //实例化密钥工厂
        KeyFactory keyFactory=KeyFactory.getInstance(KEY_ALGORITHM);
        //初始化公钥
        //密钥材料转换
        X509EncodedKeySpec x509KeySpec=new X509EncodedKeySpec(publicKey);
        //产生公钥
        PublicKey pubKey=keyFactory.generatePublic(x509KeySpec);
        //实例化Signature
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
        //初始化Signature
        signature.initVerify(pubKey);
        //更新
        signature.update(data);
        //验证
        return signature.verify(sign);
    }
    /**
     * 取得私钥
     * @param keyMap 密钥map
     * @return byte[] 私钥
     * */
    public static byte[] getPrivateKey(Map<String,Object> keyMap){
        Key key=(Key)keyMap.get(PRIVATE_KEY);
        return key.getEncoded();
    }
    /**
     * 取得公钥
     * @param keyMap 密钥map
     * @return byte[] 公钥
     * */
    public static byte[] getPublicKey(Map<String,Object> keyMap) throws Exception{
        Key key=(Key) keyMap.get(PUBLIC_KEY);
        return key.getEncoded();
    }
    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {

//       System.out.println(new BigDecimal(1.00).compareTo(new BigDecimal(0.00)));
//        String str = "13786693955";
//        System.out.println(str.substring(0, 3));
//        //初始化密钥
//        //生成密钥对
//        Map<String,Object> keyMap=RSACoder.initKey();
//        String merchant_public_key = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCUujRem7Hy9ll3ue3+OgCQa3NPeX897rYbYKNKMXjS7bif1M+1OKSoD5HsBADkXWST78ok1RV8f/OtNrEFXxKhcr8uPs2RpheKgjPrBIFxTn54oUmAxARXem1k5KO+uo+cQtpRJsk+YMDssHdy6MSFHHdtlGaJPKy9k+3ApjxWAQIDAQAB";
//        //公钥
//        //byte[] publicKey=RSACoder.getPublicKey(keyMap);
//        byte[] publicKey= Base64.decodeBase64(merchant_public_key);
//        //私钥
//        //byte[] privateKey=RSACoder.getPrivateKey(keyMap);
//        String merchant_private_key = "MIICdgIBAdANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBANX5GMeHwd+ZrpZQ9aKKaD2rrXDXVlh53fOcu5prqQ+ZZ6BzB+0nrARU+Y8pmZdsUxMZ1mYLcDTRbRXh8idkjFUhIfrFrwUKBpu/G5HLZ2k4w03BS1pYz9CYKwyKrQ0cTfVot0k61EQZ8WlYNyaGD96pGUwwDNIgaUspSVutSKnvAgMBAAECgYAuMgJEia20nZRQxZfSiLkqn1bppsPwhBcEElF6bEXqpT73J/6NF+SOZt4eJ2gOhgeFdy2PiGaoMJKxh79k+9ND5LHnkASjOtp5Cqa3/CSHCQxEaFNOabYBZUpm4XqomxeJn5lMv/a9qNNY28Gx3g0dRRpRn0g+c7OPEicqOHjyyQJBAPERjAHN2ZIIRhzHgRhIBEm07M0w6wo+tDn6LEluQbCrU2sV02VblP88RfkIToxafjByAfX+cQlnN8zq2u56DNMCQQDjOeplpZWmByEyqWrOXJUD1vghEeZJguM+FEkknLbxikQ2IxLE46OyBT4FabB7kDnNZCIV1grAFqIxgJDITYz1AkANaUp+tzMJeshbxYWbEjaa2yPpbnVFBqQELbTVCPtCluV3KamvE99AK9xAtIOaL1ah31XYl6U2PrXOAqrXZZbdAkEAoBeH/AHEA+v2CdmvdKFqJABrZfFUjOp47J4iQndftaIzGOlxKeMwzBZBclLaktQ0xW8NTNE3Vcscj0ADwfxRmQJAImSU9H4TGrDFcdGpmd6M+4+SsyUBf35keX/DWZkBqlc9o4lDraC1WSfshEvM6GOzkyd2hJc38OrPqllprI7OeQ==";
//        byte[] privateKey= Base64.decodeBase64(merchant_private_key);
//        System.out.println("公钥：/n"+ Base64.encodeBase64String(publicKey));
//        System.out.println("私钥：/n"+ Base64.encodeBase64String(privateKey));
//
//        String str="RSA数字签名算法";
//        System.out.println("原文:"+str);
//        //甲方进行数据的加密
//        byte[] sign=RSACoder.sign(str.getBytes(), privateKey);
//        System.out.println("产生签名："+ Base64.encodeBase64String(sign));
//        //验证签名
//        boolean status=RSACoder.verify(str.getBytes(), publicKey, sign);
//        System.out.println("状态："+status+"/n/n");
    }
}