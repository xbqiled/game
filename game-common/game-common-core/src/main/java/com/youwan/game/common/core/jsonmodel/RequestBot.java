package com.youwan.game.common.core.jsonmodel;


import lombok.Data;

import java.io.Serializable;

@Data
public class RequestBot implements Serializable {
    private int id;
    private String nick;
}
