package com.youwan.game.common.core.otherplatform.bg.json.resp;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class UserDetailsResp implements Serializable {

    private String id;

    private Result result;

    private Map<Object, Object> error;

    private String jsonrpc;

    @Data
    public static class Result implements Serializable {
        private String birthday;
        private Integer certType;
        private Integer userStatus;

        private String loginId;
        private String gender;
        private String parentPath;
        private Integer awardPoint;

        private String memo;
        private String remark;
        private String idNumber;

        private String userImage;
        private Float balance;
        private String nickname;

        private String tel;
        private String currency;
        private String sn;

        private String wechatName;
        private String realSn;
        private String regType;

        private String email;
        private String passportNumber;
        private String qq;

        private String alipay;
        private String address;
        private String mobile;
        private String wechat;

        private String alipayName;
        private Integer userId ;
        private Integer parentId;

        private String  regIp;
        private Integer loginCount;


        private Integer certNumber;
        private Integer unreadNotice;
        private String  regTime;

        private String  name;
        private String  payPassword;
        private String  loginMobile;

        private Integer status;
    }

}
