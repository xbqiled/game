package com.youwan.game.common.core.sms;


import lombok.Getter;
import lombok.Setter;

public class CloudLetterConfig extends SmsConfig {



    @Getter
    @Setter
    private String account;

    @Getter
    @Setter
    private String clientPw;

    @Getter
    @Setter
    private String intfacePw;

    @Getter
    @Setter
    private String code;

    @Getter
    @Setter
    private String serverUrl;

}
