package com.youwan.game.common.core.pay;

import cn.hutool.core.date.DateUtil;
import com.youwan.game.common.core.util.MD5;

import java.math.BigDecimal;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;

public class SanYiUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/sanYi";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success";
    //下单接口
    private static String ORDER_URL = "";

    public static Map<String, String> packaData(String... params) {
        String memberNumber = params[0]; //商户号
        String memberOrderNumber = params[1]; //商户订单号
        String tradeAmount = params[2]; //交易金额

        String defrayalType = params[3]; //支付方式
        String miyao = params[6];  //商户秘钥
        ORDER_URL = params[4];
        String time = new Long(DateUtil.currentSeconds()).toString();
//        String time = "2576928413";

        SortedMap<String, String> map = new TreeMap<>();
        map.put("userid", memberNumber);
        map.put("orderid", memberOrderNumber);
        map.put("stamp", time);

        map.put("channelcode", defrayalType);
        map.put("amount", new BigDecimal(tradeAmount).toString()); // 订单金额（单位：元）
        map.put("notifyurl", API_SYSTM_URL + NOTIFY_URL);

        StringBuffer sb = new StringBuffer();
        sb.append("amount=" + new BigDecimal(tradeAmount).toString() + "&");
        sb.append("channelcode=" + defrayalType+ "&");
        sb.append("notifyurl=" + API_SYSTM_URL + NOTIFY_URL + "&");
        sb.append("orderid=" + memberOrderNumber + "&");
        sb.append("stamp=" + time + "&");
        sb.append("userid=" + memberNumber + "&");
        sb.append("key=" + miyao);
        String signStr = sb.toString();
        map.put("sign", MD5.encryption(signStr));

//        map.put("sign", "67e26c62f27f403c946a52f8c7552854");
        return map;
    }

}
