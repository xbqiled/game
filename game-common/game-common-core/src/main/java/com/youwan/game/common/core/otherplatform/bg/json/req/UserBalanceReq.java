package com.youwan.game.common.core.otherplatform.bg.json.req;

import lombok.Data;

import java.io.Serializable;


@Data
public class UserBalanceReq implements Serializable {

    private String id;
    private String method = "open.balance.get";
    private Params params;
    private String jsonrpc = "2.0";

    @Data
    public static class Params implements Serializable {
        private String random;

        private String sn;
        private String digest;
        private String loginId;
    }


}
