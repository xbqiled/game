package com.youwan.game.common.core.util;

import javax.crypto.Cipher;
import java.io.ByteArrayOutputStream;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class RsaChengYiTongUtil {

	/** */
	/**
	 * RSA最大加密明文大小
	 */
	private static final int MAX_ENCRYPT_BLOCK = 117;

	/** */
	/**
	 * RSA最大解密密文大小
	 */
	private static final int MAX_DECRYPT_BLOCK = 128;

	public static final String KEY_ALGORITHM = "RSA";
	private static KeyFactory keyFactory = null;

	static {
		try {
			keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	public static Key getPrivateKeyFromBase64UtilsKeyEncodeStr(String keyStr) {
		byte[] keyBytes = Base64Utils.decodeFromString(keyStr);
		// 取得私钥
		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
		Key privateKey = null;
		try {
			privateKey = keyFactory.generatePrivate(pkcs8KeySpec);
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return privateKey;
	}

	/**
	 * 获取Base64Utils加密后的字符串的原始公钥
	 * 
	 * @param keyStr
	 * @return
	 */
	public static Key getPublicKeyFromBase64UtilsKeyEncodeStr(String keyStr) {
		byte[] keyBytes = Base64Utils.decodeFromString(keyStr);
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
		Key publicKey = null;
		try {
			publicKey = keyFactory.generatePublic(x509KeySpec);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return publicKey;
	}

	/**
	 * 使用公钥进行分段加密
	 * 
	 * @param dataStr 要加密的数据
	 * @return 公钥Base64Utils字符串
	 * @throws Exception
	 */
	public static String encryptByPublicKey(String dataStr,String publicKey) throws Exception {
		byte[] data = dataStr.getBytes();
		// 对公钥解密
		Key decodePublicKey = getPublicKeyFromBase64UtilsKeyEncodeStr(publicKey);

		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		// 对数据加密
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, decodePublicKey);
		int inputLen = data.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		// 对数据分段加密
		while (inputLen - offSet > 0) {
			if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
				cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
			} else {
				cache = cipher.doFinal(data, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_ENCRYPT_BLOCK;
		}
		byte[] encryptedData = out.toByteArray();
		out.close();
		String encodedDataStr = Base64Utils.encodeToString(encryptedData);
		return encodedDataStr;
	}

	/**
	 * 使用私钥进行分段解密
	 * 
	 * @param dataStr 使用Base64Utils处理过的密文
	 * @return 解密后的数据
	 * @throws Exception
	 */
	public static String decryptByPrivateKey(String dataStr,String PrivateKey) throws Exception {

		byte[] encryptedData = Base64Utils.decodeFromString(dataStr);

		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key decodePrivateKey = getPrivateKeyFromBase64UtilsKeyEncodeStr(PrivateKey);

		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, decodePrivateKey);
		int inputLen = encryptedData.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int offSet = 0;
		byte[] cache;
		int i = 0;
		// 对数据分段解密
		while (inputLen - offSet > 0) {
			if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
				cache = cipher.doFinal(encryptedData, offSet, MAX_DECRYPT_BLOCK);
			} else {
				cache = cipher.doFinal(encryptedData, offSet, inputLen - offSet);
			}
			out.write(cache, 0, cache.length);
			i++;
			offSet = i * MAX_DECRYPT_BLOCK;
		}
		byte[] decryptedData = out.toByteArray();
		out.close();
		String decodedDataStr = new String(decryptedData, "utf-8");
		return decodedDataStr;
	}
}
