package com.youwan.game.common.core.pay;


import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.google.gson.Gson;
import com.youwan.game.common.core.pay.json.ResultXinFa;
import com.youwan.game.common.core.util.ToolKit;
import sun.misc.BASE64Encoder;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.SortedMap;
import java.util.TreeMap;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;
import static com.youwan.game.common.core.constant.UrlConstant.FEONT_SYSTM_URL;

/**
 * <B>鑫发支付签名并生成</B>
 */
public class XinFaUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/xinFa";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "";

    /**
     * 组装数据
     * @param params
     * @return
     */
    public static String packaData(String... params) {
        String merchantKey = params[6];
        String publicKey = params[9];
        //网关地址
        ORDER_URL = params[4];

        SortedMap<String, String> map = new TreeMap<>();
        //版本
        map.put("version", "V3.3.0.0");
        //商户号
        map.put("merchNo", params[0]);
        //支付通道
        map.put("payType", params[3]);

        //随机数
        map.put("randomNum", ToolKit.randomStr(4));

        //订单号
        map.put("orderNo",  params[1]);
        //支付金额
        map.put("amount", new BigDecimal(params[2]).multiply(new BigDecimal(100)).toString()); // 订单金额（单位：分）
        //商品名称
        map.put("goodsName", "OnlinePay");
        map.put("notifyUrl", API_SYSTM_URL + NOTIFY_URL);
        map.put("notifyViewUrl", FEONT_SYSTM_URL + CALL_BACK_URL + params[1]);

        //map.put("notifyUrl", "http://182.16.53.98:9999/api" + NOTIFY_URL);
        //map.put("notifyViewUrl", "http://182.16.53.98:9999/front" + CALL_BACK_URL + params[1]);
        map.put("charsetCode", "UTF-8");

        String metaSignJsonStr = ToolKit.mapToJson(map);
        String sign = ToolKit.MD5(metaSignJsonStr + merchantKey, ToolKit.CHARSET);// 32位
        map.put("sign", sign.toUpperCase());

        try {
            byte[] dataStr = ToolKit.encryptByPublicKey(ToolKit.mapToJson(map).getBytes(ToolKit.CHARSET), publicKey);
            String param = new BASE64Encoder().encode(dataStr);
            String reqParam = "data=" + URLEncoder.encode(param, ToolKit.CHARSET) + "&merchNo=" + map.get("merchNo");
            return reqParam;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return  null;
    }


    /**
     * <B>去支付</B>
     * @param bodyStr
     * @return
     */
    public static ResultXinFa doPay(String bodyStr) {
        String resultStr =  HttpUtil.post(ORDER_URL, bodyStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  ResultXinFa.class);
        }
        return  null;
    }
}
