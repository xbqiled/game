package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonGameCtrl implements Serializable {

    Integer dangerDown;

    Integer safeDown;

    Integer safeUp;

    Integer dangerUp;
}
