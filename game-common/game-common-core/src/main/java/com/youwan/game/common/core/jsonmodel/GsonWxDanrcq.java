package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonWxDanrcq implements Serializable {

    private Integer id;

    private Integer maxWin;

    private Integer chance;

    private Integer isProtect;
}
