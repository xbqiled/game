package com.youwan.game.common.core.pay;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.google.gson.Gson;
import com.youwan.game.common.core.pay.json.ResultDingDing;
import com.youwan.game.common.core.util.MerchSdkSign;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;

/**
 * <B>钉钉支付</B>
 */
public class DingDingUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/dingDing";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "";


    public static Map<String, String> packaData(String... params) {
        String memberNumber =params[0]; //商户号

        String memberOrderNumber =params[1]; //商户订单号
        String tradeAmount =params[2]; //交易金额
        String defrayalType =params[3]; //支付方式

        String payGateway =params[4]; //支付网关
        String commodityBody =params[5]; //商品信息
        String miyao =params[6];  //商户秘钥
        ORDER_URL = payGateway;

        Map<String, String> postParamMap = new HashMap();
        postParamMap.put("version", "v1.0");//接口版本号
        postParamMap.put("type", defrayalType);//接口类型 aliPay aliPayH5 unionPay wxPay wxPayH5

        postParamMap.put("userId", memberNumber);//商户编号
        postParamMap.put("requestNo", memberOrderNumber);//请求流水号
        BigDecimal value = new BigDecimal(100);
        postParamMap.put("amount",  value.multiply(new BigDecimal(tradeAmount)).toString());//订单金额

        postParamMap.put("callBackURL", API_SYSTM_URL + NOTIFY_URL);//异步回调URL 必填项
        postParamMap.put("redirectUrl", API_SYSTM_URL + CALL_BACK_URL +  params[1]);//支付完成跳转地址,部分不支持,非必填
        String request_sign = MerchSdkSign.getSign(postParamMap, miyao);

        postParamMap.put("sign", request_sign);//签名
        postParamMap.put("attachData", "购买金币");//订单描述
        return postParamMap;
    }


    public static ResultDingDing doPay(Map<String, String> map) {
        String bodyStr = new Gson().toJson(map);
        String resultStr =  HttpUtil.post(ORDER_URL, bodyStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  ResultDingDing.class);
        }

        return  null;
    }

}
