package com.youwan.game.common.core.otherplatform;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


public class PlatformConfig implements Serializable {

    @Getter
    @Setter
    public Integer type;

    @Getter
    @Setter
    public String platformCode;


}
