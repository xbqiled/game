package com.youwan.game.common.core.jsonmodel;


import lombok.Data;

import java.io.Serializable;

@Data
public class GsonUserInfo implements Serializable {

    private int ret;
    private String msg;

    private int insureScore;
    private String nickName;
    private int points;

    private int revenue;
    private int score;
    private int userID;

    private String userName;
    private int UserType;
    private String alipay;

    private String bank;
    private String commonDevices;
    private String iconID;

    private String phoneNum;
    private String thirdAccList;
}
