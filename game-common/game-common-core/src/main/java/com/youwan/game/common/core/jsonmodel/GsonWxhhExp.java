package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;

@Data
public class GsonWxhhExp implements Serializable {

    private Integer isKillInning;

    private Long killInning;

    private Long kingTotalBet;

    private Long kingSysTotalWin;

    private Long kingBetTotalPlayerCnt;

    private Long multiplyTotalXiMa;

    private Long multiplyTotalTax;

    private Long multiplyRewardPoolCoin;

    private Long multiplyTotalWinLose;

    private Integer gameId;

    private String updateTime;

    private String gameKindName;

    private String phoneGameName;

}
