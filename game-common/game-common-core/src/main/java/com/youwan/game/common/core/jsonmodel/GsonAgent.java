package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;

@Data
public class GsonAgent implements Serializable {
    private int ret;
    private String msg;
    private Long accountID;
    private Long promoterId;
    private Long totalNum;

    private Long nextNum;
    private Long histWin;
    private Long canGet;

    private Long nextC;
    private Long otherC;

    private NoteList [] list;

    @Data
    public class NoteList {

        private Long id;
        private Long totalNum;

        private Long nextNum;
        private Long histWin;
        private Long canGet;

        private Long nextC;
        private Long otherC;

    }


}
