package com.youwan.game.common.core.pay;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.google.gson.Gson;
import com.youwan.game.common.core.pay.json.ResultNiuNiu;
import com.youwan.game.common.core.pay.json.ResultQiTongBao;
import com.youwan.game.common.core.util.MD5;

import java.math.BigDecimal;
import java.util.*;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;
import static com.youwan.game.common.core.constant.UrlConstant.FEONT_SYSTM_URL;

public class NiuNiuUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/niuNiu";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "";


    public static Map<String, String> packaData(String... params) {
        String memberNumber =params[0]; //商户号
        String memberOrderNumber =params[1]; //商户订单号
        String tradeAmount =params[2]; //交易金额

        String defrayalType =params[3]; //支付方式
        String payGateway =params[4]; //支付网关
        String commodityBody =params[5]; //商品信息

        String miyao =params[6];  //商户秘钥
        String bankCode =params[7];   //银行代码
        String userId =params[8];   // url传来的userid

        String gongyao =params[9];   //商户公钥
        ORDER_URL = params[4];


        SortedMap<String, String> map = new TreeMap<>();
        map.put("login_id", memberNumber);
        map.put("create_time", System.currentTimeMillis()/1000+"");//System.currentTimeMillis()+""
        map.put("create_ip", "100.100.100.100");
        map.put("nonce", randomStr(4));
        map.put("sign_type", "MD5");

        map.put("pay_type", defrayalType);
        map.put("order_type", "1");
        map.put("order_sn", memberOrderNumber);
        map.put("amount", new BigDecimal(tradeAmount).setScale(2).toString());
        map.put("send_currency", "cny");
        map.put("recv_currency", "cny");
        map.put("extra", "Onlinepay");
        map.put("notify_url", API_SYSTM_URL + NOTIFY_URL);

        StringBuffer buffer = new StringBuffer();
        Set<Map.Entry<String, String>> set = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
            buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }
        String signSrc = buffer.append("api_secret=").append(gongyao).toString();
        map.put("sign", MD5.encryption(signSrc).toLowerCase());
        //map.put("redirectUrl", payGateway);
        return map;

    }


    //随机数
    public static String randomStr(int num) {
        char[] randomMetaData = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        Random random = new Random();
        String tNonceStr = "";
        for (int i = 0; i < num; i++) {
            tNonceStr += (randomMetaData[random
                    .nextInt(randomMetaData.length - 1)]);
        }
        return tNonceStr;
    }


    public static ResultNiuNiu doPay(Map map) {
        String bodyStr = new Gson().toJson(map);
        String resultStr =  HttpUtil.post(ORDER_URL, bodyStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, ResultNiuNiu.class);
        }
        return  null;
    }
}
