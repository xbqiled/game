package com.youwan.game.common.core.push.jpush;

import lombok.Data;

import java.io.Serializable;


@Data
public class JPushConfig implements Serializable {

    private Integer type;

    private String appKey;

    private String masterSecret;
    
}
