package com.youwan.game.common.core.sms;

import com.youwan.game.common.core.constant.ConfigConstant;

public final class SMSFactory {

    public static SmsMessageService build(SmsConfig config) {
        //获取云存储配置信息
        if (config.getType() == ConfigConstant.SmsService.ALIYUN.getValue()) {
            return new SmsAliyunMessageService();
        } else if (config.getType() == ConfigConstant.SmsService.YUNXING.getValue()) {
            return new SmsCloudLetterMessageService();
        } else if (config.getType() == ConfigConstant.SmsService.RONGLIAN.getValue()) {
            return new SmsRongLianMessageService();
        }
        return null;
    }
}
