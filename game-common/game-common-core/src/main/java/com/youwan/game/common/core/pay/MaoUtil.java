package com.youwan.game.common.core.pay;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONUtil;
import com.google.gson.Gson;
import com.youwan.game.common.core.pay.json.ResultMao;
import com.youwan.game.common.core.util.MD5;

import java.math.BigDecimal;
import java.util.*;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;
import static com.youwan.game.common.core.constant.UrlConstant.FEONT_SYSTM_URL;

public class MaoUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/mao";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "";

    public static Map<String, String> packaData(String... params) {
        String memberNumber =params[0]; //商户号
        String memberOrderNumber =params[1]; //商户订单号
        String tradeAmount =params[2]; //交易金额

        String defrayalType =params[3]; //支付方式
        String payGateway =params[4]; //支付网关
        String commodityBody =params[5]; //商品信息

        String miyao =params[6];  //商户秘钥
        String bankCode =params[7];   //银行代码
        String userId =params[8];   // url传来的userid

        String gongyao =params[9];   //商户公钥
        ORDER_URL = payGateway;

        SortedMap<String, String> map = new TreeMap<String,String>();
        map.put("pay_memberid", memberNumber);
        map.put("pay_orderid", memberOrderNumber);
        map.put("pay_applydate", DateUtil.now());

        map.put("pay_bankcode", defrayalType);
        map.put("pay_notifyurl", API_SYSTM_URL + NOTIFY_URL);
        map.put("pay_callbackurl", FEONT_SYSTM_URL + CALL_BACK_URL + memberOrderNumber);

        map.put("pay_amount", new BigDecimal(tradeAmount).setScale(2).toString());
        Set<Map.Entry<String, String>> set = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();

        StringBuffer buffer = new StringBuffer();
        while (iterator.hasNext()) {
            Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
            buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }

        String signSrc = buffer.append("key=").append(gongyao).toString();
        map.put("pay_md5sign", MD5.encryption(signSrc).toUpperCase());
        map.put("pay_productname", "Onlinepay");
        return map;
    }


    public static ResultMao doPay(Map map) {
        String bodyStr = new Gson().toJson(map);
        HttpRequest.closeCookie();
        String resultStr = HttpRequest.post(ORDER_URL).form(map).timeout(10000).execute().body();
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, ResultMao.class);
        }
        return  null;
    }

}
