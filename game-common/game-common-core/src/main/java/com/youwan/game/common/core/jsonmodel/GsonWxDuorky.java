package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonWxDuorky implements Serializable {

    private Integer maxWin;

    private Integer min;

    private Integer max;
    
}
