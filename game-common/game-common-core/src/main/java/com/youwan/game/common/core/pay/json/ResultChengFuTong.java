package com.youwan.game.common.core.pay.json;


import lombok.Data;

import java.io.Serializable;


@Data
public class ResultChengFuTong implements Serializable {

    private Integer rspCode;
    private String rspMsg;
    private Content data;

    @Data
    public class Content implements Serializable {
        private Integer r1_mchtid;
        private String r2_systemorderno;
        private String r3_orderno;
        private Float r4_amount;

        private String r5_version;
        private String r6_qrcode;
        private String r7_paytype;
        private String sign;
    }
}
