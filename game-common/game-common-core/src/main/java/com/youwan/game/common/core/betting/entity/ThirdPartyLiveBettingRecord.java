package com.youwan.game.common.core.betting.entity;


import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 第三方真人投注记录
 */
@Data
public class ThirdPartyLiveBettingRecord  extends ThirdPartyRecord implements Serializable {

    // 游戏类型
    public static final int TYPE_AG = 1;
    public static final int TYPE_MG = 3;
    public static final int TYPE_BBIN = 2;
    public static final int TYPE_QT = 4;
    public static final int TYPE_ALLBET = 5;
    public static final int TYPE_PT = 6;
    public static final int TYPE_OG = 7;
    public static final int TYPE_DS = 8;
    public static final int TYPE_KY = 12;
    public static final int TYPE_VR = 97;
    public static final int TYPE_BG = 98;
    public static final int TYPE_EBET = 13;


    /**
     * 订单ID
     */
    @Id
    private String orderId;

    /**
     * 下注玩法code
     */
    private String bettingCode;

    /**
     * 游戏类型，1=AG，2=MG，3=BBin
     */
    private Integer type;

    /**
     * 平台类型
     */
    private String platformType;
    /**
     * 用户ID
     */
    private Long accountId;
    /**
     * 用户账号
     */
    private String account;

    /**
     * 站点ID
     */
    private Long stationId;

    /**
     * 集群ID
     */
    private Long serverId;

    /**
     * 第三方账号ID
     */
    private Long thirdMemberId;

    /**
     * 第三方账号名
     */
    private String thirdMemberAccount;

    /**
     * 下注前金额
     */
    private BigDecimal beforeMoney;
    /**
     * 下注金额
     */
    private BigDecimal bettingMoney;

    /**
     * 实际下注金额
     */
    private BigDecimal realBettingMoney;

    /**
     * 下注时间
     */
    private Date bettingTime;

    /**
     * 下注时间（Gmt4）
     */
    private Date bettingTimeGmt4;

    /**
     * 盈利金额
     */
    private BigDecimal winMoney;

    /**
     * 游戏类型
     */
    private String gameType;

    /**
     * 游戏类型code
     */
    private String gameTypeCode;

    /**
     * 玩法code
     */
    private String playType;

    /**
     * 玩法code
     */
    private String gameCode;

    /**
     * 桌号
     */
    private String tableCode;

    /**
     * 创建时间
     */
    private Date createDatetime;

    /**
     * 币种
     */
    private String currency;

    /**
     * 登录IP
     */
    private String loginIp;

    /**
     * 注单扩展内容
     */
    private String thirdExtInfo;

    /**
     * 局号
     */
    private String round;

    /**
     * 上级账号
     */
    private String parents;

    private BigDecimal rakebackMoney;
    private BigDecimal rakebackRate;

    private String rakebackDesc;
    private BigDecimal rakebackDrawNeed;

    /**
     * AG游戏 1 为电子桌面游戏 0 为非电子桌面游戏
     *
     * @return
     */
    private Integer gameCategory;
    /**
     * 投注内容
     */
    private String bettingContent;

    /**
     *
     */
    private String stationName;

}
