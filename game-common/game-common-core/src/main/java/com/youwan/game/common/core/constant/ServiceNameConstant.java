package com.youwan.game.common.core.constant;

/**
 * @author lion
 * @date 2018年06月22日16:41:01
 * 服务名称
 */
public interface ServiceNameConstant {
	/**
	 * 认证中心
	 */
	String AUTH_SERVICE = "game-auth";

	/**
	 * UMPS模块
	 */
	String UMPS_SERVICE = "game-upms-biz";


	/**
	 * 棋牌管理模块
	 */
	String CAC_SERVICE = "game-cac-biz";


	/**
	 * 报表管理模块
	 */
	String REPORT_SERVICE = "game-report-biz";


	/**
	 * 分布式事务协调服务
	 */
	String TX_MANAGER = "game-tx-manager";
}
