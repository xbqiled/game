package com.youwan.game.common.core.pay;

import com.youwan.game.common.core.util.MD5;

import java.math.BigDecimal;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;
import static com.youwan.game.common.core.constant.UrlConstant.FEONT_SYSTM_URL;

/**
 * <B>龙城支付</B>
 */
public class LongChengUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/longCheng";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success";
    //下单接口
    private static String ORDER_URL = "";

    public static Map<String, String> packaData(String... params) {
        SortedMap<String, String> map = new TreeMap<>();
        String memberNumber = params[0]; //商户号

        String memberOrderNumber = params[1]; //商户订单号
        String tradeAmount = params[2]; //交易金额
        String defrayalType = params[3]; //支付方式

        String payGateway = params[4]; //支付网关
        String miyao = params[6];  //商户秘钥
        String gongyao =params[9];   //商户公钥
        ORDER_URL = payGateway;

        map.put("version", "1.0");
        map.put("customerid", memberNumber);
        map.put("sdorderno", memberOrderNumber);

        map.put("total_fee", new BigDecimal(tradeAmount).setScale(2).toString());
        map.put("paytype", defrayalType);
        map.put("notifyurl", API_SYSTM_URL + NOTIFY_URL);

        map.put("returnurl", FEONT_SYSTM_URL + CALL_BACK_URL + memberOrderNumber);
        map.put("pay_model","1");
        String signsrc = String.format("version=%s&customerid=%s&total_fee=%s&sdorderno=%s&notifyurl=%s&returnurl=%s&%s",
                map.get("version"),map.get("customerid"),map.get("total_fee"),map.get("sdorderno"),map.get("notifyurl"),map.get("returnurl"), gongyao);
        map.put("sign", MD5.encryption(signsrc).toLowerCase());
        return map;
    }

}
