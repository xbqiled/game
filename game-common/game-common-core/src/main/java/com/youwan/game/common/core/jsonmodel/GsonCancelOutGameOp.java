package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonCancelOutGameOp implements Serializable {

    Integer ret;

    String msg;

    Integer outerType;

    Integer beforeCoinOpType;

    Integer beforeCoinOpNum;

    Integer accountId;

    Integer coinChangeNum;

    Integer leftCoin;
}
