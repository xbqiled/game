package com.youwan.game.common.core.otherplatform.bg;

/**
 * <B>对BG的定义</B>
 */
public enum  BGGameType {

    GAME_TYPE_LIVE("1", "视讯"), GAME_TYPE_EGAME("2", "电子"), GAME_TYPE_LOTTERY("3", "彩票");

    public static BGGameType toGameType(String gt) {
        BGGameType[] gts = BGGameType.values();
        for (int i = 0; i < gts.length; i++) {
            String type = gts[i].getType();
            if (type.equals(gt)) {
                return gts[i];
            }
        }
        return null;
    }

    private String remark;
    private String type;

    private BGGameType(String value, String name) {
        this.type = value;
        this.remark = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
