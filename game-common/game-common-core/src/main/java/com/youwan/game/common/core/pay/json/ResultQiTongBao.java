package com.youwan.game.common.core.pay.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class ResultQiTongBao implements Serializable {

    private String retCode;
    private String retMessage;
    private String sign;
    private String method;
    private String version;
    private String memberNumber;
    private Content content;

    @Data
    public class Content implements Serializable {
        private String memberOrderNumber;
        private String orderNumber;
        private String orderStatus;
        private String content;
    }
}