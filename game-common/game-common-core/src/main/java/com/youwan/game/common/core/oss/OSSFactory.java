package com.youwan.game.common.core.oss;


import com.youwan.game.common.core.constant.ConfigConstant;

/**
 * 文件上传Factory
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2017-03-26 10:18
 */
public final class OSSFactory {

    public static CloudStorageService build(CloudStorageConfig config){
        //获取云存储配置信息
        if(config.getType() == ConfigConstant.CloudService.QINIU.getValue()){
            return new QiniuCloudStorageService(config);
        }else if(config.getType() == ConfigConstant.CloudService.ALIYUN.getValue()){
            return new AliyunCloudStorageService(config);
        }
        return null;
    }

}
