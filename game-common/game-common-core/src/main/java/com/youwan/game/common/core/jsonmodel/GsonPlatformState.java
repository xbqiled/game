package com.youwan.game.common.core.jsonmodel;


import lombok.Data;

import java.io.Serializable;


@Data
public class GsonPlatformState implements Serializable {

    private  long start;
    private  long finish;
    private  int status;

}
