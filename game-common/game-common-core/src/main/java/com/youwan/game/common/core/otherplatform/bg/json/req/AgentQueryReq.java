package com.youwan.game.common.core.otherplatform.bg.json.req;

import lombok.Data;

import java.io.Serializable;

@Data
public class AgentQueryReq implements Serializable {

    private String id;
    private String method = "open.order.agent.query";
    private Params params;
    private String jsonrpc;

    @Data
    public static class  Params implements Serializable {
        private String random;
        private String sn;

        private String digest;
        private String startTime;
        private String endTime;

        private Long pageIndex;
        private Long pageSize;
        private Long [] userIds;
        private String agentLoginId;
        private String [] loginIds;

        //游戏类别
        private Integer gameId;
        //游戏名称
        private String gameName;
        //期数
        private Long issueId;
        //注单号
        private Long orderId;
    }

}
