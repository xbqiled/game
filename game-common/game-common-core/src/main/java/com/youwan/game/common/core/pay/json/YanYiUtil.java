package com.youwan.game.common.core.pay.json;


import cn.hutool.http.HttpRequest;
import com.youwan.game.common.core.util.SignUtil;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;
import static com.youwan.game.common.core.constant.UrlConstant.FEONT_SYSTM_URL;

public class YanYiUtil {


    //回调地址
    private static String NOTIFY_URL = "/notify/yanYi";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "http://www.fj-gx.com:8008/Pay_Index.html";

    public static Map<String, String> packaData(String... params) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Map<String,String> map = new HashMap<>();
        String memberNumber = params[0]; //商户号
        String memberOrderNumber = params[1]; //商户订单号
        String tradeAmount = params[2]; //交易金额
        String defrayalType = params[3]; //支付方式
        String key = params[6];  //商户秘钥
        map.put("pay_memberid", memberNumber);
        map.put("pay_orderid", memberOrderNumber);
        map.put("pay_amount", tradeAmount);
        map.put("pay_applydate", sdf.format(new Date()));
        map.put("pay_bankcode", defrayalType);
        map.put("pay_notifyurl", FEONT_SYSTM_URL + CALL_BACK_URL + memberOrderNumber);
        map.put("pay_callbackurl", API_SYSTM_URL + NOTIFY_URL);
        TreeMap<String, String> treeMap = new TreeMap<>(map);
        String str = SignUtil.mapToUrlString(treeMap)+"&key="+key;
        map.put("pay_md5sign",SignUtil.encryption(str).toUpperCase());
        return map;
    }

}
