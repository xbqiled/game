package com.youwan.game.common.core.pay;


import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONUtil;
import com.google.gson.Gson;
import com.youwan.game.common.core.pay.json.ResultJinZun;
import com.youwan.game.common.core.pay.json.ResultXinXing;
import com.youwan.game.common.core.util.SignUtil;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;
import static com.youwan.game.common.core.constant.UrlConstant.FEONT_SYSTM_URL;

public class JinZunUtil {


    //回调地址
    private static String NOTIFY_URL = "/notify/jinZun";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "http://api.jdzf.net/pay/createOrder";


    public static Map<String, String> packaData(String... params) {
        Map<String,String> map = new HashMap<>();
        String memberNumber = params[0]; //商户号
        String memberOrderNumber = params[1]; //商户订单号
        String tradeAmount = params[2]; //交易金额
        String defrayalType = params[3]; //支付方式
        String payGateway =params[4]; //支付网关
        ORDER_URL = payGateway;
        String key = params[6];  //商户秘钥
        map.put("businessId",memberNumber);
        map.put("amount",new BigDecimal(tradeAmount).setScale(2).toString());
        map.put("outTradeNo",memberOrderNumber);
        map.put("payMethodId",defrayalType);
        map.put("returnUrl", FEONT_SYSTM_URL + CALL_BACK_URL + memberOrderNumber);
        map.put("notifyUrl", API_SYSTM_URL + NOTIFY_URL);
        map.put("random",System.currentTimeMillis()+"");
        map.put("secret",key);
        String str = SignUtil.formatBizQueryParaMap(map,false);
        map.put("sign", SignUtil.encryption(str));
        return map;
    }

    public static Map<String,String> doPay(Map map) {
        Map<String,String> resMap = new HashMap<>();
        String resultStr = HttpRequest.post(ORDER_URL).form(map).timeout(10000).execute().body();
        if (StrUtil.isNotBlank(resultStr) && resultStr.indexOf("script") != -1) {
            resMap.put("payUrl",resultStr.substring(resultStr.indexOf("\"")+1,resultStr.lastIndexOf("\"")));
        }
        resMap.put("msg",resultStr);
        return  resMap;
    }

}
