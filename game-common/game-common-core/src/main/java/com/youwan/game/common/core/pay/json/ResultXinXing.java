package com.youwan.game.common.core.pay.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class ResultXinXing implements Serializable {

    private String agentAcct;
    private String agentOrderId;
    private String channelType;

    private String payUrl;
    private String sign;
    private String statusCode;
    private String statusMsg;
}
