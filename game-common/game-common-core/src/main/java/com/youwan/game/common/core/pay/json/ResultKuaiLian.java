package com.youwan.game.common.core.pay.json;


import lombok.Data;

import java.io.Serializable;


@Data
public class ResultKuaiLian implements Serializable {

        private String orderAmount;
        private String orderNo;
        private String sign;

        private String payUrl;
        private String msg;
        private String merchantNo;
        private String code;

}
