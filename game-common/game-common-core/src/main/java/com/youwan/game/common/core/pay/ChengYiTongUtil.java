package com.youwan.game.common.core.pay;


import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.youwan.game.common.core.pay.json.ResultChengFuTong;
import com.youwan.game.common.core.util.MD5;
import com.youwan.game.common.core.util.RsaChengYiTongUtil;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;

public class ChengYiTongUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/chengYiTong";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "";


    public static Map<String, String> packaData(String... params) {
        SortedMap<String, String> map = new TreeMap<>();

        String memberNumber =params[0]; //商户号
        String memberOrderNumber =params[1]; //商户订单号
        String tradeAmount =params[2]; //交易金额
        ORDER_URL = params[4];
        String userId =params[8];   // url传来的userid
        String gongyao =params[9];   //商户公钥
        String appid =params[10];   //appId

        map.put("p1_mchtid", memberNumber);
        map.put("p2_paytype", "UFWECHAT");

        map.put("p3_paymoney", new BigDecimal(tradeAmount).setScale(2).toString());
        map.put("p4_orderno", memberOrderNumber);
        map.put("p5_callbackurl", API_SYSTM_URL + NOTIFY_URL);

        map.put("p6_notifyurl", API_SYSTM_URL + NOTIFY_URL);
        map.put("p7_version", "v2.9");
        map.put("p8_signtype", "2");

        map.put("p9_attach", "Onlinepay");
        map.put("p10_appname", "Onlinepay");
        map.put("p11_isshow", "0");

        map.put("p12_orderip", "192.168.0.1");
        map.put("p13_memberid", userId);

        String signSrc = String.format("p1_mchtid=%s&p2_paytype=%s&p3_paymoney=%s&p4_orderno=%s&p5_callbackurl=%s&p6_notifyurl=%s&p7_version=%s&p8_signtype=%s&p9_attach=%s&p10_appname=%s&p11_isshow=%s&p12_orderip=%s&p13_memberid=%s",
                map.get("p1_mchtid"),map.get("p2_paytype"),map.get("p3_paymoney"),map.get("p4_orderno"),map.get("p5_callbackurl"),map.get("p6_notifyurl"),map.get("p7_version"),map.get("p8_signtype"),map.get("p9_attach"),map.get("p10_appname"),map.get("p11_isshow"),map.get("p12_orderip"),map.get("p13_memberid"));
        map.put("sign", MD5.encryption(signSrc + appid));

        String reqdata = "";
        try {
            String data = RsaChengYiTongUtil.encryptByPublicKey(JSON.toJSONString(map), gongyao);
            reqdata = URLEncoder.encode(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }

        Map<String,String> resultMap = new HashMap<>();
        resultMap.put("mchtid", memberNumber);
        resultMap.put("reqdata", reqdata);
        return resultMap;
    }


    /**
     * <B>去支付</B>
     * @param map
     * @return
     */
    public static ResultChengFuTong doPay(Map map) {
        String bodyStr = new Gson().toJson(map);
        String resultStr =  HttpUtil.post(ORDER_URL, bodyStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  ResultChengFuTong.class);
        }
        return  null;
    }

}
