package com.youwan.game.common.core.otherplatform.bg.json.req;


import lombok.Data;

import java.io.Serializable;


@Data
public class CreateAgentReq implements Serializable {

    private String id;
    private String mothod = "open.agent.create";
    private Params params;
    private String jsonrpc;

    @Data
    public class Params implements Serializable {
        private String random;
        private String sn;
        private String loginId;
        private String password;
    }


}
