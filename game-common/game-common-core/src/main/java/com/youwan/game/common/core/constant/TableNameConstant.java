package com.youwan.game.common.core.constant;

public class TableNameConstant {

    /*********************龙虎斗牌局************************/
    public static final String LBDBOARD_TABLENAME =  "RecordBoard151";

    public static final String LBDBOARD_VIEWNAME =  "V_RecordBoard151";

    /*******************斗地主牌局************************/
    public static final String DDZBOARD_TABLENAME =  "RecordBoard101";

    public static final String DDZBOARD_VIEWNAME =  "V_RecordBoard101";

    public static final String DDZEXTINFO_TABLENAME =  "RecordExtInfo101";

    public static final String DDZEXTINFO_VIEWNAME =  "V_RecordExtInfo101";

    /*******************跑得快牌局************************/
    public static final String PDKBOARD_TABLENAME =  "RecordBoard206";

    public static final String PDKBOARD_VIEWNAME =  "V_RecordBoard206";

    public static final String PDKEXTINFO_TABLENAME =  "RecordExtInfo206";

    public static final String PDKEXTINFO_VIEWNAME =  "V_RecordExtInfo206";

    /******************捕鱼牌局***************************/
    public static final String BYBOARD_TABLENAME =  "RecordBoard106";

    public static final String BYBOARD_VIEWNAME =  "V_RecordBoard106";


    /******************牛牛牌局***************************/
    public static final String NNBOARD_TABLENAME =  "RecordBoard105";

    public static final String NNBOARD_VIEWNAME =  "V_RecordBoard105";


    /******************财神到牌局***************************/
    public static final String CSDBOARD_TABLENAME =  "RecordBoard181";

    public static final String CSDBOARD_VIEWNAME =  "V_RecordBoard181";


    /******************比花色牌局***************************/
    //public static final String HSJLHBOARD_TABLENAME =  "RecordBoard141";

    //public static final String HSJLHBOARD_VIEWNAME =  "V_RecordBoard141";


    /******************炸金花牌局***************************/
    //public static final String ZJHBOARD_TABLENAME =  "RecordBoard171";

    //public static final String ZJHBOARD_VIEWNAME =  "V_RecordBoard171";

    //public static final String ZJHEXTINFO_TABLENAME =  "RecordExtInfo171";

    //public static final String ZJHEXTINFO_VIEWNAME =  "V_RecordExtInfo171";


    /******************百家乐牌局***************************/
    public static final String BJLBOARD_TABLENAME =  "RecordBoard161";

    public static final String BJLBOARD_VIEWNAME =  "V_RecordBoard161";


    /******************红黑大战牌局***************************/
    //public static final String HHDZBOARD_TABLENAME =  "RecordBoard191";

    //public static final String HHDZBOARD_VIEWNAME =  "V_RecordBoard191";


    /******************红包接龙牌局***************************/
    public static final String HBJLBOARD_TABLENAME =  "RecordBoard200";

    public static final String HBJLBOARD_VIEWNAME =  "V_RecordBoard200";


    /******************红包奖金池***************************/
    public static final String REDBLACKPOOL_TABLENAME =  "RedBlackRewardPool";

    public static final String REDBLACKPOOL_VIEWNAME =  "V_RedBlackRewardPool";


    /******************抢庄牛牛牌局***************************/
    //public static final String QZNNBOARD_TABLENAME =  "RecordBoard201";

    //public static final String QZNNBOARD_VIEWNAME =  "V_RecordBoard201";


    /******************飞禽走兽牌局***************************/
    //public static final String FQZSBOARD_TABLENAME =  "RecordBoard203";

    //public static final String FQZSBOARD_VIEWNAME =  "V_RecordBoard203";


    /******************奔驰宝马牌局***************************/
    //public static final String BCBMBOARD_TABLENAME =  "RecordBoard204";

    //public static final String BCBMBOARD_VIEWNAME =  "V_RecordBoard204";


    /******************水果机牌局***************************/
    public static final String SGJBOARD_TABLENAME =  "recordboard202";

    public static final String SGJBOARD_VIEWNAME =  "v_recordboard202";


    /******************21点牌局***************************/
    //public static final String ESYDBOARD_TABLENAME =  "recordboard205";

    //public static final String ESYDBOARD_VIEWNAME =  "v_recordboard205";


    /*****************用户游戏登录记录表*************************/
    public static final String USERLOGINGAME_TABLENAME =  "recordUserLoginGame";

    public static final String USERLOGINGAME_VIEW =  "V_RecordUserLoginGame";


    /*****************用户游戏登出记录表*************************/
    public static final String USERLOGOUTGAME_TABLENAME =  "recordUserLogoutGame";

    public static final String USERLOGOUTGAME_VIEW =  "V_RecordUserLogoutGame";


    /*****************用户平台登录记录表*************************/
    public static final String USERLOGINPLATFORM_TABLENAME =  "recordUserPlatformLogin";

    public static final String USERLOGINPLATFORM_VIEW =  "V_RecordUserPlatformLogin";


    /*****************用户平台登出记录表*************************/
    public static final String USERLOGOUTPLATFORM_TABLENAME =  "recordUserPlatformLogout";

    public static final String USERLOGOUTPLATFORM_VIEW =  "V_RecordUserPlatformLogout";

    /*****************签到记录表*************************/
    public static final String USERSIGNIN_TABLENAME =  "playerSignIn";

    public static final String USERSIGNIN_VIEW =  "V_PlayerSignIn";


    /*****************推广返利记录表*************************/
    public static final String PLAYDEPOSIT_TABLENAME =  "playerDeposit";

    public static final String PLAYDEPOSIT_VIEW =  "V_PlayerDeposit";


    /******************充值返利**********************************/
    public static final String GAMEREBATE_TABLENAME =  "recordGameRebateDetail";

    public static final String GAMEREBATE_VIEW =  "V_RecordGameRebateDetail";


    /******************分享记录*********************************/
    public static final String DAYSHARE_TABLENAME =  "playerDayShare";

    public static final String DAYSHARE_VIEW =  "V_PlayerDayShare";


    /******************五星宏辉多人模式*********************************/
    public static final String DANTIAOMULTIPLY_TABLENAME =  "dantiaoMultiplyMode";

    public static final String DANTIAOMULTIPLY_VIEW =  "V_DantiaoMultiplyMode";


    /******************五星宏辉单人模式*********************************/
    public static final String DANTIAOSINGLE_TABLENAME =  "dantiaoSingleMode";

    public static final String DANTIAOSINGLE_VIEW =  "V_DantiaoSingleMode";

}
