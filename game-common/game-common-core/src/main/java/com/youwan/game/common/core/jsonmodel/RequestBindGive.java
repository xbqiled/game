package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class RequestBindGive implements Serializable {

    private String channelKey;
    private Integer regularAward;
    private Integer exchange;

    private Integer rebate;
    private Integer minExchange;
    private Integer initCoin;

    private Integer damaMulti;
    private Integer rebateDamaMulti;
    private Integer exchangeCount;

    private Integer promoteType;
    private Integer resetDamaLeftCoin;
    private Integer cpStationId;
    private Integer cpLimit;
}
