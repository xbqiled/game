package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonWechatUserInfo implements Serializable {
    private String openid;//
    private String nickname;
    private int sex;
    private String language;
    private String city;
    private String province;
    private String country;
    private String headimgurl;//用户头像URL
    private String [] privilege;
    private String unionid;
}
