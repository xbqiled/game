package com.youwan.game.common.core.pay;

import cn.hutool.core.date.DateUtil;
import com.youwan.game.common.core.util.MD5;

import java.math.BigDecimal;
import java.util.*;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;

/**
 * <B>怪兽支付</B>
 */
public class HongYunUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/hongYun";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success";
    //下单接口
    private static String ORDER_URL = "";

    public static Map<String, String> packaData(String... params) {
        String memberNumber = params[0]; //商户号

        String memberOrderNumber = params[1]; //商户订单号
        String tradeAmount = params[2]; //交易金额
        String defrayalType = params[3]; //支付方式

        String payGateway = params[4]; //支付网关
        String miyao = params[6];  //商户秘钥
        String userId = params[8];   // url传来的userid
        ORDER_URL = payGateway;

        String notifyUrl = API_SYSTM_URL + NOTIFY_URL;
        String returnUrl = API_SYSTM_URL + CALL_BACK_URL +  params[1];

        SortedMap<String, String> map = new TreeMap<>();
        map.put("pay_memberid", memberNumber);
        map.put("pay_orderid", memberOrderNumber);

        map.put("pay_applydate", DateUtil.now());
        map.put("pay_bankcode", defrayalType);

        map.put("pay_notifyurl", notifyUrl);
        map.put("pay_callbackurl", returnUrl);
        map.put("pay_amount", new BigDecimal(tradeAmount).setScale(2).toString());

        Set<Map.Entry<String, String>> set = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();
        StringBuffer buffer = new StringBuffer();
        while (iterator.hasNext()) {
            Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
            buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }

        String signSrc = buffer.append("key=").append(miyao).toString();
        map.put("pay_md5sign", MD5.encryption(signSrc).toUpperCase());
        map.put("pay_userid", userId);
        map.put("pay_productname", "Onlinepay");
        return map;
    }

}
