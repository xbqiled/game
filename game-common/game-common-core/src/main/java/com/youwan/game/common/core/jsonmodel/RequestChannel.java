package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class RequestChannel implements Serializable {

    private String channelKey;
    private String name;
    private Integer open;

}
