package com.youwan.game.common.core.util.keygen;


import com.youwan.game.common.core.util.keygen.impl.HostNameKeyGenerator;
import com.youwan.game.common.core.util.keygen.impl.IPKeyGenerator;
import com.youwan.game.common.core.util.keygen.impl.IPSectionKeyGenerator;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Key generator factory.
 *
 * @author zhangliang
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class KeyGeneratorFactory {

    private static Map<Class<? extends KeyGenerator>, KeyGenerator> keyGens = new ConcurrentHashMap<>();

    //public  static KeyGenerator
    static {
        keyGens.put(IPKeyGenerator.class, new IPKeyGenerator());
        keyGens.put(IPSectionKeyGenerator.class, new IPSectionKeyGenerator());
        keyGens.put(HostNameKeyGenerator.class, new HostNameKeyGenerator());
    }

    /**
     * Create key generator.
     *
     * @param keyGeneratorClass key generator class name
     * @return key generator instance
     */
    public static KeyGenerator getInstance(final Class<? extends KeyGenerator> keyGeneratorClass) {
        try {
            KeyGenerator keyGenerator = keyGens.get(keyGeneratorClass);
            if (keyGenerator == null) {
                keyGenerator = (KeyGenerator) keyGeneratorClass.newInstance();
                keyGens.put(keyGeneratorClass, keyGenerator);
            }
            return keyGenerator;
        } catch (final ReflectiveOperationException ex) {
            throw new IllegalArgumentException(String.format("Class %s should have public privilege and no argument " +
                    "constructor", keyGeneratorClass));
        }
    }

    public static long ipKeyGeneratorNext() {
        return keyGens.get(IPKeyGenerator.class).generateKey().longValue();
    }

    public static long hostNameKeyGeneratorNext() {
        return keyGens.get(HostNameKeyGenerator.class).generateKey().longValue();
    }

    public static long ipSectionKeyGeneratorNext() {
        return keyGens.get(HostNameKeyGenerator.class).generateKey().longValue();
    }
}
