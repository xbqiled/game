package com.youwan.game.common.core.jsonmodel;


import lombok.Data;

import java.io.Serializable;


@Data
public class RequestWhite implements Serializable {
    private int operatorType;
    private String accountList[];
}
