package com.youwan.game.common.core.betting;

import lombok.Data;

import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-11.
 */
@Data
public class BettingRebateTakingRecord {


    public static final int TYPE_SCRUB = 0;
    public static final int TYPE_PROXY = 1;

    @Id
    private String id;

    private Long uid;

    private String epoch;

    private BigDecimal amount;

    private Date takeTime;

    /**
     * 领取类别 0:洗码返水 1: 代理返利
     */
    private Integer type;
}
