package com.youwan.game.common.core.otherplatform.bg.json.req;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
public class OrderQueryReq implements Serializable {

    private String id;
    private String method = "open.order.query";
    private Params params;
    private String jsonrpc;

    @Data
    public static class  Params implements Serializable {
        private String random;
        private String sn;

        private String sign;
        private String startTime;
        private String endTime;

        private Long pageIndex;
        private Long pageSize;
        private Integer [] userIds;

        //游戏类别
        private Integer gameId;
        //游戏名称
        private String gameName;
        //期数
        private Long issueId;
        //注单号
        private Long orderId;
    }

}
