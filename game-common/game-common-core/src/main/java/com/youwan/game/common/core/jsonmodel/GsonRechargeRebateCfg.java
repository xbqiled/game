package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonRechargeRebateCfg implements Serializable {
    private Long minCoin;
    private Long maxCoin;
    private Integer rebateType;
    private Long rebateContent;
}
