package com.youwan.game.common.core.betting;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-20.
 */
@Data
public class AgentPerformanceRecord {

    public final static int TYPE_DIRECT = 0;
    public final static int TYPE_INDIRECT = 1;

    public final static int SOURCE_LOTTERY = 0;
    public final static int SOURCE_CROWN = 1;
    public final static int SOURCE_EGAME = 2;
    public final static int SOURCE_LIVE = 3;
    public final static int SOURCE_SPORT = 4;
    /**
     * 用户id
     */
    private Long uid;

    /**
     * 下级
     */
    private Long subordinate;

    /**
     * 类型 0: 直属业绩 1: 下属业绩
     */
    private Integer type;

    /**
     * 业绩值 RMB
     */
    private BigDecimal value;

    /**
     * 投注来源 0:彩票, 1:皇冠 2:电子 3：真人 4: 第三方体育
     */
    private Integer source;

    /**
     *
     */
    private String epoch;

    /**
     * 是否统计
     */
    private Boolean statFlag = false;

    /**
     *
     */
    private String m_path;
}
