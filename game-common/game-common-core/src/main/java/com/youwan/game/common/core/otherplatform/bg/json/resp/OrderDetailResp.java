package com.youwan.game.common.core.otherplatform.bg.json.resp;


import lombok.Data;

import java.io.Serializable;


@Data
public class OrderDetailResp implements Serializable {
    public String result;
}
