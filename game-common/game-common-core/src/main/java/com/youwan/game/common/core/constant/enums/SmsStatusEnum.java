package com.youwan.game.common.core.constant.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SmsStatusEnum {


    /**
     * 发送中
     */
    SENDING("0", "发送中"),

    /**
     * 发送成功
     */
    SUCCESS("1", "发送成功"),

    /**
     * 已完成
     */
    FAIL("2", "发送失败");


    /**
     * 类型
     */
    private final String status;
    /**
     * 描述
     */
    private final String description;


}
