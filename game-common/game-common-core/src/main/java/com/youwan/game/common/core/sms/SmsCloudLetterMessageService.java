package com.youwan.game.common.core.sms;


import cn.hutool.http.HttpUtil;
import com.google.gson.Gson;
import com.youwan.game.common.core.util.Assert;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;


@Slf4j
public class SmsCloudLetterMessageService  extends AbstractMessageService {

    @Override
    public void execute(MobileMsgTemplate mobileMsgTemplate, SmsConfig smsConfig) {
        super.execute(mobileMsgTemplate, smsConfig);
    }

    @Override
    public void check(MobileMsgTemplate mobileMsgTemplate) {
        Assert.isBlank(mobileMsgTemplate.getMobile(), "手机号不能为空");
        Assert.isBlank(mobileMsgTemplate.getContext(), "短信内容不能为空");
    }

    @Override
    public boolean process(MobileMsgTemplate mobileMsgTemplate, SmsConfig config) {
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("action", "send");
        paramMap.put("account", ((CloudLetterConfig)config).getAccount());

        paramMap.put("password", ((CloudLetterConfig)config).getIntfacePw());
        paramMap.put("mobile", mobileMsgTemplate.getMobile());
        paramMap.put("content", mobileMsgTemplate.getContext());

        paramMap.put("rt", "json");
        paramMap.put("extno", ((CloudLetterConfig)config).getCode());
        String result = HttpUtil.post(((CloudLetterConfig)config).getServerUrl(), paramMap);

        GsonSms json = new Gson().fromJson(result, GsonSms.class);
        if (json.getStatus().equals("0")) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    @Override
    public void fail(MobileMsgTemplate mobileMsgTemplate) {
        log.error("短信发送失败 -> 网关：{} -> 手机号：{}", mobileMsgTemplate.getType(), mobileMsgTemplate.getMobile());
    }
}
