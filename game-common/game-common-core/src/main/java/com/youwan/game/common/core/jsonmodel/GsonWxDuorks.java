package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class GsonWxDuorks implements Serializable {

    private Integer maxLose;

    private Integer min;

    private Integer max;
    
}
