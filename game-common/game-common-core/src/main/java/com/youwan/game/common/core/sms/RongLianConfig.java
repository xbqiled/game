package com.youwan.game.common.core.sms;


import lombok.Getter;
import lombok.Setter;


public class RongLianConfig extends SmsConfig {

    @Getter
    @Setter
    private String rongLianServerIp;

    @Getter
    @Setter
    private String rongLianServerPort;

    @Getter
    @Setter
    private String rongLianAccount;

    @Getter
    @Setter
    private String rongLianToken;

    @Getter
    @Setter
    private String rongLianAppId;
}
