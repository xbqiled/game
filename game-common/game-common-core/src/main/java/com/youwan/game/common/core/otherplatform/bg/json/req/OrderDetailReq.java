package com.youwan.game.common.core.otherplatform.bg.json.req;

import lombok.Data;

import java.io.Serializable;

@Data
public class OrderDetailReq implements Serializable {

    private String id;
    private String method = "open.sn.video.order.detail.url.get";
    private Params params;
    private String jsonrpc;

    @Data
    public static class  Params implements Serializable {
        private String random;
        private String sn;

        private String sign;
        private String orderId;
        private String locale;
    }

}
