package com.youwan.game.common.core.pay.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class ResultDingDing implements Serializable {

    private String message;

    private String status;

    private String payUrl;


}
