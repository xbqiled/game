package com.youwan.game.common.core.jsonmodel;


import lombok.Data;

import java.io.Serializable;


@Data
public class RequestLuckyRoulette implements Serializable {

    private Long channel;
    private Integer silverCost;
    private Integer goldCost;

    private Integer openType;

    private Integer diamondCost;
    private Long start;
    private Long finish;

    private GsonLuckRoulette [] diamond;
    private GsonLuckRoulette [] gold;
    private GsonLuckRoulette [] silver;

}
