package com.youwan.game.common.core.pay;


import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.google.gson.Gson;
import com.youwan.game.common.core.pay.json.ResultKuaiLian;
import com.youwan.game.common.core.util.SignUtil;

import java.math.BigDecimal;
import java.util.*;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;
import static com.youwan.game.common.core.constant.UrlConstant.FEONT_SYSTM_URL;

/**
 * <B>快联支付签名并生成</B>
 */
public class KuaiLianUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/kuaiLian";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "";

    /**
     * 组装数据
     * @param params
     * @return
     */
    public static Map<String, String> packaData(String... params) {
        SortedMap<String, String> map = new TreeMap<>();

        String merchantKey = params[6];
        //设置下单地址
        ORDER_URL = params[4];
        //商户订单号
        map.put("merchantNo", params[0]);
        //订单号
        map.put("orderNo", params[1]);
        //支付金额  单位从元转成分
        map.put("orderAmount", new BigDecimal(params[2]).multiply(new BigDecimal(100)).toString());
        //map.put("orderAmount", "1");
        //支付类型
        map.put("payType", params[3]);
        //产品名称
        map.put("productName", params[5]);
        //如果是11
        if(params[3] != null && params[3].equals("11")) {
            map.put("bankName", params[7]);
        }

        //回调地址
        map.put("notifyUrl",  API_SYSTM_URL + NOTIFY_URL);
        //回调界面
        map.put("callbackUrl", FEONT_SYSTM_URL + CALL_BACK_URL + params[1]);

        //生成签名信息
        StringBuffer buffer = new StringBuffer();
        Set<Map.Entry<String, String>> set = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();

        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }

        String signSrc = buffer.substring(0, buffer.length()-1) + merchantKey;
        map.put("sign", SignUtil.getSign(signSrc));
        //map.put("redirectUrl", payGetwayStr);
        return map;
    }


    /**
     * <B>去支付</B>
     * @param map
     * @return
     */
    public static ResultKuaiLian doPay(Map<String, String> map) {
        String bodyStr = new Gson().toJson(map);
        String resultStr =  HttpUtil.post(ORDER_URL, bodyStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  ResultKuaiLian.class);
        }
        return  null;
    }
}
