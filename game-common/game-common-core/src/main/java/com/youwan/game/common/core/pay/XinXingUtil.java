package com.youwan.game.common.core.pay;


import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONUtil;
import com.google.gson.Gson;
import com.youwan.game.common.core.pay.json.ResultXinXing;
import com.youwan.game.common.core.util.MD5;

import java.math.BigDecimal;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;

public class XinXingUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/xinXing";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "";

    public static Map<String, String> packaData(String... params) {
        try {
            String memberNumber = params[0]; //商户号
            String memberOrderNumber = params[1]; //商户订单号
            String tradeAmount = params[2]; //交易金额

            String defrayalType = params[3]; //支付方式
            String payGateway = params[4]; //支付网关

            String miyao = params[6];  //商户秘钥
            String userId = params[8];   // url传来的userid
            ORDER_URL = payGateway;
            String callback = java.net.URLEncoder.encode(API_SYSTM_URL + NOTIFY_URL,"utf-8");
            String amount = new BigDecimal(tradeAmount).multiply(new BigDecimal(100)).toString();
            String timestamp =  String.valueOf(DateUtil.current(false));

            SortedMap<String, String> map = new TreeMap<>();
            //商户id
            map.put("agentAcct", memberNumber);
            //商户自己平台用户ID
            map.put("agentOrderId", memberOrderNumber);
            //业务类型
            map.put("bizType", "E_CHARGE");
            //充值账号
            map.put("number", "18002201314");
            //订单金额
            map.put("amount", amount);
            //支付类型
            map.put("channel", defrayalType);
            //时间
            map.put("timestamp", timestamp);
            //回调
            map.put("callback", callback);
            StringBuffer sb = new StringBuffer();
            sb.append("agentAcct=" + map.get("agentAcct"));
            sb.append("&agentOrderId=" +  map.get("agentOrderId"));
            sb.append("&amount=" +  map.get("amount"));
            sb.append("&bizType=" +  map.get("bizType"));
            sb.append("&callback=" +  map.get("callback"));
            sb.append("&channel=" +  map.get("channel"));
            sb.append("&number=" +  map.get("number"));
            sb.append("&timestamp=" + timestamp);
            sb.append("&key=" +  miyao);

            map.put("sign", MD5.encryption(sb.toString()));
            return map;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static ResultXinXing doPay(Map map) {
        String resultStr = HttpRequest.post(ORDER_URL).form(map).timeout(10000).execute().body();
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  ResultXinXing.class);
        }
        return  null;
    }
}
