package com.youwan.game.common.core.otherplatform.bg.json.resp;


import lombok.Data;

import java.io.Serializable;


@Data
public class UserBalanceResp implements Serializable {

    private String id;
    private String result;
    private String jsonpc;

}
