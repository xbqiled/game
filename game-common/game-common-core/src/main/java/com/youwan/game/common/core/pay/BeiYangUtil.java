package com.youwan.game.common.core.pay;


import com.youwan.game.common.core.util.Base64Utils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;

public class BeiYangUtil {


    //回调地址
    private static String NOTIFY_URL = "/notify/beiYang";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "";


    public static Map<String, String> packaData(String... params) {
        SortedMap<String, String> map = new TreeMap<>();

        String memberNumber =params[0]; //商户号
        String memberOrderNumber =params[1]; //商户订单号
        String tradeAmount =params[2]; //交易金额
        String defrayalType =params[3];
        ORDER_URL = params[4];
        String userId =params[8];   // url传来的userid
        String gongyao =params[9];   //商户公钥
        String appid =params[10];   //appId

        map.put("cid", memberNumber);
        map.put("uid", userId);
        String time = System.currentTimeMillis()/1000+"";
        map.put("time", time);
        map.put("amount", new BigDecimal(tradeAmount).setScale(2).toString());
        map.put("order_id", memberOrderNumber);
        map.put("ip", "0.0.0.0");
        map.put("syncurl", API_SYSTM_URL + NOTIFY_URL);
        map.put("type", defrayalType);
		map.put("tflag", appid);

        String signSrc = String.format("cid=%s&uid=%s&time=%s&amount=%s&order_id=%s&ip=%s",
                map.get("cid"), map.get("uid"), map.get("time"), map.get("amount"), map.get("order_id"), map.get("ip"));

        byte[] result = null;
        String HMAC_SHA1_ALGORITHM = "HmacSHA1"; SecretKeySpec signingKey = new SecretKeySpec(gongyao.getBytes(),HMAC_SHA1_ALGORITHM);
        Mac mac = null;
        try {
            mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
            mac.init(signingKey);
        } catch (InvalidKeyException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] rawHmac = mac.doFinal(signSrc.getBytes());
        result = Base64Utils.encode(rawHmac);

        map.put("sign", new String(result));
        return map;
    }

}
