package com.youwan.game.common.core.pay;


import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.youwan.game.common.core.pay.json.ResultRenRen;
import com.youwan.game.common.core.pay.json.UrlResultRenRen;
import com.youwan.game.common.core.util.RenRenAESUtil;
import com.youwan.game.common.core.util.RenRenMd5Util;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;

public class RenRenUtil {


    //回调地址
    private static String NOTIFY_URL = "/notify/renRen";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "";
    //公鑰
    private static String PUBLIC_KEY ="";


    public static Map<String, Object> packaData(String... params) {
        String memberNumber = params[0]; //商户号
        String memberOrderNumber = params[1]; //商户订单号
        String tradeAmount = params[2]; //交易金额

        String defrayalType = params[3]; //支付方式
        String payGateway = params[4]; //支付网关
        String commodityBody = params[5]; //商品信息

        String miyao = params[6];  //商户秘钥
        String bankCode = params[7];   //银行代码
        String userId = params[8];   // url传来的userid

        String gongyao = params[9];   //商户公钥
        ORDER_URL = payGateway;
        Map<String, Object> map = new HashMap<>();

        map.put("MerchantOrderNo", memberOrderNumber);
        map.put("ChannelCode", defrayalType);
        map.put("OrderPrice", new BigDecimal(tradeAmount).setScale(2).toString());

        map.put("CallBackUrl", API_SYSTM_URL + NOTIFY_URL);
        map.put("Message", commodityBody);
        map.put("PlatFormUserID", userId);

        //签名
        Map<String, Object> sendMap = new HashMap<>();
        String data = getReqEncryptData(map, gongyao);
        String action ="deposit";
        String merchantAccount = memberNumber;
        String sign = getReqSign(action, data, merchantAccount, miyao);

        sendMap.put("MerchantAccount", merchantAccount);
        sendMap.put("Action", action);
        sendMap.put("Data", data);
        sendMap.put("Sign", sign);

        return sendMap;
    }


    public static String getReqSign(String action, String data, String merchantAccount, String key) {
        //使用seckey对除sign以外的公共参数进行md5加密生成sign值
        String toSignStr = "Action=" +  action + "&Data=" +  data + "&MerchantAccount=" + merchantAccount + "&Key=" + key;
        String reqSign = RenRenMd5Util.md5Sign(toSignStr).toUpperCase();
        return reqSign;
    }


    public static String getReqEncryptData(Map<String, Object> paramMap, String key) {
        String encryptData = "";
        try {
            encryptData = RenRenAESUtil.encodeHexStr(RenRenAESUtil.encrypt(key, JSON.toJSONString(paramMap)), false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encryptData;
    }


    public static UrlResultRenRen doPay(Map<String, Object> map, String publicKey) {
        String bodyStr = new Gson().toJson(map);
        String resultStr =  HttpUtil.post(ORDER_URL, bodyStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            ResultRenRen renrenResult = new Gson().fromJson(resultStr,  ResultRenRen.class);
            String result  = getRes(renrenResult, publicKey);

            if (StrUtil.isNotBlank(result) && JSONUtil.isJson(result)) {
                UrlResultRenRen urlResultRenRen = new Gson().fromJson(result,  UrlResultRenRen.class);
                return urlResultRenRen;
            }
        }
        return  null;
    }


    public static String getRes(ResultRenRen resJson, String publicKey) {
        //处理rrp的成功响应
        if (resJson.getCode() == 0) {
            //对比签名正确
            if (RenRenAESUtil.verifySign(resJson.getSign(), resJson.getResult(), new Integer(resJson.getCode()).toString(),  publicKey)) {
                //解密响应
                try {
                    String resData = RenRenAESUtil.decrypt(publicKey, resJson.getResult());
                    return resData;
                } catch (Exception e) {
                    return resJson.toString();
                }
            }
        } else {
            return resJson.toString();
        }
        return resJson.toString();
    }
}