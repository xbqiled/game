package com.youwan.game.common.core.otherplatform.bg.json.resp;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
public class UserRoundResp implements Serializable {

    private String id;

    private Result result;

    private Map<Object, Object> error;

    private String jsonrpc;

    @Data
    public static class Result implements Serializable {

        public Long total;
        public Long pageIndex;
        public Long pageSize;

        public String etag;

        public List<RoundOrder> items;
        public RoundStats stats;

        @Data
        public static class RoundStats {
            public Float validAmountTotal;
            public Float aAmountTotal;
            public Float userCount;
            public Float paymentTotal;
            public Float bAmountTotal_;
            public Float bAmountTotal;
            public Float validBetTotal;
            public Float paymentTotal_;
        }

        @Data
        public static class RoundOrder {
            public Integer payoutAmount;
            public String betResult;

            public Integer gameType;
            public String baccarat;
            public String baccarat64;

            public Integer dealerId;
            public String serialNo;
            public String result;

            public Integer betAmount;
            public String gameName;
            public String calcTime;

            public String tableId;
            public String dealer;
            public Integer bettings;

            public String openTime;
            public Integer status;
        }

    }

}