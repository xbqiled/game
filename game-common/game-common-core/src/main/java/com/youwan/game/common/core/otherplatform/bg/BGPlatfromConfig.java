package com.youwan.game.common.core.otherplatform.bg;

import com.youwan.game.common.core.otherplatform.PlatformConfig;
import lombok.Data;


@Data
public class BGPlatfromConfig extends PlatformConfig {


    public BGPlatfromConfig (Integer type, String platformCode) {
        this.type = type;
        this.platformCode = platformCode;
    }

    //固定前缀
    private String prefix;

    //动态前缀长队，值范围2-4
    private String dynPrefixLen;

    //请求url
    private String apiUrl;

    //厅代码
    private String sn;

    //代理账号
    private String agentAccount;

    //代理密码
    private String agentPassword;

    //代理秘钥
    private String secretKey;

}
