package com.youwan.game.common.core.otherplatform;


import com.google.gson.Gson;
import com.youwan.game.common.core.constant.ConfigConstant;
import com.youwan.game.common.core.otherplatform.bg.BGPlatfromConfig;

/**
 * @author lion
 * @email ismyjuliet@gmail.com
 * @date 2017-03-26 10:18
 */
public final class PlatformFactory {

    /**
     * <B>类型转换</B>
     * @param type
     * @param platformCode
     * @return
     */
    public static PlatformConfig build(Integer type, String platformCode){
        //获取云存储配置信息
        if(type == ConfigConstant.PlatformService.BG.getValue()){
            return new BGPlatfromConfig(type, platformCode);
        }else if(type == ConfigConstant.PlatformService.AG.getValue()){
            return null;
        }
        return null;
    }

    /**
     * json转换成模型
     * @param type
     * @param jsonStr
     * @return
     */
    public static PlatformConfig json2Model(Integer type, String jsonStr){
        //获取云存储配置信息
        if(type == ConfigConstant.PlatformService.BG.getValue()){
            return new Gson().fromJson(jsonStr, BGPlatfromConfig.class);
        }else if(type == ConfigConstant.PlatformService.AG.getValue()){
            return null;
        }
        return null;
    }

}
