package com.youwan.game.common.core.util;

public class TableUtils {


    public static String getUserTableName(Integer accountId) {
        String accountTableName = "";

        Integer mo = Integer.valueOf(accountId) % 4;
        switch (mo) {
            case 0:
                accountTableName = "xbqp_data1.t_accountinfo";
                break;

            case 1:
                accountTableName = "xbqp_data2.t_accountinfo";
                break;

            case 2:
                accountTableName = "xbqp_data3.t_accountinfo";
                break;

            case 3:
                accountTableName = "xbqp_data4.t_accountinfo";
                break;
        }

        return accountTableName;
    }


    public static String getUserBaseTableName(Integer accountId) {
        String userbaseattrTableName = "";

        Integer mo = Integer.valueOf(accountId) % 4;
        switch (mo) {
            case 0:
                userbaseattrTableName = "xbqp_data1.t_userbaseattr";
                break;

            case 1:
                userbaseattrTableName = "xbqp_data2.t_userbaseattr";
                break;

            case 2:
                userbaseattrTableName = "xbqp_data3.t_userbaseattr";
                break;

            case 3:
                userbaseattrTableName = "xbqp_data4.t_userbaseattr";
                break;
        }

        return userbaseattrTableName;
    }

}
