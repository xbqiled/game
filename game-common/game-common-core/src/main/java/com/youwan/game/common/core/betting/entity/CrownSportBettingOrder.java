package com.youwan.game.common.core.betting.entity;


import lombok.Data;

import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 皇冠体育记录
 */
@Data
public class CrownSportBettingOrder extends ThirdPartyRecord {

    //待确认
    public final static long BETTING_STATUS_UNCONFIRMED = 1L;
    //已确认
    public final static long BETTING_STATUS_CONFIRM = 2L;
    //系统自动取消
    public final static long BETTING_STATUS_SYS_REJECT = 3L;
    //手动取消
    public final static long BETTING_STATUS_REJECT = 4L;

    //未结算
    public final static long BALANCE_UNDO = 1;
    //系统结算
    public final static long BALANCE_DONE = 2;

    //结算失败
    public final static long BALANCE_ERROR = 3;
    //比赛腰斩
    public final static long BALANCE_CUT_GAME = 4;
    //租户 手动结算
    public final static long BALANCE_AGENT_HAND_DONE = 5;
    //比分网结算
    public final static long BALANCE_BFW_DONE = 6;

    //单注
    public final static long MIX_SINGLE = 1;
    //混合过关 主单
    public final static long MIX_MAIN = 2;
    //混合过关 子单
    public final static long MIX_CHILD = 3;

    //确认状态 系统确认
    public final static long HANDLER_TYPE_SYS = 1;
    //确认状态 人工确认
    public final static long HANDLER_TYPE_NORMAL = 2;

    /**
     * 注单锁
     */
//	//未加锁
//	public final static long ORDER_LOCK_UNLOCK = 1L;
//	
//	//系统结算锁
//	public final static long ORDER_LOCK_SYSTEM = 2L;
//	
//	//手动订单锁
//	public final static long ORDER_LOCK_SYSTEM

    /**
     * 1全输 2输一半 3平 4赢一半 5全赢
     */
    public final static long RESULT_STATUS_LOST = 1;
    public final static long RESULT_STATUS_LOST_HALF = 2;
    public final static long RESULT_STATUS_DRAW = 3;
    public final static long RESULT_STATUS_WIN_HALF = 4;
    public final static long RESULT_STATUS_WIN = 5;

    /**
     * 注单编码
     */
    @Id
    private String bettingCode;

    /**
     * sport_type 表
     */
    private Integer sportType;

    /**
     * 1:滚球   2:今日  3:早盘
     */
    private Integer gameTimeType;

    /**
     * 1:独赢 ＆ 让球 ＆ 大小 & 单 / 双
     * 2：波胆  上半场
     * 3：波胆  全场
     * 4：总入球数
     * 5：全场半场输赢
     * 6：混合过关      （参照枚举类DataType）
     */
    private Integer dataType;

    /**
     * 1:全场独赢
     * 2:全场大小球
     * 3:全场让球盘
     * 4：全场得分单双
     * 5：全场波胆
     * 6：半场波胆
     * 7：总入球
     * 8：半场 全场 胜负关系
     * 9：主队全场分数大小
     * 10：客队全场分数大小
     */
    private Integer betType;

    /**
     * 1：主胜
     * 2：主负
     * 3：平
     * 4：总得分单
     * 5：总得分双
     * 6：总得分大于
     * 7：总得分小于
     * 8：让球主队赢
     * 9：让球主队输
     * 10：波胆具体比分
     * 11:波胆其他比分
     * 12：总入球数 具体分数
     * 13：全场 半场胜负关系
     */
    private Integer betItemType;


    private String project;

    /**
     * 滚球时使用，主队分数
     */
    private Long scoreH;

    /**
     * 滚球时使用，客队分数
     */
    private Long scoreC;

    /**
     * 赛事id
     */
    private Long matchId;

    /**
     * 主队
     */
    private String homeTeam;

    /**
     * 客队名称
     */
    private String guestTeam;

    /**
     * 联赛id
     */
    private Long leagueId;

    /**
     * 联赛
     */
    private String league;

    /**
     * H:亚洲盘  I:印尼盘 E:欧洲盘 M:马来西亚盘
     */
    private String plate;

    private String itemKey;

    private String gameKey;

    /**
     * 混合过关的时候  子单
     */
    private Long parentId;

    /**
     * 用于前端显示
     */
    private String remark;

    private Object remark_json;

    /**
     * 赔率
     */
    private BigDecimal odds;

    /**
     * 1 待确认
     * 2：已确认
     * 3：已取消 (滚球系统自动取消)
     * 4: 手动取消
     */
    private Integer bettingStatus;

    /**
     * 1：未结算 2：已结算
     */
    private Integer balance;

    /**
     * 下注时间
     */
    private Date bettingDate;

    /**
     * 投注结果
     */
    private BigDecimal bettingResult;

    /**
     * 结算时间
     */
    private Date accountDatetime;

    /**
     * 皇冠赛事ID
     */
    private Long gid;

    /**
     * 1全输 2输一半 3平 4赢一半 5全赢
     */
    private Integer resultStatus;

    private Long stationId;

    private Long memberId;

    /**
     * 投注金额
     */
    private BigDecimal bettingMoney;

    /**
     * 注单状态说明，取消注单时写入取消原因
     */
    private String statusRemark;

    /**
     * 1:单注  2：混合过关 主单  3：混合过关子单
     */
    private Integer mix;

    private String memberName;

    private String stationName;

    /**
     * 类别名称
     */
    private String typeNames;

    /**
     * 赛果，结算的时候回填  只用于页面显示使用
     */
    private String result;

    /**
     * 数据乐观锁
     */
    private Long dataVersion;

    /**
     * 会员返水状态 （1，还未返水 2，已经返水,还未到账 3，返水已经回滚 ,4 反水已经到账 ）多级表示返点(1，还未返点 2，已经返点 3，返点已经回滚)
     */
    private Integer rollBackStatus;

    /**
     *
     */
    private BigDecimal rollBackMoney;
}
