package com.youwan.game.common.core.pay.json;

import lombok.Data;

import java.io.Serializable;

@Data
public class UrlResultRenRen implements Serializable {

    private String URL;
    private String OrderPrice;

}
