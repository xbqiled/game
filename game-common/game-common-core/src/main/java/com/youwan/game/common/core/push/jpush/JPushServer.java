package com.youwan.game.common.core.push.jpush;

import cn.jiguang.common.ClientConfig;
import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.device.TagAliasResult;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosAlert;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;
import cn.jpush.api.report.MessagesResult;
import cn.jpush.api.report.ReceivedsResult;
import cn.jpush.api.schedule.ScheduleMsgIdsResult;
import cn.jpush.api.schedule.ScheduleResult;
import com.youwan.game.common.core.push.jpush.model.JPushRequest;
import com.youwan.game.common.core.push.jpush.model.PushAliasBean;
import com.youwan.game.common.core.push.jpush.model.RegistrationIdReq;

import javax.validation.constraints.NotNull;
import java.util.List;



/**
 * The global entrance of JPush API library.
 */
public class JPushServer {

    private static ClientConfig clientConfig = ClientConfig.getInstance();
    private static JPushClient jpushClient;
    private static JPushConfig pushConfig;

    public JPushServer(JPushConfig config) {
        this.pushConfig = config;
        jpushClient = new JPushClient(config.getMasterSecret(), config.getAppKey(), null, clientConfig);
    }


    /**
     * 发送带别名的推送
     */
    public static PushResult sendPush2Alias(@NotNull PushAliasBean reqBean) {
        List<String> aliasList = reqBean.getTargetAlias();
        if (aliasList == null || aliasList.isEmpty()) {
            return null;
        }
        String[] aliasArray = reqBean.getTargetAlias().toArray(new String[aliasList.size()]);
        Audience audience = Audience.alias(aliasArray);
        return sendPush(reqBean, audience);
    }

    /**
     * 处理给所有用户的推送
     */
    public static PushResult handleSendPush(JPushRequest reqBean) {
        return sendPush(reqBean, Audience.all());
    }

    /**
     * 真正发送推送
     *
     * @param reqBean  推送内容封装类
     * @param audience 推送范围
     * @return true:推送成功 false:推送失败
     */
    public static PushResult sendPush(@NotNull JPushRequest reqBean, @NotNull Audience audience) {
        try {
            String title = reqBean.getTitle();
            //String content = reqBean.getContent() == null ? "" : reqBean.getContent().length() > 25 ? reqBean.getContent().substring(0, 25) : reqBean.getContent();
            String content = reqBean.getContent() == null ? "" : reqBean.getContent();
            // True 表示推送生产环境，False 表示要推送开发环境；如果不指定则为推送生产环境。但注意，JPush 服务端 SDK 默认设置为推送 “开发环境”。该字段仅对 iOS 的 Notification 有效。
            boolean isApnsProduction = reqBean.isApnsProduction();
            // 推送当前用户不在线时，为该用户保留多长时间的离线消息，以便其上线时再次推送。默认 86400 （1 天），最长 10 天。设置为 0 表示不保留离线消息，只有推送当前在线的用户可以收到。该字段对 iOS 的 Notification 消息无效。
            long timeToAlive = reqBean.getTimeToAlive();
            Platform platform = null;

            if (reqBean.getDevicePlatform().equals("all")) {
                platform = Platform.android_ios();
            } else if (reqBean.getDevicePlatform().equals("ios")) {
                platform = Platform.ios();
            } else if (reqBean.getDevicePlatform().equals("android")) {
                platform = Platform.android();
            } else if (reqBean.getDevicePlatform().equals("winphone")) {
                platform = Platform.winphone();
            }

            PushPayload pushPayload = PushPayload.newBuilder()
                    .setPlatform(platform)
                    .setAudience(audience)
                    .setNotification(Notification.newBuilder().addPlatformNotification(AndroidNotification.newBuilder()
                                    .setTitle(title).setAlert(content).addExtras(reqBean.getExtras()).build())
                                    .addPlatformNotification(IosNotification.newBuilder()
                                    .setAlert(IosAlert.newBuilder().setTitleAndBody(title, null, content)
                                    .build()).incrBadge(1).addExtras(reqBean.getExtras()).build()).build())
                            .setOptions(Options.newBuilder()
                            .setApnsProduction(isApnsProduction)
                            .setTimeToLive(timeToAlive)
                            .build()).build();

            PushResult pushResult = jpushClient.sendPush(pushPayload);
            return pushResult;
            //return pushResult.statusCode == 0 || pushResult.statusCode == 200;
        } catch (APIConnectionException e) {
            e.printStackTrace();
            return null;
        } catch (APIRequestException e) {
            e.printStackTrace();
            return null;
        }
    }


    public static ScheduleResult createSingleSchedule(@NotNull String name, @NotNull String time, @NotNull JPushRequest reqBean, @NotNull Audience audience) {
        try {
            String title = reqBean.getTitle();
            //String content = reqBean.getContent() == null ? "" : reqBean.getContent().length() > 25 ? reqBean.getContent().substring(0, 25) : reqBean.getContent();
            String content = reqBean.getContent() == null ? "" : reqBean.getContent();
            // True 表示推送生产环境，False 表示要推送开发环境；如果不指定则为推送生产环境。但注意，JPush 服务端 SDK 默认设置为推送 “开发环境”。该字段仅对 iOS 的 Notification 有效。
            boolean isApnsProduction = reqBean.isApnsProduction();
            // 推送当前用户不在线时，为该用户保留多长时间的离线消息，以便其上线时再次推送。默认 86400 （1 天），最长 10 天。设置为 0 表示不保留离线消息，只有推送当前在线的用户可以收到。该字段对 iOS 的 Notification 消息无效。
            long timeToAlive = reqBean.getTimeToAlive();
            Platform platform = null;

            if (reqBean.getDevicePlatform().equals("all")) {
                platform = Platform.android_ios();
            } else if (reqBean.getDevicePlatform().equals("ios")) {
                platform = Platform.ios();
            } else if (reqBean.getDevicePlatform().equals("android")) {
                platform = Platform.android();
            } else if (reqBean.getDevicePlatform().equals("winphone")) {
                platform = Platform.winphone();
            }

            PushPayload pushPayload = PushPayload.newBuilder()
                    .setPlatform(platform)
                    .setAudience(audience)
                    .setNotification(Notification.newBuilder().addPlatformNotification(AndroidNotification.newBuilder()
                            .setTitle(title).setAlert(content).addExtras(reqBean.getExtras()).build())
                            .addPlatformNotification(IosNotification.newBuilder()
                                    .setAlert(IosAlert.newBuilder().setTitleAndBody(title, null, content)
                                            .build()).incrBadge(1).addExtras(reqBean.getExtras()).build()).build())
                    .setOptions(Options.newBuilder()
                            .setApnsProduction(isApnsProduction)
                            .setTimeToLive(timeToAlive)
                            .build()).build();

            ScheduleResult pushResult = jpushClient.createSingleSchedule(name, time, pushPayload);
            return pushResult;
        } catch (APIConnectionException e) {
            e.printStackTrace();
            return null;
        } catch (APIRequestException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过RegistrationId获取设备的别名和标签信息
     * 注：测试使用
     *
     * @return
     */
    public static boolean getInfoByRegistrationId(String registrationId) {
        try {
            TagAliasResult result = jpushClient.getDeviceTagAlias(registrationId);
            return true;
        } catch (APIConnectionException e) {
            e.printStackTrace();
            return false;
        } catch (APIRequestException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static MessagesResult getReportMessages (String msgIds) throws APIConnectionException, APIRequestException {
        MessagesResult result =  jpushClient.getReportMessages(msgIds);
        return result;
    }


    public static ReceivedsResult getReportReceiveds (String msgIds) throws APIConnectionException, APIRequestException {
        ReceivedsResult result =  jpushClient.getReportReceiveds(msgIds);
        return result;
    }

    public static ScheduleMsgIdsResult getScheduleMsgIds (String msgIds) throws APIConnectionException, APIRequestException {
        ScheduleMsgIdsResult result =  jpushClient.getScheduleMsgIds(msgIds);
        return result;
    }

}

