package com.youwan.game.common.core.sms;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


/**
 * @author lion
 * @date 2019/1/16
 * 阿里大鱼短息服务配置
 */
@Data
public class SmsConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 配置类型
     */
    @Getter
    @Setter
    private Integer type;

    @Getter
    @Setter
    private String mode;

}
