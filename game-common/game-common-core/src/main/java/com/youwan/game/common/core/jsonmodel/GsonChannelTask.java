package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;

@Data
public class GsonChannelTask implements Serializable {

    private String channel;

    private Integer activityTimeType;

    private Integer activityOpenType;

    private Long beginTime;

    private Long endTime;

    private GsonTask [] taskArr;

}
