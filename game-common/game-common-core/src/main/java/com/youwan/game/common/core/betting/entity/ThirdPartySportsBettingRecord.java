package com.youwan.game.common.core.betting.entity;


import lombok.Data;

import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class ThirdPartySportsBettingRecord extends ThirdPartyRecord {
    // 游戏类型
    public static final int TYPE_M8 = 99;
    public static final int TYPE_M8H = 991;
    public static final int TYPE_BBIN = 2;
    public static final int TYPE_IBC = 10;

    // 注单状态
    public static final int ORDER_STATUS_AUTO_ACCEPT = 1;
    public static final int ORDER_STATUS_ACCEPTED = 2;
    public static final int ORDER_STATUS_REJECT = 3;
    public static final int ORDER_STATUS_CANCEL = 4;

    // 盘口类型
    public static final int ODDS_TYPE_HK = 1;

    // 游戏类型
    public static final int GAME_TYPE_FT = 1;
    public static final int GAME_TYPE_BK = 2;
    public static final int GAME_TYPE_FB = 3;
    public static final int GAME_TYPE_IH = 4;
    public static final int GAME_TYPE_BS = 5;
    public static final int GAME_TYPE_TN = 6;
    public static final int GAME_TYPE_F1 = 7;
    public static final int GAME_TYPE_SP = 8;
    public static final int GAME_TYPE_CB = 9;

    // 玩法类型
    public static final int PLAY_TYPE_HDP = 1;
    public static final int PLAY_TYPE_1X2 = 2;
    public static final int PLAY_TYPE_OU = 3;
    public static final int PLAY_TYPE_OE = 4;
    public static final int PLAY_TYPE_CS = 5;
    public static final int PLAY_TYPE_TG = 6;
    public static final int PLAY_TYPE_FLG = 7;
    public static final int PLAY_TYPE_HFT = 8;
    public static final int PLAY_TYPE_PAR = 9;
    public static final int PLAY_TYPE_ORT = 10;
    public static final int PLAY_TYPE_DC = 11;
    public static final int PLAY_TYPE_PAS = 12;

    // 全半场
    public static final int HALF_FULL = 1;
    public static final int HALF_HALF = 2;

    /**
     * 注单状态 1:未结算,2:全赢,3:全输,4:赢一半,5:输一半,6:和局,10 取消注单 ,已结算(除了未结算之外的记录)
     */
    public static final int RESULT_NOT_MATCH_OVER = 1;
    public static final int RESULT_WIN_ALL = 2;
    public static final int RESULT_LOST_ALL = 3;
    public static final int RESULT_WIN_HALF = 4;
    public static final int RESULT_LOST_HALF = 5;
    public static final int RESULT_DRAW = 6;
    public static final int RESULT_OVER = 9;
    public static final int RESULT_REJECT = 10;

    /**
     * 订单ID
     */
    @Id
    private String orderId;
    /**
     * 下注玩法code
     */
    private String bettingCode;
    /**
     * 游戏类型，10=M8
     */
    private Integer type;
    /**
     * 第三方平台类型
     */
    private String platformType;
    /**
     * 用户id
     */
    private Long accountId;
    /**
     * 用户账号
     */
    private String account;

    /**
     * 站点ID
     */
    private Long stationId;
    /**
     * 集群ID
     */

    private Long serverId;
    /**
     * 第三方账号id
     */

    private Long thirdMemberId;
    /**
     * 第三方账号名
     */

    private String thirdMemberAccount;
    /**
     * 投注前金额
     */

    private BigDecimal beforeMoney;
    /**
     * 下注金额
     */

    private BigDecimal bettingMoney;
    /**
     * 实际下注金额
     */
    private BigDecimal realBettingMoney;
    /**
     * 下注时间
     */
    private Date bettingTime;
    /**
     * 下注时间（美东）
     */
    private Date bettingTimeGmt4;
    /**
     * 盈利金额
     */
    private BigDecimal winMoney;
    /**
     * 赔率
     */
    private BigDecimal odds;
    /**
     * 注单创建时间
     */
    private Date createDatetime;
    /**
     * 币种
     */
    private String currency;
    /**
     * 登录IP
     */
    private String loginIp;
    /**
     * 上级账号
     */
    private String parents;
    /**
     * 反水金额
     */
    private BigDecimal rakebackMoney;
    /**
     * 反水赔率
     */
    private BigDecimal rakebackRate;
    /**
     * 反水备注
     */
    private String rakebackDesc;
    /**
     * 打码量
     */
    private BigDecimal rakebackDrawNeed;

    private String stationName;

    // 主队
    private String homeTeam;
    // 客队
    private String awayTeam;
    // 联赛
    private String league;

    // 游戏类型
    private Integer gameType;

    // 游戏名称
    private String gameName;

    // 玩法
    private Integer playType;

    // 盘口
    private Integer oddsType;

    // 注单状态
    private Integer orderStatus;

    // 注意输赢状态
    private Integer resStatus;

    // 比分
    private String score;

    // 最终比分
    private String resScore;

    private Date lastModifyTime;

    // 开赛时间
    private Date matchTime;

    // 内容
    private String info;

    // 抓取记录ID
    private Long fetchId;

    // 比赛ID
    private Long matchId;

    // playName
    private String playName;

    // parlayData
    private String parlayData;

    private Object parlay_json;
}
