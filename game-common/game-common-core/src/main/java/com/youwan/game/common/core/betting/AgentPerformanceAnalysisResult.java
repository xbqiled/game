package com.youwan.game.common.core.betting;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author terry.jiang[taoj555@163.com] on 2020-10-25.
 */
@Data
public class AgentPerformanceAnalysisResult {

    /**
     * 用户id
     */
    private Long uid;

    /**
     *
     */
    private String epoch;

    /**
     * 是否领取
     */
    private Boolean took = false;

    /**
     * 总业绩
     */
    private BigDecimal totalPerformance;

    /**
     * 直属业绩
     */
    private BigDecimal directlyPerformance;

    /**
     * 下属业绩
     */
    private BigDecimal indirectlyPerformance;

    /**
     * 计算结果
     */
    private BigDecimal result;

    /**
     * 领取时间
     */
    private Date takingTime;
}
