package com.youwan.game.common.core.sms;


import com.youwan.game.common.core.util.Assert;
import com.youwan.game.common.core.util.CCPRestSDK;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;


@Slf4j
public class SmsRongLianMessageService extends AbstractMessageService {

    @Override
    public void execute(MobileMsgTemplate mobileMsgTemplate, SmsConfig smsConfig) {
        super.execute(mobileMsgTemplate, smsConfig);
    }

    @Override
    public void check(MobileMsgTemplate mobileMsgTemplate) {
        Assert.isBlank(mobileMsgTemplate.getMobile(), "手机号不能为空");
        Assert.isBlank(mobileMsgTemplate.getTemplate(), "短信模板不能为空");
    }

    /**
     * <B>发送短信</B>
     * @param mobileMsgTemplate 信息
     * @param config
     * @return
     */
    @Override
    public boolean process(MobileMsgTemplate mobileMsgTemplate, SmsConfig config) {
        HashMap<String, Object> result = null;
        CCPRestSDK restAPI = new CCPRestSDK();

        restAPI.init(((RongLianConfig)config).getRongLianServerIp(), ((RongLianConfig)config).getRongLianServerPort());// 初始化服务器地址和端口，格式如下，服务器地址不需要写https://
        restAPI.setAccount(((RongLianConfig)config).getRongLianAccount(), ((RongLianConfig)config).getRongLianToken());// 初始化主帐号和主帐号TOKEN
        restAPI.setAppId(((RongLianConfig)config).getRongLianAppId());// 初始化应用ID

        result = restAPI.sendTemplateSMS(mobileMsgTemplate.getMobile(), mobileMsgTemplate.getTemplate(), mobileMsgTemplate.getParameter());//模板Id，不带此参数查询全部可用模板
        if("000000".equals(result.get("statusCode"))){
            return Boolean.TRUE;
        }else{
            return Boolean.FALSE;
        }
    }

    @Override
    public void fail(MobileMsgTemplate mobileMsgTemplate) {
        log.error("短信发送失败 -> 网关：{} -> 手机号：{}", mobileMsgTemplate.getType(), mobileMsgTemplate.getMobile());
    }
}
