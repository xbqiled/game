package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;



@Data
public class RequestModifyAccountPw implements Serializable {

    private int accountID;
    private String oldPassword;
    private String newPassword;
    private boolean check;

}
