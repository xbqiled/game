package com.youwan.game.common.core.sms;


/**
 * @author lion
 * @date 2019/1/16
 * 抽象
 */
public abstract class AbstractMessageService implements SmsMessageService {

    /**
     * 执行入口
     *
     * @param mobileMsgTemplate 信息
     */
    @Override
    public void execute(MobileMsgTemplate mobileMsgTemplate, SmsConfig smsConfig) {
        check(mobileMsgTemplate);
        if (!process(mobileMsgTemplate, smsConfig)) {
            fail(mobileMsgTemplate);
        }
    }

    /**
     * 数据校验
     *
     * @param mobileMsgTemplate 信息
     */
    @Override
    public abstract void check(MobileMsgTemplate mobileMsgTemplate);

    /**
     * 业务处理
     *
     * @param mobileMsgTemplate 信息
     * @return boolean
     */
    @Override
    public abstract boolean process(MobileMsgTemplate mobileMsgTemplate, SmsConfig config);

    /**
     * 失败处理
     *
     * @param mobileMsgTemplate 信息
     */
    @Override
    public abstract void fail(MobileMsgTemplate mobileMsgTemplate);
}

