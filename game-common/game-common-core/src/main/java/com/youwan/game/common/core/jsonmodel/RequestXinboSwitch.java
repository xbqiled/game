package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;
import java.util.List;


@Data
public class RequestXinboSwitch implements Serializable {

    private String channel;

    private List<GsonSwitch> detail;

}
