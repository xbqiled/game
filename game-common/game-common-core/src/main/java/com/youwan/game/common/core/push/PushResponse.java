package com.youwan.game.common.core.push;


import lombok.Data;

@Data
public class PushResponse {
    private String code;
    private String msg;
}
