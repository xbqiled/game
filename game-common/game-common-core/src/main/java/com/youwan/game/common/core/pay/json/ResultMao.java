package com.youwan.game.common.core.pay.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class ResultMao  implements Serializable {


    private Integer code;

    private String msg;

    private String status;

    private Data data;

    @lombok.Data
    public class Data implements Serializable {
        private String payUrl;
    }


}
