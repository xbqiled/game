package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class RequestSign implements Serializable {

    private String channel;

    private Long beginTime;

    private Long endTime;

    private Integer openType;

    private Integer isCirculate;

    private GsonSignAwardCfg[] awardArr;
}
