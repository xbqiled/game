package com.youwan.game.common.core.otherplatform.bg.json.resp;


import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class UserRedPacketResp implements Serializable {


    public String id;
    public RedPacketData result;

    @Data
    public static class RedPacketData {
        public Long total;
        public Long pageIndex;
        public Long pageSize;

        public String etag;
        public List<UserRedPacketResp.RedPacketData.RedPacket> items;
        public AgentQueryResp.OrderData.BetStats stats;

        @Data
        public static class RedPacket {
            public long pid;
            public String sn;

            public long uid;
            public String account;
            public float balance;

            public float amount;
            public String time;
        }
    }
}

