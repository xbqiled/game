package com.youwan.game.common.core.push.jpush.model;


import lombok.Data;

@Data
public class RegistrationIdReq {

    private String registrationId;

}
