package com.youwan.game.common.core.pay.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class ResultJinZun implements Serializable {

    private String businessId;
    private String amount;
    private String outTradeNo;
    private String tradeNo;
    private String orderState;
}
