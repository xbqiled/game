package com.youwan.game.common.core.pay;

import cn.hutool.core.util.URLUtil;
import com.google.gson.Gson;
import com.youwan.game.common.core.util.AesUtil;
import com.youwan.game.common.core.util.SignUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;
import static com.youwan.game.common.core.constant.UrlConstant.FEONT_SYSTM_URL;


public class ShanRuBaoUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/shanRuBao";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success";
    //下单接口
    private static String ORDER_URL = "";


    public static Map<String, String> packaData(String... params) {
        String memberNumber = params[0]; //商户号
        String memberOrderNumber = params[1]; //商户订单号
        String tradeAmount = params[2]; //交易金额
        String defrayalType = params[3]; //支付方式
        String goodsName = params[5];  //商品名称
        String miyao = params[6];  //商户秘钥
        String userId = params[8];  //用户id
        ORDER_URL = params[4];
        Map<String,String> map = new HashMap<>();
        map.put("uid", memberNumber);
        map.put("istype", defrayalType);
        map.put("price", tradeAmount);
        map.put("orderid", memberOrderNumber);
        map.put("orderuid", userId);
        map.put("return_url", FEONT_SYSTM_URL + CALL_BACK_URL + memberOrderNumber);
        map.put("notify_url", API_SYSTM_URL + NOTIFY_URL);
        map.put("goodsname", goodsName);
        map.put("version", "2");
        map.put("token", miyao);
        TreeMap<String, String> treeMap = new TreeMap<>(map);
        map.put("key", SignUtil.encryption(SignUtil.mapToUrlString(treeMap)).toLowerCase());
        return map;
    }

}
