package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;

@Data
public class GsonSuccour implements Serializable {

    private String channel;
    private Integer openType;
    private Long coinLimit;

    private Long succourAward;
    private Integer totalSuccourCnt;
    private Integer shareConditionCnt;
}