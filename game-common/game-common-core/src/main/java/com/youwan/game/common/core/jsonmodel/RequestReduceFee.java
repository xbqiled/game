package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class RequestReduceFee implements Serializable {

    private Integer accountID;
    private BigDecimal amount;
    private Integer force;

    public Integer getAccountID() {
        return accountID;
    }

    public void setAccountID(Integer accountID) {
        this.accountID = accountID;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getForce() {
        return force;
    }

    public void setForce(Integer force) {
        this.force = force;
    }
}
