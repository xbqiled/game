package com.youwan.game.common.core.push.jpush.model;

import lombok.Data;

import java.util.List;


@Data
public class PushAliasBean extends JPushRequest {

    List<String> targetAlias;

}
