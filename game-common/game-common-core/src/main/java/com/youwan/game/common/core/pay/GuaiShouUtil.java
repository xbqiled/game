package com.youwan.game.common.core.pay;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONUtil;
import com.google.gson.Gson;
import com.youwan.game.common.core.pay.json.ResultGuaiShou;
import com.youwan.game.common.core.pay.json.ResultShunDa;
import com.youwan.game.common.core.util.RSACoder;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;

/**
 * <B>怪兽支付</B>
 */
public class GuaiShouUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/guaiShou";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success";
    //下单接口
    private static String ORDER_URL = "";

    public static Map<String, String> packaData(String... params) {
        String memberNumber = params[0]; //商户号

        String memberOrderNumber = params[1]; //商户订单号
        String tradeAmount = params[2]; //交易金额
        String defrayalType = params[3]; //支付方式

        String payGateway = params[4]; //支付网关
        String miyao = params[6];  //商户秘钥
        ORDER_URL = payGateway;

        String money = new BigDecimal(tradeAmount).setScale(2).toString();
        String notifyUrl = API_SYSTM_URL + NOTIFY_URL;
        String returnUrl = API_SYSTM_URL + CALL_BACK_URL;

        StringBuffer signSrc= new StringBuffer();
        if (!"".equals(memberNumber)) {
            signSrc.append("merchant_code=").append(memberNumber).append("&");
        }
        if (!"".equals(defrayalType)) {
            signSrc.append("method=").append(defrayalType).append("&");
        }
        if (!"".equals(money)) {
            signSrc.append("money=").append(money).append("&");
        }
        if (!"".equals(notifyUrl)) {
            signSrc.append("notify_url=").append(notifyUrl).append("&");
        }
        if (!"".equals(memberOrderNumber)) {
            signSrc.append("order_sn=").append(memberOrderNumber).append("&");
        }
        if (!"".equals(returnUrl)) {
            signSrc.append("return_url=").append(returnUrl);
        }

        String signInfo = signSrc.toString();
        String sign="";
        byte[] privateKey= org.apache.commons.codec.binary.Base64.decodeBase64(miyao);
        byte[] rsaSign= new byte[0];
        try {
            rsaSign = RSACoder.sign(signInfo.getBytes(), privateKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        sign = org.apache.commons.codec.binary.Base64.encodeBase64String(rsaSign);
        Map<String, String> postParamMap = new HashMap();
        postParamMap.put("merchant_code", memberNumber);
        postParamMap.put("method",  defrayalType);

        postParamMap.put("money",  money);
        postParamMap.put("order_sn",  memberOrderNumber);
        postParamMap.put("notify_url",  notifyUrl);

        postParamMap.put("return_url",  returnUrl);
        postParamMap.put("sign",  sign);

        return postParamMap;
    }


    public static ResultGuaiShou doPay(Map map) {
        String resultStr = HttpRequest.post(ORDER_URL).form(map).timeout(10000).execute().body();
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  ResultGuaiShou.class);
        }
        return  null;
    }
}
