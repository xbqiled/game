package com.youwan.game.common.core.util;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.youwan.game.common.core.constant.EnumConstant;
import com.youwan.game.common.core.constant.UrlConstant;
import com.youwan.game.common.core.jsonmodel.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.youwan.game.common.core.constant.UrlConstant.*;

/**
 * <B>请求服务器接口信息</B>
 */
@Slf4j
@Component
public class HttpHandler {


    public static GsonResult wxFjhs(List<GsonWxFjhs> list) {
        String body = new Gson().toJson(list);

        log.debug("配置五星宏辉请求:" + GAME_API_URL + FJHSJS_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + FJHSJS_URL,  body);
        log.debug("配置五星宏辉返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    public static GsonResult wxDuorgl(Integer killInning) {
        GsonWxDuorgl gsonWxDuorgl = new GsonWxDuorgl();
        gsonWxDuorgl.setKillInning(killInning);

        String body = new Gson().toJson(gsonWxDuorgl);

        log.debug("配置五星宏辉请求:" + GAME_API_URL + DURGL_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + DURGL_URL,  body);
        log.debug("配置五星宏辉返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    public static GsonResult wxDuorjs(List<GsonWxDuorjs> list) {
        String body = new Gson().toJson(list);

        log.debug("配置五星宏辉请求:" + GAME_API_URL + DURRSJS_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + DURRSJS_URL,  body);
        log.debug("配置五星宏辉返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    public static GsonResult wxDuoky(List<GsonWxDuorky> list) {
        String body = new Gson().toJson(list);

        log.debug("配置五星宏辉请求:" + GAME_API_URL + DURJJCZGKY_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + DURJJCZGKY_URL,  body);
        log.debug("配置五星宏辉返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    public static GsonResult wxDuoks(List<GsonWxDuorks> list) {
        String body = new Gson().toJson(list);

        log.debug("配置五星宏辉请求:" + GAME_API_URL + DURJJCZGKS_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + DURJJCZGKS_URL,  body);
        log.debug("配置五星宏辉返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    public static GsonResult wxDanrky(List<GsonWxDanrky> list) {
        String body = new Gson().toJson(list);

        log.debug("配置五星宏辉请求:" + GAME_API_URL + DARJJCZGKY_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + DARJJCZGKY_URL,  body);
        log.debug("配置五星宏辉返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    public static GsonResult wxDanrks(List<GsonWxDanrks> list) {
        String body = new Gson().toJson(list);

        log.debug("配置五星宏辉请求:" + GAME_API_URL + DARJJCZGKS_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + DARJJCZGKS_URL,  body);
        log.debug("配置五星宏辉返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    public static GsonResult wxDanrcq(List<GsonWxDanrcq> list) {
        String body = new Gson().toJson(list);

        log.debug("配置五星宏辉请求:" + GAME_API_URL + DRCQJZ_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + DRCQJZ_URL,  body);
        log.debug("配置五星宏辉返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    public static GsonResult wxDanrbh(List<GsonWxDanrbh> list) {
        String body = new Gson().toJson(list);

        log.debug("配置五星宏辉请求:" + GAME_API_URL + DRBHJZ_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + DRBHJZ_URL,  body);
        log.debug("配置五星宏辉返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    public static GsonResult gameCtrlCfg(Integer sixInterval, Integer eightInterval, Integer twelveInterval, Integer globalInterval, Integer killAllInterval) {
        RequestCtrlCfg cfg = new RequestCtrlCfg();
        cfg.setSixInterval(sixInterval);

        cfg.setEightInterval(eightInterval);
        cfg.setTwelveInterval(twelveInterval);

        cfg.setGlobalInterval(globalInterval);
        cfg.setKillAllInterval(killAllInterval);
        String body = new Gson().toJson(cfg);

        log.debug("配置飞禽走兽请求:" + GAME_API_URL + BIRDCTRL_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + BIRDCTRL_URL,  body);
        log.debug("配置飞禽走兽返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    public static GsonResult cfgTask(String channelId, Integer openType, Integer activityTimeType, Long beginTime, Long endTime, GsonTask[] taskArr) {
        GsonChannelTask task = new GsonChannelTask();
        task.setChannel(channelId);
        task.setActivityOpenType(openType);

        task.setActivityTimeType(activityTimeType);
        if (activityTimeType == 1) {
            task.setBeginTime(beginTime);
            task.setEndTime(endTime);
        }

        if (taskArr != null && taskArr.length > 0) {
            task.setTaskArr(taskArr);
        }

        String body = new Gson().toJson(task);
        log.debug("配置任务请求:" + GAME_API_URL + TASK_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + TASK_URL,  body);
        log.debug("配置任务返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    public static GsonResult cfgSuccour(String channelId, Integer openType, Long coinLimit, Long succourAward, Integer totalSuccourCnt, Integer shareConditionCnt) {
        GsonSuccour succour = new GsonSuccour();
        succour.setChannel(channelId);
        succour.setOpenType(openType);

        succour.setCoinLimit(coinLimit);
        succour.setSuccourAward(succourAward);
        succour.setTotalSuccourCnt(totalSuccourCnt);

        succour.setShareConditionCnt(shareConditionCnt);
        String body = new Gson().toJson(succour);
        log.debug("配置救济金请求:" + GAME_API_URL + SUCCOUR_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + SUCCOUR_URL,  body);
        log.debug("配置救济金返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }

    public static GsonResult cfgXinboSw(List<GsonSwitch> cfgList, String channelId) {
        RequestXinboSwitch requestXinbo = new RequestXinboSwitch();
        requestXinbo.setChannel(channelId);

        requestXinbo.setDetail(cfgList);
        List<RequestXinboSwitch> list = new ArrayList<>();
        list.add(requestXinbo);
        String body = new Gson().toJson(list);

        log.debug("接口开关请求:" + GAME_API_URL + XINBO_SWITCH_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + XINBO_SWITCH_URL, body);
        log.debug("接口开关返回:" + GAME_API_URL + XINBO_SWITCH_URL + "Json:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }

    /**
     * <B>配置昵稱獎勵</B>
     * @return
     */
    public static GsonResult configNickAward(Integer openType, String channel, Integer award) {
        RequestNick requestNick  = new RequestNick();
        requestNick.setAward(award);
        requestNick.setChannel(channel);
        requestNick.setOpenType(openType);

        String body = new Gson().toJson(requestNick);
        log.debug("配置昵称奖励请求:" + GAME_API_URL + EDIT_NICK_AWARD_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + EDIT_NICK_AWARD_URL,  body);
        log.debug("配置昵称奖励返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }

    public static GsonResult cfgBindUrl(String url, Integer opType, String channel, Long accountId) {
        RequestBindUrl  requestBindUrl  = new RequestBindUrl();
        requestBindUrl.setAccountID(accountId);
        requestBindUrl.setChannel(channel);
        requestBindUrl.setOpType(opType);
        requestBindUrl.setUrl(url);

        String body = new Gson().toJson(requestBindUrl);
        log.debug("配置推广绑定域名请求:" + GAME_API_URL + BIND_PROMOTER_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + BIND_PROMOTER_URL,  body);
        log.debug("配置推广绑定域名返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }

    public static GsonResult cfgDayShare(Long channelId, Integer opType, Long dayAward, Long awardDayShareCnt, List<GsonDayShare> dayShareList) {
        RequestDayShare requestDayShare = new RequestDayShare();
        requestDayShare.setDayAward(dayAward);
        requestDayShare.setAwardDayShareCnt(awardDayShareCnt);

        requestDayShare.setChannel(channelId);
        requestDayShare.setOpenType(opType);
        if (CollUtil.isNotEmpty(dayShareList) && dayShareList.size() > 0) {

            GsonDayShare [] dayShareArr = new GsonDayShare [dayShareList.size()];
            dayShareList.toArray(dayShareArr);
            requestDayShare.setTotalDayArr(dayShareArr);
        }

        String body = new Gson().toJson(requestDayShare);
        log.debug("配置每日分享请求:" + GAME_API_URL + DAY_SHARE_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + DAY_SHARE_URL,  body);
        log.debug("配置每日分享返回:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }

    /**
     * <B>更新打码量信息</B>
     * @param outerType
     * @param accountId
     * @return
     */
    public static GsonResult opGameDamaMulti(Integer outerType, Integer  accountId, Integer damaMulti ) {
        Integer [] accountIdArr = {accountId};
        Integer [] damaMultiArr =  {damaMulti};

        RequestGameDamaMultiOp gameDamaMulti = new RequestGameDamaMultiOp();
        gameDamaMulti.setAccountId(accountIdArr);
        gameDamaMulti.setDamaMulti(damaMultiArr);

        gameDamaMulti.setOuterType(outerType);
        String body = new Gson().toJson(gameDamaMulti);
        log.debug("获取用户余额请求:" + GAME_API_URL + BATCH_UPDATE_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + BATCH_UPDATE_URL, body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }

    /**
     * <B>获取用户余额信息</B>
     * @param outerType
     * @param accountId
     * @return
     */
    public static GsonUserBalance getUserBalance(Integer outerType, Integer accountId) {
        RequstUserBalance userBalanceRequst  = new RequstUserBalance();
        userBalanceRequst.setAccountId(accountId);
        userBalanceRequst.setOuterType(outerType);

        String body = new Gson().toJson(userBalanceRequst);
        log.debug("获取用户余额请求:" + GAME_API_URL + USER_BALANCE_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + USER_BALANCE_URL, body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonUserBalance.class);
        }
        return null;
    }

    /**
     * <B>用户游戏转换</B>
     * @param orderId
     * @param outerType
     * @param coinOpType
     * @param coinChange
     * @param accountId
     * @return
     */
    public static GsonOutGameOp opGameCoin(String orderId, Integer reason, Integer outerType, Integer coinOpType, Integer coinChange, Integer accountId) {
        RequestOutGameOp requestOutGameOp = new RequestOutGameOp();
        requestOutGameOp.setAccountId(accountId);
        requestOutGameOp.setOrderId(orderId);

        requestOutGameOp.setCoinOpNum(coinChange);
        requestOutGameOp.setCoinOpType(coinOpType == 1 ? 2: 1);
        requestOutGameOp.setOuterType(outerType);

        requestOutGameOp.setReason(reason);
        String body = new Gson().toJson(requestOutGameOp);
        log.info("用户余额转换请求:" + GAME_API_URL + OUTERGAMEOPGOIN_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + OUTERGAMEOPGOIN_URL, body);
        log.info("用户余额转换回包:" + GAME_API_URL + OUTERGAMEOPGOIN_URL + "Json:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonOutGameOp.class);
        }
        return null;
    }

    /**
     * <B>取消游戏转换信息</B>
     * @param orderId
     * @param outerType
     * @param coinOpType
     * @param coinChange
     * @param accountId
     * @return
     */
    public static GsonCancelOutGameOp cancelOpGameCoin(String orderId, Integer outerType, Integer coinOpType, Integer coinChange, Integer accountId) {
        RequestCancelOutGameOp requestCancelOutGameOp = new RequestCancelOutGameOp();
        requestCancelOutGameOp.setAccountId(accountId);
        requestCancelOutGameOp.setOrderId(orderId);

        requestCancelOutGameOp.setBeforeCoinOpNum(coinChange);
        requestCancelOutGameOp.setBeforeCoinOpType(coinOpType == 1 ? 2 : 1);
        requestCancelOutGameOp.setOuterType(outerType);

        String body = new Gson().toJson(requestCancelOutGameOp);
        log.debug("扣款取消请求:" + GAME_API_URL + CANCELOUTERGAMEOPGOIN_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + CANCELOUTERGAMEOPGOIN_URL, body);
        log.debug("扣款取消回包:" + GAME_API_URL + CANCELOUTERGAMEOPGOIN_URL + "Json:" + resultStr);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonCancelOutGameOp.class);
        }
        return null;
    }


    /**
     * <B>扣款</B>
     * @return
     */
    public static GsonReduceResult reduceFee(Integer accountID, BigDecimal amount, Integer force) {
        RequestReduceFee reduceFee = new RequestReduceFee();
        reduceFee.setAccountID(accountID);

        reduceFee.setAmount(amount);
        reduceFee.setForce(force);
        String body = new Gson().toJson(reduceFee);

        log.debug("扣款请求:" + GAME_API_URL + DIRECTSUB_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + DIRECTSUB_URL, body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonReduceResult.class);
        }
        return null;
    }


    /**
     * <B>设置代理累计充值返利</B>
     * @param channel
     * @param openType
     * @param timeType
     * @param beginTime
     * @param endTime
     * @param firstTotalDeposit
     * @param selfReward
     * @param upperReward
     * @return
     */
    public static GsonResult inviteDeposit(String channel, Long openType, Long timeType, Long beginTime, Long endTime, Long firstTotalDeposit, Long selfReward, Long upperReward) {
        RequestInviteDeposit requestInviteDeposit = new RequestInviteDeposit();
        requestInviteDeposit.setChannel(channel);
        requestInviteDeposit.setOpenType(openType);
        requestInviteDeposit.setTimeType(timeType);

        requestInviteDeposit.setBeginTime(beginTime);
        requestInviteDeposit.setEndTime(endTime);
        requestInviteDeposit.setFirstTotalDeposit(firstTotalDeposit);

        requestInviteDeposit.setSelfReward(selfReward);
        requestInviteDeposit.setUpperReward(upperReward);
        String body = new Gson().toJson(requestInviteDeposit);

        log.debug("配置充值返利请求:" + GAME_API_URL + INVITE_DEPOSIT_CHANNEL_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + INVITE_DEPOSIT_CHANNEL_URL,  body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }

    /**
     * <B>充值赠送配置</B>
     * @param channel
     * @param beginTime
     * @param endTime
     * @param openType
     * @param cfgList
     * @return
     */
    public static GsonResult rechargeRebate(String channel, Long beginTime, Long endTime, Integer openType, List<GsonRechargeRebateCfg> cfgList) {
        RequestRechargeRebate requestRechargeRebate = new RequestRechargeRebate();
        requestRechargeRebate.setChannel(channel);
        requestRechargeRebate.setBeginTime(beginTime);

        requestRechargeRebate.setEndTime(endTime);
        requestRechargeRebate.setOpenType(openType);

        if (CollUtil.isNotEmpty(cfgList) && cfgList.size() > 0) {
            GsonRechargeRebateCfg [] rechargeRabateArr = new GsonRechargeRebateCfg [cfgList.size()];
            cfgList.toArray(rechargeRabateArr);
            requestRechargeRebate.setCfgInfo(rechargeRabateArr);
        }

        String body = new Gson().toJson(requestRechargeRebate);

        log.debug("配置赠送配置请求:" + GAME_API_URL + DEPOSIT_CHANNEL_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + DEPOSIT_CHANNEL_URL,  body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }

    /**
     * <B>签到配置</B>
     * @param channel
     * @param beginTime
     * @param endTime
     * @param openType
     * @param cfgList
     * @return
     */
    public static GsonResult signConfig(String channel, Long beginTime, Long endTime, Integer openType, Integer isCirculate,  List< GsonSignAwardCfg > cfgList) {
        RequestSign requestSign = new RequestSign();
        requestSign.setChannel(channel);
        requestSign.setBeginTime(beginTime);

        requestSign.setEndTime(endTime);
        requestSign.setOpenType(openType);
        requestSign.setIsCirculate(isCirculate);

        if (CollUtil.isNotEmpty(cfgList) && cfgList.size() > 0) {
            GsonSignAwardCfg [] signAwardArr = new GsonSignAwardCfg [cfgList.size()];
            cfgList.toArray(signAwardArr);
            requestSign.setAwardArr(signAwardArr);
        }

        String body = new Gson().toJson(requestSign);

        log.debug("配置签到请求:" + GAME_API_URL + SIGN_CHANNEL_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + SIGN_CHANNEL_URL,  body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B>获取当前代理的信息</B>
     * @param accountID
     * @return
     */
    public static GsonAgent getAgentInfo(Integer accountID) {
        Map<String ,Object> paramMap = new HashMap<>();
        paramMap.put("accountID", accountID);

        String body = new Gson().toJson(paramMap);
        log.debug("获取当前代理的信息请求:" + GAME_API_URL + SIGN_CHANNEL_URL + "Json:" + body);
        String resultStr =  HttpUtil.post(GAME_API_URL + PROMOTE_INFO_URL,  body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonAgent.class);
        }
        return null;
    }


    /**
     * <B>幸运转盘</B>
     * @param channelId
     * @param diamondcfg
     * @param goldcfg
     * @param silvercfg
     * @param start
     * @param finish
     * @param diamondCost
     * @param goldCost
     * @param silverCost
     * @return
     */
    public static GsonResult luckyRoulette(Long channelId, List<GsonLuckRoulette> diamondcfg, List<GsonLuckRoulette> goldcfg, List<GsonLuckRoulette> silvercfg,
                                           Long start, Long finish, Integer diamondCost, Integer goldCost, Integer silverCost) {
        RequestLuckyRoulette  luckyRoulette = new RequestLuckyRoulette();
        luckyRoulette.setOpenType(1);

        luckyRoulette.setChannel(channelId);
        luckyRoulette.setStart(start);
        luckyRoulette.setFinish(finish);

        luckyRoulette.setDiamondCost(diamondCost);
        luckyRoulette.setGoldCost(goldCost);
        luckyRoulette.setSilverCost(silverCost);

        if (CollUtil.isNotEmpty(diamondcfg) && diamondcfg.size() > 0) {
            GsonLuckRoulette [] diamondArr = new GsonLuckRoulette [diamondcfg.size()];
            diamondcfg.toArray(diamondArr);
            luckyRoulette.setDiamond(diamondArr);
        }

        if (CollUtil.isNotEmpty(goldcfg) && goldcfg.size() > 0) {
            GsonLuckRoulette [] goldArr = new GsonLuckRoulette [goldcfg.size()];
            goldcfg.toArray(goldArr);
            luckyRoulette.setGold(goldArr);
        }

        if (CollUtil.isNotEmpty(silvercfg) && silvercfg.size() > 0) {
            GsonLuckRoulette [] silverArr = new GsonLuckRoulette [silvercfg.size()];
            silvercfg.toArray(silverArr);
            luckyRoulette.setSilver(silverArr);
        }

        String body = new Gson().toJson(luckyRoulette);

        log.debug("幸运转盘配置请求:" + GAME_API_URL + LUCKY_ROULETTE_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + LUCKY_ROULETTE_URL, body);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }



    /******************************************************生成短链接********************************************************/

    /**
     * <B>生成短网址信息</B>
     *
     * @param url
     * @param urlPara
     * @param head
     * @param body
     * @return
     */
    public static String createShortUrl(String requestUrl, String url, String urlPara, String head, String body, Integer requestType, Integer replacePostion) {
        //URL MAP
        Map<String, String> urlMap = new HashMap<>();
        //HEAD MAP
        Map<String, String> headMap = new HashMap<>();
        //请求结果
        String resultStr = "";
        String reqUrl = "";

        String paraStr = "";
        String headStr = "";
        String bodyStr = "";

        //替换位置
        if (replacePostion.intValue() == EnumConstant.REPLACE_POSITION.REPLACE_URL.getValue()) {
            paraStr = StrUtil.format(urlPara, url);
        } else if (replacePostion.intValue() == EnumConstant.REPLACE_POSITION.REPLACE_HEAD.getValue()) {
            headStr = StrUtil.format(head, url);
        } else if (replacePostion.intValue() == EnumConstant.REPLACE_POSITION.REPLACE_BODY.getValue()) {
            bodyStr = StrUtil.format(body, url);
        }

        //判断URL_PARA是否为空
        if (StrUtil.isNotBlank(paraStr)) {
            //转换成MAP
            urlMap = new Gson().fromJson(paraStr, Map.class);
        }

        //判断head 是否为空
        if (StrUtil.isNotBlank(head)) {
            //转换成MAP
            headMap = new Gson().fromJson(head, Map.class);
        }

        HttpRequest request = new HttpRequest(requestUrl);
        reqUrl = requestUrl;

        //组装URL
        if (CollUtil.isNotEmpty(urlMap)) {
            StringBuffer sb = new StringBuffer();
            sb.append(requestUrl);
            int i = 0;

            for (Map.Entry<String, String> entry : urlMap.entrySet()) {
                if (i > 0) {
                    sb.append("&");
                }
                sb.append(entry.getKey());

                sb.append("=");
                sb.append(entry.getValue());
                i ++;
            }
            reqUrl = sb.toString();
        }

        //判断请求方式
        if (requestType.intValue() == EnumConstant.REQUEST_TYPE.POST_REQUEST.getValue()) {
            resultStr = request.post(reqUrl).addHeaders(headMap).timeout(HttpRequest.TIMEOUT_DEFAULT).body(bodyStr).execute().body();
        } else {
            resultStr = request.get(reqUrl).addHeaders(headMap).timeout(HttpRequest.TIMEOUT_DEFAULT).body(bodyStr).execute().body();
        }

        Map<String , Object> resultMap = new HashMap();
        //解析返回数据,判断是否有URL地址
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            resultMap = new Gson().fromJson(resultStr, Map.class);
        }

        String urlStr = "";
        //遍历返回结果如果有URL地址便是短链接
        if (CollUtil.isNotEmpty(resultMap)) {
            for (Map.Entry<String, Object> entry : resultMap.entrySet()) {
                if (entry.getValue() != null && entry.getValue() instanceof String) {
                    String str = (String) entry.getValue();

                    if (Validator.isUrl(str) && !StrUtil.equals(str, url)) {
                        urlStr = str;
                        break;
                    }
                }
            }
        }
        return urlStr;
    }


    /**
     * <B>手机号码解绑</B>
     *
     * @param accountID
     * @return
     */
    public static GsonResult unbindPhoneNum(Integer accountID) {
        RequestAccount account = new RequestAccount();
        account.setAccountID(accountID);

        String body = new Gson().toJson(account);
        log.debug("手机号码解绑:" + GAME_API_URL + UNBIND_PHONE_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + UNBIND_PHONE_URL, body);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }

    /**
     * <B>清除常用设备号</B>
     *
     * @param accountID
     * @return
     */
    public static GsonResult clearCommonDev(Integer accountID) {
        RequestAccount account = new RequestAccount();
        account.setAccountID(accountID);

        String body = new Gson().toJson(account);
        String resultStr = HttpUtil.post(GAME_API_URL + CLEAR_DEV_URL, body);

        log.debug("清除常用设备号:" + GAME_API_URL + CLEAR_DEV_URL + "Json:" + body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B>游戏房间控制</B>
     *
     * @param gameId
     * @param dangerDown
     * @param safeDown
     * @param safeUp
     * @param dangerUp
     * @return
     */
    public static GsonResult gameCtrl(Integer gameId, Integer dangerDown, Integer safeDown, Integer safeUp, Integer dangerUp) {
        RequestGameCtrl gameCtrl = new RequestGameCtrl();
        gameCtrl.setGameId(gameId);
        gameCtrl.setDangerDown(dangerDown);

        gameCtrl.setSafeDown(safeDown);
        gameCtrl.setSafeUp(safeUp);
        gameCtrl.setDangerUp(dangerUp);

        String body = new Gson().toJson(gameCtrl);
        log.debug("游戏房间控制:" + GAME_API_URL + GAME_API_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + GAME_CTRL_URL, body);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B>处理玩家单控</B>
     *
     * @param accountId
     * @param type
     * @param gameIds
     * @param betLimit
     * @param rate
     * @param round
     * @return
     */
    public static GsonResult userCtrl(Integer accountId, Integer type, Integer[] gameIds, Integer betLimit, Integer rate, Integer round) {
        RequestUserCtrl userCtrl = new RequestUserCtrl();
        userCtrl.setAccountID(accountId);
        userCtrl.setType(type);

        userCtrl.setGameIds(gameIds);
        userCtrl.setBetLimit(betLimit);
        userCtrl.setRate(rate);

        userCtrl.setRound(round);
        String body = new Gson().toJson(userCtrl);
        log.debug("处理玩家单控:" + GAME_API_URL + USER_CTRL_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + USER_CTRL_URL, body);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }

    /**
     * <B>取消用户控制</B>
     * @param accountId
     * @param gameIds
     * @return
     */
    public static GsonResult cancelUserCtrl(Integer accountId, Integer[] gameIds) {
        RequestUserCtrl userCtrl = new RequestUserCtrl();
        userCtrl.setAccountID(accountId);
        userCtrl.setType(0);

        userCtrl.setGameIds(gameIds);
        String body = new Gson().toJson(userCtrl);
        log.debug("取消玩家单控:" + GAME_API_URL + USER_CTRL_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + USER_CTRL_URL, body);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B>修改绑定手机送金</B>
     *
     * @param regularAward
     * @param channelKey
     * @return
     */
    public static GsonResult bindGive(String channelKey, Integer regularAward, Integer rebate, Integer minExchange,
                                      Integer exchange, Integer initCoin, Integer damaMulti, Integer rebateDamaMulti,
                                      Integer exchangeCount, Integer promoteType, Integer resetDamaLeftCoin, Integer cpStationId,Integer cpLimit ) {
        RequestBindGive bindGive = new RequestBindGive();
        bindGive.setChannelKey(channelKey);
        bindGive.setRegularAward(regularAward);

        bindGive.setRebate(rebate);
        bindGive.setMinExchange(minExchange);
        bindGive.setExchange(exchange);

        bindGive.setInitCoin(initCoin);
        bindGive.setDamaMulti(damaMulti);
        bindGive.setRebateDamaMulti(rebateDamaMulti);

        bindGive.setExchangeCount(exchangeCount);
        bindGive.setPromoteType(promoteType);
        bindGive.setResetDamaLeftCoin(resetDamaLeftCoin);
        bindGive.setCpStationId(cpStationId);
        bindGive.setCpLimit(cpLimit);
        String body = new Gson().toJson(bindGive);
        log.debug("修改绑定手机送金:" + GAME_API_URL + BIND_GIVE_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + BIND_GIVE_URL, body );

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  GsonResult.class);
        }
        return  null;
    }


    /**
     * <B>同步游戏配置信息</B>
     *
     * @return
     */
    public static GsonResult synGameConfig(String channelKey, Integer[] roomId) {
        RequestGameConfig requestGameConfig = new RequestGameConfig();
        requestGameConfig.setChannelKey(channelKey);
        requestGameConfig.setNodeIds(roomId);

        String body = new Gson().toJson(requestGameConfig);
        log.debug("同步渠道信息:" + GAME_API_URL + SYS_CHANNEL_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + SYS_CHANNEL_URL, body);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B>同步渠道信息</B>
     *
     * @return
     */
    public static GsonResult synChannel(String channelKey, String name, Integer isOpen) {
        RequestChannel requestChannel = new RequestChannel();
        requestChannel.setChannelKey(channelKey);

        requestChannel.setName(name);
        requestChannel.setOpen(isOpen);
        String body = new Gson().toJson(requestChannel);

        log.debug("同步渠道信息:" + GAME_API_URL + SYS_CHANNEL_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + SYS_CHANNEL_URL, body);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }

    /**
     * <B>获取微信token信息</B>
     *
     * @return
     */
    public static GsonWechatToken getWechatToken(String appId, String secret, String code) {
        //微信授权接口https://api.weixin.qq.com/sns/oauth2/access_token?" +
        //                "appid="+Constant.WECHAT_APPID+"&secret="+Constant.WECHAT_SECRET+
        //                "&code="+code+"&grant_type=authorization_code";

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("appid", appId);
        paramMap.put("secret", secret);

        paramMap.put("code", code);
        paramMap.put("grant_type", "authorization_code");
        String resultStr = HttpUtil.post(WECHAT_TOKEN, paramMap);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonWechatToken.class);
        }
        return null;
    }

    /**
     * <B>获取微信登录信息</B>
     *
     * @return
     */
    public static GsonWechatUserInfo getWechatUserInfo(String tonken, String openId) {
        //微信用户信息接口String url = "https://api.weixin.qq.com/sns/userinfo?access_token="+
        //         weiXinToken.getAccess_token()+"&openid="+weiXinToken.getOpenid();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("access_token", tonken);
        paramMap.put("openid", openId);
        paramMap.put("lang", "zh_CN");
        String resultStr = HttpUtil.get(WECHAT_USER_INFO, paramMap);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonWechatUserInfo.class);
        }
        return null;
    }


    /**
     * optType:
     * 1-冻结
     * 2-解冻
     * 3-解绑第三方账号(暂时不做), 有参数thirdType, thirdType为1表示微信登录
     * <B>玩家状态修改</B>
     *
     * @param accountId
     * @param operatorType
     * @return
     */
    public static GsonResult operateAccount(Integer accountId, Integer operatorType, Integer thirdType) {
        RequestOperateAccount requestOperateAccount = new RequestOperateAccount();
        requestOperateAccount.setAccountID(accountId);

        requestOperateAccount.setOptType(operatorType);
        if (thirdType != null && thirdType != 0) {
            RequestOperateAccount.OptParam opt = requestOperateAccount.new OptParam();
            opt.setThirdType(thirdType);
            requestOperateAccount.setOptType(operatorType);
        }

        String body = new Gson().toJson(requestOperateAccount);
        String resultStr = HttpUtil.post(GAME_API_URL + OPERATE_ACCOUNT_URL, body);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B> 获取IP 地址信息</B>
     *
     * @param ip
     * @return
     */
    public static String getIpaddress(String ip, String type) {
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("ip", ip);

        //判断是淘宝还是新浪
        if (StrUtil.isNotBlank(type) && type.equalsIgnoreCase(UrlConstant.IpAddress.IP_TAOBAO.getName())) {
            return HttpUtil.post(UrlConstant.IpAddress.IP_TAOBAO.getValue(), paramMap);
        } else if (StrUtil.isNotBlank(type) && type.equalsIgnoreCase(UrlConstant.IpAddress.IP_SINA.getName())) {
            return HttpUtil.post(UrlConstant.IpAddress.IP_SINA.getValue(), paramMap);
        } else {
            return HttpUtil.post(UrlConstant.IpAddress.IP_126.getValue(), paramMap);
        }
    }

    /**
     * <B>同步白名单信息</B>
     *
     * @param
     * @param accountList
     * @param operatorType
     * @return
     */
    public static GsonResult synWhiteList(int operatorType, String[] accountList) {
        //OperatorType 操作类型 0-添加 1-取消
        //target 0-添加 1-取消
        RequestWhite requestWhite = new RequestWhite();
        requestWhite.setAccountList(accountList);

        requestWhite.setOperatorType(operatorType);
        String body = new Gson().toJson(requestWhite);

        log.debug("同步白名单信息:" + GAME_API_URL + WITELIST_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + WITELIST_URL, body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }

    /**
     * <B>平台维护</B>
     *
     * @param operatorType
     * @param start
     * @param finish
     * @return
     */
    public static GsonResult doPlatfromMaintenance(int operatorType, int start, int finish) {
        //operatorType 操作类型  1-维护  2-取消维护
        //start 开始时间， 取消维护 start为0
        //finish 结束时间， 取消维护 finish为0
        RequestPlatformMaint request = new RequestPlatformMaint();
        request.setFinish(finish);
        request.setStart(start);

        request.setOperatorType(operatorType);
        String body = new Gson().toJson(request);
        log.debug("平台维护信息:" + GAME_API_URL + PLATFORM_MAINTENANCE_URL + "Json:" + body);

        String resultStr = HttpUtil.post(GAME_API_URL + PLATFORM_MAINTENANCE_URL, body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B>游戏维护</B>
     *
     * @param operatorType
     * @param start
     * @param finish
     * @return
     */
    public static GsonResult doGameMaintenance(int operatorType, int start, int finish, Integer[] gameList) {
        RequestGameMaint request = new RequestGameMaint();
        request.setStart(start);
        request.setFinish(finish);

        request.setOperatorType(operatorType);
        request.setGameList(gameList);
        String body = new Gson().toJson(request);

        String resultStr = HttpUtil.post(GAME_API_URL + GAME_MAINTENANCE_URL, body);
        log.debug("游戏维护信息:" + GAME_API_URL + GAME_MAINTENANCE_URL + "Json:" + body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B>修改机器人信息</B>OK
     *
     * @return
     */
    public static GsonResult modifyBotInfo(Integer id, String nickName) {
        RequestBot request = new RequestBot();
        request.setId(id);
        request.setNick(nickName);
        String body = "{[" + new Gson().toJson(request) + "]}";

        String resultStr = HttpUtil.post(GAME_API_URL + MODIFY_AI_URL, body);
        log.debug("修改机器人信息:" + GAME_API_URL + MODIFY_AI_URL + "Json:" + body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B>添加公告信息</B>
     *
     * @param guid
     * @param start
     * @param finish
     * @return
     */
    public static GsonResult addBulletin(String guid, Integer start, Integer finish, String title, String content, String hyperlink, String[] channleList) {
        // guid 公告Id， 唯一id
        // start 开始时间
        // finish 结束时间
        // displayType 显示方式(*必填 1:静态显示 2 : 弹窗显示 3 : 下拉显示 4 : 滚动显示)
        // displayFrequence 显示频率(*必填 1：每填只弹一次 2：登陆显示一次 3：只显示一次 4：指定频率显示 5：一次性公告)
        // playFrequence 公告播放频率，只针对displayFrequence为4的情况下，指定显示频率
        // Title 公告标题
        // Hyperlink 公告链接
        // productList 公告发送的渠道(1020000001等，数组）json数组
        // gameList 公告显示的游戏（暂时填空），json数组
        String resultStr = HttpUtil.post(GAME_API_URL + ADD_BULLETIN_URL, packBulletin(guid, start, finish, title, content, hyperlink, channleList));
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    private static String packBulletin(String guid, Integer start, Integer finish, String title, String content, String hyperlink, String[] channleList) {
        RequestBulletin request = new RequestBulletin();
        request.setGuid(guid);
        request.setStart(start);
        request.setFinish(finish);

        request.setDisplayType(1);
        request.setTitle(title);
        request.setContent(content);

        request.setHyperLink(hyperlink);
        request.setChannelList(channleList);
        String body = new Gson().toJson(request);
        return body;
    }


    public static GsonResult modifyBulletin(String guid, Integer start, Integer finish, String title, String content, String hyperlink, String[] channleList) {
        // guid 公告Id， 唯一id
        // start 开始时间
        // finish 结束时间
        // displayType 显示方式(*必填 1:静态显示 2 : 弹窗显示 3 : 下拉显示 4 : 滚动显示)
        // displayFrequence 显示频率(*必填 1：每填只弹一次 2：登陆显示一次 3：只显示一次 4：指定频率显示 5：一次性公告)
        // playFrequence 公告播放频率，只针对displayFrequence为4的情况下，指定显示频率
        // Title 公告标题
        // Hyperlink 公告链接
        // productList 公告发送的渠道(1020000001等，数组）json数组
        // gameList 公告显示的游戏（暂时填空），json数组
        String resultStr = HttpUtil.post(GAME_API_URL + MODIFY_BULLETIN_URL, packBulletin(guid, start, finish, title, content, hyperlink, channleList));
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    public static GsonResult delBulletin(String guid) {
        // guid 公告Id， 唯一id
        // start 开始时间
        // finish 结束时间
        // displayType 显示方式(*必填 1:静态显示 2 : 弹窗显示 3 : 下拉显示 4 : 滚动显示)
        // displayFrequence 显示频率(*必填 1：每填只弹一次 2：登陆显示一次 3：只显示一次 4：指定频率显示 5：一次性公告)
        // playFrequence 公告播放频率，只针对displayFrequence为4的情况下，指定显示频率
        // Title 公告标题
        // Hyperlink 公告链接
        // productList 公告发送的渠道(1020000001等，数组）json数组
        // gameList 公告显示的游戏（暂时填空），json数组
        StringBuffer sb = new StringBuffer();
        sb.append("{");
        sb.append('"');
        sb.append("list");
        sb.append('"');
        sb.append("[");
        sb.append('"');
        sb.append(guid);
        sb.append('"');
        sb.append("]");
        sb.append("}");
        String resultStr = HttpUtil.post(GAME_API_URL + DELETE_BULLETIN_URL, sb.toString());
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    public static GsonResult batchDelBulletin(String[] guid) {
        // guid 公告Id， 唯一id
        // start 开始时间
        // finish 结束时间
        // displayType 显示方式(*必填 1:静态显示 2 : 弹窗显示 3 : 下拉显示 4 : 滚动显示)
        // displayFrequence 显示频率(*必填 1：每填只弹一次 2：登陆显示一次 3：只显示一次 4：指定频率显示 5：一次性公告)
        // playFrequence 公告播放频率，只针对displayFrequence为4的情况下，指定显示频率
        // Title 公告标题
        // Hyperlink 公告链接
        // productList 公告发送的渠道(1020000001等，数组）json数组
        // gameList 公告显示的游戏（暂时填空），json数组
        StringBuffer sb = new StringBuffer();
        sb.append("{");
        sb.append('"');
        sb.append("list");
        sb.append('"');
        sb.append(":[");
        for (int i = 0; i < guid.length; i++) {
            sb.append('"');
            sb.append(guid[i]);
            sb.append('"');
            if (i < guid.length - 1) {
                sb.append(",");
            }
        }
        sb.append("]}");
        String resultStr = HttpUtil.post(GAME_API_URL + DELETE_BULLETIN_URL, sb.toString());
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B>更新渠道配置文件</B>
     *
     * @param channelKey
     * @param name
     * @param versionNo
     * @param channelId
     */
    public String modifyChannelVersion(String channelKey, String name, String versionNo, List<String> channelId) {
        //channelKey 渠道号
        // name 渠道名称
        // versionNo 更新版本号
        // nodeIds 渠道开放功能，依据platform_openFunction.csv配置文件id字段，用逗号分隔

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("channelKey", channelKey);
        paramMap.put("name", name);

        paramMap.put("versionNo", versionNo);
        paramMap.put("channelId", channelId);
        return HttpUtil.post(CHANNEL_URL, paramMap);
    }


    /**
     * <B>获取游戏状态信息</B>
     */
    public static List<GsonGameState> getGameState() {
        log.debug("获取游戏状态信息:" + GAME_API_URL + GAME_STATE_URL + "Json:" + "");
        String resultStr = HttpUtil.post(GAME_API_URL + GAME_STATE_URL, "");
        List<GsonGameState> list = new ArrayList<GsonGameState>();

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            JsonParser parser = new JsonParser();
            JsonArray jsonArray = parser.parse(resultStr).getAsJsonArray();
            for (JsonElement elment : jsonArray) {

                //使用GSON，直接转成Bean对象
                GsonGameState gsonGameState = new Gson().fromJson(elment, GsonGameState.class);
                list.add(gsonGameState);
            }
        }
        return list;
    }


    /**
     * <B>获取平台状态信息</B>
     */
    public static GsonPlatformState getPlatformState() {
        log.debug("获取平台状态信息:" + GAME_API_URL + PLATFORM_STATE_URL + "Json:" + "");
        String resultStr = HttpUtil.post(GAME_API_URL + PLATFORM_STATE_URL, "");
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonPlatformState.class);
        }
        return null;
    }


    /**
     * <B>踢人</B>
     */
    public static GsonResult kickUser(Integer[] accountList) {
        // accounts 指定玩家踢人
        RequestKickUser request = new RequestKickUser();
        request.setAccounts(accountList);
        String body = new Gson().toJson(request);

        log.debug("踢人信息:" + GAME_API_URL + KICK_USER_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + KICK_USER_URL, body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B>发送邮件</B>
     */
    public static GsonResult sendMail(String title, String content, String rewardId, String rewardNum, int[] channelList, int[] toUserList, Integer sendType) {
        // batchId 批次号，作为业务关联标识
        // id 邮件ID
        // title 邮件标题
        // content 邮件内容
        // isGroupMail 是否群邮件
        // isTemplateMail 是否模板邮件
        // isTextMail 是否纯文本邮件，纯文本邮件没有附件
        // sendDate 发送时间 时间戳
        // sender 发送者
        // items 邮件附件
        // templateID 如果是模板邮件，这里是模板邮件ID, 0标识不是
        // customParams 如果是模板邮件，这里是模板邮件需要填充的参数，json数据
        // productIDList 发送给那些渠道
        // toUserList 发送给那些玩家, 玩家间用逗号隔开,如："1000001,1000002,1000003"
        RequestMail request = new RequestMail();
        RequestMail.Mail mail = request.new Mail();
        RequestMail.SendCondition sendCondition = request.new SendCondition();

        //复制信息
        mail.setTitle(title);
        mail.setContent(content);
        mail.setSendDate(DateUtil.currentSeconds());
        mail.setSender("STSTEM");

        //判断是否存在附件
        if (StrUtil.isNotBlank(rewardId) && StrUtil.isNotBlank(rewardNum)) {
            RequestMail.Mail.RewardList rewardList = mail.new RewardList();
            rewardList.setId(Integer.parseInt(rewardId));

            rewardList.setNum(Integer.parseInt(rewardNum) * 100);
            RequestMail.Mail.RewardList [] array = new RequestMail.Mail.RewardList[1];
            array[0] = rewardList;
            mail.setRewardList(array);
        }

        sendCondition.setSendType(sendType);
        if (channelList != null && channelList.length > 0) {
            sendCondition.setChannelList(channelList);
        }

        if (toUserList != null && toUserList.length > 0) {
            sendCondition.setToUserList(toUserList);
        }

        request.setSendCondition(sendCondition);
        request.setMail(mail);
        String body = new Gson().toJson(request);
        log.debug("发送邮件:" + GAME_API_URL + SEND_EMAIL_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + SEND_EMAIL_URL, body);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B> 查询用户信息</B>
     *
     * @param userId
     * @return
     */
    public static GsonUserInfo queryAccountInfo(String userId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("accountID", userId);
        String resultStr = HttpUtil.post(GAME_API_URL + USER_URL, paramMap);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonUserInfo.class);
        }
        return null;
    }


    /**
     * <B>查询玩家操作</B>
     *
     * @param userId
     * @param optType
     * @param thirdType
     * @return
     */
    public static String operateAccount(String userId, String optType, String thirdType) {
        //accountID 玩家ID
        //optType 操作类型 1-冻结 2-解冻 3-第三方账号解绑 4-账号重命名（账号名改为:原账号名-玩家ID) 5-绑定账号为手机号（手机账号转正）
        //thirdType 操作扩展内容 如果操作类型是3，有效， 这个值表示第三方账号类型： 1-微信
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("accountID", userId);
        paramMap.put("optType", optType);
        paramMap.put("thirdType", thirdType);
        return HttpUtil.post(GAME_API_URL + USER_OPERATION_URL, paramMap);
    }


    /**
     * <B>GM命令</B>OK
     *
     * @return
     */
    public static GsonResult gmAction(String domainCode, String cmd) {
        String resultStr = HttpRequest.post(GAME_API_URL + GM_URL + domainCode).contentType("Text").timeout(HttpRequest.TIMEOUT_DEFAULT).body(cmd).execute().body();
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B>上分</B>
     *
     * @param accountID
     * @param orderId
     * @return
     */
    public static GsonResult rechargeOrder(Integer accountID, String orderId, Integer amount, Integer code, Integer type, Integer isManual, Double damaMulti, Integer rate) {
        //accountID 玩家ID
        // order 订单ID
        RequestRecharge request = new RequestRecharge();
        request.setAccountID(accountID);
        request.setOrderId(orderId);

        request.setAmount(amount);
        request.setCode(code);
        request.setType(type);

        //1表示手动
        request.setIsManual(isManual);
        //设置赠送率
        if (rate != null && rate > 0) {
            double d = (double) rate/100;
            request.setRate(d);
        }

        if (damaMulti != null && damaMulti > 0) {
            request.setDamaMulti(damaMulti);
        }
        String body = new Gson().toJson(request);

        log.debug("玩家上分信息:" + GAME_API_URL + RECHARGE_RESULT_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + RECHARGE_RESULT_URL, body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B>下分</B>
     *
     * @param userId
     * @param orderId
     * @return
     */
    public static GsonResult cashOrder(Integer userId, String orderId, Integer cashFee) {
        //accountID 玩家ID0
        // order 订单ID
        RequestCash request = new RequestCash();
        request.setAccountID(userId);
        request.setCashNo(orderId);
        request.setCashFee(cashFee);

        String body = new Gson().toJson(request);

        log.debug("玩家下分信息:" + GAME_API_URL + CASH_RESULT_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + CASH_RESULT_URL, body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }

    /**
     * <B>修改用户密码</B>OK
     *
     * @param userId
     * @param oldPassword
     * @param newPassword
     * @param check
     * @return
     */
    public static GsonResult modifyAccountPassword(int userId, String oldPassword, String newPassword, Boolean check) {
        // accountID 玩家ID
        // oldPassword 旧密码
        // newPassword 新密码
        // check 是否检查旧密码是否一致 true表示要检查
        RequestModifyAccountPw request = new RequestModifyAccountPw();
        request.setAccountID(userId);
        request.setCheck(check);

        request.setOldPassword(oldPassword);
        request.setNewPassword(newPassword);
        String body = new Gson().toJson(request);

        log.debug("修改用户密码信息:" + GAME_API_URL + MODIFY_ACCOUNT_PASSWORD_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + MODIFY_ACCOUNT_PASSWORD_URL, body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B>获取用户所在游戏</B>OK
     *
     * @param
     * @param
     * @return
     */
    public static GsonUserGame getUserGameList(Integer userId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("accountID", userId);
        String json = new Gson().toJson(paramMap);

        log.debug("查询用户所在游戏信息:" + GAME_API_URL + ON_LINE_USER_URL + "Json:" + json);
        String resultStr = HttpUtil.post(GAME_API_URL + ON_LINE_USER_URL, json);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonUserGame.class);
        }
        return null;
    }


    /**
     * <B>请求提现订单结果</B>
     *
     * @param userId
     * @return
     */
    public static GsonResult cashOrderResult(Integer userId, String cashNo, Integer code, String msg) {
        RequestCashAol request = new RequestCashAol();
        request.setAccountID(userId);
        request.setCashNo(cashNo);

        request.setCode(code);
        request.setMsg(msg);
        String body = new Gson().toJson(request);

        log.debug("请求提现订单结果信息:" + GAME_API_URL + CASH_ORDER_RESULT_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + CASH_ORDER_RESULT_URL, body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }


    /**
     * <B>获取验证码信息</B>
     *
     * @param accountID
     * @param phone
     * @param reqType
     * @return
     */
    public static String getVerifyCode(String accountID, String phone, String reqType) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("accountID", accountID);
        paramMap.put("phone", phone);
        paramMap.put("reqType", reqType);

        String resultStr = HttpUtil.get(GAME_API_URL + RECHARGE_RESULT_URL, paramMap);
        return "";
    }


    /**
     * <B>验证验证码信息</B>
     *
     * @param accountID
     * @param verifyCode
     * @param phone
     * @param reqType
     * @return
     */
    public static String checkVerifyCode(String accountID, String verifyCode, String phone, String reqType) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("accountID", accountID);
        paramMap.put("verifyCode", verifyCode);

        paramMap.put("phone", phone);
        paramMap.put("reqType", reqType);
        String resultStr = HttpUtil.get(GAME_API_URL + RECHARGE_RESULT_URL, paramMap);
        return "";
    }


    /**
     * <B>解除绑定手机号码</B>
     *
     * @param userId
     * @return
     */
    public static String unbindPhoneNum(String userId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("accountID", userId);
        log.debug("解绑游戏信息:" + GAME_API_URL + CASH_ORDER_RESULT_URL + "Json:" + paramMap.toString());
        return HttpUtil.post(GAME_API_URL + UNBIND_PHONENUM_URL, paramMap);
    }


    /**
     * <B>清除常用设备号</B>
     *
     * @param userId
     * @return
     */
    public static String clearCommonDev(String userId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("accountID", userId);
        return HttpUtil.post(GAME_API_URL + CLEAR_COMMON_DEV_URL, paramMap);
    }

    /**
     * <B>清除绑定身份证号码</B>
     *
     * @param userId
     * @return
     */
    public static String clearIdCard(String userId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("accountID", userId);
        return HttpUtil.post(GAME_API_URL + CLEAR_ID_CARD_URL, paramMap);
    }

    /**
     * l
     * @param userId
     * @param cashNo
     * @param code
     * @param msg
     * @return
     */
    public static GsonResult takeRebate(Long userId, Double money, String bid) {
        RequestTakeRebate request = new RequestTakeRebate();
        request.setAccountID(userId);
        request.setMoney(money);
        request.setBid(bid);
        String body = new Gson().toJson(request);

        log.debug("请求提现订单结果信息:" + GAME_API_URL + TAKE_BETTING_REBATE_URL + "Json:" + body);
        String resultStr = HttpUtil.post(GAME_API_URL + TAKE_BETTING_REBATE_URL, body);
        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, GsonResult.class);
        }
        return null;
    }

}
