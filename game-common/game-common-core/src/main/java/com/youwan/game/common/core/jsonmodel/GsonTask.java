package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;

@Data
public class GsonTask implements Serializable {

    private Integer taskId;

    private Long targetNum;

    private Long taskReward;

    private Integer taskOpenType;

    private Integer continueTaskCoinLimit;

}
