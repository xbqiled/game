package com.youwan.game.common.core.util;

import com.alibaba.fastjson.JSON;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class SignUtil {

    public static final String SIGN_ALGORITHMS = "SHA-1";

    public static String qtbSign(String data, String privateKey) throws Exception {
        privateKey = privateKey.replaceAll("\n", "");
        return RsaUtils.sign(data, privateKey);
    }


    public static String qtbSign(Map context, String privateKey) throws Exception {
        SortedMap signParams = new TreeMap();
        signParams.putAll(context);
        return qtbSign(JSON.toJSONString(signParams), privateKey);
    }


    public static String sign(String content, String inputCharset) {
        // 获取信息摘要 - 参数字典排序后字符串
        try {
            // 指定sha1算法
            MessageDigest digest = MessageDigest.getInstance(SIGN_ALGORITHMS);
            digest.update(content.getBytes(inputCharset));
            // 获取字节数组
            byte messageDigest[] = digest.digest();
            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            // 字节数组转换为 十六进制 数
            for (int i = 0; i < messageDigest.length; i++) {
                String shaHex = Integer.toHexString(messageDigest[i] & 0xFF);
                if (shaHex.length() < 2) {
                    hexString.append(0);
                }
                hexString.append(shaHex);
            }
            return hexString.toString().toUpperCase();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 获取签名sign,规则:<br>
     * 1.按照参数名字符升序排列<br>
     * 2.移除key为sign以及value为空(""," ","null",null)的参数<br>
     * 3.MD5方式：sign = md5(待签名串 + 密钥)32位小写<br>
     *
     * @param map
     *            签名数据Map
     * @param key
     *            秘钥
     * @return 签名串
     */
    public static String getSign(Map<String, ? extends Object> map, String key) {
        return md5Str(getSignStr(map, key)).toLowerCase();
    }
    /**
     * 获取签名字符串,规则:<br>
     * 1.MD5方式：sign = md5(待签名串 + 密钥)32位小写<br>
     *
     * @param signStr
     *            待签名字符串
     * @return 签名串
     */
    public static String getSign(String signStr) {
        return md5Str(signStr).toLowerCase();
    }
    /**
     * 签名校验
     *
     * @param map
     *            签名数据Map
     * @param key
     *            秘钥
     * @param sign
     *            MD5签名串
     * @return 验签成功返回true
     */
    public static boolean signVerify(Map<String, ? extends Object> map, String key, String sign) {
        if (map == null || map.isEmpty() || key == null || sign == null) {
            return false;
        }
        return sign.equals(getSign(map, key));
    }
    /**
     * 获取待签名字符串,规则:<br>
     * 1.按照参数名字符升序排列<br>
     * 2.移除key为sign以及value为空(""," ","null",null)的参数<br>
     *
     * @param map
     *            签名数据Map
     * @param key
     *            秘钥
     * @return 待签名字符串
     */
    public static String getSignStr(Map<String, ? extends Object> map, String key) {
        map.remove("sign");
        map = new TreeMap<>(map);
        StringBuilder signStr = new StringBuilder();
        String value = null;
        for (Map.Entry<String, ? extends Object> m : map.entrySet()) {
            value = toStr(m.getValue());
            if (!"".equals(value)) {
                signStr.append(m.getKey());
                signStr.append("=");
                signStr.append(value);
                signStr.append("&");
            }
        }
        signStr.deleteCharAt(signStr.length() - 1);
        signStr.append(key);
        return signStr.toString();
    }


    // ---------------------------------------------------------------------------------------------
    private static String toStr(Object obj) {
        return obj == null || "null".equals(obj.toString().trim()) ? "" : obj.toString().trim();
    }



    private static String md5Str(String str) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(str.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        byte[] byteArray = messageDigest.digest();
        StringBuffer md5StrBuff = new StringBuffer();
        for (int i = 0; i < byteArray.length; i++) {
            if (Integer.toHexString(0xFF & byteArray[i]).length() == 1)
                md5StrBuff.append("0").append(Integer.toHexString(0xFF & byteArray[i]));
            else
                md5StrBuff.append(Integer.toHexString(0xFF & byteArray[i]));
        }
        return md5StrBuff.toString();
    }


    public static String encryption(String plainText) {
        String re_md5 = new String();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(plainText.getBytes());
            byte b[] = md.digest();

            int i;

            StringBuffer buf = new StringBuffer("");
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }

            re_md5 = buf.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return re_md5;
    }

    /**
     * 大写密文
     * @param plainText
     * @return
     */
    public static String encryptionToUpperCase(String plainText) {
        String re_md5 = new String();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(plainText.getBytes());
            byte b[] = md.digest();

            int i;

            StringBuffer buf = new StringBuffer("");
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }

            re_md5 = buf.toString().toUpperCase();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return re_md5;
    }

    public static String mapToUrlString(TreeMap<String, String> treeMap) {
        StringBuilder formData = new StringBuilder();
        if (treeMap != null && treeMap.size() > 0) {

            for(Map.Entry<String, String> entry : treeMap.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if(formData.length()>0){
                    formData.append("&");
                }
                formData.append(key+"="+value);
            }

        }
        return formData.toString();
    }

    public static String formatBizQueryParaMap(Map<String, String> paraMap,
                                               boolean urlencode) {
        String buff = "";
        try {
            List<Map.Entry<String, String>> infoIds = new ArrayList<Map.Entry<String, String>>(paraMap.entrySet());
            Collections.sort(infoIds,
                    (o1, o2) -> (o1.getKey()).toString().compareTo(
                            o2.getKey()));
            for (int i = 0; i < infoIds.size(); i++) {
                Map.Entry<String, String> item = infoIds.get(i);
                if (item.getKey() != "") {
                    String key = item.getKey();
                    String val = item.getValue();
                    if (urlencode) {
                        val = URLEncoder.encode(val, "utf-8");
                    }
                    buff += key + "=" + val + "&";
                }
            }
            if (buff.isEmpty() == false) {
                buff = buff.substring(0, buff.length() - 1);
            }
        } catch (Exception e) {
            return "";
        }
        return buff;
    }
}
