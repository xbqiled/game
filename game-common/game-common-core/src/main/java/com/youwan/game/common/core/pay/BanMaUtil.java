package com.youwan.game.common.core.pay;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.google.gson.Gson;
import com.youwan.game.common.core.pay.json.ResultXinFuTong;
import com.youwan.game.common.core.util.BanMaSignUtil;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;

public class BanMaUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/banMa";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "";

    public static Map<String, String> packaData(String... params) {
        Map<String, String> map = new HashMap<>();
        String miyao =params[6];  //商户秘钥
        String userId =params[8];   // url传来的userid
        String gongyao =params[9];   //商户公钥

        //订单号 必填
        map.put("orderId", params[1]);
        //订单下单时间
        map.put("orderTime", DateUtil.format(new Date(), DatePattern.PURE_DATETIME_MS_PATTERN));
        //订单总金额，单位分
        map.put("amount",  new BigDecimal(params[2]).multiply(new BigDecimal(100)).toString());
        //货币种类 必填
        map.put("currencyType", "CNY");
        //商品名称 必填
        map.put("goods", "buy golden");
        //商品名称 必填
        map.put("notifyUrl",  API_SYSTM_URL + NOTIFY_URL);
        //网页回调地址， 可选
        map.put("callBackUrl", API_SYSTM_URL + CALL_BACK_URL +  params[1]);
        //说明
        map.put("desc", "xinbo");


        String sign = "";
        try {
            sign = BanMaSignUtil.md5Sign(map, miyao);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //签名
        map.put("sign", sign);
        //商户编号
        map.put("mchtId", params[0]);
        //版本号 必填
        map.put("version", "20");
        //支付方式 必填
        map.put("biz", params[3]);
        return  map;
    }


    /**
     * <B>去支付</B>
     * @param map
     * @return
     */
    public static ResultXinFuTong doPay(Map<String, String> map) {
        String bodyStr = new Gson().toJson(map);
        String resultStr =  HttpUtil.post(ORDER_URL, bodyStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr,  ResultXinFuTong.class);
        }

        return  null;
    }

}
