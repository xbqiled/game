package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class RequestCashAol implements Serializable {

    private Integer accountID;
    private String cashNo;
    private Integer code;
    private String msg;

}
