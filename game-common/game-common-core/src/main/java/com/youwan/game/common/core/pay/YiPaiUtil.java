package com.youwan.game.common.core.pay;

import com.youwan.game.common.core.util.SignUtil;

import java.math.BigDecimal;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;
import static com.youwan.game.common.core.constant.UrlConstant.FEONT_SYSTM_URL;

public class YiPaiUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/yiPai";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "";

    /**
     * 组装数据去支付
     * @param params
     * @return
     */
    public static Map<String, String> packaData(String... params) {
        SortedMap<String, String> map = new TreeMap<>();

        String merchantKey = params[6];
        map.put("partner", params[0]);
        map.put("payment_type", params[3]);
        map.put("total_fee", new BigDecimal(params[2]).toString());

        map.put("out_trade_no", params[1]);
        map.put("return_url", FEONT_SYSTM_URL + CALL_BACK_URL + params[1]);
        map.put("notify_url", API_SYSTM_URL + NOTIFY_URL);

        map.put("body", "Onlinepay");
        map.put("sign_type", "MD5");
//      map.put("total_fee", new BigDecimal(amount).setScale(2).toString());

        String str = String.format("partner=%s&out_trade_no=%s&total_fee=%s&payment_type=%s&notify_url=%s&return_url=%s&body=%s%s",
                map.get("partner"),map.get("out_trade_no"),map.get("total_fee"),map.get("payment_type"),map.get("notify_url"),map.get("return_url"),map.get("body"), merchantKey);

        map.put("sign", SignUtil.encryption(str.toString()).toLowerCase());

        return map;
    }


}
