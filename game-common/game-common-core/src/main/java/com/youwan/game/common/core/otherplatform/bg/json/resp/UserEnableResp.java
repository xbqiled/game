package com.youwan.game.common.core.otherplatform.bg.json.resp;


import lombok.Data;

import java.io.Serializable;
import java.util.Map;


@Data
public class UserEnableResp implements Serializable {


    private String id;
    private String result;
    private Map<String , Object> error;
    private String jsonrpc;

}
