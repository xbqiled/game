package com.youwan.game.common.core.jsonmodel;


import lombok.Data;

import java.io.Serializable;


@Data
public class RequestCash implements Serializable {
    private Integer accountID;
    private String cashNo;
    private Integer cashFee;
}
