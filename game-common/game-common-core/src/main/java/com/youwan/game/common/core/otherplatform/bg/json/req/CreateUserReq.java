package com.youwan.game.common.core.otherplatform.bg.json.req;


import lombok.Data;

import java.io.Serializable;


@Data
public class CreateUserReq implements Serializable {

    private String id;
    private String method = "open.user.create";
    private Params params;
    private String jsonrpc = "2.0";

    @Data
    public static class Params implements Serializable {
        private String random;
        private String sn;

        private String digest;
        private String loginId;
        private String nickname;

        private String agentLoginId;
    }

}
