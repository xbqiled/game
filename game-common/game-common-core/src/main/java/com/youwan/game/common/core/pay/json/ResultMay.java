package com.youwan.game.common.core.pay.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class ResultMay implements Serializable {

    private Integer code;

    private Data data;

    @lombok.Data
    public class Data implements Serializable {
        private String order_number;
        private String mch_number;

        private String mch_id;
        private String code;
        private String pay_url;

        private String total;
        private String cash;
        private String free;
        private Integer status;
    }
}
