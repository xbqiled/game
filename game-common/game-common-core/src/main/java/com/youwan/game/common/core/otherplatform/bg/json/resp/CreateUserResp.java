package com.youwan.game.common.core.otherplatform.bg.json.resp;

import lombok.Data;

import java.io.Serializable;

@Data
public class CreateUserResp implements Serializable {

    private String id;

    private Result result;

    private String error;

    private String jsonrpc;

    @Data
    public static class Result implements Serializable {
        private String loginId;
        private Boolean success;
        private String nickname;
        private Long userId;
        private String regType;
        private String agentLoginId;
    }

}
