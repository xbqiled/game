package com.youwan.game.common.core.util.keygen;

/**
 * Key generator interface.
 *
 * @author zhangliang
 */
public interface KeyGenerator {

    /**
     * Generate key.
     *
     * @return generated key
     */
    Number generateKey();
}
