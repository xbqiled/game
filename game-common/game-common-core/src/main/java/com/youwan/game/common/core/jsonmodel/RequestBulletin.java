package com.youwan.game.common.core.jsonmodel;


import lombok.Data;

import java.io.Serializable;




@Data
public class RequestBulletin implements Serializable {

    private String guid;
    private Integer start;
    private Integer finish;
    private Integer displayType;

    private String title;
    private String content;
    private String hyperLink;

    private String[] channelList;

}
