package com.youwan.game.common.core.jsonmodel;


import lombok.Data;

import java.io.Serializable;


@Data
public class RequestUserCtrl implements Serializable {

    Integer accountID;
    Integer type;
    Integer gameIds [];

    Integer betLimit;
    Integer rate;
    Integer round;
}
