package com.youwan.game.common.core.betting.entity;

import lombok.Data;

import javax.persistence.Id;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-04-21.
 */
@Data
public class UserAccountInfo extends PojoWithMPath {

    @Id
    private Long t_accountID;
    private String t_phoneNumber;
    private String t_nickName;
    private String t_origin;
    private Long t_regTime;
    private Integer t_regTerminalType;
    private String t_regIP;
    private String t_regDevice;
    private Long t_lastLoginTime;
    private Long t_lastLogoutTime;
    private String t_channelKey;
    private Integer t_accountState;
    private Integer t_vipLevel;
}
