package com.youwan.game.common.core.otherplatform.bg.json.req;

import lombok.Data;

import java.io.Serializable;


@Data
public class UserRoundReq implements Serializable {

    public String id;
    public String method = "open.video.round.query";
    public Params params;
    public String jsonrpc = "2.0";

    @Data
    public static class Params implements Serializable {
        private String random;
        private String sn;

        private String sign;
        private String startTime;
        private String endTime;

        private String round;
        private Long pageIndex;
        private Long pageSize;
    }

}