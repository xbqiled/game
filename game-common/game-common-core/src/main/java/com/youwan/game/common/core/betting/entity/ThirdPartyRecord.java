package com.youwan.game.common.core.betting.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-15.
 */
@Data
public abstract class ThirdPartyRecord extends PojoWithMPath {

    private Long userId;
    private Date createAt = new Date();
    private Date modifyAt;
    private String userName;
    private String channelId;

    private Integer scrub_state;
    private String scrub_epoch;

}
