package com.youwan.game.common.core.constant.enums;

public enum PushResponseConstant {

    SUCCESS("0000", "操作成功"),
    FAILED("9000", "操作失败"),
    SYSTEM_EXCEPTION("9001", "系统错误");

    public String code;
    public String msg;

    private PushResponseConstant(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
