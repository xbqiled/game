package com.youwan.game.common.core.pay;


import cn.hutool.core.date.DateUtil;
import com.youwan.game.common.core.util.SignUtil;

import java.math.BigDecimal;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;
import static com.youwan.game.common.core.constant.UrlConstant.FEONT_SYSTM_URL;

public class JuBaoFuUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/juBaoFu";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "";

    public static Map<String, String> packaData(String... params) {
        SortedMap<String, String> map = new TreeMap<>();
        String memberNumber = params[0]; //商户号

        String memberOrderNumber = params[1]; //商户订单号
        String tradeAmount = params[2]; //交易金额

        String defrayalType = params[3]; //支付方式
        String miyao = params[6];  //商户秘钥
        String userId =params[8];   // url传来的userid
        //设置下单地址
        ORDER_URL = params[4];

        //商户号
        map.put("customerno", memberNumber);
        //订单号
        map.put("customerbillno", memberOrderNumber);
        //支付金额
        map.put("orderamount", new BigDecimal(tradeAmount).setScale(2).toString());
        //支付类型
        map.put("channeltype", defrayalType);
        //下单时间
        map.put("customerbilltime", DateUtil.now());

        //回调地址
        map.put("notifyurl", API_SYSTM_URL + NOTIFY_URL);
        //下单ip
        map.put("ip", "10.10.10.10");
        //下单ip
        map.put("devicetype", "web");
        //
        map.put("customeruser", userId);
        map.put("returnurl", FEONT_SYSTM_URL + CALL_BACK_URL + params[1]);

        //生成签名信息
        StringBuffer sb = new StringBuffer();
        sb.append("channeltype=" +  map.get("channeltype"));
        sb.append("&customerbillno=" +  map.get("customerbillno"));
        sb.append("&customerbilltime=" +  map.get("customerbilltime"));
        sb.append("&customerno=" + map.get("customerno"));
        sb.append("&customeruser=" + map.get("customeruser"));
        sb.append("&devicetype=" + map.get("devicetype"));

        sb.append("&ip=" +  map.get("ip"));
        sb.append("&notifyurl=" +  map.get("notifyurl"));
        sb.append("&orderamount=" +  map.get("orderamount"));
        sb.append("&returnurl=" +  map.get("returnurl"));
        sb.append("&key=" +  miyao);

        map.put("sign", SignUtil.getSign(sb.toString()));
        return map;
    }

}
