package com.youwan.game.common.core.pay.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class ResultXinFuTong implements Serializable {

    private String codeUrl;
    private String respCode;
    private String respMessage;



}
