package com.youwan.game.common.core.pay;


import com.youwan.game.common.core.util.MD5;
import java.math.BigDecimal;
import java.util.*;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;

public class FeiFuUtil {


    //回调地址
    private static String NOTIFY_URL = "/notify/feiFu";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "";


    public static Map<String, String> packaData(String... params) {
        SortedMap<String, String> map = new TreeMap<>();

        String memberNumber =params[0]; //商户号
        String memberOrderNumber =params[1]; //商户订单号
        String tradeAmount =params[2]; //交易金额

        ORDER_URL = params[4];
        String gongyao = params[9];   //商户公钥
        String defrayalType = params[3]; //支付方式

        map.put("inputCharset", "UTF-8");
        map.put("signType", "MD5");
        map.put("MerchantId", memberNumber);

        map.put("out_trade_no", memberOrderNumber);
        map.put("amount", new BigDecimal(tradeAmount).setScale(2).toString());
        map.put("attach", "Onlinepay");

        map.put("gateway", defrayalType);
        map.put("notifyUrl",  API_SYSTM_URL + NOTIFY_URL);
        //网页回调地址， 可选
        map.put("returnUrl", API_SYSTM_URL + CALL_BACK_URL +  params[1]);

        Set<Map.Entry<String, String>> set = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();
        StringBuffer buffer = new StringBuffer();
        while (iterator.hasNext()) {
            Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
            buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }
        String signSrc = buffer.substring(0, buffer.length()-1) + gongyao;
        map.put("sign", MD5.encryption(signSrc).toUpperCase());
        return map;
    }

}
