package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class RequestInviteDeposit implements Serializable {

    private String channel;
    private Long openType;

    private Long timeType;
    private Long beginTime;
    private Long endTime;

    private Long firstTotalDeposit;
    private Long selfReward;
    private Long upperReward;
}
