package com.youwan.game.common.core.pay;

import com.youwan.game.common.core.util.MD5;

import java.math.BigDecimal;
import java.util.*;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;
import static com.youwan.game.common.core.constant.UrlConstant.FEONT_SYSTM_URL;

public class TianDiUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/tianDi";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "";


    public static Map<String, String> packaData(String... params) {

        String memberNumber =params[0]; //商户号
        String memberOrderNumber =params[1]; //商户订单号
        String tradeAmount =params[2]; //交易金额

        String defrayalType =params[3]; //支付方式
        String payGateway =params[4]; //支付网关
        String commodityBody =params[5]; //商品信息

        String miyao =params[6];  //商户秘钥
        String bankCode =params[7];   //银行代码
        String userId =params[8];   // url传来的userid

        String gongyao =params[9];   //商户公钥
        String appId = params[10];   //
        ORDER_URL = params[4];

        SortedMap<String, String> map = new TreeMap<>();

        map.put("pid", memberNumber);
        map.put("type", defrayalType);
        map.put("out_trade_no", memberOrderNumber);

        map.put("return_url", FEONT_SYSTM_URL + CALL_BACK_URL + params[1]);
        map.put("notify_url", API_SYSTM_URL + NOTIFY_URL);

        map.put("name", "金币购买");
        map.put("money", new BigDecimal(tradeAmount).multiply(new BigDecimal(100)).toString()); // 订单金额（单位：分）
        map.put("sitename", "xinbo");

        StringBuffer buffer = new StringBuffer();
        Set<Map.Entry<String, String>> set = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();

        StringBuffer sb = new StringBuffer();
        sb.append("money=" + new BigDecimal(tradeAmount).multiply(new BigDecimal(100)).toString() + "&");
        sb.append("name=" + "金币购买" + "&");
        sb.append("notify_url=" + FEONT_SYSTM_URL + NOTIFY_URL + "&");
        sb.append("out_trade_no=" + memberOrderNumber + "&");
        sb.append("pid=" + memberNumber + "&");
        sb.append("return_url=" + FEONT_SYSTM_URL + CALL_BACK_URL + params[1] + "&");
        sb.append("sitename=" +  "xinbo&");
        sb.append("type=" +  defrayalType);
        sb.append("type=" +  defrayalType);

        //String signSrc = buffer.append("api_secret=").append(gongyao).toString();
        map.put("sign", MD5.encryption(sb.toString()).toLowerCase());
        map.put("sign_type", "MD5");
        return map;
    }

}
