package com.youwan.game.common.core.okhttp;

import okhttp3.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * <B>OKHTTP工具类</B>
 */
public class OKHttpApi {

    private final static Integer TIMEOUT = 10;

    public static Response get(String url) throws Exception {
        return get(url,new HashMap<>(),TIMEOUT);
    }

    public static Response get(String url, Map<String, Object> map) throws Exception {
        return get(url, map, TIMEOUT);
    }

    public static Response post(String url, Map<String, Object> map) throws Exception {
        return post(url, map, TIMEOUT);
    }

    public static Response postJson(String url, String json) throws Exception {
        return postJson(url, json, TIMEOUT);
    }

    public static Response post(String url, Map<String,Object> map, Integer timeout) throws Exception {
        OkHttpClient client = new OkHttpClient.Builder().readTimeout(timeout, TimeUnit.MINUTES).build();
        FormBody.Builder formBody = new FormBody.Builder();

        for (String key:map.keySet()) {
            formBody.add(key,String.valueOf(map.get(key)));
        }

        Request request = new Request.Builder().url(url).post(formBody.build()).build();
        return client.newCall(request).execute();
    }

    public static Response get(String url, Map<String,Object> map,Integer timeout) throws Exception {
        OkHttpClient client = new OkHttpClient.Builder().readTimeout(timeout, TimeUnit.MINUTES).build();
        HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();

        for (String key:map.keySet()) {
            urlBuilder.addQueryParameter(key,String.valueOf(map.get(key)));
        }

        Request request = new Request.Builder().url(urlBuilder.build()).get().build();
        return client.newCall(request).execute();
    }

    public static Response postJson(String url, String json, Integer timeout) throws Exception {
        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(timeout, TimeUnit.MINUTES).build();
        RequestBody requestBody = FormBody.create(MediaType.parse("application/json; charset=utf-8"), json);

        Request request = new Request.Builder()
                .url(url)//请求的url
                .post(requestBody)
                .build();
        return client.newCall(request).execute();
    }


//    public List<Goods> findGoods() {
//        try {
//            Response response = HttpHelper.post("http://localhost:9091/getGoods");
//            List<Goods> goods = returnList(response,Goods.class);
//            return goods;
//        } catch (Exception e) {
//            return null;
//        }
//    }
//    public static <K> List<K> returnList(Response response, Class<K> tClass) throws Exception{
//        if (response.isSuccessful()) {
////            List<K> list =JSON.parseArray(response.body().string(),tClass);
//            JSONObject object = JSON.parseObject(response.body().string());
//            Boolean result = object.get("status").equals(200);
//            if (result) {
//                List<K> list = object.getJSONArray("data").toJavaList(tClass);
//                return list;
//            }
//        }else {
//            throw new Exception("返回结果失败");
//        }
//        return new LinkedList<K>();
//    }
}
