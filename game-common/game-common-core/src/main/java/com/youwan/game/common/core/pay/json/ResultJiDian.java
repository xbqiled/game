package com.youwan.game.common.core.pay.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class ResultJiDian implements Serializable {

    String mch_code;
    String amount;
    String out_trade_no;

}
