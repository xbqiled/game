package com.youwan.game.common.core.jsonmodel;


import lombok.Data;

import java.io.Serializable;

@Data
public class RequestKickUser implements Serializable {
    private Integer accounts[];
}
