package com.youwan.game.common.core.betting;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-21.
 */
@Data
public class ScrubResult {

    public final static int TYPE_LOTTERY = 0;
    public final static int TYPE_CROWN = 1;
    public final static int TYPE_EGAME = 2;
    public final static int TYPE_LIVE = 3;
    public final static int TYPE_SPORT = 4;
    public final static int TYPE_QP = 5;

    /**
     * 0:彩票, 1:皇冠 2:电子 3：真人 4: 第三方体育
     */
    private Integer type;

    /**
     *
     */
    private String epoch;

    /**
     * 用户id
     */
    private Long uid;

    /**
     * 洗码统计值
     */
    private BigDecimal value;

    /**
     * 洗码结果
     */
    private BigDecimal result;

    /**
     * 洗码比例
     */
    private BigDecimal rebateRatio;

    /**
     * 领取
     */
    private Boolean took = false;
}
