package com.youwan.game.common.core.pay;


import cn.hutool.http.HttpRequest;
import com.youwan.game.common.core.util.SignUtil;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;
import static com.youwan.game.common.core.constant.UrlConstant.FEONT_SYSTM_URL;

public class DuoDuoUtil {


    //回调地址
    private static String NOTIFY_URL = "/notify/duoDuo";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "http://103.80.26.219:3020/api/pay/create_order";

    public static Map<String, String> packaData(String... params) {
        Map<String,String> map = new HashMap<>();
        String memberNumber = params[0]; //商户号
        String memberOrderNumber = params[1]; //商户订单号
        String tradeAmount = params[2]; //交易金额
        String defrayalType = params[3]; //支付方式
        String payGateway =params[4]; //支付网关
        ORDER_URL = payGateway;
        String key = params[6];  //商户秘钥
        String appId = params[10];  //商户秘钥
        map.put("mchId", memberNumber);
        map.put("appId", appId);
        map.put("productId", defrayalType);
        map.put("mchOrderNo", memberOrderNumber);
        map.put("currency", "cny");
        BigDecimal fenBd = new BigDecimal(tradeAmount).multiply(new BigDecimal(100));
        fenBd = fenBd.setScale(0, BigDecimal.ROUND_HALF_UP);
        map.put("amount", String.valueOf(fenBd));
        map.put("notifyUrl", API_SYSTM_URL + NOTIFY_URL);
        map.put("subject", "充值");
        map.put("body", "业务充值");
        TreeMap<String, String> treeMap = new TreeMap<>(map);
        String str = SignUtil.mapToUrlString(treeMap)+"&key="+key;
        map.put("sign",SignUtil.encryption(str).toUpperCase());
        return map;
    }

    public static String doPay(Map map) {
        return HttpRequest.post(ORDER_URL).form(map).timeout(10000).execute().body();
    }

}
