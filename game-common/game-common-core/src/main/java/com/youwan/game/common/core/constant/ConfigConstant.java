package com.youwan.game.common.core.constant;




public class ConfigConstant {


    public final static String DATE_FORMAT = "yyyy-MM-dd";
    public final static String MONTH_DATE_FORMAT = "MM-dd";
    public final static String YEAR_WEEK_FORMAT = "yyyy-ww";
    public final static String YEAR_MONTH_FORMAT = "yyyy-MM";
    public final static String YEAR_WEEK_FORMAT_SHORT = "yy-ww";
    public final static String YEAR_MONTH_FORMAT_SHORT = "yy-MM";
    public final static String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";
    public final static String FULL_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public final static String TIME_FORMAT = "HH:mm";
    public final static String MONTH_DAY_HOUR_MINUTE_FORMAT = "MM-dd HH:mm";
    public final static String LOCATE_DATE_FORMAT = "yyyyMMddHHmmss";
    public final static String THE_DATE_FORMAT = "yyyyMMdd";
    /**
     * 云存储配置KEY
     */
    public final static String CLOUD_STORAGE_CONFIG_KEY = "CLOUD_STORAGE_CONFIG_KEY";


    /**
     * 激活短信存储KEY
     */
    public final static String ACTIVAT_SMS_CONFIG_KEY = "ACTIVAT_SMS_CONFIG_KEY";


    /**
     * 二维码生成自定义信息
     */
    public final static int WIDTH = 300;
    public final static int HEIGHT = 300;
    public final static int MARGIN = 2;

    public static final int BLACK = 0xFF000000;
    public static final int WHITE = 0xFFFFFFFF;

    public enum PAY_RESULT {
        /**
         * 目录
         */
        SUCCESS("0", "请求支付成功!"),

        FAIL("1", "请求支付失败!");

        PAY_RESULT(String value, String description) {
            this.value = value;
            this.description = description;
        }

        /**
         * 类型
         */
        private String value;
        /**
         * 描述
         */
        private String description;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }


    public enum HANDLE_TYPE {
        /**
         * 目录
         */
        HAND(0, "手动处理"),

        SYSTEM(1, "系统处理"),

        RECHARGE(2, "手动上分");

        HANDLE_TYPE(int value, String description) {
            this.value = value;
            this.description = description;
        }

        /**
         * 类型
         */
        private int value;
        /**
         * 描述
         */
        private String description;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }



    public enum PAYORDR_STATUS {
        /**
         * 目录
         */
        WAIT("0", "等待支付"),

        SUCCESS("1", "支付成功"),

        FAIL("2", "支付失败");

        PAYORDR_STATUS(String value, String description) {
            this.value = value;
            this.description = description;
        }

        /**
         * 类型
         */
        private String value;
        /**
         * 描述
         */
        private String description;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }


    public enum CASHORDR_STATUS {
        /**
         * 目录
         */
        APPLY("0", "提现申请"),

        PASS("1", "审批通过"),

        FAIL("2", "审批失败");

        CASHORDR_STATUS(String value, String description) {
            this.value = value;
        }

        /**
         * 类型
         */
        private String value;
        /**
         * 描述
         */
        private String description;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }




    /**
     * 短信服务商
     */
    public enum ApproveStatus {
        APPLY_STATUS(0),

        SUCCESS_STATUS(1),

        FAIL_STATUS(2);

        private int value;

        ApproveStatus(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }



    /**
     * 短信服务商
     */
    public enum SmsService {
        ALIYUN(1),

        YUNXING(2),

        RONGLIAN(3);

        private int value;

        SmsService(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * 云服务商
     */
    public enum CloudService {
        /**
         * 七牛云
         */
        QINIU(1),
        /**
         * 阿里云
         */
        ALIYUN(2),
        /**
         * 腾讯云
         */
        QCLOUD(3);

        private int value;

        CloudService(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }



    public enum PlatformService {
        /**
         * BG
         */
        BG(1),
        /**
         * AG
         */
        AG(2);

        private int value;

        PlatformService(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}
