package com.youwan.game.common.core.exception;

import lombok.NoArgsConstructor;

/**
 * @author lion
 * @date 2018年06月22日16:22:03
 * 403 授权拒绝
 */
@NoArgsConstructor
public class GameDeniedException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public GameDeniedException(String message) {
		super(message);
	}

	public GameDeniedException(Throwable cause) {
		super(cause);
	}

	public GameDeniedException(String message, Throwable cause) {
		super(message, cause);
	}

	public GameDeniedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
