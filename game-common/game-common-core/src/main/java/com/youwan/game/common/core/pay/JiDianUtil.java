package com.youwan.game.common.core.pay;


import com.youwan.game.common.core.util.SignUtil;

import java.math.BigDecimal;
import java.util.*;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;
import static com.youwan.game.common.core.constant.UrlConstant.FEONT_SYSTM_URL;


public class JiDianUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/jidian";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "";


    public static Map<String, String> packaData(String... params) {
        SortedMap<String, String> map = new TreeMap<>();

        String merchantKey = params[6];
        //设置下单地址
        ORDER_URL = params[4];

        //商户订单号
        map.put("mch_code", params[0]);
        //订单号
        map.put("out_trade_no", params[1]);
        //支付金额
        map.put("amount", new BigDecimal(params[2]).multiply(new BigDecimal(100)).toString());

        //支付类型
        map.put("pay_type", params[3]);
        //支付金额  单位从元转成分

        //回调地址
        map.put("notify_url", API_SYSTM_URL + NOTIFY_URL);
        //产品名称
        //map.put("productName", params[5]);
        //如果是11
        if(params[3] != null && params[3].equals("11")) {
            map.put("bankName", params[7]);
        }
        //回调地址
        map.put("callback_url", FEONT_SYSTM_URL + CALL_BACK_URL + params[1]);

        //生成签名信息
        StringBuffer buffer = new StringBuffer();
        Set<Map.Entry<String, String>> set = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();

        while (iterator.hasNext()) {
            Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
            buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }

        String signSrc = buffer.append("key=").append(merchantKey).toString();
        map.put("sign", SignUtil.getSign(signSrc));
        return map;
    }

}
