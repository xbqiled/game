package com.youwan.game.common.core.otherplatform.bg.json.req;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserRedPacketReq implements Serializable {

    private String id;
    private String method = "open.red.packet.query";
    private UserRedPacketReq.Params params;
    private String jsonrpc;

    @Data
    public static class Params implements Serializable {
        private String random;
        private String sign;
        private String sn;

        private String account;
        private String userId;
        private String startTime;

        private String endTime;
        private Long pageIndex;
        private Long pageSize;
    }
}
