package com.youwan.game.common.core.pay.json;

import lombok.Data;

import java.io.Serializable;


@Data
public class ResultShunDa implements Serializable {


    private String retCode;

    private PayParams payParams;

    @lombok.Data
    public class PayParams implements Serializable {
        private String url;
        private String method;
    }

}
