package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;

@Data
public class RequestPlatformMaint implements Serializable {

    private int operatorType;
    private int start;
    private int finish;
}
