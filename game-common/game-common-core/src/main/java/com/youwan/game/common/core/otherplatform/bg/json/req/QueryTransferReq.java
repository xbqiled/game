package com.youwan.game.common.core.otherplatform.bg.json.req;


import lombok.Data;

import java.io.Serializable;


@Data
public class QueryTransferReq implements Serializable {

    private String id;
    private String mothod = "open.user.create";
    private Params params;
    private String jsonrpc;

    @Data
    public class Params implements Serializable {
        private String random;
        private String sign;

        private String sn;
        private String loginId;
        private String startTime;

        private String endTime;
        private String bizId;
        private String pageIndex;
        private String pageSize;
    }
}
