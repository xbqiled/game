package com.youwan.game.common.core.otherplatform.bg.json.req;

import lombok.Data;

import java.io.Serializable;


@Data
public class UserTransferReq implements Serializable {

    private String id;
    private String method = "open.balance.transfer";
    private Params params;
    private String jsonrpc;

    @Data
    public static class Params implements Serializable {
        private String random;
        private String sn;

        private String digest;
        private String loginId;
        private String amount;

        private String bizId;
        private String checkBizId = "1";
    }
}
