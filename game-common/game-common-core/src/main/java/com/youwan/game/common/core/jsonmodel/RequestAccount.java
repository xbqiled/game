package com.youwan.game.common.core.jsonmodel;

import lombok.Data;

import java.io.Serializable;


@Data
public class RequestAccount implements Serializable {

   private Integer accountID;

}
