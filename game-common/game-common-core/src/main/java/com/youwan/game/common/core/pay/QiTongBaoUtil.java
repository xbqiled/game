package com.youwan.game.common.core.pay;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.google.gson.Gson;
import com.youwan.game.common.core.pay.json.ResultQiTongBao;
import com.youwan.game.common.core.util.QtbUtil;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static com.youwan.game.common.core.constant.UrlConstant.API_SYSTM_URL;
import static com.youwan.game.common.core.constant.UrlConstant.FEONT_SYSTM_URL;

public class QiTongBaoUtil {

    //回调地址
    private static String NOTIFY_URL = "/notify/qiTongBao";
    //回调界面
    private static String CALL_BACK_URL = "/pay/success?orderId=";
    //下单接口
    private static String ORDER_URL = "";

    public static Map<String, String> packaData(String... params)  {
        String memberNumber =params[0]; //商户号
        String memberOrderNumber =params[1]; //商户订单号
        String tradeAmount =params[2]; //交易金额
        String defrayalType =params[3]; //支付方式
        String payGateway =params[4]; //支付网关
        String commodityBody =params[5]; //商品信息
        String miyao =params[6];  //商户秘钥
        String bankCode =params[7];   //银行代码
        String userId =params[8];   // url传来的userid
        String gongyao =params[9];   //商户公钥

        ORDER_URL = params[4];

        SortedMap<String, String> business = new TreeMap<>();
        business.put("memberOrderNumber", memberOrderNumber);
        business.put("tradeCheckCycle", "T1");
        business.put("orderTime", new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
        business.put("currenciesType", "CNY");

        //乘以100 启通宝是以分为单位的
        String payAmount = new BigDecimal(tradeAmount).multiply(new BigDecimal(100)).toString();
        business.put("tradeAmount", payAmount);
        business.put("commodityBody", "Onlinepay");
        business.put("commodityDetail", "Onlinepay");
        business.put("notifyUrl",API_SYSTM_URL + NOTIFY_URL);
        business.put("returnUrl", FEONT_SYSTM_URL + CALL_BACK_URL + params[1]);
        business.put("terminalIP", "8.8.8.8");
        business.put("defrayalType",defrayalType);
        String sign = "";
        try {
            sign = QtbUtil.sign(business, miyao);
        } catch (Exception e) {
            e.printStackTrace();
        }

        SortedMap common = new TreeMap();
        common.put("memberNumber", memberNumber);//商户号
        common.put("method", "UNIFIED_PAYMENT");//请求接口名称
        common.put("version", "V2.0.0");//版本号
        common.put("sign", sign);//签名
        common.put("content", business);//业务参数
        return common;
    }

    public static ResultQiTongBao doPay(Map map) {
       String bodyStr = new Gson().toJson(map);
       String resultStr =  HttpUtil.post(ORDER_URL, bodyStr);

        if (StrUtil.isNotBlank(resultStr) && JSONUtil.isJson(resultStr)) {
            return new Gson().fromJson(resultStr, ResultQiTongBao.class);
        }
        return  null;
    }

}
