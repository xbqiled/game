package com.youwan.game.common.core.jsonmodel;


import lombok.Data;

import java.io.Serializable;


@Data
public class GsonGameState implements Serializable {

    private int status;
    private long finish;
    private int id;
    private long start;
    private int num;

}
