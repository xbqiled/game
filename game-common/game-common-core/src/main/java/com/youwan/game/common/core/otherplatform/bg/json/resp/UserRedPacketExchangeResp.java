package com.youwan.game.common.core.otherplatform.bg.json.resp;


import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class UserRedPacketExchangeResp implements Serializable {

    public String id;
    public RedPacketExchangeData result;

    @Data
    public static class RedPacketExchangeData {
        public Long total;
        public Long pageIndex;
        public Long pageSize;

        public String etag;
        public List<UserRedPacketExchangeResp.RedPacketExchangeData.RedPacketExchange> items;

        @Data
        public static class RedPacketExchange {
            public long pid;
            public String sn;
            public long uid;


            public String account;
            public float balance;
            public float amount;
            public String time;
        }
    }
}

