package com.youwan.game.common.log.event;


import com.youwan.game.admin.api.entity.SysLog;
import org.springframework.context.ApplicationEvent;

/**
 * @author lion
 * 系统日志事件
 */
public class SysLogEvent extends ApplicationEvent {

	public SysLogEvent(SysLog source) {
		super(source);
	}
}
