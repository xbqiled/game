package com.youwan.game.common.transaction;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *@author LCN on 2017/6/26.
 *
 * @author LCN
 * @since 4.1.0
 */

@Configuration
@ComponentScan({"com.codingapi.tx", "com.youwan.game.common.transaction"})
public class TransactionConfiguration {


}
