package com.youwan.game.common.data.core;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by terry on 2018/1/22.
 */
@Data
public abstract class BaseEntity implements Serializable {

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date createAt = new Date();

}
