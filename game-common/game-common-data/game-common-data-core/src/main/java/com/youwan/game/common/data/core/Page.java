package com.youwan.game.common.data.core;

import java.util.List;
import java.util.Map;

public interface Page<T> {

    long getTotal();

    void setTotal(long total);

    List<T> getRecords();

    void setRecords(List<T> records);

    void setAttach(Map<String, Object> attach);

    Map<String, Object> getAttach();

    static <T> Page<T> empty() {
        return new PageImpl<>();
    }

    static <T> Page<T> newPage(long total, List<T> records) {
        return new PageImpl<>(total, records);
    }
}
