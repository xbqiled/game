package com.youwan.game.common.data.core;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * 通用分页结果
 * Created by Administrator on 2018/1/22 0022.
 */
public class PageImpl<T> implements Page<T>, Serializable {

    @Getter
    @Setter
    private long total;

    @Getter
    @Setter
    private List<T> records;

    @Getter
    @Setter
    private Map<String, Object> attach;

    /**
     * 构造方法，只构造空页.
     */
    public PageImpl() {
        this(0, Collections.emptyList());
    }

    public PageImpl(long total, List<T> records) {
        this.total = total;
        this.records = records;
    }

}
