package com.youwan.game.common.data.elasticsearch.operations;

import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;

import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Administrator on 2017/6/13.
 */
public class DeleteByQueryOperation {

    private QueryBuilder query;
    private List<String> indices;
    private List<String> types;
    private boolean abortOnVersionConflict;
    private String routing;
    private TimeValue scroll;
    private int slices = 1;
    private boolean refresh;
    private TimeValue timeout = TimeValue.timeValueMinutes(1);

    public DeleteByQueryOperation(final List<String> indices,
                                  final List<String> types,
                                  final QueryBuilder query) {
        checkNotNull(query, "query object is required");
        checkArgument(indices != null && !indices.isEmpty(), "indices is empty");
        checkArgument(types != null && !types.isEmpty(), "types is empty");
        this.query = query;
        this.indices = indices;
        this.types = types;
    }

    public QueryBuilder getQuery() {
        return query;
    }

    public DeleteByQueryOperation setQuery(QueryBuilder query) {
        this.query = query;
        return this;
    }

    public List<String> getIndices() {
        return indices;
    }

    public DeleteByQueryOperation setIndices(List<String> indices) {
        this.indices = indices;
        return this;
    }

    public List<String> getTypes() {
        return types;
    }

    public DeleteByQueryOperation setTypes(List<String> types) {
        this.types = types;
        return this;
    }

    public boolean getAbortOnVersionConflict() {
        return abortOnVersionConflict;
    }

    public DeleteByQueryOperation setAbortOnVersionConflict(boolean abortOnVersionConflict) {
        this.abortOnVersionConflict = abortOnVersionConflict;
        return this;
    }

    public String getRouting() {
        return routing;
    }

    public DeleteByQueryOperation setRouting(String routing) {
        this.routing = routing;
        return this;
    }

    public TimeValue getScroll() {
        return scroll;
    }

    public DeleteByQueryOperation setScroll(TimeValue scroll) {
        this.scroll = scroll;
        return this;
    }

    public int getSlices() {
        return slices;
    }

    public DeleteByQueryOperation setSlices(int slices) {
        this.slices = slices;
        return this;
    }

    public boolean getRefresh() {
        return refresh;
    }

    public DeleteByQueryOperation setRefresh(boolean refresh) {
        this.refresh = refresh;
        return this;
    }

    public TimeValue getTimeout() {
        return timeout;
    }

    public DeleteByQueryOperation setTimeout(TimeValue timeout) {
        this.timeout = timeout;
        return this;
    }
}
