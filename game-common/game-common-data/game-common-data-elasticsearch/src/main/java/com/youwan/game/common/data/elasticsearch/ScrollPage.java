package com.youwan.game.common.data.elasticsearch;


import com.youwan.game.common.data.core.Page;

import java.util.List;

public interface ScrollPage<T> extends Page<T> {

    String getScrollId();

    static <T> ScrollPage<T> empty() {
        return new ScrollPageImpl<>(null);
    }

    static <T> ScrollPage<T> newScrollPage(long total, List<T> records, String scrollId) {
        return new ScrollPageImpl<>(total, records, scrollId);
    }
}
