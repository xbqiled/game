package com.youwan.game.common.data.elasticsearch.configuration;

import org.elasticsearch.client.RestClientBuilder;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-04-22.
 */
@FunctionalInterface
public interface RestClientBuilderCustomizer {

    /**
     * Customize the {@link RestClientBuilder}.
     * @param builder the builder to customize
     */
    void customize(RestClientBuilder builder);

}
