package com.youwan.game.common.data.elasticsearch;

import com.youwan.game.common.core.util.ThreeConsumer;
import com.youwan.game.common.data.core.BaseEntity;
import com.youwan.game.common.data.core.Page;
import com.youwan.game.common.data.elasticsearch.operations.*;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.script.Script;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.Sum;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.youwan.game.common.data.elasticsearch.operations.OperationsSupport.*;


public class ElasticsearchTemplate implements ElasticsearchOperations {

    private Client client;

    public ElasticsearchTemplate(Client client) {
        this.client = client;
    }

    @Override
    public Client getClient() {
        return client;
    }

    @Override
    public String index(IndexOperation<?> indexOperation) {
        try {
            return client.index(indexRequest(indexOperation), RequestOptions.DEFAULT).getId();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public CompletableFuture<String> indexAsync(IndexOperation<?> indexOperation) {
        return createFuture(client::indexAsync, indexRequest(indexOperation), rep -> ((IndexResponse) rep).getId());
    }

    @Override
    public <T> String index(String index, String type, String id, T source) {
        return index(new IndexOperation<>(index, type, id).setSource(source));
    }

    @Override
    public <T> CompletableFuture<String>
    indexAsync(String index, String type, String id, T source) {
        return indexAsync(new IndexOperation<>(index, type, id).setSource(source));
    }

    @Override
    public <T> String index(String index, String type, T source) {
        return index(new IndexOperation<>(index, type, source));
    }

    @Override
    public <T> CompletableFuture<String> indexAsync(String index, String type, T source) {
        return indexAsync(new IndexOperation<>(index, type, source));
    }

    @Override
    public void bulkIndex(List<IndexOperation<?>> indexOperationQueries) {
        BulkRequest request = new BulkRequest().add(indexOperationQueries.stream().map(OperationsSupport::indexRequest)
                .collect(Collectors.toList()).toArray(new IndexRequest[]{}));
        try {
            client.bulk(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public CompletableFuture<Void> bulkIndexAsync(List<IndexOperation<?>> indexOperationQueries) {
        return createFuture(client::bulkAsync, new BulkRequest().add(indexOperationQueries.stream()
                .map(OperationsSupport::indexRequest)
                .collect(Collectors.toList()).toArray(new IndexRequest[]{})), rep -> null);
    }

    @Override
    public String delete(String index, String type, String id, String routing) {
        try {
            return client.delete(new DeleteRequest(index, type, id).routing(routing), RequestOptions.DEFAULT).getId();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public CompletableFuture<String> deleteAsync(String index, String type, String id, String routing) {
        return createFuture(client::deleteAsync, new DeleteRequest(index, type, id).routing(routing)
                , rep -> ((DeleteResponse) rep).getId());
    }

    @Override
    public void deleteByQuery(DeleteByQueryOperation deleteByQueryOperation) {
        try {
            client.deleteByQuery(deleteByQueryRequest(deleteByQueryOperation), RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public CompletableFuture<Void> deleteByQueryAsync(DeleteByQueryOperation deleteByQueryOperation) {
        return createFuture(client::deleteByQueryAsync, deleteByQueryRequest(deleteByQueryOperation), rep -> null);
    }

    @Override
    public <T> T get(GetOperation<T> getOperation) {
        try {
            return convertSingle(client.get(getRequest(getOperation),
                    RequestOptions.DEFAULT), getOperation.getSourceClass());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> CompletableFuture<T>
    getAsync(GetOperation<T> getOperation) {
        return createFuture(client::getAsync, getRequest(getOperation),
                rep -> convertSingle((GetResponse) rep, getOperation.getSourceClass()));
    }

    @Override
    public <T> T get(String index, String type, String id, Class<T> sourceClass) {
        return get(new GetOperation<>(index, type, id, sourceClass));
    }

    @Override
    public <T> CompletableFuture<T>
    getAsync(String index, String type, String id, Class<T> sourceClass) {
        return getAsync(new GetOperation<>(index, type, id, sourceClass));
    }

    @Override
    public <T> List<T> multiGet(MultiGetOperation<T> multiGetOperation) {
        try {
            MultiGetResponse response = client.mget(multiGetRequest(multiGetOperation), RequestOptions.DEFAULT);
            return convertToList(response, multiGetOperation.getSourceClass());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> CompletableFuture<List<T>>
    multiGetAsync(final MultiGetOperation<T> multiGetOperation) {
        return createFuture(client::mgetAsync, multiGetRequest(multiGetOperation),
                rep -> convertToList((MultiGetResponse) rep, multiGetOperation.getSourceClass()));
    }

    @Override
    public void update(UpdateOperation<?> updateOperation) {
        try {
            client.update(updateRequest(updateOperation), RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> void update(String index, String type, String id, T source) {
        update(new UpdateOperation<>(index, type, id).setSource(source));
    }

    @Override
    public void update(String index, String type, String id, Script script) {
        update(new UpdateOperation<>(index, type, id).setScript(script));
    }

    @Override
    public CompletableFuture<Void> updateAsync(UpdateOperation<?> updateOperation) {
        return createFuture(client::updateAsync, updateRequest(updateOperation), rep -> null);
    }

    @Override
    public <T> CompletableFuture<Void>
    updateAsync(String index, String type, String id, T source) {
        return updateAsync(new UpdateOperation<>(index, type, id).setSource(source));
    }

    @Override
    public CompletableFuture<Void> updateAsync(String index, String type, String id, Script script) {
        return updateAsync(new UpdateOperation<>(index, type, id).setScript(script));
    }


    @Override
    public void bulkUpdate(List<UpdateOperation<?>> updateOps) {
        BulkRequest request = new BulkRequest().add(updateOps.stream().map(OperationsSupport::updateRequest)
                .collect(Collectors.toList()).toArray(new UpdateRequest[]{}));
        try {
            client.bulk(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public CompletableFuture<Void> bulkUpdateAsync(List<UpdateOperation<?>> updateOps) {
        return createFuture(client::bulkAsync, new BulkRequest()
                .add(updateOps.stream().map(OperationsSupport::updateRequest)
                        .collect(Collectors.toList()).toArray(new UpdateRequest[]{})), rep -> null);
    }

    @Override
    public void updateByQuery(UpdateByQueryOperation operation) {
        try {
            client.updateByQuery(updateByQueryRequest(operation), RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public CompletableFuture<Void> updateByQueryAsync(UpdateByQueryOperation operation) {
        return createFuture(client::updateByQueryAsync, updateByQueryRequest(operation), rep -> null);
    }

    @Override
    public <T> T searchOne(SearchOperation<T> operation) {
        try {
            SearchResponse response = client.search(
                    searchRequest(operation.setFrom(0).setSize(1)), RequestOptions.DEFAULT);
            return convertSingle(response, operation.getSourceClass());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> CompletableFuture<T> searchOneAsync(SearchOperation<T> operation) {
        return createFuture(client::searchAsync, searchRequest(operation.setFrom(0).setSize(1)),
                rep -> convertSingle((SearchResponse) rep, operation.getSourceClass()));
    }

    @Override
    public <T> List<T> search(SearchOperation<T> operation) {
        try {
            SearchResponse response = client.search(
                    searchRequest(operation), RequestOptions.DEFAULT);
            return convertToList(response, operation.getSourceClass());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> CompletableFuture<List<T>> searchAsync(SearchOperation<T> operation) {
        return createFuture(client::searchAsync, searchRequest(operation),
                rep -> convertToList((SearchResponse) rep, operation.getSourceClass()));
    }

    @Override
    public <T> Page<T> searchForPage(SearchOperation<T> operation) {
        try {
            SearchResponse response = client.search(
                    searchRequest(operation), RequestOptions.DEFAULT);
            return convertToPage(response, operation.getSourceClass());
        } catch (IOException e) {//Miscellaneous APIs
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> CompletableFuture<Page<T>> searchForPageAsync(SearchOperation<T> operation) {
        return createFuture(client::searchAsync, searchRequest(operation),
                rep -> convertToPage((SearchResponse) rep, operation.getSourceClass()));
    }

    @Override
    public <T> ScrollPage<T> startScroll(SearchOperation<T> operation, long scrollTimeInMillis) {
        try {
            return convertToScrollPage(
                    client.search(searchRequest(operation).scroll(TimeValue.timeValueMillis(scrollTimeInMillis)),
                            RequestOptions.DEFAULT), operation.getSourceClass());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> long count(SearchOperation<T> operation) {

        try {
            return client.search(countRequest(operation), RequestOptions.DEFAULT).getHits().getTotalHits().value;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> CompletableFuture<Long> countAsync(SearchOperation<T> operation) {
        return createFuture(client::searchAsync, countRequest(operation),
                rep -> getCount((SearchResponse) rep));
    }

    @Override
    public Map<String, Long> termsBucketCount(SearchOperation<?> operation, String field, int size) {
        try {
            SearchResponse response = client.search(termsBucketCountRequest(operation, field, size), RequestOptions.DEFAULT);
            return getTermsBucketCount(response, field);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public CompletableFuture<Map<String, Long>> termsBucketCountAsync(SearchOperation<?> operation, String field, int size) {
        return createFuture(client::searchAsync, termsBucketCountRequest(operation, field, size),
                rep -> getTermsBucketCount((SearchResponse) rep, field));
    }

    @Override
    public Map<String, Long> termsBucketCount(SearchOperation<?> operation, String field) {
        return termsBucketCount(operation, field, 0);
    }

    @Override
    public CompletableFuture<Map<String, Long>> termsBucketCountAsync(SearchOperation<?> operation, String field) {
        return termsBucketCountAsync(operation, field, 0);
    }

    @Override
    public double sum(SearchOperation<?> operation, String field) {
        try {
            return ((Sum) client.search(sumRequest(operation, field), RequestOptions.DEFAULT).getAggregations().get(field)).getValue();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public CompletableFuture<Double> sumAsync(SearchOperation<?> operation, String field) {
        return createFuture(client::searchAsync, sumRequest(operation, field),
                rep -> ((Sum) ((SearchResponse) rep).getAggregations().get(field)).getValue());
    }


    @Override
    public Map<String, Double> termsBucketSum(SearchOperation<?> operation, String bucketField, String sumField) {
        return termsBucketSum(operation, bucketField, sumField, 0);
    }

    @Override
    public Map<String, Double> termsBucketSum(SearchOperation<?> operation, String bucketField, String sumField, int bucketSize) {
        try {
            SearchResponse response = client.search(termsBucketSumRequest(operation, bucketField, sumField, bucketSize), RequestOptions.DEFAULT);
            return getTermsBucketSum(response, bucketField, sumField);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public CompletableFuture<Map<String, Double>> termsBucketSumAsync(SearchOperation<?> operation, String bucketField, String sumField) {
        return termsBucketSumAsync(operation, bucketField, sumField, 0);
    }

    @Override
    public CompletableFuture<Map<String, Double>> termsBucketSumAsync(SearchOperation<?> operation, String bucketField, String sumField,
                                                                      int bucketSize) {
        return createFuture(client::searchAsync, termsBucketSumRequest(operation, bucketField, sumField, bucketSize),
                rep -> getTermsBucketSum((SearchResponse) rep, bucketField, sumField));
    }

    @Override
    public <T> CompletableFuture<ScrollPage<T>>
    startScrollAsync(SearchOperation<T> operation, long scrollTimeInMillis) {
        return createFuture(client::searchAsync, searchRequest(operation)
                        .scroll(TimeValue.timeValueMillis(scrollTimeInMillis)),
                rep -> convertToScrollPage((SearchResponse) rep, operation.getSourceClass()));
    }

    @Override
    public <T> ScrollPage<T>
    continueScroll(String scrollId, long scrollTimeInMillis, Class<T> sourceClass) {
        SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId)
                .scroll(TimeValue.timeValueMillis(scrollTimeInMillis));
        try {
            return convertToScrollPage(client.scroll(scrollRequest, RequestOptions.DEFAULT), sourceClass);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> CompletableFuture<ScrollPage<T>>
    continueScrollAsync(String scrollId, long scrollTimeInMillis, Class<T> sourceClass) {
        return createFuture(client::scrollAsync, new SearchScrollRequest(scrollId)
                        .scroll(TimeValue.timeValueMillis(scrollTimeInMillis)),
                rep -> convertToScrollPage((SearchResponse) rep, sourceClass));
    }

    @Override
    public void clearScroll(String scrollId) {
        ClearScrollRequest request = new ClearScrollRequest();
        request.addScrollId(scrollId);
        try {
            client.clearScroll(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public CompletableFuture<Void> clearScrollAsync(String scrollId) {
        ClearScrollRequest request = new ClearScrollRequest();
        request.addScrollId(scrollId);
        return createFuture(client::clearScrollAsync, request, rep -> null);
    }

    private <Request, Response, Result> CompletableFuture<Result> createFuture(
            final ThreeConsumer<Request, RequestOptions, ActionListener> action,
            final Request request,
            final Function<Response, Result> responseConvert) {

        CompletableFuture<Result> future = new CompletableFuture<>();
        action.accept(request, RequestOptions.DEFAULT, new ActionListener<Response>() {
            @Override
            public void onResponse(Response response) {
                future.complete(responseConvert.apply(response));
            }

            @Override
            public void onFailure(Exception e) {
                future.completeExceptionally(e);
            }
        });
        return future;
    }
}
