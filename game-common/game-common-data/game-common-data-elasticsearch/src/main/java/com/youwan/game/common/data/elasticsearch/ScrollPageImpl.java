package com.youwan.game.common.data.elasticsearch;


import com.youwan.game.common.data.core.PageImpl;

import java.util.List;

public class ScrollPageImpl<T> extends PageImpl<T> implements ScrollPage<T> {

    private String scrollId;

    public ScrollPageImpl(String scrollId) {
        this.scrollId = scrollId;
    }

    public ScrollPageImpl(long total, List<T> records, String scrollId) {
        super(total, records);
        this.scrollId = scrollId;
    }

    @Override
    public String getScrollId() {
        return this.scrollId;
    }
}
