package com.youwan.game.common.data.elasticsearch.operations;

import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.script.Script;

import java.util.ArrayList;
import java.util.List;

public class UpdateByQueryOperation {

    private QueryBuilder query;
    private List<String> indices = new ArrayList<>();
    private List<String> types = new ArrayList<>();
    private boolean abortOnVersionConflict;
    private String routing;
    private TimeValue scroll;
    private int slices = 1;
    private boolean refresh;
    private TimeValue timeout = TimeValue.timeValueMinutes(1);
    private Script script;
    private int maxRetries = 11;
    private String preference;

    public QueryBuilder getQuery() {
        return query;
    }

    public UpdateByQueryOperation(String index, String type, QueryBuilder query, Script script) {
        this.indices.add(index);
        this.types.add(type);
        this.script = script;
        this.query = query;
    }

    public UpdateByQueryOperation(List<String> indices, List<String> types, QueryBuilder query, Script script) {
        this.indices = indices;
        this.types = types;
        this.script = script;
        this.query = query;
    }

    public UpdateByQueryOperation setQuery(QueryBuilder query) {
        this.query = query;
        return this;
    }

    public List<String> getIndices() {
        return indices;
    }

    public UpdateByQueryOperation setIndices(List<String> indices) {
        this.indices = indices;
        return this;
    }

    public List<String> getTypes() {
        return types;
    }

    public UpdateByQueryOperation setTypes(List<String> types) {
        this.types = types;
        return this;
    }

    public boolean getAbortOnVersionConflict() {
        return abortOnVersionConflict;
    }

    public UpdateByQueryOperation setAbortOnVersionConflict(boolean abortOnVersionConflict) {
        this.abortOnVersionConflict = abortOnVersionConflict;
        return this;
    }

    public String getRouting() {
        return routing;
    }

    public UpdateByQueryOperation setRouting(String routing) {
        this.routing = routing;
        return this;
    }

    public TimeValue getScroll() {
        return scroll;
    }

    public UpdateByQueryOperation setScroll(TimeValue scroll) {
        this.scroll = scroll;
        return this;
    }

    public int getSlices() {
        return slices;
    }

    public UpdateByQueryOperation setSlices(int slices) {
        this.slices = slices;
        return this;
    }

    public boolean getRefresh() {
        return refresh;
    }

    public UpdateByQueryOperation setRefresh(boolean refresh) {
        this.refresh = refresh;
        return this;
    }

    public TimeValue getTimeout() {
        return timeout;
    }

    public UpdateByQueryOperation setTimeout(TimeValue timeout) {
        this.timeout = timeout;
        return this;
    }

    public Script getScript() {
        return script;
    }

    public UpdateByQueryOperation setScript(Script script) {
        this.script = script;
        return this;
    }

    public int getMaxRetries() {
        return maxRetries;
    }

    public UpdateByQueryOperation setMaxRetries(int maxRetries) {
        this.maxRetries = maxRetries;
        return this;
    }

    public String getPreference() {
        return preference;
    }

    public UpdateByQueryOperation setPreference(String preference) {
        this.preference = preference;
        return this;
    }
}
