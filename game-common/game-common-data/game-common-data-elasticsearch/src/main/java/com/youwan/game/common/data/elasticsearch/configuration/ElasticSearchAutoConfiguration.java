package com.youwan.game.common.data.elasticsearch.configuration;

import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import com.youwan.game.common.data.elasticsearch.ReactiveElasticsearchTemplate;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.settings.Settings;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.context.properties.PropertyMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.youwan.game.common.data.elasticsearch.Client;
import org.springframework.context.annotation.DependsOn;

@Configuration
@ConditionalOnClass(Client.class)
@EnableConfigurationProperties(RestClientProperties.class)
public class ElasticSearchAutoConfiguration {
    private static final int KEEP_ALIVE_MS = 20 * 60 * 1000; // 20 minutes
    private RestClientProperties properties;

    public ElasticSearchAutoConfiguration(RestClientProperties properties) {
        this.properties = properties;
    }

    @Bean("esClient")
    @DependsOn("restHighLevelClient")
    public Client client(RestHighLevelClient restHighLevelClient) {
        return new Client(Settings.EMPTY, restHighLevelClient);
    }

    //@Bean
    public RestClientBuilder restClientBuilder() {
        HttpHost[] hosts = this.properties.getUris().stream().map(HttpHost::create)
                .toArray(HttpHost[]::new);
        RestClientBuilder builder = RestClient.builder(hosts);
        PropertyMapper map = PropertyMapper.get();
        map.from(this.properties::getUsername).whenHasText().to((username) -> {
            CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            Credentials credentials = new UsernamePasswordCredentials(
                    this.properties.getUsername(), this.properties.getPassword());
            credentialsProvider.setCredentials(AuthScope.ANY, credentials);
            builder.setHttpClientConfigCallback((httpClientBuilder) -> httpClientBuilder
                    .setDefaultCredentialsProvider(credentialsProvider).setKeepAliveStrategy(((response, context) -> KEEP_ALIVE_MS)));
        });
        return builder;
    }

    @Bean
    public RestHighLevelClient restHighLevelClient() {
        return new RestHighLevelClient(restClientBuilder());
    }

    @Bean
    @DependsOn("esClient")
    public ElasticsearchTemplate elasticsearchTemplate(Client client) {
        return new ElasticsearchTemplate(client);
    }

    @Bean
    public ReactiveElasticsearchTemplate reactiveElasticsearchTemplate(ElasticsearchTemplate elasticsearchTemplate) {
        return new ReactiveElasticsearchTemplate(elasticsearchTemplate);
    }
}
