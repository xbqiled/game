package com.youwan.game.common.data.elasticsearch.operations;

import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.common.unit.TimeValue;

import static org.elasticsearch.action.support.WriteRequest.RefreshPolicy.NONE;

public class IndexOperation<T> {

    private String id;
    private String index;
    private String type;
    private String routing;
    private Long version;
    private String parent;
    private T source;
    private DocWriteRequest.OpType opType = DocWriteRequest.OpType.INDEX;
    private TimeValue timeout = TimeValue.timeValueMinutes(1);
    private WriteRequest.RefreshPolicy refreshPolicy = NONE;
    private String pipeline;

    public IndexOperation(String index) {
        this.index = index;
    }

    public IndexOperation(String index, String type, String id) {
        this.id = id;
        this.index = index;
        this.type = type;
    }

    public IndexOperation(String index, String type) {
        this.index = index;
        this.type = type;
    }

    public IndexOperation(String index, String type, T source) {
        //this.id = id;
        this.source = source;
        try {
            this.id = OperationsSupport.getIdStrValue(source);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        this.index = index;
        this.type = type;
    }

    public IndexOperation<T> create(boolean create) {
        if (create) {
            return this.setOpType(DocWriteRequest.OpType.CREATE);
        } else {
            return this.setOpType(DocWriteRequest.OpType.INDEX);
        }
    }

    public String getId() {
        return id;
    }

    public IndexOperation<T> setId(String id) {
        this.id = id;
        return this;
    }

    public String getIndex() {
        return index;
    }

    public IndexOperation<T> setIndex(String index) {
        this.index = index;
        return this;
    }

    public String getType() {
        return type;
    }

    public IndexOperation<T> setType(String type) {
        this.type = type;
        return this;
    }

    public String getRouting() {
        return routing;
    }

    public IndexOperation<T> setRouting(String routing) {
        this.routing = routing;
        return this;
    }

    public Long getVersion() {
        return version;
    }

    public IndexOperation<T> setVersion(Long version) {
        this.version = version;
        return this;
    }

    public String getParent() {
        return parent;
    }

    public IndexOperation<T> setParent(String parent) {
        this.parent = parent;
        return this;
    }

    public T getSource() {
        return source;
    }

    public IndexOperation<T> setSource(T source) {
        this.source = source;
        if (this.id == null) {
            try {
                this.id = OperationsSupport.getIdStrValue(source);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return this;
    }

    public DocWriteRequest.OpType getOpType() {
        return opType;
    }

    public IndexOperation<T> setOpType(DocWriteRequest.OpType opType) {
        this.opType = opType;
        return this;
    }

    public TimeValue getTimeout() {
        return timeout;
    }

    public IndexOperation<T> setTimeout(TimeValue timeout) {
        this.timeout = timeout;
        return this;
    }

    public WriteRequest.RefreshPolicy getRefreshPolicy() {
        return refreshPolicy;
    }

    public IndexOperation<T> setRefreshPolicy(WriteRequest.RefreshPolicy refreshPolicy) {
        this.refreshPolicy = refreshPolicy;
        return this;
    }

    public String getPipeline() {
        return pipeline;
    }

    public IndexOperation<T> setPipeline(String pipeline) {
        this.pipeline = pipeline;
        return this;
    }

}
