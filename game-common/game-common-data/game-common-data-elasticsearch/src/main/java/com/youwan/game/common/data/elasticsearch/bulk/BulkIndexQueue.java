package com.youwan.game.common.data.elasticsearch.bulk;

import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import com.youwan.game.common.data.elasticsearch.operations.IndexOperation;
import com.youwan.game.common.data.elasticsearch.util.BatchTakeQueue;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.common.settings.Settings;

import java.util.List;
import java.util.Objects;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-04-20.
 */
@Slf4j
public class BulkIndexQueue extends BatchTakeQueue<IndexOperation<?>> {

    private ElasticsearchTemplate elasticsearchTemplate;

    public BulkIndexQueue(Settings settings, ElasticsearchTemplate elasticsearchTemplate) {
        super(settings);
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    @Override
    public void take(List<IndexOperation<?>> lst) {
        elasticsearchTemplate.bulkIndexAsync(lst).whenCompleteAsync((v, t) -> {
            //If any exception occurs, add back the items that has been taken out into the queue
            if (Objects.nonNull(t)) {
                log.error("bulk index failure, count:[{}], reason:[{}]", lst.size(), t.getMessage());
                BulkIndexQueue.this.addAll(lst);
            }
        });
    }
}
