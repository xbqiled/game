package com.youwan.game.common.data.shardingalgorithm;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-05-08.
 */
public class ByDayShardingAlgorithm extends AbstractTimeRangeShardingAlgorithm {

    @Override
    String timeFormatPattern() {
        return "yyyyMMdd";
    }

    @Override
    String timeExtractPattern() {
        return "(.+)(\\d{8})";
    }

    @Override
    BiFunction<DateTime, DateTime, Integer> timesBetweenFunction() {
        return (start, end)
                -> Days.daysBetween(start, end).getDays();
    }

    @Override
    BiFunction<DateTime, Integer, DateTime> timePlusFunction() {
        return DateTime::plusDays;
    }

    @Override
    Function<DateTime, DateTime> topTimeFunction() {
        return start -> start.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0);
    }

    public static void main(String[] args) {
        /*Collection<String> availableTargetNames = Arrays.asList("xxx20200301", "xxx20200302", "xxx20200303", "xxx20200304");
        ByDayShardingAlgorithm byDayShardingAlgorithm = new ByDayShardingAlgorithm();
        Collection<String> ret = byDayShardingAlgorithm.doSharding(availableTargetNames, new RangeShardingValue<>("xxx", "time",
                Range.greaterThan(1583150725)));
        System.err.println(ret);*/

        DateTimeZone.getAvailableIDs().forEach(System.err::println);
    }
}
