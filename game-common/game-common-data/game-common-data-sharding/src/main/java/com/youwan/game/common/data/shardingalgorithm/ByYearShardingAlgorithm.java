package com.youwan.game.common.data.shardingalgorithm;

import com.google.common.collect.Range;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingValue;
import org.joda.time.DateTime;
import org.joda.time.Years;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-05-08.
 */
public class ByYearShardingAlgorithm extends AbstractTimeRangeShardingAlgorithm {
    @Override
    String timeFormatPattern() {
        return "yyyy";
    }

    @Override
    String timeExtractPattern() {
        return "(.+)(\\d{4})";
    }

    @Override
    BiFunction<DateTime, DateTime, Integer> timesBetweenFunction() {
        return (start, end)
                -> Years.yearsBetween(start, end).getYears();
    }

    @Override
    BiFunction<DateTime, Integer, DateTime> timePlusFunction() {
        return DateTime::plusYears;
    }

    @Override
    Function<DateTime, DateTime> topTimeFunction() {
        return start -> start.withMonthOfYear(1).withDayOfMonth(1).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0);
    }

    public static void main(String[] args) {
        Collection<String> availableTargetNames = Arrays.asList("xxx2020", "xxx2021", "xxx2022", "xxx2023", "xxx2024", "xxx2025");
        ByYearShardingAlgorithm byDayShardingAlgorithm = new ByYearShardingAlgorithm();
        Collection<String> ret = byDayShardingAlgorithm.doSharding(availableTargetNames, new RangeShardingValue<>("xxx", "time",
                Range.closed(1596369925, 1727870725)));
        System.err.println(ret);
    }
}
