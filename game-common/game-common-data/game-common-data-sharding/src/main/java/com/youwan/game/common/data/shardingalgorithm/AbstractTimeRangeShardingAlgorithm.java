package com.youwan.game.common.data.shardingalgorithm;

import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingValue;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-05-08.
 */
public abstract class AbstractTimeRangeShardingAlgorithm implements PreciseShardingAlgorithm<Integer>, RangeShardingAlgorithm<Integer> {

    @Override
    public String doSharding(Collection<String> availableTargetNames, PreciseShardingValue<Integer> shardingValue) {
        int value = shardingValue.getValue();
        DateTime d = new DateTime(value * 1000L);
        return shardingValue.getLogicTableName() + d.toString(timeFormatPattern());
    }

    @Override
    public Collection<String> doSharding(Collection<String> availableTargetNames, RangeShardingValue<Integer> shardingValue) {
        DateTime[] minAndMaxTime = fetchMinAndMaxTime(availableTargetNames);
        DateTime start = fetchStart(minAndMaxTime[0], minAndMaxTime[1], shardingValue);
        DateTime end = fetchEnd(minAndMaxTime[0], minAndMaxTime[1], shardingValue);
        if (start.toDate().getTime() > end.toDate().getTime()) {
            throw new IllegalArgumentException("invalid time range");
        }
        int interval = timesBetweenFunction().apply(start, end);
        Set<String> ret = new HashSet<>();
        for (int i = 0; i <= interval; i++) {
            ret.add(shardingValue.getLogicTableName() + timePlusFunction().apply(start, i).toString(timeFormatPattern()));
        }
        return ret;
    }

    private DateTime[] fetchMinAndMaxTime(Collection<String> availableTargetNames) {
        List<Integer> dayStream =
                availableTargetNames.stream()
                        .map(s -> Integer.parseInt(s.replaceAll(timeExtractPattern(), "$2"))).collect(Collectors.toList());
        int min = dayStream.stream().mapToInt(item -> item).min().getAsInt();
        int max = dayStream.stream().mapToInt(item -> item).max().getAsInt();
        DateTime s = DateTime.parse(Integer.toString(min), DateTimeFormat.forPattern(timeFormatPattern()));
        DateTime e = DateTime.parse(Integer.toString(max), DateTimeFormat.forPattern(timeFormatPattern()));
        return new DateTime[]{s, e};
    }


    private DateTime fetchStart(DateTime minTime, DateTime maxTime, RangeShardingValue<Integer> rangeShardingValue) {
        if (!rangeShardingValue.getValueRange().hasLowerBound())
            return minTime;
        DateTime start = new DateTime(rangeShardingValue.getValueRange().lowerEndpoint() * 1000L);
        if (start.compareTo(minTime) < 0) {
            return minTime;
        }
        if (start.compareTo(maxTime) > 0) {
            return maxTime;
        }
        return topTimeFunction().apply(start);
    }

    private DateTime fetchEnd(DateTime minTime, DateTime maxTime, RangeShardingValue<Integer> rangeShardingValue) {
        if (!rangeShardingValue.getValueRange().hasUpperBound())
            return maxTime;
        DateTime end = new DateTime(rangeShardingValue.getValueRange().upperEndpoint() * 1000L);
        if (end.compareTo(minTime) < 0) {
            return minTime;
        }
        if (end.compareTo(maxTime) > 0) {
            return maxTime;
        }
        return end;
    }

    abstract String timeFormatPattern();

    abstract String timeExtractPattern();

    abstract BiFunction<DateTime, DateTime, Integer> timesBetweenFunction();

    abstract BiFunction<DateTime, Integer, DateTime> timePlusFunction();

    abstract Function<DateTime,DateTime> topTimeFunction();
}
