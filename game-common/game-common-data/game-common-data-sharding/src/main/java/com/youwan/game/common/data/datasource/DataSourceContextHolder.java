package com.youwan.game.common.data.datasource;

import com.alibaba.ttl.TransmittableThreadLocal;
import lombok.experimental.UtilityClass;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-05-07.
 */
@UtilityClass
public class DataSourceContextHolder {


    private final ThreadLocal<String> THREAD_LOCAL_TENANT = new TransmittableThreadLocal<>();


    public void setDSKey(String key) {
        THREAD_LOCAL_TENANT.set(key);
    }


    public String getDSKey() {
        return THREAD_LOCAL_TENANT.get();
    }

    public void clear() {
        THREAD_LOCAL_TENANT.remove();
    }
}
