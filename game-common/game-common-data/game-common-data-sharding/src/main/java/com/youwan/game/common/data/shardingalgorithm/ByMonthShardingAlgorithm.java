package com.youwan.game.common.data.shardingalgorithm;

import com.google.common.collect.Range;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingValue;
import org.joda.time.DateTime;
import org.joda.time.Months;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.BiFunction;
import java.util.function.Function;


/**
 * @author terry.jiang[taoj555@163.com] on 2020-05-08.
 */
public class ByMonthShardingAlgorithm extends AbstractTimeRangeShardingAlgorithm {

    @Override
    String timeFormatPattern() {
        return "yyyyMM";
    }

    @Override
    String timeExtractPattern() {
        return "(.+)(\\d{6})";
    }

    @Override
    BiFunction<DateTime, DateTime, Integer> timesBetweenFunction() {
        return (start, end)
                -> Months.monthsBetween(start, end).getMonths();
    }

    @Override
    BiFunction<DateTime, Integer, DateTime> timePlusFunction() {
        return DateTime::plusMonths;
    }

    @Override
    Function<DateTime, DateTime> topTimeFunction() {
        return start -> start.withDayOfMonth(1).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0);
    }

    public static void main(String[] args) {
        Collection<String> availableTargetNames = Arrays.asList("xxx202003", "xxx202004");
        ByMonthShardingAlgorithm byDayShardingAlgorithm = new ByMonthShardingAlgorithm();
        Collection<String> ret = byDayShardingAlgorithm.doSharding(availableTargetNames, new RangeShardingValue<>("xxx", "time",
                Range.greaterThan(1584085724)));
        System.err.println(ret);
    }
}
