package com.youwan.game.common.data.shardingalgorithm;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Range;
import groovy.util.logging.Slf4j;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingValue;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-06-24.
 */
@Slf4j
public class FixedMonthShardingAlgorithm implements PreciseShardingAlgorithm<Integer>, RangeShardingAlgorithm<Integer> {

    private static final String TIME_ZONE_ID = "Asia/Shanghai";
    private static final long MILLISECOND_MULTIPLIER = 1000L;
    private static final Set<Integer> FULL_MONTHS = ImmutableSet.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);

    @Override
    public String doSharding(Collection<String> availableTargetNames, PreciseShardingValue<Integer> shardingValue) {
        LocalDateTime d = new LocalDateTime(shardingValue.getValue() * MILLISECOND_MULTIPLIER, DateTimeZone.forID(TIME_ZONE_ID));
        return shardingValue.getLogicTableName() + d.getMonthOfYear();
    }

    @Override
    public Collection<String> doSharding(Collection<String> availableTargetNames, RangeShardingValue<Integer> shardingValue) {
        Range<Integer> range = shardingValue.getValueRange();
        Set<Integer> months = fetchMonths(range);
        return months.stream().map(m -> String.format("%s_%d", shardingValue.getLogicTableName(), m)).collect(Collectors.toSet());
    }

    private Set<Integer> fetchMonths(Range<Integer> range) {
        //如果没有开始时间,则返回所有月份
        if (!range.hasLowerBound()) {
            return FULL_MONTHS;
        }
        LocalDateTime start = new LocalDateTime(range.lowerEndpoint() * MILLISECOND_MULTIPLIER, DateTimeZone.forID(TIME_ZONE_ID))
                .withDayOfMonth(1)
                .withTime(0, 0, 0, 0);
        LocalDateTime end;
        if (range.hasUpperBound()) {
            end = new LocalDateTime(range.upperEndpoint() * MILLISECOND_MULTIPLIER, DateTimeZone.forID(TIME_ZONE_ID))
                    .withDayOfMonth(1)
                    .withTime(0, 0, 0, 0);
        } else {
            // 如果没有结束时间，则取当前月份的第一天
            end = LocalDateTime.now(DateTimeZone.forID(TIME_ZONE_ID)).withDayOfMonth(1).withTime(0,0,0,0);
        }
        Set<Integer> s = new HashSet<>();
        for (LocalDateTime d = start; d.toDate().getTime() <= end.toDate().getTime(); d = d.plusMonths(1)) {
            s.add(d.getMonthOfYear());
        }
        return ImmutableSet.copyOf(s);
    }

    public static void main(String[] args) {
        Collection<String> availableTargetNames = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            availableTargetNames.add("xxx" + i);
        }
        FixedMonthShardingAlgorithm fixedMonthShardingAlgorithm = new FixedMonthShardingAlgorithm();
        Collection<String> ret = fixedMonthShardingAlgorithm.doSharding(availableTargetNames, new RangeShardingValue<>("xxx", "time",
                Range.greaterThan(1586102400)));
        System.err.println(ret);
    }
}
