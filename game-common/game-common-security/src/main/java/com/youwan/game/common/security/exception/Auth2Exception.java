package com.youwan.game.common.security.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.youwan.game.common.security.component.Auth2ExceptionSerializer;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

/**
 * @author lion
 * @date 2018/7/8
 * 自定义OAuth2Exception
 */
@JsonSerialize(using = Auth2ExceptionSerializer.class)
public class Auth2Exception extends OAuth2Exception {

	public Auth2Exception(String msg) {
		super(msg);
	}
}
