package com.youwan.game.common.security.feign;

import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author lion
 * @date 2018/9/14
 * feign 租户信息拦截
 */
@Configuration
public class GameFeignTenantConfiguration {
	@Bean
	public RequestInterceptor gameFeignTenantInterceptor() {
		return new GameFeignTenantInterceptor();
	}
}
