package com.youwan.game.admin.api.dto;


import lombok.Data;


@Data
public class QrCodeDto {
    /**宽度**/
    private Integer width;
    /**长度 **/
     private Integer height;
    /** 边距1~4 */
    private Integer margin;
    /**url地址**/
    private String url;
    /**logoUrl**/
    private String logoUrl;

    public QrCodeDto(Integer width, Integer height, Integer margin, String url, String logoUrl) {
        this.width = width;
        this.height = height;
        this.margin = margin;
        this.url = url;
        this.logoUrl = logoUrl;
    }

}
