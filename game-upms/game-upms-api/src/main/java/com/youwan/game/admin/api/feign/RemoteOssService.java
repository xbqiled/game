package com.youwan.game.admin.api.feign;


import com.youwan.game.admin.api.dto.QrCodeDto;
import com.youwan.game.admin.api.feign.factory.RemoteOssServiceFallbackFactory;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.constant.ServiceNameConstant;
import com.youwan.game.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;


/**
 * <B>OOS配置信息查询</B>
 */
@FeignClient(value = ServiceNameConstant.UMPS_SERVICE, fallbackFactory = RemoteOssServiceFallbackFactory.class)
public interface RemoteOssService {

    @GetMapping("/sysfile/config/")
    R getOssConfigInfo( @RequestHeader(SecurityConstants.FROM) String from);


    @PostMapping("/sysfile/qrcode/")
    R createQrcode(@RequestBody QrCodeDto qrCodeDto, @RequestHeader(SecurityConstants.FROM) String from);


    @PostMapping("/sysfile/defineQrCode/")
    R createDefineQrcode(@RequestBody QrCodeDto qrCodeDto, @RequestHeader(SecurityConstants.FROM) String from);
}
