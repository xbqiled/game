package com.youwan.game.admin.api.feign.factory;



import com.youwan.game.admin.api.feign.RemoteOssService;
import com.youwan.game.admin.api.feign.fallback.RemoteOssServiceFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author lion
 * @date 2019/2/14
 */
@Component
public class RemoteOssServiceFallbackFactory implements FallbackFactory<RemoteOssService> {



    @Override
    public RemoteOssService create(Throwable throwable) {
        RemoteOssServiceFallbackImpl remoteSmsServiceFallbackImpl = new RemoteOssServiceFallbackImpl();
        remoteSmsServiceFallbackImpl.setCause(throwable);
        return remoteSmsServiceFallbackImpl;
    }
}
