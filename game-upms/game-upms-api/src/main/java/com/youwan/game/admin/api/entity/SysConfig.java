package com.youwan.game.admin.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统配置信息表
 *
 * @author code lion
 * @date 2019-02-17 12:46:16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_config")
public class SysConfig extends Model<SysConfig> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private Long id;
    /**
   * key
   */
    private String paramKey;
    /**
   * value
   */
    private String paramValue;
    /**
   * 状态   0：隐藏   1：显示
   */
    private Integer status;
    /**
   * 备注
   */
    private String remark;
  
}
