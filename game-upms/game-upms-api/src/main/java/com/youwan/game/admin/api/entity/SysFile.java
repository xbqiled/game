package com.youwan.game.admin.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-27 16:32:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_file")
public class SysFile extends Model<SysFile> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private Long id;
    /**
   * 
   */
    private String name;
    /**
   * 
   */
    private String fileName;
    /**
   * 
   */
    private String suffix;
    /**
   * 
   */
    private String type;
    /**
   * 
   */
    private String fileUrl;
    /**
   * 
   */
    private LocalDateTime createTime;
  
}
