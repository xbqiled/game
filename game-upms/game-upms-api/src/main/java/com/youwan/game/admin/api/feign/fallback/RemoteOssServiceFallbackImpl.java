package com.youwan.game.admin.api.feign.fallback;



import com.youwan.game.admin.api.dto.QrCodeDto;
import com.youwan.game.admin.api.feign.RemoteOssService;
import com.youwan.game.common.core.util.R;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author lion
 * @date 2019/02/14
 * feign token  fallback
 */
@Slf4j
@Component
public class RemoteOssServiceFallbackImpl implements RemoteOssService {

    @Setter
    private Throwable cause;

    /**
     * <B>发送短信信息</B>
     * @param from
     */
    public R getOssConfigInfo(String from) {
        log.error("feign 获取OOS配置信息失败", cause);
        return null;
    }

    @Override
    public R createQrcode(QrCodeDto qrCodeDto, String from) {
        log.error("feign 创建二维码失败", cause);
        return null;
    }

    @Override
    public R createDefineQrcode(QrCodeDto qrCodeDto, String from) {
        log.error("feign 创建自定义二维码失败", cause);
        return null;
    }
}
