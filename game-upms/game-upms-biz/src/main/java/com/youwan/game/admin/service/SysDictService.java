package com.youwan.game.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.admin.api.entity.SysDict;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author lion
 * @since 2017-11-19
 */
public interface SysDictService extends IService<SysDict> {
}
