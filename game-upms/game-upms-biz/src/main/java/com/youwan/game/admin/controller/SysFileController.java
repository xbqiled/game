package com.youwan.game.admin.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.admin.api.dto.QrCodeDto;
import com.youwan.game.admin.api.entity.SysFile;
import com.youwan.game.admin.service.SysConfigService;
import com.youwan.game.admin.service.SysFileService;
import com.youwan.game.common.core.constant.ConfigConstant;
import com.youwan.game.common.core.oss.CloudStorageConfig;
import com.youwan.game.common.core.oss.OSSFactory;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.common.security.annotation.Inner;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.net.URL;
import java.time.LocalDateTime;


/**
 * 
 *
 * @author
 * @date 2018-12-27 16:32:11
 */
@RestController
@AllArgsConstructor
@RequestMapping("/sysfile")
public class SysFileController {

  private final static String KEY = ConfigConstant.CLOUD_STORAGE_CONFIG_KEY;

  private final SysFileService sysFileService;

  private final SysConfigService sysConfigService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param sysFile 
   * @return
   */
  @GetMapping("/page")
  public R getSysFilePage(Page page, SysFile sysFile) {
    return  new R<>(sysFileService.page(page, Wrappers.query(sysFile).orderByDesc(SysFile::getCreateTime)));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Long id){
    return new R<>(sysFileService.removeById(id));
  }



  @SysLog("上传文件")
  @RequestMapping("/upload")
  public R uploadImage(@RequestParam(value = "file") MultipartFile file) {
    if (file.isEmpty()) {
      return new R<>(false, "上传文件不正确!");
    }
    SysFile sysFile  = new SysFile();
    try {
      //文件名
      String fileName = file.getOriginalFilename();
      String name=fileName.substring(0, fileName.lastIndexOf("."));
      //文件后缀
      String prefix=fileName.substring(fileName.lastIndexOf(".")+1);

      //上传文件
      String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
      //返回文件的URL
      CloudStorageConfig config = sysConfigService.getConfigObject(ConfigConstant.CLOUD_STORAGE_CONFIG_KEY, CloudStorageConfig.class);
      String url = OSSFactory.build(config).uploadSuffix(file.getBytes(), suffix);

      sysFile.setFileName(fileName);
      sysFile.setName(name);
      sysFile.setSuffix(prefix);

      sysFile.setType(prefix);
      sysFile.setFileUrl(url);
      sysFile.setCreateTime(LocalDateTime.now());

      sysFileService.save(sysFile);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return new R<>(true);
  }

  /**
   * 云存储配置信息
   */
  @Inner
  @GetMapping("/config")
  public R config(){
    return new R<>(sysConfigService.getConfigObject(KEY, CloudStorageConfig.class));
  }

  @GetMapping("/ossConfig")
  public R ossConfig(){
    return new R<>(sysConfigService.getConfigObject(KEY, CloudStorageConfig.class));
  }

  /**
   * 生成二维码
   */
  @Inner
  @PostMapping("/qrcode")
  public R qrCode(@Valid @RequestBody QrCodeDto qrCodeDto) {
    //获取配置信息
    CloudStorageConfig ossConfig = sysConfigService.getConfigObject(ConfigConstant.CLOUD_STORAGE_CONFIG_KEY, CloudStorageConfig.class);

    //根据URL生成 文件信息
    QrConfig config = new QrConfig();
    byte[] image = QrCodeUtil.generatePng(qrCodeDto.getUrl(), config);
    String imageUrl = OSSFactory.build(ossConfig).uploadSuffix(image, ".png");

    if (StrUtil.isNotBlank(imageUrl)) {
      return new R<>(imageUrl);
    } else {
      return new R<>(false, "生成文件失败!");
    }
  }

  /**
   * <B>生自定义的二维码</B>
   * @return
   */
  @Inner
  @PostMapping("/defineQrCode")
  public R  defineQrCode(@Valid @RequestBody QrCodeDto qrCodeDto) {
    CloudStorageConfig ossConfig = sysConfigService.getConfigObject(ConfigConstant.CLOUD_STORAGE_CONFIG_KEY, CloudStorageConfig.class);
    int width = qrCodeDto.getWidth() != null && qrCodeDto.getWidth() != 0 ? qrCodeDto.getWidth() : ConfigConstant.WIDTH;
    int hight = qrCodeDto.getHeight() != null && qrCodeDto.getHeight() != 0 ? qrCodeDto.getHeight() : ConfigConstant.HEIGHT;

    QrConfig config = new QrConfig();
    config.setWidth(width);
    config.setHeight(hight);

    if (StrUtil.isNotBlank(qrCodeDto.getLogoUrl())) {
      URL  url = URLUtil.url(qrCodeDto.getLogoUrl());
      File image = FileUtil.file(url);
      config.setImg(image);
    }

    //根据URL生成 文件信息
    byte[] image = QrCodeUtil.generatePng(qrCodeDto.getUrl(), config);
    String imageUrl = OSSFactory.build(ossConfig).uploadSuffix(image, "png");

    if (StrUtil.isNotBlank(imageUrl)) {
      return new R<>(imageUrl);
    } else {
      return new R<>(false, "生成文件失败!");
    }
  }



  /**
   * 保存云存储配置信息
   */
  @SysLog("保存云存储配置")
  @PostMapping("/saveConfig")
  public R saveConfig(@RequestBody CloudStorageConfig config){
    Boolean result = sysConfigService.updateValueByKey(KEY, config);
    return new R<>(result);
  }
}
