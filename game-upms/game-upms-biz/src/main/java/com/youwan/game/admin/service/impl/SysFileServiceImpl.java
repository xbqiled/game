package com.youwan.game.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.admin.api.entity.SysFile;
import com.youwan.game.admin.mapper.SysFileMapper;
import com.youwan.game.admin.service.SysFileService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-27 16:32:11
 */
@Service("sysFileService")
public class SysFileServiceImpl extends ServiceImpl<SysFileMapper, SysFile> implements SysFileService {

    @Override
    public boolean saveFile(SysFile sysFile) {
        sysFile.setCreateTime(LocalDateTime.now());
        this.saveFile(sysFile);
        return true;
    }
}
