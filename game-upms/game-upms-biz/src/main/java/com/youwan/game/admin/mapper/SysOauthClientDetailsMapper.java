package com.youwan.game.admin.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.admin.api.entity.SysOauthClientDetails;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lion
 * @since 2018-05-15
 */
public interface SysOauthClientDetailsMapper extends BaseMapper<SysOauthClientDetails> {

}
