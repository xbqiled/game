package com.youwan.game.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.admin.api.entity.SysConfig;
import com.youwan.game.common.core.oss.CloudStorageConfig;


/**
 * 系统配置信息表
 *
 * @author code lion
 * @date 2019-02-17 12:46:16
 */
public interface SysConfigService extends IService<SysConfig> {

    /**
     * 根据key，获取value的Object对象
     * @param key    key
     * @param clazz  Object对象
     */
     <T> T getConfigObject(String key, Class<T> clazz);

    /**
     * 根据key，更新value
     */
     Boolean updateValueByKey(String key, CloudStorageConfig config);

    /**
     * <B>获取信息</B>
     * @param key
     * @return
     */
     String getValue(String key);

}
