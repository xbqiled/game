package com.youwan.game.admin.service;

import com.youwan.game.common.core.util.R;

/**
 * @author lion
 * @date 2018/11/14
 */
public interface MobileService {
	/**
	 * 发送手机验证码
	 *
	 * @param mobile mobile
	 * @return code
	 */
	R<Boolean> sendSmsCode(String mobile);
}
