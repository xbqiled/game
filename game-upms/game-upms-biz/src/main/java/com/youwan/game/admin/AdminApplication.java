package com.youwan.game.admin;


import com.youwan.game.common.security.annotation.EnableGameFeignClients;
import com.youwan.game.common.security.annotation.EnableGameResourceServer;
import com.youwan.game.common.swagger.annotation.EnableSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * @author lion
 * @date 2018年06月21日
 * 用户统一管理系统
 */
@EnableSwagger2
@SpringCloudApplication
@EnableGameFeignClients
@EnableGameResourceServer(details = true)
public class AdminApplication {
	public static void main(String[] args) {
		SpringApplication.run(AdminApplication.class, args);
	}

}
