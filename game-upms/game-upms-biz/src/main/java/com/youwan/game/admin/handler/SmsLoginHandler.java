package com.youwan.game.admin.handler;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.youwan.game.admin.api.dto.UserInfo;
import com.youwan.game.admin.api.entity.SysUser;
import com.youwan.game.admin.service.SysUserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author lion
 * @date 2018/11/18
 */
@Slf4j
@Component("SMS")
@AllArgsConstructor
public class SmsLoginHandler extends AbstractLoginHandler {
	private final SysUserService sysUserService;

	/**
	 * 验证码登录传入为手机号
	 * 不用不处理
	 *
	 * @param mobile
	 * @return
	 */
	@Override
	public String identify(String mobile) {
		return mobile;
	}

	/**
	 * 通过mobile 获取用户信息
	 *
	 * @param identify
	 * @return
	 */
	@Override
	public UserInfo info(String identify) {
		SysUser sysUser = sysUserService
			.getOne(Wrappers.<SysUser>query()
				.lambda().eq(SysUser::getPhone, identify));

		if (sysUser == null) {
			log.info("手机号未注册:{}", identify);
			return null;
		}
		return sysUserService.findUserInfo(sysUser);
	}
}
