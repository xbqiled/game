package com.youwan.game.admin.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.admin.api.entity.SysRoleMenu;

/**
 * <p>
 * 角色菜单表 Mapper 接口
 * </p>
 *
 * @author lion
 * @since 2017-10-29
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
