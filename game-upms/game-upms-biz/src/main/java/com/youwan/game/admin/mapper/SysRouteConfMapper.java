package com.youwan.game.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.admin.api.entity.SysRouteConf;

/**
 * 路由
 *
 * @author lion
 * @date 2018-11-06 10:17:18
 */
public interface SysRouteConfMapper extends BaseMapper<SysRouteConf> {

}
