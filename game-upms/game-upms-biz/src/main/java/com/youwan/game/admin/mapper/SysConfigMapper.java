package com.youwan.game.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.admin.api.entity.SysConfig;
import org.apache.ibatis.annotations.Param;

/**
 * 系统配置信息表
 *
 * @author code lion
 * @date 2019-02-17 12:46:16
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

    /**
     * 根据key，查询value
     */
    SysConfig queryByKey(String paramKey);

    /**
     * 根据key，更新value
     */
    int updateValueByKey(@Param("paramKey") String paramKey, @Param("paramValue") String paramValue);

}
