package com.youwan.game.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.admin.api.entity.SysLog;

/**
 * <p>
 * 日志表 Mapper 接口
 * </p>
 *
 * @author lion
 * @since 2017-11-20
 */
public interface SysLogMapper extends BaseMapper<SysLog> {
}
