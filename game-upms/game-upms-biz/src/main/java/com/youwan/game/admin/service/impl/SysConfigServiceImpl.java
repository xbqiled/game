package com.youwan.game.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.youwan.game.admin.api.entity.SysConfig;
import com.youwan.game.admin.mapper.SysConfigMapper;
import com.youwan.game.admin.service.SysConfigService;
import com.youwan.game.common.core.exception.CheckedException;
import com.youwan.game.common.core.oss.CloudStorageConfig;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;


/**
 * 系统配置信息表
 *
 * @author code lion
 * @date 2019-02-17 12:46:16
 */
@Slf4j
@AllArgsConstructor
@Service("sysConfigService")
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements SysConfigService {

    private final RedisTemplate redisTemplate;


    @Override
    public String getValue(String key) {
        SysConfig config  = (SysConfig)redisTemplate.opsForValue().get(key);
        if (config == null) {
            config = baseMapper.queryByKey(key);
            redisTemplate.opsForValue().set(key, config);
        }
        return config == null ? null : config.getParamValue();
    }


    @Override
    public <T> T getConfigObject(String key, Class<T> clazz) {
        String value = getValue(key);
        if(StringUtils.isNotBlank(value)){
            return new Gson().fromJson(value, clazz);
        }
        try {
            return clazz.newInstance();
        } catch (Exception e) {
            throw new CheckedException("获取参数失败");
        }
    }


    @Override
    public Boolean updateValueByKey(String key, CloudStorageConfig config) {
        baseMapper.updateValueByKey(key, new Gson().toJson(config));
        return Boolean.TRUE;
    }
}
