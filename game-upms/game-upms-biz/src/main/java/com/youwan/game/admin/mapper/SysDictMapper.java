package com.youwan.game.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.admin.api.entity.SysDict;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author lion
 * @since 2017-11-19
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

}
