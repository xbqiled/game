package com.youwan.game.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.admin.api.entity.SysFile;
import org.apache.ibatis.annotations.Param;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-27 16:32:11
 */
public interface SysFileMapper extends BaseMapper<SysFile> {

    void saveFile(@Param("model")SysFile sysFile);
}
