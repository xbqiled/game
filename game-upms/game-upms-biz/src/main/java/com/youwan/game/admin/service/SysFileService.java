package com.youwan.game.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.admin.api.entity.SysFile;


/**
 * 
 *
 * @author code generator
 * @date 2018-12-27 16:32:11
 */
public interface SysFileService extends IService<SysFile> {


    boolean saveFile(SysFile sysFile);

}
