package com.youwan.game.admin;

import com.ulisesbocchio.jasyptspringboot.encryptor.DefaultLazyEncryptor;
import org.jasypt.encryption.StringEncryptor;
import org.junit.Test;
import org.springframework.core.env.StandardEnvironment;

/**
 * @author lion
 * @date 2018/10/7
 * <p>
 * 加解密单元测试
 */
public class AdminApplicationTest {
	@Test
	public void testJasypt() {
		// 对应application-dev.yml 中配置的根密码
		System.setProperty("jasypt.encryptor.password", "pigx");
		StringEncryptor stringEncryptor = new DefaultLazyEncryptor(new StandardEnvironment());

		//加密方法
		System.out.println(stringEncryptor.encrypt("xinbo"));

		//解密方法
		System.out.println(stringEncryptor.decrypt("gPFcUOmJm8WqM3k3eSqS0Q=="));
	}
}
