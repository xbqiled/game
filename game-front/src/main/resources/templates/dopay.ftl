<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'/>
    <meta name="baidu-site-verification" content="2nXjznVkpe">

    <title>${channelVo.channelName}</title>
    <meta name="keywords" content="${channelVo.channelName}">
    <meta name="description" content="${channelVo.channelName}">

    <link href="/front/css/pace-theme-minimal.css" rel="stylesheet">
    <link href="/front/css/bootstrap.min.css" rel="stylesheet">

    <link href="/front/css/editor.css" rel="stylesheet">
    <link href="/front/css/plugins.css" rel="stylesheet">
    <link href="/front/css/style.css" rel="stylesheet">

    <link href="/front/css/simple-line-icons.css" rel="stylesheet">
    <link href="/front/css/font-awesome.min.css" rel="stylesheet">
    <link href="/front/css/layer.css" rel="stylesheet" id="layui_layer_skinlayercss" style="">

    <script src="/front/js/pace.min.js"></script>
    <script src="/front/jquery/jquery.min.js"></script>
    <script src="/front/js/layer.js"></script>
    <script src="/front/js/clipboard.min.js"></script>
    <script src="/front/js/bootstrap.min.js"></script>

    <link href="${channelVo.iconUrl}" rel="apple-touch-icon-precomposed">
    <link href="${channelVo.iconUrl}" rel="shortcut icon">
    <script type="text/javascript">
        var _MTONS = _MTONS || {};
        _MTONS.BASE_PATH = '';
        _MTONS.LOGIN_TOKEN = '';
    </script>

    <link href="${channelVo.iconUrl}" rel="apple-touch-icon-precomposed">
    <link href="${channelVo.iconUrl}" rel="shortcut icon">
</head>
<body class="  pace-done">
<div class="pace  pace-inactive">
    <div class="pace-progress" data-progress-text="100%" data-progress="99"
         style="transform: translate3d(100%, 0px, 0px);">
        <div class="pace-progress-inner"></div>
    </div>
    <div class="pace-activity"></div>
</div>
<!-- header -->
<header class="site-header headroom">
    <div class="container">
        <nav class="navbar" role="navigation">
            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">

                </button>
                <a class="navbar-brand" href="#">
                    <img src="${channelVo.iconUrl}">
                </a>
            </div>
        </nav>
    </div>
</header>

<div id="weOther" style="position: absolute;top:0;left: 0;right: 0;bottom: 0;background: rgba(0,0,0,0.8);z-index: 99999;display: none;align-items: center;justify-content: center;">
    <img src="data:image/gif;base64,R0lGODlhJQAlAJECAL3L2AYrTv///wAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQFCgACACwAAAAAJQAlAAACi5SPqcvtDyGYIFpF690i8xUw3qJBwUlSadmcLqYmGQu6KDIeM13beGzYWWy3DlB4IYaMk+Dso2RWkFCfLPcRvFbZxFLUDTt21BW56TyjRep1e20+i+eYMR145W2eefj+6VFmgTQi+ECVY8iGxcg35phGo/iDFwlTyXWphwlm1imGRdcnuqhHeop6UAAAIfkEBQoAAgAsEAACAAQACwAAAgWMj6nLXAAh+QQFCgACACwVAAUACgALAAACFZQvgRi92dyJcVJlLobUdi8x4bIhBQAh+QQFCgACACwXABEADAADAAACBYyPqcsFACH5BAUKAAIALBUAFQAKAAsAAAITlGKZwWoMHYxqtmplxlNT7ixGAQAh+QQFCgACACwQABgABAALAAACBYyPqctcACH5BAUKAAIALAUAFQAKAAsAAAIVlC+BGL3Z3IlxUmUuhtR2LzHhsiEFACH5BAUKAAIALAEAEQAMAAMAAAIFjI+pywUAIfkEBQoAAgAsBQAFAAoACwAAAhOUYJnAagwdjGq2amXGU1PuLEYBACH5BAUKAAIALBAAAgAEAAsAAAIFhI+py1wAIfkEBQoAAgAsFQAFAAoACwAAAhWUL4AIvdnciXFSZS6G1HYvMeGyIQUAIfkEBQoAAgAsFwARAAwAAwAAAgWEj6nLBQAh+QQFCgACACwVABUACgALAAACE5RgmcBqDB2MarZqZcZTU+4sRgEAIfkEBQoAAgAsEAAYAAQACwAAAgWEj6nLXAAh+QQFCgACACwFABUACgALAAACFZQvgAi92dyJcVJlLobUdi8x4bIhBQAh+QQFCgACACwBABEADAADAAACBYSPqcsFADs=" alt="">
</div>
<!-- content -->
<div class="wrap">
    <!-- Main -->
    <div class="container">

        <div class="row">
            <div class="col-md-4 col-md-offset-4 floating-box">
                <div class="panel panel-default">
                    <div class="panel-heading">${payChannelVo.payName}</div>
                    <div class="panel-body">
                        <div id="message"></div>
                            <div class="form-group ">
                                <label class="control-label" for="payfee">支付金额(元)</label>
                                <input class="form-control" id="payfee" name="payfee" type="text">
                            </div>
                            <#if payChannelVo.type == 1 || payChannelVo.type == 2 || payChannelVo.type == 3>
                                <div class="form-group ">
                                    <label class="control-label" for="payaccount">支付账号</label>
                                    <input class="form-control" id="payaccount" name="payaccount" type="text">
                                </div>
                                <div class="form-group ">
                                    <label class="control-label" for="payname">支付姓名</label>
                                    <input class="form-control" id="payname" name="payname" type="text">
                                </div>
                            </#if>
                            <input  id="type" name="type" type="hidden" value="${type}">
                            <input  id="channelId" name="channelId" type="hidden" value="${channelId}">
                            <input  id="userId" name="userId" type="hidden" value="${userId}">

                            <input  id="min" name="min" type="hidden" value="${min}">
                            <input  id="max" name="max" type="hidden" value="${max}">
                            <input  id="configId" name="userId" type="hidden" value="${configId}">

                            <button type="button" class="btn btn-success btn-block" onclick="doPay()">
                                支付
                            </button>
                            <br/>
                            <fieldset class="form-group">
                                <div class="alert alert-info">
                                    <#if payImage != ''>
                                        <img src="${payImage}" alt="" style="width:100%">
                                    <#else>
                                        ${payChannelVo.payDesc}
                                    </#if>
                                        <#--<div>${payImage}</div>-->
                                    <br/>
                                </div>
                            <#if payChannelVo.type == 1 || payChannelVo.type == 2 || payChannelVo.type == 3>
                                 <#list payMap?keys as key>
                                    <input type="text" style="position:absolute;z-index:-2;opacity: 0" id="copy${key}" value="${payMap["${key}"]}" />
                                    <a id="oncopy${key}"  style=";color:#4aa9db;"  onclick="copy(&quot;copy${key}&quot;)" class="btn btn-default btn-block" href="javascript:void(0)">
                                        ${key}:${payMap["${key}"]}(点击复制)
                                    </a>
                                 <!--
                                 <a id="oncopy${key}" style=";color:#4aa9db;" onclick="copy(&quot;copy${key}&quot;)">复制</a> -->
                                 </#list>
                             </#if>
                            </fieldset>
                        <!--</form>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <div class="footer-col footer-col-logo hidden-xs hidden-sm">
            <img src="${channelVo.iconUrl}" alt="${channelVo.channelName}">
        </div>
        <div class="footer-col footer-col-copy">
            <div class="copyright">
                <span>Copyright ©  ${channelVo.channelName}</span>
            </div>
        </div>
        <div class="footer-col footer-col-sns hidden-xs hidden-sm">
            <div class="footer-sns">
                <span>Powered by <a href="${channelVo.iconUrl}" target="_blank">${channelVo.channelName}</a></span>
            </div>
        </div>
    </div>
</footer>
</body>
<script type="text/javascript">
    function copy(id){
        $('#on'+id).attr('data-clipboard-target', '#'+id);
        var clipboard = new ClipboardJS ("#on"+id);
        clipboard.on('success', function (e) {
            if(e.text){
                alert('复制成功');
            }else{
                alert('复制失败');
            }
        });
    }
    function rmoney(s) {
        return parseFloat(s.replace(/[^\d\.-]/g, ""));
    }

    function showDialog() {
        document.getElementById("weOther").display = block;
    }
    
    function copyText() {
        document.getElementById("input").select(); // 选中文本
        document.execCommand("copy"); // 执行浏览器复制命令
        alert("复制成功");
    }

    function doPay() {
        document.getElementById("weOther").style.display = 'flex';
        var contextUrl = "${url}";
        var url = contextUrl + "/pay/result";

        var type =  $("#type").val();
        var channelId = $("#channelId").val();
        var min =  $("#min").val();

        var max = $("#max").val();
        var userId =  $("#userId").val();
        var configId =  $("#configId").val();

        var payfee =  $("#payfee").val();
        var payaccount =  $("#payaccount").val();
        var payname =  $("#payname").val();

        var temp = document.createElement("form");
        temp.method = "get";
        temp.style.display = "none";
        temp.action = url;

        //userIdInput
        var payTypeInput = document.createElement("input");
        payTypeInput.type="hidden";
        payTypeInput.name= "type";
        payTypeInput.value= type;

        //userIdInput
        var userIdInput = document.createElement("input");
        userIdInput.type="hidden";
        userIdInput.name= "userId";
        userIdInput.value= userId;

        //channelId
        var channelIdInput = document.createElement("input");
        channelIdInput.type="hidden";
        channelIdInput.name= "channelId";
        channelIdInput.value= rmoney(channelId);

        //configId
        var configIdInput = document.createElement("input");
        configIdInput.type="hidden";
        configIdInput.name= "configId";
        configIdInput.value= rmoney(configId);

        //payFee
        var payfeeInput = document.createElement("input");
        payfeeInput.type="hidden";
        payfeeInput.name= "payfee";
        payfeeInput.value= rmoney(payfee);

        //payaccount
        var payaccountInput = document.createElement("input");
        payaccountInput.type="hidden";
        payaccountInput.name= "payaccount";
        payaccountInput.value= payaccount;

        //payname
        var paynameInput = document.createElement("input");
        paynameInput.type="hidden";
        paynameInput.name= "payname";
        paynameInput.value= payname;

        temp.appendChild(payTypeInput);
        temp.appendChild(paynameInput);
        temp.appendChild(payaccountInput);
        temp.appendChild(payfeeInput);

        temp.appendChild(channelIdInput);
        temp.appendChild(configIdInput);
        temp.appendChild(userIdInput);

        if (payfee == '' || payfee == null || payfee == undefined ) {
            alert("请输入支付金额");
            return
        }

        if(payfee < rmoney(min)) {
            alert("支付金额不能小于通道最小金额");
            return
        }

        if(payfee >  rmoney(max)) {
            alert("支付金额不能大于通道最大金额");
            return
        }

        if (type == 2 || type == 3 || type == 4) {
          //银行支付
            if (payaccount == '' || payaccount == null || payaccount == undefined || payaccount == 'undefined' ) {
                alert("请输入付款账号");
                return
            }
            if (payname == '' || payaccount == null || payname == undefined || payname == 'undefined' ) {
                alert("请输入付款用户名");
                return
            }
        }

        document.body.appendChild(temp);
        temp.submit();
        return temp;
    }
</script>
</html>