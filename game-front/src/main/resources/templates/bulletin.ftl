<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>${vo.title!}</title>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta name="renderer" content="webkit">
    <meta name="theme-color" content="#fff">
    <meta name="author" content="lion">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link href="/front/css/font-awesome.min.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="/front/css/blog_basic.min.css">
    <link href="/front/css/style.min.css" type="text/css" rel="stylesheet">
    <link rel="alternate" type="application/rss+xml" title="atom 1.0" href="http://localhost:8090/feed.xml">
    <style>
        ::-webkit-scrollbar {
            width: 6px;
            height: 6px;
            background-color: #eee;
        }
        ::-webkit-scrollbar-thumb {
            background-color: #3798e8;
        }
        ::-webkit-scrollbar-track {
            background-color: #eee;
        }
    </style>
</head>
<div class="main">
    <link href="/front/css/prism.css" type="text/css" rel="stylesheet">
    <style>
        code, tt {
            font-size: 1.2em;
        }
        table {
            border-spacing: 0;
            border-collapse: collapse;
            margin-top: 0;
            margin-bottom: 16px;
            display: block;
            width: 100%;
            overflow: auto;

        }
        table th {
            font-weight: 600;
        }
        table th,
        table td {
            padding: 6px 13px;
            border: 1px solid #dfe2e5;
        }
        table tr {
            background-color: #fff;
            border-top: 1px solid #c6cbd1;
        }
        table tr:nth-child(2n) {
            background-color: #f6f8fa;
        }
    </style>
    <div class="post-page">
        <div class="post animated fadeInDown">
            <div class="post-title">
                <h3>
                  ${title!}
                </h3>
            </div>
            <hr>
            <div class="post-content">
                 ${content!}
            </div>
            <div class="post-footer">
                <div class="meta">
                    <div class="info">
                        <i class="fa fa-sun-o"></i>
                        <span class="date">${createtime!}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/front/js/prism.js"></script>
<script type="text/javascript" src="/front/jquery/jquery.min.js"></script>
</body></html>