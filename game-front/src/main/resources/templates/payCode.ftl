<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'/>
    <meta name="baidu-site-verification" content="2nXjznVkpe">

    <title>${payCodeVo.channelName}</title>
    <meta name="keywords" content="${payCodeVo.channelName}">
    <meta name="description" content="${payCodeVo.channelName}">
    <link href="/front/css/pace-theme-minimal.css" rel="stylesheet">

    <link href="/front/css/bootstrap.min.css" rel="stylesheet">
    <link href="/front/css/editor.css" rel="stylesheet">
    <link href="/front/css/plugins.css" rel="stylesheet">
    <link href="/front/css/style.css" rel="stylesheet">

    <link href="/front/css/simple-line-icons.css" rel="stylesheet">
    <link href="/front/css/font-awesome.min.css" rel="stylesheet">
    <link href="/front/css/layer.css" rel="stylesheet" id="layui_layer_skinlayercss" style="">

    <script src="/front/js/pace.min.js"></script>
    <script src="/front/jquery/jquery.min.js"></script>
    <script src="/front/js/layer.js"></script>
    <script src="/front/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        var _MTONS = _MTONS || {};
        _MTONS.BASE_PATH = '';
        _MTONS.LOGIN_TOKEN = '';
    </script>

    <script src="/front/js/sea.js"></script>
    <script src="/front/js/sea.config.js"></script>
    <!-- Favicons -->
    <link href="${payCodeVo.iconUrl}" rel="apple-touch-icon-precomposed">
    <link href="${payCodeVo.iconUrl}" rel="shortcut icon">
</head>
<body class="  pace-done">
<div class="pace  pace-inactive">
    <div class="pace-progress" data-progress-text="100%" data-progress="99"
         style="transform: translate3d(100%, 0px, 0px);">
        <div class="pace-progress-inner"></div>
    </div>
    <div class="pace-activity"></div>
</div>
<!-- header -->

<!-- Fixed navbar -->
<header class="site-header headroom">
    <div class="container">
        <nav class="navbar" role="navigation">
            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">

                </button>
                <a class="navbar-brand" href="#">
                     <span> ${payCodeVo.channelName}</span>
                </a>
            </div>
        </nav>
    </div>
</header>

<!-- content -->
<div class="wrap">
    <!-- Main -->
    <div class="container">

        <div class="row">
            <div class="col-md-4 col-md-offset-4 floating-box">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align: center">${payCodeVo.payName}</div>
                        <div class="panel-body">
                            <div class="form-group " style="display: flex; justify-content: center">
                                <img src="${payCodeVo.imgUrl}">
                            </div>
                        </div>
                    <div class="panel-heading" style="text-align: center"><font size="3" color="red"><font>${payCodeVo.payFee}元 </font></div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer" style="position: fixed; width: 100%; height: 48px; bottom: 0;line-height: 39px;padding: 0">
    <div class="container">
        <div class="footer-col footer-col-logo hidden-sm">
            <img src="${payCodeVo.iconUrl}" alt="${payCodeVo.channelName}" style="margin-right:0;max-height: 35px;">
        </div>
        <div class="footer-col footer-col-copy">
            <div class="copyright">
                <span>Copyright ©  ${payCodeVo.channelName}</span>
            </div>
        </div>
    </div>
</footer>

</body>
</html>