<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>${vo.channelName}</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=0">
    <meta name="format-detection" content="telephone=no">
    <script type="text/javascript" src="/front/js/pub_rem.js"></script>
    <link rel="stylesheet" href="/front/css/pub_reset.css">
    <!-- Favicons -->
    <link href="${vo.iconUrl}" rel="apple-touch-icon-precomposed">
    <link href="${vo.iconUrl}" rel="shortcut icon">
    <style type="text/css">
        html {
            background: #fff;
        }

        ::-webkit-scrollbar {
            width: 16px;
            height: 10px;
        }

        /*scroll shadow radius*/
        ::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
            border-radius: 10px;
            background-color: #ddd;
        }

        /*swipe inset radius*/
        ::-webkit-scrollbar-thumb {
            border-radius: 10px;
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
            background-color: #029ed7;
        }

        .touch {
            display: block;
            width: 100%;
            height: 1rem;
            line-height: 1rem;
            color: #fff;
            font-size: 0.46rem;
            text-align: center;
            border-radius: 0.06rem;
            background-image: url(/front/images/download_btn.png);
            background-size: 100% 100%;
        }

        .max_width {
            max-width: 720px;
            margin: 0 auto;
            padding-bottom: 2rem;
        }

        .banner {
            width: 100%;
            height: 5.17rem;
        }

        .banner img {
            width: 100%;
        }

        .dw_area {
            min-height: 1.6rem;
            padding: 0.18rem 0.22rem;
            position: relative;
        }

        .dw_v {
            display: flex;
            justify-content: center;
            align-items: Center;
            min-height: 0.8rem;
            padding: 0.18rem 0.22rem;
            position: relative;
        }

        .dw_v .in {
            min-height: 1rem;
        }

        .dw_v .in .m img {
            width: 100%;
        }

        .dw_v .in .rt_t {
            padding: 0.15rem 0 0 1.82rem;
            color: #343434;
        }

        .dw_v .in .rt_t span {
            display: inline-block;
            padding: 0.15rem 0 0 2.3rem;
            font-size: 0.3rem;
            background: url(/front/images/star.png) no-repeat 0 0.15rem;
            background-size: 2rem 0.4rem;
        }

        .dw_area .in {
            min-height: 1rem;
        }

        .dw_area .in .m {
            display: block;
            width: 1.6rem;
            height: 1.6rem;
            position: absolute;
            left: 0.27rem;
            top: .18rem;
        }

        .dw_area .in .m img {
            width: 100%;
        }

        .dw_area .in .rt_t {
            padding: 0.15rem 0 0 1.82rem;
            color: #343434;
        }

        .dw_area .in .rt_t h3 {
            font-size: 0.48rem;
        }

        .dw_area .in .rt_t span {
            display: inline-block;
            padding: 0.15rem 0 0 2.3rem;
            font-size: 0.3rem;
            background: url(/front/images/star.png) no-repeat 0 0.15rem;
            background-size: 2rem 0.4rem;
        }

        .btn {
            padding: 0.16rem 0 0.24rem;
        }

        .gray_w {
            background: #ededed;
        }

        .game {
            padding: 0.26rem 0.2rem;
            min-height: 4.17rem;
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -o-box-sizing: border-box;
            overflow: hidden;
        }

        .game_pic {
            width: 100%;
            min-height: 4.17rem;
            white-space: nowrap;
            overflow-x: auto;
            overflow-scrolling: touch;
            -webkit-overflow-scrolling: touch;
            -moz-overflow-scrolling: touch;
        }

        .game_pic a img {
            width: 40%;
            margin-right: 2.4%;
        }

        .game_lis {
            margin-top: 0.38rem;
            padding: 0.3rem;
            border-top: 1px solid #d5d5d5;
            background: #ededed;
        }

        .game_lis h3 {
            font-size: 0.38rem;
            color: #333;
        }

        .game_lis ul {
            padding: 0.32rem 0.24rem 0;
            min-height: 1.7rem;
            overflow: hidden;
        }

        .game_lis ul li {
            width: 25%;
            float: left;
        }

        .game_lis ul li a {
            display: block;
        }

        .game_lis ul li a .m {
            display: block;
            width: 1.2rem;
            height: 1.2rem;
            margin: 0 auto;
        }

        .game_lis ul li a .m img {
            width: 100%;
        }

        .game_lis ul li a .t {
            width: 100%;
            text-align: center;
            color: #3333;
            font-size: 0.24rem;
            display: inline-block;
            margin-top: 0.16rem;
        }

        .dw_propt {
            width: 100%;
            min-height: 1.54rem;
            background: rgba(255, 255, 255, 0.77);
            position: fixed;
            left: 0;
            bottom: 0;
            box-shadow: 0 0 2px rgba(0, 0, 0, 0.35);
        }

        .ppt_lt {
            position: relative;
        }

        .img {
            display: block;
            width: 1.1rem;
            height: 1.1rem;
            position: absolute;
            top: 0.22rem;
            left: 0.38rem;
            overflow: hidden;
        }

        .img img {
            display: block;
            width: 100%;
            height: 100%;
        }

        .ppt_lt .lt_t {
            padding: 0.30rem 0 0 1.74rem;
            color: #333;
        }

        .ppt_lt .lt_t h3 {
            font-size: 0.4rem;
        }

        .ppt_lt .lt_t span {
            font-size: 0.28rem;
            color: #545454;
            display: block;
            margin-top: 0.15rem;
        }

        .ppt_lt_ii .lt_in {
            margin: 0 0.3rem;
            position: relative;
        }

        .ppt_lt .ppt_dw {
            display: block;
            width: 2.5rem;
            height: 0.9rem;
            line-height: 0.9rem;
            background: #43c117;
            color: #fff;
            text-align: center;
            font-size: 0.42rem;
            border-radius: 0.08rem;
            position: absolute;
            top: 0.33rem;
            right: 0.3rem;
        }

        .ppt_lt_ii .ppt_dw {
            width: 100%;
            left: 0;
            right: 0
        }

        footer {
            min-height: 0.80rem;
            padding: 0.23rem 0;
            border-top: 1px solid #ededed;
            background: #424a55;
            text-align: center;
        }

        footer p {
            color: #c9c9c9;
            font-size: 0.28rem;
        }

        footer p:last-child {
            margin-top: 0.1rem;
        }

        body {
            /*background: url(images/back.jpg) no-repeat;
            background-size: 100%;*/
            overflow-x: hidden;
        }

        .dw_area .in.icons {
            margin-top: 1rem;
            width: 100%;
            text-align: center;
        }

        .dw_area .in.icons .m {
            position: static;
            display: inline-block;
            width: 1.25rem;
            height: 1.25rem;
            margin-right: 0.14rem;
            /*background: rgba(0, 0, 0, 0.5);*/
        }

        .dw_area .in.icons .m .name, .dw_area.game-item .m .name {
            width: 100%;
            text-align: center;
            font-weight: bold;
        }

        .game-item {
            margin: 0.25rem;
            background: #eaf1f7;
            height: 2.6rem;
            border-radius: 0.06rem;
            position: relative;
        }

        .dw_area.game-item .m {
            display: inline-block;
            width: 1.25rem;
            height: 1.25rem;
            position: absolute;
        }

        .dw_area.game-item .m img {
            width: 100%;
            height: 100%;
        }

        .dw_area.game-item .tip {
            position: absolute;
            left: 0.15rem;
            top: 2.45rem;
            width: 1.5rem;
            text-align: center;
            font-size: 0.2rem;
            color: gray;
            font-weight: bold;
        }

        .down-button {
            display: block;
            background: url(/front/images/download.png);
            background-size: 100%;
            width: 1.5rem;
            height: 0.7rem;
            top: 1.9rem;
            left: 0.15rem;
            position: absolute;
            background-repeat: no-repeat;
        }

        .dw_area.game-item .pic {
            position: absolute;
            left: 2rem;
            top: 0.2rem;
            /*background: orange;*/
            width: 4.5rem;
            height: 2.55rem;
            background-size: 100% 100%;
        }

        a:hover {
            cursor: pointer;
        }
    </style>
</head>

<body>
<div class="max_width">
    <div class="banner">
        <img src="/front/images/back.jpg" alt="">
    </div>

    <div class="dw_area">
        <div class="in icons">
            <a class="m">
                <img src="/front/images/doudizhu.png" alt="">
                <div id="ddz" class="name">斗地主</div>
            </a>
            <a class="m">
                <img src="/front/images/buyu.png" alt="">
                <div id="by" class="name">捕鱼</div>
            </a>
            <a class="m">
                <img src="/front/images/bairenniuniu.png" alt="">
                <div id="brnn" class="name">百人牛牛</div>
            </a>
            <a class="m">
                <img src="/front/images/qiangzhuangniuniu.png" alt="">
                <div id="qznn" class="name">抢庄牛牛</div>
            </a>
        </div>
    </div>

    <div class="dw_v">
        <div class="in" style="min-height: 0.2rem; margin-top: -0.3rem;">
            <section class="rt_t" style="padding: 0">
                <span id="wj">（10亿玩家的选择）</span>
            </section>
        </div>
    </div>

    <div class="dw_area game-item">
        <a class="m ddz">
            <img src="/front/images/doudizhu.png" alt="">
            <div class="name">斗地主</div>
        </a>
        <a class="down-button">

        </a>
        <div class="tip">8000万下载</div>
        <a class="pic" style="background-image: url(/front/images/doudizhu_p.jpg)"></a>
    </div>

    <div class="dw_area game-item">
        <a class="m by">
            <img src="/front/images/buyu.png" alt="">
            <div class="name">捕鱼</div>
        </a>
        <a class="down-button">

        </a>
        <div class="tip">8000万下载</div>
        <a class="pic" style="background-image: url(/front/images/buyu_p.jpg)"></a>
    </div>

    <div class="dw_area game-item">
        <a class="m brnn">
            <img src="/front/images/bairenniuniu.png" alt="">
            <div class="name">百人牛牛</div>
        </a>
        <a class="down-button">

        </a>
        <div class="tip">8000万下载</div>
        <a class="pic" style="background-image: url(/front/images/bairenniuniu_p.jpg)"></a>
    </div>

    <div class="dw_area game-item">
        <a class="m qznn">
            <img src="/front/images/qiangzhuangniuniu.png" alt="">
            <div class="name">抢庄牛牛</div>
        </a>
        <a class="down-button">

        </a>
        <div class="tip">8000万下载</div>
        <a class="pic" style="background-image: url(/front/images/qiangzhuangniuniu_p.jpg)"></a>
    </div>

    <footer>
        <p id="jj">全球对战同台竞技！</p>
        <p id="jr">还不赶快加入！</p>
    </footer>
</div>
<div class="dw_propt">
    <div class="ppt_lt">
             <span class="img">
                 <img src="${vo.iconUrl}" alt="">
             </span>
        <div class="lt_t">
            <h3 id="cf">${vo.channelName}</h3>
            <span id="sg">0秒下载，顶级手感</span>
        </div>
        <a href="#"  class="ppt_dw" id="lj">立即下载</a>
    </div>
</div>
</body>
<script type="text/javascript" src="/front/jquery/jquery.min.js"></script>
<link rel="preconnect" href="//openinstall.io" crossorigin="use-credentials"/>
<script type="text/javascript" charset="utf-8" src="//res.cdn.openinstall.io/openinstall.js"></script>
<script type="text/javascript">
    //添加openinstall
    //openinstall初始化时将与openinstall服务器交互，应尽可能早的调用
    /*web页面向app传递的json数据(json string/js Object)，应用被拉起或是首次安装时，通过相应的android/ios api可以获取此数据*/
    var data = OpenInstall.parseUrlParams();//openinstall.js中提供的工具函数，解析url中的所有查询参数
    var isInstall = 0;
    var phoneType = 1;
    var locationUrl = document.referrer;
    //var androidAppKey = "${vo.androidAppKey}";
    //var androidPkgid = "${vo.androidPkgid}";
    //var iosAppKey = "${vo.iosAppKey}";
    //var iosPkgid = "${vo.iosPkgid}";
    if (data != null && data != '' && locationUrl != null && locationUrl != '') {
        var url = ''
        if (isURL(locationUrl)) {
            if (locationUrl.indexOf("https") == 0) {
                url = locationUrl.substr(8, locationUrl.length)
            } else if (locationUrl.indexOf("http") == 0) {
                url = locationUrl.substr(8, locationUrl.length)
            }

            if (url != null && url != '') {
                if (url.substr(url.length - 1, url.length) == "\/") {
                    url = url.substr(0, url.length - 1)
                }
            }
        }

        if (url != null && url != '') {
            data['locationUrl'] = url
        }
        console.log(data)
    }

    function isURL(str){
        return !!str.match(/(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/g);
    }

    new OpenInstall({
        /*appKey必选参数，openinstall平台为每个应用分配的ID*/
        appKey: "${vo.androidAppKey}",
        //_pkgId: "${vo.androidPkgid}",
        /*可选参数，自定义android平台的apk下载文件名，只有apk在openinstall托管时才有效；个别andriod浏览器下载时，中文文件名显示乱码，请慎用中文文件名！*/
        //apkFileName : 'com.XinBo.LoveLottery-v1.0.0.apk',
        /*可选参数，是否优先考虑拉起app，以牺牲下载体验为代价*/
        //preferWakeup:true,
        /*自定义遮罩的html*/
        /*openinstall初始化完成的回调函数，可选*/
        onready: function () {
            var m = this, button = document.getElementById("lj");
            button.style.visibility = "visible";

            /*在app已安装的情况尝试拉起app*/
            m.schemeWakeup();

            /*用户点击某个按钮时(假定按钮id为downloadButton)，安装app*/
            button.onclick = function () {
                m.wakeupOrInstall();
                return false;
            }
        }
    }, data);

    //处理打开游戏
    function getMobileType() {
        //首先判断是pc端
        if (isPC()) {
            phoneType = 1;
        } else {
            //判断手机类型
            if (mobileType() == '1') {
                phoneType = 1;
            } else if (mobileType() == '2') {
                phoneType = 2;
            } else {
                phoneType = 1;
            }
        }
    }

    //处理下载
    function downloadGame() {
        var iosUrl = "${vo.iosUrl}";
        var androidUrl = "${vo.androidUrl}";

        //首先判断是pc端
        if (isPC()) {
            if (isInstall == 1) {
                //已经安装
                window.location.href = schemeUrl;
            } else {
                window.location.href = androidUrl;
            }
        } else {
            //判断是否是微信

            //判断是否是微博

            //判断是否是QQ

            //判断手机类型
            if (mobileType() == '1') {
                window.location.href = androidUrl;
            } else if (mobileType() == '2') {
                window.location.href = iosUrl;
            } else {
                window.location.href = androidUrl;
            }
        }
    }

    //判断手机类型
    function mobileType(){
        var u = navigator.userAgent, app = navigator.appVersion;
        var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1; //g
        var isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
        if (isAndroid) {
            //这个是安卓操作系统
            return "1";
        }
        if (isIOS) {
            //这个是ios操作系统
            return "2";
        }
    }
    //判断是否是PC端
    function isPC(){
        var userAgentInfo = navigator.userAgent;
        var Agents = ["Android", "iPhone",
            "SymbianOS", "Windows Phone",
            "iPad", "iPod"];
        var flag = true;
        for (var v = 0; v < Agents.length; v++) {
            if (userAgentInfo.indexOf(Agents[v]) > 0) {
                flag = false;
                break;
            }
        }
        return flag;
    }
    //判断是否是微信
    function isWeixin(){
        var ua = navigator.userAgent.toLowerCase();
        if (ua.match(/MicroMessenger/i) == "micromessenger") {
            return true;
        } else {
            return false;
        }
    }
    //判断是否是微博
    function isWeibo() {
        var ua = navigator.userAgent.toLowerCase();
        if (ua.match(/WeiBo/i) == "weibo") {
            return true;
        } else {
            return false;
        }
    }
    //判断是否QQ
    function isMobileQQ() {
        var ua = navigator.userAgent;
        return /(iPad|iPhone|iPod).*? (IPad)?QQ\/([\d\.]+)/.test(ua) || /\bV1_AND_SQI?_([\d\.]+)(.*? QQ\/([\d\.]+))?/.test(ua);
    }

    //启动执行
    $(function (){
        open_or_download_app();
        //getMobileType();
    });

    function open_or_download_app() {
        var androidUrl = "${vo.androidUrl}";
        var schemeUrl = "${schemeUrl}";
        isInstall = 1;
        $("#lj").text('立即打开')

        var ifr = document.createElement("iframe");
        ifr.src = schemeUrl;
        ifr.style.display = "none";

        document.body.appendChild(ifr);
        window.setTimeout(function () {
            document.body.removeChild(ifr);
            isInstall = 0;
            $("#lj").text('立即下载')
        }, 1000)
    }
</script>
</html>
