<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta name="renderer" content="webkit">
    <meta name="theme-color" content="#fff">
    <meta name="author" content="lion">
    <meta name="keywords" content="">
    <meta name="description" content="">
</head>
<body>

<form action="${redirectUrl}" method="${formMethod }" id="redirectUrlFormId">
  <#if payMap?exists>
      <#list payMap?keys as key>
          <input name="${key}" value="${payMap["${key}"]}" type="hidden">
      </#list>
  </#if>
</form>
</body>
<script type="text/javascript" src="/front/jquery/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#redirectUrlFormId").submit();
    });
</script>
</html>