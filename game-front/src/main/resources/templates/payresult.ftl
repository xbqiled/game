<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'/>
    <meta name="baidu-site-verification" content="2nXjznVkpe">

    <title>${channelVo.channelName}</title>
    <meta name="keywords" content="${channelVo.channelName}">
    <meta name="description" content="${channelVo.channelName}">

    <link href="/front/css/pace-theme-minimal.css" rel="stylesheet">
    <link href="/front/css/bootstrap.min.css" rel="stylesheet">

    <link href="/front/css/editor.css" rel="stylesheet">
    <link href="/front/css/plugins.css" rel="stylesheet">
    <link href="/front/css/style.css" rel="stylesheet">

    <link href="/front/css/simple-line-icons.css" rel="stylesheet">
    <link href="/front/css/font-awesome.min.css" rel="stylesheet">
    <link href="/front/css/layer.css" rel="stylesheet" id="layui_layer_skinlayercss" style="">

    <script src="/front/js/pace.min.js"></script>

    <script src="/front/jquery/jquery.min.js"></script>
    <script src="/front/js/layer.js"></script>
    <script src="/front/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        var _MTONS = _MTONS || {};
        _MTONS.BASE_PATH = '';
        _MTONS.LOGIN_TOKEN = '';
    </script>

    <!-- Favicons -->
    <link href="${channelVo.iconUrl}" rel="apple-touch-icon-precomposed">
    <link href="${channelVo.iconUrl}" rel="shortcut icon">
</head>
<body class="  pace-done">
<div class="pace  pace-inactive">
    <div class="pace-progress" data-progress-text="100%" data-progress="99"
         style="transform: translate3d(100%, 0px, 0px);">
        <div class="pace-progress-inner"></div>
    </div>
    <div class="pace-activity"></div>
</div>
<!-- header -->

<!-- Fixed navbar -->
<header class="site-header headroom">
    <div class="container">
        <nav class="navbar" role="navigation">
            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">

                </button>
                <a class="navbar-brand" href="#">
                    <img src="${channelVo.iconUrl}">
                </a>
            </div>
        </nav>
    </div>
</header>

<script type="text/javascript">
    $(function () {
        $('a[nav]').each(function () {
            $this = $(this);
            if ($this[0].href == String(window.location)) {
                $this.closest('li').addClass("active");
            }
        });
    });
</script>

<!-- content -->
<div class="wrap">
    <!-- Main -->
    <div class="container">

        <div class="row">
            <div class="col-md-4 col-md-offset-4 floating-box">
                <div class="panel panel-default">
                    <div class="panel-heading">支付结果</div>
                    <div class="panel-body">
                        <div id="message"></div>
                         <#list resultMap?keys as key>
                            <div class="form-group ">
                                <label class="control-label"><span> ${key} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font color="red">${resultMap["${key}"]}</font> </span></label>
                                <hr>
                            </div>
                         </#list>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <div class="footer-col footer-col-logo hidden-xs hidden-sm">
            <img src="${channelVo.iconUrl}" alt="${channelVo.channelName}">
        </div>
        <div class="footer-col footer-col-copy">
            <div class="copyright">
                <span>Copyright ?  ${channelVo.channelName}</span>
            </div>
        </div>
        <div class="footer-col footer-col-sns hidden-xs hidden-sm">
            <div class="footer-sns">
                <span>Powered by <a href="${channelVo.iconUrl}" target="_blank">${channelVo.channelName}</a></span>
            </div>
        </div>
    </div>
</footer>

</body>
</html>