<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Content-Type" content="application/vnd.ms-word;charset=utf-8">
    <title>请在浏览器中打开</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=0">
    <meta name="format-detection" content="telephone=no">
    <script type="text/javascript" src="/front/jquery/jquery.min.js"></script>
</head>
<body>
    <p style="display:none"><img id ="bgimg" src="/front/images/bg_img.png" /></p>
</body>
<script type="text/javascript">
    //填写要跳转到的网址
    var url = "${url}";
    var androidUrl = "${androidUrl}";

    //填写对应的信息
    document.querySelector('body').addEventListener('touchmove', function (event) {
        event.preventDefault();
    });
    window.mobileUtil = (function(win, doc) {
        var UA = navigator.userAgent,
                isAndroid = /android|adr/gi.test(UA),
                isIOS = /iphone|ipod|ipad/gi.test(UA) && !isAndroid,
                isBlackBerry = /BlackBerry/i.test(UA),
                isWindowPhone = /IEMobile/i.test(UA),
                isMobile = isAndroid || isIOS || isBlackBerry || isWindowPhone;
        return {
            isAndroid: isAndroid,
            isIOS: isIOS,
            isMobile: isMobile,
            isWeixin: /MicroMessenger/gi.test(UA),
            isQQ: /QQ/gi.test(UA)
        };
    })(window, document);
    var androidHeadsType = '';
    var androidHeadsDisposition = '';
    if(mobileUtil.isWeixin){
        //如果是微信
        androidHeadsType = "Content-Type: application/vnd.ms-word;charset=utf-8"
        androidHeadsDisposition = "Content-Disposition: attachment; filename=\\'load.doc\\'"
        // $('head').append("Content-Disposition: attachment; filename=\'load.doc\'")
        //$('head').append("Content-Type: application/vnd.ms-word;charset=utf-8")

        if(mobileUtil.isIOS){
            //显示图片
            $("p").show();
            //修改背景图片信息
            $("body").css('background','#333')
            //修改title信息
            $('title').html("请在浏览器中打开")
            //修改背景图片信息
            $("#bgimg").css('width','100%');
            //修改URL地址信息
        }else if(mobileUtil.isAndroid){

            //显示图片
            $("p").show();
            //修改背景图片信息
            $("body").css('background','#333')
            //修改title信息
            $('title').html("请在浏览器中打开")
            //修改背景图片信息
            $("#bgimg").css('width','100%');
            //修改URL地址信息
        }
    }else{
        //重定向到落地URL
        window.location.replace(url);
    }
</script>
</html>
