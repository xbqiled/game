<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>${vo.channelName}</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=0">
    <meta name="format-detection" content="telephone=no">
    <script type="text/javascript" src="/front/js/pub_rem.js"></script>
    <link rel="stylesheet" href="/front/css/pub_reset.css">
    <!-- Favicons -->
    <link href="${vo.iconUrl}" rel="apple-touch-icon-precomposed">
    <link href="${vo.iconUrl}" rel="shortcut icon">
    <style type="text/css">
        *{
            margin: 0;
            padding: 0;
        }
        .imgTop img{
            width: 100%;
            height: 100%;
            float: left;
        }
        .downloadButton{
            background: url('${vo.backgroundUrl}') no-repeat;
            width: 100%;
            float: left;
            background-size:100%;
        }
        .downloadButton img{
            margin-left: 10%;
            width: 80%;
        }
        .bgBottom {
            float: left;
        }
        .bgBottom img {
            width: 100%;
        }
        .bottom{
            background: rgba(0,0,0,.5);
            left: 0;
            right: 0;
            bottom: 0;
            position: fixed;
        }
        .bottom img{
            width: 100%;
        }
    </style>
</head>

<body>
<div class="main-body">
    <div class="imgTop">
        <img src="${vo.headUrl}" alt="" >
    </div>
    <div class="downloadButton">
        <img src="/front/images/ljxz.gif" id="dlj" alt="">
    </div>
    <div class="bgBottom">
        <img src="${vo.bodyUrl}" alt="">
    </div>
    <div class="bottom">
        <img src="${vo.footUrl}"  id="lj"  alt="">
    </div>
</div>
</body>
<script type="text/javascript" src="/front/jquery/jquery.min.js"></script>
<link rel="preconnect" href="//openinstall.io" crossorigin="use-credentials"/>
<script type="text/javascript" charset="utf-8" src="//res.cdn.openinstall.io/openinstall.js"></script>
<script type="text/javascript">
    //添加openinstall
    //openinstall初始化时将与openinstall服务器交互，应尽可能早的调用
    /*web页面向app传递的json数据(json string/js Object)，应用被拉起或是首次安装时，通过相应的android/ios api可以获取此数据*/
    var data = OpenInstall.parseUrlParams();//openinstall.js中提供的工具函数，解析url中的所有查询参数
    var isInstall = 0;
    var phoneType = 1;
    //var androidAppKey = "${vo.androidAppKey}";
    //var androidPkgid = "${vo.androidPkgid}";
    //var iosAppKey = "${vo.iosAppKey}";
    //var iosPkgid = "${vo.iosPkgid}";
    if (data != null && data != '' && locationUrl != null && locationUrl != '') {
        data['locationUrl'] = locationUrl
        console.log(data)
    }

    new OpenInstall({
        /*appKey必选参数，openinstall平台为每个应用分配的ID*/
        appKey: "${vo.androidAppKey}",
        //_pkgId: "${vo.androidPkgid}",
        /*可选参数，自定义android平台的apk下载文件名，只有apk在openinstall托管时才有效；个别andriod浏览器下载时，中文文件名显示乱码，请慎用中文文件名！*/
        //apkFileName : 'com.XinBo.LoveLottery-v1.0.0.apk',
        /*可选参数，是否优先考虑拉起app，以牺牲下载体验为代价*/
        //preferWakeup:true,
        /*自定义遮罩的html*/
        /*openinstall初始化完成的回调函数，可选*/
        onready: function () {
            var m = this,
                button = document.getElementById("lj");
            var aa = document.getElementById("dlj");
            button.style.visibility = "visible";
            aa.style.visibility = "visible";

            /*在app已安装的情况尝试拉起app*/
            m.schemeWakeup();
            /*用户点击某个按钮时(假定按钮id为downloadButton)，安装app*/
            button.onclick = function () {
                m.wakeupOrInstall();
                return false;
            }

            aa.onclick = function () {
                m.wakeupOrInstall();
                return false;
            }
        }
    }, data);

    //处理打开游戏
    function getMobileType() {
        //首先判断是pc端
        if (isPC()) {
            phoneType = 1;
        } else {
            //判断手机类型
            if (mobileType() == '1') {
                phoneType = 1;
            } else if (mobileType() == '2') {
                phoneType = 2;
            } else {
                phoneType = 1;
            }
        }
    }

    //处理下载
    function downloadGame() {
        var iosUrl = "${vo.iosUrl}";
        var androidUrl = "${vo.androidUrl}";

        //首先判断是pc端
        if (isPC()) {
            if (isInstall == 1) {
                //已经安装
                window.location.href = schemeUrl;
            } else {
                window.location.href = androidUrl;
            }
        } else {
            //判断是否是微信

            //判断是否是微博

            //判断是否是QQ

            //判断手机类型
            if (mobileType() == '1') {
                window.location.href = androidUrl;
            } else if (mobileType() == '2') {
                window.location.href = iosUrl;
            } else {
                window.location.href = androidUrl;
            }
        }
    }

    //判断手机类型
    function mobileType(){
        var u = navigator.userAgent, app = navigator.appVersion;
        var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1; //g
        var isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
        if (isAndroid) {
            //这个是安卓操作系统
            return "1";
        }
        if (isIOS) {
            //这个是ios操作系统
            return "2";
        }
    }
    //判断是否是PC端
    function isPC(){
        var userAgentInfo = navigator.userAgent;
        var Agents = ["Android", "iPhone",
            "SymbianOS", "Windows Phone",
            "iPad", "iPod"];
        var flag = true;
        for (var v = 0; v < Agents.length; v++) {
            if (userAgentInfo.indexOf(Agents[v]) > 0) {
                flag = false;
                break;
            }
        }
        return flag;
    }
    //判断是否是微信
    function isWeixin(){
        var ua = navigator.userAgent.toLowerCase();
        if (ua.match(/MicroMessenger/i) == "micromessenger") {
            return true;
        } else {
            return false;
        }
    }
    //判断是否是微博
    function isWeibo() {
        var ua = navigator.userAgent.toLowerCase();
        if (ua.match(/WeiBo/i) == "weibo") {
            return true;
        } else {
            return false;
        }
    }
    //判断是否QQ
    function isMobileQQ() {
        var ua = navigator.userAgent;
        return /(iPad|iPhone|iPod).*? (IPad)?QQ\/([\d\.]+)/.test(ua) || /\bV1_AND_SQI?_([\d\.]+)(.*? QQ\/([\d\.]+))?/.test(ua);
    }

    //启动执行
    $(function (){
        //open_or_download_app();
        //getMobileType();
    });
</script>
</html>
