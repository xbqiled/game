<!DOCTYPE html>
<html><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name ="viewport" content ="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <style type="text/css">
        *{
            padding: 0;
            margin: 0;
            font-size: 62.5%;
            color: #ffffff;
        }
        li{
            list-style: none;
        }
        h1{
            font-size: 25px;
        }
        body{
            background: rgb(243, 243, 243);
            overflow: hidden;
        }
        a{
            color: #000000;
        }
        @media screen and (-webkit-min-device-pixel-ratio:0) {
            *{
                font-size: 12px;
            }
        }
        .main-body .header{
            width: 100%;
            height: 50px;
            float: left;
            background:#008cff!important;
            position: relative;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .main-body .header .left-arrow{
            position: absolute;
            left: 10px;
            height: 30px;
            top: 10px;
        }
        .main-body .header .left-arrow img{
            height: 100%;
        }
        .main-body .header .right-info{
            position: absolute;
            height: 50px;
            right: 5px;
            top:0;
            line-height: 22px;
            text-align: right;
            font-size: 14px;
        }
        .main-body .chess-box{
            padding:20px 20px;
            background:linear-gradient(#c18fed,#7b8cf4);
            float: left;
            width: 100%;
        }
        .main-body .balance{
            float: left;
            width: 100%;
            height: 46px;
            display: flex;
            align-items: center;
            padding: 0 10px;
        }
        .main-body .balance h3{
            font-size: 14px;
            color: #000000;
            display: flex;
        }
        .main-body .balance span{
            color: red;
            font-size: 14px;
        }
        .main-body .balance img{
            width: 20px;
        }
        .main-body .balance .balanceButton{
            color:#008cff  !important;
            border:1px solid #008cff  !important;
            border-radius: 5px;
            padding: 5px 10px;
            background: none;
            margin-right: 10px;
        }
        .main-body .balance input[type='button']{
            background: linear-gradient(#c692ee,#b18cec);
            border-radius: 5px;
            width: 78px;
            height: 34px;
            border: none;
        }
        .main-body .imgModule{
            padding: 0 10px;
            margin-top: 10px;
            float: left
        }
        .main-body .imgModuleSmall{
            width: 50%;
            box-sizing: border-box;
            padding: 4px;
        }
        .main-body .imgModule img,imgModuleSmall img{
            width: 100%;
        }
        .v-modal{
            position: fixed;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            background:rgba(0,0,0,0.5);
            display: none;
            justify-content: center;
            align-items: center;
            z-index: 2002;
        }
        .v-modal .mint-msgbox{
            width: 85%;
            height: 260px;
            background: #ffffff;
            border-radius: 3px;
            padding: 10px 20px;
            box-sizing: border-box;
            position: relative;
        }
        .v-modal .mint-msgbox h2{
            font-size: 18px;
            text-align: center;
            color: #666;
            margin-top:10px ;
            display: flex;
            justify-content: center;
        }
        .v-modal .mint-msgbox h3{
            font-size: 14px;
            text-align: center;
            color: #666;
            margin-top:10px ;
        }
        .v-modal .mint-msgbox img{
            width: 20px;
            margin: 0 5px;
        }
        .v-modal .mint-msgbox input[type='number']{
            width: 80%;
            height: 30px;
            padding: 3px 8px;
            color: #000000;
            border: 1px solid #dadada;
            border-radius: 4px;
        }
        .v-modal .limitButton{
            position: absolute;
            width: 100%;
            height: 37px;
            left: 0;
            bottom: 0;
            border: 1px solid #ddd;
        }
        .v-modal .limitButton span{
            width: 50%;
            position: absolute;
            box-sizing: border-box;
            height: 36px;
            background: none;
            border: none;
        }
        .v-modal .limitButton span:nth-child(1){
            width: 50%;
            position: absolute;
            box-sizing: border-box;
            height: 36px;
            background: none;
            border: none;
            color: #000000;
            border-right: 1px solid #ddd;
        }

        .v-modal .limitButton span:nth-child(2){
            width: 50%;
            position: absolute;
            box-sizing: border-box;
            height: 36px;
            background: none;
            border: none;
            color: #26a2ff;
            right: 0;
        }
    </style>
</head>
<body>
<form>
<div class="main-body">
    <div class="header">
        <div class="left-arrow">

        </div>
        <h2>
            会员ID:${userId}
        </h2>
        <div class="right-info">
        </div>
    </div>
    <div class="chess-box">
        进入BG真人视讯前，需进行额度转换
    </div>
    <div class="balance">
        <h3>BG余额：<span> ${userBalance} 元</span><img onclick="refurbish()" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/PjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIKICAgICAgICAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB0PSIxNTMwNDIxMTI1OTYzIiBjbGFzcz0iaWNvbiIgdmlld0JveD0iMCAwIDEwMjQgMTAyNCIgdmVyc2lvbj0iMS4xIgogICAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgcC1pZD0iMzI2NCIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMDAiIGhlaWdodD0iMjAwIj4KICAgIDxkZWZzPgogICAgICAgIDxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PC9zdHlsZT4KICAgIDwvZGVmcz4KICAgIDxwYXRoIHN0eWxlPSJmaWxsOiAjOTc4ZGYxOyIgZD0iTTM0OS4wNzMzMTggNjk3Ljg2MjgzN2EyNTkuNDk1ODkzIDI1OS40OTU4OTMgMCAwIDEtOTcuMTg4NDEtMjAyLjY4NDQwNGMwLTEyMS4xNTY1NjMgODIuOTcyNjM4LTIyMi42NTM1NjUgMTk1LjA5OTIxOS0yNTEuNjAxMTA5VjE3Ni41NDkwNTVjLTE0OC40MDEzMSAzMC4xMzQzNDEtMjYwLjExNTA5MiAxNjEuMzI3MDg1LTI2MC4xMTUwOTIgMzE4LjYyOTM3OCAwIDk3LjE2MjYxIDQyLjY3MzExNyAxODQuMzQwNjQgMTEwLjI0MzE4NSAyNDMuODg2OTI0IDUyLjMyMjI5OCA0OC43MzYxMDUgODQuODMwMjM0LTE2LjI3OTc2OCA1MS45NjEwOTgtNDEuMjAyNTJ6TTcyNC4xMjcxODYgMjQ4Ljg2NjMxNGMtMzMuMzA3NzM1LTMwLjA1Njk0MS04Mi4wOTU0NCAxOC43MDQ5NjMtNTAuMTAzNTAyIDQyLjg3OTUxNmEyNTkuNTIxNjkzIDI1OS41MjE2OTMgMCAwIDEgOTguMDkxNDA4IDIwMy40MzI2MDNjMCAxMjEuMTU2NTYzLTgyLjk3MjYzOCAyMjIuNjUzNTY1LTE5NS4wOTkyMTkgMjUxLjU0OTUwOXY2Ny4wNTQwNjljMTQ4LjQyNzExLTMwLjE2MDE0MSAyNjAuMDg5MjkyLTE2MS4zMjcwODUgMjYwLjA4OTI5Mi0zMTguNjAzNTc4IDAtOTguNTA0MjA4LTQzLjc4MjUxNC0xODYuNjYyNjM1LTExMi45Nzc5NzktMjQ2LjMxMjExOXogbS0yNDguNDI3NzE1IDUwLjg3NzUwMWwxMDUuODMxMzkzLTYzLjM2NDY3N2MxNS40MDI1Ny05LjIzNjM4MiAxNS40MjgzNy0yNC4yMjYxNTMgMC4xMDMyLTMzLjUzOTkzNGwtMTA2LjgzNzU5MS02NC42Mjg4NzRjLTE1LjM3Njc3LTkuMjg3OTgyLTI3Ljc2MDc0Ni0yLjI5NjE5Ni0yNy42ODMzNDYgMTUuNjg2MzY5bDAuNTY3NTk5IDEzMC4wNTc1NDZjMC4wNTE2IDE3LjkzMDk2NSAxMi42MTYxNzUgMjUuMDI1OTUxIDI4LjAxODc0NSAxNS43ODk1N3ogbTczLjUwNDA1NiAzOTEuMTc4ODM2bC0xMDYuODExNzkxIDY0LjYyODg3M2MtMTUuMzc2NzcgOS4zMTM3ODItMTUuMzI1MTcgMjQuMjc3NzUzIDAuMDUxNiAzMy41Mzk5MzVsMTA1Ljg1NzE5MyA2My4zNjQ2NzZjMTUuNDAyNTcgOS4yMTA1ODIgMjcuOTY3MTQ1IDIuMTQxMzk2IDI4LjA0NDU0NS0xNS44MTUzNjlsMC41NDE3OTktMTMwLjA1NzU0NmMwLjA3NzQtMTcuOTU2NzY1LTEyLjMzMjM3Ni0yNC45NDg1NTEtMjcuNjgzMzQ2LTE1LjY2MDU2OXoiCiAgICAgICAgICBwLWlkPSIzMjY1Ij48L3BhdGg+Cjwvc3ZnPgo=" alt=""></h3>
        <span class="balanceButton" style="margin-left: 8%" id="goIntoMoney">转入</span>
        <span class="balanceButton" id="goOutMoney">转出</span>
        <input type="button" value="进入游戏" onclick="openGame()">
    </div>
    <div class="imgModule">
        <img src="https://youwanimage.oss-cn-shanghai.aliyuncs.com/bg/bg2.jpg">
    </div>
    <div class="imgModule">
        <img src="https://youwanimage.oss-cn-shanghai.aliyuncs.com/bg/bg1.jpg">
    </div>
    <div class="imgModule">
        <img src="https://youwanimage.oss-cn-shanghai.aliyuncs.com/bg/bg3.jpg">
    </div>
</div>
<div class="v-modal">
    <div class="mint-msgbox">
        <h2>额度转换</h2>
        <h3>信博系统余额 <span style="color: green">0</span></h3>
        <h3>爱棋牌余额 <span style="color: red">0</span></h3>
        <h2 id="xitongLimit">系统
            <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIKCSB2aWV3Qm94PSIwIDAgNDg3LjYyMiA0ODcuNjIyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA0ODcuNjIyIDQ4Ny42MjI7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPGc+Cgk8Zz4KCQk8Y2lyY2xlIHN0eWxlPSJmaWxsOiNFRUQyNkE7IiBjeD0iMjQzLjgxMSIgY3k9IjI0My44MTEiIHI9IjI0My44MTEiLz4KCQk8Y2lyY2xlIHN0eWxlPSJmaWxsOiNDNEUxNUU7IiBjeD0iMjQzLjgxMSIgY3k9IjI0My44MTEiIHI9IjI0My44MTEiLz4KCQk8cGF0aCBzdHlsZT0ib3BhY2l0eTowLjM3O2ZpbGw6I0I1RDI0RjtlbmFibGUtYmFja2dyb3VuZDpuZXcgICAgOyIgZD0iTTQ4Ni41NTQsMjY2LjU4NEwyOTcuMTEsMTQ0LjY4OGwzNS4zMzMsOTkuMTIzCgkJCWwtMTU1LjU0MS05OS4xMjNsNDcuNjIsMTE0Ljk5NmwtMTIwLjIwOSw4My4yNUwzMzEuODQxLDQ3MS4yM0M0MTYuMjI4LDQzOC41NDEsNDc3Ljg3NSwzNjAuMjQ3LDQ4Ni41NTQsMjY2LjU4NHoiLz4KCQk8Zz4KCQkJPHBvbHlnb24gc3R5bGU9ImZpbGw6I0ZGRkZGRjsiIHBvaW50cz0iMTc2LjkwMiwxNDQuNjg4IDEwNC4zMTMsMTQ0LjY4OCAxOTAuNTEyLDI0My44MTEgMTA0LjMxMywzNDIuOTM0IDE3Ni45MDIsMzQyLjkzNCAKCQkJCTI2My4xMDEsMjQzLjgxMSAJCQkiLz4KCQkJPHBvbHlnb24gc3R5bGU9ImZpbGw6I0ZGRkZGRjsiIHBvaW50cz0iMjk3LjExLDE0NC42ODggMjI0LjUyMiwxNDQuNjg4IDMxMC43MjEsMjQzLjgxMSAyMjQuNTIyLDM0Mi45MzQgMjk3LjExLDM0Mi45MzQgCgkJCQkzODMuMzA5LDI0My44MTEgCQkJIi8+CgkJPC9nPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" alt="">BG娱乐</h2>
        <h2 id="qipaiLimit">BG娱乐
            <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIKCSB2aWV3Qm94PSIwIDAgNDg3LjYyMiA0ODcuNjIyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA0ODcuNjIyIDQ4Ny42MjI7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPGc+Cgk8Zz4KCQk8Y2lyY2xlIHN0eWxlPSJmaWxsOiNFRUQyNkE7IiBjeD0iMjQzLjgxMSIgY3k9IjI0My44MTEiIHI9IjI0My44MTEiLz4KCQk8Y2lyY2xlIHN0eWxlPSJmaWxsOiNDNEUxNUU7IiBjeD0iMjQzLjgxMSIgY3k9IjI0My44MTEiIHI9IjI0My44MTEiLz4KCQk8cGF0aCBzdHlsZT0ib3BhY2l0eTowLjM3O2ZpbGw6I0I1RDI0RjtlbmFibGUtYmFja2dyb3VuZDpuZXcgICAgOyIgZD0iTTQ4Ni41NTQsMjY2LjU4NEwyOTcuMTEsMTQ0LjY4OGwzNS4zMzMsOTkuMTIzCgkJCWwtMTU1LjU0MS05OS4xMjNsNDcuNjIsMTE0Ljk5NmwtMTIwLjIwOSw4My4yNUwzMzEuODQxLDQ3MS4yM0M0MTYuMjI4LDQzOC41NDEsNDc3Ljg3NSwzNjAuMjQ3LDQ4Ni41NTQsMjY2LjU4NHoiLz4KCQk8Zz4KCQkJPHBvbHlnb24gc3R5bGU9ImZpbGw6I0ZGRkZGRjsiIHBvaW50cz0iMTc2LjkwMiwxNDQuNjg4IDEwNC4zMTMsMTQ0LjY4OCAxOTAuNTEyLDI0My44MTEgMTA0LjMxMywzNDIuOTM0IDE3Ni45MDIsMzQyLjkzNCAKCQkJCTI2My4xMDEsMjQzLjgxMSAJCQkiLz4KCQkJPHBvbHlnb24gc3R5bGU9ImZpbGw6I0ZGRkZGRjsiIHBvaW50cz0iMjk3LjExLDE0NC42ODggMjI0LjUyMiwxNDQuNjg4IDMxMC43MjEsMjQzLjgxMSAyMjQuNTIyLDM0Mi45MzQgMjk3LjExLDM0Mi45MzQgCgkJCQkzODMuMzA5LDI0My44MTEgCQkJIi8+CgkJPC9nPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" alt="">系统</h2>
        <h3><input type="number" placeholder="请输入转换额度" id="limitValue"></h3>
        <div class="limitButton">
            <span onclick="$('.v-modal').hide()">取消</span>
            <span onclick="getLimitMoney()">确定</span>
        </div>
    </div>
</div>
</form>
</body>
<script type="text/javascript" src="/front/jquery/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#goIntoMoney").click(function () {
            $(".v-modal").css('display','flex')
            $("#qipaiLimit").hide()
            $("#xitongLimit").show()
        })
        $("#goOutMoney").click(function () {
            $(".v-modal").css('display','flex')
            $("#qipaiLimit").show()
            $("#xitongLimit").hide()
        })
    })
    function refurbish() {
        alert('点击了刷新金额按钮')
    }
    function openGame() {
        var isMobileUrl = ${isMobileUrl};
        var isHttpsUrl = ${isHttpsUrl};
        var userId = ${userId};
        var channelId = ${channelId};
        var url = "${url}";
        var postUrl = url.concat("/bgplatform/openGame?isMobileUrl=", isMobileUrl, "&isHttpsUrl=", isHttpsUrl, "&userId=" , userId, "&channelId", channelId);
        console.log(postUrl)
        window.location.href = postUrl;
    }
    function getLimitMoney() {
        var money = $("#limitValue").val()
        $(".v-modal").hide()
        $("#limitValue").val('')
    }
</script>
</html>