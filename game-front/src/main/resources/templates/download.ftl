<!DOCTYPE html>
<html style="font-size: 50px;">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <style type="text/css">
        @charset "gbk";
        body, div, html, li, p, span, ul {
            list-style: none;
            margin: 0;
            padding: 0;
            font-size: .3rem;
            box-sizing: border-box
        }

        a {
            text-decoration: none
        }

        body {
            position: relative;
            background: #f2f2f2
        }

        header {
            position: fixed;
            z-index: 999;
            top: 0;
            left: 0;
            margin: 0;
            padding: 0;
            right: 0;
            height: 1.3rem;
            background: #1b1c32
        }

        .flex-center {
            display: flex;
            justify-content: center;
            align-items: center
        }

        .top {
            position: relative;
            height: 1.3rem;
            margin: 0;
            padding: 0;
            overflow: hidden
        }

        .top_mid {
            width: 1.31rem;
            height: .47rem;
            margin: 0 auto 0 auto;
            background-size: 100% 100%;
            display: block
        }

        .top_left {
            position: absolute;
            left: 0;
            top: 0;
            right0;
            display: inline-block;
            width: 1.3rem;
            line-height: 1rem;
            padding-top: .3rem;
            text-align: center
        }

        .hidden .menu_icon {
            background-position: left -.84rem
        }

        .menu_icon, .tel_icon {
            width: .64rem;
            height: .64rem;
            display: inline-block
        }

        .menu_icon {
            background: url(/front/images/menu_tb.png) no-repeat;
            background-size: 100% auto;
            transition: all .3s linear
        }

        .tel_icon {
            background: url(/front/images/tel.png) no-repeat;
            background-size: 100% auto
        }

        .top_right {
            border-left: 1px solid #363646;
            line-height: 1.3rem;
            background: #222335;
            font-size: .38rem;
            position: absolute;
            right: 0;
            top: 0;
            color: #fff;
            display: inline-block;
            width: 1.85rem;
            text-align: center
        }

        .top_right img {
            width: .5rem;
            height: .5rem;
            vertical-align: middle
        }

        .menu {
            z-index: 99;
            position: fixed;
            top: 1.3rem;
            width: 100%;
            transition: transform .35s linear;
            transform: translate3d(0, -100%, -10rem)
        }

        .menu_list {
            background: #1b1c32;
            padding-top: .3rem;
            overflow: hidden
        }

        .show {
            transform: translate3d(0, 0, 0)
        }

        .nav_list a {
            padding-left: .3rem;
            display: block;
            border-bottom: 1px solid #303044;
            color: #fff;
            line-height: 1.2rem;
            font-size: .38rem
        }

        .kfqq {
            padding-left: .3rem;
            line-height: 1.6rem;
            color: #fff;
            font-size: .44rem
        }

        .main {
            padding-top: 1.3rem;
            width: 100%;
            overflow: hidden
        }

        .index_banner {
            border-radius: 0.2rem;
            margin: 0.3rem auto;
            width: 7rem;
            height: 8.0rem;
            background: url(/front/images/b1.jpg) no-repeat left top;
            background-size: 100%;
        }

        .index_banner a {
            display: block;
            margin-top: 6.5rem;
            float: left;
            width: 45%;
            height: 0.99rem;
            margin-left: 10px;
        }

        .btn_new { /* right:-1px; *//* bottom:-1px; *//* border:none; *//* border-radius:30px 0 0 30px; *//* outline:0; *//* background:linear-gradient(#ffd046,#ffa315); *//* color:#fff; *//* font-size:15px; *//* line-height:.85rem; *//* text-align:center; *//* font-weight:700; */
            background: url(../images/btn1.png) no-repeat left top;
            background-size: 100% 100%;
        }

        .btn_old { /* background-size:100% 100%; *//* right:-1px; *//* bottom:-1px; *//* border:none; *//* border-radius:30px 0 0 30px; *//* outline:0; *//* background:linear-gradient(#ae61ba,#7b4484); *//* color:#fff; *//* font-size:15px; *//* line-height:.85rem; *//* text-align:center; *//* font-weight:700 */
            background: url(../images/btn2.png) no-repeat left top;
            background-size: 100% 100%;
        }

        .games {
            width: 7rem;
            height: auto;
            margin: 0 auto .35rem;
            border-radius: .2rem;
            padding: 10px;
            background: #fff;
            box-shadow: 0px 0px 10px #dfe0e3;
        }

        .games_list li {
            overflow: hidden;
            border-bottom: 1px solid #e4e4e4;
            padding: 0.2rem 0rem 0.2rem 0rem;
        }

        .games_list li span {
            display: block;
            float: left
        }

        .gl_icon {
            position: relative;
            width: 1.2rem;
            height: 1.2rem;
            border-radius: .1rem;
            overflow: hidden
        }

        .gl_icon img {
            width: 100%
        }

        .gl_icon label.play {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%
        }

        .gl_icon label.play em {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: #000;
            opacity: .4
        }

        .gl_icon label.play i {
            position: relative;
            width: 100%;
            height: 100%;
            display: inline-block;
            background: url(/images/icon_video_play.png) no-repeat center;
            background-size: 50%
        }

        .gl_info {
            width: 5.3rem;
            padding-left: .2rem;
        }

        .gl_info i {
            display: block;
            font-style: inherit
        }

        .gli_tit {
            color: #3d3e4d;
            font-size: .32rem;
            padding-bottom: .1rem
        }

        .gli_wz {
            font-size: .24rem;
            color: #989898;
            line-height: .34rem;
        }

        .gl_down {
            display: block;
            margin-top: .1rem;
            text-align: center;
            float: right;
            background: linear-gradient(to bottom, #ffda1b, #fea81b);
            border-radius: .27rem;
            font-size: .24rem;
            line-height: .54rem;
            width: 2.5rem;
            color: #fff;
            padding: 0px 5px 0px 5px;
        }

        .game_more {
            text-align: center;
            padding-top: .2rem;
        }

        .game_more a {
            color: #3d3e4d;
            font-size: .34rem
        }

        .news {
            width: 7rem;
            margin: .5rem auto
        }

        .news_tit {
            line-height: .6rem;
            font-size: .44rem;
            color: #63647e;
            text-align: center
        }

        .news_tit span {
            display: block;
            font-size: .28rem;
            color: #9d9eae;
            font-weight: lighter
        }

        .news_list {
            padding: 0 .3rem
        }

        .news_list li {
            border-bottom: 1px dashed #cccddd;
            padding: .3rem 0
        }

        .nl_tit {
            display: inline-block;
            width: 100%;
            font-size: .28rem;
            color: #3d3e4d;
            line-height: .4rem
        }

        .nl_num {
            color: #9d9eae
        }

        .nl_num span {
            padding-right: .2rem;
            font-size: .24rem
        }

        .news_more {
            padding: .3rem;
            text-align: right
        }

        .news_more a {
            color: #63647e;
            font-size: .34rem
        }

        .help {
            background: #1b1c32;
            padding: .25rem
        }

        .help_tit {
            text-align: center;
            padding-top: .5rem;
            color: #fff;
            font-size: .44rem
        }

        .help_tit span {
            display: block;
            font-weight: lighter;
            line-height: .6rem
        }

        .compro {
            border-radius: .2rem;
            width: 7rem;
            margin: .3rem auto;
            background: linear-gradient(#7b90fb, #775eff);
            height: 4.2rem
        }

        .cp_tit {
            text-align: center;
            line-height: 1.2rem
        }

        .cp_tit i {
            font-style: normal;
            text-shadow: 0 0 5px #4c50b3;
            color: #fff;
            font-weight: 600;
            font-size: .44rem
        }

        .hlist {
            padding-left: .5rem
        }

        .hlist li {
            font-size: .28rem;
            line-height: .6rem
        }

        .hlist li a {
            color: #fff
        }

        .hmore {
            text-align: right;
            padding-right: .5rem;
            padding-top: .3rem
        }

        .hmore a {
            color: #fff
        }

        .newhelp {
            border-radius: .2rem;
            width: 7rem;
            margin: .3rem auto;
            background: linear-gradient(#84b0f7, #7c74e4);
            height: 4.2rem
        }

        .bottom {
            width: 7rem;
            margin: 0 auto;
            padding-bottom: .3rem
        }

        .b_menu {
            padding: .2rem .8rem;
            overflow: hidden
        }

        .b_menu li {
            float: left;
            padding: .3rem;
            font-size: .28rem
        }

        .b_menu li a {
            color: #63647e
        }

        .b_list span {
            display: block;
            text-align: center;
            line-height: .4rem;
            font-size: .24rem;
            color: #9d9eae
        }

        .clear {
            clear: both;
            font-size: 1px;
            height: .4rem
        }

        .banner {
            width: 7.5rem;
            margin: 0 auto
        }

        .game_banner {
            background: url(/images/nb1.jpg) no-repeat left top;
            background-size: 100% 100%;
            padding-top: 3.1rem
        }

        .news_banner {
            background: url(/images/nb2.jpg) no-repeat left top;
            background-size: 100% 100%;
            height: 2.5rem
        }

        .nbtn {
            margin: 0 auto;
            width: 4.4rem;
            box-shadow: 0 5px 10px #ddd;
            border-radius: .44rem;
            font-size: .38rem;
            color: #fff;
            text-align: center;
            display: block;
            line-height: .9rem;
            background: #feb108
        }

        .nbtn img {
            width: .42rem;
            height: .42rem;
            vertical-align: -.07rem;
            margin-right: .1rem
        }

        .gameinfo {
            width: 7rem;
            height: 2.78rem;
            background: #fff;
            box-shadow: 0 5px 10px #e0e1e4;
            margin: 0 auto;
            border-radius: .2rem
        }

        .gameinfo p {
            display: inline-block
        }

        .gi_left {
            width: 4.3rem;
            float: left
        }

        .gi_left span {
            display: block;
            padding: .2rem 0 0 .36rem
        }

        .gil_tit {
            font-size: .38rem;
            line-height: .6rem;
            color: #3d3e4d
        }

        .gil_del {
            font-size: .24rem;
            color: #63647e;
            line-height: .4rem
        }

        .gi_right {
            width: 2.6rem;
            height: 2.6rem;
            float: right
        }

        .gi_right img {
            vertical-align: middle;
            width: 100%
        }

        .nb_tit {
            text-align: center;
            color: #fff;
            line-height: .66rem;
            padding-top: .6rem;
            font-size: .54rem
        }

        .nb_tit span {
            display: block;
            font-weight: lighter
        }

        .newcenter {
            overflow: hidden
        }

        .newmenu {
            list-style: none;
            padding: 0 .3rem;
            display: flex;
            background: #fff;
            height: .9rem;
            border-bottom: 2px solid #e6e6f1;
            position: relative
        }

        .newmenu li {
            flex: 1;
            text-align: center;
            line-height: .9rem;
            height: .9rem;
            font-size: .34rem;
            color: #bfc0cf
        }

        .newmenu li.active {
            border-bottom: 2px solid #feb108;
            color: #feb108
        }

        .newcenter .content {
            padding: 0 .3rem;
            margin: 15px 0
        }

        .newcenter .content > div {
            display: none
        }

        .newcenter .content > div.active {
            display: block
        }

        .newslist {
            overflow: hidden;
            margin-top: -.3rem
        }

        .newslist ul li {
            line-height: .9rem;
            font-size: .28rem;
            border-bottom: 1px dashed #cccddd
        }

        .newslist ul li > * {
            line-height: .9rem
        }

        .newslist ul li a.title {
            width: calc(100% - 1rem);
            display: inline-block;
            line-height: 1;
            color: #3d3e4d
        }

        .newslist ul li span {
            float: right;
            color: #9d9eae
        }

        .nt {
            padding: .2rem .3rem
        }

        .nt .nt_top {
            line-height: .6rem;
            color: #3d3e4d;
            font-size: .38rem;
            font-weight: 600
        }

        .nt .nt_num {
            line-height: .3rem;
            color: #9d9eae;
            font-size: .24rem
        }

        .ni {
            padding: .2rem .3rem
        }

        .ni img {
            max-width: 100%
        }

        .ni p {
            padding-bottom: .2rem;
            line-height: .4rem;
            font-size: .28rem;
            color: #3d3e4d
        }

        .ni_cont {
            padding: 0 .3rem
        }

        .ni_cont p {
            line-height: .4rem;
            font-size: .28rem;
            color: #3d3e4d
        }

        .azjc {
            padding: .5rem .3rem;
            background: #212126
        }

        .az_tit {
            text-align: center
        }

        .az_tit img {
            width: 60%
        }

        .az_info span {
            color: #fff;
            line-height: .8rem;
            font-size: .28rem;
            margin: .3rem 0;
            display: block
        }

        .az_info img {
            width: 100%
        }

        .ljxz {
            position: fixed;
            bottom: 0;
            width: 100%
        }

        .ljxz a {
            display: block;
            width: 100%;
            line-height: 1rem;
            background: #feb108;
            color: #000;
            font-size: .36rem;
            text-align: center
        }

        .ellipsis {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap
        }

        .vdo-player {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 9999
        }

        .vdo-player em {
            position: absolute;
            width: 100%;
            height: 100%;
            background: #000;
            opacity: .7
        }

        .vdo-player video {
            position: relative;
            background: #000
        }

        .gg {
            background-size: 100% 100%; /* margin: 0px 5px 10px 5px; */
            height: 0.5rem; /* border-radius: 0.2rem; */
            margin: 0.0rem auto 0.5rem auto;
            width: 7rem;
            margin-top: -20px;
        }

        .zuo {
            float: left;
            width: 49.5%;
            background: #fec211a8;
            padding: 5px 0 5px 5px;
            border-radius: 10px 0 10px 0;
        }

        .you {
            float: right;
            width: 49.5%;
            background: #ae61baa8;
            padding: 5px 0 5px 5px;
            border-radius: 0 10px 0 10px;
        }

        .gg a {
            color: #3d3e4d
        }

        .gg li {
            color: #fffcf7
        }

        .zuo ul li {
            text-align: -webkit-auto
        }

        .gg zuo ul li {
            text-align: left
        }

        .you ul li {
            text-align: -webkit-auto
        }

        .gg a {
            font-size: .35rem
        }

        .zuo a {
            color: #fff;
            font-weight: bolder
        }

        .you a {
            color: #fff;
            font-weight: bolder
        }

        a.bei {
            display: block;
            margin-top: 6.5rem;
            float: left;
            width: 20%;
            height: .85rem;
            background: linear-gradient(#ffa315, #ffd046);
            border-radius: 0 20px 20px 0;
            color: #fff;
            line-height: .85rem;
            text-align: center;
        }

        a.bei2 {
            display: block;
            margin-top: 6.5rem;
            float: left;
            width: 20%;
            height: .85rem;
            background: linear-gradient(#7b4484, #ae61ba);
            border-radius: 0 20px 20px 0;
            color: #fff;
            line-height: .85rem;
            text-align: center;
        }

        .footer {
            margin: 0 5%;
            padding-bottom: 10px;
            float: left;
            width: 90%
        }

        .footer .list {
            margin-top: 3%;
            float: left;
            width: 100%
        }

        .footer .no {
            height: 42px;
            font-weight: 700
        }

        .footer .no span {
            color: #fff;
            font-size: 200%;
            background: url(/images/nobg.png) no-repeat center center;
            display: block;
            float: left;
            width: 12%;
            padding-left: 2%;
            background-size: 88%;
            height: 42px;
            line-height: 42px;
            font-style: italic
        }

        .footer .no i {
            width: 85%;
            color: #4da1ec;
            line-height: 35px;
            padding-top: 7px;
            display: block;
            float: left;
            font-size: 140%
        }

        .footer .img {
            background: #ecf1f7;
            border-radius: 10px;
            width: 94%;
            padding: 3%;
            margin-top: 2%
        }

        .footer .img img {
            width: 100%
        }
    </style>
<body>
<header>
    <div class="top">
        <a id="nav" href="javascript:void(0);" onclick="navToggle()" class="top_left">
            <i class="menu_icon"></i>
        </a>
        <a id="indexa" href="#" class="top_mid"
           style="background:url('${vo.logoUrl}') no-repeat;background-size:100% 100%; width:117px;height:65px;"></a>
        <!--
        <a id="right" class="top_right" onclick="toCs()">
            <i class="tel_icon"></i>
        </a>-->
        <a onclick="toCs()" class="top_right">
            <img src="/front/images/tel.png">客服
        </a>
    </div>
    <script>
        //事件
        function navToggle() {
            if ($('.top_left').hasClass('hidden')) {
                $('.top_left').removeClass('hidden');
                $('.menu').removeClass('show');
            } else {
                $('.top_left').addClass('hidden');
                $('.menu').addClass('show');
            }
        }
    </script>
</header>
<div class="menu">
    <div class="menu_list">
        <nav class="nav_list">
            <a href="${vo.csUrl}">${vo.channelName}棋牌在线客服</a>
        </nav>
    </div>
</div>
<div class="main">
    <div class="index_banner" style="background:url('${vo.backgroundUrl}') no-repeat left top;background-size:100%;">
        <a href="#" onclick="downLoad()" id="down1_1" class="btn_new"></a>
        <a href="#  " onclick="downLoad()" id="down1_2" class="btn_old"></a>
    </div>
    <div class="games" id="ios-wrap">
        <ul class="games_list">
            <li>
                <span class="gl_icon">
                    <img src="/front/images/jy.png">
                    <label><em></em><i></i></label>
                </span>
                <span class="gl_info">
                        <i class="gli_tit">内置聊天室<font color="red">【美女随意撩】</font></i>
                        <i class="gli_wz">游戏内置聊天系统，互加好友，聊天室(自动加入随便聊)。<br/>不想玩游戏？没关系，当做社交APP使用，隐蔽约炮，从此不再怕老婆查手机！</i>
                    </span>
                <a href="javascript:downLoad();" class="gl_down">${vo.channelName}棋牌备用下载</a>
            </li>
            <li>
                <span class="gl_icon">
                    <img src="/front/images/dj2.png">
                    <label><em></em><i></i></label>
                </span>
                <span class="gl_info">
                        <i class="gli_tit">超级代理系统<font color="red">【日赚斗金】</font></i>
                        <i class="gli_wz">无限下级，躺赚从现在开始！<br/>代理功能强大，下线的游戏、账变都有明细，每一分收益都能看得见！</i>
                    </span>
                <a href="javascript:downLoad();" class="gl_down">${vo.channelName}棋牌备用下载</a>
            </li>
            <li>
                <span class="gl_icon">
                    <img src="/front/images/icon8.png">
                        <label><em></em><i></i></label>
                </span>
                <span class="gl_info">
                        <i class="gli_tit">斗地主百人大赛<font color="red">【竞技】</font></i>
                        <i class="gli_wz">百人大赛，奖金高达30倍！<br/>是时候展示真正的技术了！</i>
                    </span>
                <a href="javascript:downLoad();" class="gl_down">${vo.channelName}棋牌备用下载</a>
            </li>
            <li>
                <span class="gl_icon">
                    <img src="/front/images/dblistlb.png">
                    <label><em></em><i></i></label>
                </span>
                <span class="gl_info">
                    <i class="gli_tit">${vo.activityMsg}
                            <!--<a href="javascript:alert('${vo.csTips}')">点击办理</a>--></i>
                </span>
                <a href="javascript:downLoad();" class="gl_down">${vo.channelName}棋牌备用下载</a>
            </li>
            <li>
                <span class="gl_icon">
                    <img src="/front/images/wxhh.png">
                    <label><em></em><i></i></label>
                </span>
                <span class="gl_info">
                        <i class="gli_tit">五星宏辉<font color="red">【火爆】</font></i>
                        <i class="gli_wz">经典重现，单挑王再次亮剑于江湖！<br/>
                                          梦中的回忆，还是一样的感觉!</i>
                    </span>
                <a href="javascript:downLoad();" class="gl_down">${vo.channelName}棋牌备用下载</a>
            </li>
            <li>
                    <span class="gl_icon">
                        <img src="/front/images/icon1.png">
                        <label><em></em><i></i></label>
                    </span>
                <span class="gl_info">
                        <i class="gli_tit">捕鱼达人</i>
                        <i class="gli_wz">超丰富鱼类，尽显绚丽海底美景， 升级炮弹，抓捕大鱼，超高倍数奖 励等你拿！</i>
                    </span>
                <a href="javascript:downLoad();" class="gl_down">${vo.channelName}棋牌备用下载</a>
            </li>
            <li>
                    <span class="gl_icon">
                        <img src="/front/images/icon2.png">

                        <label><em></em><i></i></label>
                    </span>
                <span class="gl_info">
                        <i class="gli_tit">扎金花</i>
                        <i class="gli_wz">经典拼花，真人对战更刺激，操作简单一不小心变土豪，画面精致难以抗拒</i>
                    </span>
                <a href="javascript:downLoad();" class="gl_down">${vo.channelName}棋牌备用下载</a>

            </li>
            <li>
                    <span class="gl_icon">
                        <img src="/front/images/icon3.png">

                        <label><em></em><i></i></label>
                    </span>
                <span class="gl_info">
                        <i class="gli_tit">抢庄牛牛</i>
                        <i class="gli_wz">多种玩法，多种押注，抢庄，坐庄，更高级，更血腥的战场—在这里只有王者的巅峰对决</i>
                    </span>
                <a href="javascript:downLoad();" class="gl_down">${vo.channelName}棋牌备用下载</a>

            </li>
            <li>
                    <span class="gl_icon">
                        <img src="/front/images/icon4.png">

                        <label><em></em><i></i></label>
                    </span>
                <span class="gl_info">
                        <i class="gli_tit">龙虎斗</i>
                        <i class="gli_wz">超刺激的玩法，最真实的游戏画面，带给你最精致的视觉体验！</i>
                    </span>
                <a href="javascript:downLoad();" class="gl_down">${vo.channelName}棋牌备用下载</a>

            </li>
            <li>
                    <span class="gl_icon">
                        <img src="/front/images/icon5.png">

                        <label><em></em><i></i></label>
                    </span>
                <span class="gl_info">
                        <i class="gli_tit">水果机</i>
                        <i class="gli_wz">风靡全球的最经典老虎机，高倍数、高奖池、高中奖率，超高奖池等你来拿！</i>
                    </span>
                <a href="javascript:downLoad();" class="gl_down">${vo.channelName}棋牌备用下载</a>
            </li>
            <li>
                    <span class="gl_icon">
                        <img src="/front/images/icon6.png">

                        <label><em></em><i></i></label>
                    </span>
                <span class="gl_info">
                        <i class="gli_tit">奔驰宝马</i>
                        <i class="gli_wz">游戏超级跑车戏色彩绚丽，玩法新颖, 全网最流行的转盘游戏，简单易中，可上庄秒变土豪</i>
                    </span>
                <a href="javascript:downLoad();" class="gl_down">${vo.channelName}棋牌备用下载</a>

            </li>
            <li>
                    <span class="gl_icon">
                        <img src="/front/images/icon7.png">

                        <label><em></em><i></i></label>
                    </span>
                <span class="gl_info">
                        <i class="gli_tit">斗地主</i>
                        <i class="gli_wz">一二三四五，快来斗地主，只要有梦想，农民变地主。</i>
                    </span>
                <a href="javascript:downLoad();" class="gl_down">${vo.channelName}棋牌备用下载</a>
            </li>
        </ul>
    </div>
</div>
<div class="bottom">
    <p class="b_list">
        <span>Copyright ©2019 ${vo.channelName}棋牌</span>
    </p>
</div>
</body>
<script type="text/javascript" src="/front/jquery/jquery.min.js"></script>
<link rel="preconnect" href="//openinstall.io" crossorigin="use-credentials"/>
<script type="text/javascript" charset="utf-8" src="//res.cdn.openinstall.io/openinstall.js"></script>
<script type="text/javascript">
    //添加openinstall
    //openinstall初始化时将与openinstall服务器交互，应尽可能早的调用
    /*web页面向app传递的json数据(json string/js Object)，应用被拉起或是首次安装时，通过相应的android/ios api可以获取此数据*/
    var data = OpenInstall.parseUrlParams();//openinstall.js中提供的工具函数，解析url中的所有查询参数
    var m = this;
    var locationUrl = document.referrer;
    if (data != null && data != '' && locationUrl != null && locationUrl != '') {
        var url = ''
        if (isURL(locationUrl)) {
            if (locationUrl.indexOf("https") == 0) {
                url = locationUrl.substr(8, locationUrl.length)
            } else if (locationUrl.indexOf("http") == 0) {
                url = locationUrl.substr(8, locationUrl.length)
            }

            if (url != null && url != '') {
                if (url.substr(url.length - 1, url.length) == "\/") {
                    url = url.substr(0, url.length - 1)
                }
            }
        }

        if (url != null && url != '') {
            data['locationUrl'] = url
        }
        console.log(data)
    }

    function isURL(str) {
        return !!str.match(/(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/g);
    }

    new OpenInstall({
        /*appKey必选参数，openinstall平台为每个应用分配的ID*/
        appKey: "${vo.androidAppKey}",
        //_pkgId: "${vo.androidPkgid}",
        /*可选参数，自定义android平台的apk下载文件名，只有apk在openinstall托管时才有效；个别andriod浏览器下载时，中文文件名显示乱码，请慎用中文文件名！*/
        //apkFileName : 'com.XinBo.LoveLottery-v1.0.0.apk',
        /*可选参数，是否优先考虑拉起app，以牺牲下载体验为代价*/
        //preferWakeup:true,
        /*自定义遮罩的html*/
        /*openinstall初始化完成的回调函数，可选*/
        onready: function () {
            m = this,
                /*在app已安装的情况尝试拉起app*/
                m.schemeWakeup();
        }
    }, data);

    function downLoad() {
        m.wakeupOrInstall();
        return false;
    }

    function toCs() {
        var csUrl = "${vo.csUrl}";
        window.location.href = csUrl;
    }
</script>
</html>
