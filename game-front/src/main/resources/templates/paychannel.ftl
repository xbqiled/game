<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>${vo.channelName}支付</title>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <meta name="renderer" content="webkit">
    <meta name="theme-color" content="#fff">
    <meta name="author" content="lion">
    <meta name="keywords" content="Anatole">
    <meta name="description" content="Anatole">
    <link type="text/css" rel="stylesheet" href="/front/css/font-awesome.min.css">
    <link rel="stylesheet" href="/front/css/blog_basic.min.css">
    <link type="text/css" rel="stylesheet" href="/front/css/OwO.min.css">
    <link type="text/css" rel="stylesheet" href="/front/css/comment.min.css">
    <link type="text/css" rel="stylesheet" href="/front/css/style.min1.css">

    <link href="${vo.iconUrl}" rel="apple-touch-icon-precomposed">
    <link href="${vo.iconUrl}" rel="shortcut icon">
    <script src="/front/js/jquery.js"></script>
    <style>
        ::-webkit-scrollbar {
            width: 6px;
            height: 6px;
            background-color: #eee;
        }

        ::-webkit-scrollbar-thumb {
            background-color: #3798e8;
        }

        ::-webkit-scrollbar-track {
            background-color: #eee;
        }

        .arrow-line {
            display: inline-block;
            width: 0;
            height: 0;
            border: 10px solid transparent;
            border-top-color: #000;
            transform: rotate(630deg);
        }

        .arrow-right {
            float: left;
            border-width: 8px;
            border-top-color: #fff;
            margin-top: -11px;
            margin-left: -8px;
            transform: rotate(0deg);
        }
    </style>
</head>
<body style="zoom: 1;">
<div class="sidebar animated fadeInDown">
    <div class="logo-title">
        <div class="title">
            <img src="${vo.iconUrl}" style="width:114px;">
            <h3 title="">
                <a href="#">${vo.channelName}</a>
            </h3>
            <div class="description">
                <p>第一国人游戏</p>
            </div>
        </div>
    </div>
    <div class="footer">
        <a target="_blank" href="#">
            <span>Designed by </span>
        </a><a href="#">${vo.channelName}</a>
        <div class="by_halo">
            <a href="#" target="_blank">2018-2020</a>
        </div>
    </div>
</div>
<div class="main">
    <div class="autopagerize_page_element">
        <div class="content" >
          <#list payChannelList as payChannel>
              <div class="comment-container" onclick="doPay(${payChannel.id}, ${channelId}, ${userId})">
                  <ul class="comment-list" id="comments-list">
                      <li class="comment-list-one" id="comment-id-4">
                          <div class="comment-img-rigth">
                              <img class="comment-list-one-img" src="${payChannel.icon}">
                          </div>
                          <div class="post animated fadeInDown">
                              <div class="post-title" style="width: 150%;">
                                  <h3>
                                      <a href="#">${payChannel.payName}</a>
                                  </h3>
                              </div>
                              <div class="post-content">
                                  <div class="p_part">
                                      <p>${payChannel.company!}
                                          <span class="border right"></span>
                                      </p>
                                  </div>
                                  <div class="p_part">
                                      <p></p>
                                  </div>
                              </div>
                              <div class="post-footer">
                                  <div class="meta">
                                      <div class="info">
                                          <i class="fa fa-window-minimize">${payChannel.min}</i>
                                          <i class="fa fa-window-maximize">${payChannel.max}</i>
                                      </div>
                                  </div>
                              </div>
                              <div>
                              </div>
                          </div>
                      </li>
                  </ul>
              </div>
          </#list>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">
    function doPay(configId, channelId, userId) {
        var contextUrl = "${url}";
        var url = contextUrl + "/pay/doPay";
        get(url, configId, channelId, userId);
    }

    function get(URL, configId, channelId, userId) {
        var temp = document.createElement("form");
        temp.method = "get";
        temp.style.display = "none";
        temp.action = URL;

        var configIdInput = document.createElement("input");
        configIdInput.type="hidden";
        //传入参数名,相当于get请求中的content=
        configIdInput.name= "configId";
        //传入数据，只传递了一个参数内容，实际可传递多个。
        configIdInput.value= configId;

        var channelIdInput = document.createElement("input");
        channelIdInput.type="hidden";
        //传入参数名,相当于get请求中的content=
        channelIdInput.name= "channelId";
        //传入数据，只传递了一个参数内容，实际可传递多个。
        channelIdInput.value= channelId;

        var userIdInput = document.createElement("input");
        userIdInput.type="hidden";
        //传入参数名,相当于get请求中的content=
        userIdInput.name= "userId";
        //传入数据，只传递了一个参数内容，实际可传递多个。
        userIdInput.value= userId;

        temp.appendChild(configIdInput);
        temp.appendChild(channelIdInput);
        temp.appendChild(userIdInput);

        document.body.appendChild(temp);
        temp.submit();
        return temp;
    }
</script>
</html>