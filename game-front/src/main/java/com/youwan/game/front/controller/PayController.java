package com.youwan.game.front.controller;


import cn.hutool.core.util.StrUtil;
import com.youwan.game.cac.api.dto.PayOrderDto;
import com.youwan.game.cac.api.entity.TChannel;
import com.youwan.game.cac.api.feign.RemoteChannelService;
import com.youwan.game.cac.api.feign.RemotePayOrderService;
import com.youwan.game.cac.api.feign.RemotePayService;
import com.youwan.game.cac.api.vo.*;
import com.youwan.game.common.core.constant.ConfigConstant;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.constant.UrlConstant;
import com.youwan.game.common.core.util.R;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <B>处理前端支付页面显示</B>
 */

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/pay")
public class PayController {

    private final RemotePayService remotePayService;

    private final RemoteChannelService remoteChannelService;

    private final RemotePayOrderService remotePayOrderService;

    /**
     * <B>支付列表页面</B>
     * @param channelId
     * @param userId
     * @return
     */
    @GetMapping("/channel")
    public ModelAndView payPage(Model model, String  channelId, String userId){
        if (StrUtil.isBlank(channelId) || StrUtil.isBlank(userId)) {
            return null;
        }

        R<List<PayChannelVo>> r = remotePayService.queryPayChannel(channelId, SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return null;
        }

        R<TChannel> channel = remoteChannelService.getChannelInfo(channelId, SecurityConstants.FROM_IN);
        if (channel == null || channel.getData() == null) {
            return null;
        }

        List<PayChannelVo> payChannelList = r.getData();
        TChannel vo = channel.getData();
        model.addAttribute("vo", vo);

        model.addAttribute("userId", userId);
        model.addAttribute("channelId", channelId);
        model.addAttribute("payChannelList", payChannelList);

        model.addAttribute("url", UrlConstant.FEONT_SYSTM_URL);
        return new ModelAndView("paychannel");
    }


    /**
     * <B>跳转到支付处理页面</B>
     * @param model
     * @param channelId
     * @param configId
     * @param userId
     * @return
     */
    @RequestMapping(value = "/doPay", method = RequestMethod.GET)
    public ModelAndView doPay(Model model, String  channelId, String configId, String userId){
        if (StrUtil.isBlank(channelId) || StrUtil.isBlank(userId) || StrUtil.isBlank(configId)) {
            return null;
        }

        //获取渠道信息
        R<TChannel> channel = remoteChannelService.getChannelInfo(channelId, SecurityConstants.FROM_IN);
        if (channel == null || channel.getData() == null) {
            return null;
        }
        TChannel channelVo = channel.getData();

        //获取配置信息
        R<PayChannelVo> r  = remotePayService.queryPayConfig(configId, SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return null;
        }

        PayChannelVo payChannelVo = r.getData();
        //银行入款
        Map<String, String> payMap = new HashMap<>();
        //判断信息,银行打款
        if (payChannelVo.getType() == 1) {
            payMap.put("收款银行", payChannelVo.getMerchantKey());
            payMap.put("收款账号", payChannelVo.getAccount());
            payMap.put("收款人", payChannelVo.getMerchantCode());
            payMap.put("开户网点", payChannelVo.getPublicKey());
        } else if (payChannelVo.getType() == 2) {
            //支付宝打款
            payMap.put("支付宝账号", payChannelVo.getAccount());
            payMap.put("收款人", payChannelVo.getMerchantCode());
        } else if (payChannelVo.getType() == 3) {
            //微信打款
            payMap.put("微信账号", payChannelVo.getAccount());
            payMap.put("收款人", payChannelVo.getMerchantCode());
        }

        //image url 2,微信扫码支付  5,支付宝扫码, 10,QQ钱包手机扫码,  12银联二维码
        if (payChannelVo.getPayType() == 1  || payChannelVo.getPayType() == 5 || payChannelVo.getPayType() == 10 ||  payChannelVo.getPayType() == 12) {
            model.addAttribute("payImage", payChannelVo.getPayDesc());
        } else {
            model.addAttribute("payImage", "");
        }

        model.addAttribute("userId", userId);
        model.addAttribute("min", payChannelVo.getMin());
        model.addAttribute("max", payChannelVo.getMax());

        model.addAttribute("channelId", payChannelVo.getChannelId());
        model.addAttribute("type", payChannelVo.getType());
        model.addAttribute("configId", payChannelVo.getId());

        model.addAttribute("payMap", payMap);
        model.addAttribute("payChannelVo", payChannelVo);

        model.addAttribute("channelVo", channelVo);
        model.addAttribute("url", UrlConstant.FEONT_SYSTM_URL);
        return new ModelAndView("dopay");
    }


    /**
     * <B>支付成功界面</B>
     * @param model
     * @param orderId
     * @return
     */
    @RequestMapping(value = "/success", method = RequestMethod.GET)
    public ModelAndView paySuccess(Model model, String orderId) {
        if (StrUtil.isBlank(orderId)) {
            return null;
        }

        R<PayOrderVo> payOrder = remotePayOrderService.getPayOrder(orderId, SecurityConstants.FROM_IN);
        if (payOrder == null || payOrder.getData() == null) {
            return null;
        }

        Map<String, Object> map = new HashMap<>();
        PayOrderVo payOrderVo = new PayOrderVo();
        if (payOrder.getData() instanceof PayOrderVo) {
            payOrderVo =  payOrder.getData();
            model.addAttribute("payOrder", payOrderVo);
        } else {
            map  = (Map<String, Object>) payOrder.getData();
            model.addAttribute("payOrder", map);
        }

        //获取渠道信息
        R<TChannel> channel = remoteChannelService.getChannelInfo(map != null ? map.get("channelId").toString() : payOrderVo.getChannelId(), SecurityConstants.FROM_IN);
        if (channel == null || channel.getData() == null) {
            return null;
        }
        TChannel channelVo = channel.getData();
        model.addAttribute("channelVo", channelVo);
        model.addAttribute("url", UrlConstant.FEONT_SYSTM_URL);
        return new ModelAndView("payresult");
    }



    /**
     * <B>支付处理页面</B>
     * @param model
     * @param channelId
     * @param userId
     * @param configId
     * @param payfee
     * @param payaccount
     * @param payname
     * @return
     */
    @RequestMapping(value = "/result", method = RequestMethod.GET)
    public ModelAndView payResult(Model model, String channelId, String type, String userId, String configId, String payfee, String payaccount, String payname) {
        if (StrUtil.isBlank(channelId) || StrUtil.isBlank(type) || StrUtil.isBlank(userId) || StrUtil.isBlank(configId) || StrUtil.isBlank(payfee)) {
            return null;
        }

        //创建订单信息
        PayOrderDto  payOrderDto = new PayOrderDto();
        payOrderDto.setChannelId(channelId);
        payOrderDto.setConfigId(configId);

        payOrderDto.setUserId(userId);
        payOrderDto.setPayfee(payfee);
        payOrderDto.setPayaccount(payaccount);

        payOrderDto.setPayname(payname);
        R<PayResult> order = remotePayOrderService.createPayOrder(payOrderDto, SecurityConstants.FROM_IN);
        if (order == null || order.getData() == null) {
            return null;
        }

        //获取渠道信息
        R<TChannel> channel = remoteChannelService.getChannelInfo(channelId, SecurityConstants.FROM_IN);
        if (channel == null || channel.getData() == null) {
            return null;
        }

        TChannel channelVo = channel.getData();
        model.addAttribute("channelVo", channelVo);
        Map<String, String> resultMap = new HashMap<String, String >();

        //判断是在线支付还是什么别的
        if (type.equals("1") || type.equals("2") || type.equals("3")) {
            PayResult payResult = new PayResult();
            if (order.getData() instanceof PayResult) {
                payResult = order.getData();
                if (payResult.getCode() != null &&Integer.valueOf(payResult.getCode()) == 1) {
                    return new ModelAndView("payerror");
                } else {
                    resultMap.put("充值金额", payResult.getPayFee());
                    resultMap.put("支付方式", payResult.getPayName());
                    resultMap.put("订单号", payResult.getOrderNo());
                    resultMap.put("支付状态", payResult.getStatus().equals(ConfigConstant.PAYORDR_STATUS.WAIT.getValue()) ? ConfigConstant.PAYORDR_STATUS.WAIT.getDescription() : ConfigConstant.PAYORDR_STATUS.FAIL.getDescription());
                    model.addAttribute("resultMap", resultMap);
                    return new ModelAndView("payresult");
                }
            }
        } else {
            //在线支付跳转
            PayResult payResult = new PayResult();
            String payUrl = "";
            if (order.getData() instanceof PayResult) {
                payResult = order.getData();
                payUrl = payResult.getPayUrl();
            } else {
                Map<String, Object> map  = (Map<String, Object>) order.getData();
                payUrl = map.get("payUrl").toString();
            }

             model.addAttribute("url", UrlConstant.FEONT_SYSTM_URL);
             return  new ModelAndView(new RedirectView(payUrl));
        }

        return  null;
    }


    /**
     * <B>处理在线支付</B>
     * @param model
     * @param configId
     * @param channelId
     * @param userId
     * @param payFee
     * @return
     */
    @RequestMapping(value = "/autoSubmitPay", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView autoSubmitPay(Model model, String configId, String channelId,  String userId, String payFee) {
        //获取支付订单信息
        if (StrUtil.isBlank(configId) || StrUtil.isBlank(channelId) || StrUtil.isBlank(userId) || StrUtil.isBlank(payFee)) {
            return null;
        }

        //创建订单信息
        PayOrderDto payOrderDto = new PayOrderDto();
        payOrderDto.setChannelId(channelId);
        payOrderDto.setConfigId(configId);

        payOrderDto.setPayfee(payFee);
        payOrderDto.setUserId(userId);

        R<PayConfigVo> r = remotePayOrderService.getPayInfo(payOrderDto, SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return null;
        }

        PayConfigVo vo = r.getData();
        model.addAttribute("payMap", vo.getParameterMap());
        model.addAttribute("redirectUrl", vo.getPayGetway());

        model.addAttribute("formMethod", vo.getMethod().intValue() ==  1 ? "post" : "get");
        return new ModelAndView("autoSubmitPay");
    }


    /**
     * <B>获取二维码并显示</B>
     * @param model
     * @param orderNo
     * @return
     */
    @RequestMapping(value = "/showPayCode", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView showPayCode(Model model, String orderNo) {
        //判断信息不能为空
        if (StrUtil.isBlank(orderNo)) {
            return null;
        }

        R<PayCodeVo> r = remotePayOrderService.getPayCode(orderNo, SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return null;
        }

        PayCodeVo vo = r.getData();
        model.addAttribute("payCodeVo", vo);
        return new ModelAndView("payCode");
    }
}
