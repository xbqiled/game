package com.youwan.game.front.controller;


import cn.hutool.core.util.StrUtil;
import com.youwan.game.cac.api.entity.SourceUrl;
import com.youwan.game.cac.api.feign.RemoteJumpService;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.util.R;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@Slf4j
@AllArgsConstructor
@RequestMapping("/share")
public class ShareController {

    private final RemoteJumpService remoteJumpService;

    /**
     * <B>中路分享跳转地址</B>
     * @param model
     * @param doId
     * @return
     */
    @RequestMapping("/jump")
    public ModelAndView jump(Model model, String doId) {
        //首先判断是否存在
        if (StrUtil.isBlank(doId)) {
            return null;
        }

        //获取落地地址信息
        R<SourceUrl> r = remoteJumpService.getSourceUrl(doId, SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return null;
        }

        SourceUrl sourceUrl = r.getData();
        model.addAttribute("url", sourceUrl.getSourceUrl());
        return new ModelAndView("share");
    }
}
