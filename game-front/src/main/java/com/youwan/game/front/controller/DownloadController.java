package com.youwan.game.front.controller;


import cn.hutool.core.util.StrUtil;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.DeviceInfoDto;
import com.youwan.game.cac.api.entity.SourceUrl;
import com.youwan.game.cac.api.feign.RemoteDeviceInfoService;
import com.youwan.game.cac.api.feign.RemoteDownloadService;
import com.youwan.game.cac.api.feign.RemoteJumpService;
import com.youwan.game.cac.api.vo.DownloadConfigVo;
import com.youwan.game.cac.api.vo.DownloadConfigVo1;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.constant.UrlConstant;
import com.youwan.game.common.core.jsonmodel.GsonDeviceInfo;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.core.util.Result;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * <B>处理下载页面显示</B>
 */
@Controller
@Slf4j
@AllArgsConstructor
@RequestMapping("/download")
public class DownloadController {

    private final RemoteDownloadService remoteDownloadService;

    private final RemoteJumpService remoteJumpService;

    private final RemoteDeviceInfoService remoteDeviceInfoService;

    /**
     * <B>渠道的APP下载页面</B>
     * @param model
     * @param channelId
     * @return
     */
    @RequestMapping("/beforeChannelPage")
    public ModelAndView beforeChannelPage(HttpServletRequest request, Model model, String  channelId){
        if (StrUtil.isBlank(channelId)) {
            return null;
        }

        R<DownloadConfigVo1> r = remoteDownloadService.downConfig1ByChannelId(channelId, SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return null;
        }

        StringBuffer sb  = new StringBuffer();
        DownloadConfigVo1 vo = r.getData();

        model.addAttribute("vo", vo);
        return new ModelAndView("downloadx");
    }


    @RequestMapping("/channelPage")
    public ModelAndView chanelPage(Model model, String  channelId){
        if (StrUtil.isBlank(channelId)) {
            return null;
        }

        R<DownloadConfigVo> r = remoteDownloadService.downConfigByChannelId(channelId, SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return null;
        }

        StringBuffer sb  = new StringBuffer();
        DownloadConfigVo vo = r.getData();

        model.addAttribute("vo", vo);
        return new ModelAndView("cfgDownload");
    }


    @RequestMapping("/beforeOpenApp")
    public ModelAndView beforeOpenApp(Model model, String  channelId, String promoterId){
        if (StrUtil.isBlank(channelId)) {
            return null;
        }

        R<DownloadConfigVo> r = remoteDownloadService.downConfigByChannelId(channelId, SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return null;
        }

        StringBuffer sb  = new StringBuffer();
        DownloadConfigVo vo = r.getData();

        sb.append(vo.getScheme()).append("://").append(vo.getHost()).append("/").append(vo.getPathprefix()).append("/scheme?");
        sb.append("channelId=" + channelId);
        if (StrUtil.isNotBlank(promoterId)) {
            sb.append("&promoterId=" + promoterId);
        }

        String schemeUrl = sb.toString();
        model.addAttribute("schemeUrl", schemeUrl);
        model.addAttribute("vo", vo);
        return new ModelAndView("open");
    }


    /**
     * <B>下载或直接打开APP</B>
     * @param model
     * @param channelId
     * @param promoterId
     * @return
     */
    @RequestMapping("/openApp")
    public ModelAndView openApp(Model model, String  channelId, String promoterId){
        if (StrUtil.isBlank(channelId)) {
            return null;
        }

        R<DownloadConfigVo> r = remoteDownloadService.downConfigByChannelId(channelId, SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return null;
        }

        StringBuffer sb  = new StringBuffer();
        DownloadConfigVo vo = r.getData();

        sb.append(vo.getScheme()).append("://").append(vo.getHost()).append("/").append(vo.getPathprefix()).append("/scheme?");
        sb.append("channelId=" + channelId);
        if (StrUtil.isNotBlank(promoterId)) {
            sb.append("&promoterId=" + promoterId);
        }

        String schemeUrl = sb.toString();
        model.addAttribute("schemeUrl", schemeUrl);
        model.addAttribute("vo", vo);
        return new ModelAndView("cfgOpen");
    }



    /**
     * <B>获取浏览器用户信息,并存储到服务端</B>
     * @param model
     * @param channelId
     * @param promoterId
     * @return
     */
    @RequestMapping("/beforeDownload")
    public ModelAndView beforeDownload (Model model,  String  channelId, String promoterId) {
        if (StrUtil.isBlank(channelId)) {
            return null;
        }

        R<DownloadConfigVo> r = remoteDownloadService.downConfigByChannelId(channelId, SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null)
        {
            return null;
        }
        StringBuffer sb  = new StringBuffer();
        DownloadConfigVo vo = r.getData();

        sb.append(vo.getScheme()).append("://").append(vo.getHost()).append("/").append(vo.getPathprefix()).append("/scheme?");
        sb.append("channelId=" + channelId);
        if (StrUtil.isNotBlank(promoterId)) {
            sb.append("&promoterId=" + promoterId);
        }

        String schemeUrl = sb.toString();
        model.addAttribute("schemeUrl", schemeUrl);
        model.addAttribute("vo", vo);

        model.addAttribute("channelId", channelId);
        if (StrUtil.isNotBlank(channelId)) {
            model.addAttribute("promoterId", promoterId);
        }

        model.addAttribute("url", UrlConstant.FEONT_SYSTM_URL);
        return new ModelAndView("doDownload");
    }



    /**
     * <B>获取浏览器用户信息,并存储到服务端</B>
     * @param model
     * @param channelId
     * @param promoterId
     * @return
     */
    @RequestMapping("/doDownload")
    public ModelAndView download (Model model,  String  channelId, String promoterId) {
        if (StrUtil.isBlank(channelId)) {
            return null;
        }

        R<DownloadConfigVo> r = remoteDownloadService.downConfigByChannelId(channelId, SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null)
        {
            return null;
        }
        StringBuffer sb  = new StringBuffer();
        DownloadConfigVo vo = r.getData();

        sb.append(vo.getScheme()).append("://").append(vo.getHost()).append("/").append(vo.getPathprefix()).append("/scheme?");
        sb.append("channelId=" + channelId);
        if (StrUtil.isNotBlank(promoterId)) {
            sb.append("&promoterId=" + promoterId);
        }

        String schemeUrl = sb.toString();
        model.addAttribute("schemeUrl", schemeUrl);
        model.addAttribute("vo", vo);

        model.addAttribute("channelId", channelId);
        if (StrUtil.isNotBlank(channelId)) {
            model.addAttribute("promoterId", promoterId);
        }

        model.addAttribute("url", UrlConstant.FEONT_SYSTM_URL);
        return new ModelAndView("cfgDoDownload");
    }


    /**
     * <B>提交设备信息</B>
     * @return
     */
    @ResponseBody
    @RequestMapping("/addDeviceInfo")
    public Result addDeviceInfo(String ipInfo, String channelId, String promoterId, String os, String osVersion, String appVersion, String colorDepth, String deviceMemory,
                                String deviceType, String fingerprint, String hardwareConcurrency, String language, String netWork,  String screenHeight, String screenWidth, String userAgent) {
        if (StrUtil.isBlank(channelId) || StrUtil.isBlank(ipInfo) ) {
            return  Result.error("提交设备信息失败!");
        }

        DeviceInfoDto dto = new DeviceInfoDto();
        dto.setIp(ipInfo);
        dto.setOs(os);
        dto.setChannelId(channelId);
        dto.setPromoterId(promoterId);

        GsonDeviceInfo info = new GsonDeviceInfo();
        info.setAppVersion(appVersion);
        info.setOsVersion(osVersion);

        info.setColorDepth(colorDepth);
        info.setDeviceMemory(deviceMemory);
        info.setDeviceType(deviceType);

        info.setFingerprint(fingerprint);
        info.setHardwareConcurrency(hardwareConcurrency);
        info.setLanguage(language);

        info.setNetWork(netWork);
        info.setScreenHeight(screenHeight);
        info.setScreenWidth(screenWidth);

        info.setUserAgent(userAgent);
        dto.setDeviceInfo(new Gson().toJson(info));
        R<Boolean> r = remoteDeviceInfoService.addDeviceInfo(dto, SecurityConstants.FROM_IN);

        Boolean result = r.getData();
        if (result) {
            return  Result.ok("提交设备信息成功!");
        } else {
            return  Result.error("提交设备信息失败!");
        }
    }



    /**
     * <B>中路跳转地址</B>
     * @param model
     * @param doId
     * @return
     */
    @RequestMapping("/shareJump")
    public ModelAndView shareJump(Model model, HttpServletResponse response, String doId, String isOpen) {
        //首先判断是否存在
        if (StrUtil.isBlank(doId)) {
            return null;
        }

        //获取落地地址信息
        R<SourceUrl> r = remoteJumpService.getSourceUrl(doId, SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return null;
        }

        SourceUrl sourceUrl = r.getData();
        model.addAttribute("androidUrl", sourceUrl.getJumpUrl());
        model.addAttribute("url", sourceUrl.getSourceUrl());

        if (StrUtil.isNotEmpty(isOpen) && isOpen.equals("1")) {
            response.addHeader("Content-Type", "application/vnd.ms-word;charset=utf-8");
            response.addHeader("Content-Disposition", "attachment; filename=load.doc");
        }
        return new ModelAndView("doShare");
    }

    /**
     * @param model
     * @param channelId
     * @param promoterId
     * @return
     */
    @RequestMapping("/file")
    public ModelAndView file(Model model, HttpServletResponse response, String channelId, String promoterId) {
        //首先判断是否存在
        response.addHeader("Content-Type", "application/vnd.ms-word;charset=utf-8");
        response.addHeader("Content-Disposition", "attachment; filename='load.doc'");
        return new ModelAndView("doShare");
    }

}
