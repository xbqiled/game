package com.youwan.game.front.controller;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.youwan.game.cac.api.dto.BgExitGameDto;
import com.youwan.game.cac.api.dto.BgUserBalanceDto;
import com.youwan.game.cac.api.dto.OpenBgGameDto;
import com.youwan.game.cac.api.feign.RemoteChannelService;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.constant.UrlConstant;
import com.youwan.game.common.core.util.R;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.util.Map;

/*
 *<B>第三方bg等接入</B>
 */
@Controller
@Slf4j
@AllArgsConstructor
@RequestMapping("/bgplatform")
public class BgController {

    private final RemoteChannelService remoteChannelService;

    /**
     * <B>进入游戏初始化界面</B>
     * @param model
     * @param channelId
     * @param userId
     * @return
     */
    @RequestMapping("/gameInit")
    public ModelAndView gameInitPage(Model model, String channelId, String userId, Integer isMobileUrl, Integer isHttpsUrl){
        if (StrUtil.isBlank(channelId) || StrUtil.isBlank(userId) || ObjectUtil.isNull(isMobileUrl) || ObjectUtil.isNull(isHttpsUrl) ) {
            return null;
        }

        //判断用户是否存在
        R<Map<String, BigDecimal>>  result = remoteChannelService.bgUserBalance(new BgUserBalanceDto(userId), SecurityConstants.FROM_IN);
        if (result == null || result.getData() == null) {
            return null;
        }

        Map<String, BigDecimal> userBalance = result.getData();
        if (userBalance.size() == 2) {
            model.addAttribute("userBalance", userBalance.get("bg"));
            model.addAttribute("sysBalance", userBalance.get("sys"));
        }

        model.addAttribute("isMobileUrl", isMobileUrl);
        model.addAttribute("isHttpsUrl", isHttpsUrl);
        model.addAttribute("userId", userId);

        model.addAttribute("channelId", channelId);
        model.addAttribute("url", UrlConstant.FEONT_SYSTM_URL);
        return new ModelAndView("bgGameInit");
    }


    /**
     * <B>进入游戏界面</B>
     * @param isMobileUrl
     * @param userId
     * @return
     */
    @RequestMapping("/openGame")
    public ModelAndView openGame(Integer isMobileUrl, Integer isHttpsUrl, String userId, String channelId, String table){
        if (ObjectUtil.isNull(isMobileUrl) || ObjectUtil.isNull(isHttpsUrl) || StrUtil.isBlank(userId) ) {
            return null;
        }

//        R<String> result = remoteChannelService.openBgGame(new OpenBgGameDto(isMobileUrl, isHttpsUrl, userId, channelId), SecurityConstants.FROM_IN);
//        if (result == null || result.getData() == null) {
//            return null;
//        }
//
//        String url = result.getData();
//        if (StrUtil.isNotEmpty(table)) {
//           url += "&table=" + table;
//        }
//
//        //直接跳转到游戏页
//        return new ModelAndView("redirect:" + url);
        return null;
    }
}
