package com.youwan.game.front.controller;


import cn.hutool.core.util.StrUtil;
import com.youwan.game.cac.api.feign.RemoteBulletinService;
import com.youwan.game.cac.api.vo.BulletinVo;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.util.R;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


/**
 * <B>公告的前端展示</B>
 */
@Controller
@Slf4j
@AllArgsConstructor
@RequestMapping("/bulletin")
public class BulletinController {

    private final RemoteBulletinService remoteBulletinService;

    @RequestMapping("/show/{bulletinId}")
    public ModelAndView showBulletin(Model model, @PathVariable String  bulletinId){
        if (StrUtil.isBlank(bulletinId)) {
            return null;
        }

        R<BulletinVo> r = remoteBulletinService.getBulletinInfo(bulletinId, SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return null;
        }

        BulletinVo vo = r.getData();
        model.addAttribute("vo", vo);
        model.addAttribute("title", vo.getTTitle());
        model.addAttribute("createtime", vo.getTCreatetime());
        model.addAttribute("content", vo.getContent());
        return new ModelAndView("bulletin");
    }

}
