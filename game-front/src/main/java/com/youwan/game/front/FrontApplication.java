package com.youwan.game.front;


import com.youwan.game.common.security.annotation.EnableGameFeignClients;
import com.youwan.game.common.security.annotation.EnableGameResourceServer;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * @author lion
 * @date 2018年06月21日
 *  前端启动
 */

@SpringCloudApplication
@EnableGameFeignClients
@EnableGameResourceServer(details = true)
public class FrontApplication {
	public static void main(String[] args) {
		SpringApplication.run(FrontApplication.class, args);
	}
}