package com.youwan.game.mc;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-08-27.
 */
public interface ConsumerConstants {

    /**
     * 消费topic
     */
    String XB_TOPIC = "xb_etl";

    //------------ 彩票 ------------------
    String CONSUMER_GROUP_LOTTERY = "lottery_group";
    String TAG_LOTTERY_INSERT = "LOTTERY_INS";
    String TAG_LOTTERY_UPDATE = "LOTTERY_UPD";

    //------------ 皇冠体育 ------------------
    String CONSUMER_GROUP_CROWN_SPORT = "crown_sport_group";
    String TAG_CROWN_INSERT = "HG_SPORT_INS";
    String TAG_CROWN_UPDATE = "HG_SPORT_UPD";

    //------------ 电子游戏 ------------------
    String CONSUMER_GROUP_EGAME = "3rd_egame_group";
    String TAG_EGAME_INSERT = "EGAME_INS";
    String TAG_EGAME_UPDATE = "EGAME_UPD";

    //------------ 真人 ------------------
    String CONSUMER_GROUP_LIVE = "3rd_live_group";
    String TAG_LIVE_INSERT = "REAL_INS";
    String TAG_LIVE_UPDATE = "REAL_UPD";

    //------------ 真人 ------------------
    String CONSUMER_GROUP_3RD_SPORT = "3rd_sport_group";
    String TAG_3RD_SPORT_INSERT = "THIRD_SPORT_INS";
    String TAG_3RD_SPORT_UPDATE = "THIRD_SPORT_UPD";

    //------------ 账变 ------------------
    String CONSUMER_GROUP_ACC_CHANGE = "3rd_account_change_group";
    String TAG_ACC_CHANGE_INSERT = "MNY_MONEY_INS";
    String TAG_ACC_CHANGE_UPDATE = "MNY_MONEY_UPD";
}
