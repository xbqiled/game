package com.youwan.game.mc.listener;

import com.youwan.game.common.core.betting.AgentPerformanceRecord;
import com.youwan.game.common.core.betting.entity.BcLotteryOrder;
import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import com.youwan.game.common.data.elasticsearch.bulk.BulkIndexQueue;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.stereotype.Component;

import static com.youwan.game.mc.ConsumerConstants.*;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-08-26.
 */
@Component
@Slf4j
@RocketMQMessageListener(topic = XB_TOPIC, consumerGroup = CONSUMER_GROUP_LOTTERY, selectorExpression =
        TAG_LOTTERY_INSERT + "||" + TAG_LOTTERY_UPDATE, consumeThreadMax = 8)
public class LotteryOrderListener extends AbstractETLConsumerListener<BcLotteryOrder> {

    private static final String INDEX_LOTTERY_ORDER = "bc_lottery_order";

    public LotteryOrderListener(BulkIndexQueue bulkIndexQueue, ElasticsearchTemplate elasticsearchTemplate) {
        super(bulkIndexQueue, elasticsearchTemplate);
    }

    @Override
    Class<BcLotteryOrder> getEntityClass() {
        return BcLotteryOrder.class;
    }

    @Override
    String getTargetIndex() {
        return INDEX_LOTTERY_ORDER;
    }

    @Override
    String getInsertTag() {
        return TAG_LOTTERY_INSERT;
    }

    @Override
    String getUpdateTag() {
        return TAG_LOTTERY_UPDATE;
    }

    @Override
    String getTPIdFieldName() {
        return "accountId";
    }

    @Override
    protected boolean needAgentPerformanceRecord() {
        return true;
    }

    @Override
    Integer getBettingSource() {
        return AgentPerformanceRecord.SOURCE_LOTTERY;
    }

    @Override
    String getBettingMoneyFieldName() {
        return "buyMoney";
    }
}
