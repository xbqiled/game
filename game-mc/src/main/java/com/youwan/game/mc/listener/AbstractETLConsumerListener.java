package com.youwan.game.mc.listener;

import com.alibaba.fastjson.JSON;
import com.youwan.game.common.core.betting.AgentPerformanceRecord;
import com.youwan.game.common.core.betting.entity.ThirdPartyRecord;
import com.youwan.game.common.core.betting.entity.UserAccountInfo;
import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import com.youwan.game.common.data.elasticsearch.bulk.BulkIndexQueue;
import com.youwan.game.common.data.elasticsearch.operations.IndexOperation;
import com.youwan.game.common.data.elasticsearch.operations.OperationsSupport;
import com.youwan.game.common.data.elasticsearch.operations.SearchOperation;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.elasticsearch.common.Strings;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.youwan.game.common.core.betting.AgentPerformanceRecord.TYPE_DIRECT;
import static com.youwan.game.common.core.betting.AgentPerformanceRecord.TYPE_INDIRECT;
import static com.youwan.game.common.data.elasticsearch.ElasticsearchOperations.DEFAULT_TYPE;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-08-27.
 */
@Slf4j
public abstract class AbstractETLConsumerListener<T extends ThirdPartyRecord> implements RocketMQListener<MessageExt> {

    protected BulkIndexQueue bulkIndexQueue;
    protected ElasticsearchTemplate elasticsearchTemplate;
    private Field thirdPartyBindId;
    private Field bettingMoneyField;

    public AbstractETLConsumerListener(BulkIndexQueue bulkIndexQueue, ElasticsearchTemplate elasticsearchTemplate) {
        this.bulkIndexQueue = bulkIndexQueue;
        this.elasticsearchTemplate = elasticsearchTemplate;
        this.thirdPartyBindId = getEntityField(getTPIdFieldName());
        this.bettingMoneyField = getEntityField(getBettingMoneyFieldName());
    }

    private Field getEntityField(String fieldName) {
        if (fieldName == null)
            return null;
        Class<T> tClass = getEntityClass();
        try {
            Field f = tClass.getDeclaredField(fieldName);
            if (f != null)
                f.setAccessible(true);
            return f;
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }

    @SneakyThrows
    @Override
    public void onMessage(MessageExt message) {
        log.debug("received message >> id:{}, tag:{}, body:{}", message.getMsgId(), message.getTags(), new String(message.getBody()));
        String tag = message.getTags();
        try {
            T ret = JSON.parseObject(message.getBody(), getEntityClass());
            if (getInsertTag().equals(tag)) {
                //bulkIndexQueue.add(new IndexOperation<>(getTargetIndex(), DEFAULT_TYPE, ret));
                insert(ret);
            } else if (getUpdateTag().equals(tag)) {
                /*String id = OperationsSupport.getIdStrValue(ret);
                elasticsearchTemplate.update(getTargetIndex(), DEFAULT_TYPE, id, ret);*/
                update(getTargetIndex(), ret);
            }
        } catch (Exception e) {
            log.error("process message failed，reason :[{}], message >> id:[{}], tag:[{}], body:[{}]", e.getMessage(), message.getMsgId(),
                    message.getTags(), new String(message.getBody()), e);
        }
    }

    private void insert(T ret) throws IllegalAccessException {
        UserAccountInfo account;
        String tpId = getTPId(ret);
        if (tpId != null) {
            account = elasticsearchTemplate.searchOne(new SearchOperation<>("account_info", termQuery("t_thirdpartBindId", tpId),
                    UserAccountInfo.class));
            if (Objects.nonNull(account)) {
                ret.setUserId(account.getT_accountID());
                ret.setChannelId(account.getT_channelKey());
                ret.setUserName(account.getT_nickName());
                ret.setPid(account.getPid() == null ? 0 : account.getPid());
                ret.setM_path(account.getM_path() == null ? account.getT_accountID().toString() : JSON.parseArray(account.getM_path(),
                        String.class).stream().collect(Collectors.joining("/")));
            }
        }
        ret.setCreateAt(new Date());
        ret.setScrub_state(0);
        ret.setScrub_epoch("0");
        IndexOperation<T> indexOperation = new IndexOperation<>(getTargetIndex(), DEFAULT_TYPE, ret);
        if (tpId != null)
            indexOperation.setRouting(tpId);//把第三方id作为路由
        if (Strings.hasText(ret.getM_path())) {
            indexOperation.setPipeline("split_m_path");
            if (needAgentPerformanceRecord()) {
                try {
                    recordAgentPerformance(ret.getM_path(), ret);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        bulkIndexQueue.add(indexOperation);
    }

    private String getTPId(T ret) throws IllegalAccessException {
        if (thirdPartyBindId == null)
            return null;
        return thirdPartyBindId.get(ret) + "";
    }

    private void recordAgentPerformance(String mPath, T bettingRecord) throws IllegalAccessException {
        String epoch = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        if (bettingRecord == null) {
            return;
        }
        if (bettingMoneyField == null) {
            return;
        }
        String[] ancestors = mPath.split("/");
        Deque<String> queue = Stream.of(ancestors).collect(Collectors.toCollection(ArrayDeque::new));
        if (queue.size() < 2) {
            return;
        }
        //从右向左取出
        queue.pollLast();//自身id
        String pid = queue.pollLast();//上级id
        String spid = queue.pollLast();//上上级id
        BigDecimal bettingMoney = (BigDecimal) bettingMoneyField.get(bettingRecord);
        if (bettingMoney == null) {
            return;
        }
        //记录上级的直属业绩
        AgentPerformanceRecord directPerformanceRecord = new AgentPerformanceRecord();
        directPerformanceRecord.setEpoch(epoch);
        directPerformanceRecord.setSource(getBettingSource());
        directPerformanceRecord.setType(TYPE_DIRECT);
        directPerformanceRecord.setValue(bettingMoney);
        directPerformanceRecord.setUid(Long.parseLong(pid));
        directPerformanceRecord.setSubordinate(bettingRecord.getUserId());
        /*IndexOperation<AgentPerformanceRecord> indexOperation = new IndexOperation<>("agent_performance_record", DEFAULT_TYPE,
                directPerformanceRecord);
        indexOperation.setRouting(directPerformanceRecord.getUid().toString());
        bulkIndexQueue.add(indexOperation);*/
        directPerformanceRecord.setM_path(mPath);
        indexAgentPerformanceRecord(directPerformanceRecord);
        if (spid != null) {
            //记录上上级的下属业绩
            AgentPerformanceRecord indirectPerformanceRecord = new AgentPerformanceRecord();
            indirectPerformanceRecord.setEpoch(epoch);
            indirectPerformanceRecord.setSource(getBettingSource());
            indirectPerformanceRecord.setType(TYPE_INDIRECT);
            indirectPerformanceRecord.setValue(bettingMoney);
            indirectPerformanceRecord.setUid(Long.parseLong(spid));
            indirectPerformanceRecord.setSubordinate(Long.parseLong(pid));
            directPerformanceRecord.setM_path(mPath);
            indexAgentPerformanceRecord(indirectPerformanceRecord);
        }

    }

    private void indexAgentPerformanceRecord(AgentPerformanceRecord agentPerformanceRecord) {
        IndexOperation<AgentPerformanceRecord> indexOperation = new IndexOperation<>("agent_performance_record", DEFAULT_TYPE,
                agentPerformanceRecord);
        indexOperation.setRouting(agentPerformanceRecord.getUid().toString());
        indexOperation.setPipeline("split_m_path");
        bulkIndexQueue.add(indexOperation);
    }

    private void update(String index, T ret) throws IllegalAccessException {
        ret.setModifyAt(new Date());
        String id = OperationsSupport.getIdStrValue(ret);
        elasticsearchTemplate.update(index, DEFAULT_TYPE, id, ret);
    }

    abstract Class<T> getEntityClass();

    abstract String getTargetIndex();

    abstract String getInsertTag();

    abstract String getUpdateTag();

    abstract String getTPIdFieldName();

    abstract String getBettingMoneyFieldName();

    abstract Integer getBettingSource();

    protected boolean needAgentPerformanceRecord() {
        return false;
    }

}
