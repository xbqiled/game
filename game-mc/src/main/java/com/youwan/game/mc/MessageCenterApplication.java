package com.youwan.game.mc;


import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import com.youwan.game.common.data.elasticsearch.bulk.BulkIndexQueue;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.unit.TimeValue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;


/**
 * @author lion
 * @date 2019年01月21日
 * 消息中心
 */
@EnableDiscoveryClient
@SpringBootApplication
public class MessageCenterApplication {
    public static void main(String[] args) {
        SpringApplication.run(MessageCenterApplication.class, args);
    }

    @Bean(initMethod = "start", destroyMethod = "stop")
    public BulkIndexQueue bulkIndexQueue(ElasticsearchTemplate elasticsearchTemplate) {
        Settings settings = Settings.builder()
                .put("queue.capacity", 50 * 10000)
                .put("queue.take.size", 2000)
				.put("queue.take.interval", TimeValue.timeValueMillis(1000)).build();
        return new BulkIndexQueue(settings, elasticsearchTemplate);
    }
}
