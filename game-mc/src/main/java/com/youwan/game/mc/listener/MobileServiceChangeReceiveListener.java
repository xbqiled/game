package com.youwan.game.mc.listener;



import com.youwan.game.common.core.constant.MqQueueConstant;
import com.youwan.game.common.core.sms.MobileMsgTemplate;
import com.youwan.game.common.core.sms.SmsMessageService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author lion
 * @date 2019年02月15日15:59:00
 * 监听服务状态改变发送请求
 */
//@Slf4j
//@Component
public class MobileServiceChangeReceiveListener {

//    @Autowired
//    private Map<String, SmsMessageService> smsMessageService;


    public void receive(MobileMsgTemplate mobileMsgTemplate) {
        long startTime = System.currentTimeMillis();
        //log.info("消息中心接收到短信发送请求-> 手机号：{} -> 信息体：{} ", mobileMsgTemplate.getMobile(), mobileMsgTemplate.getContext());

//        String channel = mobileMsgTemplate.getChannel();
//        //SmsMessageService smsMessageService = messageHandlerMap.get(channel);
//        if (messageHandler == null) {
//            log.error("没有找到指定的路由通道，不进行发送处理完毕！");
//            return;
//        }
//
//        messageHandler.execute(mobileMsgTemplate);
        long useTime = System.currentTimeMillis() - startTime;
        //log.info("调用 {} 短信网关处理完毕，耗时 {}毫秒", mobileMsgTemplate.getType(), useTime);
    }
}
