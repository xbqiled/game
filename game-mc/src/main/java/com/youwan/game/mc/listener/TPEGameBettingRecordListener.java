package com.youwan.game.mc.listener;

import com.youwan.game.common.core.betting.AgentPerformanceRecord;
import com.youwan.game.common.core.betting.entity.ThirdPartyEGameBettingRecord;
import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import com.youwan.game.common.data.elasticsearch.bulk.BulkIndexQueue;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.stereotype.Component;

import static com.youwan.game.mc.ConsumerConstants.*;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-08-27.
 */
@Component
@Slf4j
@RocketMQMessageListener(topic = XB_TOPIC, consumerGroup = CONSUMER_GROUP_EGAME,
        selectorExpression = TAG_EGAME_INSERT + "||" + TAG_EGAME_UPDATE, consumeThreadMax = 8)
public class TPEGameBettingRecordListener extends AbstractETLConsumerListener<ThirdPartyEGameBettingRecord> {

    private static final String INDEX_EGAME_ORDER = "3rd_egame_betting_record";

    public TPEGameBettingRecordListener(BulkIndexQueue bulkIndexQueue, ElasticsearchTemplate elasticsearchTemplate) {
        super(bulkIndexQueue, elasticsearchTemplate);
    }

    @Override
    Class<ThirdPartyEGameBettingRecord> getEntityClass() {
        return ThirdPartyEGameBettingRecord.class;
    }

    @Override
    String getTargetIndex() {
        return INDEX_EGAME_ORDER;
    }

    @Override
    String getInsertTag() {
        return TAG_EGAME_INSERT;
    }

    @Override
    String getUpdateTag() {
        return TAG_EGAME_UPDATE;
    }

    @Override
    String getTPIdFieldName() {
        return "accountId";
    }

    @Override
    protected boolean needAgentPerformanceRecord() {
        return true;
    }

    @Override
    Integer getBettingSource() {
        return AgentPerformanceRecord.SOURCE_EGAME;
    }

    @Override
    String getBettingMoneyFieldName() {
        return "realBettingMoney";
    }
}
