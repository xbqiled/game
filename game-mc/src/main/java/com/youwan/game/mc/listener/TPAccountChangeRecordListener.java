package com.youwan.game.mc.listener;

import com.youwan.game.common.core.betting.entity.ThirdPartyAccountChangeRecord;
import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import com.youwan.game.common.data.elasticsearch.bulk.BulkIndexQueue;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.stereotype.Component;

import static com.youwan.game.mc.ConsumerConstants.*;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-08.
 */
@Component
@Slf4j
@RocketMQMessageListener(topic = XB_TOPIC, consumerGroup = CONSUMER_GROUP_ACC_CHANGE,
        selectorExpression = TAG_ACC_CHANGE_INSERT + "||" + TAG_ACC_CHANGE_UPDATE, consumeThreadMax = 8)
public class TPAccountChangeRecordListener extends AbstractETLConsumerListener<ThirdPartyAccountChangeRecord> {

    private static final String INDEX_NAME = "3rd_account_change_record";

    public TPAccountChangeRecordListener(BulkIndexQueue bulkIndexQueue, ElasticsearchTemplate elasticsearchTemplate) {
        super(bulkIndexQueue, elasticsearchTemplate);
    }

    @Override
    Class<ThirdPartyAccountChangeRecord> getEntityClass() {
        return ThirdPartyAccountChangeRecord.class;
    }

    @Override
    String getTargetIndex() {
        return INDEX_NAME;
    }

    @Override
    String getInsertTag() {
        return TAG_ACC_CHANGE_INSERT;
    }

    @Override
    String getUpdateTag() {
        return TAG_ACC_CHANGE_UPDATE;
    }

    @Override
    String getTPIdFieldName() {
        return "accountId";
    }

    @Override
    String getBettingMoneyFieldName() {
        return null;
    }

    @Override
    Integer getBettingSource() {
        return null;
    }
}
