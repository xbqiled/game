package com.youwan.game.mc.listener;

import com.youwan.game.common.core.betting.AgentPerformanceRecord;
import com.youwan.game.common.core.betting.entity.ThirdPartyLiveBettingRecord;
import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import com.youwan.game.common.data.elasticsearch.bulk.BulkIndexQueue;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.stereotype.Component;

import static com.youwan.game.mc.ConsumerConstants.*;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-08-27.
 */
@Slf4j
@Component
@RocketMQMessageListener(topic = XB_TOPIC, consumerGroup = CONSUMER_GROUP_LIVE,
        selectorExpression = TAG_LIVE_INSERT + "||" + TAG_LIVE_UPDATE, consumeThreadMax = 8)
public class TPLiveBettingRecordListener extends AbstractETLConsumerListener<ThirdPartyLiveBettingRecord> {

    private static final String INDEX_LIVE_ORDER = "3rd_live_betting_record";

    public TPLiveBettingRecordListener(BulkIndexQueue bulkIndexQueue, ElasticsearchTemplate elasticsearchTemplate) {
        super(bulkIndexQueue, elasticsearchTemplate);
    }

    @Override
    Class<ThirdPartyLiveBettingRecord> getEntityClass() {
        return ThirdPartyLiveBettingRecord.class;
    }

    @Override
    String getTargetIndex() {
        return INDEX_LIVE_ORDER;
    }

    @Override
    String getInsertTag() {
        return TAG_LIVE_INSERT;
    }

    @Override
    String getUpdateTag() {
        return TAG_LIVE_UPDATE;
    }

    @Override
    String getTPIdFieldName() {
        return "accountId";
    }

    @Override
    protected boolean needAgentPerformanceRecord() {
        return true;
    }

    @Override
    Integer getBettingSource() {
        return AgentPerformanceRecord.SOURCE_LIVE;
    }

    @Override
    String getBettingMoneyFieldName() {
        return "realBettingMoney";
    }
}
