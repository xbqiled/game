package com.youwan.game.mc.listener;

import com.youwan.game.common.core.betting.AgentPerformanceRecord;
import com.youwan.game.common.core.betting.entity.CrownSportBettingOrder;
import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import com.youwan.game.common.data.elasticsearch.bulk.BulkIndexQueue;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.stereotype.Component;

import static com.youwan.game.mc.ConsumerConstants.*;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-08-27.
 */
@Component
@Slf4j
@RocketMQMessageListener(topic = XB_TOPIC, consumerGroup = CONSUMER_GROUP_CROWN_SPORT, selectorExpression =
        TAG_CROWN_INSERT + "||" + TAG_CROWN_UPDATE, consumeThreadMax = 8)
public class CrownSportOrderListener extends AbstractETLConsumerListener<CrownSportBettingOrder> {

    private static final String INDEX_CROWN_SPORT_ORDER = "crown_sport_betting_order";

    public CrownSportOrderListener(BulkIndexQueue bulkIndexQueue, ElasticsearchTemplate elasticsearchTemplate) {
        super(bulkIndexQueue, elasticsearchTemplate);
    }

    @Override
    Class<CrownSportBettingOrder> getEntityClass() {
        return CrownSportBettingOrder.class;
    }

    /*@Override
    public void onMessage(MessageExt message) {
        log.debug("received message >> id:{}, tag:{}, body:{}", message.getMsgId(), message.getTags(), new String(message.getBody()));
        String tag = message.getTags();
        try {
            CrownSportBettingOrder ret = JSON.parseObject(message.getBody(), getEntityClass());
            if (getInsertTag().equals(tag)) {
                bulkIndexQueue.add(new IndexOperation<>(getTargetIndex(), DEFAULT_TYPE, ret).setPipeline("remark_json"));
            } else if (getUpdateTag().equals(tag)) {
                String id = OperationsSupport.getIdStrValue(ret);
                elasticsearchTemplate.update(getTargetIndex(), DEFAULT_TYPE, id, ret);
            }
        } catch (Exception e) {
            log.error("process message failed，reason :[{}], message >> id:[{}], tag:[{}], body:[{}]", e.getMessage(),message.getMsgId(),
                    message.getTags(), new String(message.getBody()), e);
        }
    }*/

    @Override
    String getTargetIndex() {
        return INDEX_CROWN_SPORT_ORDER;
    }

    @Override
    String getInsertTag() {
        return TAG_CROWN_INSERT;
    }

    @Override
    String getUpdateTag() {
        return TAG_CROWN_UPDATE;
    }

    @Override
    String getTPIdFieldName() {
        return "memberId";
    }

    @Override
    Integer getBettingSource() {
        return AgentPerformanceRecord.SOURCE_CROWN;
    }

    @Override
    protected boolean needAgentPerformanceRecord() {
        return true;
    }

    @Override
    String getBettingMoneyFieldName() {
        return "bettingMoney";
    }
}
