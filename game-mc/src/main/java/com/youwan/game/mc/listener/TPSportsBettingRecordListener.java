package com.youwan.game.mc.listener;

import com.youwan.game.common.core.betting.AgentPerformanceRecord;
import com.youwan.game.common.core.betting.entity.ThirdPartySportsBettingRecord;
import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import com.youwan.game.common.data.elasticsearch.bulk.BulkIndexQueue;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.stereotype.Component;

import static com.youwan.game.mc.ConsumerConstants.*;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-08-28.
 */
@Component
@Slf4j
@RocketMQMessageListener(topic = XB_TOPIC, consumerGroup = CONSUMER_GROUP_3RD_SPORT,
        selectorExpression = TAG_3RD_SPORT_INSERT + "||" + TAG_3RD_SPORT_UPDATE, consumeThreadMax = 8)
public class TPSportsBettingRecordListener extends AbstractETLConsumerListener<ThirdPartySportsBettingRecord> {

    private static final String INDEX_3RD_SPORT = "3rd_sport_betting_record";

    public TPSportsBettingRecordListener(BulkIndexQueue bulkIndexQueue, ElasticsearchTemplate elasticsearchTemplate) {
        super(bulkIndexQueue, elasticsearchTemplate);
    }

    @Override
    Class<ThirdPartySportsBettingRecord> getEntityClass() {
        return ThirdPartySportsBettingRecord.class;
    }

    @Override
    String getTargetIndex() {
        return INDEX_3RD_SPORT;
    }

    @Override
    String getInsertTag() {
        return TAG_3RD_SPORT_INSERT;
    }

    @Override
    String getUpdateTag() {
        return TAG_3RD_SPORT_UPDATE;
    }

    @Override
    String getTPIdFieldName() {
        return "accountId";
    }

    @Override
    protected boolean needAgentPerformanceRecord() {
        return true;
    }

    @Override
    Integer getBettingSource() {
        return AgentPerformanceRecord.SOURCE_SPORT;
    }

    @Override
    String getBettingMoneyFieldName() {
        return "realBettingMoney";
    }
}
