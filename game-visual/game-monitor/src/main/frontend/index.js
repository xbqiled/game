import '@/assets/css/base.scss';
import moment from 'moment';
import Vue from 'vue';
import VueRouter from 'vue-router';
import components from './components';
import Notifications from './notifications';
import sbaShell from './shell';
import Store from './store';
import ViewRegistry from './viewRegistry';
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import views from './views';

moment.locale(window.navigator.language);
Vue.use(VueRouter);
Vue.use(components);
Vue.use(ElementUI)

const applicationStore = new Store();
const viewRegistry = new ViewRegistry();

const installables = [
  Notifications,
  ...views,
  ...global.SBA.extensions
];

installables.forEach(view => view.install({
  viewRegistry,
  applicationStore,
  vue: Vue
}));

new Vue({
  router: new VueRouter({
    linkActiveClass: 'is-active',
    routes: viewRegistry.routes
  }),
  el: '#app',
  render(h) {
    return h(sbaShell, {
      props: {
        views: this.views,
        applications: this.applications,
        error: this.error
      }
    });
  },
  data: {
    views: viewRegistry.views,
    applications: applicationStore.applications,
    error: null
  },
  methods: {
    onError(error) {
      console.warn('Connection to server failed:', error);
      this.error = error;
    },
    onConnected() {
      this.error = null;
    }
  },
  created() {
    applicationStore.addEventListener('connected', this.onConnected);
    applicationStore.addEventListener('error', this.onError);
    applicationStore.start();
  },
  beforeDestroy() {
    applicationStore.stop();
    applicationStore.removeEventListener('connected', this.onConnected);
    applicationStore.removeEventListener('error', this.onError)
  }
});
