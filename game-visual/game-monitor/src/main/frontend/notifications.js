let granted = false;

const requestPermissions = () => {
  if ('Notification' in window) {
    granted = (window.Notification.permission === 'granted');
    if (!granted && window.Notification.permission !== 'denied') {
      window.Notification.requestPermission(permission => granted = (permission === 'granted'));
    }
  }
};

const notify = (title, options) => {
  if (granted) {
    const note = new window.Notification(title, options);
    if (options.url !== null) {
      note.onclick = () => {
        window.focus();
        window.open(options.url, '_self');
      };
    }
    if (options.timeout > 0) {
      note.onshow = () => setTimeout(() => note.close(), options.timeout);
    }
  }
};

export default {
  install: ({applicationStore}) => {
    requestPermissions();

    applicationStore.addEventListener('updated', (newVal, oldVal) => {
      if (newVal.status !== oldVal.status) {
        notify(`${newVal.name} is now ${newVal.status}`, {
          tag: `${newVal.name}-${newVal.status}`,
          lang: 'en',
          body: `was ${oldVal.status}.`,
          icon: newVal.status === 'UP' ? 'assets/img/favicon.png' : 'assets/img/favicon-danger.png',
          renotify: true,
          timeout: 10000
        })
      }
    });
  }
}
