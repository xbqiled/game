const createTextVNode = label => {
  return {
    render() {
      return this._v(label)
    }
  }
};

export default class ViewRegistry {
  constructor() {
    this._views = [];
    this._redirects = [];
  }

  get views() {
    return this._views;
  }

  get routes() {
    return [
      ...this._toRoutes(this._views, v => !v.parent),
      ...this._redirects
    ]
  }

  addView(...views) {
    views.forEach(view => this._addView(view));
  }

  addRedirect(path, redirectToView) {
    this._redirects.push({path, redirect: {name: redirectToView}});
  }

  _addView(view) {
    if (view.label && !view.handle) {
      view.handle = createTextVNode(view.label);
    }
    this._views.push(view);
  }

  _toRoutes(views, filter) {
    return views.filter(filter).map(
      p => {
        const children = this._toRoutes(views, v => v.parent === p.name);
        return ({
          path: p.path,
          name: children.length === 0 ? p.name : undefined,
          component: p.component,
          props: p.props,
          meta: {view: p},
          children
        });
      }
    )
  }
}
