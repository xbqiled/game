const views = [];

/* global require */
const context = require.context('.', true, /^\.\/.+\/index\.(js|vue)$/);
context.keys().forEach(function (key) {
  const defaultExport = context(key).default;
  if (defaultExport && defaultExport.install) {
    views.push(defaultExport)
  }
});

export default views;
