import {shallowMount} from '@vue/test-utils';
import moment from 'moment';
import sbaTimeAgo from './sba-time-ago.js';

moment.now = () => +new Date(1318781879406);

describe('time-ago', () => {

  describe('given  a short time passed time', () => {
    it('should match the snapshot', () => {
      const wrapper = shallowMount(sbaTimeAgo, {
        propsData: {
          date: moment(1318781000000)
        }
      });
      expect(wrapper.vm.$el).toMatchSnapshot();
    });
  });

  describe('given a long time passed time', () => {
    it('should match the snapshot', () => {
      const wrapper = shallowMount(sbaTimeAgo, {
        propsData: {
          date: moment(1310000000000)
        }
      });
      expect(wrapper.vm.$el).toMatchSnapshot();
    });
  });

});
