import {mount} from '@vue/test-utils';
import moment from 'moment';
import sbaStatus from './sba-status';

moment.now = () => +new Date(1318781879406);

describe('application-status', () => {

  const testSnapshotForStatus = (status, date) => {
    const wrapper = mount(sbaStatus, {
      propsData: {
        status,
        date
      },
      stubs: {
        'font-awesome-icon': true
      }
    });
    expect(wrapper.vm.$el).toMatchSnapshot();
  };

  it('should match the snapshot with status UP with Timestamp', () => {
    testSnapshotForStatus('UP', 1318781000000);
  });

  it('should match the snapshot with status RESTRICTED', () => {
    testSnapshotForStatus('RESTRICTED');
  });

  it('should match the snapshot with status OUT_OF_SERVICE', () => {
    testSnapshotForStatus('OUT_OF_SERVICE');
  });

  it('should match the snapshot with status DOWN', () => {
    testSnapshotForStatus('DOWN');
  });

  it('should match the snapshot with status UNKNOWN', () => {
    testSnapshotForStatus('UNKNOWN');
  });

  it('should match the snapshot with status OFFLINE', () => {
    testSnapshotForStatus('OFFLINE');
  });

  it('should match the snapshot with custom status', () => {
    testSnapshotForStatus('?');
  });

});
