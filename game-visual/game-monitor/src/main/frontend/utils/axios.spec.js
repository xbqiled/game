import {redirectOn401} from './axios';

describe('redirectOn401', () => {
  it('should not redirect on 500', async () => {
    window.location.assign = jest.fn();

    const error = {
      response: {
        status: 500
      }
    };

    try {
      await redirectOn401()(error);
    } catch (e) {
      expect(e).toBe(error);
    }

    expect(window.location.assign).not.toBeCalled();
  });

  it('should redirect on 401', async () => {
    window.location.assign = jest.fn();

    const error = {
      response: {
        status: 401
      }
    };

    try {
      await redirectOn401()(error);
    } catch (e) {
      expect(e).toBe(error);
    }

    expect(window.location.assign).toBeCalledWith('login?redirectTo=http%3A%2F%2Fexample.com%2F');
  });

  it('should not redirect on 401 for predicate yields false', async () => {
    window.location.assign = jest.fn();

    const error = {
      response: {
        status: 401
      }
    };

    try {
      await redirectOn401(() => false)(error);
    } catch (e) {
      expect(e).toBe(error);
    }

    expect(window.location.assign).not.toBeCalled();
  });
});
