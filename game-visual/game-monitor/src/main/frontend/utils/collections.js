export const compareBy = mapper => (a, b) => {
  const valA = mapper(a);
  const valB = mapper(b);
  return valA > valB ? 1 : valA < valB ? -1 : 0;
};
