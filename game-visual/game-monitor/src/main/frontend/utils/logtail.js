import {concatMap, EMPTY, of, timer} from '@/utils/rxjs';

export default (getFn, interval, initialSize = 300 * 1024) => {
  let range = `bytes=-${initialSize}`;
  let size = 0;

  return timer(0, interval)
    .pipe(
      concatMap(() => getFn({headers: {range, 'Accept': 'text/plain, */*'}})),
      concatMap(response => {
        const initial = size === 0;
        const contentLength = response.data.length;
        if (response.status === 200) {
          if (!initial) {
            throw 'Expected 206 - Partial Content on subsequent requests.';
          }
          size = contentLength;
        } else if (response.status === 206) {
          size = parseInt(response.headers['content-range'].split('/')[1]);
        } else {
          throw 'Unexpected response status: ' + response.status;
        }

        // Reload the last byte to avoid a 416: Range unsatisfiable.
        // If the response has length = 1 the file hasn't beent changed.
        // If the response status is 416 the logfile has been truncated.
        range = `bytes=${size - 1}-`;

        let addendum = null;
        let skipped = 0;

        if (initial) {
          if (contentLength >= size) {
            addendum = response.data;
          } else {
            // In case of a partial response find the first line break.
            addendum = response.data.substring(response.data.indexOf('\n') + 1);
            skipped = size - addendum.length;
          }
        } else if (response.data.length > 1) {
          // Remove the first byte which has been part of the previos response.
          addendum = response.data.substring(1);
        }

        return addendum ? of({
          totalBytes: size,
          skipped,
          addendum
        }) : EMPTY;
      })
    );
}
