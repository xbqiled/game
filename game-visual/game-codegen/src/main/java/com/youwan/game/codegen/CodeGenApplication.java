package com.youwan.game.codegen;

import com.youwan.game.common.security.annotation.EnableGameResourceServer;
import com.youwan.game.common.security.annotation.EnableGameFeignClients;
import com.youwan.game.common.swagger.annotation.EnableSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * @author lion
 * @date 2018/07/29
 * 代码生成模块
 */
@EnableSwagger2
@SpringCloudApplication
@EnableGameFeignClients
@EnableGameResourceServer
public class CodeGenApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeGenApplication.class, args);
	}
}
