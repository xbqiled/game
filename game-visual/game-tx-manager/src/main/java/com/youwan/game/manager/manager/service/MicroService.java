package com.youwan.game.manager.manager.service;

import com.youwan.game.manager.model.TxServer;
import com.youwan.game.manager.model.TxState;

/**
 * @author LCN on 2017/11/11
 */
public interface MicroService {

	String TMKEY = "tx-manager";

	TxServer getServer();

	TxState getState();
}
