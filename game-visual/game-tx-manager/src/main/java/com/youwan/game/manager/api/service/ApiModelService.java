package com.youwan.game.manager.api.service;

import com.youwan.game.manager.model.ModelInfo;

import java.util.List;

/**
 *
 * @author LCN on 2017/11/13
 * @author LCN
 */
public interface ApiModelService {

	List<ModelInfo> onlines();


}
