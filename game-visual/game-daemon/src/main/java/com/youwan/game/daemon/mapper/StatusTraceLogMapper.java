package com.youwan.game.daemon.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.OtherBgRedPacket;
import com.youwan.game.cac.api.entity.OtherBgRedPacketExchange;
import com.youwan.game.cac.api.entity.OtherBgbetRecord;
import com.youwan.game.daemon.entity.StatusTraceLog;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 任务轨迹处理
 *
 * @author lishangbu
 * @date 2018/11/22
 */
public interface StatusTraceLogMapper extends BaseMapper<StatusTraceLog> {

    List<String> getCashOrderList();

    List<String> getServerCashOrderList(@Param("list") List<String> list);

    int backupCashOrder(@Param("list") List<String> list);

    int delRepeatCashOrder(@Param("list") List<String> list);

    int  batchInsertBgBetRecord(@Param("tableName")String tableName, @Param("list")List<OtherBgbetRecord> list);

    int  insertBgBetRecord(@Param("tableName")String tableName, @Param("record")OtherBgbetRecord otherBgbetRecord);

    int  insertBgRedPacketRecord(@Param("tableName")String tableName, @Param("redPacket") OtherBgRedPacket otherBgRedPacket);

    int  insertBgRedPacketExchanel(@Param("tableName")String tableName, @Param("redPacketExchanel") OtherBgRedPacketExchange otherBgRedPacketExchange);

    Date getLastBet(@Param("tableName")String tableName);

    Date getBgLastRedPacket(@Param("tableName")String tableName);

    Date getBgLastRedPacketExchange(@Param("tableName")String tableName);

    List<Map<String, Object>> getBgBetRecordList(@Param("tableName") String tableName);

    int updateBgBetUrl(@Param("tableName") String tableName, @Param("detailUrl") String detailUrl, @Param("orderId") String orderId, @Param("userId") String userId);

}
