package com.youwan.game.daemon.service.tpscrub;

import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static com.youwan.game.common.core.betting.ScrubResult.TYPE_QP;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-25.
 */
@Component
@Slf4j
public class QPRecordScrubber extends AbstractBettingRecordScrubber{

    public QPRecordScrubber(ElasticsearchTemplate elasticsearchTemplate, ScrubConfigService scrubConfigService) {
        super(elasticsearchTemplate, scrubConfigService);
    }

    @Override
    String getTargetIndex() {
        return "record_game_score";
    }

    @Override
    String getSumAggField() {
        return "stake";
    }

    @Override
    int getScrubType() {
        return TYPE_QP;
    }

    @Override
    protected String getUidField() {
        return "userID";
    }

    @Override
    protected boolean needDividedByOneHundred() {
        return true;
    }

    @Override
    protected boolean needSharding() {
        return false;
    }
}
