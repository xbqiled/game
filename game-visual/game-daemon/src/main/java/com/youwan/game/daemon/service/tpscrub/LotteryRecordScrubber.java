package com.youwan.game.daemon.service.tpscrub;

import com.youwan.game.common.core.betting.ScrubResult;
import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-21.
 */
@Component
@Slf4j
public class LotteryRecordScrubber extends AbstractBettingRecordScrubber {

    public LotteryRecordScrubber(ElasticsearchTemplate elasticsearchTemplate, ScrubConfigService scrubConfigService) {
        super(elasticsearchTemplate, scrubConfigService);
    }

    @Override
    String getTargetIndex() {
        return "bc_lottery_order";
    }

    @Override
    String getSumAggField() {
        return "buyMoney";
    }

    @Override
    int getScrubType() {
        return ScrubResult.TYPE_LOTTERY;
    }

    /*@Override
    protected void initMark(String epoch, int shard) {
        BoolQueryBuilder query = boolQuery();
        query.must(QueryBuilders.termQuery("scrub_epoch", "0"));

        Script script = new Script(DEFAULT_SCRIPT_TYPE, DEFAULT_SCRIPT_LANG, "ctx._source.scrub_epoch = params.epoch;ctx._source.scrub_state = 0;",
                MapBuilder.newMapBuilder(new HashMap<String, Object>()).put("epoch", epoch).immutableMap());
        getElasticsearchTemplate().updateByQuery(new UpdateByQueryOperation("bc_lottery_order", "_doc", query, script).setPreference("_shards:" + shard));
    }*/

    /*protected Tuple<Integer, List<ScrubResult>> doScrub(String epoch, Integer after, int shard) throws IOException {
        SearchRequest searchRequest = new SearchRequest("bc_lottery_order").preference("_shards:" + shard);
        CompositeAggregationBuilder composite = AggregationBuilders.composite("buckets",
                ImmutableList.of(new TermsValuesSourceBuilder("uid").field("userId"))).subAggregation(sum(
                "buyMoney").field("buyMoney")).size(100);
        if (after != null) {
            composite.aggregateAfter(MapBuilder.newMapBuilder(new HashMap<String, Object>()).put("uid", after).immutableMap());
        }
        BoolQueryBuilder query = boolQuery();
        query.must(termQuery("scrub_epoch", epoch)).must(termQuery("scrub_state", 0));
        SearchSourceBuilder source =
                new SearchSourceBuilder().query(query).aggregation(composite);
        SearchResponse response = getElasticsearchTemplate().getClient().search(searchRequest.source(source), RequestOptions.DEFAULT);
        ParsedComposite compositeAgg = response.getAggregations().get("buckets");
        Integer afterKey = compositeAgg.afterKey() == null ? null : (Integer) compositeAgg.afterKey().get("uid");
        List<ScrubResult> ret = new ArrayList<>();
        compositeAgg.getBuckets().forEach(bucket -> {
            ScrubResult result = new ScrubResult();
            result.setEpoch(epoch);
            result.setType(ScrubResult.TYPE_LOTTERY);
            result.setUid(Long.valueOf((Integer) bucket.getKey().get("uid")));
            result.setValue(BigDecimal.valueOf(((Sum) bucket.getAggregations().get("buyMoney")).getValue()));
            ret.add(result);
        });
        return new Tuple<>(afterKey, ret);
    }*/

    /*@Override
    public void finishMark(String epoch, int shard) {
        BoolQueryBuilder query = boolQuery();
        query.must(termQuery("scrub_epoch", epoch)).must(termQuery("scrub_state", 0));
        Script script = new Script(DEFAULT_SCRIPT_TYPE, DEFAULT_SCRIPT_LANG, "ctx._source.scrub_state = 1;",
                MapBuilder.newMapBuilder(new HashMap<String, Object>()).put("epoch", epoch).immutableMap());
        getElasticsearchTemplate().updateByQuery(new UpdateByQueryOperation("bc_lottery_order", "_doc", query, script).setPreference("_shards:" + shard));
    }*/
}
