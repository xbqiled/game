package com.youwan.game.daemon.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-07-16.
 */
public interface CashOrderMapper {

    List<String> getCashOrderList();

    List<String> getServerCashOrderList(@Param("list") List<String> list);

    int backupCashOrder(@Param("list") List<String> list);

    int delRepeatCashOrder(@Param("list") List<String> list);

}
