package com.youwan.game.daemon.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.youwan.game.daemon.service.ExecutionLogService;
import com.youwan.game.daemon.service.StatusTraceLogService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lion
 * @date 2018/2/8
 */
@Slf4j
@AllArgsConstructor
public class SynBgBetRecordJob implements SimpleJob {

	private final StatusTraceLogService statusTraceLogService;

	@Override
	public void execute(ShardingContext shardingContext) {
		statusTraceLogService.synBgBetRecord();
		log.info("更新BG注单信息表任务执行啦:{}", shardingContext);
	}
}
