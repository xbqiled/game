package com.youwan.game.daemon.job.scrub;

import com.youwan.game.daemon.service.tpscrub.QPRecordScrubber;
import lombok.extern.slf4j.Slf4j;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-25.
 */
@Slf4j
public class QPScrubJob extends AbstractScrubJob<QPRecordScrubber> {

    public QPScrubJob(QPRecordScrubber scrubber) {
        super(scrubber);
    }

}
