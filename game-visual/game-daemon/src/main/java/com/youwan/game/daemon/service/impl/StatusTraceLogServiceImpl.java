package com.youwan.game.daemon.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.OtherBgRedPacket;
import com.youwan.game.cac.api.entity.OtherBgRedPacketExchange;
import com.youwan.game.cac.api.entity.OtherBgbetRecord;
import com.youwan.game.cac.api.feign.RemoteChannelService;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.otherplatform.bg.BGApi;
import com.youwan.game.common.core.otherplatform.bg.BGPlatfromConfig;
import com.youwan.game.common.core.otherplatform.bg.json.resp.AgentQueryResp;
import com.youwan.game.common.core.otherplatform.bg.json.resp.OrderDetailResp;
import com.youwan.game.common.core.otherplatform.bg.json.resp.UserRedPacketExchangeResp;
import com.youwan.game.common.core.otherplatform.bg.json.resp.UserRedPacketResp;
import com.youwan.game.common.core.util.R;
import com.youwan.game.daemon.entity.StatusTraceLog;
import com.youwan.game.daemon.mapper.StatusTraceLogMapper;
import com.youwan.game.daemon.service.StatusTraceLogService;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;

import static cn.hutool.core.date.DatePattern.NORM_DATE_PATTERN;

/**
 * 任务轨迹处理
 *
 * @author Lion
 * @date 2018/11/22
 */
@Slf4j
@Service("statusTraceLogService")
@AllArgsConstructor
public class StatusTraceLogServiceImpl extends ServiceImpl<StatusTraceLogMapper, StatusTraceLog> implements StatusTraceLogService {

    private final RemoteChannelService remoteChannelService;

    private static final Long pageIndex = new Long(1);

    private static final Long pageSize = new Long(10000);

    private static String bgBetRecordTableName = "bgbet_record";

    private static String bgRedPacketExchangeTableName = "bgredpacket_exchange";
    //bg红包兑换信息
    private static String bgRedPacketRecordTableName = "bgredpacket_record";

    /*@Override
    public Boolean delRepeatCashOrder() {
        //查询
        List<String> cashList = baseMapper.getCashOrderList();
        if (CollUtil.isNotEmpty(cashList)) {
            List<String> confirmCashList = baseMapper.getServerCashOrderList(cashList);

            //排序出多余的
            cashList.removeAll(confirmCashList);
            if (CollUtil.isNotEmpty(cashList) && cashList.size() > 0) {
                //备份信息
               baseMapper.backupCashOrder(cashList);
               baseMapper.delRepeatCashOrder(cashList);
            }
        }
        return Boolean.TRUE;
    }*/

    @Override
    public Boolean synBgRedPacketExchangeRecord() {
        R<BGPlatfromConfig> r = remoteChannelService.getbgConfig(SecurityConstants.FROM_IN);
        if (r == null) {
            return Boolean.FALSE;
        }
        BGPlatfromConfig config = r.getData();
        //查询表里记录为空的信息，每次100条
        Date todayDate = DateUtil.date();
        String todayStr = DateUtil.format(todayDate, "yyyyMM");

        //获取最以后时间
        Date lastDate = baseMapper.getBgLastRedPacketExchange(bgRedPacketExchangeTableName + todayStr);
        //判断是否没有数据,如果没有数据就从本月1号0点开始
        Date bjStartTime = null;
        Date bjEndTime = null;

        //首先判断是否有最后一条数据记录
        if (lastDate != null) {
            //当前时间向偏移10分钟,并转换成美东时间
            bjStartTime = DateUtil.offsetMinute(lastDate, -10);
            bjEndTime = DateUtil.offsetHour(bjStartTime, 24);
        } else {
            //如果等于空，那么从本月第一天开始
            //bjStartTime = getRedPacketStartTime();
            bjStartTime = getRedPacketStartTime();
            bjEndTime = DateUtil.offsetHour(bjStartTime, 24);
        }

        Date startTime = channelMdTime(bjStartTime);
        Date endTime = channelMdTime(bjEndTime);
        UserRedPacketResp userRedPacketResp = BGApi.getUserRedPacketRecord(config, pageIndex, pageSize, startTime, endTime);
        if (userRedPacketResp != null && userRedPacketResp.getResult() != null && CollUtil.isNotEmpty(userRedPacketResp.getResult().getItems())) {
            for (UserRedPacketResp.RedPacketData.RedPacket redPacketExchange : userRedPacketResp.getResult().getItems()) {
                OtherBgRedPacket otherBgRedPacket = new OtherBgRedPacket();

                BeanUtil.copyProperties(redPacketExchange, otherBgRedPacket);
                otherBgRedPacket.setTime(channelBjTime(otherBgRedPacket.getTime()));
                String tabelName = bgRedPacketRecordTableName + DateUtil.format(this.channelBjTime(otherBgRedPacket.getTime()), "yyyyMM");

                try {
                    this.baseMapper.insertBgRedPacketRecord(tabelName, otherBgRedPacket);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return Boolean.TRUE;
    }


    @Override
    public Boolean synBgRedPacketRecord() {
        R<BGPlatfromConfig> r = remoteChannelService.getbgConfig(SecurityConstants.FROM_IN);
        if (r == null) {
            return Boolean.FALSE;
        }
        BGPlatfromConfig config = r.getData();
        //查询表里记录为空的信息，每次100条
        Date todayDate = DateUtil.date();
        String todayStr = DateUtil.format(todayDate, "yyyyMM");

        //获取最以后时间
        Date lastDate = baseMapper.getBgLastRedPacket(bgRedPacketRecordTableName + todayStr);
        //判断是否没有数据,如果没有数据就从本月1号0点开始
        Date bjStartTime = null;
        Date bjEndTime = null;

        //首先判断是否有最后一条数据记录
        if (lastDate != null) {
            //当前时间向偏移10分钟,并转换成美东时间
            bjStartTime = DateUtil.offsetMinute(lastDate, -10);
            bjEndTime = DateUtil.offsetHour(bjStartTime, 24);
        } else {
            //如果等于空，那么从本月第一天开始
            //bjStartTime = getRedPacketStartTime();
            bjStartTime = getRedPacketStartTime();
            bjEndTime = DateUtil.offsetHour(bjStartTime, 24);
        }

        Date startTime = channelMdTime(bjStartTime);
        Date endTime = channelMdTime(bjEndTime);
        UserRedPacketExchangeResp userRedPacketExchangeResp = BGApi.getUserRedPacketExchange(config, pageIndex, pageSize, startTime, endTime);

        if (userRedPacketExchangeResp != null && userRedPacketExchangeResp.getResult() != null && CollUtil.isNotEmpty(userRedPacketExchangeResp.getResult().getItems())) {
            for (UserRedPacketExchangeResp.RedPacketExchangeData.RedPacketExchange redPacketExchange : userRedPacketExchangeResp.getResult().getItems()) {
                OtherBgRedPacketExchange otherBgRedPacketExchange = new OtherBgRedPacketExchange();

                BeanUtil.copyProperties(redPacketExchange, otherBgRedPacketExchange);
                otherBgRedPacketExchange.setTime(channelBjTime(otherBgRedPacketExchange.getTime()));
                String tabelName = bgRedPacketExchangeTableName + DateUtil.format(this.channelBjTime(otherBgRedPacketExchange.getTime()), "yyyyMM");
                try {
                    this.baseMapper.insertBgRedPacketExchanel(tabelName, otherBgRedPacketExchange);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return Boolean.TRUE;
    }


    @Override
    public Boolean synBgBetDetail() {
        //获取bg配置信息
        R<BGPlatfromConfig> r = remoteChannelService.getbgConfig(SecurityConstants.FROM_IN);
        if (r == null) {
            return Boolean.FALSE;
        }

        BGPlatfromConfig config = r.getData();
        //查询表里记录为空的信息，每次100条
        Date nowDate = DateUtil.date();
        String nowDateStr = DateUtil.format(nowDate, "yyyyMM");
        List<Map<String, Object>>  resultList = baseMapper.getBgBetRecordList(bgBetRecordTableName + nowDateStr);

        if (CollUtil.isNotEmpty(resultList)) {
            for (Map<String, Object> map: resultList) {
                String orderId = "";
                String userId = "";

                if (map.get("orderId") instanceof Long) {
                    orderId = String.valueOf((Long) map.get("orderId"));
                } else {
                    orderId = (String)map.get("orderId");
                }

                if (map.get("userId") instanceof Long) {
                    userId = String.valueOf((Long) map.get("userId"));
                } else {
                    orderId = (String)map.get("orderId");
                }

                OrderDetailResp orderDetailResp = BGApi.getOrderDetail(config, orderId);
                if (orderDetailResp != null && StrUtil.isNotEmpty(orderDetailResp.getResult())) {
                    baseMapper.updateBgBetUrl(bgBetRecordTableName + nowDateStr, orderDetailResp.getResult(), orderId, userId);
                }
            }
        }
        return null;
    }



    @Override
    public Boolean synBgBetRecord() {
        //获取bg配置信息
        R<BGPlatfromConfig> r = remoteChannelService.getbgConfig(SecurityConstants.FROM_IN);
        if (r == null) {
            return Boolean.FALSE;
        }

        BGPlatfromConfig config = r.getData();
        Date todayDate = DateUtil.date();
        String todayStr = DateUtil.format(todayDate, "yyyyMM");
        //获取最以后时间
        Date lastDate = baseMapper.getLastBet(bgBetRecordTableName + todayStr);
        //判断是否没有数据,如果没有数据就从本月1号0点开始
        Date bjStartTime = null;
        Date bjEndTime = null;

        //首先判断是否有最后一条数据记录
        if (lastDate != null) {
            //当前时间向偏移10分钟,并转换成美东时间
            bjStartTime = DateUtil.offsetMinute(lastDate, -10);
            bjEndTime = DateUtil.offsetHour(bjStartTime, 24);
        } else {
            //如果等于空，那么从本月第一天开始
            bjStartTime = getMonthStartTime();
            bjEndTime = DateUtil.offsetHour(bjStartTime, 24);
        }

        Date startTime = channelMdTime(bjStartTime);
        Date endTime = channelMdTime(bjEndTime);
        //获取信息
        AgentQueryResp order = BGApi.agentQuery(config, null, startTime, endTime, pageIndex, pageSize, null, null, null, null);

        //判断是否为空
        if (order != null && CollUtil.isNotEmpty(order.getResult().getItems())) {
            for (AgentQueryResp.OrderData.BgBetOrder bgBetOrder : order.getResult().getItems()) {
                OtherBgbetRecord otherBgbetRecord = new OtherBgbetRecord();
                BeanUtil.copyProperties(bgBetOrder, otherBgbetRecord);

                otherBgbetRecord.setOrderTime(channelBjTime(otherBgbetRecord.getOrderTime()));
                otherBgbetRecord.setLastUpdateTime(channelBjTime(otherBgbetRecord.getLastUpdateTime()));
                String tabelName = bgBetRecordTableName + DateUtil.format(this.channelBjTime(otherBgbetRecord.getOrderTime()), "yyyyMM");

                //判断是不是自己的账号
                if (bgBetOrder != null && bgBetOrder.getOrderStatus() != 1 && bgBetOrder.getOrderStatus() != 0 &&
                        StrUtil.isNotBlank(bgBetOrder.getLoginId()) && bgBetOrder.getLoginId().substring(0, 3).equals(config.getPrefix())) {
                    log.info("执行BG数据的插入了", "loginId" + bgBetOrder.getLoginId(), "uId" + bgBetOrder.getUid(),
                            "orderStatus" + bgBetOrder.getOrderStatus(), "orderStatus" + bgBetOrder.getOrderTime());

//                    OrderDetailResp orderDetailResp = BGApi.getOrderDetail(config, String.valueOf(bgBetOrder.getOrderId()));
//                    if (orderDetailResp != null && StrUtil.isNotEmpty(orderDetailResp.getResult())) {
//                        otherBgbetRecord.setDetailUrl(orderDetailResp.getResult());
//                    }

                    try {
                        this.baseMapper.insertBgBetRecord(tabelName, otherBgbetRecord);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return Boolean.TRUE;
    }


    public Date channelMdTime(Date dataTime) {
        return DateUtil.offsetHour(dataTime, -12);
    }


    public Date channelBjTime(Date dataTime) {
        return DateUtil.offsetHour(dataTime, 12);
    }

    private Boolean isOneDay(Date lastDate) {
        String today = DateUtil.format(DateUtil.date(), NORM_DATE_PATTERN);
        String day = DateUtil.format(lastDate, NORM_DATE_PATTERN);

        if (StrUtil.equals(day, today)) {
            return true;
        }
        return false;
    }

    private Date getRedPacketStartTime() {
        return DateUtil.parse("2020-01-22 00:00:00");
    }

    /**
     * <B>获取本月第一天</B>
     */
    private Date getMonthStartTime() {
        String ym = DateUtil.format(DateUtil.date(), "yyyy-MM");
        return DateUtil.parse(ym + "-01 00:00:00");
    }

    /**
     * @param startTime
     * @param endTime
     * @return
     */
    private List<String> getMonthStr(Date startTime, Date endTime) {
        List<String> resultList = new ArrayList<>();
        String startMonth = DateUtil.format(startTime, "yyyyMM");
        String endMonth = DateUtil.format(endTime, "yyyyMM");

        if (StrUtil.equals(startMonth, endMonth)) {
            resultList.add(startMonth);
        } else {
            resultList.add(startMonth);
            resultList.add(endMonth);
        }
        return resultList;
    }
}
