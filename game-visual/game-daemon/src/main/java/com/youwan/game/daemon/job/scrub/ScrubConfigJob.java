package com.youwan.game.daemon.job.scrub;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.youwan.game.daemon.service.tpscrub.ScrubConfigService;
import lombok.extern.slf4j.Slf4j;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-04.
 */
@Slf4j
public class ScrubConfigJob implements SimpleJob {

    private ScrubConfigService configService;

    public ScrubConfigJob(ScrubConfigService configService) {
        this.configService = configService;
    }

    @Override
    public void execute(ShardingContext shardingContext) {
        configService.refreshScrubConfigs();
    }
}
