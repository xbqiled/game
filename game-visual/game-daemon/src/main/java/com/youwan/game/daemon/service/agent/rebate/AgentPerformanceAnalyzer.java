package com.youwan.game.daemon.service.agent.rebate;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.ImmutableList;
import com.youwan.game.common.core.betting.AgentPerformanceAnalysisResult;
import com.youwan.game.common.core.betting.ScrubResult;
import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.collect.MapBuilder;
import org.elasticsearch.common.collect.Tuple;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.composite.CompositeAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.composite.ParsedComposite;
import org.elasticsearch.search.aggregations.bucket.composite.TermsValuesSourceBuilder;
import org.elasticsearch.search.aggregations.metrics.Sum;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;
import static org.elasticsearch.search.aggregations.AggregationBuilders.sum;

/**
 * 代理业绩分析器
 *
 * @author terry.jiang[taoj555@163.com] on 2020-10-25.
 */
@Component
@Slf4j
public class AgentPerformanceAnalyzer {

    private ElasticsearchTemplate elasticsearchTemplate;

    public AgentPerformanceAnalyzer(ElasticsearchTemplate elasticsearchTemplate) {
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    /**
     * 计算返利
     *
     * @param epoch 周期
     * @param shard 分片
     * @return
     * @throws IOException
     */
    public void calculateRebate(String epoch, int shard) throws IOException {
        List<AgentPerformanceAnalysisResult> ret = new LinkedList<>();
        Integer after = null;
        for (; ; ) {
            Tuple<Integer, List<AgentPerformanceAnalysisResult>> tuple = doFirstStageCalculate(epoch, after, shard);
            if (tuple.v2().isEmpty())
                break;
            ret.addAll(tuple.v2());
            after = tuple.v1();
        }
        log.error("scrub >>>>>>>>>>> {}, {}", shard, JSON.toJSONString(ret));
    }

    /**
     * firstStageCalculation
     *
     * @param epoch
     * @param after
     * @param shard
     * @return
     * @throws IOException
     */
    private Tuple<Integer, List<AgentPerformanceAnalysisResult>> doFirstStageCalculate(String epoch, Integer after, int shard) throws IOException {
        SearchRequest searchRequest = new SearchRequest("agent_performance_record").preference("_shards:" + shard);
        CompositeAggregationBuilder composite = AggregationBuilders.composite("buckets",
                ImmutableList.of(new TermsValuesSourceBuilder("uid").field("uid"))).subAggregation(sum(
                "bettingMoney").field("value")).size(100);
        if (after != null) {
            composite.aggregateAfter(MapBuilder.newMapBuilder(new HashMap<String, Object>()).put("uid", after).immutableMap());
        }
        BoolQueryBuilder query = boolQuery().must(termQuery("epoch", epoch)).must(termQuery("statFlag", false));
        SearchSourceBuilder source =
                new SearchSourceBuilder().query(query).aggregation(composite);
        SearchResponse response = elasticsearchTemplate.getClient().search(searchRequest.source(source), RequestOptions.DEFAULT);
        ParsedComposite compositeAgg = response.getAggregations().get("buckets");
        Integer afterKey = compositeAgg.afterKey() == null ? null : (Integer) compositeAgg.afterKey().get("uid");
        List<AgentPerformanceAnalysisResult> ret = new ArrayList<>();
        compositeAgg.getBuckets().forEach(bucket -> {
            AgentPerformanceAnalysisResult result = new AgentPerformanceAnalysisResult();
            result.setEpoch(epoch);
            result.setTotalPerformance(BigDecimal.valueOf(((Sum) bucket.getAggregations().get("bettingMoney")).getValue()));
            result.setUid(Long.valueOf((Integer) bucket.getKey().get("uid")));
            ret.add(result);
        });
        return new Tuple<>(afterKey, ret);
    }

    private void doSecondStageCalculateAndArchive(AgentPerformanceAnalysisResult tempResult, int shard) {
        SearchRequest searchRequest = new SearchRequest("agent_performance_record").preference("_shards:" + shard);
        CompositeAggregationBuilder composite = AggregationBuilders.composite("buckets",
                ImmutableList.of(new TermsValuesSourceBuilder("subordinate").field("subordinate"))).subAggregation(sum(
                "bettingMoney").field("value")).size(100);
    }

    //private List<Integer, Map<Integer,BigDecimal>>
}
