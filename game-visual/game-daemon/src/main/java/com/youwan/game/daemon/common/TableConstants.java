package com.youwan.game.daemon.common;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-07-17.
 */
public interface TableConstants {

    //时间表名
    String TIME_TABLE = "statis_date";
    //用户统计表名
    String ACCOUNT_TABLE = "statis_account";
    //渠道统计表
    String CHANNEL_TABLE = "statis_channel";
    //游戏统计表
    String GAME_TABLE = "statis_game";
    //bg下注信息表
    String BG_BET_RECORD_TABLE = "bgbet_record";

    //bg红包兑换信息
    String BG_REDPACKET_EXCHANGE_TABLE = "bgredpacket_exchange";
    //bg红包信息
    String BG_REDPACKET_REORD_TABLE = "bgredpacket_record";
}
