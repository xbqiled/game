package com.youwan.game.daemon.service.tpscrub;

import com.youwan.game.common.core.betting.ScrubResult;

import java.io.IOException;
import java.util.List;

/**
 * 第三方记录洗码器
 *
 * @author terry.jiang[taoj555@163.com] on 2020-09-21.
 */
public interface ThirdPartyRecordScrubber {

    /**
     * 标记记录
     */
    String initialMark(int shard);

    /**
     * 统计数据
     *
     * @return
     */
    List<ScrubResult> scrub(String epoch, int shard) throws IOException;

    /**
     * 归档
     *
     * @param data
     */
    void archive(List<ScrubResult> data);

    /**
     * 重新标记
     */
    void finishMark(String epoch, int shard);
}
