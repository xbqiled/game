package com.youwan.game.daemon.job.scrub;

import com.youwan.game.daemon.service.tpscrub.LotteryRecordScrubber;
import lombok.extern.slf4j.Slf4j;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-05.
 */
@Slf4j
public class LotteryScrubJob extends AbstractScrubJob<LotteryRecordScrubber> {

    public LotteryScrubJob(LotteryRecordScrubber scrubber) {
        super(scrubber);
    }
}
