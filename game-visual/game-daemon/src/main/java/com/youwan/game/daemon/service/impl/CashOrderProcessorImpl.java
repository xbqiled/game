package com.youwan.game.daemon.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.youwan.game.daemon.mapper.CashOrderMapper;
import com.youwan.game.daemon.service.CashOrderProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-07-17.
 */
@Slf4j
@Component
public class CashOrderProcessorImpl implements CashOrderProcessor {

    private CashOrderMapper cashOrderMapper;

    public CashOrderProcessorImpl(CashOrderMapper cashOrderMapper) {
        this.cashOrderMapper = cashOrderMapper;
    }

    @Override
    public void delRepeatCashOrder() {
        //查询
        List<String> cashList = cashOrderMapper.getCashOrderList();
        if (CollUtil.isNotEmpty(cashList)) {
            List<String> confirmCashList = cashOrderMapper.getServerCashOrderList(cashList);

            //排序出多余的
            cashList.removeAll(confirmCashList);
            if (CollUtil.isNotEmpty(cashList) && cashList.size() > 0) {
                //备份信息
                cashOrderMapper.backupCashOrder(cashList);
                cashOrderMapper.delRepeatCashOrder(cashList);
            }
        }
    }
}
