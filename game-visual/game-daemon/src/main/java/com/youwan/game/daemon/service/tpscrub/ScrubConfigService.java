package com.youwan.game.daemon.service.tpscrub;

import com.youwan.game.common.core.betting.ScrubConfig;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-03.
 */
public interface ScrubConfigService {

    void refreshScrubConfigs();

    ScrubConfig getScrubConfig(String channelId, Integer vipLevel);

}
