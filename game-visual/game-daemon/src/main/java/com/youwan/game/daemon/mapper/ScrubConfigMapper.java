package com.youwan.game.daemon.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.CfgChlScrub;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-04.
 */
public interface ScrubConfigMapper extends BaseMapper<CfgChlScrub> {


    List<CfgChlScrub> getAllConfigs();

    CfgChlScrub getConfigByChannelId(@Param("channelId") String channelId);

}
