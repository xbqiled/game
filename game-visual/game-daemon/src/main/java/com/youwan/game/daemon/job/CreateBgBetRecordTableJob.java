package com.youwan.game.daemon.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.youwan.game.daemon.service.ExecutionLogService;
import com.youwan.game.daemon.service.TableCreator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lion
 * @date 2018/2/7
 * 同步定时信息
 */
@Slf4j
@AllArgsConstructor
public class CreateBgBetRecordTableJob implements SimpleJob {

	//private final ExecutionLogService executionLogService;
	private TableCreator tableCreator;
	/**
	 * 业务执行逻辑
	 *
	 * @param shardingContext 分片信息
	 */
	@Override
	public void execute(ShardingContext shardingContext) {

		//executionLogService.crateBgBetRecordTable();
		//executionLogService.crateBgRedPacketRecordTable();
		//executionLogService.crateBgRedPacketExchangeRecordTable();
		tableCreator.createBgBetRecordTable();
		tableCreator.createBgRedPacketRecordTable();
		tableCreator.createBgRedPacketExchangeRecordTable();
		log.info("创建bg下注记录表任务执行啦:{}", shardingContext);
	}
}
