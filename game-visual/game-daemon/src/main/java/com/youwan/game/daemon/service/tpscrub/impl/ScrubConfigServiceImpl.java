package com.youwan.game.daemon.service.tpscrub.impl;

import cn.hutool.core.bean.BeanUtil;
import com.google.gson.Gson;
import com.youwan.game.cac.api.entity.CfgChlScrub;
import com.youwan.game.common.core.betting.ScrubConfig;
import com.youwan.game.daemon.mapper.ScrubConfigMapper;
import com.youwan.game.daemon.service.tpscrub.ScrubConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-03.
 */
@Slf4j
@Component
public class ScrubConfigServiceImpl implements ScrubConfigService, InitializingBean {

    private Map<String, Map<Integer, ScrubConfig>> scrubConfigsCache = new HashMap<>();

    private ScrubConfigMapper scrubConfigMapper;

    private final Map<Integer, ScrubConfig> defaultConfigs;

    public ScrubConfigServiceImpl(ScrubConfigMapper scrubConfigMapper) {
        this.scrubConfigMapper = scrubConfigMapper;
        this.defaultConfigs = loadDefaultConfig();
    }

    private Map<Integer, ScrubConfig> loadDefaultConfig() {
        String config_json = "{\"1\": {\"qp\": 0.6, \"live\": 0.2, \"egame\": 0.6, \"sport\": 0.2, \"esport\": 0.6, \"lottery\": 0.2}, \"2\": " +
                "{\"qp\": 0.65, \"live\": 0.25, \"egame\": 0.65, \"sport\": 0.25, \"esport\": 0.65, \"lottery\": 0.25}, \"3\": {\"qp\": 0.7, " +
                "\"live\": 0.3, \"egame\": 0.7, \"sport\": 0.3, \"esport\": 0.7, \"lottery\": 0.3}, \"4\": {\"qp\": 0.75, \"live\": 0.4, \"egame\":" +
                " 0.75, \"sport\": 0.4, \"esport\": 0.75, \"lottery\": 0.4}, \"5\": {\"qp\": 0.8, \"live\": 0.4, \"egame\": 0.8, \"sport\": 0.4, " +
                "\"esport\": 0.75, \"lottery\": 0.4}, \"6\": {\"qp\": 0.9, \"live\": 0.5, \"egame\": 0.9, \"sport\": 0.5, \"esport\": 1, " +
                "\"lottery\": 0.5}, \"7\": {\"qp\": 1, \"live\": 0.5, \"egame\": 1, \"sport\": 0.5, \"esport\": 1, \"lottery\": 0.5}, \"8\": " +
                "{\"qp\": 1, \"live\": 0.5, \"egame\": 1.2, \"sport\": 0.5, \"esport\": 1, \"lottery\": 0.5}, \"9\": {\"qp\": 1, \"live\": 0.5, " +
                "\"egame\": 1.5, \"sport\": 0.5, \"esport\": 1, \"lottery\": 0.5}}";
        Map<String, Map<String, Double>> m = new Gson().fromJson(config_json, Map.class);
        Map<Integer, ScrubConfig> defaultConfigs = new HashMap<>();
        m.entrySet().forEach(entry -> {
            Integer vipLevel = Integer.parseInt(entry.getKey());
            ScrubConfig config = BeanUtil.mapToBean(entry.getValue(), ScrubConfig.class, true);
            defaultConfigs.put(vipLevel, config);
        });
        return defaultConfigs;
    }

    @Override
    public ScrubConfig getScrubConfig(String channelId, Integer vipLevel) {
        assert vipLevel != null && vipLevel > 0 && vipLevel <= 9;
        Map<Integer, ScrubConfig> channelConfigs = scrubConfigsCache.get(channelId);
        if (channelConfigs == null){
            channelConfigs = defaultConfigs;
        }
        ScrubConfig config = channelConfigs.get(vipLevel);
        return config == null ? defaultConfigs.get(vipLevel) : config;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        refreshScrubConfigs();
    }

    @Override
    public void refreshScrubConfigs() {
        List<CfgChlScrub> list = scrubConfigMapper.getAllConfigs();
        list.forEach(cfg -> {
            String channelId = cfg.getChannelId();
            Map<String, Map<String, Double>> scrubCfgsFromJson = new Gson().fromJson(cfg.getScrubConfig(), Map.class);
            Map<Integer, ScrubConfig> chlConfigs = new HashMap<>();
            scrubCfgsFromJson.entrySet().forEach(entry -> {
                Integer vipLevel = Integer.parseInt(entry.getKey());
                ScrubConfig config = BeanUtil.mapToBean(entry.getValue(), ScrubConfig.class, true);
                chlConfigs.put(vipLevel, config);
            });
            scrubConfigsCache.put(channelId, chlConfigs);
        });
    }
}
