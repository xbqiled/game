package com.youwan.game.daemon.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.dataflow.DataflowJob;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author lion
 * @date 2018/2/8
 */
@Slf4j
public class GameDataflowJob implements DataflowJob<Integer> {

    private AtomicInteger c = new AtomicInteger(1);

    @Override
    public List<Integer> fetchData(ShardingContext shardingContext) {
        log.error("fetchData>>>>>>>>>>>>>>>>>>>>>>>>> " + c.get());
        if (c.getAndAdd(1) < 10) {

            return Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10).map(i -> i * c.get()).collect(Collectors.toList());
        } else {
            return Arrays.asList();
        }

    }

    @Override
    public void processData(ShardingContext shardingContext, List<Integer> list) {
        list.forEach(i -> log.error(">>>>>>>>>>>>>>>>>>>>>>>> {}", i));
    }
}
