package com.youwan.game.daemon.service;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-07-16.
 */
public interface CashOrderProcessor {

    void delRepeatCashOrder();

}
