package com.youwan.game.daemon.job.scrub;

import com.youwan.game.daemon.service.tpscrub.CrownSportRecordScrubber;
import lombok.extern.slf4j.Slf4j;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-06.
 */
@Slf4j
public class CrownScrubJob extends AbstractScrubJob<CrownSportRecordScrubber> {

    public CrownScrubJob(CrownSportRecordScrubber scrubber) {
        super(scrubber);
    }
}
