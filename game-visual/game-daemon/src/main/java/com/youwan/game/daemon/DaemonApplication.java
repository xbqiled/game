package com.youwan.game.daemon;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.youwan.game.common.job.annotation.EnableJob;
import com.youwan.game.common.security.annotation.EnableGameFeignClients;
import com.youwan.game.common.security.annotation.EnableGameResourceServer;
import com.youwan.game.common.swagger.annotation.EnableSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * @author lion
 * @date 2018/7/24
 * 分布式任务调度模块
 */
@EnableJob
@EnableSwagger2
@EnableGameFeignClients
@SpringCloudApplication
@EnableGameResourceServer
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, DruidDataSourceAutoConfigure.class})
public class DaemonApplication {

	public static void main(String[] args) {
		SpringApplication.run(DaemonApplication.class, args);
	}

}
