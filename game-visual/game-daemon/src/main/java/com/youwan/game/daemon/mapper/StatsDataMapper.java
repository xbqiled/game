package com.youwan.game.daemon.mapper;

import com.youwan.game.daemon.entity.GameScoreAgg;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-07-17.
 */
public interface StatsDataMapper {

    /**
     * ds => xbqp
     *
     * @param statisTime
     * @return
     */
    List<Map<String, Object>> queryAccountByStatsTime(@Param("seek") Long seek, @Param("statisTime") String statisTime,
                                                      @Param("limit") Integer limit);

    /**
     * ds => recordlog
     *
     * @param startTime
     * @param endTime
     * @return
     */
    //List<Map<String, Object>> queryTreasureChangeAgg(@Param("startTime") Integer startTime, @Param("endTime") Integer endTime);


    /**
     * <B>通过时间获取数据</B>
     *
     * @param tableName
     * @param statisTime
     * @return
     */
    Integer isHaveData(@Param("tableName") String tableName, @Param("statisTime") String statisTime);

    /**
     * <B>删除数据信息</B>
     *
     * @param tableName
     * @param statisTime
     * @return
     */
    Integer delStatisDateData(@Param("tableName") String tableName, @Param("statisTime") String statisTime);

    /**
     * 批量插入statis_date表
     *
     * @param tableName
     * @param list
     * @return
     */
    Integer bulkInsertStatisDateData(@Param("tableName") String tableName, @Param("list") List<Map<String, Object>> list);

    // ----------------------- 账号统计 -----------------------------------

    /**
     * 批量插入statis_account表
     *
     * @param tableName
     * @param list
     * @return
     */
    //Integer bulkInsertStatisAccountData(@Param("tableName") String tableName, @Param("list") List<Map<String, Object>> list);


    Integer insertUserBetData(@Param("tableName") String tableName,
                              @Param("betTableName") String betTableName,
                              @Param("statisTime") String statisTime);

    /**
     * @param tableName
     * @param statisTime
     * @return
     */
    Integer updateUserRechargeCashData(@Param("tableName") String tableName, @Param("statisTime") String statisTime);

    /**
     * @param tableName
     * @param gameTableName
     * @param matchPlayerRankTableName
     * @param statisTime
     * @return
     */
    Integer updateUserDamaBetData(@Param("tableName") String tableName,
                                  @Param("gameTableName") String gameTableName,
                                  @Param("matchPlayerRankTableName") String matchPlayerRankTableName,
                                  @Param("statisTime") String statisTime);

    Integer updateUserGameTimeData(@Param("tableName") String tableName,
                                   @Param("logoutGameTableName") String logoutGameTableName,
                                   @Param("statisTime") String statisTime);

    Integer updateUserStatisData(@Param("tableName") String tableName,
                                 @Param("statisTime") String statisTime);


    // ------------------------- 游戏统计 ----------------------------/
    Integer insertGameStatisData(@Param("tableName") String tableName,
                                 @Param("gameTableName") String gameTableName,
                                 @Param("statisTime") String statisTime);

    Integer getStaticGameCount(@Param("tableName") String tableName, @Param("statisTime") String statisTime);

    Integer updateGameStatisTimeData(@Param("tableName") String tableName,
                                     @Param("logoutGameTableName") String logoutGameTableName,
                                     @Param("statisTime") String statisTime);

    Integer insertMatchGameStatisData(@Param("tableName") String tableName,
                                      @Param("matchPlayerRankTableName") String matchPlayerRankTableName,
                                      @Param("statisTime") String statisTime);

    Integer updateMatchGameStatisTimeData(@Param("tableName") String tableName,
                                          @Param("logoutGameTableName") String logoutGameTableName,
                                          @Param("statisTime") String statisTime);

    // ------------------------- 渠道统计 ----------------------------/
    Integer getStaticChannelCount(@Param("tableName") String tableName, @Param("statisTime") String statisTime);

    Integer insertStaticChannel(@Param("tableName") String tableName, @Param("statisTime") String statisTime);

    Integer updateChannelRechargeCashAggData(@Param("tableName") String tableName, @Param("statisTime") String statisTime);

    Integer updateChannelLoginAggData(@Param("tableName") String tableName, @Param("statisLoginTable") String statisLoginTable,
                                      @Param("statisTime") String statisTime);

    Integer updateChannelLoginGameAggData(@Param("tableName") String tableName,
                                          @Param("regTableName") String regTableName,
                                          @Param("loginGameTableName") String loginGameTableName,
                                          @Param("statisTime") String statisTime);

    Integer updateChannelGameTimeAggData(@Param("tableName") String tableName,
                                         @Param("logoutGameTableName") String logoutGameTableName,
                                         @Param("regTableName") String regTableName,
                                         @Param("loginGameTableName") String loginGameTableName,
                                         @Param("statisTime") String statisTime);

    Integer updateChannelFirstRecharge(@Param("tableName") String tableName,
                                       @Param("logoutGameTableName") String logoutGameTableName,
                                       @Param("regTableName") String regTableName,
                                       @Param("statisTime") String statisTime);

    Integer updateChannelStatisData(@Param("tableName") String tableName,
                                    @Param("statisTime") String statisTime);
}
