package com.youwan.game.daemon.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-07-15.
 */
@Data
public class GameScoreAgg {

    private Integer gameId;
    private Integer userId;
    private Integer betCount;
    private String channelId;
    private BigDecimal score;
    private BigDecimal betFee;
    private BigDecimal revenueFee;
    private Date recordTime;

}
