package com.youwan.game.daemon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.daemon.entity.ExecutionLog;

/**
 * 任务日志处理
 *
 * @author Lion
 * @date 2018-10-27 10:55:42
 */
public interface ExecutionLogService extends IService<ExecutionLog> {

    //Boolean createStatisTable();

    //Boolean createStatisGameTable();

    //Boolean createStatisChannelTable();

    //Boolean processData();

    //Boolean processHisData();

    //Boolean processChannelData();

    //Boolean processAccountData();

    //Boolean processGameData();

    //Boolean crateBgBetRecordTable();

    //Boolean crateBgRedPacketRecordTable();

    //Boolean crateBgRedPacketExchangeRecordTable();
}
