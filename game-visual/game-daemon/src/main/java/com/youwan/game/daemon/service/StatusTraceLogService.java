package com.youwan.game.daemon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.daemon.entity.StatusTraceLog;

/**
 * 任务轨迹处理
 *
 * @author lion
 * @date 2018-10-28 07:18:05
 */
public interface StatusTraceLogService extends IService<StatusTraceLog> {

    Boolean synBgBetRecord();

    Boolean synBgBetDetail();

    Boolean synBgRedPacketExchangeRecord();

    Boolean synBgRedPacketRecord();

    //Boolean delRepeatCashOrder();

}
