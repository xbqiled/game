package com.youwan.game.daemon.job.scrub;

import com.youwan.game.daemon.service.tpscrub.TPSportRecordScrubber;
import lombok.extern.slf4j.Slf4j;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-06.
 */
@Slf4j
public class TPSportScrubJob extends AbstractScrubJob<TPSportRecordScrubber> {

    public TPSportScrubJob(TPSportRecordScrubber scrubber) {
        super(scrubber);
    }

}
