package com.youwan.game.daemon.job;


import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.youwan.game.daemon.service.ExecutionLogService;
import com.youwan.game.daemon.service.StatsDataProcessor;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;


/**
 * <B>定时同步历史数据</B>
 */
@Slf4j
@AllArgsConstructor
public class ProcessHisDataJob implements SimpleJob {



    //private final ExecutionLogService executionLogService;
    private StatsDataProcessor statsDataProcessor;
    /**
     * 业务执行逻辑
     *
     * @param shardingContext 分片信息
     */
    @Override
    public void execute(ShardingContext shardingContext) {
        //log.info("次日同步数据执行了:{}", shardingContext);
        //executionLogService.processHisData();
        statsDataProcessor.processHisData();
    }

}
