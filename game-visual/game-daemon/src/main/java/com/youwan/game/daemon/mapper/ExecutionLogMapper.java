package com.youwan.game.daemon.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.daemon.entity.ExecutionLog;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 任务日志处理
 *
 * @author lishangbu
 * @date 2018/11/22
 */
public interface ExecutionLogMapper extends BaseMapper<ExecutionLog> {

    /**
     * <B>删除对应的数据信息</B>
     * @param tableName
     * @param timeTableName
     * @param statisTime
     * @return
     */
    Integer delAccount(@Param("tableName")String tableName, @Param("timeTableName")String timeTableName, @Param("statisTime")String statisTime);


    /**
     * <B>通过统计时间获取信息</B>
     * @param tableName
     * @param statisTime
     * @return
     */
    List<Map<String, Object>> getStaticChannelList(@Param("tableName")String tableName, @Param("statisTime")String statisTime, @Param("step")Integer step);


    /**
     * <B>获取所有渠道信息</B>
     * @return
     */
    List<Map<String, Object>> getChannelList();


    /**
     * <B>获取游戏统计数据信息</B>
     * @param tableName
     * @param statisTime
     * @return
     */
    List<Map<String, Object>> getStaticGameList(@Param("tableName")String tableName, @Param("statisTime")String statisTime);

    /*********************************************渠道统计信息开始***********************************************/

    Integer insertStaticChannel(@Param("tableName")String tableName, @Param("channelId")String channelId, @Param("statisTime") Date statisTime);


    Integer updateRechargeCashData(@Param("tableName")String tableName,  @Param("statisTime") String statisTime);


    Integer updateLoginData(@Param("tableName")String tableName,  @Param("statisLoginTable") String statisLoginTable,  @Param("statisTime") String statisTime);


    Integer updateLoginGameData(@Param("tableName")String tableName,
                                @Param("regTableName") String regTableName,
                                @Param("loginGameTableName") String loginGameTableName,
                                @Param("statisTime") String statisTime);


    Integer updateGameTimeData(@Param("tableName")String tableName,
                               @Param("logoutGameTableName") String logoutGameTableName,
                               @Param("regTableName") String regTableName,
                               @Param("loginGameTableName") String loginGameTableName,
                               @Param("statisTime") String statisTime);


    Integer updateFirstRecharge(@Param("tableName")String tableName,
                                @Param("logoutGameTableName") String logoutGameTableName,
                                @Param("regTableName") String regTableName,
                                @Param("statisTime") String statisTime);


    Integer updateStatisData(@Param("tableName")String tableName,
                                @Param("statisTime") String statisTime);

    /*********************************************统计信息结束***********************************************/


    /*********************************************用户统计信息开始***********************************************/
    Integer insertUserBetData(@Param("tableName")String tableName,
                              @Param("betTableName") String betTableName,
                              @Param("statisTime") String statisTime);


//    Integer insertAccount(@Param("tableName")String tableName, @Param("timeTableName")String timeTableName, @Param("statisTime")String statisTime);


    Integer updateUserRechargeCashData(@Param("tableName")String tableName,  @Param("statisTime") String statisTime);




    Integer updateDamaBetData(@Param("tableName")String tableName,
                             @Param("gameTableName")String gameTableName,
                             @Param("matchPlayerRankTableName")String matchPlayerRankTableName,
                             @Param("statisTime") String statisTime);


    Integer updateUserGameTimeData(@Param("tableName")String tableName,
                               @Param("logoutGameTableName") String logoutGameTableName,
                               @Param("statisTime") String statisTime);


    Integer updateUserStatisData(@Param("tableName")String tableName,
                             @Param("statisTime") String statisTime);

    /*********************************************用户统计信息结束***********************************************/

    /********************************************游戏统计开始**************************************************/
    Integer insertGameStatisData(@Param("tableName")String tableName,
                                 @Param("gameTableName")String gameTableName,
                                 @Param("statisTime") String statisTime);

    Integer updateGameStatisTimeData(@Param("tableName")String tableName,
                                 @Param("logoutGameTableName")String logoutGameTableName,
                                 @Param("statisTime") String statisTime);

    Integer insertMatchGameStatisData(@Param("tableName")String tableName,
                                 @Param("matchPlayerRankTableName")String matchPlayerRankTableName,
                                 @Param("statisTime") String statisTime);

    Integer updateMatchGameStatisTimeData(@Param("tableName")String tableName,
                                     @Param("logoutGameTableName")String logoutGameTableName,
                                     @Param("statisTime") String statisTime);

    /******************************************游戏统计结束***************************************************/
    /**
     * <B>删除数据信息</B>
     * @param tableName
     * @param statisTime
     * @return
     */
    Integer delDate(@Param("tableName")String tableName, @Param("statisTime")String statisTime);
    /**
     * <B>创建新的表信息</B>
     * @param tableName
     * @return
     */
    Integer createDateTabel(@Param("tableName")String tableName);


    Integer createChannelDateTabel(@Param("tableName")String tableName);


    Integer createGameDateTabel(@Param("tableName")String tableName);
    /**
     * <B>创建历史表信息</B>
     * @return
     */
    Integer createAccountTable(@Param("tableName")String tableName);

    /**
     * <B>插入时间数据信息</B>
     * @param tableName
     * @return
     */
    Integer insertDate(@Param("tableName")String tableName, @Param("statisTime")String statisTime);

    /**
     * <B>通过时间获取数据</B>
     * @param tableName
     * @param statisTime
     * @return
     */
    Integer isHaveData(@Param("tableName")String tableName, @Param("statisTime")String statisTime);

    /**
     * <B>查询库里是否存在对应的表</B>
     * @param tableName
     * @return
     */
    Integer queryTableByName(@Param("tableName")String tableName);


    Integer queryTableByIndex(@Param("tableName")String tableName);
    /**
     * <B>创建索引</B>
     * @param tableName
     * @return
     */
    Integer createDataIdx(@Param("tableName")String tableName);

    /**
     * <B>删除主键</B>
     * @param tableName
     * @return
     */
    Integer dropPrimaryKey(@Param("tableName")String tableName);

    /**
     * <B>创建索引</B>
     * @param tableName
     * @return
     */
    Integer createAccountIdx(@Param("tableName")String tableName);


    /**
     * <B>创建新的表信息</B>
     * @param tableName
     * @return
     */
    Integer createBgBetDateTabel(@Param("tableName")String tableName);


    /**
     * <B>创建红包纪录表</B>
     * @param tableName
     * @return
     */
    Integer createBgRedPacketTabel(@Param("tableName")String tableName);


    /**
     * <B>创建红包兑换纪录纪录表</B>
     * @param tableName
     * @return
     */
    Integer createBgRedPacketExchangeTabel(@Param("tableName")String tableName);
}
