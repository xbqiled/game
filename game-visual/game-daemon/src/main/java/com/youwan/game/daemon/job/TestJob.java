package com.youwan.game.daemon.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-07-15.
 */
@Slf4j
@AllArgsConstructor
public class TestJob implements SimpleJob {

    @Override
    public void execute(ShardingContext shardingContext) {
        log.info("test job executed .............. >> [{}]", shardingContext.getShardingItem());
    }
}
