package com.youwan.game.daemon.service;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-07-17.
 */
public interface StatsDataProcessor {

    Boolean processHisData();

    Boolean processChannelData();

    Boolean processAccountData();

    Boolean processGameData();

}
