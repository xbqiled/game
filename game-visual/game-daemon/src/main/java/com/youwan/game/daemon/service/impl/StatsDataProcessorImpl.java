package com.youwan.game.daemon.service.impl;

import com.youwan.game.daemon.mapper.StatsDataMapper;
import com.youwan.game.daemon.service.StatsDataProcessor;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static com.youwan.game.common.core.constant.CommonConstant.SECONDS_OF_DAY;
import static com.youwan.game.daemon.common.TableConstants.*;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-07-17.
 */
@Slf4j
@Component
public class StatsDataProcessorImpl implements StatsDataProcessor {

    private StatsDataMapper statsDataMapper;

    public StatsDataProcessorImpl(StatsDataMapper statsDataMapper) {
        this.statsDataMapper = statsDataMapper;
    }

    @Override
    public Boolean processHisData() {
        LocalDateTime today = LocalDateTime.now().withTime(0, 0, 0, 0);
        LocalDateTime yesterday = today.minusDays(1);
        String yesterdayStr = yesterday.toString("yyyyMM");
        String yesterdayFmtStr = yesterday.toString("yyyyMMdd");
        /*Integer yesterdayCount = statsDataMapper.isHaveData(TIME_TABLE + yesterdayStr, yesterdayFmtStr);
        if (yesterdayCount > 0) {
            //删除数据信息

        }*/
        statsDataMapper.delStatisDateData(TIME_TABLE + yesterdayStr, yesterdayFmtStr);
        //插入数据
        //List<Map<String, Object>> accounts = statsDataMapper.queryAccountByStatsTime(yesterdayFmtStr);
        //Lists.partition(accounts, 50).forEach(subAccounts -> statsDataMapper.bulkInsertStatisDateData(TIME_TABLE + yesterdayStr, subAccounts));
        //return Boolean.TRUE;

        //插入数据
        List<Map<String, Object>> accounts;
        long seek = 0;
        int limit = 1000;
        while (!(accounts = statsDataMapper.queryAccountByStatsTime(seek, yesterdayFmtStr, limit)).isEmpty()) {
            seek = (long) accounts.get(accounts.size() - 1).get("t_regTime");
            statsDataMapper.bulkInsertStatisDateData(TIME_TABLE + yesterdayStr, accounts);
            if (accounts.size() < limit)
                break;
        }
        return Boolean.TRUE;
    }

    @Override
    public Boolean processChannelData() {
        LocalDateTime yesterday = LocalDateTime.now().withTime(0, 0, 0, 0).minusDays(1);
        String yesterdayMonthStr = yesterday.toString("yyyyMM");
        String yesterdayFmtStr = yesterday.toString("yyyy-MM-dd");

        Integer staticChannelCount = statsDataMapper.getStaticChannelCount(CHANNEL_TABLE + yesterdayMonthStr, yesterdayFmtStr);
        if (staticChannelCount > 0) {
            return Boolean.TRUE;
        }
        statsDataMapper.insertStaticChannel(CHANNEL_TABLE + yesterdayMonthStr, yesterdayFmtStr);
        //更新充值金额和提现金额
        statsDataMapper.updateChannelRechargeCashAggData(CHANNEL_TABLE + yesterdayMonthStr, yesterdayFmtStr);
        //登录人数
        statsDataMapper.updateChannelLoginAggData(CHANNEL_TABLE + yesterdayMonthStr,
                "recordlog_to.recorduserplatformlogin_" + yesterday.getMonthOfYear(), yesterdayFmtStr);
        //游戏人数
        statsDataMapper.updateChannelLoginGameAggData(CHANNEL_TABLE + yesterdayMonthStr,
                "recordlog_to.recordaccountreg_" + yesterday.getMonthOfYear(),
                "recordlog_to.recorduserlogingame_" + yesterday.getMonthOfYear(),
                yesterdayFmtStr);
        //游戏时长
        statsDataMapper.updateChannelGameTimeAggData(CHANNEL_TABLE + yesterdayMonthStr,
                "recordlog_to.recorduserlogoutgame_" + yesterday.getMonthOfYear(),
                "recordlog_to.recordaccountreg_" + yesterday.getMonthOfYear(),
                "recordlog_to.recorduserlogingame_" + yesterday.getMonthOfYear(),
                yesterdayFmtStr);
        //首次充值的金额
        statsDataMapper.updateChannelFirstRecharge(CHANNEL_TABLE + yesterdayMonthStr,
                "recordlog_to.recorduserlogoutgame_" + yesterday.getMonthOfYear(),
                "recordlog_to.recordaccountreg_" + yesterday.getMonthOfYear(),
                yesterdayFmtStr);
        //收尾,计算金额汇总
        statsDataMapper.updateChannelStatisData(CHANNEL_TABLE + yesterdayMonthStr, yesterdayFmtStr);
        return Boolean.TRUE;
    }

    @Override
    public Boolean processAccountData() {
        LocalDateTime yesterday = LocalDateTime.now().withTime(0, 0, 0, 0).minusDays(1);
        //Date yesterDate = DateUtil.parse("2020-03-10", "yyyy-MM-dd");
        String yesterdayStr = yesterday.toString("yyyyMM");
        //String yesterdayFmtStr = yesterday.toString("yyyyMMdd");
        String yesterdayFmtDashStr = yesterday.toString("yyyy-MM-dd");
        Integer yesterAccountStrCount = statsDataMapper.isHaveData(ACCOUNT_TABLE + yesterdayStr, yesterdayFmtDashStr);
        Integer start = Math.toIntExact(yesterday.toDate().getTime() / 1000);
        Integer end = start + SECONDS_OF_DAY;
        if (yesterAccountStrCount > 0) {
            return Boolean.TRUE;
        }
       /* List<Map<String, Object>> ret = statsDataMapper.queryTreasureChangeAgg(start, end);
        if (ret.isEmpty())
            return Boolean.TRUE;*/
        statsDataMapper.insertUserBetData(ACCOUNT_TABLE + yesterdayStr,
                "recordlog_to.usertreasurechange_" + yesterday.getMonthOfYear(),
                yesterdayFmtDashStr);
        //statsDataMapper.bulkInsertStatisAccountData(ACCOUNT_TABLE + yesterdayStr, ret);
        //更新充值提现数据信息
        statsDataMapper.updateUserRechargeCashData(ACCOUNT_TABLE + yesterdayStr, yesterdayFmtDashStr);
        //更新打码量信息
        statsDataMapper.updateUserDamaBetData(ACCOUNT_TABLE + yesterdayStr,
                "recordlog_to.recorduserscorepergame_" + yesterday.getMonthOfYear(),
                "recordlog_to.matchplayerrank_" + yesterday.getMonthOfYear(),
                yesterdayFmtDashStr);
        //更新用户游戏时长信息
        statsDataMapper.updateUserGameTimeData(ACCOUNT_TABLE + yesterdayStr,
                "recordlog_to.recorduserlogoutgame_" + yesterday.getMonthOfYear(), yesterdayFmtDashStr);
        //更新统计信息
        statsDataMapper.updateUserStatisData(ACCOUNT_TABLE + yesterdayStr, yesterdayFmtDashStr);
        return Boolean.TRUE;
    }

    @Override
    public Boolean processGameData() {
        LocalDateTime yesterday = LocalDateTime.now().withTime(0, 0, 0, 0).minusDays(1);
        String yesterdayMonthStr = yesterday.toString("yyyyMM");
        String yesterdayFmtStr = yesterday.toString("yyyy-MM-dd");

        Integer yesterdayCount = statsDataMapper.getStaticGameCount(GAME_TABLE + yesterdayMonthStr, yesterdayFmtStr);
        if (yesterdayCount > 0)
            return Boolean.TRUE;
        statsDataMapper.insertGameStatisData(GAME_TABLE + yesterdayMonthStr, "recordlog_to.recorduserscorepergame_" + yesterday.getMonthOfYear(),
                yesterdayFmtStr);
        statsDataMapper.updateGameStatisTimeData(GAME_TABLE + yesterdayMonthStr, "recordlog_to.recorduserlogoutgame_" + yesterday.getMonthOfYear(),
                yesterdayFmtStr);
        statsDataMapper.insertMatchGameStatisData(GAME_TABLE + yesterdayMonthStr,
                "recordlog_to.matchplayerrank_" + yesterday.getMonthOfYear(), yesterdayFmtStr);

        statsDataMapper.updateMatchGameStatisTimeData(GAME_TABLE + yesterdayMonthStr,
                "recordlog_to.recorduserlogoutgame_" + yesterday.getMonthOfYear(),
                yesterdayFmtStr);


        return Boolean.TRUE;
    }
}
