package com.youwan.game.daemon.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.daemon.entity.ExecutionLog;
import com.youwan.game.daemon.mapper.ExecutionLogMapper;
import com.youwan.game.daemon.service.ExecutionLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 任务日志处理
 *
 * @author lion
 * @date 2018/11/22
 */
@Slf4j
@Service("executionLogService")
public class ExecutionLogServiceImpl extends ServiceImpl<ExecutionLogMapper, ExecutionLog> implements ExecutionLogService {


    //登录平台用户表
    private static String userLoginPlatformTableName = "recordlog.recorduserplatformlogin";
    //登录游戏用户表
    private static String userLoginGameTableName = "recordlog.recorduserlogingame";
    //退出游戏用户表
    private static String userLogoutGameTableName = "recordlog.recorduserlogoutgame";
    //注册用户表
    private static String registerTableName = "recordlog.recordaccountreg";
    //用户账变表
    private static String userTreasureChangeName = "recordlog.usertreasurechange";
    //用户打码量记录表
    private static String damaChangeName = "recordlog.recorddamachange";
    //用户打码量记录表
    private static String scoreperGameName = "recordlog.recorduserscorepergame";
    //用户打码量记录表
    private static String matchpLayerRankName = "recordlog.matchplayerrank";



    //时间表名
    private static String timeTableName = "statis_date";
    //用户统计表名
    private static String accountTableName = "statis_account";
    //渠道统计表
    private static String channelTableName = "statis_channel";
    //游戏统计表
    private static String gameTableName = "statis_game";
    //bg下注信息表
    private static String bgBetRecordTableName = "bgbet_record";
    //bg红包信息
    private static String bgRedPacketExchangeTableName = "bgredpacket_exchange";
    //bg红包兑换信息
    private static String bgRedPacketRecordTableName = "bgredpacket_record";


    /*@Override
    public Boolean createStatisGameTable() {
        //获取当前日期时间
        Date todayDate = DateUtil.date();
        String todayStr = DateUtil.format(todayDate, "yyyyMM");

        //获取明天日期
        Date tomorrowDate = DateUtil.tomorrow();
        String tomorrowStr = DateUtil.format(tomorrowDate, "yyyyMM");

        //获取昨日日期
        Date yesterDate = DateUtil.yesterday();
        String yesterStr = DateUtil.format(yesterDate, "yyyyMM");

        //今天的游戏月表
        Integer todayGameTbList = baseMapper.queryTableByName(gameTableName + todayStr);

        //今天的表的索引
        Integer todayGameIndexList = baseMapper.queryTableByIndex(gameTableName + todayStr);

        //明天的游戏月表
        Integer tomorrowGameTbList = baseMapper.queryTableByName(gameTableName + tomorrowStr);

        //明天的游戏表索引
        Integer tomorrowGameIndexList = baseMapper.queryTableByIndex(gameTableName + tomorrowStr);

        //昨日的统计数据月表
        Integer yesterGameTbList = baseMapper.queryTableByName(gameTableName + yesterStr);

        //昨天游戏统计表索引
        Integer yesterGameIndexList = baseMapper.queryTableByIndex(gameTableName + yesterStr);


        //如果不存在就生成
        if (todayGameTbList == 0) {
            baseMapper.createGameDateTabel(gameTableName + todayStr);
        }

        //判断索引是否存在
        if (todayGameIndexList == 0) {
            String sql = getGameDataIdxSql(gameTableName + todayStr);
            baseMapper.createDataIdx(sql);
        }

        //如果不存在就生成
        if (tomorrowGameTbList == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            baseMapper.createGameDateTabel(gameTableName + tomorrowStr);
        }

        //判断索引是否存在
        if (tomorrowGameIndexList == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            String sql = getGameDataIdxSql(gameTableName + tomorrowStr);
            baseMapper.createDataIdx(sql);
        }

        //如果不存在就生成
        if (yesterGameTbList == 0 && !StrUtil.equalsIgnoreCase(todayStr, yesterStr)) {
            baseMapper.createGameDateTabel(gameTableName + yesterStr);
        }

        if (yesterGameIndexList == 0 && !StrUtil.equalsIgnoreCase(todayStr, yesterStr)) {
            String sql = getGameDataIdxSql(gameTableName + yesterStr);
            baseMapper.createAccountIdx(sql);
        }
        return Boolean.TRUE;
    }*/

    /**
     * <B>定时创建表信息</B>
     * @return
     */
    /*@Override
    public Boolean createStatisTable() {
        //获取当前日期时间
        Date todayDate = DateUtil.date();
        String todayStr = DateUtil.format(todayDate, "yyyyMM");

        //获取明天日期
        Date tomorrowDate = DateUtil.tomorrow();
        String tomorrowStr = DateUtil.format(tomorrowDate, "yyyyMM");

        //获取昨日日期
        Date yesterDate = DateUtil.yesterday();
        String yesterStr = DateUtil.format(yesterDate, "yyyyMM");

        //判断月表是否存在
        //今天的时间月表
        Integer todayTimeTbList = baseMapper.queryTableByName(timeTableName + todayStr);

        //今天的表的索引
        Integer todayTimeIndexList = baseMapper.queryTableByIndex(timeTableName + todayStr);

        //今天的时间月表
        Integer tomorrowTimeTbList = baseMapper.queryTableByName(timeTableName + tomorrowStr);

        //今天的表的索引
        Integer tomorrowTimeIndexList = baseMapper.queryTableByIndex(timeTableName + todayStr);

        //今天的统计数据月表
        Integer todayAccountTbList = baseMapper.queryTableByName(accountTableName + todayStr);

        Integer todayAccountIndexList = baseMapper.queryTableByIndex(accountTableName + todayStr);

        //昨日的统计数据月表
        Integer yesterAccountTbList = baseMapper.queryTableByName(accountTableName + yesterStr);

        Integer yesterAccountIndexList = baseMapper.queryTableByIndex(accountTableName + yesterStr);

        //如果不存在就生成
        if (todayTimeTbList == 0) {
            baseMapper.createDateTabel(timeTableName + todayStr);
        }

        //判断索引是否存在
        if (todayTimeIndexList == 0) {
            String sql = getDataIdxSql(timeTableName + todayStr);
            baseMapper.createDataIdx(sql);
        }

        //如果不存在就生成
        if (tomorrowTimeTbList == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            baseMapper.createDateTabel(timeTableName + tomorrowStr);
        }

        //判断索引是否存在
        if (tomorrowTimeIndexList == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            String sql = getDataIdxSql(timeTableName + tomorrowStr);
            baseMapper.createDataIdx(sql);
        }

        //如果不存在就生成
        if (todayAccountTbList == 0) {
            baseMapper.createAccountTable(accountTableName + todayStr);
        }

        if (todayAccountIndexList == 0) {
            String sql = getAccountIdxSql(accountTableName + todayStr);
            baseMapper.createAccountIdx(sql);
        }

        //如果不存在就生成
        if (yesterAccountTbList == 0) {
            baseMapper.createAccountTable(accountTableName + yesterStr);
        }

        if (yesterAccountIndexList == 0) {
            String sql = getAccountIdxSql(accountTableName + yesterStr);
            baseMapper.createAccountIdx(sql);
        }
        return Boolean.TRUE;
    }*/


    /*@Override
    public Boolean createStatisChannelTable() {
        //获取当前日期时间
        Date todayDate = DateUtil.date();
        String todayStr = DateUtil.format(todayDate, "yyyyMM");

        //获取明天日期
        Date tomorrowDate = DateUtil.tomorrow();
        String tomorrowStr = DateUtil.format(tomorrowDate, "yyyyMM");

        //获取昨日日期
        Date yesterDate = DateUtil.yesterday();
        String yesterStr = DateUtil.format(yesterDate, "yyyyMM");

        //判断月表是否存在
        //今天的时间月表
        Integer todayChannelTbList = baseMapper.queryTableByName(channelTableName + todayStr);

        //今天的表的索引
        Integer todayChannelIndexList = baseMapper.queryTableByIndex(channelTableName + todayStr);

        //明天的时间月表
        Integer tomorrowChannelTbList = baseMapper.queryTableByName(channelTableName + tomorrowStr);

        //今天的表的索引
        Integer tomorrowChannelIndexList = baseMapper.queryTableByIndex(channelTableName + tomorrowStr);

        //昨日的统计数据月表
        Integer yesterChannelTbList = baseMapper.queryTableByName(channelTableName + yesterStr);

        Integer yesterChannelIndexList = baseMapper.queryTableByIndex(channelTableName + yesterStr);

        //如果不存在就生成
        if (todayChannelTbList == 0) {
            baseMapper.createChannelDateTabel(channelTableName + todayStr);
        }

        //判断索引是否存在
        if (todayChannelIndexList == 0) {
            String sql = getChannelDataIdxSql(channelTableName + todayStr);
            baseMapper.createDataIdx(sql);
        }

        //如果不存在就生成
        if (tomorrowChannelTbList == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            baseMapper.createChannelDateTabel(channelTableName + tomorrowStr);
        }

        //判断索引是否存在
        if (tomorrowChannelIndexList == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            String sql = getChannelDataIdxSql(channelTableName + tomorrowStr);
            baseMapper.createDataIdx(sql);
        }

        //如果不存在就生成
        if (yesterChannelTbList == 0 && !StrUtil.equalsIgnoreCase(todayStr, yesterStr)) {
            baseMapper.createChannelDateTabel(channelTableName + yesterStr);
        }

        if (yesterChannelIndexList == 0 && !StrUtil.equalsIgnoreCase(todayStr, yesterStr)) {
            String sql = getChannelDataIdxSql(channelTableName + yesterStr);
            baseMapper.createAccountIdx(sql);
        }
        return Boolean.TRUE;
    }*/


    /**
     * <B>生成索引sql</B>
     * @return
     */
    /*private String getDataIdxSql(String tableName) {
        StringBuffer sb = new StringBuffer();
        sb.append("ALTER TABLE `"+ tableName+ "` ");
        sb.append("ADD INDEX `idx_" + tableName + "_userId`(`user_id`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_channelId`(`channel_id`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_statisTime`(`statis_time`) USING BTREE; ");
        return sb.toString();
    }


    private String getChannelDataIdxSql(String tableName) {
        StringBuffer sb = new StringBuffer();
        sb.append("ALTER TABLE `"+ tableName+ "` ");
        sb.append("ADD INDEX `idx_" + tableName + "_channelId`(`channel_id`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_statisTime`(`statis_time`) USING BTREE; ");
        return sb.toString();
    }


    private String getGameDataIdxSql(String tableName) {
        StringBuffer sb = new StringBuffer();
        sb.append("ALTER TABLE `"+ tableName+ "` ");
        sb.append("ADD INDEX `idx_" + tableName + "_userId`(`user_id`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_channelId`(`channel_id`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_statisTime`(`statis_time`) USING BTREE; ");
        return sb.toString();
    }*/


    private String dropPrimaryKey(String tableName) {
        StringBuffer sb = new StringBuffer();
        sb.append("ALTER TABLE `"+ tableName+ "` ");
        sb.append("DROP PRIMARY KEY");
        return sb.toString();
    }


    /**
     * <B>生成索引sql</B>
     * @return
     */
    /*private String getAccountIdxSql(String tableName) {
        StringBuffer sb = new StringBuffer();
        sb.append("ALTER TABLE `"+ tableName+ "` ");
        sb.append("ADD INDEX `idx_" + tableName + "_userId`(`user_id`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_channelId`(`channel_id`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_statisTime`(`statis_time`) USING BTREE; ");
        return sb.toString();
    }*/


    /*@Override
    public Boolean processData() {
        try {
            //获取当前日期时间
            Date todayDate = DateUtil.date();
            String todayStr = DateUtil.format(todayDate, "yyyyMM");
            String todayFormatStr = DateUtil.format(todayDate, "yyyyMMdd");

            //获取明天日期
            Date tomorrowDate = DateUtil.tomorrow();
            String tomorrowStr = DateUtil.format(tomorrowDate, "yyyyMM");
            String tomorrowFormatStr = DateUtil.format(tomorrowDate, "yyyyMMdd");

            //查询今天的时间数据是否存在
            Integer todayCount = baseMapper.isHaveData(timeTableName + todayStr, todayFormatStr);
            //查询明天数据是否存在
            Integer tomorrowCount = baseMapper.isHaveData(timeTableName + tomorrowStr, tomorrowFormatStr);

            //插入今天的数据信息
            if (todayCount > 0) {
                baseMapper.delDate(timeTableName + todayStr, todayFormatStr);
            }
            baseMapper.insertDate(timeTableName + todayStr, todayFormatStr);

            //插入明天的数据信息
            if (tomorrowCount == 0) {
                baseMapper.insertDate(timeTableName + tomorrowStr, tomorrowFormatStr);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }*/


    //@Override
    public Boolean processHisData() {
        //删除昨天的数据，生成一批新的昨天的数据, 1点执行
        //获取昨日日期
        Date yesterDate = DateUtil.yesterday();
        String yesterStr = DateUtil.format(yesterDate, "yyyyMM");
        String yesterFormatStr = DateUtil.format(yesterDate, "yyyyMMdd");

        //查询统计用户是否存在
        Integer yesterCount = baseMapper.isHaveData(timeTableName + yesterStr, yesterFormatStr);
        if (yesterCount > 0) {
            //删除数据信息
            baseMapper.delDate(timeTableName + yesterStr, yesterFormatStr);
        }

        //插入数据
        baseMapper.insertDate(timeTableName + yesterStr, yesterFormatStr);
        return Boolean.TRUE;
    }

    /**
     * <B>加工数据信息</B>
     * @return
     */
    //@Override
    public Boolean processAccountData() {
        Date yesterDate = DateUtil.yesterday();
        //Date yesterDate = DateUtil.parse("2020-03-10", "yyyy-MM-dd");
        String yesterStr = DateUtil.format(yesterDate, "yyyyMM");
        String yesterFormatStr = DateUtil.format(yesterDate, "yyyyMMdd");
        String yesterDayStr = DateUtil.format(yesterDate, "yyyy-MM-dd");

        Integer yesterAccountStrCount = baseMapper.isHaveData(accountTableName + yesterStr, yesterFormatStr);
        if (yesterAccountStrCount == 0) {
            //更新投注信息
            baseMapper.insertUserBetData(accountTableName + yesterStr, userTreasureChangeName + yesterFormatStr,  yesterDayStr);

            //baseMapper.insertAccount(accountTableName + yesterStr, timeTableName + yesterStr, yesterFormatStr);
            //更新充值提现数据信息
            Integer rechargeCash = baseMapper.updateUserRechargeCashData(accountTableName + yesterStr, yesterDayStr);

            //更新打码量信息
            Integer dama = baseMapper.updateDamaBetData(accountTableName + yesterStr, scoreperGameName + yesterFormatStr,
                    matchpLayerRankName + yesterFormatStr,   yesterDayStr);
            //更新用户游戏时长信息
            Integer logOut = baseMapper.updateUserGameTimeData(accountTableName + yesterStr, userLogoutGameTableName + yesterFormatStr,  yesterDayStr);
            //更新统计信息
            Integer statis = baseMapper.updateUserStatisData(accountTableName + yesterStr, yesterDayStr);
        }
        return Boolean.TRUE;
    }



    /*@Override
    public Boolean processChannelData() {
        try {
            //查询今天的数据是否存在
            Date yesterDate = DateUtil.yesterday();
            //Date yesterDate = DateUtil.parse("2020-02-27", "yyyy-MM-dd");

            String yesterYearStr = DateUtil.format(yesterDate, "yyyy");
            String yesterMonthStr = DateUtil.format(yesterDate, "yyyyMM");
            String yesterDayStr = DateUtil.format(yesterDate, "yyyyMMdd");
            String yesterDay = DateUtil.format(yesterDate, "yyyy-MM-dd");

            //查询昨日的数据是否统计
            List<Map<String, Object>> resultList = baseMapper.getStaticChannelList(channelTableName + yesterMonthStr, yesterDay, null);
            //判断一条数据，没有走完

            //第一步插入渠道信息
            if (CollUtil.isEmpty(resultList)) {
                //插入数据信息
                List<Map<String, Object>> channelList = baseMapper.getChannelList();
                for (Map<String, Object> map : channelList) {
                    String channelId = (String) map.get("channel_id");
                    baseMapper.insertStaticChannel(channelTableName + yesterMonthStr, channelId, yesterDate);
                }

                //更新充值金额和提现金额
                Integer rechargeCashResult =  baseMapper.updateRechargeCashData(channelTableName + yesterMonthStr, yesterDay);

                //登录人数
                Integer loginResult =  baseMapper.updateLoginData(channelTableName + yesterMonthStr, userLoginPlatformTableName + yesterDayStr, yesterDay);

                //游戏人数
                Integer loginGameResult =  baseMapper.updateLoginGameData(channelTableName + yesterMonthStr,
                        registerTableName + yesterYearStr,   userLoginGameTableName + yesterDayStr, yesterDay);

                //游戏时长
                Integer timeLengthResult =  baseMapper.updateGameTimeData(channelTableName + yesterMonthStr,
                        userLogoutGameTableName + yesterDayStr, registerTableName + yesterYearStr,
                        userLoginGameTableName + yesterDayStr, yesterDay);

                //首次充值的金额
                Integer firstRechargeResult =  baseMapper.updateFirstRecharge(channelTableName + yesterMonthStr,
                        userLogoutGameTableName + yesterDayStr, registerTableName + yesterYearStr, yesterDay);

                //计算金额汇总
                Integer statisResult =  baseMapper.updateStatisData(channelTableName + yesterMonthStr, yesterDay);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Boolean.TRUE;
    }*/


    /*@Override
    public Boolean processGameData() {
        Date yesterDate = DateUtil.yesterday();
        //Date yesterDate = DateUtil.parse("2020-03-08", "yyyy-MM-dd");
        String yesterMonthStr = DateUtil.format(yesterDate, "yyyyMM");
        String yesterDayStr = DateUtil.format(yesterDate, "yyyyMMdd");
        String yesterDay = DateUtil.format(yesterDate, "yyyy-MM-dd");

        //查询昨日的数据是否统计
        List<Map<String, Object>> resultList = baseMapper.getStaticGameList(gameTableName + yesterMonthStr, yesterDay);
        //判断一条数据，没有走完

        //第一步插入渠道信息
        if (CollUtil.isEmpty(resultList)) {
            //插入数据信息
            Integer gameStatisResult =  baseMapper.insertGameStatisData(gameTableName + yesterMonthStr, scoreperGameName + yesterDayStr,  yesterDay);
            Integer gameLengthResult =  baseMapper.updateGameStatisTimeData(gameTableName + yesterMonthStr,userLogoutGameTableName + yesterDayStr,  yesterDay);

            Integer matchStatisResult =  baseMapper.insertMatchGameStatisData(gameTableName + yesterMonthStr, matchpLayerRankName + yesterDayStr,  yesterDay);
            Integer matchLengthResult =  baseMapper.updateMatchGameStatisTimeData(gameTableName + yesterMonthStr, userLogoutGameTableName + yesterDayStr,  yesterDay);
        }
        return Boolean.TRUE;
    }*/

    /**
     * <B>创建BG表信息</B>
     * @return
     */
    /*@Override
    public Boolean crateBgBetRecordTable() {
        //获取当前日期时间
        Date todayDate = DateUtil.date();
        String todayStr = DateUtil.format(todayDate, "yyyyMM");

        //获取明天日期
        Date tomorrowDate = DateUtil.tomorrow();
        String tomorrowStr = DateUtil.format(tomorrowDate, "yyyyMM");

        //判断月表是否存在
        //今天的时间月表
        Integer todayTimeTbList = baseMapper.queryTableByName(bgBetRecordTableName + todayStr);

        //今天的表的索引
        Integer todayTimeIndexList = baseMapper.queryTableByIndex(bgBetRecordTableName + todayStr);

        //明天的时间月表
        Integer tomorrowTimeTbList = baseMapper.queryTableByName(bgBetRecordTableName + tomorrowStr);

        //今天的表的索引
        Integer tomorrowTimeIndexList = baseMapper.queryTableByIndex(bgBetRecordTableName + tomorrowStr);

        //如果不存在就生成
        if (todayTimeTbList == 0) {
            baseMapper.createBgBetDateTabel(bgBetRecordTableName + todayStr);
        }

        //判断索引是否存在
        if (todayTimeIndexList == 0) {
            String sql = getBgBetDataIdxSql(bgBetRecordTableName + todayStr);
            baseMapper.createDataIdx(sql);
        }

        //如果不存在就生成
        if (tomorrowTimeTbList == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            baseMapper.createBgBetDateTabel(bgBetRecordTableName + tomorrowStr);
        }

        //判断索引是否存在
        if (tomorrowTimeIndexList == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            String sql = getBgBetDataIdxSql(bgBetRecordTableName + tomorrowStr);
            baseMapper.createDataIdx(sql);
        }

        return Boolean.TRUE;
    }*/


    /*private String getBgBetDataIdxSql(String tableName) {
        StringBuffer sb = new StringBuffer();
        sb.append("ALTER TABLE `"+ tableName+ "` ");
        sb.append("ADD UNIQUE INDEX `idx_" + tableName + "_orderId`(`order_id`, `login_id`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_loginId`(`login_id`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_gameId`(`game_id`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_uId`(`uid`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_gameName`(`game_name`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_issueId`(`issue_id`) USING BTREE; ");
        return sb.toString();
    }*/

    /*@Override
    public Boolean crateBgRedPacketRecordTable() {
        //获取当前日期时间
        Date todayDate = DateUtil.date();
        String todayStr = DateUtil.format(todayDate, "yyyyMM");

        //获取明天日期
        Date tomorrowDate = DateUtil.tomorrow();
        String tomorrowStr = DateUtil.format(tomorrowDate, "yyyyMM");

        //判断月表是否存在
        //今天的时间月表
        Integer todayTimeTbList = baseMapper.queryTableByName(bgRedPacketRecordTableName + todayStr);

        //今天的表的索引
        Integer todayTimeIndexList = baseMapper.queryTableByIndex(bgRedPacketRecordTableName + todayStr);

        //明天的时间月表
        Integer tomorrowTimeTbList = baseMapper.queryTableByName(bgRedPacketRecordTableName + tomorrowStr);

        //今天的表的索引
        Integer tomorrowTimeIndexList = baseMapper.queryTableByIndex(bgRedPacketRecordTableName + tomorrowStr);

        //如果不存在就生成
        if (todayTimeTbList == 0) {
            baseMapper.createBgRedPacketTabel(bgRedPacketRecordTableName + todayStr);
        }

        //判断索引是否存在
        if (todayTimeIndexList == 0) {
            String sql = getBgRedPacketDataIdxSql(bgRedPacketRecordTableName + todayStr);
            baseMapper.createDataIdx(sql);
        }

        //如果不存在就生成
        if (tomorrowTimeTbList == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            baseMapper.createBgRedPacketTabel(bgRedPacketRecordTableName + tomorrowStr);
        }

        //判断索引是否存在
        if (tomorrowTimeIndexList == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            String sql = getBgRedPacketDataIdxSql(bgRedPacketRecordTableName + tomorrowStr);
            baseMapper.createDataIdx(sql);
        }

        return Boolean.TRUE;
    }*/


    /*public String getBgRedPacketDataIdxSql(String tableName) {
        StringBuffer sb = new StringBuffer();
        sb.append("ALTER TABLE `"+ tableName+ "` ");
        sb.append("ADD INDEX `idx_" + tableName + "_uid`(`uid`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_time`(`time`) USING BTREE, ");
        sb.append("ADD UNIQUE INDEX `idx_" + tableName + "_pid`(`pid`) USING BTREE; ");
        return sb.toString();
    }*/

    /*@Override
    public Boolean crateBgRedPacketExchangeRecordTable() {
        //获取当前日期时间
        Date todayDate = DateUtil.date();
        String todayStr = DateUtil.format(todayDate, "yyyyMM");

        //获取明天日期
        Date tomorrowDate = DateUtil.tomorrow();
        String tomorrowStr = DateUtil.format(tomorrowDate, "yyyyMM");

        //判断月表是否存在
        //今天的时间月表
        Integer todayTimeTbList = baseMapper.queryTableByName(bgRedPacketExchangeTableName + todayStr);

        //今天的表的索引
        Integer todayTimeIndexList = baseMapper.queryTableByIndex(bgRedPacketExchangeTableName + todayStr);

        //明天的时间月表
        Integer tomorrowTimeTbList = baseMapper.queryTableByName(bgRedPacketExchangeTableName + tomorrowStr);

        //今天的表的索引
        Integer tomorrowTimeIndexList = baseMapper.queryTableByIndex(bgRedPacketExchangeTableName + tomorrowStr);

        //如果不存在就生成
        if (todayTimeTbList == 0) {
            baseMapper.createBgRedPacketExchangeTabel(bgRedPacketExchangeTableName + todayStr);
        }

        //判断索引是否存在
        if (todayTimeIndexList == 0) {
            String sql = getBgRedPacketExchangeDataIdxSql(bgRedPacketExchangeTableName + todayStr);
            baseMapper.createDataIdx(sql);
        }

        //如果不存在就生成
        if (tomorrowTimeTbList == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            baseMapper.createBgRedPacketExchangeTabel(bgRedPacketExchangeTableName + tomorrowStr);
        }

        //判断索引是否存在
        if (tomorrowTimeIndexList == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            String sql = getBgRedPacketExchangeDataIdxSql(bgRedPacketExchangeTableName + tomorrowStr);
            baseMapper.createDataIdx(sql);
        }

        return Boolean.TRUE;
    }*/

    /*public String getBgRedPacketExchangeDataIdxSql(String tableName) {
        StringBuffer sb = new StringBuffer();
        sb.append("ALTER TABLE `"+ tableName+ "` ");
        sb.append("ADD INDEX `idx_" + tableName + "_uid`(`uid`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_time`(`time`) USING BTREE, ");
        sb.append("ADD UNIQUE INDEX `idx_" + tableName + "_pid`(`pid`) USING BTREE; ");
        return sb.toString();
    }*/

}
