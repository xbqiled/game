package com.youwan.game.daemon.service.tpscrub;

import cn.hutool.core.util.NumberUtil;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.ImmutableList;
import com.youwan.game.common.core.betting.ScrubConfig;
import com.youwan.game.common.core.betting.ScrubResult;
import com.youwan.game.common.core.betting.entity.UserAccountInfo;
import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import com.youwan.game.common.data.elasticsearch.operations.IndexOperation;
import com.youwan.game.common.data.elasticsearch.operations.UpdateByQueryOperation;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.collect.MapBuilder;
import org.elasticsearch.common.collect.Tuple;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.script.Script;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.composite.CompositeAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.composite.ParsedComposite;
import org.elasticsearch.search.aggregations.bucket.composite.TermsValuesSourceBuilder;
import org.elasticsearch.search.aggregations.metrics.Sum;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static com.youwan.game.common.data.elasticsearch.ElasticsearchOperations.DEFAULT_TYPE;
import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;
import static org.elasticsearch.script.Script.DEFAULT_SCRIPT_LANG;
import static org.elasticsearch.script.Script.DEFAULT_SCRIPT_TYPE;
import static org.elasticsearch.search.aggregations.AggregationBuilders.sum;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-21.
 */
@Slf4j
public abstract class AbstractBettingRecordScrubber implements ThirdPartyRecordScrubber {

    private ElasticsearchTemplate elasticsearchTemplate;

    private ScrubConfigService scrubConfigService;

    public AbstractBettingRecordScrubber(ElasticsearchTemplate elasticsearchTemplate, ScrubConfigService scrubConfigService) {
        this.elasticsearchTemplate = elasticsearchTemplate;
        this.scrubConfigService = scrubConfigService;
    }

    protected ElasticsearchTemplate getElasticsearchTemplate() {
        return elasticsearchTemplate;
    }

    @Override
    public String initialMark(int shard) {
        String epoch = DateTime.now().toString(DateTimeFormat.forPattern("yyyyMMdd"));
        initMark(epoch, shard);//
        return epoch;
    }

    private void initMark(String epoch, int shard) {
        BoolQueryBuilder query = boolQuery();
        query.must(QueryBuilders.termQuery("scrub_state", 0));

        Script script = new Script(DEFAULT_SCRIPT_TYPE, DEFAULT_SCRIPT_LANG, "ctx._source.scrub_state = 1;",
                MapBuilder.newMapBuilder(new HashMap<String, Object>()).put("epoch", epoch).immutableMap());
        UpdateByQueryOperation updateByQueryOperation = new UpdateByQueryOperation(getTargetIndex(), "_doc", query, script);
        if (needSharding()) {
            updateByQueryOperation.setPreference("_shards:" + shard);
        }
        getElasticsearchTemplate().updateByQuery(updateByQueryOperation);
    }

    @Override
    public List<ScrubResult> scrub(String epoch, int shard) throws IOException {
        Integer after = null;
        List<ScrubResult> ret = new ArrayList<>();
        for (; ; ) {
            Tuple<Integer, List<ScrubResult>> tuple = doScrub(epoch, after, shard);
            if (tuple.v2().isEmpty())
                break;
            ret.addAll(tuple.v2());
            after = tuple.v1();
        }
        log.error("scrub >>>>>>>>>>> {}, {}", shard, JSON.toJSONString(ret));
        return ret;
    }

    private Tuple<Integer, List<ScrubResult>> doScrub(String epoch, Integer after, int shard) throws IOException {
        SearchRequest searchRequest = new SearchRequest(getTargetIndex());//.preference("_shards:" + shard);
        if (needSharding()){
            searchRequest.preference("_shards:" + shard);
        }
        CompositeAggregationBuilder composite = AggregationBuilders.composite("buckets",
                ImmutableList.of(new TermsValuesSourceBuilder("uid").field(getUidField()))).subAggregation(sum(
                "bettingMoney").field(getSumAggField())).size(100);
        if (after != null) {
            composite.aggregateAfter(MapBuilder.newMapBuilder(new HashMap<String, Object>()).put("uid", after).immutableMap());
        }
        BoolQueryBuilder query = boolQuery();
        query/*.must(termQuery("scrub_epoch", epoch))*/.must(termQuery("scrub_state", 1));
        SearchSourceBuilder source =
                new SearchSourceBuilder().query(query).aggregation(composite);
        SearchResponse response = getElasticsearchTemplate().getClient().search(searchRequest.source(source), RequestOptions.DEFAULT);
        ParsedComposite compositeAgg = response.getAggregations().get("buckets");
        Integer afterKey = compositeAgg.afterKey() == null ? null : (Integer) compositeAgg.afterKey().get("uid");
        List<ScrubResult> ret = new ArrayList<>();
        compositeAgg.getBuckets().forEach(bucket -> {
            ScrubResult result = new ScrubResult();
            result.setEpoch(epoch);
            result.setType(getScrubType());
            result.setUid(Long.valueOf((Integer) bucket.getKey().get("uid")));
            if (needDividedByOneHundred()) {
                result.setValue(BigDecimal.valueOf(((Sum) bucket.getAggregations().get("bettingMoney")).getValue()).divide(BigDecimal.valueOf(100),
                        2, RoundingMode.HALF_UP));
            } else {
                result.setValue(BigDecimal.valueOf(((Sum) bucket.getAggregations().get("bettingMoney")).getValue()));
            }

            ret.add(result);
        });
        return new Tuple<>(afterKey, ret);
    }

    @Override
    public void archive(List<ScrubResult> data) {
        List<IndexOperation<?>> indexOperations = data.stream().map(item -> {
            item.setResult(getScrubResult(item));
            return new IndexOperation<>("scrub_result", DEFAULT_TYPE, item);
        }).collect(Collectors.toList());
        elasticsearchTemplate.bulkIndexAsync(indexOperations);
    }

    private BigDecimal getScrubResult(ScrubResult scrubResult) {
        Long uid = scrubResult.getUid();
        UserAccountInfo user = elasticsearchTemplate.get("account_info", "_doc", uid.toString(), UserAccountInfo.class);
        if (user == null)
            return BigDecimal.valueOf(0d);
        String channelId = user.getT_channelKey();
        Integer vipLevel = user.getT_vipLevel();
        if (channelId == null || vipLevel == null || vipLevel == 0)
            return BigDecimal.valueOf(0d);
        ScrubConfig scrubConfig = scrubConfigService.getScrubConfig(channelId, vipLevel);
        Double rebateRatio = 0d;
        switch (scrubResult.getType()) {
            case ScrubResult.TYPE_LOTTERY:
                rebateRatio = scrubConfig.getLottery();
                break;
            case ScrubResult.TYPE_CROWN:
            case ScrubResult.TYPE_SPORT:
                rebateRatio = scrubConfig.getSport();
                break;
            case ScrubResult.TYPE_EGAME:
                rebateRatio = scrubConfig.getEgame();
                break;
            case ScrubResult.TYPE_LIVE:
                rebateRatio = scrubConfig.getLive();
                break;
            case ScrubResult.TYPE_QP:
                rebateRatio = scrubConfig.getQp();
                break;
            default:
        }
        scrubResult.setRebateRatio(BigDecimal.valueOf(rebateRatio));
        return NumberUtil.round(NumberUtil.mul(scrubResult.getValue(), rebateRatio, 0.01d), 2);
    }

    public void finishMark(String epoch, int shard) {
        BoolQueryBuilder query = boolQuery();
        query.must(termQuery("scrub_state", 1));
        Script script = new Script(DEFAULT_SCRIPT_TYPE, DEFAULT_SCRIPT_LANG, "ctx._source.scrub_state = 2;ctx._source.scrub_epoch = params.epoch;",
                MapBuilder.newMapBuilder(new HashMap<String, Object>()).put("epoch", epoch).immutableMap());
        UpdateByQueryOperation updateByQueryOperation =  new UpdateByQueryOperation(getTargetIndex(), DEFAULT_TYPE, query, script);
        if (needSharding()){
            updateByQueryOperation.setPreference("_shards:" + shard);
        }
        getElasticsearchTemplate().updateByQuery(updateByQueryOperation);
    }

    abstract String getTargetIndex();

    abstract String getSumAggField();

    abstract int getScrubType();

    protected String getUidField() {
        return "userId";
    }

    protected boolean needDividedByOneHundred() {
        return false;
    }

    protected boolean needSharding() {
        return true;
    }
}
