package com.youwan.game.daemon.job.scrub;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.youwan.game.daemon.service.tpscrub.AbstractBettingRecordScrubber;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-05.
 */
@Slf4j
public abstract class AbstractScrubJob<T extends AbstractBettingRecordScrubber> implements SimpleJob {

    private T scrubber;

    public AbstractScrubJob(T scrubber) {
        this.scrubber = scrubber;
    }

    @SneakyThrows
    @Override
    public void execute(ShardingContext shardingContext) {
        int shard = shardingContext.getShardingItem();
        String epoch = scrubber.initialMark(shard);
        Thread.sleep(3000);
        scrubber.archive(scrubber.scrub(epoch, shard));
        scrubber.finishMark(epoch, shard);
    }
}
