package com.youwan.game.daemon.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.youwan.game.daemon.service.StatusTraceLogService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@AllArgsConstructor
public class SynBgBetDetailJob implements SimpleJob {

    private final StatusTraceLogService statusTraceLogService;

    @Override
    public void execute(ShardingContext shardingContext) {
        statusTraceLogService.synBgBetDetail();
        log.info("更新BG注单详情信息表任务执行啦:{}", shardingContext);
    }
}
