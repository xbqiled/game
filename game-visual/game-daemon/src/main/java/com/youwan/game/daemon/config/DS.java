package com.youwan.game.daemon.config;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-05-29.
 */
public interface DS {
    String XBQP = "xbqp";
    String RECORD_LOG = "record_log";
    String PLATFORM = "platform_ds";
}
