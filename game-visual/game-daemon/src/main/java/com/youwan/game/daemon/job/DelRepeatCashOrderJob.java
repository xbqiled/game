package com.youwan.game.daemon.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.youwan.game.daemon.service.CashOrderProcessor;
import com.youwan.game.daemon.service.StatusTraceLogService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@AllArgsConstructor
public class DelRepeatCashOrderJob implements SimpleJob {

    //private final StatusTraceLogService statusTraceLogService;
    private CashOrderProcessor cashOrderProcessor;

    @Override
    public void execute(ShardingContext shardingContext) {
        //statusTraceLogService.delRepeatCashOrder();
        //log.info("delete repeat cas order task ");
        cashOrderProcessor.delRepeatCashOrder();
    }
}
