package com.youwan.game.daemon.service.tpscrub;

import com.youwan.game.common.core.betting.ScrubResult;
import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-06.
 */
@Component
@Slf4j
public class CrownSportRecordScrubber extends AbstractBettingRecordScrubber{

    public CrownSportRecordScrubber(ElasticsearchTemplate elasticsearchTemplate, ScrubConfigService scrubConfigService) {
        super(elasticsearchTemplate, scrubConfigService);
    }

    @Override
    String getTargetIndex() {
        return "crown_sport_betting_order";
    }

    @Override
    String getSumAggField() {
        return "bettingMoney";
    }

    @Override
    int getScrubType() {
        return ScrubResult.TYPE_CROWN;
    }
}
