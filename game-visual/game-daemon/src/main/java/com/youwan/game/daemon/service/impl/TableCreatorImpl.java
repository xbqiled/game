package com.youwan.game.daemon.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.youwan.game.daemon.mapper.CreateTableMapper;
import com.youwan.game.daemon.service.TableCreator;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-07-16.
 */
@Component
@Slf4j
public class TableCreatorImpl implements TableCreator {


    private CreateTableMapper createTableMapper;

    //时间表名
    private static final String TIME_TABLE = "statis_date";
    //用户统计表名
    private static final String ACCOUNT_TABLE = "statis_account";
    //渠道统计表
    private static final String CHANNEL_TABLE = "statis_channel";
    //游戏统计表
    private static final String GAME_TABLE = "statis_game";
    //bg下注信息表
    private static final String BG_BET_RECORD_TABLE = "bgbet_record";

    //bg红包兑换信息
    private static final String BG_REDPACKET_EXCHANGE_TABLE = "bgredpacket_exchange";
    //bg红包信息
    private static final String BG_REDPACKET_REORD_TABLE = "bgredpacket_record";

    public TableCreatorImpl(CreateTableMapper createTableMapper) {
        this.createTableMapper = createTableMapper;
    }

    @Override
    public Boolean createStatisTable() {
        LocalDateTime today = LocalDateTime.now().withTime(0, 0, 0, 0);
        String todayStr = today.toString("yyyyMM");

        LocalDateTime tomorrow = today.plusDays(1);
        String tomorrowStr = tomorrow.toString("yyyyMM");

        LocalDateTime yesterday = today.minusDays(1);
        String yesterdayStr = yesterday.toString("yyyyMM");

        //判断月表是否存在
        //今天的时间月表
        Integer todayTimeTbExist = createTableMapper.queryTableByName(TIME_TABLE + todayStr);

        //今天的表的索引
        Integer todayTimeIndexExist = createTableMapper.queryTableByIndex(TIME_TABLE + todayStr);

        //今天的时间月表
        Integer tomorrowTimeTbExist = createTableMapper.queryTableByName(TIME_TABLE + tomorrowStr);

        //今天的表的索引
        Integer tomorrowTimeIndexExist = createTableMapper.queryTableByIndex(TIME_TABLE + todayStr);

        //今天的统计数据月表
        Integer todayAccountTbExist = createTableMapper.queryTableByName(ACCOUNT_TABLE + todayStr);

        Integer todayAccountIndexExist = createTableMapper.queryTableByIndex(ACCOUNT_TABLE + todayStr);

        //昨日的统计数据月表
        Integer yesterAccountTbExist = createTableMapper.queryTableByName(ACCOUNT_TABLE + yesterdayStr);

        Integer yesterAccountIndexExist = createTableMapper.queryTableByIndex(ACCOUNT_TABLE + yesterdayStr);

        //如果不存在就生成
        if (todayTimeTbExist == 0) {
            createTableMapper.createDateTable(TIME_TABLE + todayStr);
        }

        //判断索引是否存在
        if (todayTimeIndexExist == 0) {
            String sql = getDataIdxSql(TIME_TABLE + todayStr);
            createTableMapper.createDataIdx(sql);
        }

        //如果不存在就生成
        if (tomorrowTimeTbExist == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            createTableMapper.createDateTable(TIME_TABLE + tomorrowStr);
        }

        //判断索引是否存在
        if (tomorrowTimeIndexExist == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            String sql = getDataIdxSql(TIME_TABLE + tomorrowStr);
            createTableMapper.createDataIdx(sql);
        }

        //如果不存在就生成
        if (todayAccountTbExist == 0) {
            createTableMapper.createAccountTable(ACCOUNT_TABLE + todayStr);
        }

        if (todayAccountIndexExist == 0) {
            String sql = getAccountIdxSql(ACCOUNT_TABLE + todayStr);
            createTableMapper.createAccountIdx(sql);
        }

        //如果不存在就生成
        if (yesterAccountTbExist == 0 && !StrUtil.equalsIgnoreCase(todayStr, yesterdayStr)) {
            createTableMapper.createAccountTable(ACCOUNT_TABLE + yesterdayStr);
        }

        if (yesterAccountIndexExist == 0 && !StrUtil.equalsIgnoreCase(todayStr, yesterdayStr)) {
            String sql = getAccountIdxSql(ACCOUNT_TABLE + yesterdayStr);
            createTableMapper.createAccountIdx(sql);
        }
        return Boolean.TRUE;
    }

    @Override
    public Boolean createStatisGameTable() {
        LocalDateTime today = LocalDateTime.now().withTime(0, 0, 0, 0);
        String todayStr = today.toString("yyyyMM");

        LocalDateTime tomorrow = today.plusDays(1);
        String tomorrowStr = tomorrow.toString("yyyyMM");

        LocalDateTime yesterday = today.minusDays(1);
        String yesterdayStr = yesterday.toString("yyyyMM");
        //今天的游戏月表
        Integer todayGameTbExist = createTableMapper.queryTableByName(GAME_TABLE + todayStr);

        //今天的表的索引
        Integer todayGameIndexExist = createTableMapper.queryTableByIndex(GAME_TABLE + todayStr);

        //明天的游戏月表
        Integer tomorrowGameTbExist = createTableMapper.queryTableByName(GAME_TABLE + tomorrowStr);

        //明天的游戏表索引
        Integer tomorrowGameIndexExist = createTableMapper.queryTableByIndex(GAME_TABLE + tomorrowStr);

        //昨日的统计数据月表
        Integer yesterGameTbExist = createTableMapper.queryTableByName(GAME_TABLE + yesterdayStr);

        //昨天游戏统计表索引
        Integer yesterGameIndexExist = createTableMapper.queryTableByIndex(GAME_TABLE + yesterdayStr);


        //如果不存在就生成
        if (todayGameTbExist == 0) {
            createTableMapper.createGameDateTabel(GAME_TABLE + todayStr);
        }

        //判断索引是否存在
        if (todayGameIndexExist == 0) {
            String sql = getGameDataIdxSql(GAME_TABLE + todayStr);
            createTableMapper.createDataIdx(sql);
        }

        //如果不存在就生成
        if (tomorrowGameTbExist == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            createTableMapper.createGameDateTabel(GAME_TABLE + tomorrowStr);
        }

        //判断索引是否存在
        if (tomorrowGameIndexExist == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            String sql = getGameDataIdxSql(GAME_TABLE + tomorrowStr);
            createTableMapper.createDataIdx(sql);
        }

        //如果不存在就生成
        if (yesterGameTbExist == 0 && !StrUtil.equalsIgnoreCase(todayStr, yesterdayStr)) {
            createTableMapper.createGameDateTabel(GAME_TABLE + yesterdayStr);
        }

        if (yesterGameIndexExist == 0 && !StrUtil.equalsIgnoreCase(todayStr, yesterdayStr)) {
            String sql = getGameDataIdxSql(GAME_TABLE + yesterdayStr);
            createTableMapper.createDataIdx(sql);
        }
        return Boolean.TRUE;
    }

    @Override
    public Boolean createStatisChannelTable() {
        LocalDateTime today = LocalDateTime.now().withTime(0, 0, 0, 0);
        String todayStr = today.toString("yyyyMM");

        LocalDateTime tomorrow = today.plusDays(1);
        String tomorrowStr = tomorrow.toString("yyyyMM");

        LocalDateTime yesterday = today.minusDays(1);
        String yesterdayStr = yesterday.toString("yyyyMM");

        //判断月表是否存在
        //今天的时间月表
        Integer todayChannelTbExist = createTableMapper.queryTableByName(CHANNEL_TABLE + todayStr);

        //今天的表的索引
        Integer todayChannelIndexExist = createTableMapper.queryTableByIndex(CHANNEL_TABLE + todayStr);

        //明天的时间月表
        Integer tomorrowChannelTbExist = createTableMapper.queryTableByName(CHANNEL_TABLE + tomorrowStr);

        //今天的表的索引
        Integer tomorrowChannelIndexExist = createTableMapper.queryTableByIndex(CHANNEL_TABLE + tomorrowStr);

        //昨日的统计数据月表
        Integer yesterdayChannelTbExist = createTableMapper.queryTableByName(CHANNEL_TABLE + yesterdayStr);

        Integer yesterdayChannelIndexExist = createTableMapper.queryTableByIndex(CHANNEL_TABLE + yesterdayStr);

        //如果不存在就生成
        if (todayChannelTbExist == 0) {
            createTableMapper.createChannelDateTabel(CHANNEL_TABLE + todayStr);
        }

        //判断索引是否存在
        if (todayChannelIndexExist == 0) {
            String sql = getChannelDataIdxSql(CHANNEL_TABLE + todayStr);
            createTableMapper.createDataIdx(sql);
        }

        //如果不存在就生成
        if (tomorrowChannelTbExist == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            createTableMapper.createChannelDateTabel(CHANNEL_TABLE + tomorrowStr);
        }

        //判断索引是否存在
        if (tomorrowChannelIndexExist == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            String sql = getChannelDataIdxSql(CHANNEL_TABLE + tomorrowStr);
            createTableMapper.createDataIdx(sql);
        }

        //如果不存在就生成
        if (yesterdayChannelTbExist == 0 && !StrUtil.equalsIgnoreCase(todayStr, yesterdayStr)) {
            createTableMapper.createChannelDateTabel(CHANNEL_TABLE + yesterdayStr);
        }

        if (yesterdayChannelIndexExist == 0 && !StrUtil.equalsIgnoreCase(todayStr, yesterdayStr)) {
            String sql = getChannelDataIdxSql(CHANNEL_TABLE + yesterdayStr);
            createTableMapper.createDataIdx(sql);
        }
        return Boolean.TRUE;
    }

    @Override
    public Boolean createBgBetRecordTable() {
        LocalDateTime today = LocalDateTime.now().withTime(0, 0, 0, 0);
        String todayStr = today.toString("yyyyMM");

        LocalDateTime tomorrow = today.plusDays(1);
        String tomorrowStr = tomorrow.toString("yyyyMM");

        //判断月表是否存在
        //今天的时间月表
        Integer todayTimeTbExist = createTableMapper.queryTableByName(BG_BET_RECORD_TABLE + todayStr);

        //今天的表的索引
        Integer todayTimeIndexExist = createTableMapper.queryTableByIndex(BG_BET_RECORD_TABLE + todayStr);

        //明天的时间月表
        Integer tomorrowTimeTbExist = createTableMapper.queryTableByName(BG_BET_RECORD_TABLE + tomorrowStr);

        //明天的表的索引
        Integer tomorrowTimeIndexExist = createTableMapper.queryTableByIndex(BG_BET_RECORD_TABLE + tomorrowStr);

        //如果不存在就生成
        if (todayTimeTbExist == 0) {
            createTableMapper.createBgBetDateTabel(BG_BET_RECORD_TABLE + todayStr);
        }

        //判断索引是否存在
        if (todayTimeIndexExist == 0) {
            String sql = getBgBetDataIdxSql(BG_BET_RECORD_TABLE + todayStr);
            createTableMapper.createDataIdx(sql);
        }

        //如果不存在就生成
        if (tomorrowTimeTbExist == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            createTableMapper.createBgBetDateTabel(BG_BET_RECORD_TABLE + tomorrowStr);
        }

        //判断索引是否存在
        if (tomorrowTimeIndexExist == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            String sql = getBgBetDataIdxSql(BG_BET_RECORD_TABLE + tomorrowStr);
            createTableMapper.createDataIdx(sql);
        }

        return Boolean.TRUE;
    }

    @Override
    public Boolean createBgRedPacketRecordTable() {

        LocalDateTime today = LocalDateTime.now().withTime(0, 0, 0, 0);
        String todayStr = today.toString("yyyyMM");

        LocalDateTime tomorrow = today.plusDays(1);
        String tomorrowStr = tomorrow.toString("yyyyMM");

        //判断月表是否存在
        //今天的时间月表
        Integer todayTimeTbExist = createTableMapper.queryTableByName(BG_REDPACKET_REORD_TABLE + todayStr);

        //今天的表的索引
        Integer todayTimeIndexExist = createTableMapper.queryTableByIndex(BG_REDPACKET_REORD_TABLE + todayStr);

        //明天的时间月表
        Integer tomorrowTimeTbExist = createTableMapper.queryTableByName(BG_REDPACKET_REORD_TABLE + tomorrowStr);

        //今天的表的索引
        Integer tomorrowTimeIndexExist = createTableMapper.queryTableByIndex(BG_REDPACKET_REORD_TABLE + tomorrowStr);

        //如果不存在就生成
        if (todayTimeTbExist == 0) {
            createTableMapper.createBgRedPacketTabel(BG_REDPACKET_REORD_TABLE + todayStr);
        }

        //判断索引是否存在
        if (todayTimeIndexExist == 0) {
            String sql = getBgRedPacketDataIdxSql(BG_REDPACKET_REORD_TABLE + todayStr);
            createTableMapper.createDataIdx(sql);
        }

        //如果不存在就生成
        if (tomorrowTimeTbExist == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            createTableMapper.createBgRedPacketTabel(BG_REDPACKET_REORD_TABLE + tomorrowStr);
        }

        //判断索引是否存在
        if (tomorrowTimeIndexExist == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            String sql = getBgRedPacketDataIdxSql(BG_REDPACKET_REORD_TABLE + tomorrowStr);
            createTableMapper.createDataIdx(sql);
        }

        return Boolean.TRUE;
    }

    @Override
    public Boolean createBgRedPacketExchangeRecordTable() {
        LocalDateTime today = LocalDateTime.now().withTime(0, 0, 0, 0);
        String todayStr = today.toString("yyyyMM");

        LocalDateTime tomorrow = today.plusDays(1);
        String tomorrowStr = tomorrow.toString("yyyyMM");

        //判断月表是否存在
        //今天的时间月表
        Integer todayTimeTbExist = createTableMapper.queryTableByName(BG_REDPACKET_EXCHANGE_TABLE + todayStr);

        //今天的表的索引
        Integer todayTimeIndexExist = createTableMapper.queryTableByIndex(BG_REDPACKET_EXCHANGE_TABLE + todayStr);

        //明天的时间月表
        Integer tomorrowTimeTbExist = createTableMapper.queryTableByName(BG_REDPACKET_EXCHANGE_TABLE + tomorrowStr);

        //明天的表的索引
        Integer tomorrowTimeIndexExist = createTableMapper.queryTableByIndex(BG_REDPACKET_EXCHANGE_TABLE + tomorrowStr);

        //如果不存在就生成
        if (todayTimeTbExist == 0) {
            createTableMapper.createBgRedPacketExchangeTabel(BG_REDPACKET_EXCHANGE_TABLE + todayStr);
        }

        //判断索引是否存在
        if (todayTimeIndexExist == 0) {
            String sql = getBgRedPacketExchangeDataIdxSql(BG_REDPACKET_EXCHANGE_TABLE + todayStr);
            createTableMapper.createDataIdx(sql);
        }

        //如果不存在就生成
        if (tomorrowTimeTbExist == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            createTableMapper.createBgRedPacketExchangeTabel(BG_REDPACKET_EXCHANGE_TABLE + tomorrowStr);
        }

        //判断索引是否存在
        if (tomorrowTimeIndexExist == 0 && !StrUtil.equalsIgnoreCase(todayStr, tomorrowStr)) {
            String sql = getBgRedPacketExchangeDataIdxSql(BG_REDPACKET_EXCHANGE_TABLE + tomorrowStr);
            createTableMapper.createDataIdx(sql);
        }

        return Boolean.TRUE;
    }

    private String getDataIdxSql(String tableName) {
        StringBuffer sb = new StringBuffer();
        sb.append("ALTER TABLE `" + tableName + "` ");
        sb.append("ADD INDEX `idx_" + tableName + "_userId`(`user_id`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_channelId`(`channel_id`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_statisTime`(`statis_time`) USING BTREE; ");
        return sb.toString();
    }


    private String getChannelDataIdxSql(String tableName) {
        StringBuffer sb = new StringBuffer();
        sb.append("ALTER TABLE `" + tableName + "` ");
        sb.append("ADD INDEX `idx_" + tableName + "_channelId`(`channel_id`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_statisTime`(`statis_time`) USING BTREE; ");
        return sb.toString();
    }

    private String getGameDataIdxSql(String tableName) {
        StringBuffer sb = new StringBuffer();
        sb.append("ALTER TABLE `" + tableName + "` ");
        sb.append("ADD INDEX `idx_" + tableName + "_userId`(`user_id`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_channelId`(`channel_id`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_statisTime`(`statis_time`) USING BTREE; ");
        return sb.toString();
    }

    /**
     * suffix
     * <B>生成索引sql</B>
     * @return
     */
    private String getAccountIdxSql(String tableName) {
        StringBuffer sb = new StringBuffer();
        sb.append("ALTER TABLE `"+ tableName+ "` ");
        sb.append("ADD INDEX `idx_" + tableName + "_userId`(`user_id`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_channelId`(`channel_id`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_statisTime`(`statis_time`) USING BTREE; ");
        return sb.toString();
    }

    private String getBgBetDataIdxSql(String tableName) {
        StringBuffer sb = new StringBuffer();
        sb.append("ALTER TABLE `"+ tableName+ "` ");
        sb.append("ADD UNIQUE INDEX `idx_" + tableName + "_orderId`(`order_id`, `login_id`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_loginId`(`login_id`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_gameId`(`game_id`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_uId`(`uid`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_gameName`(`game_name`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_issueId`(`issue_id`) USING BTREE; ");
        return sb.toString();
    }

    private String getBgRedPacketDataIdxSql(String tableName) {
        StringBuffer sb = new StringBuffer();
        sb.append("ALTER TABLE `"+ tableName+ "` ");
        sb.append("ADD INDEX `idx_" + tableName + "_uid`(`uid`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_time`(`time`) USING BTREE, ");
        sb.append("ADD UNIQUE INDEX `idx_" + tableName + "_pid`(`pid`) USING BTREE; ");
        return sb.toString();
    }

    private String getBgRedPacketExchangeDataIdxSql(String tableName) {
        StringBuffer sb = new StringBuffer();
        sb.append("ALTER TABLE `"+ tableName+ "` ");
        sb.append("ADD INDEX `idx_" + tableName + "_uid`(`uid`) USING BTREE, ");
        sb.append("ADD INDEX `idx_" + tableName + "_time`(`time`) USING BTREE, ");
        sb.append("ADD UNIQUE INDEX `idx_" + tableName + "_pid`(`pid`) USING BTREE; ");
        return sb.toString();
    }
}
