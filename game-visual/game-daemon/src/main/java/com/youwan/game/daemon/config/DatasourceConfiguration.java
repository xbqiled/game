package com.youwan.game.daemon.config;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import com.youwan.game.common.data.datasource.DynamicDataSource;
import org.apache.shardingsphere.shardingjdbc.api.yaml.YamlShardingDataSourceFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.ResourceUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static com.youwan.game.daemon.config.DS.*;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-07-15.
 */
@Configuration
public class DatasourceConfiguration implements ApplicationContextAware {
    private ApplicationContext context;

    public String getActiveProfile() {
        return context.getEnvironment().getActiveProfiles()[0];
    }

    /*@Bean(name = RECORD_LOG)
    public DataSource recordlogDS() throws IOException, SQLException {
        return YamlShardingDataSourceFactory
                .createDataSource(FileCopyUtils.copyToByteArray(ResourceUtils.getURL("classpath:sharding/record_log.yml").openStream()));
    }*/

    @Bean(name = XBQP)
    public DataSource xbqpDS() throws IOException, SQLException {
        return YamlShardingDataSourceFactory
                .createDataSource(FileCopyUtils.copyToByteArray(ResourceUtils.getURL("classpath:sharding/xbqp_data-" + getActiveProfile() + ".yml").openStream()));
    }

    @Bean(name = PLATFORM)
    @ConfigurationProperties(prefix = "spring.datasource.platform-ds")
    public DataSource platformDS() {
        return DruidDataSourceBuilder.create().build();
    }

    @Bean
    @DependsOn({PLATFORM, XBQP})
    @Primary
    public DataSource dynamicDataSource(@Qualifier(PLATFORM) DataSource platform,
                                        @Qualifier(XBQP) DataSource xbqp/*,
                                        @Qualifier(RECORD_LOG) DataSource recordlog*/) {
        DynamicDataSource dynamicDataSource = new DynamicDataSource();
        Map<Object, Object> targetDataSources = new HashMap<>();
        targetDataSources.put(PLATFORM, platform);
        targetDataSources.put(XBQP, xbqp);
        //targetDataSources.put(RECORD_LOG, recordlog);
        dynamicDataSource.setTargetDataSources(targetDataSources);
        dynamicDataSource.setDefaultTargetDataSource(platform);
        return dynamicDataSource;
    }

    @Bean
    public DataSourceSwitcher dataSourceSwitcher() {
        return new DataSourceSwitcher();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}
