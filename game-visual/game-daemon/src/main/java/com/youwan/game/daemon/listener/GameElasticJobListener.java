package com.youwan.game.daemon.listener;

import com.dangdang.ddframe.job.executor.ShardingContexts;
import com.dangdang.ddframe.job.lite.api.listener.ElasticJobListener;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lion
 * @date 2018/7/24
 * 任务监听器
 */
@Slf4j
public class GameElasticJobListener implements ElasticJobListener {

    @Override
    public void beforeJobExecuted(ShardingContexts shardingContexts) {
        //System.out.println(shardingContexts.getJobName() + " | MyElasticJobListener beforeJobExecuted");
        log.info("Job [{}] started ......", shardingContexts.getJobName());
    }

    @Override
    public void afterJobExecuted(ShardingContexts shardingContexts) {
        //System.out.println(shardingContexts.getJobName() + " | MyElasticJobListener afterJobExecuted");
		log.info("Job [{}] done ......", shardingContexts.getJobName());
    }
}
