package com.youwan.game.daemon.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.youwan.game.daemon.service.tpscrub.LotteryRecordScrubber;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-20.
 */
@Slf4j
public class WashCodeJob implements SimpleJob {

    private LotteryRecordScrubber lotteryScrubber;

    public WashCodeJob(LotteryRecordScrubber lotteryScrubber) {
        this.lotteryScrubber = lotteryScrubber;
    }

    @SneakyThrows
    @Override
    public void execute(ShardingContext shardingContext) {
        int shard = shardingContext.getShardingItem();

        String epoch = lotteryScrubber.initialMark(shard);
        Thread.sleep(3000);
        lotteryScrubber.archive(lotteryScrubber.scrub(epoch, shard));
        lotteryScrubber.finishMark(epoch, shard);
    }
}
