package com.youwan.game.daemon.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.youwan.game.daemon.service.TableCreator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lion
 * @date 2018/2/8
 */
@Slf4j
@AllArgsConstructor
public class CreateGameTableJob implements SimpleJob {

	//private final ExecutionLogService executionLogService;
	private TableCreator tablesCreator;
	@Override
	public void execute(ShardingContext shardingContext) {
		System.out.println("======================SynGameTableJob========================>");
		//executionLogService.createStatisGameTable();
		tablesCreator.createStatisGameTable();
	}
}
