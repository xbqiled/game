package com.youwan.game.daemon.service.tpscrub;

import com.youwan.game.common.core.betting.ScrubResult;
import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-06.
 */
@Component
@Slf4j
public class TPSportRecordScrubber extends AbstractBettingRecordScrubber{

    public TPSportRecordScrubber(ElasticsearchTemplate elasticsearchTemplate, ScrubConfigService scrubConfigService) {
        super(elasticsearchTemplate, scrubConfigService);
    }

    @Override
    String getTargetIndex() {
        return "3rd_sport_betting_record";
    }

    @Override
    String getSumAggField() {
        return "realBettingMoney";
    }

    @Override
    int getScrubType() {
        return ScrubResult.TYPE_SPORT;
    }
}
