package com.youwan.game.daemon.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.youwan.game.daemon.service.TableCreator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lion
 * @date 2018/2/7
 * 测试Job
 */
@Slf4j
@AllArgsConstructor
public class CreateChannelTableJob implements SimpleJob {

	//private final ExecutionLogService executionLogService;
	private TableCreator tablesCreator;
	/**
	 * 业务执行逻辑
	 *
	 * @param shardingContext 分片信息
	 */
	@Override
	public void execute(ShardingContext shardingContext) {
		log.info("创建渠道统计表任务执行啦:{}", shardingContext);
		//executionLogService.createStatisChannelTable();
		tablesCreator.createStatisChannelTable();
	}
}
