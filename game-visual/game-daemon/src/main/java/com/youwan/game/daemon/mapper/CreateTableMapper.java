package com.youwan.game.daemon.mapper;

import org.apache.ibatis.annotations.Param;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-07-16.
 */
public interface CreateTableMapper {

    /**
     * <B>查询库里是否存在对应的表</B>
     * @param tableName
     * @return
     */
    Integer queryTableByName(@Param("tableName")String tableName);

    /**
     *
     * @param tableName
     * @return
     */
    Integer queryTableByIndex(@Param("tableName")String tableName);

    /**
     * <B>创建索引</B>
     * @param sql
     * @return
     */
    Integer createDataIdx(@Param("sql")String sql);

    /**
     * <B>创建索引</B>
     * @param sql
     * @return
     */
    Integer createAccountIdx(@Param("sql") String sql);

    /**
     * <B>删除数据信息</B>
     * @param tableName
     * @param statisTime
     * @return
     */
    Integer delDate(@Param("tableName")String tableName, @Param("statisTime")String statisTime);
    /**
     * <B>创建新的表信息</B>
     * @param tableName
     * @return
     */
    Integer createDateTable(@Param("tableName")String tableName);


    Integer createChannelDateTabel(@Param("tableName")String tableName);


    Integer createGameDateTabel(@Param("tableName")String tableName);
    /**
     * <B>创建历史表信息</B>
     * @return
     */
    Integer createAccountTable(@Param("tableName")String tableName);

    /**
     * <B>创建新的表信息</B>
     * @param tableName
     * @return
     */
    Integer createBgBetDateTabel(@Param("tableName")String tableName);


    /**
     * <B>创建红包纪录表</B>
     * @param tableName
     * @return
     */
    Integer createBgRedPacketTabel(@Param("tableName")String tableName);


    /**
     * <B>创建红包兑换纪录纪录表</B>
     * @param tableName
     * @return
     */
    Integer createBgRedPacketExchangeTabel(@Param("tableName")String tableName);
}
