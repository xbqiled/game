package com.youwan.game.daemon.mapper.recordlog;


import com.alibaba.fastjson.JSON;
import com.youwan.game.daemon.DaemonApplication;
import com.youwan.game.daemon.entity.GameScoreAgg;
import com.youwan.game.daemon.service.StatsDataProcessor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DaemonApplication.class})
public class DBTest {
    @Autowired
    private StatsDataProcessor statsDataProcessor;

    @Test
    public void test() {
        statsDataProcessor.processHisData();
    }

    @Test
    public void test2() {
        statsDataProcessor.processAccountData();
    }

    @Test
    public void test3() {
        statsDataProcessor.processGameData();
    }

    @Test
    public void test4() {
        statsDataProcessor.processChannelData();
    }
}