package com.youwan.game.report;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.report.api.dto.*;
import com.youwan.game.report.api.entity.Userlogoutrecord;
import com.youwan.game.report.service.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-07-09.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ReportApplication.class})
public class DBTest {

    @Autowired
    private DantiaomultiplyService dantiaomultiplyService;

    @Test
    public void test() {
        Page page = new Page(1, 10);
        DantiaomultiplyDto dto = new DantiaomultiplyDto();
        dto.setStartTime("2020-04-01");
        dto.setEndTime("2020-06-10");
        IPage<Map<String, Object>> ret = dantiaomultiplyService.queryMultiplyPageList(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private DantiaosingleService dantiaosingleService;

    @Test
    public void test2() {
        Page page = new Page(1, 10);
        DantiaosingleDto dto = new DantiaosingleDto();
        dto.setStartTime("2020-04-01");
        dto.setEndTime("2020-06-10");
        IPage<Map<String, Object>> ret = dantiaosingleService.querySinglePageList(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private LogingamerecordService logingamerecordService;

    @Test
    public void test3() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();
        dto.setStartTime("2020-04-01");
        dto.setEndTime("2020-06-10");
        IPage<Map<String, Object>> ret = logingamerecordService.queryPageList(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private LogoutgamerecordService logoutgamerecordService;

    @Test
    public void test4() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();
        dto.setStartTime("2020-04-01");
        dto.setEndTime("2020-06-10");
        IPage<Map<String, Object>> ret = logoutgamerecordService.queryPageList(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private PlayerdepositService playerdepositService;

    @Test
    public void test5() {
        Page page = new Page(1, 10);
        ParameterDto dto = new ParameterDto();
        dto.setStartTime("2020-04-01");
        dto.setEndTime("2020-06-10");
        IPage<Map<String, Object>> ret = playerdepositService.queryDepositPageList(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private PlayersigninService playersigninService;

    @Test
    public void test6() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();
        dto.setStartTime("2020-04-01");
        dto.setEndTime("2020-06-10");
        IPage<Map<String, Object>> ret = playersigninService.querySignInPageList(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private RechargeplatformorderService rechargeplatformorderService;

    @Test
    public void test7() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();
        dto.setStartTime("2020-04-01");
        dto.setEndTime("2020-06-10");
        IPage ret = rechargeplatformorderService.queryPageList(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private GamerebateService gamerebateService;

    @Test
    public void test8() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();
        dto.setStartTime("2020-04-01");
        dto.setEndTime("2020-06-10");
        IPage ret = gamerebateService.queryGamerebatePage(page, "1026258", null);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private RedblackpoolService redblackpoolService;

    @Test
    public void test9() {
        Page page = new Page(1, 10);
        ParameterDto dto = new ParameterDto();
        dto.setQueryTime("2020-05-26");
        IPage ret = redblackpoolService.queryHhdzPoolPageList(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private ScoreRecordService scoreRecordService;

    @Test
    public void test10() {
        Page page = new Page(1, 10);
        ScoreDto dto = new ScoreDto();
        dto.setStartTime("2020-04-01");
        dto.setEndTime("2020-06-10");
        IPage ret = scoreRecordService.queryPageList(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private TreasurechangeService treasurechangeService;

    @Test
    public void test11() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();
        dto.setStartTime("2020-04-01");
        dto.setEndTime("2020-06-10");
        IPage ret = treasurechangeService.queryUserAccountPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private UserloginrecordService userloginrecordService;

    @Test
    public void test12() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();
        //dto.setStartTime("2020-04-01");
        //dto.setEndTime("2020-06-10");
        dto.setLogintime("1585702641000");
        IPage ret = userloginrecordService.queryPageList(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test13() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();
        dto.setStartTime("2020-04-01");
        dto.setEndTime("2020-06-10");
        //dto.setLogintime("1585702641000");
        IPage ret = userloginrecordService.queryLoginReport(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private UserlogoutrecordService userlogoutrecordService;

    @Test
    public void test14() {
        Page page = new Page(1, 10);
        Userlogoutrecord dto = new Userlogoutrecord();
        //dto.setStartTime("2020-04-01");
        //dto.setEndTime("2020-06-10");
        dto.setLogouttime("1585672933000");
        IPage ret = userlogoutrecordService.queryPageList(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test15() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();
        dto.setStartTime("2020-04-01");
        dto.setEndTime("2020-06-10");
        IPage ret = userlogoutrecordService.queryLogoutReport(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private ReportService reportService;

    @Test
    public void test16() {

        Map<String, Object> ret = reportService.queryBcbmBoardDetail("1590109824114949", "1026199", "2020-5-22 09:10:39");
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test17() {
        Page page = new Page(1, 10);
        BoardDto dto = new BoardDto();
        dto.setQueryTime("2020-5-22");
        IPage ret = reportService.queryBcbmBoardPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test18() {
        Map<String, Object> ret = reportService.queryFqzsBoardDetail("1585705581111376", "1025742", "2020-04-01 09:46:36");
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test19() {
        Page page = new Page(1, 10);
        BoardDto dto = new BoardDto();
        dto.setQueryTime("2020-04-01");
        IPage ret = reportService.queryFqzsBoardPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test20() {
        Map<String, Object> ret = reportService.queryQznnBoardDetail("159011004510000", "16249", "2020-05-22 09:14:37");
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test21() {
        Page page = new Page(1, 10);
        BoardDto dto = new BoardDto();
        dto.setQueryTime("2020-05-22");
        IPage ret = reportService.queryQznnBoardPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test22() {
        Page page = new Page(1, 10);
        BoardDto dto = new BoardDto();
        dto.setQueryTime("2020-05-22");
        IPage ret = reportService.queryHbjlBoardPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test23() {
        Page page = new Page(1, 10);
        BoardDto dto = new BoardDto();
        dto.setQueryTime("2020-05-17");
        IPage ret = reportService.queryHhdzBoardPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test24() {
        Page page = new Page(1, 10);
        BoardDto dto = new BoardDto();
        dto.setQueryTime("2020-05-18");
        IPage ret = reportService.queryCsdBoardPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test25() {
        Page page = new Page(1, 10);
        BoardDto dto = new BoardDto();
        dto.setQueryTime("2020-05-18");
        List<Map<String, Object>> ret = reportService.queryCsdboardDetail("15897811541810011026199");
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test26() {
        Page page = new Page(1, 10);
        BoardDto dto = new BoardDto();
        dto.setQueryTime("2020-05-26");
        IPage ret = reportService.queryZjhBoardPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test27() {
        Page page = new Page(1, 10);
        BoardDto dto = new BoardDto();
        dto.setQueryTime("2020-05-09");
        IPage ret = reportService.queryBhsBoardPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test28() {
        Page page = new Page(1, 10);
        BoardDto dto = new BoardDto();
        dto.setQueryTime("2020-05-26");
        IPage ret = reportService.queryTwentyOneBoardPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test29() {
        Page page = new Page(1, 10);
        BoardDto dto = new BoardDto();
        dto.setQueryTime("2020-05-22");
        IPage ret = reportService.queryDdzBoardPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test30() {
        Page page = new Page(1, 10);
        BoardDto dto = new BoardDto();
        dto.setQueryTime("2020-05-22");
        IPage ret = reportService.querySgjBoardPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test31() {
        Page page = new Page(1, 10);
        BoardDto dto = new BoardDto();
        dto.setQueryTime("2020-05-27");
        IPage ret = reportService.queryBjlBoardPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test32() {
        Page page = new Page(1, 10);
        BoardDto dto = new BoardDto();
        dto.setQueryTime("2020-05-22");
        IPage ret = reportService.queryLhBoardPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test33() {
        Page page = new Page(1, 10);
        BoardDto dto = new BoardDto();
        dto.setQueryTime("2020-05-22");
        IPage ret = reportService.queryByBoardPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test34() {
        Page page = new Page(1, 10);
        BoardDto dto = new BoardDto();
        dto.setQueryTime("2020-05-26");
        IPage ret = reportService.queryNnBoardPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test35() {
        Page page = new Page(1, 10);
        BoardDto dto = new BoardDto();
        //dto.setQueryTime("2020-05-26");
        IPage ret = reportService.queryWealthRankingPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }
}
