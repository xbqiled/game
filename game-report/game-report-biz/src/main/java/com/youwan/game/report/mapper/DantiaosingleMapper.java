package com.youwan.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.report.api.entity.Dantiaosingle;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2020-04-18 12:19:42
 */
public interface DantiaosingleMapper extends BaseMapper<Dantiaosingle> {

    IPage<Map<String, Object>> querySinglePageList(Page page, @Param("recordId") String recordId,
                                                         @Param("userId") String userId,
                                                         @Param("startTime") Integer startTime,
                                                         @Param("endTime") Integer endTime);


}
