package com.youwan.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.report.api.entity.Platformgamecount;

/**
 * 实时游戏在线记录
 *
 * @author code generator
 * @date 2019-01-25 19:13:50
 */
public interface PlatformgamecountMapper extends BaseMapper<Platformgamecount> {

}
