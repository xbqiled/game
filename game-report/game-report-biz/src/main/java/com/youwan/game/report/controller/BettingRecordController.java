package com.youwan.game.report.controller;

import com.youwan.game.common.core.util.R;
import com.youwan.game.report.api.dto.betting.*;
import com.youwan.game.report.service.BettingRecordService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-01.
 */
@RestController
@AllArgsConstructor
@RequestMapping("/3rd/betting_records")
public class BettingRecordController {

    private BettingRecordService bettingRecordService;

    @GetMapping("/lotteries")
    public R queryLotteryRecord(LotteryQueryDto queryDto) {
        return new R(bettingRecordService.queryLotteryOrders(queryDto));
    }

    @GetMapping("/hgsport")
    public R queryCrownSportBetting(CrownSportQueryDto queryDto) {
        return new R(bettingRecordService.queryCrownSportOrders(queryDto));
    }

    @GetMapping("/egame")
    public R queryEGameBetting(EGameQueryDto queryDto) {
        return new R(bettingRecordService.queryEGameBettingRecords(queryDto));
    }

    @GetMapping("/live")
    public R queryLiveBetting(LiveQueryDto queryDto) {
        return new R(bettingRecordService.queryLiveBettingRecords(queryDto));
    }

    @GetMapping("/3rdsport")
    public R query3rdSportBetting(TPSportQueryDto queryDto) {
        return new R(bettingRecordService.query3rdSportBettingRecords(queryDto));
    }
}
