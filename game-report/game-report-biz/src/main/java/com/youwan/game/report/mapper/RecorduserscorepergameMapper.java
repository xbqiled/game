package com.youwan.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.report.api.entity.Recorduserscorepergame;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-29 13:42:42
 */
public interface RecorduserscorepergameMapper extends BaseMapper<Recorduserscorepergame> {

}
