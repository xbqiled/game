package com.youwan.game.report.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.report.api.dto.QueryDto;
import com.youwan.game.report.api.entity.Logingamerecord;
import com.youwan.game.report.service.LogingamerecordService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-01-25 17:23:37
 */
@RestController
@AllArgsConstructor
@RequestMapping("/logingameRecord")
public class LogingamerecordController {

  private final LogingamerecordService logingamerecordService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param queryDto
   * @return
   */
  @GetMapping("/page")
  public R getLogingamerecordPage(Page page, QueryDto queryDto) {
    return  new R<>(logingamerecordService.queryPageList(page, queryDto));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(logingamerecordService.getById(id));
  }

  /**
   * 新增
   * @param logingamerecord
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody Logingamerecord logingamerecord){
    return new R<>(logingamerecordService.save(logingamerecord));
  }

  /**
   * 修改
   * @param logingamerecord
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody Logingamerecord logingamerecord){
    return new R<>(logingamerecordService.updateById(logingamerecord));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(logingamerecordService.removeById(id));
  }

}
