package com.youwan.game.report.service.impl;

import com.youwan.game.common.core.betting.entity.ThirdPartyAccountChangeRecord;
import com.youwan.game.common.data.core.Page;
import com.youwan.game.common.data.core.XSort;
import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import com.youwan.game.common.data.elasticsearch.operations.SearchOperation;
import com.youwan.game.report.api.dto.TPAccountChangeQueryDto;
import com.youwan.game.report.mapper.platform.MobileBindMapper;
import com.youwan.game.report.mapper.xbqp.AccountInfoMapper;
import com.youwan.game.report.service.ThirdPartyAccountChangeService;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-09.
 */
@Service
public class ThirdPartyAccountChangeServiceImpl implements ThirdPartyAccountChangeService {

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    private MobileBindMapper mobileBindMapper;

    @Autowired
    private AccountInfoMapper accountInfoMapper;

    @Override
    public Page<ThirdPartyAccountChangeRecord> query3rdAccountChangeRecords(TPAccountChangeQueryDto queryDto) {
        BoolQueryBuilder query = QueryBuilders.boolQuery();
        Map<String, Object> tpInfo = null;
        if (queryDto.getStartTime() != null) {
            RangeQueryBuilder rangeQuery = rangeQuery("bizDatetime").gte(queryDto.getStartTime());

            if (queryDto.getEndTime() != null) {
                rangeQuery.lte(queryDto.getEndTime());
            }
            query.must(rangeQuery);
        }
        if (queryDto.getType() != null) {
            query.must(termQuery("type", queryDto.getType()));
        }
        if (StringUtils.isNotBlank(queryDto.getOrderId())) {
            query.must(termQuery("orderId.keyword", queryDto.getOrderId()));
        }
        /*if (StringUtils.isNotBlank(queryDto.getLotCode())) {
            query.must(termQuery("lotCode.keyword", queryDto.getLotCode()));
        }
        if (StringUtils.isNotBlank(queryDto.getQiHao())) {
            query.must(termQuery("qiHao.keyword", queryDto.getQiHao()));
        }
        if (queryDto.getStatus() != null) {
            query.must(termQuery("status", queryDto.getStatus()));
        }
        if (StringUtils.isNotBlank(queryDto.getOrderId())) {
            query.must(termQuery("_id", queryDto.getOrderId()));
        }
        if (StringUtils.isNotBlank(queryDto.getBetIp())) {
            query.must(matchQuery("betIp", queryDto.getBetIp()));
        }*/
        if (StringUtils.isNotBlank(queryDto.getChannelId())) {
            Integer cpStationId = mobileBindMapper.getCpStationIdByChannel(queryDto.getChannelId());
            if (cpStationId == null)
                cpStationId = 0;
            query.must(termQuery("stationId", cpStationId));
        }
        if (queryDto.getUserId() != null) {
            tpInfo = accountInfoMapper.get3rdAccountId(queryDto.getUserId());
            Integer tpId = tpInfo == null ? 0 : (Integer) tpInfo.get("tpId");
            query.must(termQuery("accountId", tpId));
        }
        Page<ThirdPartyAccountChangeRecord> ret = elasticsearchTemplate.searchForPage(
                new SearchOperation<>("3rd_account_change_record", query, ThirdPartyAccountChangeRecord.class)
                        .setFrom(queryDto.getOffset())
                        .setSize(queryDto.getLimit())
                        .setSort(XSort.desc("bizDatetime"))
        );
        if (ret.getRecords().isEmpty())
            return ret;
        Set<Integer> mermberIds =
                ret.getRecords().stream().map(ThirdPartyAccountChangeRecord::getAccountId).distinct().mapToInt(Long::intValue).boxed().collect(Collectors.toSet());
        Set<Integer> stationIds =
                ret.getRecords().stream().map(ThirdPartyAccountChangeRecord::getStationId).distinct().mapToInt(Long::intValue).boxed().collect(Collectors.toSet());
        Map<Integer, Map<String, Object>> umap = new HashMap<>();
        if (tpInfo == null) {
            List<Map<String, Object>> users = accountInfoMapper.queryUserBy3rdIds(mermberIds);
            users.forEach(user -> umap.put((Integer) user.get("tpId"), user));
        } else {
            umap.put((Integer) tpInfo.get("tpId"), tpInfo);
        }

        Map<Integer, Map<String, Object>> bmap = new HashMap<>();
        if (StringUtils.isBlank(queryDto.getChannelId())) {
            List<Map<String, Object>> binds = mobileBindMapper.queryChannelIdsBy3rdIds(stationIds);
            binds.forEach(bind -> bmap.put((Integer) bind.get("cp_station_id"), bind));
        }
        ret.getRecords().forEach(order -> {
            Long tpId = order.getAccountId();
            Long stationId = order.getStationId();
            Map<String, Object> user = umap.get(tpId.intValue());
            if (user != null) {
                order.setUserName((String) user.get("nickName"));
                order.setUserId((Long) user.get("userId"));
            }
            if (StringUtils.isNotBlank(queryDto.getChannelId())) {
                order.setChannelId(queryDto.getChannelId());
            } else {
                Map<String, Object> bind = bmap.get(stationId.intValue());
                if (bind != null) {
                    order.setChannelId((String) bind.get("channel_id"));
                }
            }
        });
        return ret;
    }

}
