package com.youwan.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.report.api.dto.QueryDto;
import com.youwan.game.report.api.entity.Userlogoutrecord;

import java.util.Map;


/**
 * 玩家登出平台记录表
 *
 * @author code generator
 * @date 2019-01-25 11:48:53
 */
public interface UserlogoutrecordService extends IService<Userlogoutrecord> {

    IPage queryPageList(Page page, Userlogoutrecord recorduserplatformlogout);

    IPage queryLogoutReport(Page page, QueryDto queryDto);
}
