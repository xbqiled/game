package com.youwan.game.report.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.common.core.util.R;
import com.youwan.game.report.api.dto.BoardDto;
import com.youwan.game.report.service.ReportService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * VIEW
 *
 * @author code generator
 * @date 2019-02-05 14:02:39
 */
@RestController
@AllArgsConstructor
@RequestMapping("/statisreport")
public class ReportController {

    private final ReportService reportService;

    /**
     * <B>查询每日的报表信息</B>
     *
     * @param page 分页对象
     * @param boardDto
     * @return
     */
    @GetMapping("/day/page")
    public R getDayReportPage(Page page,BoardDto boardDto) {
        return new R<>(reportService.getDayReportPage(page, boardDto));
    }


    /**
     * <B>查询每周报表信息</B>
     *
     * @param page
     * @param boardDto
     * @return
     */
    @GetMapping("/week/page")
    public R getWeekReportPage(Page page, BoardDto boardDto) {
        return new R<>(reportService.getWeekReportPage(page, boardDto));
    }


    /**
     * <B>查询每月的报表信息</B>
     *
     * @param page 分页对象
     * @return
     */
    @GetMapping("/month/page")
    public R getMonthReportPage(Page page,  BoardDto boardDto) {
        return new R<>(reportService.getMonthReportPage(page, boardDto));
    }


    /**
     * <B> 查询财富排行榜</B>
     */
    @GetMapping("/wealth/page")
    public R getWealthRankingPage(Page page, BoardDto boardDto) {
        return new R<>(reportService.queryWealthRankingPage(page, boardDto));
    }


    /**
     * <B> 查询牛牛牌局</B>
     */
    @GetMapping("/nnboard/page")
    public R getNnBoardPage(Page page, BoardDto boardDto) {
        return new R<>(reportService.queryNnBoardPage(page, boardDto));
    }


    /**
     * <B> 查询龙虎斗牌局</B>
     */
    @GetMapping("/lhboard/page")
    public R getLhBoardPage(Page page, BoardDto boardDto) {
        return new R<>(reportService.queryLhBoardPage(page, boardDto));
    }


    /**
     * <B> 查询斗地主牌局</B>
     */
    @GetMapping("/ddzboard/page")
    public R getDdzBoardPage(Page page, BoardDto boardDto) {
        return new R<>(reportService.queryDdzBoardPage(page, boardDto));
    }


    /**
     * <B> 查询捕鱼牌局</B>
     */
    @GetMapping("/byboard/page")
    public R getByBoardPage(Page page, BoardDto boardDto) {
        IPage<Map<String, Object>> pageList = reportService.queryByBoardPage(page, boardDto);
        return new R<>(pageList);
    }


    /**
     * <B>查询百家乐牌局</B>
     */
    @GetMapping("/bjlboard/page")
    public R getBjlBoardPage(Page page, BoardDto boardDto) {
        IPage<Map<String, Object>> pageList = reportService.queryBjlBoardPage(page, boardDto);
        return new R<>(pageList);
    }

    /**
     * <B>查询水果机牌局</B>
     */
    @GetMapping("/sgjboard/page")
    public R getJxlwBoardPage(Page page, BoardDto boardDto) {
        IPage<Map<String, Object>> pageList = reportService.querySgjBoardPage(page, boardDto);
        return new R<>(pageList);
    }

    /**
     * <B>查询21点牌局</B>
     */
    @GetMapping("/twentyOneboard/page")
    public R getTwentyOneBoardPage(Page page, BoardDto boardDto) {
        IPage<Map<String, Object>> pageList = reportService.queryTwentyOneBoardPage(page, boardDto);
        return new R<>(pageList);
    }

    /**
     * <B>查询跑得快牌局</B>
     */
    @GetMapping("/pdkboard/page")
    public R getPdkBoardPage(Page page, BoardDto boardDto) {
        IPage<Map<String, Object>> pageList = reportService.queryPdkBoardPage(page, boardDto);
        return new R<>(pageList);
    }


    /**
     * <B>查询炸金花牌局</B>
     */
    @GetMapping("/zjhboard/page")
    public R getZjhBoardPage(Page page, BoardDto boardDto) {
        IPage<Map<String, Object>> pageList = reportService.queryZjhBoardPage(page, boardDto);
        return new R<>(pageList);
    }


    /**
     * <B>查询比花色牌局</B>
     */
    @GetMapping("/bhsboard/page")
    public R getBhsBoardPage(Page page, BoardDto boardDto) {
        IPage<Map<String, Object>> pageList = reportService.queryBhsBoardPage(page, boardDto);
        return new R<>(pageList);
    }

    /**
     * <B>查询财神到牌局</B>
     */
    @GetMapping("/csdboard/page")
    public R getCsdBoardPage(Page page, BoardDto boardDto) {
        IPage<Map<String, Object>> pageList = reportService.queryCsdBoardPage(page, boardDto);
        return new R<>(pageList);
    }

    /**
     * <B>查詢財神到詳情</B>
     * @param id
     * @return
     */
    @GetMapping("/csdboard/boardDetail/{id}")
    public R getCsdBoardDetail(@PathVariable String id) {
        List<Map<String, Object>> detailList = reportService.queryCsdboardDetail(id);
        return new R<>(detailList);
    }

    /**
     * <B>查詢紅黑大戰牌局</B>
     * @param page
     * @param boardDto
     * @return
     */
    @GetMapping("/hhdzboard/page")
    public R getHhdzBoardPage(Page page, BoardDto boardDto) {
        IPage<Map<String, Object>> pageList = reportService.queryHhdzBoardPage(page, boardDto);
        return new R<>(pageList);
    }

    /**
     * <B>查詢紅包接龍牌局</B>
     * @param page
     * @param boardDto
     * @return
     */
    @GetMapping("/hbjlboard/page")
    public R getHbjlBoardPage(Page page, BoardDto boardDto) {
        IPage<Map<String, Object>> pageList = reportService.queryHbjlBoardPage(page, boardDto);
        return new R<>(pageList);
    }

    /**
     * <B>查詢紅黑大戰牌局詳情</B>
     * @param id
     * @return
     */
    @GetMapping("/hhdzboard/boardDetail/{id}")
    public R getHhdzBoardDetail(@PathVariable String id) {
        List<Map<String, Object>> pageList = reportService.queryHhBoardDetail(id);
        return new R<>(pageList);
    }


    @GetMapping("/qznnboard/page")
    public R getQznnBoardPage(Page page, BoardDto boardDto) {
        IPage<Map<String, Object>> pageList = reportService.queryQznnBoardPage(page, boardDto);
        return new R<>(pageList);
    }

    @GetMapping("/qznnboard/boardDetail")
    public R getQznnBoardDetail(@RequestParam Map<String, Object> params) {
        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String) params.get("recordId") : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String) params.get("userId") : "";
        String queryTime = ObjectUtil.isNotNull(params.get("queryTime")) ? (String) params.get("queryTime") : "";
        Map<String, Object> map = reportService.queryQznnBoardDetail(recordId, userId, queryTime);
        return new R<>(map);
    }

    @GetMapping("/fqzsboard/page")
    public R getFqzsBoardPage(Page page, BoardDto boardDto) {
        IPage<Map<String, Object>> pageList = reportService.queryFqzsBoardPage(page, boardDto);
        return new R<>(pageList);
    }

    @GetMapping("/fqzsboard/boardDetail")
    public R getFqzsBoardDetail(@RequestParam Map<String, Object> params) {
        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String queryTime = ObjectUtil.isNotNull(params.get("queryTime")) ? (String)params.get("queryTime") : "";
        Map<String, Object> map = reportService.queryFqzsBoardDetail(recordId, userId, queryTime);
        return new R<>(map);
    }

    @GetMapping("/bcbmboard/page")
    public R getBcbmBoardPage(Page page, BoardDto boardDto) {
        IPage<Map<String, Object>> pageList = reportService.queryBcbmBoardPage(page, boardDto);
        return new R<>(pageList);
    }

    @GetMapping("/bcbmboard/boardDetail")
    public R getBcbmBoardDetail(@RequestParam Map<String, Object> params) {
        String recordId = ObjectUtil.isNotNull(params.get("recordId")) ? (String)params.get("recordId") : "";
        String userId = ObjectUtil.isNotNull(params.get("userId")) ? (String)params.get("userId") : "";
        String queryTime = ObjectUtil.isNotNull(params.get("queryTime")) ? (String)params.get("queryTime") : "";
        Map<String, Object> map = reportService.queryBcbmBoardDetail(recordId, userId, queryTime);
        return new R<>(map);
    }
}
