package com.youwan.game.report.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.common.core.util.R;
import com.youwan.game.report.api.dto.DantiaomultiplyDto;
import com.youwan.game.report.service.DantiaomultiplyService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * VIEW
 *
 * @author code generator
 * @date 2020-04-18 12:19:29
 */
@RestController
@AllArgsConstructor
@RequestMapping("/dantiaomultiply")
public class DantiaomultiplyController {

  private final DantiaomultiplyService dantiaomultiplyService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param dantiaomultiplyDto
   * @return
   */
  @GetMapping("/page")
  public R getDantiaomultiplyPage(Page page, DantiaomultiplyDto dantiaomultiplyDto) {
    return  new R<>(dantiaomultiplyService.queryMultiplyPageList(page, dantiaomultiplyDto));
  }


}
