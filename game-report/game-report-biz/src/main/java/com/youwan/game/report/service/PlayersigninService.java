package com.youwan.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.report.api.dto.QueryDto;
import com.youwan.game.report.api.entity.Playersignin;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-03 14:15:54
 */
public interface PlayersigninService extends IService<Playersignin> {

    IPage<Map<String, Object>> querySignInPageList(Page page, QueryDto queryDto);

}
