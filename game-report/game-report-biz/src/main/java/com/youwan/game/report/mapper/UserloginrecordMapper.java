package com.youwan.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.report.api.entity.Userloginrecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author code generator
 * @date 2019-01-25 11:56:48
 */
public interface UserloginrecordMapper extends BaseMapper<Userloginrecord> {


    /**
     * <B>分页查询</B>
     *
     * @param userId
     * @return
     */
    IPage<Userloginrecord> queryPageList(Page page, @Param("userId") Integer userId,
                                         @Param("startTime") Integer startTime,
                                         @Param("endTime") Integer endTime);


    /**
     * <B>查询是否存在表名</B>
     *
     * @param tableName
     * @return
     */
    List<Map<String, Object>> queryTableList(@Param("tableName") String tableName);


    /**
     * <B>条件查询登录信息</B>
     *
     * @param page
     * @param channelId
     * @param userId
     * @param startTime
     * @param endTime
     * @return
     */
    IPage<Map<String, Object>> queryLoginReport(Page page,
                                                @Param("channelId") String channelId,
                                                @Param("userId") Integer userId,
                                                @Param("startTime") Integer startTime,
                                                @Param("endTime") Integer endTime);
}
