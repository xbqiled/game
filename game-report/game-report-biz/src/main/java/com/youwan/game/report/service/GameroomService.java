package com.youwan.game.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.common.core.jsonmodel.GsonPlatformState;
import com.youwan.game.report.api.dto.GmActionDto;
import com.youwan.game.report.api.dto.MaintDto;
import com.youwan.game.report.api.entity.Gameroom;

import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author code generator
 * @date 2019-03-15 20:02:39
 */
public interface GameroomService extends IService<Gameroom> {

     List<Map<String, Object>> getGamePage(String status);

     Boolean gameMaint(MaintDto maintDto);

     List<Map<String, Object>>  getPlatformPage();

     Boolean platformMaint(MaintDto maintDto);

     Boolean gmAction(GmActionDto action);
}
