package com.youwan.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.report.api.entity.Usertreasurechange;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author code generator
 * @date 2019-01-25 13:11:34
 */
public interface UsertreasurechangeMapper extends BaseMapper<Usertreasurechange> {


    /**
     * <B>获取用户账变信息</B>
     *
     * @param page
     * @param userId
     * @return
     */
    IPage<Map<String, Object>> queryUserAccountPage(Page page,
                                                    @Param("channelId") String channelId,
                                                    @Param("startTime") Integer startTime,
                                                    @Param("endTime") Integer endTime,
                                                    @Param("userId") Integer userId);

}
