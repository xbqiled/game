package com.youwan.game.report.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.common.core.constant.TableNameConstant;
import com.youwan.game.common.core.util.TimeUtil;
import com.youwan.game.report.api.dto.DantiaosingleDto;
import com.youwan.game.report.api.entity.Dantiaosingle;
import com.youwan.game.report.mapper.DantiaosingleMapper;
import com.youwan.game.report.service.DantiaosingleService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.youwan.game.common.core.constant.CommonConstant.SECONDS_OF_DAY;
import static com.youwan.game.common.core.constant.ConfigConstant.*;

/**
 * VIEW
 *
 * @author code generator
 * @date 2020-04-18 12:19:42
 */
@Service("dantiaosingleService")
public class DantiaosingleServiceImpl extends ServiceImpl<DantiaosingleMapper, Dantiaosingle> implements DantiaosingleService {


    @Override
    public IPage<Map<String, Object>> querySinglePageList(Page page, DantiaosingleDto dantiaosingleDto) {
        try {

           String boardTableName = dantiaosingleDto != null && StrUtil.isNotBlank(dantiaosingleDto.getStartTime())
                    && StrUtil.isNotBlank(dantiaosingleDto.getEndTime())
                    && dantiaosingleDto.getStartTime().equals(dantiaosingleDto.getEndTime()) ?
                    TableNameConstant.DANTIAOSINGLE_TABLENAME +
                            DateUtil.format(DateUtil.parse(dantiaosingleDto.getStartTime(), DATE_FORMAT),
                                    THE_DATE_FORMAT) : TableNameConstant.DANTIAOSINGLE_VIEW;
            Integer start = null, end = null;
            if (dantiaosingleDto != null) {
                start = StrUtil.isNotBlank(dantiaosingleDto.getStartTime()) ? TimeUtil.stringToUnixTimeStamp(dantiaosingleDto.getStartTime(),
                        DATE_FORMAT) : null;
                end = StrUtil.isNotBlank(dantiaosingleDto.getEndTime()) ? TimeUtil.stringToUnixTimeStamp(dantiaosingleDto.getEndTime(),
                        DATE_FORMAT) + SECONDS_OF_DAY : null;
            }
            return baseMapper.querySinglePageList(page, dantiaosingleDto == null ? null : dantiaosingleDto.getRecordId(),
                    dantiaosingleDto == null ? null : dantiaosingleDto.getAccountId(), start, end);

        } catch (Exception e) {
            e.printStackTrace();
            List<Map<String, Object>> resultList = new ArrayList<>();
            return page.setRecords(resultList);
        }
    }
}
