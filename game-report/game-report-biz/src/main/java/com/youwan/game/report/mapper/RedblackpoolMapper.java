package com.youwan.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.report.api.entity.Redblackpool;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-03 13:55:49
 */
public interface RedblackpoolMapper extends BaseMapper<Redblackpool> {

    IPage<Map<String, Object>> queryHhdzPoolPage(Page page,
                                                       @Param("channelId") String channelId,
                                                       @Param("periodical") Integer periodical,
                                                       @Param("userId") Integer userId,
                                                       @Param("recordId") String recordId,
                                                       @Param("startTime") Integer startTime,
                                                       @Param("endTime") Integer endTime);

}
