package com.youwan.game.report.service.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.common.core.util.TimeUtil;
import com.youwan.game.report.api.dto.QueryDto;
import com.youwan.game.report.api.entity.Userloginrecord;
import com.youwan.game.report.mapper.UserloginrecordMapper;
import com.youwan.game.report.service.UserloginrecordService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.youwan.game.common.core.constant.CommonConstant.SECONDS_OF_DAY;
import static com.youwan.game.common.core.constant.ConfigConstant.DATE_FORMAT;

/**
 * @author code generator
 * @date 2019-01-25 11:56:48
 */
@Service("userloginrecordService")
public class UserloginrecordServiceImpl extends ServiceImpl<UserloginrecordMapper, Userloginrecord> implements UserloginrecordService {

    @Override
    public IPage queryPageList(Page page, Userloginrecord recorduserplatformlogin) {
        /*//首先判断是否存在当前表
        String tableName = "";

        //判断时间是否为空
        if (recorduserplatformlogin != null && StrUtil.isNotBlank(recorduserplatformlogin.getLogintime())) {
            DateTime dateTime = DateUtil.date(new Long(recorduserplatformlogin.getLogintime()));
            String time = DateUtil.format(dateTime, DatePattern.PURE_DATE_PATTERN);
            if (StrUtil.isNotBlank(time)) {
                tableName = "RecordUserPlatformLogin" + time ;
            }
        }

        //判断是否存在表名

        if (StrUtil.isNotBlank(tableName)) {
            List<Map<String, Object>> list = baseMapper.queryTableList(tableName);
            if (list != null && list.size() > 0) {
                //查询当前表数据
                return baseMapper.queryPageList(page, tableName, recorduserplatformlogin.getUserid());

            } else {
                return null;
            }
        } else {
            return null;
        }*/
        Integer start = null, end = null;
        Integer userId = null;
        if (recorduserplatformlogin != null) {
            userId = recorduserplatformlogin.getUserid();
            if (StringUtils.isNumeric(recorduserplatformlogin.getLogintime())) {
                org.joda.time.LocalDateTime time =
                        org.joda.time.LocalDateTime.fromDateFields(new Date(Long.parseLong(recorduserplatformlogin.getLogintime())))
                                .withTime(0, 0, 0, 0);
                start = Math.toIntExact(time.toDate().getTime() / 1000);
                end = start + SECONDS_OF_DAY;
            }
        }
        if (start == null) {
            return null;
        }
        return baseMapper.queryPageList(page, userId, start, end);
    }


    @Override
    public IPage queryLoginReport(Page page, QueryDto queryDto) {
        Integer start = null, end = null;
        assert queryDto != null;
        start = StrUtil.isNotBlank(queryDto.getStartTime()) ? TimeUtil.stringToUnixTimeStamp(queryDto.getStartTime(),
                DATE_FORMAT) : null;
        end = StrUtil.isNotBlank(queryDto.getEndTime()) ? TimeUtil.stringToUnixTimeStamp(queryDto.getEndTime(),
                DATE_FORMAT) + SECONDS_OF_DAY : null;

        return baseMapper.queryLoginReport(page, queryDto.getChannelNo(),
               StringUtils.isNumeric(queryDto.getAccountId()) ? Integer.parseInt(queryDto.getAccountId()) : null, start, end);
    }
}
