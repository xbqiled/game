package com.youwan.game.report.service;

import com.youwan.game.common.core.betting.entity.ThirdPartyAccountChangeRecord;
import com.youwan.game.common.data.core.Page;
import com.youwan.game.report.api.dto.TPAccountChangeQueryDto;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-09.
 */
public interface ThirdPartyAccountChangeService {

    Page<ThirdPartyAccountChangeRecord> query3rdAccountChangeRecords(TPAccountChangeQueryDto queryDto);

}
