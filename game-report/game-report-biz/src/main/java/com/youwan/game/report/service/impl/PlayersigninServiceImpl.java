package com.youwan.game.report.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.common.core.util.TimeUtil;
import com.youwan.game.report.api.dto.QueryDto;
import com.youwan.game.report.api.entity.Playersignin;
import com.youwan.game.report.mapper.PlayersigninMapper;
import com.youwan.game.report.service.PlayersigninService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static com.youwan.game.common.core.constant.CommonConstant.SECONDS_OF_DAY;
import static com.youwan.game.common.core.constant.ConfigConstant.DATE_FORMAT;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-03 14:15:54
 */
@Service("playersigninService")
public class PlayersigninServiceImpl extends ServiceImpl<PlayersigninMapper, Playersignin> implements PlayersigninService {

    @Override
    public IPage<Map<String, Object>> querySignInPageList(Page page, QueryDto queryDto) {
        Integer start = null, end = null;
        if (queryDto != null) {
            start = StrUtil.isNotBlank(queryDto.getStartTime()) ? TimeUtil.stringToUnixTimeStamp(queryDto.getStartTime(),
                    DATE_FORMAT) : null;
            end = StrUtil.isNotBlank(queryDto.getEndTime()) ? TimeUtil.stringToUnixTimeStamp(queryDto.getEndTime(),
                    DATE_FORMAT) + SECONDS_OF_DAY : null;
        }
        assert queryDto != null;
        return baseMapper.querySginPage(page, queryDto.getChannelNo(),
                StringUtils.isNumeric(queryDto.getAccountId()) ? Integer.parseInt(queryDto.getAccountId()) : null,  start, end);
    }
}
