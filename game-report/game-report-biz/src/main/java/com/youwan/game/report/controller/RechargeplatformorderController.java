package com.youwan.game.report.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.report.api.dto.QueryDto;
import com.youwan.game.report.api.entity.Rechargeplatformorder;
import com.youwan.game.report.service.RechargeplatformorderService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 充值日志(平台)
 *
 * @author code generator
 * @date 2019-01-27 16:14:56
 */
@RestController
@AllArgsConstructor
@RequestMapping("/rechargeplatformorder")
public class RechargeplatformorderController {

  private final RechargeplatformorderService rechargeplatformorderService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param queryDto
   * @return
   */
  @GetMapping("/page")
  public R getRecharPage(Page page, QueryDto queryDto) {
    return  new R<>(rechargeplatformorderService.queryPageList(page, queryDto));
  }


  /**
   * 通过id查询充值日志(平台)
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(rechargeplatformorderService.getById(id));
  }

  /**
   * 新增充值日志(平台)
   * @param rechargeplatformorder 充值日志(平台)
   * @return R
   */
  @SysLog("新增充值日志(平台)")
  @PostMapping
  public R save(@RequestBody Rechargeplatformorder rechargeplatformorder){
    return new R<>(rechargeplatformorderService.save(rechargeplatformorder));
  }

  /**
   * 修改充值日志(平台)
   * @param rechargeplatformorder 充值日志(平台)
   * @return R
   */
  @SysLog("修改充值日志(平台)")
  @PutMapping
  public R updateById(@RequestBody Rechargeplatformorder rechargeplatformorder){
    return new R<>(rechargeplatformorderService.updateById(rechargeplatformorder));
  }

  /**
   * 通过id删除充值日志(平台)
   * @param id id
   * @return R
   */
  @SysLog("删除充值日志(平台)")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(rechargeplatformorderService.removeById(id));
  }

}
