package com.youwan.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.report.api.dto.ParameterDto;
import com.youwan.game.report.api.entity.Playerdeposit;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-03 14:04:14
 */
public interface PlayerdepositService extends IService<Playerdeposit> {

    IPage<Map<String, Object>> queryDepositPageList(Page page, ParameterDto parameterDto);

}
