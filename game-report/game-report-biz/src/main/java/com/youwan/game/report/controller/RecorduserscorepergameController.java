package com.youwan.game.report.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.report.api.entity.Recorduserscorepergame;
import com.youwan.game.report.service.RecorduserscorepergameService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-01-29 13:42:42
 */
@RestController
@AllArgsConstructor
@RequestMapping("/recorduserscorepergame")
public class RecorduserscorepergameController {

  private final RecorduserscorepergameService recorduserscorepergameService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param recorduserscorepergame 
   * @return
   */
  @GetMapping("/page")
  public R getRecorduserscorepergamePage(Page page, Recorduserscorepergame recorduserscorepergame) {
    return  new R<>(recorduserscorepergameService.page(page, Wrappers.query(recorduserscorepergame)));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(recorduserscorepergameService.getById(id));
  }

  /**
   * 新增
   * @param recorduserscorepergame 
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody Recorduserscorepergame recorduserscorepergame){
    return new R<>(recorduserscorepergameService.save(recorduserscorepergame));
  }

  /**
   * 修改
   * @param recorduserscorepergame 
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody Recorduserscorepergame recorduserscorepergame){
    return new R<>(recorduserscorepergameService.updateById(recorduserscorepergame));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(recorduserscorepergameService.removeById(id));
  }

}
