package com.youwan.game.report.service.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.common.core.util.TimeUtil;
import com.youwan.game.report.api.dto.QueryDto;
import com.youwan.game.report.api.entity.Userlogoutrecord;
import com.youwan.game.report.mapper.UserlogoutRecordMapper;
import com.youwan.game.report.service.UserlogoutrecordService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.youwan.game.common.core.constant.CommonConstant.SECONDS_OF_DAY;
import static com.youwan.game.common.core.constant.ConfigConstant.DATE_FORMAT;

/**
 * 玩家登出平台记录表
 *
 * @author code generator
 * @date 2019-01-25 11:48:53
 */
@Service("userlogoutrecordService")
public class UserlogoutrecordServiceImpl extends ServiceImpl<UserlogoutRecordMapper, Userlogoutrecord> implements UserlogoutrecordService {


    @Override
    public IPage queryPageList(Page page, Userlogoutrecord recorduserplatformlogout) {
        //首先判断是否存在当前表
        /*String tableName = "";

        //判断时间是否为空
        if (recorduserplatformlogout != null && StrUtil.isNotBlank(recorduserplatformlogout.getLogouttime())) {
            DateTime dateTime = DateUtil.date(new Long(recorduserplatformlogout.getLogouttime()));
            String time = DateUtil.format(dateTime, DatePattern.PURE_DATE_PATTERN);
            if (StrUtil.isNotBlank(time)) {
                tableName = "RecordUserPlatformLogout" + time;
            }
        }

        //判断是否存在表名
        if (StrUtil.isNotBlank(tableName)) {
            List<Map<String, Object>> list = baseMapper.queryTableList(tableName);
            if (list != null && list.size() > 0) {
                //查询当前表数据
                return baseMapper.queryPageList(page, tableName, recorduserplatformlogout.getUserid());

            } else {
                return null;
            }
        } else {
            return null;
        }*/

        Integer start = null, end = null;
        Integer userId = null;
        if (recorduserplatformlogout != null) {
            userId = recorduserplatformlogout.getUserid();
            if (StringUtils.isNumeric(recorduserplatformlogout.getLogouttime())) {
                org.joda.time.LocalDateTime time =
                        org.joda.time.LocalDateTime.fromDateFields(new Date(Long.parseLong(recorduserplatformlogout.getLogouttime())))
                                .withTime(0, 0, 0, 0);
                start = Math.toIntExact(time.toDate().getTime() / 1000);
                end = start + SECONDS_OF_DAY;
            }
        }
        if (start == null) {
            return null;
        }
        return baseMapper.queryPageList(page, userId, start, end);
    }


    @Override
    public IPage queryLogoutReport(Page page, QueryDto queryDto) {
        Integer start = null, end = null;
        assert queryDto != null;
        start = StrUtil.isNotBlank(queryDto.getStartTime()) ? TimeUtil.stringToUnixTimeStamp(queryDto.getStartTime(),
                DATE_FORMAT) : null;
        end = StrUtil.isNotBlank(queryDto.getEndTime()) ? TimeUtil.stringToUnixTimeStamp(queryDto.getEndTime(),
                DATE_FORMAT) + SECONDS_OF_DAY : null;

        return baseMapper.queryLogoutReport(page, queryDto.getChannelNo(),
                StringUtils.isNumeric(queryDto.getAccountId()) ? Integer.parseInt(queryDto.getAccountId()) : null,
                start, end);
    }
}
