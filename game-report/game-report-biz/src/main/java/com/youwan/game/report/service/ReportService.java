package com.youwan.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.report.api.dto.BoardDto;
import com.youwan.game.report.api.entity.Report;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-02-05 14:02:39
 */
public interface ReportService extends IService<Report> {

    List<Report> queryEcharReport() throws IOException;


    IPage<List<Report>> getDayReportPage(Page page, BoardDto boardDto);


    IPage<List<Map<String, Object>>> getWeekReportPage(Page page, BoardDto boardDto);


    IPage<List<Map<String, Object>>> getMonthReportPage(Page page, BoardDto boardDto);


    IPage<Map<String, Object>> queryWealthRankingPage(Page page, BoardDto boardDto);


    IPage<Map<String, Object>> queryDdzBoardPage(Page page, BoardDto boardDto);


    IPage<Map<String, Object>> queryLhBoardPage(Page page, BoardDto boardDto);


    IPage<Map<String, Object>> queryByBoardPage(Page page, BoardDto boardDto);


    IPage<Map<String, Object>> queryNnBoardPage(Page page, BoardDto boardDto);


    IPage<Map<String, Object>> queryBjlBoardPage(Page page, BoardDto boardDto);


    IPage<Map<String, Object>> querySgjBoardPage(Page page, BoardDto boardDto);


    IPage<Map<String, Object>> queryPdkBoardPage(Page page, BoardDto boardDto);


    IPage<Map<String, Object>> queryTwentyOneBoardPage(Page page, BoardDto boardDto);


    IPage<Map<String, Object>> queryBhsBoardPage(Page page, BoardDto boardDto);


    IPage<Map<String, Object>> queryZjhBoardPage(Page page, BoardDto boardDto);


    IPage<Map<String, Object>> queryCsdBoardPage(Page page, BoardDto boardDto);


    List<Map<String, Object>> queryCsdboardDetail(String id);


    IPage<Map<String, Object>> queryHhdzBoardPage(Page page, BoardDto boardDto);


    IPage<Map<String, Object>> queryHbjlBoardPage(Page page, BoardDto boardDto);


    List<Map<String, Object>> queryHhBoardDetail(String recordId);


    IPage<Map<String, Object>> queryQznnBoardPage(Page page, BoardDto boardDto);


    Map<String, Object> queryQznnBoardDetail(String recordId, String userId, String queryTime);


    IPage<Map<String, Object>> queryFqzsBoardPage(Page page, BoardDto boardDto);


    Map<String, Object> queryFqzsBoardDetail(String recordId, String userId, String queryTime);


    IPage<Map<String, Object>> queryBcbmBoardPage(Page page, BoardDto boardDto);


    Map<String, Object> queryBcbmBoardDetail(String recordId, String userId, String queryTime);
}
