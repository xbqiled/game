package com.youwan.game.report.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.common.core.util.R;
import com.youwan.game.report.api.dto.ParameterDto;
import com.youwan.game.report.service.PlayerdepositService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-03 14:04:14
 */
@RestController
@AllArgsConstructor
@RequestMapping("/playerdeposit")
public class PlayerdepositController {

    private final PlayerdepositService playerdepositService;

    /**
     * 分页查询
     *
     * @param page         分页对象
     * @param parameterDto VIEW
     * @return
     */
    @GetMapping("/page")
    public R getPlayerdepositPage(Page page, ParameterDto parameterDto) {
        return new R<>(playerdepositService.queryDepositPageList(page, parameterDto));
    }


}
