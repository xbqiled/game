package com.youwan.game.report.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.report.api.entity.Gamerebate;
import com.youwan.game.report.mapper.GamerebateMapper;
import com.youwan.game.report.service.GamerebateService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-02 12:18:56
 */
@Service("gamerebateService")
public class GamerebateServiceImpl extends ServiceImpl<GamerebateMapper, Gamerebate> implements GamerebateService {

    @Override
    public IPage<Map<String, Object>> queryGamerebatePage(Page page, String accountId, String channelNo) {
        return baseMapper.queryGamerebatePage(page, StringUtils.isNumeric(accountId) ? Integer.parseInt(accountId) : null, channelNo);
    }
}
