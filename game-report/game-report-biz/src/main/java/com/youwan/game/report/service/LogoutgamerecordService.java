package com.youwan.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.report.api.dto.QueryDto;
import com.youwan.game.report.api.entity.Logoutgamerecord;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-25 17:19:43
 */
public interface LogoutgamerecordService extends IService<Logoutgamerecord> {

    IPage queryPageList(Page page, QueryDto qeryDto);
}
