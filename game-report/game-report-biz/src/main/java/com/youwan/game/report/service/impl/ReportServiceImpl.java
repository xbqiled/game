package com.youwan.game.report.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.ImmutableList;
import com.youwan.game.common.core.constant.TableNameConstant;
import com.youwan.game.common.core.util.TimeUtil;
import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import com.youwan.game.report.api.dto.BoardDto;
import com.youwan.game.report.api.entity.Report;
import com.youwan.game.report.handler.BoardHandler;
import com.youwan.game.report.mapper.ReportMapper;
import com.youwan.game.report.mapper.xbqp.AccountInfoMapper;
import com.youwan.game.report.service.ReportService;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.collect.Tuple;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.bucket.range.Range;
import org.elasticsearch.search.aggregations.bucket.range.RangeAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.Cardinality;
import org.elasticsearch.search.aggregations.metrics.Sum;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static com.youwan.game.common.core.constant.CommonConstant.SECONDS_OF_DAY;
import static com.youwan.game.common.core.constant.ConfigConstant.*;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;
import static org.elasticsearch.index.query.QueryBuilders.termsQuery;
import static org.elasticsearch.search.aggregations.AggregationBuilders.*;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-02-05 14:02:39
 */
@Service("reportService")
public class ReportServiceImpl extends ServiceImpl<ReportMapper, Report> implements ReportService {

    @Autowired
    private AccountInfoMapper accountInfoMapper;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    /**
     * <B>日报信息表</B>
     *
     * @param page
     * @param boardDto
     * @return
     */
    @Override
    public IPage<List<Report>> getDayReportPage(Page page, BoardDto boardDto) {
        return baseMapper.getDayReportPage(page, boardDto.getChannelNo(), boardDto.getStartTime(), boardDto.getEndTime());
    }


    /**
     * <B>周报信息表</B>
     *
     * @param page
     * @param boardDto
     * @return
     */
    @Override
    public IPage<List<Map<String, Object>>> getWeekReportPage(Page page, BoardDto boardDto) {
        String weekTime = "";
        if (boardDto != null && StrUtil.isNotBlank(boardDto.getWeekTime())) {
            long length = boardDto.getWeekTime().length();
            String year = boardDto.getWeekTime().substring(0, 10);
            String week = boardDto.getWeekTime().substring(0, 4);
        }
        return baseMapper.getWeekReportPage(page, boardDto.getChannelNo(), weekTime);
    }

    /**
     * <B>月报信息表</B>
     *
     * @param page
     * @param boardDto
     * @return
     */
    @Override
    public IPage<List<Map<String, Object>>> getMonthReportPage(Page page, BoardDto boardDto) {
        return baseMapper.getMonthReportPage(page, boardDto.getChannelNo(), boardDto.getMonthTime());
    }

    /**
     * <B>查询报表信息</B>
     *
     * @return
     */
    @Override
    public List<Report> queryEcharReport() throws IOException {
        Map<String, Tuple<Long, Long>> rangs = new HashMap<>();
        Map<String, Report> entitys = new TreeMap<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o2.compareTo(o1);
            }
        });
        long diff = 24 * 60 * 60 * 1000;
        Long end = LocalDateTime.now().withTime(0, 0, 0, 0).plusDays(1).toDate().getTime();
        //return baseMapper.queryEcharReport();
        for (int i = 0; i < 7; i++) {
            Tuple<Long, Long> rangeTupe = new Tuple<>(end - diff, end);
            String key = LocalDateTime.fromDateFields(new Date(rangeTupe.v1())).toString("yyyy-MM-dd");
            rangs.put(key, rangeTupe);
            Report entity = new Report();
            entity.setRecordtime(key);
            entitys.put(key, entity);
            end = rangeTupe.v1();
        }
        //recharge stats
        rechargeStats(rangs, entitys);
        //login platform stats
        userLoginStats(rangs, entitys);
        //login game stats
        loginGameStats(rangs, entitys);
        //account reg stats
        accountRegStats(rangs, entitys);
        //System.err.println(entitys);
        return entitys.values().stream().collect(Collectors.toList());
    }

    private void rechargeStats(Map<String, Tuple<Long, Long>> rangs, Map<String, Report> entitys) throws IOException {
        BoolQueryBuilder query = QueryBuilders.boolQuery().must(termsQuery("handler_type", ImmutableList.of(1, 2, 3))).must(termQuery("status", 1));
        SearchRequest searchRequest = new SearchRequest("sys_pay_order");
        RangeAggregationBuilder rangeAgg = range("ranges").field("create_time").subAggregation(sum("pay_fee").field("pay_fee"));

        rangs.forEach((key, tuple) -> rangeAgg.addRange(key, tuple.v1(), tuple.v2()));
        SearchSourceBuilder source = new SearchSourceBuilder().query(query)
                .size(0).aggregation(rangeAgg);
        SearchResponse response = elasticsearchTemplate.getClient().search(searchRequest.source(source), RequestOptions.DEFAULT);
        Range range = response.getAggregations().get("ranges");
        range.getBuckets().forEach(bucket -> {
            Report entity = entitys.get(bucket.getKeyAsString());
            entity.setRechargecount(bucket.getDocCount());
            entity.setTotalrechargemoney(BigDecimal.valueOf(((Sum) bucket.getAggregations().get("pay_fee")).getValue()));
        });
    }

    private void userLoginStats(Map<String, Tuple<Long, Long>> rangs, Map<String, Report> entitys) throws IOException {
        BoolQueryBuilder query = QueryBuilders.boolQuery();
        SearchRequest searchRequest = new SearchRequest("record_user_platform_login");
        RangeAggregationBuilder rangeAgg = range("ranges").field("loginTime").subAggregation(cardinality("userID").field("userID"));
        rangs.forEach((key, tuple) -> rangeAgg.addRange(key, tuple.v1(), tuple.v2()));

        SearchSourceBuilder source = new SearchSourceBuilder().query(query).size(0).aggregation(rangeAgg);
        SearchResponse response = elasticsearchTemplate.getClient().search(searchRequest.source(source), RequestOptions.DEFAULT);
        Range range = response.getAggregations().get("ranges");
        range.getBuckets().forEach(bucket -> {
            Report entity = entitys.get(bucket.getKeyAsString());
            entity.setLoginusercount(bucket.getDocCount());
            entity.setLogincount(((Cardinality) bucket.getAggregations().get("userID")).getValue());
        });
    }

    private void loginGameStats(Map<String, Tuple<Long, Long>> rangs, Map<String, Report> entitys) throws IOException {
        BoolQueryBuilder query = QueryBuilders.boolQuery();
        SearchRequest searchRequest = new SearchRequest("record_user_game_login");
        RangeAggregationBuilder rangeAgg = range("ranges").field("recordTime").subAggregation(cardinality("userID").field("userID"));
        rangs.forEach((key, tuple) -> rangeAgg.addRange(key, tuple.v1(), tuple.v2()));
        SearchSourceBuilder source = new SearchSourceBuilder().query(query).size(0).aggregation(rangeAgg);
        SearchResponse response = elasticsearchTemplate.getClient().search(searchRequest.source(source), RequestOptions.DEFAULT);
        Range range = response.getAggregations().get("ranges");
        range.getBuckets().forEach(bucket -> {
            Report entity = entitys.get(bucket.getKeyAsString());
            entity.setLogingamecount(((Cardinality) bucket.getAggregations().get("userID")).getValue());
        });
    }

    private void accountRegStats(Map<String, Tuple<Long, Long>> rangs, Map<String, Report> entitys) throws IOException {
        BoolQueryBuilder query = QueryBuilders.boolQuery();
        SearchRequest searchRequest = new SearchRequest("record_account_reg");
        RangeAggregationBuilder rangeAgg = range("ranges").field("recordTime");
        rangs.forEach((key, tuple) -> rangeAgg.addRange(key, tuple.v1(), tuple.v2()));
        SearchSourceBuilder source = new SearchSourceBuilder().query(query).size(0).aggregation(rangeAgg);
        SearchResponse response = elasticsearchTemplate.getClient().search(searchRequest.source(source), RequestOptions.DEFAULT);
        Range range = response.getAggregations().get("ranges");
        range.getBuckets().forEach(bucket -> {
            Report entity = entitys.get(bucket.getKeyAsString());
            entity.setRegcount(bucket.getDocCount());
        });
    }
    /**
     * <B>查询财富排行榜</B>
     *
     * @param page
     * @param boardDto
     * @return
     */
    @Override
    public IPage<Map<String, Object>> queryWealthRankingPage(Page page, BoardDto boardDto) {

        assert boardDto != null;
        Integer start = null, end = null;
        Integer userId = null;
        userId = StringUtils.isNumeric(boardDto.getAccountId()) ? Integer.parseInt(boardDto.getAccountId()) : null;
        if (StringUtils.isNotBlank(boardDto.getStartTime())) {
            start = TimeUtil.stringToUnixTimeStamp(boardDto.getStartTime(), DATE_FORMAT);
        }
        if (StringUtils.isNotBlank(boardDto.getEndTime())) {
            end = TimeUtil.stringToUnixTimeStamp(boardDto.getEndTime(), DATE_FORMAT) + SECONDS_OF_DAY - 1;
        }
        page.setTotal(-1l);
        IPage<Map<String, Object>> ret = baseMapper.queryWealthRankingPage(page, boardDto.getChannelNo(), userId, start, end);
        ret.setTotal(baseMapper.queryWealthRankCountList(boardDto.getChannelNo(), userId, start, end).stream().filter(Objects::nonNull).mapToLong(_int -> _int).sum());
        List<Integer> ids = ret.getRecords().stream().mapToInt(item -> (Integer) item.get("UserId")).distinct().boxed().collect(Collectors.toList());
        List<Map<String, Object>> users = accountInfoMapper.queryUserByIds(ids);
        Map<Integer, Map<String, Object>> umap = new HashMap<>();
        users.forEach(user -> umap.put(Math.toIntExact((Long) user.get("userId")), user));
        ret.getRecords().forEach(item -> {
            Integer uid = (Integer) item.get("UserId");
            Map<String, Object> user = umap.get(uid);
            if (user != null) {
                item.put("nickName", user.get("nickName"));
                item.put("userName", user.get("userName"));
                item.put("phoneNumber", user.get("phoneNumber"));
                item.put("channelName", user.get("channelName"));
            }
        });
        return ret;
    }

    /**
     * <B>斗地主牌局</B>
     *
     * @param page
     * @param boardDto
     * @return
     */
    @Override
    public IPage<Map<String, Object>> queryDdzBoardPage(Page page, BoardDto boardDto) {
        try {
            /*String boardTableName = boardDto != null && StrUtil.isNotEmpty(boardDto.getQueryTime()) ?
                    TableNameConstant.DDZBOARD_TABLENAME + DateUtil.format(DateUtil.parse(boardDto.getQueryTime(), DATE_FORMAT), THE_DATE_FORMAT) :
                    TableNameConstant.DDZBOARD_VIEWNAME;

            String extTableName = boardDto != null && StrUtil.isNotEmpty(boardDto.getQueryTime()) ?
                    TableNameConstant.DDZEXTINFO_TABLENAME + DateUtil.format(DateUtil.parse(boardDto.getQueryTime(), DATE_FORMAT), THE_DATE_FORMAT)
                     : TableNameConstant.DDZEXTINFO_VIEWNAME;*/
            assert boardDto != null;
            Integer start = null, end = null;
            Integer userId = null;
            userId = StringUtils.isNumeric(boardDto.getAccountId()) ? Integer.parseInt(boardDto.getAccountId()) : null;
            if (StringUtils.isNotBlank(boardDto.getQueryTime())) {
                start = TimeUtil.stringToUnixTimeStamp(boardDto.getQueryTime(), DATE_FORMAT);
                end = start + SECONDS_OF_DAY - 1;
            }
            IPage<Map<String, Object>> ipage = baseMapper.queryDdzBoardPage(page, boardDto.getChannelNo(),
                    boardDto.getRecordId(), userId, start, end);
            BoardHandler boardHandler = new BoardHandler();
            return boardHandler.handlerDdzBoard(ipage);

        } catch (Exception e) {
            List<Map<String, Object>> resultList = new ArrayList<>();
            return page.setRecords(resultList);
        }
    }

    /**
     * <B>龙虎斗牌局</B>
     *
     * @param page
     * @param boardDto
     * @return
     */
    @Override
    public IPage<Map<String, Object>> queryLhBoardPage(Page page, BoardDto boardDto) {
        try {
           /* String boardTableName = boardDto != null && StrUtil.isNotEmpty(boardDto.getQueryTime()) ?
                    TableNameConstant.LBDBOARD_TABLENAME +
                            DateUtil.format(DateUtil.parse(boardDto.getQueryTime(), DATE_FORMAT), THE_DATE_FORMAT) :
                    TableNameConstant.LBDBOARD_VIEWNAME;*/
            assert boardDto != null;
            Integer start = null, end = null;
            Integer userId = null;
            userId = StringUtils.isNumeric(boardDto.getAccountId()) ? Integer.parseInt(boardDto.getAccountId()) : null;
            if (StringUtils.isNotBlank(boardDto.getQueryTime())) {
                start = TimeUtil.stringToUnixTimeStamp(boardDto.getQueryTime(), DATE_FORMAT);
                end = start + SECONDS_OF_DAY - 1;
            }
            IPage<Map<String, Object>> ipage = baseMapper.queryLhBoardPage(page, boardDto.getChannelNo(),
                    boardDto.getRecordId(), userId, start, end);
            BoardHandler boardHandler = BoardHandler.getInstance();
            return boardHandler.handlerLhBoard(ipage);
        } catch (Exception e) {
            List<Map<String, Object>> resultList = new ArrayList<>();
            return page.setRecords(resultList);
        }
    }

    /**
     * <B>捕鱼牌局</B>
     *
     * @param page
     * @param boardDto
     * @return
     */
    @Override
    public IPage<Map<String, Object>> queryByBoardPage(Page page, BoardDto boardDto) {
        try {
            String boardTableName = boardDto != null && StrUtil.isNotEmpty(boardDto.getQueryTime()) ?
                    TableNameConstant.BYBOARD_TABLENAME +
                            DateUtil.format(DateUtil.parse(boardDto.getQueryTime(), DATE_FORMAT), THE_DATE_FORMAT) :
                    TableNameConstant.BYBOARD_VIEWNAME;
            assert boardDto != null;
            Integer start = null, end = null;
            Integer userId = null;
            userId = StringUtils.isNumeric(boardDto.getAccountId()) ? Integer.parseInt(boardDto.getAccountId()) : null;
            if (StringUtils.isNotBlank(boardDto.getQueryTime())) {
                start = TimeUtil.stringToUnixTimeStamp(boardDto.getQueryTime(), DATE_FORMAT);
                end = start + SECONDS_OF_DAY - 1;
            }
            IPage<Map<String, Object>> ipage = baseMapper.queryByBoardPage(page, boardDto.getChannelNo(),
                    boardDto.getRecordId(), userId, start, end);
            BoardHandler boardHandler = BoardHandler.getInstance();
            return boardHandler.handlerByBoard(ipage);
        } catch (Exception e) {
            List<Map<String, Object>> resultList = new ArrayList<>();
            return page.setRecords(resultList);
        }
    }

    /**
     * <B>牛牛牌局</B>
     *
     * @param page
     * @param boardDto
     * @return
     */
    @Override
    public IPage<Map<String, Object>> queryNnBoardPage(Page page, BoardDto boardDto) {
        try {
            /*String boardTableName = boardDto != null && StrUtil.isNotEmpty(boardDto.getQueryTime()) ?
                    TableNameConstant.NNBOARD_TABLENAME +
                            DateUtil.format(DateUtil.parse(boardDto.getQueryTime(), DATE_FORMAT), THE_DATE_FORMAT) :
                    TableNameConstant.NNBOARD_VIEWNAME;*/
            assert boardDto != null;
            Integer start = null, end = null;
            Integer userId = null;
            userId = StringUtils.isNumeric(boardDto.getAccountId()) ? Integer.parseInt(boardDto.getAccountId()) : null;
            if (StringUtils.isNotBlank(boardDto.getQueryTime())) {
                start = TimeUtil.stringToUnixTimeStamp(boardDto.getQueryTime(), DATE_FORMAT);
                end = start + SECONDS_OF_DAY - 1;
            }
            IPage<Map<String, Object>> ipage = baseMapper.queryNnBoardPage(page, boardDto.getChannelNo(),
                    boardDto.getRecordId(), userId, start, end);
            BoardHandler boardHandler = new BoardHandler();
            return boardHandler.handlerNnBoard(ipage);
        } catch (Exception e) {
            List<Map<String, Object>> resultList = new ArrayList<>();
            return page.setRecords(resultList);
        }
    }

    /**
     * <B>百家乐牌局</B>
     *
     * @param page
     * @param boardDto
     * @return
     */
    @Override
    public IPage<Map<String, Object>> queryBjlBoardPage(Page page, BoardDto boardDto) {
        try {
            /*String boardTableName = boardDto != null && StrUtil.isNotEmpty(boardDto.getQueryTime()) ?
                    TableNameConstant.BJLBOARD_TABLENAME +
                            DateUtil.format(DateUtil.parse(boardDto.getQueryTime(), DATE_FORMAT), THE_DATE_FORMAT) :
                    TableNameConstant.BJLBOARD_VIEWNAME;*/
            assert boardDto != null;
            Integer start = null, end = null;
            Integer userId = null;
            userId = StringUtils.isNumeric(boardDto.getAccountId()) ? Integer.parseInt(boardDto.getAccountId()) : null;
            if (StringUtils.isNotBlank(boardDto.getQueryTime())) {
                start = TimeUtil.stringToUnixTimeStamp(boardDto.getQueryTime(), DATE_FORMAT);
                end = start + SECONDS_OF_DAY - 1;
            }
            IPage<Map<String, Object>> ipage = baseMapper.queryBjlBoardPage(page,
                    boardDto.getChannelNo(), boardDto.getRecordId(), userId, start, end);
            BoardHandler boardHandler = BoardHandler.getInstance();
            return boardHandler.handlerBjlBoard(ipage);
        } catch (Exception e) {
            List<Map<String, Object>> resultList = new ArrayList<>();
            return page.setRecords(resultList);
        }
    }

    /**
     * <B>水果机牌局</B>
     *
     * @param page
     * @param boardDto
     * @return
     */
    @Override
    public IPage<Map<String, Object>> querySgjBoardPage(Page page, BoardDto boardDto) {
        try {
            /*String boardTableName = boardDto != null && StrUtil.isNotEmpty(boardDto.getQueryTime()) ?
                    TableNameConstant.SGJBOARD_TABLENAME +
                            DateUtil.format(DateUtil.parse(boardDto.getQueryTime(), DATE_FORMAT), THE_DATE_FORMAT) :
                    TableNameConstant.SGJBOARD_VIEWNAME;*/
            assert boardDto != null;
            Integer start = null, end = null;
            Integer userId = null;
            userId = StringUtils.isNumeric(boardDto.getAccountId()) ? Integer.parseInt(boardDto.getAccountId()) : null;
            if (StringUtils.isNotBlank(boardDto.getQueryTime())) {
                start = TimeUtil.stringToUnixTimeStamp(boardDto.getQueryTime(), DATE_FORMAT);
                end = start + SECONDS_OF_DAY - 1;
            }
            IPage<Map<String, Object>> ipage = baseMapper.querySgjBoardPage(page, boardDto.getChannelNo(),
                    boardDto.getRecordId(), userId, start, end);
            BoardHandler boardHandler = BoardHandler.getInstance();
            return boardHandler.handlerSgjBoard(ipage);
        } catch (Exception e) {
            List<Map<String, Object>> resultList = new ArrayList<>();
            return page.setRecords(resultList);
        }
    }

    /**
     * <B>21点牌局</B>
     *
     * @param page
     * @param boardDto
     * @return
     */
    @Override
    public IPage<Map<String, Object>> queryTwentyOneBoardPage(Page page, BoardDto boardDto) {
        try {
            /*String boardTableName = boardDto != null && StrUtil.isNotEmpty(boardDto.getQueryTime()) ?
                    TableNameConstant.ESYDBOARD_TABLENAME +
                            DateUtil.format(DateUtil.parse(boardDto.getQueryTime(), DATE_FORMAT), THE_DATE_FORMAT) :
                    TableNameConstant.ESYDBOARD_VIEWNAME;*/
            assert boardDto != null;
            Integer start = null, end = null;
            Integer userId = null;
            userId = StringUtils.isNumeric(boardDto.getAccountId()) ? Integer.parseInt(boardDto.getAccountId()) : null;
            if (StringUtils.isNotBlank(boardDto.getQueryTime())) {
                start = TimeUtil.stringToUnixTimeStamp(boardDto.getQueryTime(), DATE_FORMAT);
                end = start + SECONDS_OF_DAY - 1;
            }
            IPage<Map<String, Object>> ipage = baseMapper.queryTwentyOneBoardPage(page, boardDto.getChannelNo(),
                    boardDto.getRecordId(), userId, start, end);
            BoardHandler boardHandler = BoardHandler.getInstance();
            return boardHandler.handlerTwentyOneBoard(ipage);
        } catch (Exception e) {
            List<Map<String, Object>> resultList = new ArrayList<>();
            return page.setRecords(resultList);
        }
    }

    /**
     * <B>跑得快牌局</B>
     *
     * @param page
     * @param boardDto
     * @return
     */
    @Override
    public IPage<Map<String, Object>> queryPdkBoardPage(Page page, BoardDto boardDto) {
        try {
            /*String boardTableName = boardDto != null && StrUtil.isNotEmpty(boardDto.getQueryTime()) ?
                    TableNameConstant.PDKBOARD_TABLENAME +
                            DateUtil.format(DateUtil.parse(boardDto.getQueryTime(), DATE_FORMAT), THE_DATE_FORMAT) :
                    TableNameConstant.PDKBOARD_VIEWNAME;

            String extTableName = boardDto != null && StrUtil.isNotEmpty(boardDto.getQueryTime()) ?
                    TableNameConstant.PDKEXTINFO_TABLENAME + boardDto.getQueryTime() : TableNameConstant.PDKEXTINFO_VIEWNAME;*/
            assert boardDto != null;
            Integer start = null, end = null;
            Integer userId = null;
            userId = StringUtils.isNumeric(boardDto.getAccountId()) ? Integer.parseInt(boardDto.getAccountId()) : null;
            if (StringUtils.isNotBlank(boardDto.getQueryTime())) {
                start = TimeUtil.stringToUnixTimeStamp(boardDto.getQueryTime(), DATE_FORMAT);
                end = start + SECONDS_OF_DAY - 1;
            }
            IPage<Map<String, Object>> ipage = baseMapper.queryPdkBoardPage(page, boardDto.getChannelNo(),
                    boardDto.getRecordId(), userId, start, end);
            BoardHandler boardHandler = BoardHandler.getInstance();
            return boardHandler.handlerPdkBoard(ipage);
        } catch (Exception e) {
            List<Map<String, Object>> resultList = new ArrayList<>();
            return page.setRecords(resultList);
        }
    }

    /**
     * <B>比花色牌局</B>
     *
     * @param page
     * @param boardDto
     * @return
     */
    @Override
    public IPage<Map<String, Object>> queryBhsBoardPage(Page page, BoardDto boardDto) {
        try {
            /*String boardTableName = boardDto != null && StrUtil.isNotEmpty(boardDto.getQueryTime()) ?
                    TableNameConstant.HSJLHBOARD_TABLENAME +
                            DateUtil.format(DateUtil.parse(boardDto.getQueryTime(), DATE_FORMAT), THE_DATE_FORMAT) :
                    TableNameConstant.HSJLHBOARD_VIEWNAME;*/
            assert boardDto != null;
            Integer start = null, end = null;
            Integer userId = null;
            userId = StringUtils.isNumeric(boardDto.getAccountId()) ? Integer.parseInt(boardDto.getAccountId()) : null;
            if (StringUtils.isNotBlank(boardDto.getQueryTime())) {
                start = TimeUtil.stringToUnixTimeStamp(boardDto.getQueryTime(), DATE_FORMAT);
                end = start + SECONDS_OF_DAY - 1;
            }
            IPage<Map<String, Object>> ipage = baseMapper.queryBhsBoardPage(page, boardDto.getChannelNo(),
                    boardDto.getRecordId(), userId, start, end);
            BoardHandler boardHandler = BoardHandler.getInstance();
            return boardHandler.handlerBhsBoard(ipage);
        } catch (Exception e) {
            List<Map<String, Object>> resultList = new ArrayList<>();
            return page.setRecords(resultList);
        }
    }

    /**
     * <B>炸金花牌局</B>
     *
     * @param page
     * @param boardDto
     * @return
     */
    @Override
    public IPage<Map<String, Object>> queryZjhBoardPage(Page page, BoardDto boardDto) {
        try {
            /*String boardTableName = boardDto != null && StrUtil.isNotEmpty(boardDto.getQueryTime()) ?
                    TableNameConstant.ZJHBOARD_TABLENAME +
                            DateUtil.format(DateUtil.parse(boardDto.getQueryTime(), DATE_FORMAT), THE_DATE_FORMAT) :
                    TableNameConstant.ZJHBOARD_VIEWNAME;

            String extTableName = boardDto != null && StrUtil.isNotEmpty(boardDto.getQueryTime()) ?
                    TableNameConstant.ZJHEXTINFO_TABLENAME +
                            DateUtil.format(DateUtil.parse(boardDto.getQueryTime(), DATE_FORMAT), THE_DATE_FORMAT) :
                    TableNameConstant.ZJHEXTINFO_VIEWNAME;*/
            assert boardDto != null;
            Integer start = null, end = null;
            Integer userId = null;
            userId = StringUtils.isNumeric(boardDto.getAccountId()) ? Integer.parseInt(boardDto.getAccountId()) : null;
            if (StringUtils.isNotBlank(boardDto.getQueryTime())) {
                start = TimeUtil.stringToUnixTimeStamp(boardDto.getQueryTime(), DATE_FORMAT);
                end = start + SECONDS_OF_DAY - 1;
            }
            IPage<Map<String, Object>> ipage = baseMapper.queryZjhBoardPage(page, boardDto.getChannelNo(),
                    boardDto.getRecordId(), userId, start, end);
            BoardHandler boardHandler = BoardHandler.getInstance();
            return boardHandler.handlerZjhBoard(ipage);
        } catch (Exception e) {
            List<Map<String, Object>> resultList = new ArrayList<>();
            return page.setRecords(resultList);
        }
    }

    /**
     * <B>财神到牌局</B>
     *
     * @param page
     * @param boardDto
     * @return
     */
    @Override
    public IPage<Map<String, Object>> queryCsdBoardPage(Page page, BoardDto boardDto) {
        try {
            /*String boardTableName = boardDto != null && StrUtil.isNotEmpty(boardDto.getQueryTime()) ?
                    TableNameConstant.CSDBOARD_TABLENAME +
                            DateUtil.format(DateUtil.parse(boardDto.getQueryTime(), DATE_FORMAT), THE_DATE_FORMAT) :
                    TableNameConstant.CSDBOARD_VIEWNAME;*/
            assert boardDto != null;
            Integer start = null, end = null;
            Integer userId = null;
            userId = StringUtils.isNumeric(boardDto.getAccountId()) ? Integer.parseInt(boardDto.getAccountId()) : null;
            if (StringUtils.isNotBlank(boardDto.getQueryTime())) {
                start = TimeUtil.stringToUnixTimeStamp(boardDto.getQueryTime(), DATE_FORMAT);
                end = start + SECONDS_OF_DAY - 1;
            }
            IPage<Map<String, Object>> ipage = baseMapper.queryCsdBoardPage(page, boardDto.getChannelNo(),
                    boardDto.getRecordId(), userId, start, end);
            BoardHandler boardHandler = BoardHandler.getInstance();
            return boardHandler.handlerCsdBoard(ipage);
        } catch (Exception e) {
            List<Map<String, Object>> resultList = new ArrayList<>();
            return page.setRecords(resultList);
        }
    }


    /**
     * <B>显示财神到牌局的详情</B>
     *
     * @param id
     * @return
     */
    @Override
    public List<Map<String, Object>> queryCsdboardDetail(String id) {
        String boardDetail = baseMapper.queryCsdboardDetail(id);
        BoardHandler boardHandler = BoardHandler.getInstance();
        return boardHandler.handlerCsdBoardDetail(boardDetail);
    }

    @Override
    public IPage<Map<String, Object>> queryHhdzBoardPage(Page page, BoardDto boardDto) {
        try {
            /*String boardTableName = boardDto != null && StrUtil.isNotEmpty(boardDto.getQueryTime()) ?
                    TableNameConstant.HHDZBOARD_TABLENAME +
                            DateUtil.format(DateUtil.parse(boardDto.getQueryTime(), DATE_FORMAT), THE_DATE_FORMAT) :
                    TableNameConstant.HHDZBOARD_VIEWNAME;*/
            assert boardDto != null;
            Integer start = null, end = null;
            Integer userId = null;
            userId = StringUtils.isNumeric(boardDto.getAccountId()) ? Integer.parseInt(boardDto.getAccountId()) : null;
            if (StringUtils.isNotBlank(boardDto.getQueryTime())) {
                start = TimeUtil.stringToUnixTimeStamp(boardDto.getQueryTime(), DATE_FORMAT);
                end = start + SECONDS_OF_DAY - 1;
            }
            return baseMapper.queryHhdzBoard(page, boardDto.getChannelNo(), userId, boardDto.getRecordId(),
                    start, end);
        } catch (Exception e) {
            e.printStackTrace();
            List<Map<String, Object>> resultList = new ArrayList<>();
            return page.setRecords(resultList);
        }
    }

    @Override
    public List<Map<String, Object>> queryHhBoardDetail(String recordId) {
        BoardHandler boardHandler = BoardHandler.getInstance();
        List<Map<String, Object>> list = baseMapper.queryHhBoardDetail(recordId);
        return boardHandler.handlerHhdzBoard(list);
    }

    @Override
    public IPage<Map<String, Object>> queryHbjlBoardPage(Page page, BoardDto boardDto) {
        try {
            /*String boardTableName = boardDto != null && StrUtil.isNotEmpty(boardDto.getQueryTime()) ?
                    TableNameConstant.HBJLBOARD_TABLENAME +
                            DateUtil.format(DateUtil.parse(boardDto.getQueryTime(), DATE_FORMAT), THE_DATE_FORMAT) :
                    TableNameConstant.HBJLBOARD_VIEWNAME;*/
            assert boardDto != null;
            Integer start = null, end = null;
            Integer userId = null;
            userId = StringUtils.isNumeric(boardDto.getAccountId()) ? Integer.parseInt(boardDto.getAccountId()) : null;
            if (StringUtils.isNotBlank(boardDto.getQueryTime())) {
                start = TimeUtil.stringToUnixTimeStamp(boardDto.getQueryTime(), DATE_FORMAT);
                end = start + SECONDS_OF_DAY - 1;
            }
            return baseMapper.queryHbjlBoard(page, boardDto.getChannelNo(), userId, boardDto.getRecordId(), start, end);
        } catch (Exception e) {
            List<Map<String, Object>> resultList = new ArrayList<>();
            return page.setRecords(resultList);
        }
    }


    @Override
    public IPage<Map<String, Object>> queryQznnBoardPage(Page page, BoardDto boardDto) {
        try {
            /*String boardTableName = boardDto != null && StrUtil.isNotEmpty(boardDto.getQueryTime()) ?
                    TableNameConstant.QZNNBOARD_TABLENAME +
                            DateUtil.format(DateUtil.parse(boardDto.getQueryTime(), DATE_FORMAT), THE_DATE_FORMAT) :
                    TableNameConstant.QZNNBOARD_VIEWNAME;*/
            assert boardDto != null;
            Integer start = null, end = null;
            Integer userId = null;
            userId = StringUtils.isNumeric(boardDto.getAccountId()) ? Integer.parseInt(boardDto.getAccountId()) : null;
            if (StringUtils.isNotBlank(boardDto.getQueryTime())) {
                start = TimeUtil.stringToUnixTimeStamp(boardDto.getQueryTime(), DATE_FORMAT);
                end = start + SECONDS_OF_DAY - 1;
            }
            return baseMapper.queryQznnBoard(page, boardDto.getChannelNo(), userId, boardDto.getRecordId(),
                    start, end);
        } catch (Exception e) {
            List<Map<String, Object>> resultList = new ArrayList<>();
            return page.setRecords(resultList);
        }
    }


    @Override
    public Map<String, Object> queryQznnBoardDetail(String recordId, String userId, String queryTime) {
        //Date queryDate = DateUtil.parse(queryTime, FULL_DATE_TIME_FORMAT);
        //String boardTableName = TableNameConstant.QZNNBOARD_TABLENAME + DateUtil.format(queryDate, THE_DATE_FORMAT);

        DateTime start = null, end = null;
        if (StringUtils.isNotBlank(queryTime)) {
            start = DateTime.parse(queryTime, DateTimeFormat.forPattern(FULL_DATE_TIME_FORMAT)).withDayOfMonth(1).withTime(0, 0, 0, 0);
            end = start.plusMonths(1).minusSeconds(1);
        }
        Map<String, Object> boardMap = baseMapper.getQznnBoardById(recordId,
                StringUtils.isNumeric(userId) ? Integer.parseInt(userId) : null,
                start == null ? null : Math.toIntExact(start.toDate().getTime() / 1000)
                , end == null ? null : Math.toIntExact(end.toDate().getTime() / 1000));

        String betMultiple = ObjectUtil.isNotNull(boardMap.get("betMultiple")) ? (String) boardMap.get("betMultiple") : "";
        String CardRecord = ObjectUtil.isNotNull(boardMap.get("CardRecord")) ? (String) boardMap.get("CardRecord") : "";
        String cardType = ObjectUtil.isNotNull(boardMap.get("cardType")) ? (String) boardMap.get("cardType") : "";

        BoardHandler boardHandler = new BoardHandler();
        Map<String, Object> resultMap = new HashMap<>();
        String betMultipleStr = boardHandler.analysisQznnBetMultiple(betMultiple);
        //解析出牌信息
        List<Map<String, Object>> cardRecordList = boardHandler.analysisQznnCardRecord(CardRecord);
        //解析用户信息
        String cardTypeStr = boardHandler.analysisQznnCardType(cardType);

        resultMap.put("betMultiple", betMultipleStr);
        resultMap.put("cardRecord", cardRecordList);
        resultMap.put("cardType", cardTypeStr);
        return resultMap;
    }


    @Override
    public IPage<Map<String, Object>> queryFqzsBoardPage(Page page, BoardDto boardDto) {
        try {
            /*String boardTableName = boardDto != null && StrUtil.isNotEmpty(boardDto.getQueryTime()) ?
                    TableNameConstant.FQZSBOARD_TABLENAME +
                            DateUtil.format(DateUtil.parse(boardDto.getQueryTime(), DATE_FORMAT), THE_DATE_FORMAT) :
                    TableNameConstant.FQZSBOARD_VIEWNAME;*/
            assert boardDto != null;
            Integer start = null, end = null;
            Integer userId = null;
            userId = StringUtils.isNumeric(boardDto.getAccountId()) ? Integer.parseInt(boardDto.getAccountId()) : null;
            if (StringUtils.isNotBlank(boardDto.getQueryTime())) {
                start = TimeUtil.stringToUnixTimeStamp(boardDto.getQueryTime(), DATE_FORMAT);
                end = start + SECONDS_OF_DAY - 1;
            }
            return baseMapper.queryFqzsBoard(page, boardDto.getChannelNo(), userId, boardDto.getRecordId(),
                    start, end);
        } catch (Exception e) {
            List<Map<String, Object>> resultList = new ArrayList<>();
            return page.setRecords(resultList);
        }
    }


    @Override
    public Map<String, Object> queryFqzsBoardDetail(String recordId, String userId, String queryTime) {
        //Date queryDate = DateUtil.parse(queryTime, FULL_DATE_TIME_FORMAT);
        //String boardTableName = TableNameConstant.FQZSBOARD_TABLENAME + DateUtil.format(queryDate, THE_DATE_FORMAT);

        DateTime start = null, end = null;
        if (StringUtils.isNotBlank(queryTime)) {
            start = DateTime.parse(queryTime, DateTimeFormat.forPattern(FULL_DATE_TIME_FORMAT)).withDayOfMonth(1).withTime(0, 0, 0, 0);
            end = start.plusMonths(1).minusSeconds(1);
        }

        Map<String, Object> boardMap = baseMapper.getFqzsBoardById(recordId, StringUtils.isNumeric(userId) ? Integer.parseInt(userId) : null,
                start == null ? null : Long.valueOf(start.toDate().getTime() / 1000).intValue()
                , end == null ? null : Long.valueOf(end.toDate().getTime() / 1000).intValue());

        Integer rewardType = ObjectUtil.isNotNull(boardMap.get("rewardType")) ? (Integer) boardMap.get("rewardType") : 0;
        String awardBetArea = ObjectUtil.isNotNull(boardMap.get("awardBetArea")) ? (String) boardMap.get("awardBetArea") : "";
        String areaTimes = ObjectUtil.isNotNull(boardMap.get("areaTimes")) ? (String) boardMap.get("areaTimes") : "";
        String playerBet = ObjectUtil.isNotNull(boardMap.get("playerBet")) ? (String) boardMap.get("playerBet") : "";

        BoardHandler boardHandler = new BoardHandler();
        Map<String, Object> resultMap = new HashMap<>();

        String rewardTypeStr = boardHandler.analysisFqzsRewardType(rewardType);
        List<String> awardBetAreaList = boardHandler.analysisFqzsAwardBetArea(awardBetArea);
        List<Map<String, Object>> areaTimesList = boardHandler.analysisFqzsAwardAreaTimes(areaTimes);
        List<Map<String, Object>> playerBetList = boardHandler.analysisFqzsPlayerBet(playerBet);

        resultMap.put("rewardType", rewardTypeStr);
        resultMap.put("awardBetArea", awardBetAreaList);
        resultMap.put("areaTimes", areaTimesList);
        resultMap.put("playerBet", playerBetList);
        return resultMap;
    }


    @Override
    public IPage<Map<String, Object>> queryBcbmBoardPage(Page page, BoardDto boardDto) {
        try {
            /*String boardTableName = boardDto != null && StrUtil.isNotEmpty(boardDto.getQueryTime()) ?
                    TableNameConstant.BCBMBOARD_TABLENAME +
                            DateUtil.format(DateUtil.parse(boardDto.getQueryTime(), DATE_FORMAT), THE_DATE_FORMAT) :
                    TableNameConstant.BCBMBOARD_VIEWNAME;*/
            Integer start = null, end = null;
            Integer userId = null;
            if (boardDto != null) {
                userId = StringUtils.isNumeric(boardDto.getAccountId()) ? Integer.parseInt(boardDto.getAccountId()) : null;
                if (StringUtils.isNotBlank(boardDto.getQueryTime())) {
                    start = TimeUtil.stringToUnixTimeStamp(boardDto.getQueryTime(), DATE_FORMAT);
                    end = start + SECONDS_OF_DAY - 1;
                }
            }
            return baseMapper.queryBcbmBoard(page, boardDto.getChannelNo(),
                    userId, boardDto.getRecordId(), start, end);
        } catch (Exception e) {
            List<Map<String, Object>> resultList = new ArrayList<>();
            return page.setRecords(resultList);
        }
    }


    @Override
    public Map<String, Object> queryBcbmBoardDetail(String recordId, String userId, String queryTime) {
        //Date queryDate = DateUtil.parse(queryTime, FULL_DATE_TIME_FORMAT);
        DateTime start = null, end = null;
        if (StringUtils.isNotBlank(queryTime)) {
            start = DateTime.parse(queryTime, DateTimeFormat.forPattern(FULL_DATE_TIME_FORMAT)).withDayOfMonth(1).withTime(0, 0, 0, 0);
            end = start.plusMonths(1).minusSeconds(1);
        }

        Map<String, Object> boardMap = baseMapper.getBcbmBoardById(recordId, StringUtils.isNumeric(userId) ? Integer.parseInt(userId) : null,
                start == null ? null : Long.valueOf(start.toDate().getTime() / 1000).intValue()
                , end == null ? null : Long.valueOf(end.toDate().getTime() / 1000).intValue());

        Integer rewardType = ObjectUtil.isNotNull(boardMap.get("rewardType")) ? (Integer) boardMap.get("rewardType") : 0;
        String awardBetArea = ObjectUtil.isNotNull(boardMap.get("awardBetArea")) ? (String) boardMap.get("awardBetArea") : "";
        String areaTimes = ObjectUtil.isNotNull(boardMap.get("areaTimes")) ? (String) boardMap.get("areaTimes") : "";
        String playerBet = ObjectUtil.isNotNull(boardMap.get("playerBet")) ? (String) boardMap.get("playerBet") : "";

        BoardHandler boardHandler = new BoardHandler();
        Map<String, Object> resultMap = new HashMap<>();

        String rewardTypeStr = boardHandler.analysisBcbmRewardType(rewardType);
        List<String> list = boardHandler.analysisBcbmAwardBetArea(awardBetArea);
        List<Map<String, Object>> awardList = boardHandler.analysisBcbmAwardAreaTimes(areaTimes);
        List<Map<String, Object>> betList = boardHandler.analysisBcbmPlayerBet(playerBet);

        resultMap.put("rewardType", rewardTypeStr);
        resultMap.put("awardBetArea", list);
        resultMap.put("areaTimes", awardList);
        resultMap.put("playerBet", betList);
        return resultMap;
    }
}
