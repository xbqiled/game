package com.youwan.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.report.api.entity.Rechargeplatformorder;
import org.apache.ibatis.annotations.Param;

/**
 * 充值日志(平台)
 *
 * @author code generator
 * @date 2019-01-27 16:14:56
 */
public interface RechargeplatformorderMapper extends BaseMapper<Rechargeplatformorder> {
     IPage queryPageList(Page page,
                         @Param("userId") Integer userId,
                         @Param("startTime") Integer startTime,
                         @Param("endTime") Integer endTime);
}
