package com.youwan.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.report.api.entity.Gamerebate;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-02 12:18:56
 */
public interface GamerebateService extends IService<Gamerebate> {

    IPage<Map<String, Object>> queryGamerebatePage(Page page, String accountId, String channelNo);

}
