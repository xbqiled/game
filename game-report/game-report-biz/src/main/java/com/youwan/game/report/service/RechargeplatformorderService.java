package com.youwan.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.report.api.dto.QueryDto;
import com.youwan.game.report.api.entity.Rechargeplatformorder;


/**
 * 充值日志(平台)
 *
 * @author code generator
 * @date 2019-01-27 16:14:56
 */
public interface RechargeplatformorderService extends IService<Rechargeplatformorder> {

    IPage queryPageList(Page page, QueryDto queryDto);

}
