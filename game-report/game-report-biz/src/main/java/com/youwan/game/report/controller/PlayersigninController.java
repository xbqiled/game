package com.youwan.game.report.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.common.core.util.R;
import com.youwan.game.report.api.dto.QueryDto;
import com.youwan.game.report.service.PlayersigninService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-03 14:15:54
 */
@RestController
@AllArgsConstructor
@RequestMapping("/playersignin")
public class PlayersigninController {

  private final PlayersigninService playersigninService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param queryDto VIEW
   * @return
   */
  @GetMapping("/page")
  public R getPlayersignPage(Page page, QueryDto queryDto) {
    return new R<>(playersigninService.querySignInPageList(page, queryDto));
  }

}
