package com.youwan.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.report.api.dto.QueryDto;
import com.youwan.game.report.api.entity.Logingamerecord;


/**
 * 
 *
 * @author code generator
 * @date 2019-01-25 17:23:37
 */
public interface LogingamerecordService extends IService<Logingamerecord> {

    IPage queryPageList(Page page, QueryDto queryDto);

}
