package com.youwan.game.report.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.report.api.entity.Platformonlinecount;
import com.youwan.game.report.mapper.PlatformonlinecountMapper;
import com.youwan.game.report.service.PlatformonlinecountService;
import org.springframework.stereotype.Service;

/**
 * 平台实时在线记录
 *
 * @author code generator
 * @date 2019-01-25 13:12:14
 */
@Service("platformonlinecountService")
public class PlatformonlinecountServiceImpl extends ServiceImpl<PlatformonlinecountMapper, Platformonlinecount> implements PlatformonlinecountService {

}
