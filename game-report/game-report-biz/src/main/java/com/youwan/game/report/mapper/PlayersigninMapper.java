package com.youwan.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.report.api.entity.Playersignin;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-03 14:15:54
 */
public interface PlayersigninMapper extends BaseMapper<Playersignin> {

    IPage<Map<String, Object>> querySginPage(Page page,
                                                   @Param("channelId") String channelId,
                                                   @Param("userId") Integer userId,
                                                   @Param("startTime") Integer startTime, @Param("endTime") Integer endTime);

}
