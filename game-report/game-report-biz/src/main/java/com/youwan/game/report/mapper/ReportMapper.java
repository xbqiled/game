package com.youwan.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.report.api.entity.Report;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-02-05 14:02:39
 */
public interface ReportMapper extends BaseMapper<Report> {

    /**
     * <B>查询数据信息</B>
     *
     * @param page
     * @param channelId
     * @param startTime
     * @param endTime
     * @return
     */
    IPage<List<Map<String, Object>>> queryUserStatisDate(Page page, @Param("channelId") String channelId, @Param("userId") String userId, @Param(
            "startTime") String startTime, @Param("endTime") String endTime);


    /**
     * <B>获取平台日报表信息</B>
     *
     * @return
     */
    List<Report> queryEcharReport();

    /**
     * <B>获取指定时间段以日和渠道编码为维度的统计数据</B>
     *
     * @param page
     * @param startTime
     * @param endTime
     * @return
     */
    IPage<List<Report>> getDayReportPage(Page page, @Param("channelId") String channelId, @Param("startTime") String startTime,
                                         @Param("endTime") String endTime);


    /**
     * <B>获取指定时间段以周和渠道编码为维度的统计数据</B>
     *
     * @param page
     * @param weekTime
     * @return
     */
    IPage<List<Map<String, Object>>> getWeekReportPage(Page page, @Param("channelId") String channelId, @Param("weekTime") String weekTime);


    /**
     * <B>获取指定时间段以月和渠道编码为维度的统计数据</B>
     *
     * @param page
     * @param monthTime
     * @return
     */
    IPage<List<Map<String, Object>>> getMonthReportPage(Page page, @Param("channelId") String channelId, @Param("monthTime") String monthTime);


    /**
     * <B>获取用户账变信息</B>
     *
     * @param page
     * @param userId
     * @return
     */
    IPage<List<Map<String, Object>>> queryUserAccountPage(Page page, @Param("userId") String userId, @Param("itemId") String itemId);


    /**
     * <B>获取用户财富榜信息</B>
     *
     * @param page
     * @param channelId
     * @param userId
     * @param startTime
     * @param endTime
     * @return
     */
    IPage<Map<String, Object>> queryWealthRankingPage(Page page,
                                                      @Param("channelId") String channelId,
                                                      @Param("userId") Integer userId,
                                                      @Param("startTime") Integer startTime,
                                                      @Param("endTime") Integer endTime);

    List<Integer> queryWealthRankCountList(@Param("channelId") String channelId,
                                 @Param("userId") Integer userId,
                                 @Param("startTime") Integer startTime,
                                 @Param("endTime") Integer endTime);
    /**
     * <B>获取斗地主牌局信息</B>
     *
     * @param page
     * @param channelId
     * @param recordId
     * @param userId
     * @return
     */
    IPage<Map<String, Object>> queryDdzBoardPage(Page page,
                                                 @Param("channelId") String channelId,
                                                 @Param("recordId") String recordId,
                                                 @Param("userId") Integer userId,
                                                 @Param("startTime") Integer startTime,
                                                 @Param("endTime") Integer endTime);


    /**
     * <B>获取龙虎大战牌局信息</B>
     *
     * @param page
     * @param channelId
     * @param recordId
     * @param userId
     * @return
     */
    IPage<Map<String, Object>> queryLhBoardPage(Page page,
                                                @Param("channelId") String channelId,
                                                @Param("recordId") String recordId,
                                                @Param("userId") Integer userId,
                                                @Param("startTime") Integer startTime,
                                                @Param("endTime") Integer endTime);


    /**
     * <B>获取捕鱼牌局信息</B>
     *
     * @param page
     * @param channelId
     * @param recordId
     * @param userId
     * @return
     */
    IPage<Map<String, Object>> queryByBoardPage(Page page,
                                                      @Param("channelId") String channelId,
                                                      @Param("recordId") String recordId,
                                                      @Param("userId") Integer userId,
                                                      @Param("startTime") Integer startTime,
                                                      @Param("endTime") Integer endTime);


    /**
     * <B>获取牛牛牌局信息</B>
     *
     * @param page
     * @param channelId
     * @param recordId
     * @param userId
     * @return
     */
    IPage<Map<String, Object>> queryNnBoardPage(Page page,
                                                      @Param("channelId") String channelId,
                                                      @Param("recordId") String recordId,
                                                      @Param("userId") Integer userId,
                                                      @Param("startTime") Integer startTime,
                                                      @Param("endTime") Integer endTime);


    /**
     * <B>获取百家乐牌局信息</B>
     *
     * @param page
     * @param channelId
     * @param recordId
     * @param userId
     * @return
     */
    IPage<Map<String, Object>> queryBjlBoardPage(Page page,
                                                 @Param("channelId") String channelId,
                                                 @Param("recordId") String recordId,
                                                 @Param("userId") Integer userId,
                                                 @Param("startTime") Integer startTime,
                                                 @Param("endTime") Integer endTime);


    /**
     * <B>获取水果机牌局信息</B>
     *
     * @param page
     * @param channelId
     * @param recordId
     * @param userId
     * @return
     */
    IPage<Map<String, Object>> querySgjBoardPage(Page page,
                                                 @Param("channelId") String channelId,
                                                 @Param("recordId") String recordId,
                                                 @Param("userId") Integer userId,
                                                 @Param("startTime") Integer startTime,
                                                 @Param("endTime") Integer endTime);

    /**
     * <B>获取水果机牌局信息</B>
     *
     * @param page
     * @param channelId
     * @param recordId
     * @param userId
     * @return
     */
    IPage<Map<String, Object>> queryTwentyOneBoardPage(Page page,
                                                       @Param("channelId") String channelId,
                                                       @Param("recordId") String recordId,
                                                       @Param("userId") Integer userId,
                                                       @Param("startTime") Integer startTime,
                                                       @Param("endTime") Integer endTime);

    /**
     * <B>获取跑得快牌局信息</B>
     *
     * @param page
     * @param recordId
     * @param userId
     * @return
     */
    IPage<Map<String, Object>> queryPdkBoardPage(Page page,
                                                 @Param("channelId") String channelId,
                                                 @Param("recordId") String recordId,
                                                 @Param("userId") Integer userId,
                                                 @Param("startTime") Integer startTime,
                                                 @Param("endTime") Integer endTime);


    /**
     * <B>获取比花色牌局信息</B>
     *
     * @param page
     * @param channelId
     * @param recordId
     * @param userId
     * @return
     */
    IPage<Map<String, Object>> queryBhsBoardPage(Page page,
                                                 @Param("channelId") String channelId,
                                                 @Param("recordId") String recordId,
                                                 @Param("userId") Integer userId,
                                                 @Param("startTime") Integer startTime,
                                                 @Param("endTime") Integer endTime);


    /**
     * <B>获取炸金花牌局</B>
     *
     * @param page
     * @param channelId
     * @param recordId
     * @param userId
     * @return
     */
    IPage<Map<String, Object>> queryZjhBoardPage(Page page,
                                                 @Param("channelId") String channelId,
                                                 @Param("recordId") String recordId,
                                                 @Param("userId") Integer userId,
                                                 @Param("startTime") Integer startTime,
                                                 @Param("endTime") Integer endTime);


    /**
     * <B>获取财神到牌局</B>
     *
     * @param page
     * @param channelId
     * @param recordId
     * @param userId
     * @return
     */
    IPage<Map<String, Object>> queryCsdBoardPage(Page page,
                                                 @Param("channelId") String channelId,
                                                 @Param("recordId") String recordId,
                                                 @Param("userId") Integer userId,
                                                 @Param("startTime") Integer startTime,
                                                 @Param("endTime") Integer endTime);


    /**
     * <B>财神到牌局</B>
     *
     * @param recordId
     * @return
     */
    String queryCsdboardDetail(@Param("recordId") String recordId);


    /**
     * <B>获取红黑大战牌局</B>
     *
     * @param page
     * @param channelId
     * @param userId
     * @param recordId
     * @return
     */
    IPage<Map<String, Object>> queryHhdzBoard(Page page,
                                              @Param("channelId") String channelId,
                                              @Param("userId") Integer userId,
                                              @Param("recordId") String recordId,
                                              @Param("startTime") Integer startTime,
                                              @Param("endTime") Integer endTime);


    /**
     * <B>获取红黑大战的牌局</B>
     *
     * @param recordId
     * @return
     */
    List<Map<String, Object>> queryHhBoardDetail(@Param("recordId") String recordId);


    /**
     * <B>获取红包接龙牌局</B>
     *
     * @param page
     * @param channelId
     * @param userId
     * @param recordId
     * @return
     */
    IPage<Map<String, Object>> queryHbjlBoard(Page page,
                                              @Param("channelId") String channelId,
                                              @Param("userId") Integer userId,
                                              @Param("recordId") String recordId,
                                              @Param("startTime") Integer startTime,
                                              @Param("endTime") Integer endTime);


    IPage<Map<String, Object>> queryQznnBoard(Page page,
                                              @Param("channelId") String channelId,
                                              @Param("userId") Integer userId,
                                              @Param("recordId") String recordId,
                                              @Param("startTime") Integer startTime,
                                              @Param("endTime") Integer endTime);


    Map<String, Object> getQznnBoardById(@Param("recordId") String recordId,
                                         @Param("userId") Integer userId,
                                         @Param("startTime") Integer startTime,
                                         @Param("endTime") Integer endTime);


    IPage<Map<String, Object>> queryFqzsBoard(Page page,
                                              @Param("channelId") String channelId,
                                              @Param("userId") Integer userId,
                                              @Param("recordId") String recordId,
                                              @Param("startTime") Integer startTime,
                                              @Param("endTime") Integer endTime);


    Map<String, Object> getFqzsBoardById(@Param("recordId") String recordId,
                                         @Param("userId") Integer userId,
                                         @Param("startTime") Integer startTime,
                                         @Param("endTime") Integer endTime);


    IPage<Map<String, Object>> queryBcbmBoard(Page page,
                                              @Param("channelId") String channelId,
                                              @Param("userId") Integer userId,
                                              @Param("recordId") String recordId,
                                              @Param("startTime") Integer startTime,
                                              @Param("endTime") Integer endTime);


    Map<String, Object> getBcbmBoardById(@Param("recordId") String recordId,
                                         @Param("userId") Integer userId,
                                         @Param("startTime") Integer startTime,
                                         @Param("endTime") Integer endTime);

}
