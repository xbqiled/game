package com.youwan.game.report.controller;


import com.youwan.game.common.core.util.R;
import com.youwan.game.report.api.dto.GmActionDto;
import com.youwan.game.report.api.dto.MaintDto;
import com.youwan.game.report.service.GameroomService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@AllArgsConstructor
@RequestMapping("/maintenance")
public class MaintenanceController {


    private final GameroomService gameroomService;

    /**
     * <B>调用接口查询游戏房间信息</B>
     * @return
     */
    @GetMapping("/game/page")
    public R getGamePage(String status) {
        return  new R<>(gameroomService.getGamePage(status));
    }


    /**
     * <B>处理游戏维护</B>
     */
    @GetMapping("/game/maint")
    @PreAuthorize("@pms.hasPermission('maint_game_handle')")
    public R doGameMaint(MaintDto maintDto) {
        Boolean result = gameroomService.gameMaint(maintDto);
        return  result ? new R<>() :  new R<>().setCode(1).setMsg("游戏维护失败!");
    }


    /**
     * <B>调用接口查询平台信息</B>
     * @return
     */
    @GetMapping("/platform/page")
    public R getPlatformPage() {
        return new R<>(gameroomService.getPlatformPage());
    }

    /**
     *<B>处理平台维护</B>
     */
    @GetMapping("/platform/maint")
    @PreAuthorize("@pms.hasPermission('maint_platform_handle')")
    public R doPlatformMaint(MaintDto maintDto) {
        Boolean result = gameroomService.platformMaint(maintDto);
        return  result ? new R<>() :  new R<>().setCode(1).setMsg("平台维护失败!");
    }

    /**
     * <B>处理 GM命令</B>
     * @return
     */
    @GetMapping("/gmaction")
    public R doGmAction(GmActionDto action) {
        Boolean result = gameroomService.gmAction(action);
        return  result ? new R<>() :  new R<>().setCode(1).setMsg("命令执行失败!");
    }
}
