package com.youwan.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.report.api.dto.QueryDto;
import com.youwan.game.report.api.entity.Usertreasurechange;

import java.util.List;
import java.util.Map;


/**
 *
 *
 * @author code generator
 * @date 2019-01-25 13:11:34
 */
public interface TreasurechangeService extends IService<Usertreasurechange> {

    IPage<Map<String, Object>> queryUserAccountPage(Page page, QueryDto queryDto);
}
