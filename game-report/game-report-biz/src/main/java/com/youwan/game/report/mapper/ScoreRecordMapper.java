package com.youwan.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.report.api.entity.ScoreRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-25 12:22:25
 */
public interface ScoreRecordMapper extends BaseMapper<ScoreRecord> {

    IPage<Map<String, Object>> queryPageList(Page page,
                                             @Param("channelId") String channelId,
                                             @Param("type") String type,
                                             @Param("userId") Integer userId,
                                             @Param("startTime") Integer startTime,
                                             @Param("endTime") Integer endTime);

}
