package com.youwan.game.report.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.common.core.constant.TableNameConstant;
import com.youwan.game.common.core.util.TimeUtil;
import com.youwan.game.report.api.dto.DantiaomultiplyDto;
import com.youwan.game.report.api.entity.Dantiaomultiply;
import com.youwan.game.report.mapper.DantiaomultiplyMapper;
import com.youwan.game.report.service.DantiaomultiplyService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.youwan.game.common.core.constant.CommonConstant.SECONDS_OF_DAY;
import static com.youwan.game.common.core.constant.ConfigConstant.*;

/**
 * @author code generator
 * @date 2020-04-18 12:19:29
 */
@Service("dantiaomultiplyService")
public class DantiaomultiplyServiceImpl extends ServiceImpl<DantiaomultiplyMapper, Dantiaomultiply> implements DantiaomultiplyService {

    @Override
    public IPage<Map<String, Object>> queryMultiplyPageList(Page page, DantiaomultiplyDto dantiaomultiplyDto) {
        try {
            /*String boardTableName = dantiaomultiplyDto != null && StrUtil.isNotBlank(dantiaomultiplyDto.getStartTime())
                    && StrUtil.isNotBlank(dantiaomultiplyDto.getEndTime())
                    && dantiaomultiplyDto.getStartTime().equals(dantiaomultiplyDto.getEndTime()) ?
                    TableNameConstant.DANTIAOMULTIPLY_TABLENAME +
                            DateUtil.format(DateUtil.parse(dantiaomultiplyDto.getStartTime(), DATE_FORMAT),
                                    THE_DATE_FORMAT) : TableNameConstant.DANTIAOMULTIPLY_VIEW;*/
            Integer start = null, end = null;
            if (dantiaomultiplyDto != null) {
                start = StrUtil.isNotBlank(dantiaomultiplyDto.getStartTime()) ? TimeUtil.stringToUnixTimeStamp(dantiaomultiplyDto.getStartTime(),
                        DATE_FORMAT) : null;
                end = StrUtil.isNotBlank(dantiaomultiplyDto.getEndTime()) ? TimeUtil.stringToUnixTimeStamp(dantiaomultiplyDto.getEndTime(),
                        DATE_FORMAT) + SECONDS_OF_DAY : null;
            }
            return baseMapper.queryMultiplyPageList(page, dantiaomultiplyDto == null ? null : dantiaomultiplyDto.getRecordId(), start, end);

        } catch (Exception e) {
            List<Map<String, Object>> resultList = new ArrayList<>();
            return page.setRecords(resultList);
        }
    }
}
