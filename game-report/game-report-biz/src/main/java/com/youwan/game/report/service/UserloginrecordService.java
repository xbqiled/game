package com.youwan.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.report.api.dto.QueryDto;
import com.youwan.game.report.api.entity.Userloginrecord;


/**
 * 
 *
 * @author code generator
 * @date 2019-01-25 11:56:48
 */
public interface UserloginrecordService extends IService<Userloginrecord> {

    IPage queryPageList(Page page, Userloginrecord userloginrecord);

    IPage queryLoginReport(Page page,  QueryDto queryDto);
}
