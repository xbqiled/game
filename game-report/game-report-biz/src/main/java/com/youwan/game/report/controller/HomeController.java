package com.youwan.game.report.controller;


import cn.hutool.core.util.StrUtil;
import com.youwan.game.common.core.util.R;
import com.youwan.game.report.api.entity.Report;
import com.youwan.game.report.service.ReportService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@AllArgsConstructor
@RequestMapping("/home")
public class HomeController {

    private final ReportService reportService;

    @RequestMapping("/echarReport")
    public R eChart() throws IOException {
        List<Report> reportList = reportService.queryEcharReport();

        //分解出图表信息
        List<String> timeList = new ArrayList<>();
        List<String> typeList = new ArrayList<>();
        List<String> zcrsList = new ArrayList<>();

        List<String> xzyhList = new ArrayList<>();
        List<String> dlrsList = new ArrayList<>();
        List<String> czrsList = new ArrayList<>();

        typeList.add("充值人数");
        typeList.add("登录人数");
        typeList.add("注册人数");
        typeList.add("游戏人数");

        if (reportList != null && reportList.size() > 0) {
            for (Report report : reportList) {

                String recordtime = "";
                //统计时间
                if (report.getRecordtime() != null &&  StrUtil.isBlank(report.getRecordtime())) {
                    recordtime = "";
                } else {
                    recordtime = report.getRecordtime();
                }
                timeList.add(recordtime);

                //注册人数
                Long regcount = new Long(0);
                if (report.getRegcount() != null && !report.getRegcount().equals("")) {
                    regcount =  report.getRegcount();
                }
                zcrsList.add(regcount.toString());

                //游戏人数
                Long newgamecount = new Long(0);
                if (report.getLogingamecount() != null && !report.getLogingamecount().equals("")) {
                    newgamecount = report.getLogingamecount();
                }
                xzyhList.add(newgamecount.toString());

                //登录人数
                Long logincount = new Long(0);
                if (report.getLogincount() != null && !report.getLogincount().equals("")) {
                    logincount = report.getLogincount();
                }
                dlrsList.add(logincount.toString());

                //充值人数
                Long rechargecount = new Long(0);
                if (report.getRechargecount() != null && !report.getRechargecount().equals("")) {
                    rechargecount = report.getRechargecount();
                }
                czrsList.add(rechargecount.toString());
            }
        }

        //充值折线统计
        List<Map<String, Object>> lineResultList = new ArrayList<>();
        //柱图统计
        List<Map<String, Object>> barResultList = new ArrayList<>();

        for (String str : typeList) {
            Map<String, Object> seriesMap = new HashMap<>();
            Map<String, Object> seriesBarMap = new HashMap<>();

            seriesMap.put("name", str);
            seriesMap.put("type", "line");
            seriesMap.put("stack", "数量");

            seriesBarMap.put("name", str);
            seriesBarMap.put("type", "bar");
            seriesBarMap.put("stack", "数量");

            if (str.equals("注册人数")) {
                seriesMap.put("data", zcrsList);
                seriesBarMap.put("data", zcrsList);
            } else if (str.equals("游戏人数")) {
                seriesMap.put("data", xzyhList);
                seriesBarMap.put("data", xzyhList);
            } else if (str.equals("登录人数")) {
                seriesMap.put("data", dlrsList);
                seriesBarMap.put("data", dlrsList);
            } else if (str.equals("充值人数")) {
                seriesMap.put("data", czrsList);
                seriesBarMap.put("data", czrsList);
            }
            lineResultList.add(seriesMap);
            barResultList.add(seriesBarMap);
        }

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("xDataList", timeList);
        resultMap.put("typeList", typeList);

        resultMap.put("lineSeries", lineResultList);
        resultMap.put("barSeries", barResultList);
        return  new R<>().setData(resultMap);
    }
}
