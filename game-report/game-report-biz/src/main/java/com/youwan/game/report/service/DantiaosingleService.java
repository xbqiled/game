package com.youwan.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.report.api.dto.DantiaosingleDto;
import com.youwan.game.report.api.entity.Dantiaosingle;

import java.util.List;
import java.util.Map;


/**
 * VIEW
 *
 * @author code generator
 * @date 2020-04-18 12:19:42
 */
public interface DantiaosingleService extends IService<Dantiaosingle> {


    IPage<Map<String, Object>> querySinglePageList(Page page, DantiaosingleDto dantiaosingleDto);

}
