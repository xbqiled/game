package com.youwan.game.report.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.common.core.util.R;
import com.youwan.game.report.api.dto.ParameterDto;
import com.youwan.game.report.service.RedblackpoolService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-03 13:55:49
 */
@RestController
@AllArgsConstructor
@RequestMapping("/redblackpool")
public class RedblackpoolController {

  private final RedblackpoolService redblackpoolService;

  /**
   * 分页查询
   * @param page 分页对象
   * @return
   */
  @GetMapping("/page")
  public R getHhdzPoolPage(Page page, ParameterDto parameterDto) {
    return new R<>(redblackpoolService.queryHhdzPoolPageList(page, parameterDto));
  }
}
