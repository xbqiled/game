package com.youwan.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.report.api.entity.Gameroom;
import org.apache.ibatis.annotations.Param;

/**
 * 
 *
 * @author code generator
 * @date 2019-03-15 20:02:39
 */
public interface GameroomMapper extends BaseMapper<Gameroom> {

    Gameroom findGameRoomById( @Param("typeId") String typeId);

}
