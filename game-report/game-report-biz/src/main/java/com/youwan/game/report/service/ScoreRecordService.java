package com.youwan.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.report.api.dto.ScoreDto;
import com.youwan.game.report.api.entity.ScoreRecord;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-25 12:22:25
 */
public interface ScoreRecordService extends IService<ScoreRecord> {

    IPage queryPageList(Page page, ScoreDto scoreDto);

}
