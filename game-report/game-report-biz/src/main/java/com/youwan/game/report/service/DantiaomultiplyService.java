package com.youwan.game.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.report.api.dto.DantiaomultiplyDto;
import com.youwan.game.report.api.entity.Dantiaomultiply;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2020-04-18 12:19:29
 */
public interface DantiaomultiplyService extends IService<Dantiaomultiply> {

    IPage<Map<String, Object>> queryMultiplyPageList(Page page, DantiaomultiplyDto dantiaomultiplyDto);

}
