package com.youwan.game.report.handler;

import lombok.Data;

@Data
public class TwentyOneCardRecord {

    Integer area;
    Long bet;
    Integer [] cards;

    Integer dim;
    Integer point;
    Integer tax;

    Integer insure;
    Integer win;

}
