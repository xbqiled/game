package com.youwan.game.report.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.report.api.entity.Platformgamecount;
import com.youwan.game.report.mapper.PlatformgamecountMapper;
import com.youwan.game.report.service.PlatformgamecountService;
import org.springframework.stereotype.Service;

/**
 * 实时游戏在线记录
 *
 * @author code generator
 * @date 2019-01-25 19:13:50
 */
@Service("platformgamecountService")
public class PlatformgamecountServiceImpl extends ServiceImpl<PlatformgamecountMapper, Platformgamecount> implements PlatformgamecountService {

}
