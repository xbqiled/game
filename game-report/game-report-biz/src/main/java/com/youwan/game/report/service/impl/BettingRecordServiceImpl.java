package com.youwan.game.report.service.impl;

import com.alibaba.fastjson.JSON;
import com.youwan.game.common.core.betting.entity.*;
import com.youwan.game.common.data.core.Page;
import com.youwan.game.common.data.core.XSort;
import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import com.youwan.game.common.data.elasticsearch.operations.SearchOperation;
import com.youwan.game.report.api.dto.betting.*;
import com.youwan.game.report.mapper.platform.MobileBindMapper;
import com.youwan.game.report.mapper.xbqp.AccountInfoMapper;
import com.youwan.game.report.service.BettingRecordService;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-01.
 */
@Service
public class BettingRecordServiceImpl implements BettingRecordService {

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    private MobileBindMapper mobileBindMapper;

    @Autowired
    private AccountInfoMapper accountInfoMapper;

    @Override
    public Page<BcLotteryOrder> queryLotteryOrders(LotteryQueryDto queryDto) {
        BoolQueryBuilder query = QueryBuilders.boolQuery();
        Map<String, Object> tpInfo = null;
        if (queryDto.getStartTime() != null) {
            RangeQueryBuilder rangeQuery = rangeQuery("createTime").gte(queryDto.getStartTime());

            if (queryDto.getEndTime() != null) {
                rangeQuery.lte(queryDto.getEndTime());
            }
            query.must(rangeQuery);
        }
        if (StringUtils.isNotBlank(queryDto.getLotCode())) {
            query.must(termQuery("lotCode.keyword", queryDto.getLotCode()));
        }
        if (StringUtils.isNotBlank(queryDto.getQiHao())) {
            query.must(termQuery("qiHao.keyword", queryDto.getQiHao()));
        }
        if (queryDto.getStatus() != null) {
            query.must(termQuery("status", queryDto.getStatus()));
        }
        if (StringUtils.isNotBlank(queryDto.getOrderId())) {
            query.must(termQuery("_id", queryDto.getOrderId()));
        }
        if (StringUtils.isNotBlank(queryDto.getBetIp())) {
            query.must(matchQuery("betIp", queryDto.getBetIp()));
        }
        if (StringUtils.isNotBlank(queryDto.getChannelId())) {
            Integer cpStationId = mobileBindMapper.getCpStationIdByChannel(queryDto.getChannelId());
            if (cpStationId == null)
                cpStationId = 0;
            query.must(termQuery("stationId", cpStationId));
        }
        if (queryDto.getUserId() != null) {
            tpInfo = accountInfoMapper.get3rdAccountId(queryDto.getUserId());
            Integer tpId = tpInfo == null ? 0 : (Integer) tpInfo.get("tpId");
            query.must(termQuery("accountId", tpId));
        }
        Page<BcLotteryOrder> ret = elasticsearchTemplate.searchForPage(
                new SearchOperation<>("bc_lottery_order", query, BcLotteryOrder.class)
                        .setFrom(queryDto.getOffset())
                        .setSize(queryDto.getLimit())
                        .setSort(XSort.desc("createTime"))
        );
        //ret.getRecords().stream().forEach(order -> order.setChannelId("1020001001"));
        if (!ret.getRecords().isEmpty()) {
            Set<Integer> tpIds =
                    ret.getRecords().stream().map(BcLotteryOrder::getAccountId).distinct().mapToInt(Long::intValue).boxed().collect(Collectors.toSet());
            Set<Integer> stationIds =
                    ret.getRecords().stream().map(BcLotteryOrder::getStationId).distinct().mapToInt(Long::intValue).boxed().collect(Collectors.toSet());

            Map<Integer, Map<String, Object>> umap = new HashMap<>();

            if (tpInfo == null) {
                List<Map<String, Object>> users = accountInfoMapper.queryUserBy3rdIds(tpIds);
                users.forEach(user -> umap.put((Integer) user.get("tpId"), user));
            } else {
                umap.put((Integer) tpInfo.get("tpId"), tpInfo);
            }

            Map<Integer, Map<String, Object>> bmap = new HashMap<>();
            if (StringUtils.isBlank(queryDto.getChannelId())) {
                List<Map<String, Object>> binds = mobileBindMapper.queryChannelIdsBy3rdIds(stationIds);
                binds.forEach(bind -> bmap.put((Integer) bind.get("cp_station_id"), bind));
            }

            ret.getRecords().forEach(order -> {
                Long tpId = order.getAccountId();
                Long stationId = order.getStationId();
                Map<String, Object> user = umap.get(tpId.intValue());
                if (user != null) {
                    order.setUserName((String) user.get("nickName"));
                    order.setUserId((Long) user.get("userId"));
                }
                if (StringUtils.isNotBlank(queryDto.getChannelId())) {
                    order.setChannelId(queryDto.getChannelId());
                } else {
                    Map<String, Object> bind = bmap.get(stationId.intValue());
                    if (bind != null) {
                        order.setChannelId((String) bind.get("channel_id"));
                    }
                }

            });
        }
        return ret;
    }

    @Override
    public Page<CrownSportBettingOrder> queryCrownSportOrders(CrownSportQueryDto queryDto) {
        BoolQueryBuilder query = QueryBuilders.boolQuery();
        Map<String, Object> tpInfo = null;
        if (queryDto.getStartTime() != null) {
            RangeQueryBuilder rangeQuery = rangeQuery("bettingDate").gte(queryDto.getStartTime());
            if (queryDto.getEndTime() != null) {
                rangeQuery.lte(queryDto.getEndTime());
            }
            query.must(rangeQuery);
        }
        if (StringUtils.isNotBlank(queryDto.getChannelId())) {
            Integer cpStationId = mobileBindMapper.getCpStationIdByChannel(queryDto.getChannelId());
            if (cpStationId == null)
                cpStationId = 0;
            query.must(termQuery("stationId", cpStationId));
        }
        if (queryDto.getUserId() != null) {
            tpInfo = accountInfoMapper.get3rdAccountId(queryDto.getUserId());
            Integer tpId = tpInfo == null ? 0 : (Integer) tpInfo.get("tpId");
            query.must(termQuery("memberId", tpId));
        }
        if (queryDto.getSportType() != null) {
            query.must(termQuery("sportType", queryDto.getSportType()));
        }
        if (queryDto.getBettingStatus() != null) {
            query.must(termQuery("bettingStatus", queryDto.getBettingStatus()));
        }
        if (queryDto.getBalance() != null) {
            query.must(termQuery("balance", queryDto.getBalance()));
        }
        if (queryDto.getResultStatus() != null) {
            query.must(termQuery("resultStatus", queryDto.getResultStatus()));
        }
        if (StringUtils.isNotBlank(queryDto.getBettingCode())) {
            query.must(termQuery("_id", queryDto.getBettingCode()));
        }
        Page<CrownSportBettingOrder> ret = elasticsearchTemplate.searchForPage(
                new SearchOperation<>("crown_sport_betting_order", query, CrownSportBettingOrder.class)
                        .setFrom(queryDto.getOffset())
                        .setSize(queryDto.getLimit())
                        .setSort(XSort.desc("bettingDate"))
        );
        if (ret.getRecords().isEmpty())
            return ret;
        Set<Integer> mermberIds =
                ret.getRecords().stream().map(CrownSportBettingOrder::getMemberId).distinct().mapToInt(Long::intValue).boxed().collect(Collectors.toSet());
        Set<Integer> stationIds =
                ret.getRecords().stream().map(CrownSportBettingOrder::getStationId).distinct().mapToInt(Long::intValue).boxed().collect(Collectors.toSet());
        Map<Integer, Map<String, Object>> umap = new HashMap<>();
        if (tpInfo == null) {
            List<Map<String, Object>> users = accountInfoMapper.queryUserBy3rdIds(mermberIds);
            users.forEach(user -> umap.put((Integer) user.get("tpId"), user));
        } else {
            umap.put((Integer) tpInfo.get("tpId"), tpInfo);
        }

        Map<Integer, Map<String, Object>> bmap = new HashMap<>();
        if (StringUtils.isBlank(queryDto.getChannelId())) {
            List<Map<String, Object>> binds = mobileBindMapper.queryChannelIdsBy3rdIds(stationIds);
            binds.forEach(bind -> bmap.put((Integer) bind.get("cp_station_id"), bind));
        }

        ret.getRecords().forEach(order -> {
            try {
                if (StringUtils.isNotBlank(order.getRemark()))
                    order.setRemark_json(JSON.parse(order.getRemark()));
            } catch (Exception e) {
                order.setRemark_json(null);
            }
            order.setRemark(null);
            Long tpId = order.getMemberId();
            Long stationId = order.getStationId();
            Map<String, Object> user = umap.get(tpId.intValue());
            if (user != null) {
                order.setUserName((String) user.get("nickName"));
                order.setUserId((Long) user.get("userId"));
            }
            if (StringUtils.isNotBlank(queryDto.getChannelId())) {
                order.setChannelId(queryDto.getChannelId());
            } else {
                Map<String, Object> bind = bmap.get(stationId.intValue());
                if (bind != null) {
                    order.setChannelId((String) bind.get("channel_id"));
                }
            }
        });
        return ret;
    }

    @Override
    public Page<ThirdPartyEGameBettingRecord> queryEGameBettingRecords(EGameQueryDto queryDto) {
        BoolQueryBuilder query = QueryBuilders.boolQuery();
        Map<String, Object> tpInfo = null;
        if (queryDto.getStartTime() != null) {
            RangeQueryBuilder rangeQuery = rangeQuery("bettingTime").gte(queryDto.getStartTime());
            if (queryDto.getEndTime() != null) {
                rangeQuery.lte(queryDto.getEndTime());
            }
            query.must(rangeQuery);
        }
        if (StringUtils.isNotBlank(queryDto.getChannelId())) {
            Integer cpStationId = mobileBindMapper.getCpStationIdByChannel(queryDto.getChannelId());
            if (cpStationId == null)
                cpStationId = 0;
            query.must(termQuery("stationId", cpStationId));
        }
        if (queryDto.getUserId() != null) {
            tpInfo = accountInfoMapper.get3rdAccountId(queryDto.getUserId());
            Integer tpId = tpInfo == null ? 0 : (Integer) tpInfo.get("tpId");
            query.must(termQuery("accountId", tpId));
        }
        if (StringUtils.isNotBlank(queryDto.getBettingCode())) {
            query.must(termQuery("bettingCode.keyword", queryDto.getBettingCode()));
        }
        if (queryDto.getType() != null) {
            query.must(termQuery("type", queryDto.getType()));
        }
        Page<ThirdPartyEGameBettingRecord> ret = elasticsearchTemplate.searchForPage(
                new SearchOperation<>("3rd_egame_betting_record", query, ThirdPartyEGameBettingRecord.class)
                        .setFrom(queryDto.getOffset())
                        .setSize(queryDto.getLimit())
                        .setSort(XSort.desc("bettingTime"))
        );
        if (ret.getRecords().isEmpty())
            return ret;
        Set<Integer> mermberIds =
                ret.getRecords().stream().map(ThirdPartyEGameBettingRecord::getAccountId).distinct().mapToInt(Long::intValue).boxed().collect(Collectors.toSet());
        Set<Integer> stationIds =
                ret.getRecords().stream().map(ThirdPartyEGameBettingRecord::getStationId).distinct().mapToInt(Long::intValue).boxed().collect(Collectors.toSet());
        Map<Integer, Map<String, Object>> umap = new HashMap<>();
        if (tpInfo == null) {
            List<Map<String, Object>> users = accountInfoMapper.queryUserBy3rdIds(mermberIds);
            users.forEach(user -> umap.put((Integer) user.get("tpId"), user));
        } else {
            umap.put((Integer) tpInfo.get("tpId"), tpInfo);
        }

        Map<Integer, Map<String, Object>> bmap = new HashMap<>();
        if (StringUtils.isBlank(queryDto.getChannelId())) {
            List<Map<String, Object>> binds = mobileBindMapper.queryChannelIdsBy3rdIds(stationIds);
            binds.forEach(bind -> bmap.put((Integer) bind.get("cp_station_id"), bind));
        }
        ret.getRecords().forEach(order -> {
            Long tpId = order.getAccountId();
            Long stationId = order.getStationId();
            Map<String, Object> user = umap.get(tpId.intValue());
            if (user != null) {
                order.setUserName((String) user.get("nickName"));
                order.setUserId((Long) user.get("userId"));
            }
            if (StringUtils.isNotBlank(queryDto.getChannelId())) {
                order.setChannelId(queryDto.getChannelId());
            } else {
                Map<String, Object> bind = bmap.get(stationId.intValue());
                if (bind != null) {
                    order.setChannelId((String) bind.get("channel_id"));
                }
            }
        });
        return ret;
    }

    @Override
    public Page<ThirdPartyLiveBettingRecord> queryLiveBettingRecords(LiveQueryDto queryDto) {
        BoolQueryBuilder query = QueryBuilders.boolQuery();
        Map<String, Object> tpInfo = null;
        if (queryDto.getStartTime() != null) {
            RangeQueryBuilder rangeQuery = rangeQuery("bettingTime").gte(queryDto.getStartTime());
            if (queryDto.getEndTime() != null) {
                rangeQuery.lte(queryDto.getEndTime());
            }
            query.must(rangeQuery);
        }
        if (StringUtils.isNotBlank(queryDto.getChannelId())) {
            Integer cpStationId = mobileBindMapper.getCpStationIdByChannel(queryDto.getChannelId());
            if (cpStationId == null)
                cpStationId = 0;
            query.must(termQuery("stationId", cpStationId));
        }
        if (queryDto.getUserId() != null) {
            tpInfo = accountInfoMapper.get3rdAccountId(queryDto.getUserId());
            Integer tpId = tpInfo == null ? 0 : (Integer) tpInfo.get("tpId");
            query.must(termQuery("accountId", tpId));
        }
        if (StringUtils.isNotBlank(queryDto.getBettingCode())) {
            query.must(termQuery("bettingCode.keyword", queryDto.getBettingCode()));
        }
        if (queryDto.getType() != null) {
            query.must(termQuery("type", queryDto.getType()));
        }
        Page<ThirdPartyLiveBettingRecord> ret = elasticsearchTemplate.searchForPage(
                new SearchOperation<>("3rd_live_betting_record", query, ThirdPartyLiveBettingRecord.class)
                        .setFrom(queryDto.getOffset())
                        .setSize(queryDto.getLimit())
                        .setSort(XSort.desc("bettingTime"))
        );
        if (ret.getRecords().isEmpty())
            return ret;
        Set<Integer> mermberIds =
                ret.getRecords().stream().map(ThirdPartyLiveBettingRecord::getAccountId).distinct().mapToInt(Long::intValue).boxed().collect(Collectors.toSet());
        Set<Integer> stationIds =
                ret.getRecords().stream().map(ThirdPartyLiveBettingRecord::getStationId).distinct().mapToInt(Long::intValue).boxed().collect(Collectors.toSet());
        Map<Integer, Map<String, Object>> umap = new HashMap<>();
        if (tpInfo == null) {
            List<Map<String, Object>> users = accountInfoMapper.queryUserBy3rdIds(mermberIds);
            users.forEach(user -> umap.put((Integer) user.get("tpId"), user));
        } else {
            umap.put((Integer) tpInfo.get("tpId"), tpInfo);
        }

        Map<Integer, Map<String, Object>> bmap = new HashMap<>();
        if (StringUtils.isBlank(queryDto.getChannelId())) {
            List<Map<String, Object>> binds = mobileBindMapper.queryChannelIdsBy3rdIds(stationIds);
            binds.forEach(bind -> bmap.put((Integer) bind.get("cp_station_id"), bind));
        }
        ret.getRecords().forEach(order -> {
            Long tpId = order.getAccountId();
            Long stationId = order.getStationId();
            Map<String, Object> user = umap.get(tpId.intValue());
            if (user != null) {
                order.setUserName((String) user.get("nickName"));
                order.setUserId((Long) user.get("userId"));
            }
            if (StringUtils.isNotBlank(queryDto.getChannelId())) {
                order.setChannelId(queryDto.getChannelId());
            } else {
                Map<String, Object> bind = bmap.get(stationId.intValue());
                if (bind != null) {
                    order.setChannelId((String) bind.get("channel_id"));
                }
            }
        });
        return ret;
    }

    @Override
    public Page<ThirdPartySportsBettingRecord> query3rdSportBettingRecords(TPSportQueryDto queryDto) {
        BoolQueryBuilder query = QueryBuilders.boolQuery();
        Map<String, Object> tpInfo = null;
        if (queryDto.getStartTime() != null) {
            RangeQueryBuilder rangeQuery = rangeQuery("bettingTime").gte(queryDto.getStartTime());
            if (queryDto.getEndTime() != null) {
                rangeQuery.lte(queryDto.getEndTime());
            }
            query.must(rangeQuery);
        }
        if (StringUtils.isNotBlank(queryDto.getChannelId())) {
            Integer cpStationId = mobileBindMapper.getCpStationIdByChannel(queryDto.getChannelId());
            if (cpStationId == null)
                cpStationId = 0;
            query.must(termQuery("stationId", cpStationId));
        }
        if (queryDto.getUserId() != null) {
            tpInfo = accountInfoMapper.get3rdAccountId(queryDto.getUserId());
            Integer tpId = tpInfo == null ? 0 : (Integer) tpInfo.get("tpId");
            query.must(termQuery("accountId", tpId));
        }
        if (StringUtils.isNotBlank(queryDto.getBettingCode())) {
            query.must(termQuery("bettingCode.keyword", queryDto.getBettingCode()));
        }
        if (queryDto.getType() != null) {
            query.must(termQuery("type", queryDto.getType()));
        }
        if (queryDto.getResStatus()!=null){
            query.must(termQuery("resStatus", queryDto.getResStatus()));
        }
        Page<ThirdPartySportsBettingRecord> ret = elasticsearchTemplate.searchForPage(
                new SearchOperation<>("3rd_sport_betting_record", query, ThirdPartySportsBettingRecord.class)
                        .setFrom(queryDto.getOffset())
                        .setSize(queryDto.getLimit())
                        .setSort(XSort.desc("bettingTime"))
        );
        if (ret.getRecords().isEmpty())
            return ret;
        Set<Integer> mermberIds =
                ret.getRecords().stream().map(ThirdPartySportsBettingRecord::getAccountId).distinct().mapToInt(Long::intValue).boxed().collect(Collectors.toSet());
        Set<Integer> stationIds =
                ret.getRecords().stream().map(ThirdPartySportsBettingRecord::getStationId).distinct().mapToInt(Long::intValue).boxed().collect(Collectors.toSet());
        Map<Integer, Map<String, Object>> umap = new HashMap<>();
        if (tpInfo == null) {
            List<Map<String, Object>> users = accountInfoMapper.queryUserBy3rdIds(mermberIds);
            users.forEach(user -> umap.put((Integer) user.get("tpId"), user));
        } else {
            umap.put((Integer) tpInfo.get("tpId"), tpInfo);
        }

        Map<Integer, Map<String, Object>> bmap = new HashMap<>();
        if (StringUtils.isBlank(queryDto.getChannelId())) {
            List<Map<String, Object>> binds = mobileBindMapper.queryChannelIdsBy3rdIds(stationIds);
            binds.forEach(bind -> bmap.put((Integer) bind.get("cp_station_id"), bind));
        }
        ret.getRecords().forEach(order -> {
            try {
                if (StringUtils.isNotBlank(order.getParlayData()))
                    order.setParlay_json(JSON.parse(order.getParlayData()));
            } catch (Exception e) {
                order.setParlay_json(null);
            }
            order.setParlayData(null);
            Long tpId = order.getAccountId();
            Long stationId = order.getStationId();
            Map<String, Object> user = umap.get(tpId.intValue());
            if (user != null) {
                order.setUserName((String) user.get("nickName"));
                order.setUserId((Long) user.get("userId"));
            }
            if (StringUtils.isNotBlank(queryDto.getChannelId())) {
                order.setChannelId(queryDto.getChannelId());
            } else {
                Map<String, Object> bind = bmap.get(stationId.intValue());
                if (bind != null) {
                    order.setChannelId((String) bind.get("channel_id"));
                }
            }
        });
        return ret;
    }
}
