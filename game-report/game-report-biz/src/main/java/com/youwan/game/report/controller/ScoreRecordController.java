package com.youwan.game.report.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.common.core.util.R;
import com.youwan.game.report.api.dto.ScoreDto;
import com.youwan.game.report.service.ScoreRecordService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-25 12:22:25
 */
@RestController
@AllArgsConstructor
@RequestMapping("/scoreRecord")
public class ScoreRecordController {

  private final ScoreRecordService scoreRecordService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param scoreDto VIEW
   * @return
   */
  @GetMapping("/page")
  public R getScoreRecordPage(Page page, ScoreDto scoreDto) {
    return new R<>(scoreRecordService.queryPageList(page, scoreDto));
  }

}
