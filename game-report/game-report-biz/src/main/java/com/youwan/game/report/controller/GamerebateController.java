package com.youwan.game.report.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.common.core.util.R;
import com.youwan.game.report.api.entity.Gamerebate;
import com.youwan.game.report.service.GamerebateService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-02 12:18:56
 */
@RestController
@AllArgsConstructor
@RequestMapping("/gamerebate")
public class GamerebateController {


  private final GamerebateService gamerebateService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param accountId
   * @return
   */
  @GetMapping("/page")
  public R getGamerebatePage(Page page, String accountId, String channelNo) {
    return  new R<>(gamerebateService.queryGamerebatePage(page, accountId, channelNo));
  }


  /**
   * 通过id查询VIEW
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Long id){
    return new R<>(gamerebateService.getById(id));
  }


}
