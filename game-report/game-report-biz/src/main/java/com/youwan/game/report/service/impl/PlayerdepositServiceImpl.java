package com.youwan.game.report.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.common.core.util.TimeUtil;
import com.youwan.game.report.api.dto.ParameterDto;
import com.youwan.game.report.api.entity.Playerdeposit;
import com.youwan.game.report.mapper.PlayerdepositMapper;
import com.youwan.game.report.service.PlayerdepositService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static com.youwan.game.common.core.constant.CommonConstant.SECONDS_OF_DAY;
import static com.youwan.game.common.core.constant.ConfigConstant.DATE_FORMAT;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-03 14:04:14
 */
@Service("playerdepositService")
public class PlayerdepositServiceImpl extends ServiceImpl<PlayerdepositMapper, Playerdeposit> implements PlayerdepositService {

    @Override
    public IPage<Map<String, Object>> queryDepositPageList(Page page, ParameterDto parameterDto) {
        Integer start = null, end = null;
        if (parameterDto != null) {
            start = StrUtil.isNotBlank(parameterDto.getStartTime()) ? TimeUtil.stringToUnixTimeStamp(parameterDto.getStartTime(),
                    DATE_FORMAT) : null;
            end = StrUtil.isNotBlank(parameterDto.getEndTime()) ? TimeUtil.stringToUnixTimeStamp(parameterDto.getEndTime(),
                    DATE_FORMAT) + SECONDS_OF_DAY : null;
        }
        assert parameterDto != null;
        return baseMapper.queryDepositPage(page, parameterDto.getChannelNo(), parameterDto.getOrderId(),
                StringUtils.isNumeric(parameterDto.getAccountId()) ? Integer.parseInt(parameterDto.getAccountId()) : null,
                start, end);
    }
}
