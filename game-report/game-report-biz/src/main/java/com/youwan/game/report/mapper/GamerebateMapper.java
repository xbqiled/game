package com.youwan.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.report.api.entity.Gamerebate;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-02 12:18:56
 */
public interface GamerebateMapper extends BaseMapper<Gamerebate> {
    IPage<Map<String, Object>> queryGamerebatePage(Page page, @Param("accountId") Integer accountId, @Param("channelNo") String channelNo);
}
