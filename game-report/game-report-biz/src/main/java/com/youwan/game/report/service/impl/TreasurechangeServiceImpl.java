package com.youwan.game.report.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.common.core.util.TimeUtil;
import com.youwan.game.report.api.dto.QueryDto;
import com.youwan.game.report.api.entity.Usertreasurechange;
import com.youwan.game.report.mapper.UsertreasurechangeMapper;
import com.youwan.game.report.service.TreasurechangeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.youwan.game.common.core.constant.CommonConstant.SECONDS_OF_DAY;
import static com.youwan.game.common.core.constant.ConfigConstant.*;

/**
 * @author code generator
 * @date 2019-01-25 13:11:34
 */
@Service("treasurechangeService")
public class TreasurechangeServiceImpl extends ServiceImpl<UsertreasurechangeMapper, Usertreasurechange> implements TreasurechangeService {

    String viewName = "V_UserTreasureChange";
    String tableName = "usertreasurechange";

    @Override
    public IPage<Map<String, Object>> queryUserAccountPage(Page page, QueryDto queryDto) {
        /*String queryTabelName = viewName;
        if (queryDto != null && StrUtil.isNotEmpty(queryDto.getStartTime()) &&  StrUtil.isNotEmpty(queryDto.getEndTime())) {
            Date startDate = DateUtil.parse(queryDto.getStartTime(), DATE_FORMAT);
            Date endDate = DateUtil.parse(queryDto.getEndTime(), DATE_FORMAT);

            //计算相隔天数
            Long intervalTime = DateUtil.betweenDay(startDate, endDate, Boolean.TRUE);
            if (intervalTime <= 0 && (queryDto.getStartTime().substring(0, 10).equals(queryDto.getEndTime().substring(0, 10)))) {
                queryTabelName = tableName + DateUtil.format(startDate, THE_DATE_FORMAT);
            }
        }*/
        Integer start = null, end = null;
        assert queryDto != null;
        start = StrUtil.isNotBlank(queryDto.getStartTime()) ? TimeUtil.stringToUnixTimeStamp(queryDto.getStartTime(),
                FULL_DATE_TIME_FORMAT) : null;
        end = StrUtil.isNotBlank(queryDto.getEndTime()) ? TimeUtil.stringToUnixTimeStamp(queryDto.getEndTime(),
                FULL_DATE_TIME_FORMAT) : null;
        return baseMapper.queryUserAccountPage(page, queryDto.getChannelNo(), start, end,
                StringUtils.isNumeric(queryDto.getAccountId()) ? Integer.parseInt(queryDto.getAccountId()) : null);
    }
}
