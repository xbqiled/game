package com.youwan.game.report.controller;

import com.youwan.game.common.core.util.R;
import com.youwan.game.report.api.dto.TPAccountChangeQueryDto;
import com.youwan.game.report.service.ThirdPartyAccountChangeService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-09.
 */
@RestController
@AllArgsConstructor
@RequestMapping("/3rd/acc_change_records")
public class TPAccountChangeRecordController {

    @Autowired
    private ThirdPartyAccountChangeService tpAccountChangeService;

    @GetMapping()
    public R queryAccountChangRecord(TPAccountChangeQueryDto queryDto) {
        return new R(tpAccountChangeService.query3rdAccountChangeRecords(queryDto));
    }
}
