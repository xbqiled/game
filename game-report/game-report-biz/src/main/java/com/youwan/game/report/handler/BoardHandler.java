package com.youwan.game.report.handler;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <B>牌局信息转化处理类</B>
 */
public class BoardHandler {


    private static String AND_SUFFIX = "-";
    private static String SPLIT_SUFFIX = ",";
    private static String SPACE_SUFFIX = " ";
    private static String COLON_SUFFIX = ":";
    private static String FEN_SUFFIX = ";";


    private static Map<String, String> fishMap = new HashMap<String, String>();
    private static Map<String, String> huaseMap = new HashMap<String, String>();
    private static Map<String, String> paiMap = new HashMap<String, String>();
    private static Map<String, String> cardValueMap = new HashMap<String, String>();
    private static Map<String, String> bjlCardValueMap = new HashMap<>();
    private static Map<String, String> csdCardValueMap = new HashMap<String, String>();
    private static Map<String, String> ddzCardValueMap = new HashMap<String, String>();
    private static Map<String, String> pdkCardValueMap = new HashMap<>();
    private static Map<String, String> cardTypeMap = new HashMap<>();
    private static BoardHandler instance;

    //设计一个单例模式
    public static BoardHandler getInstance() {
        if (instance == null || cardValueMap.size() != 54 || csdCardValueMap.size() != 11 || ddzCardValueMap.size() != 54 || huaseMap.size() != 6 || paiMap.size() != 13 || fishMap.size() != 42) {
            instance = new BoardHandler();
        }
        return instance;
    }


    public BoardHandler() {
        if (cardTypeMap != null && cardTypeMap.size() != 13) {
            cardTypeMap.clear();
            cardTypeMap.put("0", "无牛");
            cardTypeMap.put("1", "牛1");
            cardTypeMap.put("2", "牛2");
            cardTypeMap.put("3", "牛3");

            cardTypeMap.put("4", "牛4");
            cardTypeMap.put("5", "牛5");
            cardTypeMap.put("6", "牛6");
            cardTypeMap.put("7", "牛7");

            cardTypeMap.put("8", "牛8");
            cardTypeMap.put("9", "牛9");
            cardTypeMap.put("10", "牛牛");
            cardTypeMap.put("11", "五花牛");

            cardTypeMap.put("12", "炸弹");
            cardTypeMap.put("13", "五小牛");
        }

        if (ddzCardValueMap != null && ddzCardValueMap.size() != 54) {
            ddzCardValueMap.clear();
            ddzCardValueMap.put("00", "♠3");
            ddzCardValueMap.put("01", "♥3");
            ddzCardValueMap.put("02", "♣3");
            ddzCardValueMap.put("03", "♦3");

            ddzCardValueMap.put("04", "♠4");
            ddzCardValueMap.put("05", "♥4");
            ddzCardValueMap.put("06", "♣4");
            ddzCardValueMap.put("07", "♦4");

            ddzCardValueMap.put("08", "♠5");
            ddzCardValueMap.put("09", "♥5");
            ddzCardValueMap.put("10", "♣5");
            ddzCardValueMap.put("11", "♦5");

            ddzCardValueMap.put("12", "♠6");
            ddzCardValueMap.put("13", "♥6");
            ddzCardValueMap.put("14", "♣6");
            ddzCardValueMap.put("15", "♦6");

            ddzCardValueMap.put("16", "♠7");
            ddzCardValueMap.put("17", "♥7");
            ddzCardValueMap.put("18", "♣7");
            ddzCardValueMap.put("19", "♦7");

            ddzCardValueMap.put("20", "♠8");
            ddzCardValueMap.put("21", "♥8");
            ddzCardValueMap.put("22", "♣8");
            ddzCardValueMap.put("23", "♦8");

            ddzCardValueMap.put("24", "♠9");
            ddzCardValueMap.put("25", "♥9");
            ddzCardValueMap.put("26", "♣9");
            ddzCardValueMap.put("27", "♦9");

            ddzCardValueMap.put("28", "♠10");
            ddzCardValueMap.put("29", "♥10");
            ddzCardValueMap.put("30", "♣10");
            ddzCardValueMap.put("31", "♦10");

            ddzCardValueMap.put("32", "♠J");
            ddzCardValueMap.put("33", "♥J");
            ddzCardValueMap.put("34", "♣J");
            ddzCardValueMap.put("35", "♦J");

            ddzCardValueMap.put("36", "♠Q");
            ddzCardValueMap.put("37", "♥Q");
            ddzCardValueMap.put("38", "♣Q");
            ddzCardValueMap.put("39", "♦Q");

            ddzCardValueMap.put("40", "♠K");
            ddzCardValueMap.put("41", "♥K");
            ddzCardValueMap.put("42", "♣K");
            ddzCardValueMap.put("43", "♦K");

            ddzCardValueMap.put("44", "♠A");
            ddzCardValueMap.put("45", "♥A");
            ddzCardValueMap.put("46", "♣A");
            ddzCardValueMap.put("47", "♦A");

            ddzCardValueMap.put("48", "♠2");
            ddzCardValueMap.put("49", "♥2");
            ddzCardValueMap.put("50", "♣2");
            ddzCardValueMap.put("51", "♦2");

            ddzCardValueMap.put("52", "小王");
            ddzCardValueMap.put("53", "大王");
        }

        if (pdkCardValueMap != null && pdkCardValueMap.size() != 54) {
            pdkCardValueMap.clear();
            pdkCardValueMap.put("01", "♦A");
            pdkCardValueMap.put("03", "♦3");
            pdkCardValueMap.put("04", "♦4");
            pdkCardValueMap.put("05", "♦5");
            pdkCardValueMap.put("06", "♦6");
            pdkCardValueMap.put("07", "♦7");
            pdkCardValueMap.put("08", "♦8");
            pdkCardValueMap.put("09", "♦9");
            pdkCardValueMap.put("10", "♦10");
            pdkCardValueMap.put("11", "♦11");
            pdkCardValueMap.put("12", "♦12");
            pdkCardValueMap.put("13", "♦13");

            pdkCardValueMap.put("17", "♣1");
            pdkCardValueMap.put("19", "♣3");
            pdkCardValueMap.put("20", "♣4");
            pdkCardValueMap.put("21", "♣5");
            pdkCardValueMap.put("22", "♣6");
            pdkCardValueMap.put("23", "♣7");
            pdkCardValueMap.put("24", "♣8");
            pdkCardValueMap.put("25", "♣9");
            pdkCardValueMap.put("26", "♣10");
            pdkCardValueMap.put("27", "♣11");
            pdkCardValueMap.put("28", "♣12");
            pdkCardValueMap.put("29", "♣13");

            pdkCardValueMap.put("33", "♥1");
            pdkCardValueMap.put("35", "♥3");
            pdkCardValueMap.put("36", "♥4");
            pdkCardValueMap.put("37", "♥5");
            pdkCardValueMap.put("38", "♥6");
            pdkCardValueMap.put("39", "♥7");
            pdkCardValueMap.put("40", "♥8");
            pdkCardValueMap.put("41", "♥9");
            pdkCardValueMap.put("42", "♥10");
            pdkCardValueMap.put("43", "♥11");
            pdkCardValueMap.put("44", "♥12");
            pdkCardValueMap.put("45", "♥13");

            pdkCardValueMap.put("50", "♠2");
            pdkCardValueMap.put("51", "♠3");
            pdkCardValueMap.put("52", "♠4");
            pdkCardValueMap.put("53", "♠5");
            pdkCardValueMap.put("54", "♠6");
            pdkCardValueMap.put("55", "♠7");
            pdkCardValueMap.put("56", "♠8");
            pdkCardValueMap.put("57", "♠9");
            pdkCardValueMap.put("58", "♠10");
            pdkCardValueMap.put("59", "♠11");
            pdkCardValueMap.put("60", "♠12");
            pdkCardValueMap.put("61", "♠13");

        }

        if(bjlCardValueMap != null && bjlCardValueMap.size() != 54 ) {
            bjlCardValueMap.clear();
            bjlCardValueMap.put("1", "♠A");
            bjlCardValueMap.put("2", "♠2");
            bjlCardValueMap.put("3", "♠3");
            bjlCardValueMap.put("4", "♠4");
            bjlCardValueMap.put("5", "♠5");
            bjlCardValueMap.put("6", "♠6");

            bjlCardValueMap.put("7", "♠7");
            bjlCardValueMap.put("8", "♠8");
            bjlCardValueMap.put("9", "♠9");
            bjlCardValueMap.put("10", "♠10");
            bjlCardValueMap.put("11", "♠J");
            bjlCardValueMap.put("12", "♠Q");
            bjlCardValueMap.put("13", "♠K");

            bjlCardValueMap.put("14", "♥A");
            bjlCardValueMap.put("15", "♥2");
            bjlCardValueMap.put("16", "♥3");
            bjlCardValueMap.put("17", "♥4");
            bjlCardValueMap.put("18", "♥5");
            bjlCardValueMap.put("19", "♥6");

            bjlCardValueMap.put("20", "♥7");
            bjlCardValueMap.put("21", "♥8");
            bjlCardValueMap.put("22", "♥9");
            bjlCardValueMap.put("23", "♥10");
            bjlCardValueMap.put("24", "♥J");
            bjlCardValueMap.put("25", "♥Q");
            bjlCardValueMap.put("26", "♥K");

            bjlCardValueMap.put("27", "♣A");
            bjlCardValueMap.put("28", "♣2");
            bjlCardValueMap.put("29", "♣3");
            bjlCardValueMap.put("30", "♣4");
            bjlCardValueMap.put("31", "♣5");
            bjlCardValueMap.put("32", "♣6");

            bjlCardValueMap.put("33", "♣7");
            bjlCardValueMap.put("34", "♣8");
            bjlCardValueMap.put("35", "♣9");
            bjlCardValueMap.put("36", "♣10");
            bjlCardValueMap.put("37", "♣J");
            bjlCardValueMap.put("38", "♣Q");
            bjlCardValueMap.put("39", "♣K");

            bjlCardValueMap.put("40", "♦A");
            bjlCardValueMap.put("41", "♦2");
            bjlCardValueMap.put("42", "♦3");
            bjlCardValueMap.put("43", "♦4");
            bjlCardValueMap.put("44", "♦5");
            bjlCardValueMap.put("45", "♦6");

            bjlCardValueMap.put("46", "♦7");
            bjlCardValueMap.put("47", "♦8");
            bjlCardValueMap.put("48", "♦9");
            bjlCardValueMap.put("49", "♦10");
            bjlCardValueMap.put("50", "♦J");
            bjlCardValueMap.put("51", "♦Q");
            bjlCardValueMap.put("52", "♦K");
        }

        if(cardValueMap != null && cardValueMap.size() != 52 ) {
            cardValueMap.clear();
            cardValueMap.put("1", "♦A");
            cardValueMap.put("2", "♦2");
            cardValueMap.put("3", "♦3");
            cardValueMap.put("4", "♦4");
            cardValueMap.put("5", "♦5");
            cardValueMap.put("6", "♦6");

            cardValueMap.put("7", "♦7");
            cardValueMap.put("8", "♦8");
            cardValueMap.put("9", "♦9");
            cardValueMap.put("10", "♦10");
            cardValueMap.put("11", "♦J");
            cardValueMap.put("12", "♦Q");
            cardValueMap.put("13", "♦K");

            cardValueMap.put("17", "♣A");
            cardValueMap.put("18", "♣2");
            cardValueMap.put("19", "♣3");
            cardValueMap.put("20", "♣4");
            cardValueMap.put("21", "♣5");
            cardValueMap.put("22", "♣6");

            cardValueMap.put("23", "♣7");
            cardValueMap.put("24", "♣8");
            cardValueMap.put("25", "♣9");
            cardValueMap.put("26", "♣10");
            cardValueMap.put("27", "♣J");
            cardValueMap.put("28", "♣Q");
            cardValueMap.put("29", "♣K");

            cardValueMap.put("33", "♥A");
            cardValueMap.put("34", "♥2");
            cardValueMap.put("35", "♥3");
            cardValueMap.put("36", "♥4");
            cardValueMap.put("37", "♥5");
            cardValueMap.put("38", "♥6");

            cardValueMap.put("39", "♥7");
            cardValueMap.put("40", "♥8");
            cardValueMap.put("41", "♥9");
            cardValueMap.put("42", "♥10");
            cardValueMap.put("43", "♥J");
            cardValueMap.put("44", "♥Q");
            cardValueMap.put("45", "♥K");

            cardValueMap.put("49", "♠A");
            cardValueMap.put("50", "♠2");
            cardValueMap.put("51", "♠3");
            cardValueMap.put("52", "♠4");
            cardValueMap.put("53", "♠5");
            cardValueMap.put("54", "♠6");

            cardValueMap.put("55", "♠7");
            cardValueMap.put("56", "♠8");
            cardValueMap.put("57", "♠9");
            cardValueMap.put("58", "♠10");
            cardValueMap.put("59", "♠J");
            cardValueMap.put("60", "♠Q");
            cardValueMap.put("61", "♠K");
        }

        if(huaseMap != null && huaseMap.size() != 6 ) {
            huaseMap.clear();
            huaseMap.put("0", "♦");
            huaseMap.put("1", "♣");
            huaseMap.put("2", "♥");
            huaseMap.put("3", "♠");
            huaseMap.put("4e", "小王");
            huaseMap.put("4f", "大王");
        }

        if(paiMap != null && paiMap.size() != 13 ) {
            paiMap.clear();
            paiMap.put("1", "A");
            paiMap.put("2", "2");
            paiMap.put("3", "3");
            paiMap.put("4", "4");

            paiMap.put("5", "5");
            paiMap.put("6", "6");
            paiMap.put("7", "7");
            paiMap.put("8", "8");

            paiMap.put("9", "9");
            paiMap.put("a", "10");
            paiMap.put("b", "J");
            paiMap.put("c", "Q");
            paiMap.put("d", "K");
        }


        if (fishMap != null && fishMap.size() != 42) {
            fishMap.clear();
            fishMap.put("1", "玛丽鱼");
            fishMap.put("2", "多利鱼");
            fishMap.put("3", "神仙鱼");

            fishMap.put("4", "大眼鱼");
            fishMap.put("5", "粉河豚");
            fishMap.put("6", "小丑鱼");

            fishMap.put("7", "刺豚");
            fishMap.put("8", "蓝利鱼");
            fishMap.put("9", "灯笼鱼");

            fishMap.put("10", "小乌龟");
            fishMap.put("12", "剑尾鱼");
            fishMap.put("15", "蝴蝶鱼");

            fishMap.put("18", "花裙鱼");
            fishMap.put("19", "小丑鱼三组队");
            fishMap.put("20", "剑鱼");

            fishMap.put("24", "蓝利鱼三组队");
            fishMap.put("25", "蝙蝠鱼");
            fishMap.put("30", "白鲨");

            fishMap.put("31", "小乌龟三组队");
            fishMap.put("35", "金鲨");
            fishMap.put("36", "花裙鱼四组队");

            fishMap.put("37", "剑尾鱼四组队");
            fishMap.put("38", "蝴蝶鱼四组队");
            fishMap.put("40", "大金鲨");

            fishMap.put("50", "金蟾");
            fishMap.put("200", "超级炸弹");
            fishMap.put("201", "局部炸弹");

            fishMap.put("202", "大银龙");
            fishMap.put("300", "大金龙");
            fishMap.put("401", "玛丽鱼鱼圈");

            fishMap.put("402", "多利鱼鱼圈");
            fishMap.put("403", "神仙鱼鱼圈");
            fishMap.put("404", "大眼鱼鱼圈");

            fishMap.put("405", "粉河豚鱼圈");
            fishMap.put("406", "小丑鱼鱼圈");
            fishMap.put("407", "刺豚鱼圈");

            fishMap.put("408", "蓝利鱼鱼圈");
            fishMap.put("409", "灯笼鱼鱼圈");
            fishMap.put("410", "小乌龟鱼圈");

            fishMap.put("510", "1元话费");
            fishMap.put("511", "2元话费");
            fishMap.put("512", "5元话费");
        }

        if (csdCardValueMap != null && csdCardValueMap.size() != 11) {
            csdCardValueMap.put("1", "https://youwanimage.oss-cn-shanghai.aliyuncs.com/game/20190426/1daa949d7d454c25b08e5dde0e30e284.png");
            csdCardValueMap.put("2", "https://youwanimage.oss-cn-shanghai.aliyuncs.com/game/20190426/238e9db0a14943e68596974b55be33b0.png");
            csdCardValueMap.put("3", "https://youwanimage.oss-cn-shanghai.aliyuncs.com/game/20190426/70eaf505fbf048a29d7857e3a86a66c5.png");

            csdCardValueMap.put("4", "https://youwanimage.oss-cn-shanghai.aliyuncs.com/game/20190426/673e84a76c4f4efa9f526c2a948981af.png");
            csdCardValueMap.put("5", "https://youwanimage.oss-cn-shanghai.aliyuncs.com/game/20190426/bf14216d559a42b79e55ba70a94b3891.png");
            csdCardValueMap.put("6", "https://youwanimage.oss-cn-shanghai.aliyuncs.com/game/20190426/fd4ad496083f4f7c9ce3fb73d1d0410c.png");
            csdCardValueMap.put("7", "https://youwanimage.oss-cn-shanghai.aliyuncs.com/game/20190426/c6c9048e67ff456fb1154581b7d5ef2f.png");
            csdCardValueMap.put("8", "https://youwanimage.oss-cn-shanghai.aliyuncs.com/game/20190426/7bda44de10284b01aacb8ff5c37dbe4a.png");
            csdCardValueMap.put("9", "https://youwanimage.oss-cn-shanghai.aliyuncs.com/game/20190426/c0f42d3a1210418797c9f9026910f298.png");

            csdCardValueMap.put("11", "https://youwanimage.oss-cn-shanghai.aliyuncs.com/game/20190426/78ab13a7087049b183316880b9c4fd45.png");
            csdCardValueMap.put("12", "https://youwanimage.oss-cn-shanghai.aliyuncs.com/game/20190426/3409b990d93f4a779f8811454bd11b73.png");
        }
    }


    //定义牌图片地址信息
    private static String ddzImgHard = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/board_img/ddz/";
    private static String ddzImgFoot = ".png";

    private static String pdkImgHard = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/board_img/pdk/";
    private static String pdkImgFoot = ".png";

    private static String sgjImgHard = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/board_img/sgj/";
    private static String sgjImgFoot = ".png";

    private static String esydImgHard = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/board_img/esyd/";
    private static String esydImgFoot = ".png";

    private static String bcbmImgHard = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/board_img/bcbm/";
    private static String bcbmImgFoot = ".png";

    private static String fqzsImgHard = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/board_img/fqzs/";
    private static String fqzsImgFoot = ".png";

    private static String qznnImgHard = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/board_img/qznn/";
    private static String qznnImgFoot = ".png";

    private static String hhdzImgHard = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/board_img/hhdz/";
    private static String hhdzImgFoot = ".png";

    private static String fishImgHard = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/board_img/fish/";
    private static String fishImgFoot = ".png";

    private static String nnImgHard = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/board_img/nn/";
    private static String nnImgFoot = ".png";

    private static String zjhImgHard = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/board_img/zjh/";
    private static String zjhImgFoot = ".png";

    private static String bjlImgHard = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/board_img/bjl/";
    private static String bjlImgFoot = ".png";

    private static String bhsImgHard = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/board_img/bhs/";
    private static String bhsImgFoot = ".png";


    /**
     * <B>解析飞禽走兽</B>
     * @param rewardType
     * @return
     */
    public String analysisFqzsRewardType(Integer rewardType) {
        return fqzsImgHard + rewardType + fqzsImgFoot;
    }


    public List<Map<String, Object>> analysisFqzsAwardAreaTimes(String areaTimes) {
        List<Map<String, Object>> resultList = new ArrayList<>();
        if (StrUtil.isNotBlank(areaTimes) && areaTimes.contains(BoardHandler.FEN_SUFFIX)) {
            String[] array = areaTimes.split(BoardHandler.FEN_SUFFIX);

            if (array != null && array.length > 0) {
                for (String str : array) {
                    if (StrUtil.isNotEmpty(str) && str.contains(BoardHandler.COLON_SUFFIX)) {
                        String[] betArray = str.split(BoardHandler.COLON_SUFFIX);

                        if (betArray != null && betArray.length == 2) {
                            Map<String, Object> map = new HashMap<>();
                            map.put("type", fqzsImgHard + betArray[0] + fqzsImgFoot);
                            map.put("award", betArray[1]);
                            resultList.add(map);
                        }
                    }
                }
            }
        }
        return resultList;
    }


    public List<String> analysisFqzsAwardBetArea(String awardBetArea) {
        List<String> resultList = new ArrayList<>();
        if (StrUtil.isNotBlank(awardBetArea) && awardBetArea.contains(BoardHandler.SPLIT_SUFFIX)) {
            String[] array = awardBetArea.split(BoardHandler.SPLIT_SUFFIX);
            for (int i = 0; i < array.length; i++) {
                resultList.add(fqzsImgHard + array[i] + fqzsImgFoot);
            }
        }
        return resultList;
    }


    public List<Map<String, Object>> analysisFqzsPlayerBet(String playerBet) {
        List<Map<String, Object>> resultList = new ArrayList<>();
        if (StrUtil.isNotBlank(playerBet) && playerBet.contains(BoardHandler.FEN_SUFFIX)) {
            String[] array = playerBet.split(BoardHandler.FEN_SUFFIX);

            if (array != null && array.length > 0) {
                for (String str : array) {
                    if (StrUtil.isNotEmpty(str) && str.contains(BoardHandler.COLON_SUFFIX)) {
                        String[] betArray = str.split(BoardHandler.COLON_SUFFIX);

                        if (betArray != null && betArray.length == 2) {
                            Map<String, Object> map = new HashMap<>();
                            map.put("type", fqzsImgHard + betArray[0] + fqzsImgFoot);
                            map.put("bet", betArray[1]);
                            resultList.add(map);
                        }
                    }
                }
            }
        }
        return resultList;
    }


    /**
     * 解析奔驰宝马
     */
    public String analysisBcbmRewardType(Integer rewardType) {
        return bcbmImgHard + rewardType + bcbmImgFoot;
    }

    public List<Map<String, Object>> analysisBcbmAwardAreaTimes(String areaTimes) {
        List<Map<String, Object>> resultList = new ArrayList<>();
        if (StrUtil.isNotBlank(areaTimes) && areaTimes.contains(BoardHandler.FEN_SUFFIX)) {
            String[] array = areaTimes.split(BoardHandler.FEN_SUFFIX);

            if (array != null && array.length > 0) {
                for (String str : array) {
                    if (StrUtil.isNotEmpty(str) && str.contains(BoardHandler.COLON_SUFFIX)) {
                        String[] betArray = str.split(BoardHandler.COLON_SUFFIX);

                        if (betArray != null && betArray.length == 2) {
                            Map<String, Object> map = new HashMap<>();
                            map.put("type", bcbmImgHard + betArray[0] + bcbmImgFoot);
                            map.put("award", betArray[1]);
                            resultList.add(map);
                        }
                    }
                }
            }
        }
        return resultList;
    }


    public List<String> analysisBcbmAwardBetArea(String awardBetArea) {
        List<String> resultList = new ArrayList<>();
        if (StrUtil.isNotBlank(awardBetArea) && awardBetArea.contains(BoardHandler.SPLIT_SUFFIX)) {
            String[] array = awardBetArea.split(BoardHandler.SPLIT_SUFFIX);
            for (int i = 0; i < array.length; i++) {
                resultList.add(bcbmImgHard + array[i] + bcbmImgFoot);
            }
        }
        return resultList;
    }


    public List<Map<String, Object>> analysisBcbmPlayerBet(String playerBet) {
        List<Map<String, Object>> resultList = new ArrayList<>();
        if (StrUtil.isNotBlank(playerBet) && playerBet.contains(BoardHandler.FEN_SUFFIX)) {
            String[] array = playerBet.split(BoardHandler.FEN_SUFFIX);

            if (array != null && array.length > 0) {
                for (String str : array) {
                    if (StrUtil.isNotEmpty(str) && str.contains(BoardHandler.COLON_SUFFIX)) {
                        String[] betArray = str.split(BoardHandler.COLON_SUFFIX);

                        if (betArray != null && betArray.length == 2) {
                            Map<String, Object> map = new HashMap<>();
                            map.put("type", bcbmImgHard + betArray[0] + bcbmImgFoot);
                            map.put("bet", betArray[1]);
                            resultList.add(map);
                        }
                    }
                }
            }
        }
        return resultList;
    }


    /**
     * <B>解析抢庄牛牛开牌记录</B>
     *
     * @param cardRecord
     * @return
     */
    public List<Map<String, Object>> analysisQznnCardRecord(String cardRecord) {
        List<Map<String, Object>> resultList = new ArrayList<>();
        if (StrUtil.isNotBlank(cardRecord) && cardRecord.contains(BoardHandler.FEN_SUFFIX)) {

            String[] array = cardRecord.split(BoardHandler.FEN_SUFFIX);
            if (array != null && array.length > 0) {
                for (String str : array) {

                    Map<String, Object> map = new HashMap<>();
                    if (StrUtil.isNotBlank(str) && str.contains(BoardHandler.COLON_SUFFIX)) {
                        String[] qznnarray = str.split(BoardHandler.COLON_SUFFIX);

                        if (qznnarray != null && qznnarray.length == 2) {
                            map.put("name", "账号(" + qznnarray[0] + ")");
                            String cardStr = qznnarray[1];

                            if (StrUtil.isNotBlank(cardStr) && cardStr.contains(BoardHandler.SPLIT_SUFFIX)) {
                                String[] cardAarray = cardStr.split(BoardHandler.SPLIT_SUFFIX);
                                if (cardAarray != null && cardAarray.length > 0) {

                                    List <String> list = new ArrayList<>();
                                    for (int i = 0; i < cardAarray.length; i++) {
                                        list.add(qznnImgHard + cardAarray[i] + qznnImgFoot);
                                    }

                                    map.put("list", list);
                                }
                            }
                        }

                        resultList.add(map);
                    }
                }
            }
        }
        return  resultList;
    }

    /**
     * <B>解析牌型</B>
     *
     * @param cardType
     * @return
     */
    public String analysisQznnCardType(String cardType) {
        //851:6;11455:9;1025061:0;
        StringBuffer sb = new StringBuffer();
        if (StrUtil.isNotBlank(cardType) && cardType.contains(BoardHandler.FEN_SUFFIX)) {

            String[] array = cardType.split(BoardHandler.FEN_SUFFIX);
            if (array != null && array.length > 0) {
                for (String str : array) {

                    if (StrUtil.isNotBlank(str) && str.contains(BoardHandler.COLON_SUFFIX)) {
                        String[] qznnarray = str.split(BoardHandler.COLON_SUFFIX);
                        if (qznnarray != null && qznnarray.length == 2) {

                            sb.append("账号(");
                            sb.append(qznnarray[0]);
                            sb.append("):");

                            sb.append(cardTypeMap.get(qznnarray[1]));
                        }
                    }
                    sb.append(BoardHandler.FEN_SUFFIX);
                }
            }
        }
        return sb.toString();
    }

    /**
     * <B>解析下注倍数</B>
     *
     * @param betMultiple
     * @return
     */
    public String analysisQznnBetMultiple(String betMultiple) {
        //851:5;11455:15;
        StringBuffer sb = new StringBuffer();
        if (StrUtil.isNotBlank(betMultiple) && betMultiple.contains(BoardHandler.FEN_SUFFIX)) {

            String[] array = betMultiple.split(BoardHandler.FEN_SUFFIX);
            if (array != null && array.length > 0) {
                for (String str : array) {

                    if (StrUtil.isNotBlank(str) && str.contains(BoardHandler.COLON_SUFFIX)) {
                        String[] qznnarray = str.split(BoardHandler.COLON_SUFFIX);
                        if (qznnarray != null && qznnarray.length == 2) {

                            sb.append("账号(");
                            sb.append(qznnarray[0]);
                            sb.append("):");

                            sb.append(qznnarray[1] + "倍");
                        }
                    }
                    sb.append(BoardHandler.FEN_SUFFIX);
                }
            }
        }
        return sb.toString();
    }


    /**
     * <B>处理红黑大战牌局</B>
     * @param list
     * @return
     */
    public List<Map<String, Object>> handlerHhdzBoard(List<Map<String, Object>> list) {
        for (int i = 0; i < list.size(); i++) {
            Map<String, Object> map = list.get(i);

            String betDetailName = (String) map.get("BetDetail");
            String recordInfoName = (String) map.get("CardRecord");

            StringBuffer betSb = new StringBuffer();
            if (StrUtil.isNotBlank(recordInfoName)) {
                betSb.append(splitHhdzBetStr(recordInfoName));
            }
            map.put("betDetailName", betSb.toString());

            //初始牌信息
            StringBuffer recordSb = new StringBuffer();
            if (StrUtil.isNotBlank(betDetailName)) {
                recordSb.append(splitHhdzRecordStr(betDetailName));
            }
            map.put("cardRecordName", recordSb.toString());

        }
        return list;
    }

    /**
     * <B>处理红黑大战牌局</B>
     *
     * @param hhdzPage
     * @return
     */
    public IPage<List<Map<String, Object>>> handlerHhdzBoard(IPage<List<Map<String, Object>>> hhdzPage) {
        if (hhdzPage != null && hhdzPage.getRecords() != null && hhdzPage.getRecords().size() > 0) {
            List list = hhdzPage.getRecords();
            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> map = (Map<String, Object>) list.get(i);

                String betDetailName = (String) map.get("BetDetail");
                String recordInfoName = (String) map.get("CardRecord");

                StringBuffer betSb = new StringBuffer();
                if (StrUtil.isNotBlank(recordInfoName)) {
                    betSb.append(splitHhdzBetStr(recordInfoName));
                }
                map.put("betDetailName", betSb.toString());

                //初始牌信息
                StringBuffer recordSb = new StringBuffer();
                if (StrUtil.isNotBlank(betDetailName)) {
                    recordSb.append(splitHhdzRecordStr(betDetailName));
                }
                map.put("cardRecordName", recordSb.toString());

            }
        }
        return hhdzPage;
    }


    public String splitHhdzRecordStr(String str) {
        StringBuffer sb = new StringBuffer();
        //首先切割;
        if (StrUtil.isNotBlank(str) && str.contains(BoardHandler.FEN_SUFFIX)) {
            String[] array = str.split(BoardHandler.FEN_SUFFIX);
            if (array != null && array.length == 2) {

                sb.append(array[0] + BoardHandler.SPACE_SUFFIX);
                if (StrUtil.isNotBlank(array[1]) && str.contains(BoardHandler.SPLIT_SUFFIX)) {
                    String[] darray = str.split(BoardHandler.SPLIT_SUFFIX);

                    if (darray != null && darray.length > 0) {
                        for (int i = 0; i < darray.length; i++) {
                            String aStr = darray[i];
                            if (i == 0) {
                                sb.append("下注量~");
                            } else {
                                sb.append("输赢~");
                            }

                            if (StrUtil.isNotBlank(aStr) && str.contains(BoardHandler.SPACE_SUFFIX)) {
                                String[] colon_darray = aStr.split(BoardHandler.SPACE_SUFFIX);
                                if (colon_darray != null && colon_darray.length > 0) {

                                    for (int j = 0; j < colon_darray.length; j++) {
                                        String bStr = colon_darray[j];

                                        if (StrUtil.isNotBlank(bStr) && str.contains(BoardHandler.COLON_SUFFIX)) {
                                            String[] sp_darray = bStr.split(BoardHandler.COLON_SUFFIX);

                                            if (sp_darray != null && sp_darray.length > 0) {
                                                for (int k = 0; k < sp_darray.length; k++) {
                                                    if (k == 0) {
                                                        String strQu = sp_darray[k];
                                                        if (strQu.length() > 1) {
                                                            sb.append("红区");
                                                        } else {
                                                            if (strQu.equals("0")) {
                                                                sb.append("红区");
                                                            } else if (strQu.equals("1")) {
                                                                sb.append("黑区");
                                                            } else {
                                                                sb.append("幸运一击区");
                                                            }
                                                        }
                                                        sb.append(BoardHandler.COLON_SUFFIX);
                                                    } else {
                                                        sb.append(sp_darray[k]);
                                                        sb.append(BoardHandler.SPLIT_SUFFIX);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return sb.toString();
    }


    public String splitHhdzBetStr(String str) {
        StringBuffer sb = new StringBuffer();
        //首先用户""切割
        if (StrUtil.isNotBlank(str) && str.contains(BoardHandler.SPACE_SUFFIX)) {
            String[] darray = str.split(BoardHandler.SPACE_SUFFIX);
            if (darray != null && darray.length > 0) {
                for (int i = 0; i < darray.length; i++) {

                    String aStr = darray[i];
                    //用:切割
                    if (StrUtil.isNotBlank(aStr) && str.contains(BoardHandler.COLON_SUFFIX)) {
                        String[] colon_darray = aStr.split(BoardHandler.COLON_SUFFIX);
                        if (colon_darray != null && colon_darray.length > 0) {

                            for (int j = 0; j < colon_darray.length; j++) {
                                String bStr = colon_darray[j];
                                //判断是否含有,
                                if (bStr.equals("red")) {
                                    sb.append("红方:");
                                } else if (bStr.equals("black")) {
                                    sb.append("黑方:");
                                } else {
                                    if (StrUtil.isNotBlank(bStr) && str.contains(BoardHandler.SPLIT_SUFFIX)) {
                                        String[] split_darray = bStr.split(BoardHandler.SPLIT_SUFFIX);

                                        if (split_darray != null && split_darray.length > 0) {
                                            for (int k = 0; k < split_darray.length; k++) {
                                                sb.append(cardValueMap.get(split_darray[k]));
                                                if (k < split_darray.length - 1) {
                                                    sb.append(BoardHandler.SPLIT_SUFFIX);
                                                }

                                                if (k == split_darray.length - 1) {
                                                    sb.append(BoardHandler.FEN_SUFFIX);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return sb.toString();
    }


    /**
     * 处理加工单个牌局信息
     *
     * @param boardDetail
     * @return
     */
    public List<Map<String, Object>> handlerCsdBoardDetail(String boardDetail) {
        List<Map<String, Object>> list = new ArrayList<>();
        if (StrUtil.isNotBlank(boardDetail)) {
            list = splitCsdCspDetail(boardDetail);
        }
        return list;
    }


    public List<Map<String, Object>> splitCsdCspDetail(String str) {
        List<Map<String, Object>> list = new ArrayList<>();
        GsonCsdRecord gsonCsdRecord = new Gson().fromJson(str, GsonCsdRecord.class);

        if (StrUtil.isNotBlank(gsonCsdRecord.getCards())) {
            if (StrUtil.isNotBlank(gsonCsdRecord.getCards()) && gsonCsdRecord.getCards().contains(BoardHandler.FEN_SUFFIX)) {
                String[] carray = gsonCsdRecord.getCards().split(BoardHandler.FEN_SUFFIX);

                Map<String, Object> map1 = new HashMap<>();
                Map<String, Object> map2 = new HashMap<>();
                Map<String, Object> map3 = new HashMap<>();

                int m1 = 0;
                int m2 = 0;
                int m3 = 0;

                if (carray != null && carray.length > 0) {
                    for (int i = 0; i < carray.length; i++) {
                        String cStr = carray[i];

                        if (StrUtil.isNotBlank(cStr) && cStr.contains(BoardHandler.SPACE_SUFFIX)) {
                            String[] darray = cStr.split(BoardHandler.SPACE_SUFFIX);

                            if (darray != null && darray.length > 0) {
                                for (int j = 0; j < darray.length; j++) {

                                    String dStr = darray[j];
                                    String value = csdCardValueMap.get(dStr) == null ? "" : csdCardValueMap.get(dStr);
                                    //map.put("img"+ j, value);
                                    if (j == 0) {
                                        map1.put("img" + m1, value);
                                        m1++;
                                    }

                                    if (j == 1) {
                                        map2.put("img" + m2, value);
                                        m2++;
                                    }

                                    if (j == 2) {
                                        map3.put("img" + m3, value);
                                        m3++;
                                    }
                                }
                            }
                        }
                    }
                }
                list.add(map1);
                list.add(map2);
                list.add(map3);
            }
        }
        return list;
    }


    /**
     * <B>处理财神到牌局</B>
     *
     * @param csdPage
     * @return
     */
    public IPage<Map<String, Object>> handlerCsdBoard(IPage<Map<String, Object>> csdPage) {
        if (csdPage != null && csdPage.getRecords() != null && csdPage.getRecords().size() > 0) {
            List list = csdPage.getRecords();
            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> map = (Map<String, Object>) list.get(i);

                String endIntegralName = (String) map.get("EndIntegral");
                StringBuffer jsjfSb = new StringBuffer();
                if (StrUtil.isNotBlank(endIntegralName)) {
                    jsjfSb.append(splitCsdJsjfStr(endIntegralName));
                }
                map.put("endIntegralName", jsjfSb.toString());

                //初始牌信息
//                String recordInfoName = (String) map.get("RecordInfo");
//                StringBuffer csspSb = new StringBuffer();
//                if (StrUtil.isNotBlank(recordInfoName)) {
//                    csspSb.append(splitCsdCspStr(recordInfoName));
//                }
//                map.put("recordInfoName", csspSb.toString());
            }
        }
        return csdPage;
    }


    public String splitCsdJsjfStr(String str) {
        StringBuffer sb = new StringBuffer();
        GsonCsdIntegral gsonCsdIntegral = new Gson().fromJson(str, GsonCsdIntegral.class);

        if (StrUtil.isNotBlank(gsonCsdIntegral.getFreeNum())) {
            sb.append("激活免费次数:" + gsonCsdIntegral.getFreeNum());
        }
        sb.append("\r");
        return sb.toString();
    }


    public String splitCsdCspStr(String str) {
        StringBuffer sb = new StringBuffer();
        GsonCsdRecord gsonCsdRecord = new Gson().fromJson(str, GsonCsdRecord.class);

        if (StrUtil.isNotBlank(gsonCsdRecord.getCards())) {
            if (StrUtil.isNotBlank(gsonCsdRecord.getCards()) && gsonCsdRecord.getCards().contains(BoardHandler.FEN_SUFFIX)) {
                String[] carray = gsonCsdRecord.getCards().split(BoardHandler.FEN_SUFFIX);

                if (carray != null && carray.length > 0) {
                    for (int i = 0; i < carray.length; i++) {
                        String cStr = carray[i];

                        if (StrUtil.isNotBlank(cStr) && cStr.contains(BoardHandler.SPACE_SUFFIX)) {
                            String[] darray = cStr.split(BoardHandler.SPACE_SUFFIX);
                            if (darray != null && darray.length > 0) {
                                for (int j = 0; j < darray.length; j++) {

                                    String dStr = darray[j];
                                    String value = csdCardValueMap.get(dStr) == null ? "" : csdCardValueMap.get(dStr);
                                    sb.append(value);

                                    if (j < darray.length - 1) {
                                        sb.append(BoardHandler.SPACE_SUFFIX);
                                    }
                                }
                            }

                            if (i < carray.length - 1) {
                                sb.append(BoardHandler.FEN_SUFFIX);
                            }
                        }
                    }
                }
                sb.append("\r");
            }
        }
        return sb.toString();
    }


    public class GsonCsdRecord {
        private String Cards;

        public String getCards() {
            return Cards;
        }

        public void setCards(String cards) {
            Cards = cards;
        }
    }


    public class GsonCsdIntegral {
        private String freeNum;

        public String getFreeNum() {
            return freeNum;
        }

        public void setFreeNum(String freeNum) {
            this.freeNum = freeNum;
        }
    }


    /**
     * <B>处理百家乐牌局</B>
     *
     * @param bjlPage
     * @return
     */
    public IPage<Map<String, Object>> handlerBjlBoard(IPage<Map<String, Object>> bjlPage) {
        if (bjlPage != null && bjlPage.getRecords() != null && bjlPage.getRecords().size() > 0) {
            List list = bjlPage.getRecords();

            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> map = (Map<String, Object>) list.get(i);
                //1:11,26,13 2:17,57,8
                String cardRecord = (String) map.get("CardRecord");
                StringBuffer pxSb = new StringBuffer();

                if (StrUtil.isNotBlank(cardRecord) && cardRecord.contains(BoardHandler.SPACE_SUFFIX)) {
                    String[] strArray = cardRecord.split(BoardHandler.SPACE_SUFFIX);
                    if (strArray != null && strArray.length > 0) {
                        for (String str : strArray) {
                            if (StrUtil.isNotBlank(str) && str.contains(BoardHandler.COLON_SUFFIX)) {
                                String[] cardArray = str.split(BoardHandler.COLON_SUFFIX);
                                if (strArray != null && strArray.length > 0) {
                                    if (StrUtil.isNotBlank(cardArray[0])) {
                                        if (cardArray[0].equals("1")) {
                                            pxSb.append("庄家牌-");
                                        } else {
                                            pxSb.append("闲家牌-");
                                        }
                                    }
                                    pxSb.append(splitBjlPx(cardArray[1]));
                                }
                            }
                        }
                    }
                }
                map.put("cardRecordName", pxSb.toString());

                //0:6000 1:22000 2:0 3:0 4:0 0:-6000 1:22000 2:0 3:0 4:0
                String betDetail = (String) map.get("BetDetail");
                StringBuffer xzSb = new StringBuffer();
                if (StrUtil.isNotBlank(betDetail) && betDetail.contains(BoardHandler.SPACE_SUFFIX)) {
                    String[] strArray = betDetail.split(BoardHandler.SPACE_SUFFIX);
                    if (strArray != null && strArray.length == 10) {
                        for (int k = 0; k < strArray.length; k++) {
                            xzSb.append(splitBjlXz(strArray[k], k));
                        }
                    }
                }
                map.put("betDetailName", xzSb.toString());
            }

        }
        return bjlPage;
    }

    private String splitBjlPx(String str) {
        StringBuffer sb = new StringBuffer();
        if (str.contains(BoardHandler.SPLIT_SUFFIX)) {

            String[] strDesc = str.split(BoardHandler.SPLIT_SUFFIX);
            for (String cardStr : strDesc) {
                String cardName = cardValueMap.get(cardStr) == null ? cardStr : cardValueMap.get(cardStr);
                sb.append(cardName);
            }
            sb.append("\n");
        }

        return sb.toString();
    }


    private String splitBjlXz(String str, int k) {
        StringBuffer sb = new StringBuffer();
        if (str.contains(BoardHandler.COLON_SUFFIX)) {
            String[] strDesc = str.split(BoardHandler.COLON_SUFFIX);
            if (strDesc != null && strDesc.length == 2) {
                if (strDesc[0].equals("0") && k == 0) {
                    sb.append("闲区域总下注数:");
                } else if (strDesc[0].equals("1") && k == 1) {
                    sb.append("庄区域总下注数:");
                } else if (strDesc[0].equals("2") && k == 2) {
                    sb.append("闲对域总下注数:");
                } else if (strDesc[0].equals("3") && k == 3) {
                    sb.append("和区域总下注数:");
                } else if (strDesc[0].equals("4") && k == 4) {
                    sb.append("庄对区域下注数:");

                } else if (strDesc[0].equals("0") && k == 5) {
                    sb.append("闲区域总得分:");
                } else if (strDesc[0].equals("1") && k == 6) {
                    sb.append("庄区域总得分:");
                } else if (strDesc[0].equals("2") && k == 7) {
                    sb.append("闲对域总得分:");
                } else if (strDesc[0].equals("3") && k == 8) {
                    sb.append("和区域总得分:");
                } else if (strDesc[0].equals("4") && k == 9) {
                    sb.append("庄对区域总得分:");
                }
                sb.append(strDesc[1]);
            }
            sb.append("\n");
        }

        return sb.toString();
    }

    /**
     * <B>处理水果机牌局</B>
     *
     * @param Page
     * @return
     */
    public IPage<Map<String, Object>> handlerSgjBoard(IPage<Map<String, Object>> Page) {
        if (Page != null && Page.getRecords() != null && Page.getRecords().size() > 0) {
            List list = Page.getRecords();

            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> map = (Map<String, Object>) list.get(i);
                String endIntegral = ObjectUtil.isNotNull(map.get("EndIntegral")) ? map.get("EndIntegral").toString() : "";
                StringBuffer sb = new StringBuffer();
                GsonCsdIntegral gsonCsdIntegral = new Gson().fromJson(endIntegral, GsonCsdIntegral.class);
                if (StrUtil.isNotBlank(gsonCsdIntegral.getFreeNum())) {
                    sb.append("激活免费次数:" + gsonCsdIntegral.getFreeNum());
                }
                sb.append("\r");
                map.put("endIntegral", sb.toString());

                String recordInfo = ObjectUtil.isNotNull(map.get("RecordInfo")) ? map.get("RecordInfo").toString() : "";
                List<Map<String, Object>> recordInfoList = new ArrayList<>();
                GsonCsdRecord gsonCsdRecord = new Gson().fromJson(recordInfo, GsonCsdRecord.class);
                if (StrUtil.isNotBlank(gsonCsdRecord.getCards())) {
                    if (StrUtil.isNotBlank(gsonCsdRecord.getCards()) && gsonCsdRecord.getCards().contains(BoardHandler.FEN_SUFFIX)) {
                        String[] carray = gsonCsdRecord.getCards().split(BoardHandler.FEN_SUFFIX);

                        Map<String, Object> map1 = new HashMap<>();
                        Map<String, Object> map2 = new HashMap<>();
                        Map<String, Object> map3 = new HashMap<>();

                        int m1 = 0;
                        int m2 = 0;
                        int m3 = 0;

                        if (carray != null && carray.length > 0) {
                            for (int o = 0; o < carray.length; o++) {
                                String cStr = carray[o];

                                if (StrUtil.isNotBlank(cStr) && cStr.contains(BoardHandler.SPACE_SUFFIX)) {
                                    String[] darray = cStr.split(BoardHandler.SPACE_SUFFIX);

                                    if (darray != null && darray.length > 0) {
                                        for (int j = 0; j < darray.length; j++) {

                                            String dStr = darray[j];
                                            //String value = sgjCardValueMap.get(dStr) == null ? "" : sgjCardValueMap.get(dStr);
                                            String value = sgjImgHard + dStr + sgjImgFoot;
                                            if (j == 0) {
                                                map1.put("img" + m1, value);
                                                m1++;
                                            }

                                            if (j == 1) {
                                                map2.put("img" + m2, value);
                                                m2++;
                                            }

                                            if (j == 2) {
                                                map3.put("img" + m3, value);
                                                m3++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        recordInfoList.add(map1);
                        recordInfoList.add(map2);
                        recordInfoList.add(map3);
                    }
                }
                map.put("recordInfo", recordInfoList);
            }

        }
        return Page;
    }

    /**
     * <B>处理跑得快牌局</B>
     *
     * @param Page
     * @return
     */
    public IPage<Map<String, Object>> handlerPdkBoard(IPage<Map<String, Object>> Page) {
        if (Page != null && Page.getRecords() != null && Page.getRecords().size() > 0) {
            List list = Page.getRecords();

            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> boardMap = (Map<String, Object>) list.get(i);
                String loseOrWinIntegal =   ObjectUtil.isNotNull(boardMap.get("LoseOrWinIntegal")) ? (String)boardMap.get("LoseOrWinIntegal") : "";
                String endIntegral = ObjectUtil.isNotNull(boardMap.get("EndIntegral")) ? (String)boardMap.get("EndIntegral") : "";
                String runRecord = ObjectUtil.isNotNull(boardMap.get("RunRecord")) ? (String)boardMap.get("RunRecord") : "";
                String hosteCount = ObjectUtil.isNotNull(boardMap.get("HosteCount")) ? (String)boardMap.get("HosteCount") : "";
                String hosteTrustDate = ObjectUtil.isNotNull(boardMap.get("HosteTrustDate")) ? (String)boardMap.get("HosteTrustDate") : "";
                String userInfo = ObjectUtil.isNotNull(boardMap.get("UserInfo")) ? (String)boardMap.get("UserInfo") : "";
                String recordInfo = ObjectUtil.isNotNull(boardMap.get("RecordInfo")) ? (String)boardMap.get("RecordInfo") : "";
                String outCard = ObjectUtil.isNotNull(boardMap.get("OutCard")) ? (String)boardMap.get("OutCard") : "";
                String isHostePunish = ObjectUtil.isNotNull(boardMap.get("IsHostePunish")) ? (String)boardMap.get("IsHostePunish") : "";
                //解析手牌信息
                List<Map<String, Object>> boardList = analysisPdkRecordInfo(recordInfo);
                //解析出牌信息
                List<Map<String, Object>> outCardList = analysisPdkOutCard(outCard);
                //解析用户信息
                String userInfoStr = analysisPdkUserInfo(userInfo);
                //该局输赢积分
                String endIntegralStr = analysisPdkIntegral(endIntegral);
                //结算信息
                String loseOrWinIntegalStr = analysisPdkLoseOrWin(loseOrWinIntegal);
                //逃跑记录
                String runRecordStr = analysisPdkRunRecord(runRecord);
                //托管次数
                String hosteCountStr = analysisPdkHosteCount(hosteCount);
                //托管时长
                String hosteTrustDateStr = analysisPdkHosteTrustDate(hosteTrustDate);
                boardMap.put("IsHostePunish", "1".equalsIgnoreCase(isHostePunish)?"是":"否");
                boardMap.put("boardList", boardList);
                boardMap.put("outCardList", outCardList);
                boardMap.put("userInfoStr", userInfoStr);
                boardMap.put("endIntegralStr", endIntegralStr);
                boardMap.put("loseOrWinIntegalStr", loseOrWinIntegalStr);
                boardMap.put("runRecordStr", runRecordStr);
                boardMap.put("hosteCountStr", hosteCountStr);
                boardMap.put("hosteTrustDateStr", hosteTrustDateStr);
            }

        }
        return Page;
    }

    /***
     * <B>解析跑得快手牌</B>
     */
    public List<Map<String, Object>> analysisPdkRecordInfo(String recordInfo) {
        List<Map<String, Object>> list = new ArrayList<>();
        Object[] strArray = new Gson().fromJson(recordInfo, Object[].class);

        if (strArray != null && strArray.length > 0) {
            for (Object strArr : strArray) {
                Map<String, Object> resultMap = new HashMap<>();

                List<String> imgList = new ArrayList<>();
                List<String> nameList = new ArrayList<>();
                List<String> imgLastList = new ArrayList<>();

                List<String> nameLastList = new ArrayList<>();
                StringBuffer sb = new StringBuffer();
                Map map = (Map) strArr;

                if (ObjectUtil.isNotNull(map.get("ChairID"))) {
                    sb.append("座位:" + new Long(Math.round((Double) map.get("ChairID"))).toString());
                    sb.append(",");
                    resultMap.put("chairId", "座位" + new Long(Math.round((Double) map.get("ChairID"))).toString());
                }

                if (ObjectUtil.isNotNull(map.get("CallCent"))) {
                    sb.append("叫分:" + new Long(Math.round((Double) map.get("CallCent"))).toString());
                    sb.append(",");
                    resultMap.put("callCent", "叫分:" + new Long(Math.round((Double) map.get("CallCent"))).toString());
                }

                if (ObjectUtil.isNotNull(map.get("IsDouble"))) {
                    String isDoubleValue = "";
                    if (map.get("IsDouble") instanceof Integer) {
                        isDoubleValue = ((Integer) map.get("IsDouble")) == 1 ? "加倍" : "不加倍";
                    } else if (map.get("IsDouble") instanceof Double) {
                        isDoubleValue = ((Double) map.get("IsDouble")) == 1 ? "加倍" : "不加倍";
                    }

                    sb.append("是否加倍:" + isDoubleValue);
                    sb.append(",");
                    resultMap.put("isDouble", "是否加倍:" + isDoubleValue);
                }

                if (ObjectUtil.isNotNull(map.get("IsLandOpen"))) {
                    String isLandOpenValue = map.get("IsLandOpen").equals("0") ? "没有" : "有";
                    sb.append("是否明牌:" + isLandOpenValue);
                    sb.append(",");
                    resultMap.put("isLandOpen", "是否加倍:" + isLandOpenValue);
                }

                if (ObjectUtil.isNotNull(map.get("OpenMul"))) {
                    sb.append("明牌倍数:" + new Long(Math.round((Double) map.get("OpenMul"))).toString());
                    sb.append(",");
                    resultMap.put("openMul", "明牌倍数:" + new Long(Math.round((Double) map.get("OpenMul"))).toString());
                }

                if (ObjectUtil.isNotNull(map.get("LastDouble"))) {
                    sb.append("底牌:" + map.get("LastDouble"));
                    sb.append(",");
                }

                if (ObjectUtil.isNotNull(map.get("OpenMul"))) {
                    sb.append("明牌倍数:" + new Long(Math.round((Double) map.get("OpenMul"))).toString());
                    sb.append(",");
                    resultMap.put("openMul", "明牌倍数:" + new Long(Math.round((Double) map.get("OpenMul"))).toString());
                }

                if (ObjectUtil.isNotNull(map.get("Total"))) {
                    sb.append("手牌权值:" + new Long(Math.round((Double) map.get("Total"))).toString());
                    resultMap.put("total", "手牌权值:" + new Long(Math.round((Double) map.get("Total"))).toString());
                }

                if (ObjectUtil.isNotNull(map.get("LandCards"))) {
                    String loatCardName = (String) map.get("LandCards");

                    if (StrUtil.isNotBlank(loatCardName)) {
                        int startIndex = 0;
                        int endIndex = 2;

                        while (endIndex <= loatCardName.length()) {
                            String str = loatCardName.substring(startIndex, endIndex);
                            String cardDesc = pdkCardValueMap.get(str);
                            nameLastList.add(cardDesc);
                            imgLastList.add(this.pdkImgHard + str + this.pdkImgFoot);
                            startIndex += 2;
                            endIndex += 2;
                        }
                    }
                }

                if (ObjectUtil.isNotNull(map.get("OrginCart"))) {
                    String cardName = (String) map.get("OrginCart");

                    if (StrUtil.isNotBlank(cardName)) {
                        int startIndex = 0;
                        int endIndex = 2;

                        while (endIndex <= cardName.length()) {
                            String str = cardName.substring(startIndex, endIndex);
                            String cardDesc = pdkCardValueMap.get(str);
                            nameList.add(cardDesc);
                            imgList.add(this.pdkImgHard + str + this.pdkImgFoot);
                            startIndex += 2;
                            endIndex += 2;
                        }
                    }
                }

                resultMap.put("desc", sb.toString());
                resultMap.put("imgArr", imgList);
                resultMap.put("nameArr", nameList);

                if (CollUtil.isNotEmpty(nameLastList)) {
                    resultMap.put("nameLastList", nameLastList);
                }

                if (CollUtil.isNotEmpty(imgLastList)) {
                    resultMap.put("imgLastList", imgLastList);
                }
                list.add(resultMap);
            }
        }

        return list;
    }


    /**
     * <B>解析跑得快用戶信息</B>
     */
    public String analysisPdkUserInfo(String userInfo) {
        StringBuffer resultSb = new StringBuffer();
        Object[] strArr = new Gson().fromJson(userInfo, Object[].class);

        if (strArr != null && strArr.length > 0) {
            for (Object str : strArr) {
                Map userMap = (Map) str;

                String score = userMap.get("Chair") instanceof Double ? new Long(Math.round((Double) userMap.get("Chair"))).toString() : (String) userMap.get("Chair");
                String userId = userMap.get("UserID") instanceof Double ? new Long(Math.round((Double) userMap.get("UserID"))).toString() : (String) userMap.get("UserID");

                resultSb.append("用户(" + userId + "):");
                resultSb.append("座位:" + score);
                resultSb.append(";");
            }
        }
        return resultSb.toString();
    }

    /**
     * <B>解析跑得快積分信息</B>
     */
    public String analysisPdkIntegral(String endIntegral) {
        StringBuffer sb = new StringBuffer();
        Object[] strArr = new Gson().fromJson(endIntegral, Object[].class);

        if (strArr != null && strArr.length > 0) {
            for (Object str : strArr) {
                Map map = (Map) str;

                String score = map.get("AfterScore") instanceof Double ? new Long(Math.round((Double) map.get("AfterScore"))).toString() : (String) map.get("AfterScore");
                String userId = map.get("UserID") instanceof Double ? new Long(Math.round((Double) map.get("UserID"))).toString() : (String) map.get("UserID");

                sb.append("用户(" + userId + "):");
                sb.append("得分:" + score);
                sb.append(";");
            }
        }
        return sb.toString();
    }


    /**
     * <B>解析跑得快输赢信息</B>
     */
    public String analysisPdkLoseOrWin(String loseOrWinIntegal) {
        StringBuffer sb = new StringBuffer();
        Object[] strArr = new Gson().fromJson(loseOrWinIntegal, Object[].class);

        if (strArr != null && strArr.length > 0) {
            for (Object str : strArr) {
                Map map = (Map) str;

                String score = map.get("Score") instanceof Double ? new Long(Math.round((Double) map.get("Score"))).toString() : (String) map.get("Score");
                String userId = map.get("UserID") instanceof Double ? new Long(Math.round((Double) map.get("UserID"))).toString() : (String) map.get("UserID");

                sb.append("用户(" + userId + "):");
                sb.append("得分:" + score);
                sb.append(";");
            }
        }
        return sb.toString();
    }

    /**
     * <B>解析跑得快逃跑信息<B/>
     */
    public String analysisPdkRunRecord(String runRecord) {
        StringBuffer sb = new StringBuffer();
        Object[] strArr = new Gson().fromJson(runRecord, Object[].class);

        if (strArr != null && strArr.length > 0) {
            for (Object str : strArr) {
                Map map = (Map) str;
                String userId = map.get("UserID") instanceof Double ? new Long(Math.round((Double) map.get("UserID"))).toString() : (String) map.get("UserID");
                String isEscape = map.get("IsEscape") instanceof Double ? new Long(Math.round((Double) map.get("IsEscape"))).toString() : (String) map.get("IsEscape");
                sb.append("用户(" + userId + "):");
                sb.append("是否逃跑:");
                sb.append(isEscape.equals("0") ? "没有" : "有逃跑");
                sb.append(";");
            }
        }
        return sb.toString();
    }

    /**
     * <B>解析跑得快托管次数信息<B/>
     */
    public String analysisPdkHosteCount(String hosteCount) {
        StringBuffer sb = new StringBuffer();
        Object[] strArr = new Gson().fromJson(hosteCount, Object[].class);

        if (strArr != null && strArr.length > 0) {
            for (Object str : strArr) {
                Map map = (Map) str;
                String userId = map.get("UserID") instanceof Double ? new Long(Math.round((Double) map.get("UserID"))).toString() : (String) map.get("UserID");
                String hosteType = map.get("HosteType2") instanceof Double ? new Long(Math.round((Double) map.get("HosteType2"))).toString() : (String) map.get("HosteType2");
                sb.append("用户(" + userId + "):");
                sb.append("托管次数:" + hosteType);
                sb.append(";");
            }
        }
        return sb.toString();
    }

    /**
     * <B>解析跑得快托管时长信息<B/>
     */
    public String analysisPdkHosteTrustDate(String hosteTrustDate) {
        StringBuffer sb = new StringBuffer();
        Object[] strArr = new Gson().fromJson(hosteTrustDate, Object[].class);

        if (strArr != null && strArr.length > 0) {
            for (Object str : strArr) {
                Map map = (Map) str;

                String userId = map.get("UserID") instanceof Double ? new Long(Math.round((Double) map.get("UserID"))).toString() : (String) map.get("UserID");
                String hosteTrustDateStr = map.get("HosteTrustDate") instanceof Double ? new Long(Math.round((Double) map.get("HosteTrustDate"))).toString() : (String) map.get("HosteTrustDate");
                sb.append("用户(" + userId + "):");
                sb.append("托管时长:" + hosteTrustDateStr);
                sb.append(";");
            }
        }
        return sb.toString();
    }

    /**
     * <B>解析跑得快托管时长信息<B/>
     */
    public String analysisPdkIsHostePunish(String isHostePunish) {
        StringBuffer sb = new StringBuffer();
        Object[] strArr = new Gson().fromJson(isHostePunish, Object[].class);

        if (strArr != null && strArr.length > 0) {
            for (Object str : strArr) {
                Map map = (Map) str;

                String userId = map.get("UserID") instanceof Double ? new Long(Math.round((Double) map.get("UserID"))).toString() : (String) map.get("UserID");
                String isHostePunishStr = map.get("IsHostePunish") instanceof Double ? new Long(Math.round((Double) map.get("IsHostePunish"))).toString() : (String) map.get("IsHostePunish");
                String amountStr = map.get("Amount") instanceof Double ? new Long(Math.round((Double) map.get("Amount"))).toString() : (String) map.get("Amount");

                sb.append("用户(" + userId + "):");
                String punishStr = isHostePunishStr.equals("0") ? "否" : "是";
                sb.append("是否处罚:" + punishStr);

                sb.append("处罚分数:" + amountStr);
                sb.append(";");
            }
        }
        return sb.toString();
    }

    /**
     * <B>解析跑得快出牌信息</B>
     */
    public List<Map<String, Object>> analysisPdkOutCard(String outCard) {
        List<Map<String, Object>> list = new ArrayList<>();
        String[] strArray = outCard.split(BoardHandler.FEN_SUFFIX);

        if (strArray != null && strArray.length > 0) {
            for (String str : strArray) {
                //解析出牌
                String[] outCardArr = str.split(BoardHandler.SPLIT_SUFFIX);

                Map<String, Object> resultMap = new HashMap<>();
                //解析牌局
                List<String> imgList = new ArrayList<>();
                List<String> nameList = new ArrayList<>();
                StringBuffer sb = new StringBuffer();
                String boardDesc = "";

                if (outCardArr != null && outCardArr.length > 0) {
                    if (outCardArr.length == 3) {

                        sb.append("座位:" + outCardArr[0]);
                        if (StrUtil.isNotBlank(outCardArr[2])) {
                            if (outCardArr[2].equals("pass")) {
                                boardDesc = outCardArr[2];
                            } else {
                                int startIndex = 0;
                                int endIndex = 2;

                                while (endIndex <= outCardArr[2].length()) {
                                    String subStr = outCardArr[2].substring(startIndex, endIndex);
                                    String cardDesc = pdkCardValueMap.get(subStr);

                                    nameList.add(cardDesc);
                                    imgList.add(this.pdkImgHard + subStr + this.pdkImgFoot);
                                    startIndex += 2;
                                    endIndex += 2;
                                }
                            }
                        }

                        resultMap.put("desc", sb.toString());
                        resultMap.put("imgArr", imgList);
                        resultMap.put("nameArr", nameList);

                        if (StrUtil.isNotBlank(boardDesc)) {
                            resultMap.put("boardDesc", boardDesc);
                        }
                        list.add(resultMap);
                    }
                }
            }
        }
        return list;
    }

    /**
     * <B>处理21点牌局</B>
     *
     * @param Page
     * @return
     */
    public IPage<Map<String, Object>> handlerTwentyOneBoard(IPage<Map<String, Object>> Page) {
        if (Page != null && Page.getRecords() != null && Page.getRecords().size() > 0) {
            List list = Page.getRecords();

            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> map = (Map<String, Object>) list.get(i);
                //庄家牌
                String cardRecord = ObjectUtil.isNotNull(map.get("CardRecord")) ? map.get("CardRecord").toString() : "";
                //自己牌
                String bankCardRecord = ObjectUtil.isNotNull(map.get("BankCardRecord")) ? map.get("BankCardRecord").toString() : "";
                //庄家牌型
                String bankerDim = ObjectUtil.isNotNull(map.get("BankerDim")) ? map.get("BankerDim").toString() : "";
                List<String> cardRecordList = TwentyOneCardRecord(bankCardRecord);
                List<Map<String, Object>> bankCardRecordList = TwentyOneBankCardRecord(cardRecord);
                if("1".equalsIgnoreCase(bankerDim)){
                    bankerDim = "爆牌";
                }else if("2".equalsIgnoreCase(bankerDim)){
                    bankerDim = "低于21点";
                }else if("3".equalsIgnoreCase(bankerDim)){
                    bankerDim = "21点";
                }else if("4".equalsIgnoreCase(bankerDim)){
                    bankerDim = "五小龙";
                }else if("5".equalsIgnoreCase(bankerDim)){
                    bankerDim = "黑杰克";
                }

                map.put("BankerDim", bankerDim);
                map.put("cardRecord", cardRecordList);
                map.put("bankCardRecord", bankCardRecordList);
            }

        }
        return Page;
    }

    public List<String> TwentyOneCardRecord(String bankCardRecord) {
        List<String> list = new ArrayList<>();

        if (StrUtil.isNotBlank(bankCardRecord)) {
            String[] jsonArray = new Gson().fromJson(bankCardRecord, String[].class);

            if (jsonArray != null && jsonArray.length > 0) {
                for (String str : jsonArray) {
                    list.add(esydImgHard + str + esydImgFoot);
                }
            }
        }
        return list;
    }

    public List<Map<String, Object>>  TwentyOneBankCardRecord(String cardRecord) {
        List<Map<String, Object>> resultList = new ArrayList<>();

        if (StrUtil.isNotBlank(cardRecord)) {
            TwentyOneCardRecord[] modelArray = new Gson().fromJson(cardRecord, TwentyOneCardRecord[].class);

            if (modelArray != null && modelArray.length > 0) {
                for (TwentyOneCardRecord twentyOneCardRecord : modelArray) {
                    List<String> list = new ArrayList<>();
                    Map<String, Object> resultMap = new HashMap<>();

                    if (twentyOneCardRecord.getCards() != null && twentyOneCardRecord.getCards().length >0) {
                        for (Integer str : twentyOneCardRecord.getCards()) {
                            list.add(esydImgHard + str + esydImgFoot);
                        }
                        resultMap.put("cardRecord", list);
                    }

                    resultMap.put("area", twentyOneCardRecord.getArea());
                    resultMap.put("bet", twentyOneCardRecord.getBet());

                    resultMap.put("insure", twentyOneCardRecord.getInsure());
                    if (twentyOneCardRecord.getDim().intValue() == 1) {
                        resultMap.put("dim", "爆牌");
                    } else if (twentyOneCardRecord.getDim().intValue() == 2) {
                        resultMap.put("dim", "低于21点");
                    } else if (twentyOneCardRecord.getDim().intValue() == 3) {
                        resultMap.put("dim", "21点");
                    } else if (twentyOneCardRecord.getDim().intValue() == 4) {
                        resultMap.put("dim", "五小龙");
                    } else if (twentyOneCardRecord.getDim().intValue() == 5) {
                        resultMap.put("dim", "黑杰克");
                    }

                    resultMap.put("point", twentyOneCardRecord.getPoint());
                    resultMap.put("tax", twentyOneCardRecord.getTax());
                    resultMap.put("win", twentyOneCardRecord.getWin());
                    resultList.add(resultMap);
                }
            }
        }
        return resultList;
    }


    /**
     * <B>处理比花色牌局</B>
     *
     * @param bhsPage
     * @return
     */
    public IPage<Map<String, Object>> handlerBhsBoard(IPage<Map<String, Object>> bhsPage) {
        if (bhsPage != null && bhsPage.getRecords() != null && bhsPage.getRecords().size() > 0) {
            List list = bhsPage.getRecords();
            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> map = (Map<String, Object>) list.get(i);

                //花色
                String cardRecord = (String) map.get("CardRecord");
                StringBuffer cardSb = new StringBuffer();
                if (StrUtil.isNotBlank(cardRecord)) {
                    cardSb.append(cardValueMap.get(cardRecord));
                }
                map.put("cardRecordName", cardSb.toString());

                //下注详情
                String betDetail = (String) map.get("BetDetail");
                StringBuffer betSb = new StringBuffer();

                if (StrUtil.isNotBlank(betDetail)) {
                    //判断字符串是否含有"-"

                    if (betDetail.contains(BoardHandler.AND_SUFFIX)) {
                        String[] andArray = betDetail.split(BoardHandler.AND_SUFFIX);
                        if (andArray != null && andArray.length == 2) {
                            betSb.append("下注-");
                            String betStr = andArray[0];
                            if (betStr.contains(BoardHandler.SPACE_SUFFIX)) {
                                //如果有 分隔
                                String[] betArray = betStr.split(BoardHandler.SPACE_SUFFIX);
                                for (int k = 0; k < betArray.length; k++) {
                                    String betResultStr = splitBhsXzStr(betArray[k]);
                                    betSb.append(betResultStr);
                                }
                            }

                            betSb.append("得分-");
                            String scoreStr = andArray[1];

                            if (scoreStr.contains(BoardHandler.SPACE_SUFFIX)) {
                                //如果有 分隔
                                String[] scoreArray = scoreStr.split(BoardHandler.SPACE_SUFFIX);
                                for (int k = 0; k < scoreArray.length; k++) {
                                    String scoreResultStr = splitBhsDfStr(scoreArray[k]);
                                    betSb.append(scoreResultStr);
                                }
                            }
                        }
                    }

                    map.put("betDetailName", betSb.toString());
                }
            }
        }
        return bhsPage;
    }


    private String splitBhsXzStr(String str) {
        StringBuffer sb = new StringBuffer();
        if (str.contains(BoardHandler.COLON_SUFFIX)) {

            String[] strDesc = str.split(BoardHandler.COLON_SUFFIX);
            //判断数组长度,如果为6是龙虎和，反之是牛牛

            for (int j = 0; j < strDesc.length; j++) {
                if (j == 0) {
                    if (strDesc[j].equals("1")) {
                        sb.append("大总下注数:");
                    } else if (strDesc[j].equals("2")) {
                        sb.append("小总下注数:");
                    } else if (strDesc[j].equals("3")) {
                        sb.append("红总下注数:");
                    } else if (strDesc[j].equals("4")) {
                        sb.append("黑总下注数:");
                    } else if (strDesc[j].equals("5")) {
                        sb.append("单总下注数:");
                    } else if (strDesc[j].equals("6")) {
                        sb.append("双总下注数:");
                    } else if (strDesc[j].equals("7")) {
                        sb.append("黑桃总下注数:");
                    } else if (strDesc[j].equals("8")) {
                        sb.append("红桃总下注数:");
                    } else if (strDesc[j].equals("9")) {
                        sb.append("梅花总下注数:");
                    } else if (strDesc[j].equals("10")) {
                        sb.append("方块总下注数:");
                    } else if (strDesc[j].equals("11")) {
                        sb.append("固定牌总下注数:");
                    }
                } else if (j == 1) {
                    sb.append(strDesc[j]);
                }
            }
            sb.append("\n");
        }
        return sb.toString();
    }


    private String splitBhsDfStr(String str) {
        StringBuffer sb = new StringBuffer();
        if (str.contains(BoardHandler.COLON_SUFFIX)) {

            String[] strDesc = str.split(BoardHandler.COLON_SUFFIX);
            //判断数组长度,如果为6是龙虎和，反之是牛牛

            for (int j = 0; j < strDesc.length; j++) {
                if (j == 0) {
                    if (strDesc[j].equals("1")) {
                        sb.append("大总得分数:");
                    } else if (strDesc[j].equals("2")) {
                        sb.append("小总得分数:");
                    } else if (strDesc[j].equals("3")) {
                        sb.append("红总得分数:");
                    } else if (strDesc[j].equals("4")) {
                        sb.append("黑总得分数:");
                    } else if (strDesc[j].equals("5")) {
                        sb.append("单总得分数:");
                    } else if (strDesc[j].equals("6")) {
                        sb.append("双总得分数:");
                    } else if (strDesc[j].equals("7")) {
                        sb.append("黑桃总得分数:");
                    } else if (strDesc[j].equals("8")) {
                        sb.append("红桃总得分数:");
                    } else if (strDesc[j].equals("9")) {
                        sb.append("梅花总得分数:");
                    } else if (strDesc[j].equals("10")) {
                        sb.append("方块总得分数:");
                    } else if (strDesc[j].equals("11")) {
                        sb.append("固定牌总得分数:");
                    }
                } else if (j == 1) {
                    sb.append(strDesc[j]);
                }
            }
            sb.append("\n");
        }
        return sb.toString();
    }


    /**
     * <B>处理炸金花牌局</B>
     *
     * @param zjhPage
     * @return
     */
    public IPage<Map<String, Object>> handlerZjhBoard(IPage<Map<String, Object>> zjhPage) {
        if (zjhPage != null && zjhPage.getRecords() != null && zjhPage.getRecords().size() > 0) {
            List list = zjhPage.getRecords();

            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> map = (Map<String, Object>) list.get(i);
                //结算积分
                String endIntegralName = (String) map.get("EndIntegral");

                StringBuffer jsjfSb = new StringBuffer();
                if (StrUtil.isNotBlank(endIntegralName)) {
                    jsjfSb.append(splitZjhJsjfStr(endIntegralName));
                }
                map.put("endIntegralName", jsjfSb.toString());

                //初始牌信息
                String recordInfoName = (String) map.get("RecordInfo");
                StringBuffer csspSb = new StringBuffer();
                if (StrUtil.isNotBlank(recordInfoName)) {
                    csspSb.append(splitZjhCspStr(recordInfoName));
                }
                map.put("recordInfoName", csspSb.toString());

                //用户信息
                String userInfoName = (String) map.get("UserInfo");
                StringBuffer userSb = new StringBuffer();
                if (StrUtil.isNotBlank(userInfoName)) {
                    userSb.append(splitZjhUserStr(userInfoName));
                }
                map.put("userInfoName", userSb.toString());
            }
        }
        return zjhPage;
    }


    public String splitZjhJsjfStr(String str) {
        StringBuffer jsjfsb = new StringBuffer();
        Object[] strArray = new Gson().fromJson(str, Object[].class);

        if (strArray != null && strArray.length > 0) {
            for (Object strArr : strArray) {
                Map map = (Map) strArr;

                String afterScore = map.get("AfterScore") instanceof Double ? new Long(Math.round((Double) map.get("AfterScore"))).toString() : (String) map.get("AfterScore");
                String userId = map.get("UserID") instanceof Double ? new Long(Math.round((Double) map.get("UserID"))).toString() : (String) map.get("UserID");

                jsjfsb.append("用户(" + userId + "):");
                jsjfsb.append("得分:" + afterScore + "");
                jsjfsb.append(";");
            }
        }
        return jsjfsb.toString();
    }


    public String splitZjhCspStr(String str) {
        StringBuffer sb = new StringBuffer();
        Object[] strArray = new Gson().fromJson(str, Object[].class);

        if (strArray != null && strArray.length > 0) {
            for (Object strArr : strArray) {
                Map map = (Map) strArr;
                if (map.get("ChairID") != null) {
                    sb.append("椅子号:" + new Long(Math.round((Double) map.get("ChairID"))).toString());
                    sb.append(",");
                }

                if (map.get("Cards") != null) {
                    sb.append("手牌:" + getCardValue((String) map.get("Cards")));
                    sb.append(",");
                }

                if (map.get("Shape") != null) {
                    String shapeValue = new Long(Math.round((Double) map.get("Shape"))).toString();
                    sb.append("牌型:" + getShapeValue(shapeValue));
                    sb.append(",");
                }
                sb.append("\r");
            }
        }
        return sb.toString();
    }


    /**
     * <B>获取手牌信息</B>
     *
     * @param cards
     * @return
     */
    public String getCardValue(String cards) {
        if (StrUtil.isBlank(cards) || cards.length() != 6) {
            return "";
        } else {
            StringBuffer sb = new StringBuffer();

            String paia = cards.substring(0, 2);
            String paib = cards.substring(2, 4);
            String paic = cards.substring(4, 6);

            String aStr = Integer.toHexString(new Integer(paia)).length() == 1 ? "0" + Integer.toHexString(new Integer(paia)) : Integer.toHexString(new Integer(paia));
            String bStr = Integer.toHexString(new Integer(paib)).length() == 1 ? "0" + Integer.toHexString(new Integer(paib)) : Integer.toHexString(new Integer(paib));
            String cStr = Integer.toHexString(new Integer(paic)).length() == 1 ? "0" + Integer.toHexString(new Integer(paic)) : Integer.toHexString(new Integer(paic));

            if (aStr.substring(0, 1).equals("4")) {
                sb.append(StrUtil.isNotBlank(huaseMap.get(aStr)) ? huaseMap.get(aStr) : aStr);
            } else {
                sb.append(StrUtil.isNotBlank(huaseMap.get(aStr.substring(0, 1))) ? huaseMap.get(aStr.substring(0, 1)) : aStr.substring(0, 1));
                sb.append(StrUtil.isNotBlank(paiMap.get(aStr.substring(1, 2))) ? paiMap.get(aStr.substring(1, 2)) : aStr.substring(1, 2));
            }

            if (bStr.substring(0, 1).equals("4")) {
                sb.append(StrUtil.isNotBlank(huaseMap.get(bStr)) ? huaseMap.get(bStr) : bStr);
            } else {
                sb.append(StrUtil.isNotBlank(huaseMap.get(bStr.substring(0, 1))) ? huaseMap.get(bStr.substring(0, 1)) : bStr.substring(0, 1));
                sb.append(StrUtil.isNotBlank(paiMap.get(bStr.substring(1, 2))) ? paiMap.get(bStr.substring(1, 2)) : bStr.substring(1, 2));
            }

            if (cStr.substring(0, 1).equals("4")) {
                sb.append(StrUtil.isNotBlank(huaseMap.get(cStr)) ? huaseMap.get(cStr) : cStr);
            } else {
                sb.append(StrUtil.isNotBlank(huaseMap.get(cStr.substring(0, 1))) ? huaseMap.get(cStr.substring(0, 1)) : cStr.substring(0, 1));
                sb.append(StrUtil.isNotBlank(paiMap.get(cStr.substring(1, 2))) ? paiMap.get(cStr.substring(1, 2)) : cStr.substring(1, 2));
            }

            return sb.toString();
        }
    }


    /**
     * <B>获取信息</B>
     *
     * @param shape
     * @return
     */
    public String getShapeValue(String shape) {
        if (StrUtil.isNotBlank(shape) && shape.equals("1")) {
            return "特殊";
        } else if (StrUtil.isNotBlank(shape) && shape.equals("2")) {
            return "单牌";
        } else if (StrUtil.isNotBlank(shape) && shape.equals("3")) {
            return "对子";

        } else if (StrUtil.isNotBlank(shape) && shape.equals("4")) {
            return "顺子";
        } else if (StrUtil.isNotBlank(shape) && shape.equals("5")) {
            return "同花";
        } else if (StrUtil.isNotBlank(shape) && shape.equals("6")) {
            return "同花顺 ";

        } else if (StrUtil.isNotBlank(shape) && shape.equals("7")) {
            return "三条/炸弹/豹子";
        }

        return "";
    }


    public String splitZjhUserStr(String str) {
        StringBuffer sb = new StringBuffer();
        Object[] strArray = new Gson().fromJson(str, Object[].class);

        if (strArray != null && strArray.length > 0) {
            for (Object strArr : strArray) {
                Map map = (Map) strArr;

                String score = map.get("Chair") instanceof Double ? new Long(Math.round((Double) map.get("Chair"))).toString() : (String) map.get("Chair");
                String isRobot = map.get("IsRobot").equals("0") ? "真人" : "机器人";
                String userId = map.get("UserID") instanceof Double ? new Long(Math.round((Double) map.get("UserID"))).toString() : (String) map.get("UserID");

                sb.append("用户(" + userId + "):");
                sb.append("是否机器人(" + isRobot + "):");
                sb.append("椅子:" + score);
                sb.append(";");
            }
        }
        return sb.toString();
    }


    /**
     * <B>处理斗地主牌局</B>
     *
     * @return
     */
    public IPage<Map<String, Object>> handlerDdzBoard(IPage<Map<String, Object>> ddzPage) {
        if (ddzPage != null && ddzPage.getRecords() != null && ddzPage.getRecords().size() > 0) {
            List list = ddzPage.getRecords();

            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> map = (Map<String, Object>) list.get(i);

                //该局输赢积分
                String loseOrWinIntegalName = (String) map.get("LoseOrWinIntegal");
                StringBuffer syjfSb = new StringBuffer();
                if (StrUtil.isNotBlank(loseOrWinIntegalName)) {
                    syjfSb.append(splitDdzJfStr(loseOrWinIntegalName));
                }
                map.put("loseOrWinIntegalName", syjfSb.toString());

                //结算积分
                String endIntegralName = (String) map.get("EndIntegral");
                StringBuffer jsjfSb = new StringBuffer();
                if (StrUtil.isNotBlank(endIntegralName)) {
                    jsjfSb.append(splitDdzJsjfStr(endIntegralName));
                }
                map.put("endIntegralName", jsjfSb.toString());

                //初始牌信息
                String recordInfoName = (String) map.get("RecordInfo");
                StringBuffer csspSb = new StringBuffer();
                if (StrUtil.isNotBlank(recordInfoName)) {
                    csspSb.append(splitDdzCspStr(recordInfoName));
                }
                map.put("recordInfoName", csspSb.toString());

                //用户信息
                String userInfoName = (String) map.get("UserInfo");
                StringBuffer userSb = new StringBuffer();
                if (StrUtil.isNotBlank(userInfoName)) {
                    userSb.append(splitDdzUserStr(userInfoName));
                }
                map.put("userInfoName", userSb.toString());
            }
        }
        return ddzPage;
    }


    public String splitDdzCspStr(String str) {
        StringBuffer sb = new StringBuffer();
        Object[] strArray = new Gson().fromJson(str, Object[].class);

        if (strArray != null && strArray.length > 0) {
            for (Object strArr : strArray) {
                Map map = (Map) strArr;

                if (map.get("ChairID") != null) {
                    sb.append("椅子号:" + new Long(Math.round((Double) map.get("ChairID"))).toString());
                    sb.append(",");
                }

                if (map.get("CallCent") != null) {
                    sb.append("叫分:" + new Long(Math.round((Double) map.get("CallCent"))).toString());
                    sb.append(",");
                }

                if (map.get("IsDouble") != null) {
                    String isDoubleValue = "";
                    if (map.get("IsDouble") instanceof  Integer) {
                        isDoubleValue =  ((Integer)map.get("IsDouble")) == 1 ? "加倍" : "不加倍";
                    } else if (map.get("IsDouble") instanceof  Double) {
                        isDoubleValue = ((Double)map.get("IsDouble")) == 1 ? "加倍" : "不加倍";
                    }

                    sb.append("是否加倍:" + isDoubleValue);
                    sb.append(",");
                }

                if (map.get("IsLandOpen") != null) {
                    String isDoubleValue = map.get("IsLandOpen").equals("0") ? "明牌" : "不明牌";

                    sb.append("是否明牌:" + isDoubleValue);
                    sb.append(",");
                }

                if (map.get("LandCards") != null) {
                    sb.append("底牌:" + map.get("LandCards"));
                    sb.append(",");
                }

                if (map.get("LastDouble") != null) {
                    sb.append("底牌倍数:" + new Long(Math.round((Double) map.get("LastDouble"))).toString());
                    sb.append(",");
                }

                if (map.get("OpenMul") != null) {
                    sb.append("明牌倍数:" + new Long(Math.round((Double) map.get("OpenMul"))).toString());
                    sb.append(",");
                }

                if (map.get("OrginCart") != null) {
                    String orginCart = analysisDdzCard((String) map.get("OrginCart"));
                    sb.append("手牌:" + orginCart);
                    sb.append(",");
                }

                if (map.get("Score") != null) {
                    sb.append("初始金币:" + new Long(Math.round((Double) map.get("Score"))).toString());
                    sb.append(",");
                }

                if (map.get("Total") != null) {
                    sb.append("手牌权值:" + new Long(Math.round((Double) map.get("Total"))).toString());
                    sb.append(",");
                }

                sb.append("\r");
            }
        }
        return sb.toString();
    }


    /**
     * <B>解析斗地主手牌</B>
     *
     * @param card
     * @return
     */
    private String analysisDdzCard(String card) {
        StringBuffer sb = new StringBuffer();
        if (StrUtil.isNotBlank(card)) {
            int startIndex = 0;
            int endIndex = 2;

            while (endIndex < card.length()) {
                String str = card.substring(startIndex, endIndex);
                sb.append(ddzCardValueMap.get(str));

                startIndex += 2;
                endIndex += 2;
            }
        }
        return sb.toString();
    }


    public String splitDdzUserStr(String str) {
        StringBuffer sb = new StringBuffer();
        Object[] strArray = new Gson().fromJson(str, Object[].class);

        if (strArray != null && strArray.length > 0) {
            for (Object strArr : strArray) {
                Map map = (Map) strArr;

                String score = map.get("Chair") instanceof Double ? new Long(Math.round((Double) map.get("Chair"))).toString() : (String) map.get("Chair");
                String userId = map.get("UserID") instanceof Double ? new Long(Math.round((Double) map.get("UserID"))).toString() : (String) map.get("UserID");

                sb.append("用户(" + userId + "):");
                sb.append("椅子:" + score);
                sb.append(";");
            }
        }
        return sb.toString();
    }


    public String splitDdzJfStr(String str) {
        StringBuffer sb = new StringBuffer();
        Object[] strArray = new Gson().fromJson(str, Object[].class);

        if (strArray != null && strArray.length > 0) {
            for (Object strArr : strArray) {
                Map map = (Map) strArr;

                String score = map.get("Score") instanceof Double ? new Long(Math.round((Double) map.get("Score"))).toString() : (String) map.get("Score");
                String userId = map.get("UserID") instanceof Double ? new Long(Math.round((Double) map.get("UserID"))).toString() : (String) map.get("UserID");

                sb.append("用户(" + userId + "):");
                sb.append("得分:" + score);
                sb.append(";");
            }
        }
        return sb.toString();
    }


    public String splitDdzJsjfStr(String str) {
        StringBuffer sb = new StringBuffer();
        Object[] strArray = new Gson().fromJson(str, Object[].class);

        if (strArray != null && strArray.length > 0) {
            for (Object strArr : strArray) {
                Map map = (Map) strArr;

                String score = map.get("AfterScore") instanceof Double ? new Long(Math.round((Double) map.get("AfterScore"))).toString() : (String) map.get("AfterScore");
                String userId = map.get("UserID") instanceof Double ? new Long(Math.round((Double) map.get("UserID"))).toString() : (String) map.get("UserID");

                sb.append("用户(" + userId + "):");
                sb.append("得分:" + score);
                sb.append(";");
            }
        }
        return sb.toString();
    }


    /**
     * <B>处理捕鱼牌局</B>
     *
     * @return
     */
    public IPage<Map<String, Object>> handlerByBoard(IPage<Map<String, Object>> byPage) {
        if (byPage != null && byPage.getRecords() != null && byPage.getRecords().size() > 0) {
            List list = byPage.getRecords();
            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> map = (Map<String, Object>) list.get(i);

                //处理map信息
                String recordInfo = (String) map.get("RecordInfo");
                StringBuffer sb = new StringBuffer();

                if (StrUtil.isNotBlank(recordInfo)) {
                    //判断字符串是否含有"-"
                    if (recordInfo.contains(BoardHandler.AND_SUFFIX)) {
                        //如果有-分隔
                        String[] strArray = recordInfo.split(BoardHandler.AND_SUFFIX);
                        for (String arrayStr : strArray) {
                            String str = splitBuYuStr(arrayStr);
                            sb.append(str);
                        }
                    } else {
                        if (recordInfo.contains(BoardHandler.SPLIT_SUFFIX)) {
                            String str = splitBuYuStr(recordInfo);
                            sb.append(str);
                        }
                    }
                    map.put("cardRecordName", sb.toString());
                } else {
                    map.put("cardRecordName", "");
                }
            }
        }
        return byPage;
    }


    private String splitBuYuStr(String str) {
        StringBuffer sb = new StringBuffer();
        if (str.contains(BoardHandler.SPLIT_SUFFIX)) {

            String[] strDesc = str.split(BoardHandler.SPLIT_SUFFIX);
            for (int j = 0; j < strDesc.length; j++) {
                String fishName = fishMap.get(strDesc[j]) == null ? strDesc[j] : fishMap.get(strDesc[j]);

                if (j == 0) {
                    sb.append(fishName + "(");
                } else if (j == 1) {
                    sb.append(strDesc[j] + "倍)打死");
                } else if (j == 2) {
                    sb.append(strDesc[j] + "条");
                }
            }
            sb.append("\n");
        }

        return sb.toString();
    }


    /**
     * <B>处理牛牛牌局</B>
     *
     * @return
     */
    public IPage<Map<String, Object>> handlerNnBoard(IPage<Map<String, Object>> nnPage) {
        if (nnPage != null && nnPage.getRecords() != null && nnPage.getRecords().size() > 0) {
            List list = nnPage.getRecords();

            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> map = (Map<String, Object>) list.get(i);

                //牌型
                String cardRecordName = (String) map.get("CardRecord");
                StringBuffer pxsb = new StringBuffer();

                //判断字符串是否为空
                if (StrUtil.isNotBlank(cardRecordName)) {
                    //判断逗号分隔
                    if (cardRecordName.contains(BoardHandler.FEN_SUFFIX)) {
                        //如果有, 分隔
                        String[] strArray = cardRecordName.split(BoardHandler.FEN_SUFFIX);

                        if (strArray != null && strArray.length > 0) {
                            for (int j = 0; j < strArray.length; j++) {
                                String cardStr = strArray[j];
                                nnBoardHead(pxsb, j);

                                //切割冒号
                                if (StrUtil.isNotBlank(cardStr) && cardStr.contains(BoardHandler.COLON_SUFFIX)) {
                                    String str = splitNnPxStr(cardStr);
                                    pxsb.append(str);
                                } else {
                                    String str = splitNnPxStr(cardRecordName);
                                    pxsb.append(str);
                                }

                                pxsb.append("\n");
                            }
                        }
                    }
                }
                map.put("cardRecordName", pxsb.toString());

                //下注
                String betDetailName = (String) map.get("BetDetail");
                StringBuffer xzsb = new StringBuffer();

                if (StrUtil.isNotBlank(betDetailName)) {
                    //判断 - 分隔
                    if (betDetailName.contains(BoardHandler.SPLIT_SUFFIX)) {
                        //如果有-分隔
                        String[] strArray = betDetailName.split(BoardHandler.SPLIT_SUFFIX);
                        if (strArray != null && strArray.length == 2) {
                            xzsb.append(splitNnXzStr(strArray[0]));
                            xzsb.append(splitNnJsStr(strArray[1]));
                        } else if (strArray != null && strArray.length == 1 && strArray[0].length() > 5) {
                            xzsb.append(splitNnJsStr(strArray[0].substring(1, strArray[0].length())));
                        }
                    }
                }
                map.put("betDetailName", xzsb.toString());
            }
        }
        return nnPage;
    }


    private void nnBoardHead(StringBuffer sb, int index) {
        if (index == 0) {
            sb.append("庄家牌型-");
        } else if (index == 1) {
            sb.append("天区牌型-");
        } else if (index == 2) {
            sb.append("地区牌型-");
        } else if (index == 3) {
            sb.append("玄区牌型-");
        } else if (index == 4) {
            sb.append("黄区牌型-");
        }
    }


    private String splitNnUserStr(String str) {
        StringBuffer sb = new StringBuffer();
        if (str.contains(BoardHandler.FEN_SUFFIX)) {
            String[] strDesc = str.split(BoardHandler.FEN_SUFFIX);

            //前面是牌数
            if (strDesc != null && strDesc.length == 2) {
                String px = "用户(" + strDesc[0] + ")";
                sb.append(px);
                sb.append(":");

                if (StrUtil.isNotBlank(strDesc[1])) {
                    if (strDesc[1].equals("0")) {
                        sb.append("闲家");
                    } else if (strDesc[1].equals("1")) {
                        sb.append("庄家");
                    } else if (strDesc[1].equals("2")) {
                        sb.append("未下注");
                    }
                }
                sb.append(";");
            }
        }
        return sb.toString();
    }


    private String splitNnXzStr(String str) {
        StringBuffer sb = new StringBuffer();
        if (str.contains(BoardHandler.SPACE_SUFFIX)) {
            String[] strDesc = str.split(BoardHandler.SPACE_SUFFIX);

            //前面是牌数
            if (strDesc != null && strDesc.length == 4) {
                for (String betStr : strDesc) {

                    //判断是否有,号
                    if (StrUtil.isNotBlank(betStr) && betStr.contains(BoardHandler.COLON_SUFFIX)) {
                        String[] betArray = betStr.split(BoardHandler.COLON_SUFFIX);
                        if (StrUtil.isNotBlank(betArray[0]) && betArray[0].equals("1")) {
                            sb.append("天区下注:" + betArray[1] + " ");
                        } else if (StrUtil.isNotBlank(betArray[0]) && betArray[0].equals("2")) {
                            sb.append("地区下注:" + betArray[1] + " ");
                        } else if (StrUtil.isNotBlank(betArray[0]) && betArray[0].equals("3")) {
                            sb.append("玄区下注:" + betArray[1] + " ");
                        } else if (StrUtil.isNotBlank(betArray[0]) && betArray[0].equals("4")) {
                            sb.append("黄区下注:" + betArray[1] + " ");
                        }
                    }
                }

                sb.append("\r");
            }
        }
        return sb.toString();
    }


    private String splitNnJsStr(String str) {
        StringBuffer sb = new StringBuffer();
        if (str.contains(BoardHandler.SPACE_SUFFIX)) {
            String[] strDesc = str.split(BoardHandler.SPACE_SUFFIX);

            //前面是牌数
            if (strDesc != null && strDesc.length == 4) {
                for (String betStr : strDesc) {

                    //判断是否有,号
                    if (StrUtil.isNotBlank(betStr) && betStr.contains(BoardHandler.COLON_SUFFIX)) {
                        String[] betArray = betStr.split(BoardHandler.COLON_SUFFIX);
                        if (StrUtil.isNotBlank(betArray[0]) && betArray[0].equals("1")) {
                            sb.append("天区结算:" + betArray[1] + " ");
                        } else if (StrUtil.isNotBlank(betArray[0]) && betArray[0].equals("2")) {
                            sb.append("地区结算:" + betArray[1] + " ");
                        } else if (StrUtil.isNotBlank(betArray[0]) && betArray[0].equals("3")) {
                            sb.append("玄区结算:" + betArray[1] + " ");
                        } else if (StrUtil.isNotBlank(betArray[0]) && betArray[0].equals("4")) {
                            sb.append("黄区结算:" + betArray[1] + " ");
                        }
                    }
                }

                sb.append("\r");
            }
        }
        return sb.toString();
    }

    private String splitNnPxStr(String str) {
        StringBuffer sb = new StringBuffer();
        if (str.contains(BoardHandler.COLON_SUFFIX)) {
            String[] strDesc = str.split(BoardHandler.COLON_SUFFIX);

            //前面是牌数
            if (strDesc != null && strDesc.length == 2) {
                String px = " ";
                if (strDesc[0].equals("10")) {
                    px = "牛牛";
                } else if (strDesc[0].equals("0")) {
                    px = "没牛";
                } else {
                    px = "牛" + strDesc[0];
                }

                sb.append(px);
                sb.append(":");
                if (StrUtil.isNotBlank(strDesc[1]) && strDesc[1].contains(BoardHandler.SPLIT_SUFFIX)) {
                    String[] paiArray = strDesc[1].split(BoardHandler.SPLIT_SUFFIX);

                    if (paiArray != null && paiArray.length > 0) {
                        for (String paiStr : paiArray) {
                            String paiA = paiStr.substring(0, 1);
                            String paiB = paiStr.substring(1, 2);

                            if (paiA.equals("4")) {
                                sb.append(StrUtil.isNotBlank(huaseMap.get(paiStr)) ? huaseMap.get(paiStr) : paiStr);
                            } else {
                                sb.append(StrUtil.isNotBlank(huaseMap.get(paiA)) ? huaseMap.get(paiA) : paiStr.substring(0, 1));
                                sb.append(StrUtil.isNotBlank(paiMap.get(paiB)) ? paiMap.get(paiB) : paiStr.substring(1, 2));
                            }
                        }
                    }
                }
                sb.append("\r");
            }
        }
        return sb.toString();
    }


    /**
     * <B>处理龙虎牌局</B>
     *
     * @return
     */
    public IPage<Map<String, Object>> handlerLhBoard(IPage<Map<String, Object>> lhPage) {
        if (lhPage != null && lhPage.getRecords() != null && lhPage.getRecords().size() > 0) {
            List list = lhPage.getRecords();

            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> map = (Map<String, Object>) list.get(i);

                //处理map信息
                //CardRecord
                String cardRecord = (String) map.get("CardRecord");
                if (StrUtil.isNotBlank(cardRecord) && cardRecord.equals("1")) {
                    map.put("cardRecordName", "和");
                } else if (StrUtil.isNotBlank(cardRecord) && cardRecord.equals("2")) {
                    map.put("cardRecordName", "龙");
                } else if (StrUtil.isNotBlank(cardRecord) && cardRecord.equals("3")) {
                    map.put("cardRecordName", "虎");
                }

                //下注详情
                String betDetail = (String) map.get("BetDetail");
                StringBuffer sb = new StringBuffer();

                if (StrUtil.isNotBlank(betDetail)) {
                    //判断字符串是否含有"-"

                    if (betDetail.contains(BoardHandler.AND_SUFFIX)) {
                        String[] andArray = betDetail.split(BoardHandler.AND_SUFFIX);
                        if (andArray != null && andArray.length > 0) {

                            for (int j = 0; j < andArray.length; j++) {
                                if (j == 0) {
                                    sb.append("下注-");
                                }
                                if (j == 1) {
                                    sb.append("得分-");
                                }

                                String str = andArray[j];
                                if (str.contains(BoardHandler.SPACE_SUFFIX)) {
                                    //如果有 分隔
                                    String[] strArray = str.split(BoardHandler.SPACE_SUFFIX);
                                    for (int k = 0; k < strArray.length; k++) {
                                        String betStr = splitStr(strArray[k]);
                                        sb.append(betStr);
                                    }
                                } else {
                                    if (betDetail.contains(BoardHandler.COLON_SUFFIX)) {
                                        String betStr = splitStr(betDetail);
                                        sb.append(betStr);
                                    }
                                }
                            }
                        }
                    }

                    map.put("betDetailName", sb.toString());
                } else {
                    map.put("betDetailName", "");
                }
            }
        }
        return lhPage;
    }


    private String splitStr(String str) {
        StringBuffer sb = new StringBuffer();
        if (str.contains(BoardHandler.COLON_SUFFIX)) {

            String[] strDesc = str.split(BoardHandler.COLON_SUFFIX);
            //判断数组长度,如果为6是龙虎和，反之是牛牛

            for (int j = 0; j < strDesc.length; j++) {
                if (j == 0) {
                    if (strDesc[j].equals("1")) {
                        sb.append("和:");
                    } else if (strDesc[j].equals("2")) {
                        sb.append("龙:");
                    } else if (strDesc[j].equals("3")) {
                        sb.append("虎:");
                    }
                } else if (j == 1) {
                    sb.append(strDesc[j]);
                }
            }
            sb.append("\n");
        }

        return sb.toString();
    }

}







