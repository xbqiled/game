package com.youwan.game.report.service.impl;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.common.core.util.TimeUtil;
import com.youwan.game.report.api.dto.ScoreDto;
import com.youwan.game.report.api.entity.ScoreRecord;
import com.youwan.game.report.mapper.ScoreRecordMapper;
import com.youwan.game.report.service.ScoreRecordService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import static com.youwan.game.common.core.constant.CommonConstant.SECONDS_OF_DAY;
import static com.youwan.game.common.core.constant.ConfigConstant.DATE_FORMAT;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-25 12:22:25
 */
@Service("scoreRecordService")
public class ScoreRecordServiceImpl extends ServiceImpl<ScoreRecordMapper, ScoreRecord> implements ScoreRecordService {

    @Override
    public IPage queryPageList(Page page, ScoreDto scoreDto) {
        Integer start = null, end = null;
        assert scoreDto != null;
        start = StrUtil.isNotBlank(scoreDto.getStartTime()) ? TimeUtil.stringToUnixTimeStamp(scoreDto.getStartTime(),
                DATE_FORMAT) : null;
        end = StrUtil.isNotBlank(scoreDto.getEndTime()) ? TimeUtil.stringToUnixTimeStamp(scoreDto.getEndTime(),
                DATE_FORMAT) + SECONDS_OF_DAY : null;
        return baseMapper.queryPageList(page,
                scoreDto.getChannelNo(),
                scoreDto.getType(),
                StringUtils.isNumeric(scoreDto.getAccountId()) ? Integer.parseInt(scoreDto.getAccountId()) : null,
                start,
                end);
    }
}
