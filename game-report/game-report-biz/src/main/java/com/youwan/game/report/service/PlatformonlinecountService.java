package com.youwan.game.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.report.api.entity.Platformonlinecount;


/**
 * 平台实时在线记录
 *
 * @author code generator
 * @date 2019-01-25 13:12:14
 */
public interface PlatformonlinecountService extends IService<Platformonlinecount> {

}
