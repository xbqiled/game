package com.youwan.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.report.api.entity.Logingamerecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-25 17:23:37
 */
public interface LogingamerecordMapper extends BaseMapper<Logingamerecord> {

    /**
     * <B>查询登录游戏信息</B>
     * @param page
     * @param channelId
     * @param userId
     * @param startTime
     * @param endTime
     * @return
     */
    IPage<Map<String, Object>> queryLoginGameReport(Page page, @Param("channelId") String channelId, @Param("userId") Integer userId, @Param(
            "startTime") Integer startTime, @Param("endTime") Integer endTime);
}
