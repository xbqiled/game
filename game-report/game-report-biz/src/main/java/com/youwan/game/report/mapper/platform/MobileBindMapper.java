package com.youwan.game.report.mapper.platform;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-02.
 */
public interface MobileBindMapper {

    List<Map<String, Object>> queryChannelIdsBy3rdIds(@Param("ids") Set<Integer> tpIds);

    Integer getCpStationIdByChannel(@Param("channelId") String channelId);
}
