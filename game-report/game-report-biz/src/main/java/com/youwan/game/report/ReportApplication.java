package com.youwan.game.report;


import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.youwan.game.common.security.annotation.EnableGameFeignClients;
import com.youwan.game.common.security.annotation.EnableGameResourceServer;
import com.youwan.game.common.swagger.annotation.EnableSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * @author lion
 * @date 2018年06月21日
 * 报表展示管理系统
 */
@EnableSwagger2
@SpringCloudApplication
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, DruidDataSourceAutoConfigure.class})
@EnableGameFeignClients
@EnableGameResourceServer(details = true)
public class ReportApplication {
	public static void main(String[] args) {
		SpringApplication.run(ReportApplication.class, args);
	}
}
