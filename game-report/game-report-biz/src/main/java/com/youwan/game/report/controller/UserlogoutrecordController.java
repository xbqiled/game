package com.youwan.game.report.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.report.api.dto.QueryDto;
import com.youwan.game.report.api.entity.Userlogoutrecord;
import com.youwan.game.report.service.UserlogoutrecordService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 玩家登出平台记录表
 *
 * @author code generator
 * @date 2019-01-25 11:48:53
 */
@RestController
@AllArgsConstructor
@RequestMapping("/userlogoutRecord")
public class UserlogoutrecordController {

  private final UserlogoutrecordService userlogoutrecordService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param queryDto 玩家登出平台记录表
   * @return
   */
  @GetMapping("/page")
  public R getUserlogoutrecordPage(Page page, QueryDto queryDto) {
    IPage iPage = userlogoutrecordService.queryLogoutReport(page, queryDto);
    if (iPage == null) {
      return new R<>(false, "未找到相关数据!");
    } else {
      return  new R<>(iPage);
    }
  }


  /**
   * 通过id查询玩家登出平台记录表
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(userlogoutrecordService.getById(id));
  }

  /**
   * 新增玩家登出平台记录表
   * @param userlogoutrecord 玩家登出平台记录表
   * @return R
   */
  @SysLog("新增玩家登出平台记录表")
  @PostMapping
  public R save(@RequestBody Userlogoutrecord userlogoutrecord){
    return new R<>(userlogoutrecordService.save(userlogoutrecord));
  }

  /**
   * 修改玩家登出平台记录表
   * @param userlogoutrecord 玩家登出平台记录表
   * @return R
   */
  @SysLog("修改玩家登出平台记录表")
  @PutMapping
  public R updateById(@RequestBody Userlogoutrecord userlogoutrecord){
    return new R<>(userlogoutrecordService.updateById(userlogoutrecord));
  }

  /**
   * 通过id删除玩家登出平台记录表
   * @param id id
   * @return R
   */
  @SysLog("删除玩家登出平台记录表")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(userlogoutrecordService.removeById(id));
  }

}
