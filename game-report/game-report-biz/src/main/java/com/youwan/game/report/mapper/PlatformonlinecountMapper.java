package com.youwan.game.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.report.api.entity.Platformonlinecount;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 平台实时在线记录
 *
 * @author code generator
 * @date 2019-01-25 13:12:14
 */
public interface PlatformonlinecountMapper extends BaseMapper<Platformonlinecount> {


    /**
     * <B>分页查询</B>
     *
     * @param tableName
     * @param ChannelNo
     * @return
     */
    IPage<List<Platformonlinecount>> queryPageList(Page page, @Param("tableName") String tableName, @Param("ChannelNo") Integer ChannelNo);


    /**
     * <B>查询是否存在表名</B>
     * @param tableName
     * @return
     */
    List<Map<String, Object>> queryTableList(@Param("tableName") String tableName);
}
