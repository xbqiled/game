package com.youwan.game.report.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.report.api.entity.Platformonlinecount;
import com.youwan.game.report.service.PlatformonlinecountService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 平台实时在线记录
 *
 * @author code generator
 * @date 2019-01-25 13:12:14
 */
@RestController
@AllArgsConstructor
@RequestMapping("/platformonlinecount")
public class PlatformonlinecountController {

  private final PlatformonlinecountService platformonlinecountService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param platformonlinecount 平台实时在线记录
   * @return
   */
  @GetMapping("/page")
  public R getPlatformonlinecountPage(Page page, Platformonlinecount platformonlinecount) {
    return  new R<>(platformonlinecountService.page(page, Wrappers.query(platformonlinecount)));
  }


  /**
   * 通过id查询平台实时在线记录
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(platformonlinecountService.getById(id));
  }

  /**
   * 新增平台实时在线记录
   * @param platformonlinecount 平台实时在线记录
   * @return R
   */
  @SysLog("新增平台实时在线记录")
  @PostMapping
  public R save(@RequestBody Platformonlinecount platformonlinecount){
    return new R<>(platformonlinecountService.save(platformonlinecount));
  }

  /**
   * 修改平台实时在线记录
   * @param platformonlinecount 平台实时在线记录
   * @return R
   */
  @SysLog("修改平台实时在线记录")
  @PutMapping
  public R updateById(@RequestBody Platformonlinecount platformonlinecount){
    return new R<>(platformonlinecountService.updateById(platformonlinecount));
  }

  /**
   * 通过id删除平台实时在线记录
   * @param id id
   * @return R
   */
  @SysLog("删除平台实时在线记录")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(platformonlinecountService.removeById(id));
  }

}
