package com.youwan.game.report.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.common.core.util.TimeUtil;
import com.youwan.game.report.api.dto.QueryDto;
import com.youwan.game.report.api.entity.Rechargeplatformorder;
import com.youwan.game.report.mapper.RechargeplatformorderMapper;
import com.youwan.game.report.service.RechargeplatformorderService;
import org.springframework.stereotype.Service;

import static com.youwan.game.common.core.constant.CommonConstant.SECONDS_OF_DAY;
import static com.youwan.game.common.core.constant.ConfigConstant.DATE_FORMAT;

/**
 * 充值日志(平台)
 *
 * @author code generator
 * @date 2019-01-27 16:14:56
 */
@Service("rechargeplatformorderService")
public class RechargeplatformorderServiceImpl extends ServiceImpl<RechargeplatformorderMapper, Rechargeplatformorder> implements RechargeplatformorderService {

    @Override
    public IPage queryPageList(Page page, QueryDto queryDto) {
        Integer start = null, end = null;
        if (queryDto != null) {
            start = StrUtil.isNotBlank(queryDto.getStartTime()) ? TimeUtil.stringToUnixTimeStamp(queryDto.getStartTime(),
                    DATE_FORMAT) : null;
            end = StrUtil.isNotBlank(queryDto.getEndTime()) ? TimeUtil.stringToUnixTimeStamp(queryDto.getEndTime(),
                    DATE_FORMAT) + SECONDS_OF_DAY : null;
        }
        return baseMapper.queryPageList(page, queryDto == null ? null : queryDto.getUserid(), start, end);
    }
}
