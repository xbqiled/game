package com.youwan.game.report.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.common.core.constant.ConfigConstant;
import com.youwan.game.common.core.jsonmodel.GsonGameState;
import com.youwan.game.common.core.jsonmodel.GsonPlatformState;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.HttpHandler;
import com.youwan.game.report.api.dto.GmActionDto;
import com.youwan.game.report.api.dto.MaintDto;
import com.youwan.game.report.api.entity.Gameroom;
import com.youwan.game.report.mapper.GameroomMapper;
import com.youwan.game.report.service.GameroomService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 
 *
 * @author code generator
 * @date 2019-03-15 20:02:39
 */
@Slf4j
@Service("gameroomService")
public class GameroomServiceImpl extends ServiceImpl<GameroomMapper, Gameroom> implements GameroomService {


    @Override
    public List<Map<String, Object>> getGamePage(String status) {
        //调用接口获取
        List<GsonGameState> gamelist = HttpHandler.getGameState();
        List<Gameroom> list = this.list();
        List<Map<String, Object>> resultList = new ArrayList<>();

       //判断list是否为空
       if (CollUtil.isNotEmpty(list) && CollUtil.isNotEmpty(list)) {
           for (GsonGameState game : gamelist) {
               Map<String , Object> map = new HashMap<>();
               for (Gameroom room : list) {

                   if (game.getId() == room.getGameatomtypeid()) {
                       map.put("start", game.getStart() == 0 || String.valueOf(game.getStart()).length() < 10 ? "MAX" : DateUtil.format(new Date(game.getStart() * 1000), ConfigConstant.FULL_DATE_TIME_FORMAT));
                       map.put("finish", game.getFinish() == 0 || String.valueOf(game.getFinish()).length() < 10 ? "MAX" : DateUtil.format(new Date(game.getFinish() * 1000), ConfigConstant.FULL_DATE_TIME_FORMAT));

                       map.put("num", game.getNum());
                       map.put("roomId",  game.getId());
                       map.put("status",  game.getStatus());

                       map.put("gamekindname",  room.getGamekindname());
                       map.put("phonegamename",  room.getPhonegamename());

                       if (StrUtil.isNotBlank(status)) {
                           if (game.getStatus()  == Integer.parseInt(status)) {
                               resultList.add(map);
                           }
                       } else {
                           resultList.add(map);
                       }
                   }
               }
           }
       }
        return resultList;
    }


    @Override
    public  List<Map<String, Object>>  getPlatformPage() {
        //调用接口获取
        List<Map<String, Object>> resultList = new ArrayList<>();
        GsonPlatformState gson = HttpHandler.getPlatformState();

        if (gson != null) {
            Map<String, Object> map = new HashMap<>();
            map.put("start", gson.getStart() == 0 || String.valueOf(gson.getStart()).length() < 10 ? "MAX" : DateUtil.format(new Date(gson.getStart() * 1000), ConfigConstant.FULL_DATE_TIME_FORMAT));
            map.put("finish", gson.getFinish() == 0 || String.valueOf(gson.getFinish()).length() < 10 ? "MAX" : DateUtil.format(new Date(gson.getFinish() * 1000), ConfigConstant.FULL_DATE_TIME_FORMAT));

            map.put("status", gson.getStatus());
            map.put("name", "整个平台");
            resultList.add(map);
        }
        return resultList;
    }


    /**
     * <B>处理游戏维护</B>
     * @return
     */
    @Override
    public Boolean gameMaint(MaintDto maintDto) {
        //判断是维护还是取消维护
        String startDate = "";
        String endDate = "";
        if (maintDto.getToStatus() == 2) {
           //取消维护
            startDate = "0";
            endDate = "0";
        }  else {
            //维护
            startDate = maintDto.getStartMaintTime().substring(0, 10);
            endDate = maintDto.getEndMaintTime().substring(0, 10);
        }

        List<Integer> list = new ArrayList<>();
        list.add(maintDto.getRoomId());
        Integer gameList [] = new Integer [list.size()];

        GsonResult result = HttpHandler.doGameMaintenance(maintDto.getToStatus(), Integer.parseInt(startDate), Integer.parseInt(endDate), list.toArray(gameList));
        if (result != null && result.getRet() == 0) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    /**
     * <B>处理平台维护</B>
     * @param platformDto
     * @return
     */
    @Override
    public Boolean platformMaint(MaintDto platformDto) {
        //判断是维护还是取消维护
        String platformStartDate = "";
        String platformEndDate = "";
        if (platformDto.getToStatus() == 2) {
            //取消维护
            platformStartDate = "0";
            platformEndDate = "0";
        }  else {
            //维护
            platformStartDate = platformDto.getStartMaintTime().substring(0, 10);
            platformEndDate = platformDto.getEndMaintTime().substring(0, 10);
        }

        GsonResult result = HttpHandler.doPlatfromMaintenance(platformDto.getToStatus(), Integer.parseInt(platformStartDate), Integer.parseInt(platformEndDate));
        if (result != null && result.getRet() == 0) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }


    @Override
    public Boolean gmAction(GmActionDto action) {
        GsonResult result = HttpHandler.gmAction(action.getDomainCode(), action.getCmdAction());
        if (result != null && result.getRet() == 0) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}
