package com.youwan.game.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.report.api.entity.Recorduserscorepergame;


/**
 * 
 *
 * @author code generator
 * @date 2019-01-29 13:42:42
 */
public interface RecorduserscorepergameService extends IService<Recorduserscorepergame> {

}
