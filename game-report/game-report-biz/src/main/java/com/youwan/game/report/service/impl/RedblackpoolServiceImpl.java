package com.youwan.game.report.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.common.core.util.TimeUtil;
import com.youwan.game.report.api.dto.ParameterDto;
import com.youwan.game.report.api.dto.QueryDto;
import com.youwan.game.report.api.entity.Redblackpool;
import com.youwan.game.report.mapper.RedblackpoolMapper;
import com.youwan.game.report.service.RedblackpoolService;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static com.youwan.game.common.core.constant.CommonConstant.SECONDS_OF_DAY;
import static com.youwan.game.common.core.constant.ConfigConstant.DATE_FORMAT;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-03 13:55:49
 */
@Service("redblackpoolService")
public class RedblackpoolServiceImpl extends ServiceImpl<RedblackpoolMapper, Redblackpool> implements RedblackpoolService {

    @Override
    public IPage<Map<String, Object>> queryHhdzPoolPageList(Page page, ParameterDto parameterDto) {
        Integer start = null, end = null;
        assert parameterDto != null;

        start = StrUtil.isNotBlank(parameterDto.getQueryTime()) ? TimeUtil.stringToUnixTimeStamp(parameterDto.getQueryTime(),
                DATE_FORMAT) : null;
        if (start != null)
            end = start + SECONDS_OF_DAY;
        return baseMapper.queryHhdzPoolPage(page,
                parameterDto.getChannelNo(),
                StringUtils.isNumeric(parameterDto.getPeriodical()) ? Integer.parseInt(parameterDto.getPeriodical()) : null,
                StringUtils.isNumeric(parameterDto.getAccountId()) ? Integer.parseInt(parameterDto.getAccountId()) : null,
                parameterDto.getRecordId(), start, end);
    }
}
