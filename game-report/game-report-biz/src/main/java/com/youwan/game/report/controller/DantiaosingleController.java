package com.youwan.game.report.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.common.core.util.R;
import com.youwan.game.report.api.dto.DantiaosingleDto;
import com.youwan.game.report.service.DantiaosingleService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * VIEW
 *
 * @author code generator
 * @date 2020-04-18 12:19:42
 */
@RestController
@AllArgsConstructor
@RequestMapping("/dantiaosingle")
public class DantiaosingleController {

  private final DantiaosingleService dantiaosingleService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param dantiaosingleDto VIEW
   * @return
   */
  @GetMapping("/page")
  public R getDantiaosinglePage(Page page, DantiaosingleDto dantiaosingleDto) {
    return  new R<>(dantiaosingleService.querySinglePageList(page, dantiaosingleDto));
  }



}
