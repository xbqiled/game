package com.youwan.game.report.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.common.core.util.R;
import com.youwan.game.report.api.dto.QueryDto;
import com.youwan.game.report.service.TreasurechangeService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author code generator
 * @date 2019-01-25 13:11:34
 */
@RestController
@AllArgsConstructor
@RequestMapping("/treasureChange")
public class TreasurechangeController {

  private final TreasurechangeService usertreasurechangeService;

  /**
   * <B> 查询账变信息</B>
   */
  @GetMapping("/page")
  public R getUserAccountPage(Page page, QueryDto queryDto) {
    IPage<Map<String, Object>> pageList = usertreasurechangeService.queryUserAccountPage(page, queryDto);
    return  new R<>(pageList);
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(usertreasurechangeService.getById(id));
  }



}
