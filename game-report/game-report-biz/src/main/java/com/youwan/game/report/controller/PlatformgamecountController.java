package com.youwan.game.report.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.report.api.entity.Platformgamecount;
import com.youwan.game.report.service.PlatformgamecountService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 实时游戏在线记录
 *
 * @author code generator
 * @date 2019-01-25 19:13:50
 */
@RestController
@AllArgsConstructor
@RequestMapping("/platformgamecount")
public class PlatformgamecountController {

  private final PlatformgamecountService platformgamecountService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param platformgamecount 实时游戏在线记录
   * @return
   */
  @GetMapping("/page")
  public R getPlatformgamecountPage(Page page, Platformgamecount platformgamecount) {
    return  new R<>(platformgamecountService.page(page, Wrappers.query(platformgamecount)));
  }


  /**
   * 通过id查询实时游戏在线记录
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(platformgamecountService.getById(id));
  }

  /**
   * 新增实时游戏在线记录
   * @param platformgamecount 实时游戏在线记录
   * @return R
   */
  @SysLog("新增实时游戏在线记录")
  @PostMapping
  public R save(@RequestBody Platformgamecount platformgamecount){
    return new R<>(platformgamecountService.save(platformgamecount));
  }

  /**
   * 修改实时游戏在线记录
   * @param platformgamecount 实时游戏在线记录
   * @return R
   */
  @SysLog("修改实时游戏在线记录")
  @PutMapping
  public R updateById(@RequestBody Platformgamecount platformgamecount){
    return new R<>(platformgamecountService.updateById(platformgamecount));
  }

  /**
   * 通过id删除实时游戏在线记录
   * @param id id
   * @return R
   */
  @SysLog("删除实时游戏在线记录")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(platformgamecountService.removeById(id));
  }

}
