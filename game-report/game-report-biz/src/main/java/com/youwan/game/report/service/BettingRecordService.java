package com.youwan.game.report.service;

import com.youwan.game.common.core.betting.entity.*;
import com.youwan.game.common.data.core.Page;
import com.youwan.game.report.api.dto.betting.*;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-01.
 */
public interface BettingRecordService {

    Page<BcLotteryOrder> queryLotteryOrders(LotteryQueryDto queryDto);

    Page<CrownSportBettingOrder> queryCrownSportOrders(CrownSportQueryDto queryDto);

    Page<ThirdPartyEGameBettingRecord> queryEGameBettingRecords(EGameQueryDto queryDto);

    Page<ThirdPartyLiveBettingRecord> queryLiveBettingRecords(LiveQueryDto queryDto);

    Page<ThirdPartySportsBettingRecord> query3rdSportBettingRecords(TPSportQueryDto queryDto);
}
