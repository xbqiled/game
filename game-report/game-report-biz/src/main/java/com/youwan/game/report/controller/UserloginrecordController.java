package com.youwan.game.report.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.report.api.dto.QueryDto;
import com.youwan.game.report.api.entity.Userloginrecord;
import com.youwan.game.report.service.UserloginrecordService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-01-25 11:56:48
 */
@RestController
@AllArgsConstructor
@RequestMapping("/userloginRecord")
public class UserloginrecordController {

  private final UserloginrecordService userloginrecordService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param queryDto
   * @return
   */
  @GetMapping("/page")
  public R getUserloginrecordPage(Page page, QueryDto queryDto) {
    IPage iPage = userloginrecordService.queryLoginReport(page, queryDto);
    if (iPage == null) {
      return new R<>(false, "未找到相关数据!");
    } else {
      return new R<>(iPage);
    }
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(userloginrecordService.getById(id));
  }

  /**
   * 新增
   * @param userloginrecord
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody Userloginrecord userloginrecord){
    return new R<>(userloginrecordService.save(userloginrecord));
  }

  /**
   * 修改
   * @param userloginrecord
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody Userloginrecord userloginrecord){
    return new R<>(userloginrecordService.updateById(userloginrecord));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(userloginrecordService.removeById(id));
  }

}
