package com.youwan.game.report.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.report.api.entity.Recorduserscorepergame;
import com.youwan.game.report.mapper.RecorduserscorepergameMapper;
import com.youwan.game.report.service.RecorduserscorepergameService;
import org.springframework.stereotype.Service;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-29 13:42:42
 */
@Service("recorduserscorepergameService")
public class RecorduserscorepergameServiceImpl extends ServiceImpl<RecorduserscorepergameMapper, Recorduserscorepergame> implements RecorduserscorepergameService {

}
