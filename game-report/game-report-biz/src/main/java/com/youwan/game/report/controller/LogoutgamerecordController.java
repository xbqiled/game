package com.youwan.game.report.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.report.api.dto.QueryDto;
import com.youwan.game.report.api.entity.Logoutgamerecord;
import com.youwan.game.report.service.LogoutgamerecordService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-01-25 17:19:43
 */
@RestController
@AllArgsConstructor
@RequestMapping("/logoutgameRecord")
public class LogoutgamerecordController {

  private final LogoutgamerecordService logoutgamerecordService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param queryDto
   * @return
   */
  @GetMapping("/page")
  public R getRecorduserlogoutgamePage(Page page, QueryDto queryDto) {
    return  new R<>(logoutgamerecordService.queryPageList(page, queryDto));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(logoutgamerecordService.getById(id));
  }

  /**
   * 新增
   * @param logoutgameRecord
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody Logoutgamerecord logoutgameRecord){
    return new R<>(logoutgamerecordService.save(logoutgameRecord));
  }

  /**
   * 修改
   * @param logoutgameRecord
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody Logoutgamerecord logoutgameRecord){
    return new R<>(logoutgamerecordService.updateById(logoutgameRecord));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(logoutgamerecordService.removeById(id));
  }

}
