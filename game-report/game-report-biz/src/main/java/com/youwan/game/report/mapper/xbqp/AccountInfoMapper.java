package com.youwan.game.report.mapper.xbqp;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-07-13.
 */
public interface AccountInfoMapper {

    List<Map<String, Object>> queryUserByIds(List<Integer> ids);

    List<Map<String, Object>> queryUserBy3rdIds(@Param("ids") Set<Integer> tpIds);

    /**
     * 获取第三方平台账号id
     *
     * @param userId
     * @return
     */
    Map<String, Object> get3rdAccountId(@Param("userId") Integer userId);

}
