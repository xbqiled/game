package com.youwan.game.report.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-03-15 20:02:39
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("Configure_PlatformRoomSetting")
public class Gameroom extends Model<Gameroom> {
private static final long serialVersionUID = 1L;

    /**
   * 游戏最小基本类型ID
   */
    @TableId
    private Integer gameatomtypeid;
    /**
   * 游戏分类
   */
    private String gamekindname;
    /**
   * 手机端游戏名称
   */
    private String phonegamename;
    /**
   * 游戏分类ID
   */
    private Integer gamekindtype;
    /**
   * 游戏分类文件名
   */
    private String gamekindfilename;
    /**
   * 房间类型
   */
    private Integer roomtype;
    /**
   * 房间桌子数
   */
    private Integer tablecnt;
    /**
   * 房间底分、进房最少金币限制、进房最大金币限制、踢出房间金币限制
   */
    private String roomminscore;
    /**
   * 携带金币类型
   */
    private Integer carrygoldtype;
    /**
   * 玩家输分封顶金币数
   */
    private Integer losetopmoney;
    /**
   * 进桌验证方式
   */
    private String entertablevalidatetype;
    /**
   * 每桌最低游戏人数
   */
    private Integer begingamemincnt;
    /**
   * 防作弊功能类型
   */
    private Integer preventtype;
    /**
   * 每桌椅子数
   */
    private Integer chaircount;
    /**
   * 房间生效时间
   */
    private String roomeffectivedate;
    /**
   * 房间开放日期类型
   */
    private Integer roomopendatetype;
    /**
   * 
   */
    private String roomopendate;
    /**
   * 房间开放时间类型
   */
    private Integer roomopentimetype;
    /**
   * 
   */
    private String roomopentime;
    /**
   * 是否开启AI机制
   */
    private Integer isopenai;
    /**
   * 是否开启虚拟人数
   */
    private Integer isopenvirtualcnt;
    /**
   * 各时间段虚拟人数范围
   */
    private String virtualcnt;
    /**
   * 房费
   */
    private Integer roomcost;
    /**
   * 税收(%)
   */
    private Integer gametax;
    /**
   * 游戏中报名比赛最低时间间隔
   */
    private Integer signuptimeinterval;
    /**
   * 强退惩罚金币数目
   */
    private Integer punishmoneycnt;
    /**
   * 强退是否邮件通知开奖结果
   */
    private Integer ismailnotifyresult;
    /**
   * 开赛条件类型
   */
    private Integer conditiontype;
    /**
   * 开赛选项1
   */
    private String condition1;
    /**
   * 开赛选项2
   */
    private String condition2;
    /**
   * 开赛选项3
   */
    private String condition3;
    /**
   * 报名条件文本
   */
    private String signupconditiontext;
    /**
   * 满人赛满多少人开赛
   */
    private Integer beginmatchcnt;
    /**
   * 门票不足对应前往比赛的最小基本类型ID
   */
    private Integer togameatomtypeid;
    /**
   * 房间框左下方显示(手机端文本
   */
    private String roomleftdowntips;
    /**
   * 房间框右下方显示(手机端文本)
   */
    private String phonegamerightdowntip;
    /**
   * 报名页左边第一行的开赛时间提示(手机端文本)
   */
    private String applyleftfirstlinetips;
    /**
   * 报名页比赛详情内容(手机端文本)
   */
    private String matchdetail;
    /**
   * 报名页比赛奖励内容(手机端文本)
   */
    private String matchreward;
    /**
   * 扩展字段
   */
    private Integer extend2;
    /**
   * 时间占比
   */
    private BigDecimal timepercent;
    /**
   * 消费占比
   */
    private BigDecimal consumepercent;
    /**
   * 局数占比
   */
    private BigDecimal roundpercent;
   /**
   * 平台，1俱乐部-电玩城，2俱乐部-牌友房
   */
    private Integer platformtype;
    /**
   * 游戏大类
   */
    private Integer gamekindhead;
    /**
   * 游戏大类名称
   */
    private String headname;
  
}
