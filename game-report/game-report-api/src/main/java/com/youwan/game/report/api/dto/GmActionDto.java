package com.youwan.game.report.api.dto;


import com.youwan.game.report.api.entity.Userloginrecord;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class GmActionDto extends Userloginrecord {

    String domainCode;
    String cmdAction;
}
