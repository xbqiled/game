package com.youwan.game.report.api.dto.betting;

import lombok.Data;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-03.
 */
@Data
public class CrownSportQueryDto extends BettingQueryDto {

    private Integer sportType;
    private Integer bettingStatus;
    private Integer resultStatus;
    private Integer balance;
    private String bettingCode;

}
