package com.youwan.game.report.api.dto;


import com.youwan.game.report.api.entity.Report;
import lombok.Data;
import lombok.EqualsAndHashCode;



@Data
@EqualsAndHashCode(callSuper = true)
public class BoardDto extends Report {

    String channelNo;

    String startTime;

    String endTime;

    String accountId;

    String queryTime;

    String weekTime;

    String monthTime;

    String recordId;

}
