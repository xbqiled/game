package com.youwan.game.report.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-25 17:19:43
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("RecordUserLogoutGame")
public class Logoutgamerecord extends Model<Logoutgamerecord> {
private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId
    private Integer id;
    /**
     *
     */
    private Integer userid;
    /**
     *
     */
    private Integer experience;
    /**
     *
     */
    private Integer gametypeid;
    /**
     *
     */
    private Integer terminaltype;
    /**
     *
     */
    private Integer gametimecount;
    /**
     *
     */
    private Integer recordtime;
    /**
     *
     */
    private String matchguid;
    /**
     *
     */
    private String keyguid;
    /**
     *
     */
    private String channelno;
  
}
