package com.youwan.game.report.api.feign.factory;


import com.youwan.game.report.api.feign.RemoteMakeDataService;
import com.youwan.game.report.api.feign.fallback.RemoteMakeDataServiceFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author lion
 * @date 2019/2/14
 */
@Component
public class RemoteMakeDataServiceFallbackFactory implements FallbackFactory<RemoteMakeDataService> {


    @Override
    public RemoteMakeDataService create(Throwable throwable) {
        RemoteMakeDataServiceFallbackImpl remoteMakeDataServiceFallbackImpl = new RemoteMakeDataServiceFallbackImpl();
        remoteMakeDataServiceFallbackImpl.setCause(throwable);
        return remoteMakeDataServiceFallbackImpl;
    }
}
