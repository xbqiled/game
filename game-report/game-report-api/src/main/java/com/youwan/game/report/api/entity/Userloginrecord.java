package com.youwan.game.report.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-25 11:56:48
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("RecordUserPlatformLogin")
public class Userloginrecord extends Model<Userloginrecord> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private Integer id;
    /**
   * 用户ID
   */
    private Integer userid;
    /**
   * 1 机器人；2 真人
   */
    private Integer usertype;
    /**
   * 登录时间
   */
    private String logintime;
    /**
   * 0 平台登陆；1 微信登陆；2 SDK登陆
   */
    private Integer logintype;
    /**
   * 登陆IP
   */
    private String loginip;
    /**
   * 登陆机器码
   */
    private String machineserial;
    /**
   * 0-WIFI；1-2G；2-3G；3-4G
   */
    private Integer networdstate;
    /**
   * 玩家使用无线网时的ssid名称
   */
    private String wifiname;
    /**
   * 101官网PC；102 官网手机；106 测试包（以策划配表为准）
   */
    private String channelno;
    /**
   * 版本号
   */
    private String version;
    /**
   * 0 PC端；1 安卓端；2 苹果端；3 H5
   */
    private Integer terminaltype;
    /**
   * 机型
   */
    private String machinetype;
    /**
   * 操作系统类型
   */
    private String systemtype;
    /**
   * 登录关联标识
   */
    private String matchguid;
    /**
   * 0 正常登录,1 重连
   */
    private Integer loginmode;
  
}
