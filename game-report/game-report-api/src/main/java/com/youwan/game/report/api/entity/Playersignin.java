package com.youwan.game.report.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-03 14:15:54
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("PlayerSignIn")
public class Playersignin extends Model<Playersignin> {
private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId
    private Integer id;
    /**
   * 
   */
    private Integer accountid;
    /**
   * 
   */
    private Integer todaybet;
    /**
   * 
   */
    private Integer dayindex;
    /**
   * 
   */
    private Integer award;
    /**
   * 
   */
    private Integer additionaward;
    /**
   * 
   */
    private Integer beforecoin;
    /**
   * 
   */
    private Integer changecoin;
    /**
   * 
   */
    private Integer aftercoin;
    /**
   * 
   */
    private byte[] dayarr;
    /**
   * 
   */
    private String channel;
    /**
   * 
   */
    private String loginguid;
    /**
   * 
   */
    private String businessguid;
    /**
   * 
   */
    private Integer recordtime;
  
}
