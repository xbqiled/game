package com.youwan.game.report.api.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * VIEW
 *
 * @author code generator
 * @date 2020-04-18 12:19:42
 */
@Data
public class DantiaosingleDto implements Serializable {

    String recordId;

    String startTime;

    String endTime;

    String accountId;

}