package com.youwan.game.report.api.dto;


import com.youwan.game.report.api.entity.Userloginrecord;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class MaintDto extends Userloginrecord {

    int status;

    int toStatus;

    int roomId;

    String startMaintTime;

    String endMaintTime;
}
