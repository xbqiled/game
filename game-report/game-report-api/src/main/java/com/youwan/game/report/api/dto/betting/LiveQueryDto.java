package com.youwan.game.report.api.dto.betting;

import lombok.Data;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-04.
 */
@Data
public class LiveQueryDto extends BettingQueryDto {

    private String bettingCode;
    private Integer type;
}
