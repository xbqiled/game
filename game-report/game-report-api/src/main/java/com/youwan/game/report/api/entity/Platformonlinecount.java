package com.youwan.game.report.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 平台实时在线记录
 *
 * @author code generator
 * @date 2019-01-25 13:12:14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("PlatFormOnlineCount")
public class Platformonlinecount extends Model<Platformonlinecount> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private Integer id;
    /**
   * AI不计入在内
   */
    private Integer productcount;
    /**
   * AI不计入在内
   */
    private Integer phonecount;
    /**
   * AI不计入在内
   */
    private Integer pccount;
    /**
   * AI不计入在内
   */
    private Integer h5count;
    /**
   * 渠道号规则为  前三位产品号 中间4未为渠道号 后三位为包号
   */
    private String channelno;
    /**
   * 更新时间：十分钟统计一次
   */
    private Integer recordtime;
  
}
