package com.youwan.game.report.api.dto;

import com.youwan.game.report.api.entity.Userloginrecord;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = true)
public class ParameterDto extends Userloginrecord {

    String channelNo;

    String periodical;

    String startTime;

    String endTime;

    String accountId;

    String queryTime;

    String weekTime;

    String monthTime;

    String recordId;

    String orderId;

}
