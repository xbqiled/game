package com.youwan.game.report.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-02 12:18:56
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("RecordGameRebateDetail")
public class Gamerebate extends Model<Gamerebate> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private Long id;
    /**
   * 
   */
    private Integer accountid;
    /**
   * 
   */
    private String nickname;
    /**
   * 
   */
    private Integer promoterid;
    /**
   * 
   */
    private String promoternickname;
    /**
   * 
   */
    private Integer level;
    /**
   * 
   */
    private Integer count;
    /**
   * 
   */
    private Integer recordtime;
    /**
   * 
   */
    private String channelno;
  
}
