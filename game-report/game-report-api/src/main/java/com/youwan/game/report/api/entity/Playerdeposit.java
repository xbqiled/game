package com.youwan.game.report.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-03 14:04:14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("PlayerDeposit")
public class Playerdeposit extends Model<Playerdeposit> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private Integer id;
    /**
   * 
   */
    private Integer accountid;
    /**
   * 
   */
    private String tOrderid;
    /**
   * 
   */
    private Integer rechargecoin;
    /**
   * 
   */
    private Integer optype;
    /**
   * 
   */
    private Integer rebatecoin;
    /**
   * 
   */
    private Integer beforecoin;
    /**
   * 
   */
    private Integer changecoin;
    /**
   * 
   */
    private Integer aftercoin;
    /**
   * 
   */
    private String channel;
    /**
   * 
   */
    private String loginguid;
    /**
   * 
   */
    private String businessguid;
    /**
   * 
   */
    private Integer recordtime;
  
}
