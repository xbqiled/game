package com.youwan.game.report.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 玩家登出平台记录表
 *
 * @author code generator
 * @date 2019-01-25 11:48:53
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("RecordUserPlatformLogout")
public class Userlogoutrecord extends Model<Userlogoutrecord> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private Integer id;
    /**
   * 用户ID
   */
    private Integer userid;
    /**
   * 在线的时长
   */
    private Integer onlineduration;
    /**
   * 退出时间
   */
    private String logouttime;
    /**
   * 登录关联标识
   */
    private String matchguid;
    /**
   * 1 密码修改被踢,2 顶号,3 正常退出,4 掉线,5 重连
   */
    private Integer logouttype;

    /**
     * 渠道编码
     */
    private String channelno;
}
