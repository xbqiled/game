package com.youwan.game.report.api.dto.betting;

import lombok.Data;

import java.io.Serializable;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-03.
 */
@Data
public abstract class BettingQueryDto implements Serializable {

    private Integer offset = 0;
    private Integer limit = 20;
    private String channelId;
    private Integer userId;
    private Long startTime;
    private Long endTime;
}
