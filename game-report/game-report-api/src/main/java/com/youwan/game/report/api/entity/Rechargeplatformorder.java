package com.youwan.game.report.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 充值日志(平台)
 *
 * @author code generator
 * @date 2019-01-27 16:14:56
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("RechargePlatformOrder")
public class Rechargeplatformorder extends Model<Rechargeplatformorder> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private Integer id;
    /**
   * 订单号，不重复
   */
    private String orderno;
    /**
   * 用户ID
   */
    private Integer userid;
    /**
   * 昵称
   */
    private String nickName;

    /**
     * 手机
     */
    private String phone;

    /**
   * 关联配置表
   */
    private Integer paytype;
    /**
   * 支付金额
   */
    private BigDecimal payamount;
    /**
   *  待付款=0，支付成功=1，支付失败 = 2
   */
    private Integer paystatus;
    /**
   * 记录时间
   */
    private String recordtime;
  
}
