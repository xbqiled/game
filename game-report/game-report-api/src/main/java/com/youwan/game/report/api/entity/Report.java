package com.youwan.game.report.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-02-05 14:02:39
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("V_Day_Report")
public class Report extends Model<Report> {
private static final long serialVersionUID = 1L;

    @TableId
    private Long regcount;
    /**
     *
     */
    private Long newgamecount;
    /**
     *
     */
    private Long logincount;
    /**
     *
     */
    private Long halfhourusercount;
    /**
     *
     */
    private BigDecimal gametimecount;
    /**
     *
     */
    private BigDecimal timelength;
    /**
     *
     */
    private BigDecimal newuseravggametime;
    /**
     *
     */
    private Long loginusercount;
    /**
     *
     */
    private Long dailyactivecount;
    /**
     *
     */
    private Long ordercount;
    /**
     *
     */
    private Long rechargecount;



    private Long logingamecount;
    /**
     *
     */
    private BigDecimal totalrechargemoney;
    /**
     *
     */
    private Double arpu;
    /**
     *
     */
    private BigDecimal rechargerate;
    /**
     *
     */
    private BigDecimal gaincount;
    /**
     *
     */
    private String recordtime;
    /**
     *
     */
    private String channelid;
  
}
