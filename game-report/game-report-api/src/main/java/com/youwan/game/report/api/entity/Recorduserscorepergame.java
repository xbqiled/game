package com.youwan.game.report.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-29 13:42:42
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("RecordUserScorePerGame")
public class Recorduserscorepergame extends Model<Recorduserscorepergame> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private Integer id;
    /**
   * 牌局ID
   */
    private String recordid;
    /**
   * 用户ID
   */
    private Integer userid;
    /**
   * 底分（若某游戏无此字段类型，则为 null）
   */
    private Integer basicscore;
    /**
   * 封顶（若某游戏无此字段类型，则为 null）
   */
    private Integer topscore;
    /**
   * 桌子号（若某游戏无此字段类型，则为 null）
   */
    private Integer tableid;
    /**
   * 椅子号（若某游戏无此字段类型，则为 null）
   */
    private Integer chairid;
    /**
   * 之前金币
   */
    private Integer beforescore;
    /**
   * 金币
   */
    private Integer score;
    /**
   * 之后金币
   */
    private Integer afterscore;
    /**
   * 之前钻石
   */
    private Integer beforediamond;
    /**
   * 钻石
   */
    private Integer diamond;
    /**
   * 之后钻石
   */
    private Integer afterdiamond;
    /**
   * 游戏税收  （若某游戏无此字段类型，则为 null）
   */
    private BigDecimal revenue;
    /**
   * 下注金额   （若某游戏无此字段类型，则为 null）
   */
    private BigDecimal stake;
    /**
   * 0  无；1  有 （若某游戏无此字段类型，则为 null）
   */
    private Integer istrust;
    /**
   * 0  无；1  有 （若某游戏无此字段类型，则为 null）
   */
    private Integer ispunish;
    /**
   * 0  普通玩家（斗地主即为农民，押注类即为闲家）；1  地主；2  庄家；3 旁观（若某游戏无此字段类型，则为 null）
   */
    private Integer player;
    /**
   * 扣分类型 (0胜 1输 2和 3逃 4服务  )    （若某游戏无此字段类型，则为 null）
   */
    private Integer settlementtype;
    /**
   * 开始时间
   */
    private Integer starttime;
    /**
   * 结束时间
   */
    private Integer endtime;
    /**
   * 最小游戏类型
   */
    private Integer gametypeid;
    /**
   * 登录关联标识
   */
    private String matchguid;
    /**
   * 游戏登录关联标识
   */
    private String keyguid;
    /**
   * 与其他业务表的业务标识关联
   */
    private String businessguid;
    /**
   * 0 PC端；1 安卓端；2 苹果端；3 H5
   */
    private Integer terminaltype;
  
}
