package com.youwan.game.report.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-03 13:55:49
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("RedBlackRewardPool")
public class Redblackpool extends Model<Redblackpool> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private Integer id;
    /**
   * 
   */
    private String recordid;
    /**
   * 
   */
    private Integer periodical;
    /**
   * 
   */
    private Integer userid;
    /**
   * 
   */
    private Integer betorder;
    /**
   * 
   */
    private Integer beforecoin;
    /**
   * 
   */
    private Integer reward;
    /**
   * 
   */
    private Integer aftercoin;
    /**
   * 
   */
    private Integer starttime;
    /**
   * 
   */
    private Integer endtime;
    /**
   * 
   */
    private Integer gametypeid;
    /**
   * 
   */
    private String channelno;
  
}
