package com.youwan.game.report.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-25 12:22:25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("UserBetScoreChange")
public class ScoreRecord extends Model<ScoreRecord> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private Integer id;
    /**
   * 
   */
    private Integer userid;
    /**
   * 
   */
    private BigDecimal lastbeforecount;
    /**
   * 
   */
    private BigDecimal lastgaincount;
    /**
   * 
   */
    private BigDecimal lastaftercount;
    /**
   * 
   */
    private BigDecimal beforecount;
    /**
   * 
   */
    private BigDecimal gaincount;
    /**
   * 
   */
    private BigDecimal aftercount;
    /**
   * 
   */
    private Integer type;
    /**
   * 
   */
    private Integer gametypeid;
    /**
   * 
   */
    private Integer recordtime;
    /**
   * 
   */
    private String channelno;
  
}
