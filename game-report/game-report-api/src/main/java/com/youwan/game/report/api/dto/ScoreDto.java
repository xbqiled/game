package com.youwan.game.report.api.dto;


import com.youwan.game.report.api.entity.Userloginrecord;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ScoreDto  extends Userloginrecord {

    String channelNo;

    String startTime;

    String endTime;

    String accountId;

    String type;

}
