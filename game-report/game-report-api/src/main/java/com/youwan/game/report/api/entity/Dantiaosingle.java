package com.youwan.game.report.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * VIEW
 *
 * @author code generator
 * @date 2020-04-18 12:19:42
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("dantiaosinglemode")
public class Dantiaosingle extends Model<Dantiaosingle> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    private Integer id;
    /**
   * 
   */
    private String recordid;
    /**
   * 
   */
    private Integer accountid;
    /**
   * 
   */
    private Integer beforeisexitout;
    /**
   * 
   */
    private Integer beforeisprotect;
    /**
   * 
   */
    private Integer beforeid;
    /**
   * 
   */
    private Integer beforeouttotalwin;
    /**
   * 
   */
    private Integer beforeneedprotecttotalcnt;
    /**
   * 
   */
    private Integer beforeactualprotectcnt;
    /**
   * 
   */
    private Integer afterisexitout;
    /**
   * 
   */
    private Integer afterisprotect;
    /**
   * 
   */
    private Integer afterid;
    /**
   * 
   */
    private Integer afterouttotalwin;
    /**
   * 
   */
    private Integer afterneedprotecttotalcnt;
    /**
   * 
   */
    private Integer afteractualprotectcnt;
    /**
   * 
   */
    private Long beforetotalxima;
    /**
   * 
   */
    private Long beforetotaltax;
    /**
   * 
   */
    private Long beforerewardpoolcoin;
    /**
   * 
   */
    private Long beforetotalwinlose;
    /**
   * 
   */
    private Long aftertotalxima;
    /**
   * 
   */
    private Long aftertotaltax;
    /**
   * 
   */
    private Long afterrewardpoolcoin;
    /**
   * 
   */
    private Long aftertotalwinlose;
    /**
   * 
   */
    private Integer beforeisnewdatactl;
    /**
   * 
   */
    private Integer beforenewdatactltype;
    /**
   * 
   */
    private Integer beforeneedtotalctrlinning;
    /**
   * 
   */
    private Integer beforeactualctrlinning;
    /**
   * 
   */
    private Integer beforerandpoolmaxlose;
    /**
   * 
   */
    private Integer beforerandpoolmaxwin;
    /**
   * 
   */
    private Integer beforerandlosebegin;
    /**
   * 
   */
    private Integer beforerandloseend;
    /**
   * 
   */
    private Integer beforerandwinbegin;
    /**
   * 
   */
    private Integer beforerandwinend;
    /**
   * 
   */
    private Integer afterisnewdatactl;
    /**
   * 
   */
    private Integer afternewdatactltype;
    /**
   * 
   */
    private Integer afterneedtotalctrlinning;
    /**
   * 
   */
    private Integer afteractualctrlinning;
    /**
   * 
   */
    private Integer afterrandpoolmaxlose;
    /**
   * 
   */
    private Integer afterrandpoolmaxwin;
    /**
   * 
   */
    private Integer afterrandlosebegin;
    /**
   * 
   */
    private Integer afterrandloseend;
    /**
   * 
   */
    private Integer afterrandwinbegin;
    /**
   * 
   */
    private Integer afterrandwinend;

    private Integer onlinePlayerCnt;

    private Integer betFourColorPlayerCnt;

    private Long curTotalXima;

    /**
   * 
   */
    private Integer recordtime;
    /**
   * 
   */
    private Integer gametypeid;
  
}
