package com.youwan.game.report.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 实时游戏在线记录
 *
 * @author code generator
 * @date 2019-01-25 19:13:50
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("PlatFormGameCount")
public class Platformgamecount extends Model<Platformgamecount> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private Integer id;
    /**
   * AI不计入该在线；牌友房中当玩家未开局，不计入该在线。    （默认值，0）
   */
    private Integer platformcount;
    /**
   * AI不计入该在线；牌友房中当玩家未开局，不计入该在线。    （默认值，0）
   */
    private Integer phonecount;
    /**
   * AI不计入该在线；牌友房中当玩家未开局，不计入该在线。    （默认值，0）
   */
    private Integer pccount;
    /**
   * AI在线人数
   */
    private Integer aicount;
    /**
   * AI不计入在内
   */
    private Integer h5count;
    /**
   * 更新时间：十分钟统计一次
   */
    private Integer recordtime;
    /**
   * 最小游戏类型
   */
    private Integer gametypeid;
  
}
