package com.youwan.game.report.api.feign;

import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.constant.ServiceNameConstant;
import com.youwan.game.common.core.util.R;
import com.youwan.game.report.api.feign.factory.RemoteMakeDataServiceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * <B> 短信服务feign</B>
 */
@FeignClient(value = ServiceNameConstant.REPORT_SERVICE, fallbackFactory = RemoteMakeDataServiceFallbackFactory.class)
public interface RemoteMakeDataService {

    @PostMapping("/syssms/sendSms/")
    R makeData(@RequestBody String dateTime, @RequestHeader(SecurityConstants.FROM) String from);
}
