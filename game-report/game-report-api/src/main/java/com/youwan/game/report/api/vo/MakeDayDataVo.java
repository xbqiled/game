package com.youwan.game.report.api.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <B>日加工数据表</B>
 */
@Data
public class MakeDayDataVo implements Serializable {

    //时间
    private String dataTime;

    //用户编码
    private String userId;

    //渠道编码
    private String channelId;

    //在线充值
    private String onlineFee;

    //手动上分
    private String handFee;

    //下注金额度
    private String betFee;

    //其他奖励
    private String rewardFee;

    //加工记录时间
    private Date createTime;
}
