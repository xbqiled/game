package com.youwan.game.report.api.dto;

import com.youwan.game.report.api.dto.betting.BettingQueryDto;
import lombok.Data;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-09.
 */
@Data
public class TPAccountChangeQueryDto extends BettingQueryDto {
    private Integer type;
    private String orderId;
}
