package com.youwan.game.report.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-25 13:11:34
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("UserTreasureChange")
public class Usertreasurechange extends Model<Usertreasurechange> {
    private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private Integer id;
    /**
   * 用户ID
   */
    private Integer userid;
    /**
   * 1 机器人；2 真人
   */
    private Integer usertype;
    /**
   * 道具ID
   */
    private Integer itemid;
    /**
   * 之前数量
   */
    private BigDecimal beforecount;
    /**
   * 更新数量
   */
    private BigDecimal gaincount;
    /**
   * 之后数量
   */
    private BigDecimal aftercount;
    /**
   * 业务类型
   */
    private Integer businesstype;
    /**
   * 最小游戏类型ID
   */
    private Integer gametypeid;
    /**
   * 记录时间
   */
    private Integer recordtime;
    /**
   * 道具增加有效时间
   */
    private Integer itemvalidvalue;
    /**
   * 之前有效时间
   */
    private Integer beforevalidvalue;
    /**
   * 之后有效时间
   */
    private Integer aftervalidvalue;
    /**
   * 登录关联标识
   */
    private String matchguid;
    /**
   * 游戏关联标识
   */
    private String keyguid;
    /**
   * 业务关联标识
   */
    private String businessguid;
    /**
   * 0 PC端；1 安卓端；2 苹果端；3 H5
   */
    private Integer terminaltype;
    /**
     * 渠道编码
     */
    private String channelno;
  
}
