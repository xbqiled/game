package com.youwan.game.report.api.dto.betting;

import lombok.Data;

import java.io.Serializable;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-01.
 */
@Data
public class LotteryQueryDto extends BettingQueryDto {

    private String account;
    private String lotCode;
    private String qiHao;
    private Integer status;
    private String orderId;
    private String betIp;

}
