package com.youwan.game.report.api.feign.fallback;


import com.youwan.game.common.core.util.R;
import com.youwan.game.report.api.feign.RemoteMakeDataService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author lion
 * @date 2019/02/14
 * feign token  fallback
 */
@Slf4j
@Component
public class RemoteMakeDataServiceFallbackImpl implements RemoteMakeDataService {

    @Setter
    private Throwable cause;

    @Override
    public R makeData(String dateTime, String from) {
        log.error("feign 生成加工信息失败", cause);
        return null;
    }
}
