package com.youwan.game.report.api.dto;

import lombok.Data;

import java.io.Serializable;


@Data
public class DantiaomultiplyDto implements Serializable {

    String recordId;

    String startTime;

    String endTime;

    String accountId;

}
