package com.youwan.game.report.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-25 17:23:37
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("V_RecordUserLoginGame")
public class Logingamerecord extends Model<Logingamerecord> {
private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId
    private Integer id;
    /**
     *
     */
    private Integer userid;
    /**
     *
     */
    private BigDecimal beforescorecount;
    /**
     *
     */
    private Integer gametypeid;
    /**
     *
     */
    private Integer terminaltype;
    /**
     *
     */
    private Integer recordtime;
    /**
     *
     */
    private String matchguid;
    /**
     *
     */
    private String keyguid;
    /**
     *
     */
    private String channelno;
}
