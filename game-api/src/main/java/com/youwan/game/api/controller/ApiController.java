package com.youwan.game.api.controller;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.youwan.game.admin.api.dto.QrCodeDto;
import com.youwan.game.admin.api.feign.RemoteOssService;
import com.youwan.game.api.json.GsonIpAddress;
import com.youwan.game.api.model.*;
import com.youwan.game.cac.api.dto.*;
import com.youwan.game.cac.api.entity.*;
import com.youwan.game.cac.api.feign.*;
import com.youwan.game.cac.api.vo.*;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.jsonmodel.GsonAgent;
import com.youwan.game.common.core.jsonmodel.GsonWechatToken;
import com.youwan.game.common.core.jsonmodel.GsonWechatUserInfo;
import com.youwan.game.common.core.oss.CloudStorageConfig;
import com.youwan.game.common.core.util.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;


/**
 * <B>所有接口信息</B>
 */
@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/itf")
public class ApiController {
    //处理支付订单
    private final RemotePayOrderService remotePayOrderService;

    //OSS服务
    private final RemoteOssService remoteOssService;

    //提现服务
    private final RemoteCashOrderService remoteCashOrderService;

    //短信接口服务
    private final RemoteSmsService remoteSmsService;

    //版本配置服务
    private final RemoteVersionConfigService remoteVersionConfigService;

    //处理信息
    private final RemoteSocialConfigService remoteSocialConfigService;

    //支付接口
    private final RemotePayService remotePayService;

    //客服信息接口
    private final RemoteCsService remoteCsService;

    //短链接处理服务接口
    private final RemoteJumpService remoteJumpService;

    //验证机型信息是否
    private final RemoteDeviceInfoService remoteDeviceInfoService;

    //渠道服务
    private final RemoteChannelService remoteChannelService;

    /**
     * 给seo用的
     * @param map
     * @return
     */
    @RequestMapping(value = "/dm/getYesterdayValue", method = {RequestMethod.POST, RequestMethod.GET})
    public Result getUserDaMaValue(@RequestBody  Map<String, String> map) {
        if (map == null || StrUtil.isBlank(map.get("data"))) {
            return Result.error(1, "请求参数错误!");
        }

        try {
            String resData = AesEncodeUtil.decrypt(map.get("data"));
            DaMaValueDto daMaValue = new Gson().fromJson(resData, DaMaValueDto.class);

            if (daMaValue == null || daMaValue.getChannelId() == null && daMaValue.getUserId() == null) {
                return Result.error(3, "解密参数错误!");
            }

            R<List<DaMaValue>> result = remoteChannelService.getUserDamaValue(daMaValue, SecurityConstants.FROM_IN);
            if (result == null || result.getData() == null || result.getData().size() == 0) {
                return Result.error("暂时没有数据!");
            }

            return Result.ok().put("data", result.getData());
        } catch (Exception e) {
            return Result.error(2, "解密失败!");
        }
    }


    @RequestMapping(value = "/bg/getEnterConfig", method = {RequestMethod.POST, RequestMethod.GET})
    public Result getEnterConfig(@RequestBody  BgEnterConfig bgEnterConfig) {
        if (StrUtil.isBlank(bgEnterConfig.getChannelId())) {
            return Result.error(1, "请求参数错误!");
        }

        R<BgenterConfig> result = remoteChannelService.getBgEnterConfig(bgEnterConfig, SecurityConstants.FROM_IN);
        if (result == null || result.getData() == null) {
            return Result.error("获取进入bg配置失败!");
        }

        if (result != null && result.getData() != null) {
            return Result.ok("获取进入bg配置成功").put("enterBalance", result.getData().getEnterBalance()).put("channelId", result.getData().getChannelId());
        } else {
            return Result.error("2, 获取进入bg配置失败!");
        }
    }



    @RequestMapping(value = "/bg/synDama", method = {RequestMethod.POST, RequestMethod.GET})
    public Result synDama(@RequestBody  BgExitGameDto bgExitGameDto) {
        log.info("打码量用户为:" + bgExitGameDto.getUserId());
        log.info("打码量开始时间为:" + bgExitGameDto.getStartTime());
        log.info("打码量结束时间为:" + bgExitGameDto.getEndTime());
        if (StrUtil.isBlank(bgExitGameDto.getUserId()) || ObjectUtil.isNull(bgExitGameDto.getStartTime()) || ObjectUtil.isNull(bgExitGameDto.getEndTime())) {
            return Result.error(1, "请求参数错误!");
        }

        R<BgDamaGameDto> result = remoteChannelService.exitGame(bgExitGameDto, SecurityConstants.FROM_IN);
        if (result == null || result.getData() == null) {
            return Result.error("获取用户打码量失败!");
        }

        log.info("打码量为:" + result.getData());
        if (result != null && result.getData() != null) {
            return Result.ok("获取用户打码量成功").put("dama", result.getData());
        } else {
            return Result.error("2, 获取用户打码量失败!");
        }
    }


    @RequestMapping(value = "/bg/initUser", method = {RequestMethod.POST, RequestMethod.GET})
    public Result createBgUser(@RequestBody  BgUserDto bgUserDto) {
        if (StrUtil.isBlank(bgUserDto.getChannelId()) || StrUtil.isBlank(bgUserDto.getUserId()) || StrUtil.isBlank(bgUserDto.getNickName())) {
            return Result.error(1, "请求参数错误!");
        }

        R<OtherBgUser> result = remoteChannelService.createBgUser(bgUserDto, SecurityConstants.FROM_IN);
        if (result == null || result.getData() == null) {
            return Result.error("初始化bg用户信息失败!");
        }

        OtherBgUser userInfo = result.getData();
        if (userInfo != null) {
            return Result.ok("初始化bg用户信息成功").put("bgUserInfo", userInfo);
        } else {
            return Result.error("初始化bg用户信息失败");
        }
    }

    @RequestMapping(value = "/bg/transferbalance", method = {RequestMethod.POST, RequestMethod.GET})
    public Result transferbalance(@RequestBody  BgTransferBalanceDto bgTransferBalanceDto) {
        if (ObjectUtil.isNull(bgTransferBalanceDto.getType()) || StrUtil.isBlank(bgTransferBalanceDto.getUserId()) || StrUtil.isBlank(bgTransferBalanceDto.getChannelId()) ||  ObjectUtil.isNull(bgTransferBalanceDto.getReason())) {
            return Result.error(1, "请求参数错误!");
        }

        if (bgTransferBalanceDto.getType() == 1 && ObjectUtil.isNull(bgTransferBalanceDto.getAmount())) {
            return Result.error(2, "转入金额不能为空!");
        } else if (bgTransferBalanceDto.getType() == 1 && new Float(bgTransferBalanceDto.getAmount()) == 0) {
            return Result.ok("额度转换成功").put("reason", bgTransferBalanceDto.getReason());
        }

        R<Integer>  result = remoteChannelService.bgTransferbalance(bgTransferBalanceDto, SecurityConstants.FROM_IN);
        if (result == null || result.getData() == null) {
            return Result.error("请求bg超时!");
        }

        if (result.getData() == 0) {
            return Result.ok("额度转换成功").put("reason", bgTransferBalanceDto.getReason());
        } else if (result.getData() == 3) {
            return Result.error(3,"渠道余额度不足,请联系客服").put("reason", bgTransferBalanceDto.getReason());
        } else if (result.getData() == 4) {
            return Result.ok("您的bg额度为0").put("reason", bgTransferBalanceDto.getReason());
        } else {
            return Result.error("额度转换失败,请联系客服").put("reason", bgTransferBalanceDto.getReason());
        }
    }


    @RequestMapping(value = "/bg/openGame", method = {RequestMethod.POST, RequestMethod.GET})
    public Result openGame(@RequestBody  OpenBgGameDto openBgGameDto) {
        if (ObjectUtil.isNull(openBgGameDto.getIsHttpsUrl()) ||
                ObjectUtil.isNull(openBgGameDto.getIsMobileUrl()) ||
                StrUtil.isBlank(openBgGameDto.getUserId()) ||
                ObjectUtil.isNull(openBgGameDto.getIsWx())) {
            return Result.error(1, "请求参数错误!");
        }

        if (openBgGameDto.getIsWx() == 0 && StrUtil.isBlank(openBgGameDto.getPassword())) {
            return Result.error(1, "请求参数错误!");
        }

        R<String> result = remoteChannelService.openBgGame(openBgGameDto, SecurityConstants.FROM_IN);
        if (result == null || result.getData() == null) {
            return Result.error("获取游戏地址信息失败, 请确定余额达到准入标准");
        }

        String url = result.getData();
        if (StrUtil.isNotEmpty(url) && !url.equals("1") && !url.equals("2") && !url.equals("3") && !url.equals("4")) {
            if (StrUtil.isNotEmpty(openBgGameDto.getTable())) {
                url += "&table=" + openBgGameDto.getTable();
            }
            return Result.ok("获取游戏地址信息成功").put("url", url);
        } else if (url.equals("1")){
            return Result.error(2,"用户名密码不匹配");
        } else if (url.equals("2")){
            return Result.error(3,"未找到当前账户");
        } else if (url.equals("3")){
            return Result.error(4,"您的余额无法达到进入标准");
        } else {
            return Result.error("获取游戏地址信息失败,请与客服联系");
        }
    }


    @RequestMapping(value = "/bg/getUserbalance", method = {RequestMethod.POST, RequestMethod.GET})
    public Result getUserbalance(@RequestBody  BgUserBalanceDto bgUserBalanceDto) {
        if (ObjectUtil.isNull(bgUserBalanceDto.getUserId()) ) {
            return Result.error(1, "请求参数错误!");
        }

        R<Map<String, BigDecimal>>  result = remoteChannelService.bgUserBalance(bgUserBalanceDto, SecurityConstants.FROM_IN);
        if (result == null || result.getData() == null) {
            return null;
        }

        if (result != null && result.getData() != null) {
            return Result.ok("获取余额成功").put("blance", result.getData());
        } else {
            return Result.error("获取余额失败!");
        }

    }

    @RequestMapping(value = "/agent/info", method = {RequestMethod.POST, RequestMethod.GET})
    public Result getAgentInfo(String userId) {

        if (StrUtil.isBlank(userId)) {
            return Result.error(1, "请求参数错误!");
        }

        GsonAgent result = HttpHandler.getAgentInfo(new Integer(userId));
        if (result != null && result.getRet() == 0) {
            return Result.ok("获取推广信息成功").put("agentInfo", result);
        } else {
            return Result.error("获取推广信息失败");
        }
    }


    @RequestMapping(value = "/device/verify", method = {RequestMethod.POST, RequestMethod.GET})
    public Result verifyDeviceInfo(@RequestBody VerifyDeviceInfo verifyDeviceInfo, HttpServletRequest request) {

        if (StrUtil.isBlank(verifyDeviceInfo.getChannelId()) || StrUtil.isBlank(verifyDeviceInfo.getOs()) || StrUtil.isBlank(verifyDeviceInfo.getOsVersion())
                ||  StrUtil.isBlank(verifyDeviceInfo.getColorDepth()) ||  StrUtil.isBlank(verifyDeviceInfo.getDeviceType())
                || StrUtil.isBlank(verifyDeviceInfo.getHardwareConcurrency()) || StrUtil.isBlank(verifyDeviceInfo.getLanguage()) || StrUtil.isBlank(verifyDeviceInfo.getScreenHeight())
                || StrUtil.isBlank(verifyDeviceInfo.getScreenWidth()) ||  StrUtil.isBlank(verifyDeviceInfo.getDeviceModel())) {
            return Result.error(1, "请求参数错误!");
        }

        if (StrUtil.isBlank(verifyDeviceInfo.getIp())) {
            String ip = IPUtils.getIpAddr(request);
            verifyDeviceInfo.setIp(ip);
        }
        log.info("IP地址信息:" + verifyDeviceInfo.getIp());

        R<Map<String,String>> r = remoteDeviceInfoService.verifyDeviceInfo(verifyDeviceInfo, SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return Result.error("获取设备推广人信息失败!");
        }

        Map<String,String> info = r.getData();
        if (StrUtil.isNotEmpty(info.get("promoterId")) && StrUtil.isNotEmpty(info.get("paraInfo"))) {
            return Result.ok("获取设备推广人信息成功").put("promoterId", info.get("promoterId")).put("paraInfo", info.get("paraInfo"));
        } else {
            return Result.error("获取设备推广人信息失败");
        }
    }



    /**
     * <B>创建短链接</B>
     * @param shareDto
     * @return
     */
    @RequestMapping(value = "/share/createShortUrl", method =  {RequestMethod.POST, RequestMethod.GET})
    public Result getShareUrl(@RequestBody ShareDto shareDto) {
        if (StrUtil.isBlank(shareDto.getUrl()) || StrUtil.isBlank(shareDto.getChannelId())) {
            return Result.error(1, "请求参数错误!");
        }

        R<ShortUrlVo> r = remoteJumpService.createShortUrl(shareDto, SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return  Result.error("获取短链接信息失败");
        }

        ShortUrlVo url = r.getData();
        if (url != null  && StrUtil.isNotEmpty(url.getShortUrl()) ) {
            return  Result.ok("获取短链接信息成功").put("shortUrl", url);
        } else {
            return  Result.error("获取短链接信息失败");
        }
    }


    /**
     * <B>通过渠道获取客服信息</B>
     * @param channelId
     * @return
     */
    @RequestMapping(value = "/cs/info", method =  {RequestMethod.POST, RequestMethod.GET})
    public Result csInfo(String channelId, String type) {
        if (StrUtil.isBlank(channelId)) {
            return Result.error(1, "请求参数错误!");
        }

        R<List<CsInfo>> r = remoteCsService.queryCsInfo(new CsInfoDto(channelId, type), SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return  Result.error("获取客服信息失败");
        }

        List<CsInfo> list = r.getData();
        return  Result.ok("获取客服信息成功").put("data", list);
    }


    /**
     * <B>获取提现信息</B>
     * @param userId
     * @return
     */
    @RequestMapping(value = "/cash/info", method =  {RequestMethod.POST, RequestMethod.GET})
    public Result cashInfo(String userId, String cashType) {
        if (StrUtil.isBlank(userId)) {
            return Result.error(1, "请求参数错误!");
        }

        R<List<SysCashOrder>> r = null;
        if (StrUtil.isNotBlank(userId) && StrUtil.isNotBlank(cashType)) {
            r = remoteCashOrderService.getCashOrderInfo(new CashOrderDto(userId, cashType), SecurityConstants.FROM_IN);
        } else {
            r = remoteCashOrderService.getOrderByUserId(userId, SecurityConstants.FROM_IN);
        }

        if (r == null || r.getData() == null) {
            return  Result.error("获取提现信息失败");
        }

        List<SysCashOrder> cashOrderList = r.getData();
        if (cashOrderList != null && cashOrderList.size() > 0) {
            return  Result.ok("获取提现信息成功").put("data", cashOrderList);
        } else {
            return  Result.ok("获取提现信息成功").put("data", "");
        }
    }


    /**
     * <B>获取支付通道信息</B>
     * @param channelId
     * @return
     */
    @RequestMapping(value = "/pay/channel", method =  {RequestMethod.POST, RequestMethod.GET})
    public Result payChannel(String channelId) {
        if (StrUtil.isBlank(channelId)) {
            return Result.error(1, "请求参数错误!");
        }

        R<List<PayChannelVo>> r = remotePayService.queryPayChannel(channelId, SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return null;
        }

        List<PayChannelVo> payChannelList = r.getData();
        return  Result.ok("获取支付通道信息成功").put("data", payChannelList);
    }


    /**
     * @param channelId
     * @param userId
     * @param configId
     * @param payFee
     * @return
     */
    @RequestMapping(value = "/pay/createOrder", method =  {RequestMethod.POST, RequestMethod.GET})
    public Result createOrder(String channelId, String userId, String configId, String payFee, String account, String name) {
        if (StrUtil.isBlank(channelId) || StrUtil.isBlank(userId) || StrUtil.isBlank(configId) || StrUtil.isBlank(account) || StrUtil.isBlank(name)) {
            return Result.error(1, "请求参数错误!");
        }

        PayOrderDto payOrderDto = new PayOrderDto();
        payOrderDto.setChannelId(channelId);
        payOrderDto.setConfigId(configId);

        payOrderDto.setPayfee(payFee);
        payOrderDto.setUserId(userId);
        payOrderDto.setPayaccount(account);

        payOrderDto.setPayname(name);
        R<PayResult> r = remotePayOrderService.createPayOrder(payOrderDto, SecurityConstants.FROM_IN);

        if (r == null || r.getData() == null) {
            return null;
        }

        PayResult result = r.getData();
        if (result != null &&  StrUtil.isNotBlank(result.getOrderNo())) {
            return  Result.ok("创建支付订单成功!");
        } else {
            return  Result.ok("创建支付订单失败!");
        }
    }


    /**
     * <B>调用在线支付成功</B>
     * @param channelId
     * @param type
     * @param userId
     * @param configId
     * @param payFee
     * @return
     */
    @RequestMapping(value = "/pay/doOnline", method =  {RequestMethod.POST, RequestMethod.GET})
    public Result doOnline(String channelId, String type, String userId, String configId, String payFee) {
        if (StrUtil.isBlank(channelId) || StrUtil.isBlank(type) || StrUtil.isBlank(userId) || StrUtil.isBlank(configId)) {
            return Result.error(1, "请求参数错误!");
        }

        //创建订单信息
        PayOrderDto payOrderDto = new PayOrderDto();
        payOrderDto.setChannelId(channelId);
        payOrderDto.setConfigId(configId);
        payOrderDto.setPayfee(payFee);

        payOrderDto.setUserId(userId);
        //R<PayResult> order = remotePayOrderService.createPayOrder(payOrderDto, SecurityConstants.FROM_IN);

        R<PayResult> order = remotePayOrderService.getPayUrl(payOrderDto, SecurityConstants.FROM_IN);
        if (order == null || order.getData() == null) {
            return null;
        }

        PayResult payResult = new PayResult();
        String payUrl = "";
        if (order.getData() instanceof PayResult) {
            payResult = order.getData();

            if (StrUtil.isNotBlank(payResult.getPayUrl())) {
                return Result.ok("调用在线支付成功!").put("payUrl", payResult.getPayUrl());
            } else {
                return  Result.error("调用在线支付失败!");
            }
        } else {
            Map<String, Object> map  = (Map<String, Object>) order.getData();
            payUrl = map.get("payUrl") != null ? map.get("payUrl").toString() : "";
            if (StrUtil.isNotBlank(payUrl)) {
                return  Result.ok("调用在线支付成功!").put("payUrl", payUrl);
            } else {
                return  Result.error("调用在线支付失败!");
            }
        }
    }


    /**
     * <B>获取渠道版本及更新地址信息</B>
     * @param channelId
     * @param version
     * @return
     */
    @RequestMapping(value = "/version/doConfig", method =  {RequestMethod.POST, RequestMethod.GET} , produces = "application/json;charset=utf-8")
    public Result gameConfig(String channelId, String version) {
        if (StrUtil.isBlank(channelId)) {
            return Result.error(1, "请求参数错误!");
        }

        R<List<SysVersionConfig>> r = remoteVersionConfigService.queryVersionConfig(new VersionConfigDto(channelId, version), SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return Result.error(2, "获取信息失败!");
        }

        List<SysVersionConfig> list = r.getData();
        if (CollUtil.isNotEmpty(list) && list.size() > 0) {
            List<Map<String, Object>> resultList = packVersionConfig(list);
            return  Result.ok("获取版本配置信息成功").put("data", resultList);
        } else {
            return  Result.ok("获取版本配置信息成功");
        }
    }


    @RequestMapping(value = "/version/config", method =  {RequestMethod.POST, RequestMethod.GET} )
    public Result gameJsonConfig(@RequestBody VersionConfigJson versionConfig) {
        if (StrUtil.isBlank(versionConfig.getChannelId())) {
            return Result.error(1, "请求参数错误!");
        }

        R<List<SysVersionConfig>> r = remoteVersionConfigService.queryVersionConfig(new VersionConfigDto(versionConfig.getChannelId(), versionConfig.getVersion()), SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return Result.error(2, "获取信息失败!");
        }

        List<SysVersionConfig> list = r.getData();
        if (CollUtil.isNotEmpty(list) && list.size() > 0) {
            List<Map<String, Object>> resultList = packVersionConfig(list);
            return  Result.ok("获取版本配置信息成功").put("data", resultList);
        }
        return null;
    }


    private List<Map<String, Object>> packVersionConfig(List<SysVersionConfig> list) {
        List<Map<String, Object>> resultList = new ArrayList<>();

        for (SysVersionConfig config : list) {
            Map<String, Object> resultMap = new HashMap<String, Object>();
            resultMap.put("return_clear", config.getIsClear());
            resultMap.put("return_info", config.getUpdateUrl());

            resultList.add(resultMap);
        }

        return resultList;
    }



    /**
     *  <B>接口说明</B>
     *  短信功能类型id,短信功能类型,触发功能模块,短信提醒内容
     * 	id:int,
     * 	type:string,
     * 	module:string,
     * 	noticeSMS:string
     * 	0,verify_code,验证码,
     * 	【凤凰游戏】本次验证码:@verifyCode@，如非您本人操作，请及时联系客服临时保护帐号或修改密码。
     * 	1,expection_login_fail,异常登录失败,【凤凰游戏】您此手机绑定的帐号@accountName@于@nowDate@异常登录失败，
     * 	如非您本人操作，请及时联系客服临时保护帐号或修改密码。
     * 	2,expection_login_success,异常登录成功,【凤凰游戏】您此手机绑定的帐号@accountName@于@nowDate@异常登录成功，
     * 	如非您本人操作，请及时联系客服临时保护帐号或修改密码。
     *
     * 	<P> 业务逻辑</P>
     * 	a. 客户端请求验证码，name节点记录请求短信记录， 5分钟不能超过3条，一小时不能超过5条， 1天不能超过15条
     * 	b. a验证通过后login节点将生成短信内容，发往后台
     * 	c. 收到后台回包，后台数据中记录是否发送成功，发送成功，服务器将验证码激活，开始计时，并通知客户端输入验证码开始验证
     *
     * @return
     */
    @RequestMapping(value = "/sms/doSend", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    public Result sendSms(String phone, String templateType, String channelId, String[] parameter) {

        //需要回数据包，下面为数据包格式
        //result.put("code", 0); //类型:int, 0表示成功， -1表示失败
        //result.put("beforeSendMsg", body); //类型：字符串

        //用这个服务方法
        //RemoteSmsService.sendSmsInfo();
        //有WebServiceId, WebSessId， 表示是由后台发起的，见AppController2.java文件中GetVerifyCode接口

        //发送数据给cgi->web服务器->棋牌大厅或者棋牌游戏
        //地址：http://xinbo.com/xinbo.cgi?target=login&function=notifySendSMSResult   发往login
        //参数：result.toJSONString()

        //发送数据给cgi->web服务器->棋牌大厅或者棋牌游戏
        //地址：http://xinbo.com/xinbo.cgi?target=hall&function=notifySendSMSResult   发往hall
        //参数：result.toJSONString(
        //1621,
        //1622,1623,1625,1626,1627,1650,1651,1652,1653,
        //1655,1656,1657,1671,1672,1673,1674,1675,1700,
        //1701,1702,1703,1704,1705,1706,1707,1708,1709,
        //1710,1711,1712,1713,1715,1716,1717,1718,1719

        if (StrUtil.isBlank(channelId) || StrUtil.isBlank(templateType)  || StrUtil.isBlank(phone) || phone.length() != 11 ||  ArrayUtil.isEmpty(parameter)) {
            return Result.error(1, "请求参数错误!");
        }

        //判断是否为移动电话
        if (!Validator.isMobile(phone)) {
            return Result.error(1, "请求参数错误!");
        }

        String phoneHeadStr = phone.substring(0, 4);
        //判断是否是虚拟号码
        if (phoneHeadStr.equalsIgnoreCase("1621") ||
                phoneHeadStr.equalsIgnoreCase("1622") ||
                phoneHeadStr.equalsIgnoreCase("1623") ||
                phoneHeadStr.equalsIgnoreCase("1625") ||
                phoneHeadStr.equalsIgnoreCase("1626") ||

                phoneHeadStr.equalsIgnoreCase("1627") ||
                phoneHeadStr.equalsIgnoreCase("1650") ||
                phoneHeadStr.equalsIgnoreCase("1651") ||
                phoneHeadStr.equalsIgnoreCase("1652") ||

                phoneHeadStr.equalsIgnoreCase("1653") ||
                phoneHeadStr.equalsIgnoreCase("1655") ||
                phoneHeadStr.equalsIgnoreCase("1656") ||
                phoneHeadStr.equalsIgnoreCase("1657") ||
                phoneHeadStr.equalsIgnoreCase("1671") ||

                phoneHeadStr.equalsIgnoreCase("1672") ||
                phoneHeadStr.equalsIgnoreCase("1673") ||
                phoneHeadStr.equalsIgnoreCase("1674") ||
                phoneHeadStr.equalsIgnoreCase("1675") ||

                phone.substring(0, 3).equalsIgnoreCase("170") ||
                phoneHeadStr.equalsIgnoreCase("1710") ||
                phoneHeadStr.equalsIgnoreCase("1711") ||
                phoneHeadStr.equalsIgnoreCase("1712") ||

                phoneHeadStr.equalsIgnoreCase("1713") ||
                phoneHeadStr.equalsIgnoreCase("1715") ||
                phoneHeadStr.equalsIgnoreCase("1716") ||
                phoneHeadStr.equalsIgnoreCase("1718") ||

                phoneHeadStr.equalsIgnoreCase("1719")) {
            return Result.error(3, "虚拟号码无法发送短信!");
        }

//        if (phoneHeadStr.equalsIgnoreCase("162") ||
//                phoneHeadStr.equalsIgnoreCase("165") ||
//                phoneHeadStr.equalsIgnoreCase("167") ||
//                phoneHeadStr.equalsIgnoreCase("170") ||
//                phoneHeadStr.equalsIgnoreCase("171")) {
//            return Result.error(3, "虚拟号码无法发送短信!");
//        }

        R<Boolean> r = remoteSmsService.sendSmsInfo(new SmsDto(channelId, phone, templateType, parameter), SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return Result.error(2, "发送短信失败!");
        }

        Boolean b = r.getData();
        if(b){
            return Result.error(0, "发送短信成功!");
        } else {
            return Result.error(2, "发送短信失败!");
        }
    }


    /**
     * <B>通过模板ID获取信息，如果没有ID,显示所有的模板信息</B>
     * @param sms
     * @return
     */
    /**@RequestMapping(value = "/sms/getSmsTemplate", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    public Result getTemplate(@RequestBody SmsJson sms) {
        if (StrUtil.isBlank(sms.getChannelId())) {
            return Result.error(1, "请求参数错误!");
        }

        R<List<SysSmsTemplate>> r = remoteSmsService.getSmsTemplate(new SmsDto(sms.getChannelId(), sms.getMobile(), sms.getServerCode(), sms.getParameter()), SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return Result.error(2, "获取渠道模板失败!");
        }

        List<SysSmsTemplate> result = r.getData();
        return Result.ok("获取渠道模板成功").put("result", result);
    }**/
    //1621,
    //1622,1623,1625,1626,1627,1650,1651,1652,1653,
    //1655,1656,1657,1671,1672,1673,1674,1675,1700,
    //1701,1702,1703,1704,1705,1706,1707,1708,1709,
    //1710,1711,1712,1713,1715,1716,1717,1718,1719

    @RequestMapping(value = "/sms/send", method = {RequestMethod.POST, RequestMethod.GET})
    public Result sendJsonSms(@RequestBody  SmsJson sms) {
        if (StrUtil.isBlank(sms.getChannelId()) || StrUtil.isBlank(sms.getTemplateType())  || StrUtil.isBlank(sms.getPhone()) || sms.getPhone().length() != 11  ||  ArrayUtil.isEmpty(sms.getParameter())) {
            return Result.error(1, "请求参数错误!");
        }

        //判断是否为移动电话
        if (!Validator.isMobile(sms.getPhone())) {
            return Result.error(1, "请求参数错误!");
        }

        //判断是否是虚拟号码
        String phoneHeadStr = sms.getPhone().substring(0, 4);
        //判断是否是虚拟号码
        if (phoneHeadStr.equalsIgnoreCase("1621") ||
                phoneHeadStr.equalsIgnoreCase("1622") ||
                phoneHeadStr.equalsIgnoreCase("1623") ||
                phoneHeadStr.equalsIgnoreCase("1625") ||
                phoneHeadStr.equalsIgnoreCase("1626") ||

                phoneHeadStr.equalsIgnoreCase("1627") ||
                phoneHeadStr.equalsIgnoreCase("1650") ||
                phoneHeadStr.equalsIgnoreCase("1651") ||
                phoneHeadStr.equalsIgnoreCase("1652") ||

                phoneHeadStr.equalsIgnoreCase("1653") ||
                phoneHeadStr.equalsIgnoreCase("1655") ||
                phoneHeadStr.equalsIgnoreCase("1656") ||
                phoneHeadStr.equalsIgnoreCase("1657") ||
                phoneHeadStr.equalsIgnoreCase("1671") ||

                phoneHeadStr.equalsIgnoreCase("1672") ||
                phoneHeadStr.equalsIgnoreCase("1673") ||
                phoneHeadStr.equalsIgnoreCase("1674") ||
                phoneHeadStr.equalsIgnoreCase("1675") ||

                sms.getPhone().substring(0, 3).equalsIgnoreCase("170") ||
                phoneHeadStr.equalsIgnoreCase("1710") ||
                phoneHeadStr.equalsIgnoreCase("1711") ||
                phoneHeadStr.equalsIgnoreCase("1712") ||

                phoneHeadStr.equalsIgnoreCase("1713") ||
                phoneHeadStr.equalsIgnoreCase("1715") ||
                phoneHeadStr.equalsIgnoreCase("1716") ||
                phoneHeadStr.equalsIgnoreCase("1718") ||

                phoneHeadStr.equalsIgnoreCase("1719")) {
            return Result.error(3, "虚拟号码无法发送短信!");
        }

        //判断是否是虚拟号码
//        if (phoneHeadStr.equalsIgnoreCase("162") ||
//                phoneHeadStr.equalsIgnoreCase("165") ||
//                phoneHeadStr.equalsIgnoreCase("167") ||
//                phoneHeadStr.equalsIgnoreCase("170") ||
//                phoneHeadStr.equalsIgnoreCase("171")) {
//            return Result.error(3, "虚拟号码无法发送短信!");
//        }

        R<Boolean> r = remoteSmsService.sendSmsInfo(new SmsDto(sms.getChannelId(), sms.getPhone(), sms.getTemplateType(), sms.getParameter()), SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return Result.error(2, "发送短信失败!");
        }

        Boolean b = r.getData();
        if(b){
            return Result.ok("发送短信成功!");
        } else {
            return Result.error(2, "发送短信失败!");
        }
    }

    /**
     * <B> 接口说明</B>
     * 获取阿里云的配置信息
     *
     * @param type
     * @param userId
     */
    @RequestMapping(value = "oos/doConfig", method ={RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    public Result ossConfig(String type, String userId) {
        if (StrUtil.isBlank(type)  || StrUtil.isBlank(userId)) {
            return Result.error(1, "请求参数错误!");
        }

        //用这个方法
        R<CloudStorageConfig> r = remoteOssService.getOssConfigInfo( SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return Result.error(2, "获取配置信息失败!");
        }

        Map<String, Object> resultMap = new HashMap<>();
        if (r.getData() instanceof Map) {
            Map<String, Object> sourceMap  = (Map<String, Object>) r.getData();
            packOss(sourceMap, resultMap);
            return Result.ok("获取配置信息成功!").put("data", resultMap).put("userId",userId).put("type", type);
        } else {
            return Result.error(2, "获取配置信息失败!");
        }
    }



    @RequestMapping(value = "oos/config", method ={RequestMethod.POST, RequestMethod.GET})
    public Result ossJsonConfig(@RequestBody OssJson oss) {
        if (StrUtil.isBlank(oss.getType())  || StrUtil.isBlank(oss.getUserId())) {
            return Result.error(1, "请求参数错误!");
        }

        //用这个方法
        R<CloudStorageConfig> r = remoteOssService.getOssConfigInfo( SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return Result.error(2, "获取配置信息失败!");
        }

        Map<String, Object> resultMap = new HashMap<>();
        if (r.getData() instanceof Map) {
            Map<String, Object> sourceMap  = (Map<String, Object>) r.getData();
            packOss(sourceMap, resultMap);
            return Result.ok("获取配置信息成功!").put("data", resultMap).put("userId", oss.getUserId()).put("type", oss.getType());
        } else {
            return Result.error(2, "获取配置信息失败!");
        }
    }

    private void packOss(Map<String, Object> sourceMap, Map<String, Object> resultMap){
        resultMap.put("aliyunDomain", ObjectUtil.isNotNull(sourceMap.get("aliyunDomain")) ? sourceMap.get("aliyunDomain").toString() : "");
        resultMap.put("aliyunPrefix", ObjectUtil.isNotNull(sourceMap.get("aliyunPrefix")) ? sourceMap.get("aliyunPrefix").toString() : "");
        resultMap.put("aliyunEndPoint", ObjectUtil.isNotNull(sourceMap.get("aliyunEndPoint")) ? sourceMap.get("aliyunEndPoint").toString() : "");

        resultMap.put("aliyunAccessKeyId", ObjectUtil.isNotNull(sourceMap.get("aliyunAccessKeyId")) ? sourceMap.get("aliyunAccessKeyId").toString() : "");
        resultMap.put("aliyunAccessKeySecret", ObjectUtil.isNotNull(sourceMap.get("aliyunAccessKeySecret")) ? sourceMap.get("aliyunAccessKeySecret").toString() : "");
        resultMap.put("aliyunBucketName", ObjectUtil.isNotNull(sourceMap.get("aliyunBucketName")) ? sourceMap.get("aliyunBucketName").toString() : "");
    }


    /**
     * <B>接口说明</B>
     * 获取IP地址信息
     * ip：IP地址信息
     * type：类型:sina,taobao
     * sessId : 附加信息，本地不用，原路径传回
     * @param ip
     * @param type
     * @param sessId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/ipaddress/doInfo", method =  {RequestMethod.POST,RequestMethod.GET}, produces = "application/json;charset=utf-8")
    public Result getIpAddress(String ip, String type, String sessId) {
        if (StrUtil.isBlank(ip)  || StrUtil.isBlank(type) || StrUtil.isBlank(sessId)) {
            return Result.error(1, "请求参数错误!");
        }

        //判断参数不符合要求
        if (!type.equals("taobao") && !type.equals("126")) {
            return Result.error(1, "请求参数错误!");
        }

        //判断是否是IP4
        if (!Validator.isIpv4(ip)) {
            return Result.error(1, "请求参数错误!");
        }

        //调用接口查询 IP信息
        String str = HttpHandler.getIpaddress(ip, type);
        GsonIpAddress ipAddress = new Gson().fromJson(str.trim(), GsonIpAddress.class);
        return Result.ok("获取IP地址信息成功!").put("sessId", sessId).put("data", ipAddress.getData());
    }



    @RequestMapping(value = "/ipaddress/info", method =  {RequestMethod.POST,RequestMethod.GET})
    public Result getJsonIpAddress(@RequestBody IpJson ipJson) {
        if (StrUtil.isBlank(ipJson.getIp())  || StrUtil.isBlank(ipJson.getType()) || StrUtil.isBlank(ipJson.getSessId())) {
            return Result.error(1, "请求参数错误!");
        }

        //判断参数不符合要求
        if (!ipJson.getType().equals("taobao") && !ipJson.getType().equals("126")) {
            return Result.error(1, "请求参数错误!");
        }

        //判断是否是IP4
        if (!Validator.isIpv4(ipJson.getIp())) {
            return Result.error(1, "请求参数错误!");
        }

        //调用接口查询 IP信息
        String str = HttpHandler.getIpaddress(ipJson.getIp(), ipJson.getType());
        GsonIpAddress ipAddress = new Gson().fromJson(str.trim(), GsonIpAddress.class);
        return Result.ok("获取IP地址信息成功!").put("sessId", ipJson.getSessId()).put("data", ipAddress.getData());
    }


    /**
     * <B>创建生成二维码信息</B>
     * 创建生成二维码信息
     * accountID : 用户ID
     * clientParam : 参数信息
     *
     * @param url
     * @return
     */
    @RequestMapping(value = "/qrcode/doCreate", method = {RequestMethod.POST,RequestMethod.GET})
    public Result createQRCode(String url) {
        if (StrUtil.isBlank(url) ) {
            return Result.error(1, "请求参数错误.");
        }

        //判断是否是URL
        if (!Validator.isUrl(url)) {
            return Result.error(1, "请求参数错误.");
        }

        R<String> r = remoteOssService.createQrcode(new QrCodeDto(0, 0, 0, url, ""), SecurityConstants.FROM_IN);
        //生成 URL信息并且上传
        if (r == null || r.getData() == null) {
            return Result.error(1, "生成二维码信息失败.");
        }

        String orCodeUrl = r.getData();
        return Result.ok("创建二维码成功").put("orCodeUrl", orCodeUrl);
    }


    /**
     * <B>创建生成二维码信息</B>
     * 创建生成二维码信息
     * accountID : 用户ID
     * clientParam : 参数信息
     *
     * @return
     */
    @RequestMapping(value = "/qrcode/create", method = {RequestMethod.POST,RequestMethod.GET})
    public Result createJsonQRCode(@RequestBody QrCodeJson qrCodeJson) {
        if (StrUtil.isBlank(qrCodeJson.getUrl()) ) {
            return Result.error(1, "请求参数错误!");
        }

        //判断是否是URL
        if (!Validator.isUrl(qrCodeJson.getUrl())) {
            return Result.error(1, "请求参数错误!");
        }

        R<String> r = remoteOssService.createQrcode(new QrCodeDto(0, 0, 0, qrCodeJson.getUrl(), ""), SecurityConstants.FROM_IN);
        //生成 URL信息并且上传
        if (r == null || r.getData() == null) {
            return Result.error(1, "生成二维码信息失败!");
        }

        String orCodeUrl = r.getData();
        return Result.ok("创建二维码成功").put("orCodeUrl", orCodeUrl);
    }


    @RequestMapping(value = "/defineqrcode/doCreate", method = {RequestMethod.POST,RequestMethod.GET}, produces = "application/json;charset=utf-8")
    public Result createDefineQRCode(String url, String width, String height,  String margin, String logoUrl) {

        if (StrUtil.isBlank(url) || StrUtil.isBlank(logoUrl) ) {
            return Result.error(1, "请求参数错误!");
        }

        //判断是否是URL
        if (!Validator.isUrl(url) && !Validator.isUrl(logoUrl)) {
            return Result.error(1, "请求参数错误!");
        }

        //判断长宽是否为数据
        if (StrUtil.isNotBlank(width) && !Validator.isNumber(width)) {
            return Result.error(1, "请求参数错误!");
        }

        if (StrUtil.isNotBlank(height) && !Validator.isNumber(height)) {
            return Result.error(1, "请求参数错误!");
        }

        Integer widthRs  = StrUtil.isNotBlank(width) ? Integer.parseInt(width) : 0;
        Integer heightRs  = StrUtil.isNotBlank(height) ? Integer.parseInt(height) : 0;
        Integer marginRs  = StrUtil.isNotBlank(margin) ? Integer.parseInt(margin) : 0;

        R<String> r = remoteOssService.createDefineQrcode(new QrCodeDto(widthRs, heightRs, marginRs, url, logoUrl), SecurityConstants.FROM_IN);
        //生成 URL信息并且上传
        if (r == null || r.getData() == null) {
            return Result.error(1, "生成二维码信息失败!");
        }

        String orCodeUrl = r.getData();
        return Result.ok("创建二维码成功").put("orCodeUrl", orCodeUrl);
    }



    @RequestMapping(value = "/defineqrcode/create", method = {RequestMethod.POST,RequestMethod.GET}, produces = "application/json;charset=utf-8")
    public Result createDefineQRCode(@RequestBody QrCodeJson qrCodeJson) {

        if (StrUtil.isBlank(qrCodeJson.getUrl()) || StrUtil.isBlank(qrCodeJson.getLogoUr()) ) {
            return Result.error(1, "请求参数错误!");
        }

        //判断是否是URL
        if (StrUtil.isBlank(qrCodeJson.getUrl()) || StrUtil.isBlank(qrCodeJson.getLogoUr()) ) {
            return Result.error(1, "请求参数错误!");
        }

        //判断长宽是否为数据
        if (StrUtil.isNotBlank(qrCodeJson.getWidth()) && !Validator.isNumber(qrCodeJson.getWidth())) {
            return Result.error(1, "请求参数错误!");
        }

        if (StrUtil.isNotBlank(qrCodeJson.getHeight()) && !Validator.isNumber(qrCodeJson.getHeight())) {
            return Result.error(1, "请求参数错误!");
        }

        Integer widthRs  = StrUtil.isNotBlank(qrCodeJson.getWidth()) ? Integer.parseInt(qrCodeJson.getWidth()) : 0;
        Integer heightRs  = StrUtil.isNotBlank(qrCodeJson.getHeight()) ? Integer.parseInt(qrCodeJson.getHeight()) : 0;
        Integer marginRs  = StrUtil.isNotBlank(qrCodeJson.getMargin()) ? Integer.parseInt(qrCodeJson.getMargin()) : 0;

        R<String> r = remoteOssService.createDefineQrcode(new QrCodeDto(widthRs, heightRs, marginRs, qrCodeJson.getUrl(), qrCodeJson.getLogoUr()), SecurityConstants.FROM_IN);
        //生成 URL信息并且上传
        if (r == null || r.getData() == null) {
            return Result.error(1, "生成二维码信息失败!");
        }

        String orCodeUrl = r.getData();
        return Result.ok("创建二维码成功").put("orCodeUrl", orCodeUrl);
    }


    /**
     * <B>功能说明</B>
     * 金币兑换,用户提现申请
     * @param accountId      玩家ID
     * @param exchangeMoney  兑换金额（人民币，这里单位是分，换算成圆，要除以100
     * @param type           支付类型 1-支付宝 2-银行卡 3微信
     * @param realName       如果只支付宝这里就是支付宝绑定姓名， 如果是银行卡这里就是绑定的真实姓名
     * @param payId          如果只支付宝这里就是支付宝账号， 如果是银行卡这里就是银行卡卡号
     * @return
     */
    @RequestMapping(value = "/goldcoin/doCash", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    public Result goldCoinCash(String accountId, String channelId, String exchangeMoney,  String type, String realName, String payId,
                               String bank, String subBank, String province, String city) {
        log.info("提现接受到参数doCash:" + accountId + "channelId: " + channelId);
        Long beginTime = new Date().getTime();
        if (StrUtil.isBlank(accountId) || StrUtil.isBlank(exchangeMoney) ||  StrUtil.isBlank(type) || StrUtil.isBlank(realName) || StrUtil.isBlank(payId)) {
            return Result.error(1, "请求参数错误!");
        }

        String address = "";
        //判断是银行卡
        if (type.equals("2")) {
            if (StrUtil.isBlank(bank) || StrUtil.isBlank(subBank) || StrUtil.isBlank(city) || StrUtil.isBlank(province)) {
                return Result.error(1, "请求参数错误!");
            } else {
                 address = province + city;
            }
        }

        R<SysCashOrder> r = remoteCashOrderService.createCashOrder(new CashOrderDto(accountId, exchangeMoney, type, realName, payId, channelId), SecurityConstants.FROM_IN);
        log.info("提现申请接口请求时长:" + (new Date().getTime() - beginTime));
        if (r == null || r.getData() == null) {
            log.info("提现申请调用服务失败");
            return Result.error(2, "申请提现失败!");
        }

        log.info("返回参数:" + r.getData());
        Map<String, Object> resultMap = new HashMap<>();
        if (r.getData() instanceof Map) {
            Map<String, Object> sourceMap = (Map<String, Object>) r.getData();
            packCash(sourceMap, resultMap);
        }
        return Result.ok("申请提现成功!").put("data", resultMap);
    }



    @RequestMapping(value = "/goldcoin/cash", method = {RequestMethod.POST, RequestMethod.GET})
    public Result goldCoinJsonCash(@RequestBody CashJson cashJson) {
        log.info("提现接受到参数:" + cashJson.toString() + "channelId:" + cashJson.getChannelId() + "userId:" + cashJson.getAccountId());
        Long beginTime = new Date().getTime();
        if (StrUtil.isBlank(cashJson.getAccountId()) || StrUtil.isBlank(cashJson.getExchangeMoney()) ||  StrUtil.isBlank(cashJson.getType()) || StrUtil.isBlank(cashJson.getRealName()) || StrUtil.isBlank(cashJson.getPayId())) {
            return Result.error(1, "请求参数错误!");
        }

        String address = "";
        //判断是银行卡
        if (cashJson.getType().equals("2")) {
            if (StrUtil.isBlank(cashJson.getBank()) || StrUtil.isBlank(cashJson.getSubBank())) {
                return Result.error(1, "请求参数错误!");
            } else {
                 if (StrUtil.isNotEmpty(cashJson.getProvince()) && StrUtil.isNotEmpty(cashJson.getCity())) {
                     address = cashJson.getProvince() + cashJson.getCity();
                 } else if (StrUtil.isNotEmpty(cashJson.getProvince()) && StrUtil.isEmpty(cashJson.getCity())) {
                     address = cashJson.getProvince();
                 } else if (StrUtil.isNotEmpty(cashJson.getCity()) && StrUtil.isEmpty(cashJson.getProvince())) {
                     address = cashJson.getCity();
                 }
            }
        }

        R<SysCashOrder> r = remoteCashOrderService.createCashOrder(new CashOrderDto(cashJson.getAccountId(), cashJson.getChannelId(), cashJson.getExchangeMoney(), cashJson.getType(),
                cashJson.getRealName(), cashJson.getPayId(), cashJson.getBank(), cashJson.getSubBank(), address),  SecurityConstants.FROM_IN);
        log.info("提现申请接口请求时长:" + (new Date().getTime() - beginTime));
        if (r == null || r.getData() == null) {
            log.info("提现申请调用服务失败");
            return Result.error(2, "申请提现失败!");
        }

        Map<String, Object> resultMap = new HashMap<>();
        Integer accountID = 0;
        if (r.getData() instanceof Map) {
            Map<String, Object> sourceMap = (Map<String, Object>) r.getData();
            accountID =  ObjectUtil.isNotNull(sourceMap.get("userId")) ? (Integer)sourceMap.get("userId") : Integer.parseInt(cashJson.getAccountId());
            packCash(sourceMap, resultMap);
        }

        log.info("提现返回参数:" + resultMap.toString());
        return Result.ok("申请提现成功!").put("data", resultMap).put("accountID", accountID);
    }


    private void packCash(Map<String, Object> sourceMap, Map<String, Object> resultMap){
        resultMap.put("cashNo", ObjectUtil.isNotNull(sourceMap.get("cashNo")) ? sourceMap.get("cashNo").toString() : "");
        resultMap.put("cashFee", ObjectUtil.isNotNull(sourceMap.get("cashFee")) ? sourceMap.get("cashFee").toString() : "");

        resultMap.put("channelId", ObjectUtil.isNotNull(sourceMap.get("channelId")) ? sourceMap.get("channelId").toString() : "");
        resultMap.put("account", ObjectUtil.isNotNull(sourceMap.get("account")) ? sourceMap.get("account").toString() : "");
        resultMap.put("accountName", ObjectUtil.isNotNull(sourceMap.get("accountName")) ? sourceMap.get("accountName").toString() : "");

        resultMap.put("cashType", ObjectUtil.isNotNull(sourceMap.get("cashType")) ? sourceMap.get("cashType").toString() : "");
    }


    /**
     * <B>获取微信config信息</B>
     * @param channelId
     * @return
     */
    @RequestMapping(value = "/social/config", method =  {RequestMethod.POST, RequestMethod.GET})
    public Result configSocial(String channelId) {
        if (StrUtil.isBlank(channelId)) {
            return Result.error(1, "请求参数错误!");
        }

        R<SocialConfig> config = remoteSocialConfigService.getSocialConfigById(channelId, SecurityConstants.FROM_IN);
        if (config == null || config.getData() == null) {
            return Result.error(2, "获取第三方配置信息失败!");
        }
        return Result.ok("获取微信配置信息成功!").put("data", config.getData());
    }



    /**
     * <B>根据信息获取第三方微信数据</B>
     * @param code
     * @param channelId
     * @return
     */
    @RequestMapping(value = "/social/tonken", method =  {RequestMethod.POST, RequestMethod.GET})
    public Result tonkenSocial(String code, String channelId) {
        if (StrUtil.isBlank(channelId) || StrUtil.isBlank(code)) {
            return Result.error(1, "请求参数错误!");
        }

        R<SocialConfig> config = remoteSocialConfigService.getSocialConfigById(channelId, SecurityConstants.FROM_IN);
        if (config == null || config.getData() == null) {
            return Result.error(2, "获取第三方配置信息失败!");
        }

        String appId = "";
        String appSecret = "";

        if (config.getData() instanceof  Map) {
            Map<String, Object> map  = (Map<String, Object>) config.getData();
            appId = (String)map.get("appId");
            appSecret = (String)map.get("appSecret");
        } else {
            SocialConfig socialConfig = config.getData();
            appId = socialConfig.getAppId();
            appSecret = socialConfig.getAppSecret();
        }

        GsonWechatToken token = HttpHandler.getWechatToken(appId, appSecret, code);
        return Result.ok("获取TONKEN成功!").put("data", token);
    }


    /**
     * <B>获取微信用户信息</B>
     * @param wechatJson
     * @return
     */
    @RequestMapping(value = "/social/userinfo", method =  {RequestMethod.POST, RequestMethod.GET})
    public Result userInfoSocial(@RequestBody WechatJson wechatJson) {
        if (StrUtil.isBlank(wechatJson.getAuthID()) || StrUtil.isBlank(wechatJson.getAuthToken())) {
            return Result.error(1, "请求参数错误!");
        }

        GsonWechatUserInfo userInfo = HttpHandler.getWechatUserInfo(wechatJson.getAuthToken(), wechatJson.getAuthID()) ;
        if (userInfo != null && StrUtil.isNotBlank(userInfo.getNickname())) {
            return Result.ok("获取微信用户信息成功!").put("data", userInfo);
        } else {
            return Result.error(2,"获取微信用户信息失败!");
        }
    }

    /**
     * 银商账户-上分接口
     * @return
     */
    @RequestMapping(value = "/org/addPoint", method = RequestMethod.POST)
    public Result addPoint(@RequestBody JSONObject data) {
        JSONObject resJson = new JSONObject();
        resJson.put("flag",false);
        StringBuilder sb = new StringBuilder();

        String amount = data.getString("amount") == null ? "" : data.getString("amount");
        String random = data.getString("random") == null ? "" : data.getString("random");
        String userId = data.getString("userId") == null ? "" : data.getString("userId");
        String orgId = data.getString("orgId") == null ? "" : data.getString("orgId");
        String channelId = data.getString("channelId") == null ? "" : data.getString("channelId");
        String operateUserId = data.getString("operateUserId") == null ? "" : data.getString("operateUserId");

        sb.append("amount=" + amount);
        sb.append("random=" + random);
        sb.append("userId=" + userId);
        sb.append("orgId=" + orgId);
        sb.append("operateUserId=" + operateUserId);

        String md5 = SignUtil.encryption(sb.toString()) + "f396546c11a24dc78dc21415723a9992";
        String sign = SignUtil.encryption(md5);
        if(sign.equalsIgnoreCase(data.getString("sign"))){
            log.warn("收到银商上分数据-->{}",data.toJSONString());
            AddPointDto addPointDto = new AddPointDto(amount, userId, operateUserId, orgId, channelId);

            R<AddPointVo> r = remotePayOrderService.addPoint(addPointDto, SecurityConstants.FROM_IN);
            if (r == null && r.getData() == null && r.getData().getStatus() != 1) {
                return Result.error(2,"银商上分失败!");
            }

            return Result.ok("银商上分成功!").put("data", r.getData());
        }
        return null;
    }
}
