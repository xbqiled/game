package com.youwan.game.api.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class CashJson implements Serializable {
    private String accountId;
    private String exchangeMoney;
    private String channelId;
    private String type;
    private String realName;
    private String payId;
    private String bank;
    private String subBank;
    private String province;
    private String city;
}
