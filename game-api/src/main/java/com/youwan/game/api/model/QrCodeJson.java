package com.youwan.game.api.model;

import lombok.Data;

import java.io.Serializable;


@Data
public class QrCodeJson  implements Serializable {


    private String url;
    private String width;
    private String height;
    private String margin;
    private String logoUr;


}
