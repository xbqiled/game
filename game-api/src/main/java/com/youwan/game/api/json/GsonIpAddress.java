package com.youwan.game.api.json;


import lombok.Data;

/**
 * <B>用户版本管理配置信息</B>
 */
@Data
public class GsonIpAddress {

   private int code;

   private RusltData data;

   @Data
   public class RusltData {
      private String ip;

      private String country;

      private String area;

      private String region;

      private String city;

      private String county;

      private String isp;

      private String country_id;

      private String area_id;

      private String region_id;

      private String city_id;

      private String county_id;

      private String isp_id;
   }

}
