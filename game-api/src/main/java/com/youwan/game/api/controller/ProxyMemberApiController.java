package com.youwan.game.api.controller;

import com.youwan.game.cac.api.dto.ProxyMemberQueryDto;
import com.youwan.game.cac.api.entity.*;
import com.youwan.game.cac.api.feign.RemotePayOrderService;
import com.youwan.game.cac.api.feign.RemoteProxyMemberService;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.data.core.Page;
import com.youwan.game.common.security.annotation.Inner;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-04-27.
 */
@RestController
@Slf4j
@RequestMapping("/itf/proxyMember")
public class ProxyMemberApiController {

    private final RemoteProxyMemberService remoteProxyMemberService;

    public ProxyMemberApiController(RemoteProxyMemberService remoteProxyMemberService) {
        this.remoteProxyMemberService = remoteProxyMemberService;
    }

    @GetMapping("/getMemberProfile/{accountId}")
    public R<List<ProxyMemberGameScoreStats>> getGameScoreStats(@PathVariable("accountId") Long accountId) {
        return remoteProxyMemberService.getMemberProfile(accountId, SecurityConstants.FROM_IN);
    }

    @PostMapping("/getDirectSubordinates/{accountId}")
    public R<Page<ProxyMember>> getDirectSubordinates(@PathVariable("accountId") Long accountId,
                                                      @RequestBody ProxyMemberQueryDto queryDto) {
        return remoteProxyMemberService.getDirectSubordinates(accountId, queryDto, SecurityConstants.FROM_IN);
    }

    @PostMapping("/getAllSubordinates/{accountId}")
    public R<Page<ProxyMember>> getAllSubordinates(@PathVariable("accountId") Long accountId,
                                                   @RequestBody ProxyMemberQueryDto queryDto) {
        return remoteProxyMemberService.getAllSubordinates(accountId, queryDto, SecurityConstants.FROM_IN);
    }

    @PostMapping("/getTreasureChanges/{accountId}")
    public R<Page<ProxyMember>> getTreasureChanges(@PathVariable("accountId") Long accountId,
                                                   @RequestBody ProxyMemberQueryDto queryDto) {
        return remoteProxyMemberService.getTreasureChanges(accountId, queryDto, SecurityConstants.FROM_IN);
    }

    @PostMapping("/getCashOrders/{accountId}")
    public R<Page<ProxyMember>> getCashOrders(@PathVariable("accountId") Long accountId,
                                              @RequestBody ProxyMemberQueryDto queryDto) {
        return remoteProxyMemberService.getCashOrders(accountId, queryDto, SecurityConstants.FROM_IN);
    }

    @PostMapping("/getRechargeOrders/{accountId}")
    public R<Page<ProxyMember>> getRechargeOrders(@PathVariable("accountId") Long accountId,
                                                  @RequestBody ProxyMemberQueryDto queryDto) {
        return remoteProxyMemberService.getRechargeOrders(accountId, queryDto, SecurityConstants.FROM_IN);
    }

    @PostMapping("/getGameScoreStats/{accountId}")
    public R<List<ProxyMemberGameScoreStats>> getGameScoreStats(@PathVariable("accountId") Long accountId,
                                                                @RequestBody ProxyMemberQueryDto queryDto) {
        return remoteProxyMemberService.getGameScoreStats(accountId, queryDto, SecurityConstants.FROM_IN);
    }

    @PostMapping("/getGameScoreDetails/{accountId}")
    public R<Page<ProxyMember>> getGameScores(@PathVariable("accountId") Long accountId,
                                              @RequestBody ProxyMemberQueryDto queryDto) {
        return remoteProxyMemberService.getGameScores(accountId, queryDto, SecurityConstants.FROM_IN);
    }

    @PostMapping("/getTeamMetrics/{accountId}")
    public R<ProxyMemberTeamMetrics> getTeamMetrics(@PathVariable("accountId") Long accountId,
                                                    @RequestBody ProxyMemberQueryDto queryDto) {
        return remoteProxyMemberService.getTeamMetrics(accountId, queryDto, SecurityConstants.FROM_IN);
    }

    @PostMapping("/getSubordinateTeamMetrics/{accountId}")
    public R<Page<ProxyMemberTeamMetrics>> getSubordinateTeamMetrics(@PathVariable("accountId") Long accountId,
                                                                     @RequestBody ProxyMemberQueryDto queryDto) {
        return remoteProxyMemberService.getSubordinateTeamMetrics(accountId, queryDto, SecurityConstants.FROM_IN);
    }

    @PostMapping("/getRebateDetails/{accountId}")
    public R<Page<ProxyMemberRebateDetail>> getRebateDetails(@PathVariable("accountId") Long accountId,
                                                             @RequestBody ProxyMemberQueryDto queryDto) {
        return remoteProxyMemberService.getRebateDetails(accountId, queryDto, SecurityConstants.FROM_IN);
    }

    @PostMapping("/getRebateBalances/{accountId}")
    public R<Page<ProxyMemberRebateBalance>> getRebateBalances(@PathVariable("accountId") Long accountId,
                                                               @RequestBody ProxyMemberQueryDto queryDto) {
        return remoteProxyMemberService.getRebateBalances(accountId, queryDto, SecurityConstants.FROM_IN);
    }
}
