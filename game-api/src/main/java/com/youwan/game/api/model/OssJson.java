package com.youwan.game.api.model;


import lombok.Data;

import java.io.Serializable;


@Data
public class OssJson implements Serializable {
    private String type;

    private String userId;

}
