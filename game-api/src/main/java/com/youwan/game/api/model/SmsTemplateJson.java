package com.youwan.game.api.model;


import lombok.Data;

import java.io.Serializable;


@Data
public class SmsTemplateJson implements Serializable {

    private String phone;
    private String template;
    private String [] parameter;

}
