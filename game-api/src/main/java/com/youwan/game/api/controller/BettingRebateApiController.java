package com.youwan.game.api.controller;

import com.youwan.game.cac.api.dto.BettingRecordQueryDto;
import com.youwan.game.cac.api.feign.RemoteBettingRebateService;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.util.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-10.
 */
@RestController
@Slf4j
@RequestMapping("/itf/bettingRebate")
public class BettingRebateApiController {

    private RemoteBettingRebateService remoteBettingRebateService;

    public BettingRebateApiController(RemoteBettingRebateService remoteBettingRebateService) {
        this.remoteBettingRebateService = remoteBettingRebateService;
    }

    @GetMapping("/scrub/{userId}/details")
    public R getScrubRebateDetails(@PathVariable("userId") Long userId) {
        return remoteBettingRebateService.getScrubRebateDetails(userId, SecurityConstants.FROM_IN);
    }

    @PostMapping("/scrub/{userId}/_take")
    public R takeScrubRebate(@PathVariable("userId") Long userId) {
        return remoteBettingRebateService.takeScrubRebate(userId, SecurityConstants.FROM_IN);
    }

    @PostMapping("/scrub/{userId}/takingRecords")
    public R getPersonTakingRecords(@PathVariable("userId") Long userId, @RequestBody BettingRecordQueryDto queryDto) {
        return remoteBettingRebateService.getPersonTakingRecords(userId, queryDto, SecurityConstants.FROM_IN);
    }

    @GetMapping("/scrub/{userId}/history")
    public R getScrubRebateHistory(@PathVariable("userId") Long userId,
                                   @RequestParam(value = "offset", required = false, defaultValue = "0") Integer offset,
                                   @RequestParam(value = "limit", required = false, defaultValue = "10") Integer limit) {
        return remoteBettingRebateService.getScrubRebateHistory(userId, offset, limit,SecurityConstants.FROM_IN);
    }
}
