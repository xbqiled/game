package com.youwan.game.api.controller;


import com.youwan.game.common.core.util.Result;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


/**
 * <B> 所有第三方渠道提供的接口</B>
 */
@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/channelHandle")
public class ChannelApiController {


    /**
     * <B>用来登录游戏，如果当前账号不存在，就创建账号</B>
     *
     * @param channelId    渠道ID
     * @param content
     * @param source
     * @param verifyCode
     * @param webServiceId
     * @param webSessId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/loginGame", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public Result loginGame(String channelId, String content, String source, String verifyCode, long webServiceId, long webSessId) {

        //s           操作子类型,用来判断请求哪个接口
        //ip          用户IP信息,
        //lineCode    代理下面的站点标识, 用防止站点之间导分
        //account     账号信息,
        //orderid     流水号，
        //KindID      游戏ID，传入不同的KINDID就进入不同的游戏

        return null;
    }


    /**
     * <B>查询会员可以下分金额</B>
     *
     * @param channelId
     * @param content
     * @param source
     * @param verifyCode
     * @param webServiceId
     * @param webSessId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/accountBalance", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public Result accountBalance(String channelId, String content, String source, String verifyCode, long webServiceId, long webSessId) {

        // timestamp  时间戳
        // s    操作子类型,用来判断请求哪个接口
        // account  账号类型
        // key

        //返回值
        // {"s":101,"m":"/channelHandle","d":{"money":100,"code":0}}
        // s自操作类型
        // m主操作类型
        // d数据结果
        return null;
    }



    /**
     * <B>会员上分</B>
     *
     * @param channelId
     * @param content
     * @param source
     * @param verifyCode
     * @param webServiceId
     * @param webSessId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/accountRecharge", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public Result accountRecharge(String channelId, String content, String source, String verifyCode, long webServiceId, long webSessId) {
        //s：操作子类型:2
        //account：会员帐号
        //money：金额(上分的金额)
        //orderid：流水号（格式：代理编号+yyyyMMddHHmmssSSS+account）
        //Encrypt.AESEncrypt(param,DESKey);
        //DESKey：平台提供


        //返回值 {"s":102,"m":"/channelHandle","d":{"code":0}}
        // s自操作类型
        // m主操作类型
        // d数据结果
        return null;
    }



    /**
     * <B>会员下分</B>
     *
     * @param channelId
     * @param content
     * @param source
     * @param verifyCode
     * @param webServiceId
     * @param webSessId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/accountCash", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public Result accountCash(String channelId, String content, String source, String verifyCode, long webServiceId, long webSessId) {
        //s：操作子类型:3
        //account：会员帐号
        //money：金额(下分的金额，不要超过可下分数)
        //orderid：流水号（格式：代理编号+yyyyMMddHHmmssSSS+ account）
        //Encrypt.AESEncrypt(param,DESKey);
        //DESKey：平台提供

        //{"s":103,"m":"/channelHandle","d":{"account":"111111","code":0}}
        // d数据结果
        return null;
    }


    /**
     * <B>查询会员上下分订单</B>
     *
     * @param channelId
     * @param content
     * @param source
     * @param verifyCode
     * @param webServiceId
     * @param webSessId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/queryOrder", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public Result queryOrder(String channelId, String content, String source, String verifyCode, long webServiceId, long webSessId) {
        //agent
        //s：操作子类型:4
        //orderid：流水号（格式：代理编号+yyyyMMddHHmmssSSS+account）
        //Encrypt.AESEncrypt(param,DESKey);
        //DESKey：平台提供
        //key

        //{"s":104,"m":"/channelHandle","d":{"code":0,"status":2}}
        return null;
    }


    /**
     * <B>查询会员玩家是否在线</B>
     *
     * @param channelId
     * @param content
     * @param source
     * @param verifyCode
     * @param webServiceId
     * @param webSessId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/isOnline", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public Result isOnline(String channelId, String content, String source, String verifyCode, long webServiceId, long webSessId) {
        //agent
        //timestamp
        //s：操作子类型：5
        //account：玩家帐号
        //Encrypt.AESEncrypt(param,DESK
        //ey);
        //DESKey：平台提供
        //key

        //{"s":105,"m":"/channelHandle","d":{"code":0,"status":1}}

        return null;
    }



    /**
     * <B>查询游戏注单</B>
     *
     * @param channelId
     * @param content
     * @param source
     * @param verifyCode
     * @param webServiceId
     * @param webSessId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/betRecord", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public Result betRecord(String channelId, String content, String source, String verifyCode, long webServiceId, long webSessId) {
        // agent
        //参数加密字符串 param=（s=6&startTime=1488781836949&endTime=1488781836949）
        // s：操作子类型:6
        // startTime：开始时间(Unix 时间戳带上毫秒)
        // endTime：结束时间(Unix 时间戳带上毫秒)
        // Encrypt.AESEncrypt(param,DESKey);
        // DESKey：平台提供

        //{"s":106,"m":"/getRecordHandle","d":{"list":{"GameID":[062036007452964330-255],"Accounts":["test"],"ServerID":[3602],"KindID":[620],"TableID":[1],"ChairI
        //D":[3],"UserCount":[2],"CardValue":[0709292a0000000000000000252b0000211104281d181a],"CellScore":[0],"AllBet":[0],"Profit":[800],"Revenue":[0],"GameStartTime":
        //[" 2017-04-21 14:41:25"],"GameEndTime":[" 2017-04-21 14:45:25"],"ChannelID":[10001],"CreateTime":[" 2017-04-21 12:41:25"]},"count":13,"code":0,
        //"serverStartTime":"2017-03-27 13:00:00","serverEndTime":"2017-03-27 15:00:00"}}

        return null;
    }



    /**
     * <B>查询游戏总分</B>
     *
     * @param channelId
     * @param content
     * @param source
     * @param verifyCode
     * @param webServiceId
     * @param webSessId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/userAccount", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public Result UserAccount(String channelId, String content, String source, String verifyCode, long webServiceId, long webSessId) {
        //agent
        //timestamp
        //param s：操作子类型:7
        //account：会员帐号）
        //Encrypt.AESEncrypt(param,DESK
        //ey);
        //key

        //{"s":107,"m":"/channelHandle","d":{"totalMoney":100, "freeMoney":80,"status":0,"code":0}}
        return null;
    }



    /**
     * <B>踢玩家下线</B>
     *
     * @param channelId
     * @param content
     * @param source
     * @param verifyCode
     * @param webServiceId
     * @param webSessId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/kickUser", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public Result kickUser(String channelId, String content, String source, String verifyCode, long webServiceId, long webSessId) {
        //agent
        //timestamp
        //param
        //s：操作子类型:8
        //account：会员帐号）
        //Encrypt.AESEncrypt(param,DESKey);
        //DESKey：平台提供

        //{"s":108,"m":"/channelHandle","d":{"code":0}}
        return null;
    }



    /**
     * <B>查询代理余额</B>
     *
     * @param channelId
     * @param content
     * @param source
     * @param verifyCode
     * @param webServiceId
     * @param webSessId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/channelBalance", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public Result channelBalance(String channelId, String content, String source, String verifyCode, long webServiceId, long webSessId) {


        return null;
    }

}
