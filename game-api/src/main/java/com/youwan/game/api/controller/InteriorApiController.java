package com.youwan.game.api.controller;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.google.gson.Gson;
import com.youwan.game.admin.api.dto.QrCodeDto;
import com.youwan.game.admin.api.feign.RemoteOssService;
import com.youwan.game.api.json.GsonIpAddress;
import com.youwan.game.api.model.*;
import com.youwan.game.cac.api.dto.*;
import com.youwan.game.cac.api.entity.CsInfo;
import com.youwan.game.cac.api.entity.SocialConfig;
import com.youwan.game.cac.api.entity.SysCashOrder;
import com.youwan.game.cac.api.entity.SysVersionConfig;
import com.youwan.game.cac.api.feign.*;
import com.youwan.game.cac.api.vo.PayChannelVo;
import com.youwan.game.cac.api.vo.PayResult;
import com.youwan.game.cac.api.vo.ShortUrlVo;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.jsonmodel.GsonWechatToken;
import com.youwan.game.common.core.jsonmodel.GsonWechatUserInfo;
import com.youwan.game.common.core.oss.CloudStorageConfig;
import com.youwan.game.common.core.util.HttpHandler;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.core.util.Result;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/interiorApi")
public class InteriorApiController {

    //处理支付订单
    private final RemotePayOrderService remotePayOrderService;

    //OSS服务
    private final RemoteOssService remoteOssService;

    //提现服务
    private final RemoteCashOrderService remoteCashOrderService;

    //短信接口服务
    private final RemoteSmsService remoteSmsService;

    //版本配置服务
    private final RemoteVersionConfigService remoteVersionConfigService;

    //处理信息
    private final RemoteSocialConfigService remoteSocialConfigService;

    //支付接口
    private final RemotePayService remotePayService;

    //客服信息接口
    private final RemoteCsService remoteCsService;

    //短链接处理服务接口
    private final RemoteJumpService remoteJumpService;


    /**
     * <B>创建短链接</B>
     * @param url
     * @return
     */
    @RequestMapping(value = "/share/createShortUrl", method =  {RequestMethod.POST, RequestMethod.GET})
    public Result getShareUrl(String url, String channelId, String isRefresh) {
        if (StrUtil.isBlank(isRefresh)) {
            return Result.error(1, "未获取到tonken信息!");
        }

        if (StrUtil.isBlank(url)) {
            return Result.error(1, "请求参数错误!");
        }

        R<ShortUrlVo> r = remoteJumpService.createShortUrl(new ShareDto(url, channelId, isRefresh), SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return  Result.error("获取短链接信息失败");
        }

        ShortUrlVo result = r.getData();
        if (result != null && StrUtil.isNotEmpty(result.getJumpUrl())  && StrUtil.isNotEmpty(result.getShortUrl()) ) {
            return  Result.ok("获取短链接信息成功").put("shortUrl", result);
        } else {
            return  Result.error("获取短链接信息失败");
        }
    }


    /**
     * <B>通过渠道获取客服信息</B>
     * @param channelId
     * @return
     */
    @RequestMapping(value = "/cs/info", method =  {RequestMethod.POST, RequestMethod.GET})
    public Result csInfo(String channelId, String type, String token) {
        if (StrUtil.isBlank(token)) {
            return Result.error(1, "未获取到tonken信息!");
        }

        if (StrUtil.isBlank(channelId)) {
            return Result.error(1, "请求参数错误!");
        }

        R<List<CsInfo>> r = remoteCsService.queryCsInfo(new CsInfoDto(channelId, type), SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return  Result.error("获取客服信息失败");
        }

        List<CsInfo> list = r.getData();
        return  Result.ok("获取客服信息成功").put("data", list);
    }


    /**
     * <B>获取提现信息</B>
     * @param userId
     * @return
     */
    @RequestMapping(value = "/cash/info", method =  {RequestMethod.POST, RequestMethod.GET})
    public Result cashInfo(String userId, String cashType, String token) {
        if (StrUtil.isBlank(token)) {
            return Result.error(1, "未获取到tonken信息!");
        }
        if (StrUtil.isBlank(userId)) {
            return Result.error(1, "请求参数错误!");
        }

        R<List<SysCashOrder>> r = null;
        if (StrUtil.isNotBlank(userId) && StrUtil.isNotBlank(cashType)) {
            r = remoteCashOrderService.getCashOrderInfo(new CashOrderDto(userId, cashType), SecurityConstants.FROM_IN);
        } else {
            r = remoteCashOrderService.getOrderByUserId(userId, SecurityConstants.FROM_IN);
        }

        if (r == null || r.getData() == null) {
            return  Result.error("获取提现信息失败");
        }

        List<SysCashOrder> cashOrderList = r.getData();
        if (cashOrderList != null && cashOrderList.size() > 0) {
            return  Result.ok("获取提现信息成功").put("data", cashOrderList);
        } else {
            return  Result.ok("获取提现信息成功");
        }
    }


    /**
     * <B>获取支付通道信息</B>
     * @param channelId
     * @return
     */
    @RequestMapping(value = "/pay/channel", method =  {RequestMethod.POST, RequestMethod.GET})
    public Result payChannel(String channelId, String token) {
        if (StrUtil.isBlank(token)) {
            return Result.error(1, "未获取到tonken信息!");
        }

        if (StrUtil.isBlank(channelId)) {
            return Result.error(1, "请求参数错误!");
        }

        R<List<PayChannelVo>> r = remotePayService.queryPayChannel(channelId, SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return null;
        }

        List<PayChannelVo> payChannelList = r.getData();
        return  Result.ok("获取支付通道信息成功").put("data", payChannelList);
    }


    /**
     * <B>调用在线支付成功</B>
     * @param channelId
     * @param type
     * @param userId
     * @param configId
     * @param payFee
     * @return
     */
    @RequestMapping(value = "/pay/doOnline", method =  {RequestMethod.POST, RequestMethod.GET})
    public Result doOnline(String channelId, String type, String userId, String configId, String payFee, String token) {
        if (StrUtil.isBlank(token)) {
            return Result.error(1, "未获取到tonken信息!");
        }

        if (StrUtil.isBlank(channelId) || StrUtil.isBlank(type) || StrUtil.isBlank(userId) || StrUtil.isBlank(configId)) {
            return Result.error(1, "请求参数错误!");
        }

        //创建订单信息
        PayOrderDto payOrderDto = new PayOrderDto();
        payOrderDto.setChannelId(channelId);
        payOrderDto.setConfigId(configId);
        payOrderDto.setPayfee(payFee);

        payOrderDto.setUserId(userId);
        R<PayResult> order = remotePayOrderService.createPayOrder(payOrderDto, SecurityConstants.FROM_IN);
        if (order == null || order.getData() == null) {
            return null;
        }

        PayResult payResult = new PayResult();
        String payUrl = "";
        if (order.getData() instanceof PayResult) {
            payResult = order.getData();
            payUrl = payResult.getPayUrl();
        } else {
            Map<String, Object> map  = (Map<String, Object>) order.getData();
            payUrl = map.get("payUrl").toString();
            return  Result.ok("调用在线支付成功!").put("payUrl", payUrl);
        }

        return Result.error("调用在线支付失败!");
    }


    /**
     * <B>获取渠道版本及更新地址信息</B>
     * @param channelId
     * @param version
     * @return
     */
    @RequestMapping(value = "/version/doConfig", method =  {RequestMethod.POST, RequestMethod.GET} , produces = "application/json;charset=utf-8")
    public Result gameConfig(String channelId, String version, String token) {
        if (StrUtil.isBlank(token)) {
            return Result.error(1, "未获取到tonken信息!");
        }

        if (StrUtil.isBlank(channelId)) {
            return Result.error(1, "请求参数错误!");
        }

        R<List<SysVersionConfig>> r = remoteVersionConfigService.queryVersionConfig(new VersionConfigDto(channelId, version), SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return Result.error(2, "获取信息失败!");
        }

        List<SysVersionConfig> list = r.getData();
        if (CollUtil.isNotEmpty(list) && list.size() > 0) {
            List<Map<String, Object>> resultList = packVersionConfig(list);
            return  Result.ok("获取版本配置信息成功").put("data", resultList);
        } else {
            return  Result.ok("获取版本配置信息成功");
        }
    }


    @RequestMapping(value = "/version/config", method =  {RequestMethod.POST, RequestMethod.GET} )
    public Result gameJsonConfig(@RequestBody VersionConfigJson versionConfig) {
        if (StrUtil.isBlank(versionConfig.getChannelId())) {
            return Result.error(1, "请求参数错误!");
        }

        R<List<SysVersionConfig>> r = remoteVersionConfigService.queryVersionConfig(new VersionConfigDto(versionConfig.getChannelId(), versionConfig.getVersion()), SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return Result.error(2, "获取信息失败!");
        }

        List<SysVersionConfig> list = r.getData();
        if (CollUtil.isNotEmpty(list) && list.size() > 0) {
            List<Map<String, Object>> resultList = packVersionConfig(list);
            return  Result.ok("获取版本配置信息成功").put("data", resultList);
        }
        return null;
    }


    private List<Map<String, Object>> packVersionConfig(List<SysVersionConfig> list) {
        List<Map<String, Object>> resultList = new ArrayList<>();

        for (SysVersionConfig config : list) {
            Map<String, Object> resultMap = new HashMap<String, Object>();
            resultMap.put("return_clear", config.getIsClear());
            resultMap.put("return_info", config.getUpdateUrl());

            resultList.add(resultMap);
        }

        return resultList;
    }


    /**
     * <B> 接口说明</B>
     * 获取阿里云的配置信息
     *
     * @param type
     * @param userId
     */
    @RequestMapping(value = "oos/doConfig", method ={RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    public Result ossConfig(String type, String userId) {
        if (StrUtil.isBlank(type)  || StrUtil.isBlank(userId)) {
            return Result.error(1, "请求参数错误!");
        }

        //用这个方法
        R<CloudStorageConfig> r = remoteOssService.getOssConfigInfo( SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return Result.error(2, "获取配置信息失败!");
        }

        Map<String, Object> resultMap = new HashMap<>();
        if (r.getData() instanceof Map) {
            Map<String, Object> sourceMap  = (Map<String, Object>) r.getData();
            packOss(sourceMap, resultMap);
            return Result.ok("获取配置信息成功!").put("data", resultMap).put("userId",userId).put("type", type);
        } else {
            return Result.error(2, "获取配置信息失败!");
        }
    }


    @RequestMapping(value = "oos/config", method ={RequestMethod.POST, RequestMethod.GET})
    public Result ossJsonConfig(@RequestBody OssJson oss) {
        if (StrUtil.isBlank(oss.getType())  || StrUtil.isBlank(oss.getUserId())) {
            return Result.error(1, "请求参数错误!");
        }

        //用这个方法
        R<CloudStorageConfig> r = remoteOssService.getOssConfigInfo( SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return Result.error(2, "获取配置信息失败!");
        }

        Map<String, Object> resultMap = new HashMap<>();
        if (r.getData() instanceof Map) {
            Map<String, Object> sourceMap  = (Map<String, Object>) r.getData();
            packOss(sourceMap, resultMap);
            return Result.ok("获取配置信息成功!").put("data", resultMap).put("userId", oss.getUserId()).put("type", oss.getType());
        } else {
            return Result.error(2, "获取配置信息失败!");
        }
    }

    private void packOss(Map<String, Object> sourceMap, Map<String, Object> resultMap){
        resultMap.put("aliyunDomain", ObjectUtil.isNotNull(sourceMap.get("aliyunDomain")) ? sourceMap.get("aliyunDomain").toString() : "");
        resultMap.put("aliyunPrefix", ObjectUtil.isNotNull(sourceMap.get("aliyunPrefix")) ? sourceMap.get("aliyunPrefix").toString() : "");
        resultMap.put("aliyunEndPoint", ObjectUtil.isNotNull(sourceMap.get("aliyunEndPoint")) ? sourceMap.get("aliyunEndPoint").toString() : "");

        resultMap.put("aliyunAccessKeyId", ObjectUtil.isNotNull(sourceMap.get("aliyunAccessKeyId")) ? sourceMap.get("aliyunAccessKeyId").toString() : "");
        resultMap.put("aliyunAccessKeySecret", ObjectUtil.isNotNull(sourceMap.get("aliyunAccessKeySecret")) ? sourceMap.get("aliyunAccessKeySecret").toString() : "");
        resultMap.put("aliyunBucketName", ObjectUtil.isNotNull(sourceMap.get("aliyunBucketName")) ? sourceMap.get("aliyunBucketName").toString() : "");
    }


    /**
     * <B>接口说明</B>
     * 获取IP地址信息
     * ip：IP地址信息
     * type：类型:sina,taobao
     * sessId : 附加信息，本地不用，原路径传回
     * @param ip
     * @param type
     * @param sessId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/ipaddress/doInfo", method =  {RequestMethod.POST,RequestMethod.GET}, produces = "application/json;charset=utf-8")
    public Result getIpAddress(String ip, String type, String sessId) {
        if (StrUtil.isBlank(ip)  || StrUtil.isBlank(type) || StrUtil.isBlank(sessId)) {
            return Result.error(1, "请求参数错误!");
        }

        //判断参数不符合要求
        if (!type.equals("taobao") && !type.equals("126")) {
            return Result.error(1, "请求参数错误!");
        }

        //判断是否是IP4
        if (!Validator.isIpv4(ip)) {
            return Result.error(1, "请求参数错误!");
        }

        //调用接口查询 IP信息
        String str = HttpHandler.getIpaddress(ip, type);
        GsonIpAddress ipAddress = new Gson().fromJson(str.trim(), GsonIpAddress.class);
        return Result.ok("获取IP地址信息成功!").put("sessId", sessId).put("data", ipAddress.getData());
    }


    @RequestMapping(value = "/ipaddress/info", method =  {RequestMethod.POST,RequestMethod.GET})
    public Result getJsonIpAddress(@RequestBody IpJson ipJson) {
        if (StrUtil.isBlank(ipJson.getIp())  || StrUtil.isBlank(ipJson.getType()) || StrUtil.isBlank(ipJson.getSessId())) {
            return Result.error(1, "请求参数错误!");
        }

        //判断参数不符合要求
        if (!ipJson.getType().equals("taobao") && !ipJson.getType().equals("126")) {
            return Result.error(1, "请求参数错误!");
        }

        //判断是否是IP4
        if (!Validator.isIpv4(ipJson.getIp())) {
            return Result.error(1, "请求参数错误!");
        }

        //调用接口查询 IP信息
        String str = HttpHandler.getIpaddress(ipJson.getIp(), ipJson.getType());
        GsonIpAddress ipAddress = new Gson().fromJson(str.trim(), GsonIpAddress.class);
        return Result.ok("获取IP地址信息成功!").put("sessId", ipJson.getSessId()).put("data", ipAddress.getData());
    }


    /**
     * <B>创建生成二维码信息</B>
     * 创建生成二维码信息
     * accountID : 用户ID
     * clientParam : 参数信息
     *
     * @param url
     * @return
     */
    @RequestMapping(value = "/qrcode/doCreate", method = {RequestMethod.POST,RequestMethod.GET})
    public Result createQRCode(String url) {
        if (StrUtil.isBlank(url) ) {
            return Result.error(1, "请求参数错误.");
        }

        //判断是否是URL
        if (!Validator.isUrl(url)) {
            return Result.error(1, "请求参数错误.");
        }

        R<String> r = remoteOssService.createQrcode(new QrCodeDto(0, 0, 0, url, ""), SecurityConstants.FROM_IN);
        //生成 URL信息并且上传
        if (r == null || r.getData() == null) {
            return Result.error(1, "生成二维码信息失败.");
        }

        String orCodeUrl = r.getData();
        return Result.ok("创建二维码成功").put("orCodeUrl", orCodeUrl);
    }


    /**
     * <B>创建生成二维码信息</B>
     * 创建生成二维码信息
     * accountID : 用户ID
     * clientParam : 参数信息
     *
     * @return
     */
    @RequestMapping(value = "/qrcode/create", method = {RequestMethod.POST,RequestMethod.GET})
    public Result createJsonQRCode(@RequestBody QrCodeJson qrCodeJson) {
        if (StrUtil.isBlank(qrCodeJson.getUrl()) ) {
            return Result.error(1, "请求参数错误!");
        }

        //判断是否是URL
        if (!Validator.isUrl(qrCodeJson.getUrl())) {
            return Result.error(1, "请求参数错误!");
        }

        R<String> r = remoteOssService.createQrcode(new QrCodeDto(0, 0, 0, qrCodeJson.getUrl(), ""), SecurityConstants.FROM_IN);
        //生成 URL信息并且上传
        if (r == null || r.getData() == null) {
            return Result.error(1, "生成二维码信息失败!");
        }

        String orCodeUrl = r.getData();
        return Result.ok("创建二维码成功").put("orCodeUrl", orCodeUrl);
    }


    @RequestMapping(value = "/defineqrcode/doCreate", method = {RequestMethod.POST,RequestMethod.GET}, produces = "application/json;charset=utf-8")
    public Result createDefineQRCode(String url, String width, String height,  String margin, String logoUrl) {

        if (StrUtil.isBlank(url) || StrUtil.isBlank(logoUrl) ) {
            return Result.error(1, "请求参数错误!");
        }

        //判断是否是URL
        if (!Validator.isUrl(url) && !Validator.isUrl(logoUrl)) {
            return Result.error(1, "请求参数错误!");
        }

        //判断长宽是否为数据
        if (StrUtil.isNotBlank(width) && !Validator.isNumber(width)) {
            return Result.error(1, "请求参数错误!");
        }

        if (StrUtil.isNotBlank(height) && !Validator.isNumber(height)) {
            return Result.error(1, "请求参数错误!");
        }

        Integer widthRs  = StrUtil.isNotBlank(width) ? Integer.parseInt(width) : 0;
        Integer heightRs  = StrUtil.isNotBlank(height) ? Integer.parseInt(height) : 0;
        Integer marginRs  = StrUtil.isNotBlank(margin) ? Integer.parseInt(margin) : 0;

        R<String> r = remoteOssService.createDefineQrcode(new QrCodeDto(widthRs, heightRs, marginRs, url, logoUrl), SecurityConstants.FROM_IN);
        //生成 URL信息并且上传
        if (r == null || r.getData() == null) {
            return Result.error(1, "生成二维码信息失败!");
        }

        String orCodeUrl = r.getData();
        return Result.ok("创建二维码成功").put("orCodeUrl", orCodeUrl);
    }


    @RequestMapping(value = "/defineqrcode/create", method = {RequestMethod.POST,RequestMethod.GET}, produces = "application/json;charset=utf-8")
    public Result createDefineQRCode(@RequestBody QrCodeJson qrCodeJson) {

        if (StrUtil.isBlank(qrCodeJson.getUrl()) || StrUtil.isBlank(qrCodeJson.getLogoUr()) ) {
            return Result.error(1, "请求参数错误!");
        }

        //判断是否是URL
        if (StrUtil.isBlank(qrCodeJson.getUrl()) || StrUtil.isBlank(qrCodeJson.getLogoUr()) ) {
            return Result.error(1, "请求参数错误!");
        }

        //判断长宽是否为数据
        if (StrUtil.isNotBlank(qrCodeJson.getWidth()) && !Validator.isNumber(qrCodeJson.getWidth())) {
            return Result.error(1, "请求参数错误!");
        }

        if (StrUtil.isNotBlank(qrCodeJson.getHeight()) && !Validator.isNumber(qrCodeJson.getHeight())) {
            return Result.error(1, "请求参数错误!");
        }

        Integer widthRs  = StrUtil.isNotBlank(qrCodeJson.getWidth()) ? Integer.parseInt(qrCodeJson.getWidth()) : 0;
        Integer heightRs  = StrUtil.isNotBlank(qrCodeJson.getHeight()) ? Integer.parseInt(qrCodeJson.getHeight()) : 0;
        Integer marginRs  = StrUtil.isNotBlank(qrCodeJson.getMargin()) ? Integer.parseInt(qrCodeJson.getMargin()) : 0;

        R<String> r = remoteOssService.createDefineQrcode(new QrCodeDto(widthRs, heightRs, marginRs, qrCodeJson.getUrl(), qrCodeJson.getLogoUr()), SecurityConstants.FROM_IN);
        //生成 URL信息并且上传
        if (r == null || r.getData() == null) {
            return Result.error(1, "生成二维码信息失败!");
        }

        String orCodeUrl = r.getData();
        return Result.ok("创建二维码成功").put("orCodeUrl", orCodeUrl);
    }


    /**
     * <B>功能说明</B>
     * 金币兑换,用户提现申请
     * @param accountId      玩家ID
     * @param exchangeMoney  兑换金额（人民币，这里单位是分，换算成圆，要除以100
     * @param type           支付类型 1-支付宝 2-银行卡 3微信
     * @param realName       如果只支付宝这里就是支付宝绑定姓名， 如果是银行卡这里就是绑定的真实姓名
     * @param payId          如果只支付宝这里就是支付宝账号， 如果是银行卡这里就是银行卡卡号
     * @return
     */
    @RequestMapping(value = "/goldcoin/doCash", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json;charset=utf-8")
    public Result goldCoinCash(String accountId, String exchangeMoney,  String type, String realName, String payId,
                               String bank, String subBank, String province, String city) {
        if (StrUtil.isBlank(accountId) || StrUtil.isBlank(exchangeMoney) ||  StrUtil.isBlank(type) || StrUtil.isBlank(realName) || StrUtil.isBlank(payId)) {
            return Result.error(1, "请求参数错误!");
        }

        String address = "";
        //判断是银行卡
        if (type.equals("2")) {
            if (StrUtil.isBlank(bank) || StrUtil.isBlank(subBank) || StrUtil.isBlank(city) || StrUtil.isBlank(province)) {
                return Result.error(1, "请求参数错误!");
            } else {
                address = province + city;
            }
        }

        R<SysCashOrder> r = remoteCashOrderService.createCashOrder(new CashOrderDto(accountId, exchangeMoney, type, realName, payId), SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return Result.error(2, "申请提现失败!");
        }

        Map<String, Object> resultMap = new HashMap<>();
        if (r.getData() instanceof Map) {
            Map<String, Object> sourceMap = (Map<String, Object>) r.getData();
            packCash(sourceMap, resultMap);
        }
        return Result.ok("申请提现成功!").put("data", resultMap);
    }


    @RequestMapping(value = "/goldcoin/cash", method = {RequestMethod.POST, RequestMethod.GET})
    public Result goldCoinJsonCash(@RequestBody CashJson cashJson) {
        if (StrUtil.isBlank(cashJson.getAccountId()) || StrUtil.isBlank(cashJson.getExchangeMoney()) ||  StrUtil.isBlank(cashJson.getType()) || StrUtil.isBlank(cashJson.getRealName()) || StrUtil.isBlank(cashJson.getPayId())) {
            return Result.error(1, "请求参数错误!");
        }

        String address = "";
        //判断是银行卡
        if (cashJson.getType().equals("2")) {
            if (StrUtil.isBlank(cashJson.getBank()) || StrUtil.isBlank(cashJson.getSubBank()) || StrUtil.isBlank(cashJson.getCity()) || StrUtil.isBlank(cashJson.getProvince())) {
                return Result.error(1, "请求参数错误!");
            } else {
                address = cashJson.getProvince() + cashJson.getCity();
            }
        }

        R<SysCashOrder> r = remoteCashOrderService.createCashOrder(new CashOrderDto(cashJson.getAccountId(), cashJson.getExchangeMoney(), cashJson.getType(),
                cashJson.getRealName(), cashJson.getPayId(), cashJson.getBank(), cashJson.getSubBank(), address),  SecurityConstants.FROM_IN);
        if (r == null || r.getData() == null) {
            return Result.error(2, "申请提现失败!");
        }

        Map<String, Object> resultMap = new HashMap<>();
        Integer accountID = 0;
        if (r.getData() instanceof Map) {
            Map<String, Object> sourceMap = (Map<String, Object>) r.getData();
            accountID =  ObjectUtil.isNotNull(sourceMap.get("userId")) ? (Integer)sourceMap.get("userId") : Integer.parseInt(cashJson.getAccountId());
            packCash(sourceMap, resultMap);
        }
        return Result.ok("申请提现成功!").put("data", resultMap).put("accountID", accountID);
    }


    private void packCash(Map<String, Object> sourceMap, Map<String, Object> resultMap){
        resultMap.put("cashNo", ObjectUtil.isNotNull(sourceMap.get("cashNo")) ? sourceMap.get("cashNo").toString() : "");
        resultMap.put("cashFee", ObjectUtil.isNotNull(sourceMap.get("cashFee")) ? sourceMap.get("cashFee").toString() : "");

        resultMap.put("channelId", ObjectUtil.isNotNull(sourceMap.get("channelId")) ? sourceMap.get("channelId").toString() : "");
        resultMap.put("account", ObjectUtil.isNotNull(sourceMap.get("account")) ? sourceMap.get("account").toString() : "");
        resultMap.put("accountName", ObjectUtil.isNotNull(sourceMap.get("accountName")) ? sourceMap.get("accountName").toString() : "");

        resultMap.put("cashType", ObjectUtil.isNotNull(sourceMap.get("cashType")) ? sourceMap.get("cashType").toString() : "");
    }


    /**
     * <B>获取微信config信息</B>
     * @param channelId
     * @return
     */
    @RequestMapping(value = "/social/config", method =  {RequestMethod.POST, RequestMethod.GET})
    public Result configSocial(String channelId) {
        if (StrUtil.isBlank(channelId)) {
            return Result.error(1, "请求参数错误!");
        }

        R<SocialConfig> config = remoteSocialConfigService.getSocialConfigById(channelId, SecurityConstants.FROM_IN);
        if (config == null || config.getData() == null) {
            return Result.error(2, "获取第三方配置信息失败!");
        }
        return Result.ok("获取微信配置信息成功!").put("data", config.getData());
    }


    /**
     * <B>根据信息获取第三方微信数据</B>
     * @param code
     * @param channelId
     * @return
     */
    @RequestMapping(value = "/social/tonken", method =  {RequestMethod.POST, RequestMethod.GET})
    public Result tonkenSocial(String code, String channelId) {
        if (StrUtil.isBlank(channelId) || StrUtil.isBlank(code)) {
            return Result.error(1, "请求参数错误!");
        }

        R<SocialConfig> config = remoteSocialConfigService.getSocialConfigById(channelId, SecurityConstants.FROM_IN);
        if (config == null || config.getData() == null) {
            return Result.error(2, "获取第三方配置信息失败!");
        }

        String appId = "";
        String appSecret = "";

        if (config.getData() instanceof  Map) {
            Map<String, Object> map  = (Map<String, Object>) config.getData();
            appId = (String)map.get("appId");
            appSecret = (String)map.get("appSecret");
        } else {
            SocialConfig socialConfig = config.getData();
            appId = socialConfig.getAppId();
            appSecret = socialConfig.getAppSecret();
        }

        GsonWechatToken token = HttpHandler.getWechatToken(appId, appSecret, code);
        return Result.ok("获取TONKEN成功!").put("data", token);
    }


    /**
     * <B>获取微信用户信息</B>
     * @param wechatJson
     * @return
     */
    @RequestMapping(value = "/social/userinfo", method =  {RequestMethod.POST, RequestMethod.GET})
    public Result userInfoSocial(@RequestBody WechatJson wechatJson) {
        if (StrUtil.isBlank(wechatJson.getAuthID()) || StrUtil.isBlank(wechatJson.getAuthToken())) {
            return Result.error(1, "请求参数错误!");
        }

        GsonWechatUserInfo userInfo = HttpHandler.getWechatUserInfo(wechatJson.getAuthToken(), wechatJson.getAuthID()) ;
        if (userInfo != null && StrUtil.isNotBlank(userInfo.getNickname())) {
            return Result.ok("获取微信用户信息成功!").put("data", userInfo);
        } else {
            return Result.error(2,"获取微信用户信息失败!");
        }
    }
}
