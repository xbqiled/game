package com.youwan.game.api.controller;

import com.youwan.game.cac.api.dto.BettingRecordQueryDto;
import com.youwan.game.cac.api.feign.RemoteBettingRecordQuery;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.util.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-28.
 */
@RestController
@Slf4j
@RequestMapping("/itf/bettingRecord")
public class BettingRecordApiController {

    private final RemoteBettingRecordQuery remoteBettingRecordQuery;

    public BettingRecordApiController(RemoteBettingRecordQuery remoteBettingRecordQuery) {
        this.remoteBettingRecordQuery = remoteBettingRecordQuery;
    }

    @PostMapping("/lotteries/{userId}")
    public R queryLotteryRecord(@PathVariable("userId") Integer userId,
                                @RequestBody BettingRecordQueryDto queryDto) {
        return remoteBettingRecordQuery.queryLotteryRecord(userId, queryDto, SecurityConstants.FROM_IN);
    }

    @PostMapping("/egame/{userId}")
    public R queryEGameRecord(@PathVariable("userId") Integer userId,
                              @RequestBody BettingRecordQueryDto queryDto) {
        return remoteBettingRecordQuery.queryEGameRecord(userId, queryDto, SecurityConstants.FROM_IN);
    }

    @PostMapping("/live/{userId}")
    public R queryLiveRecord(@PathVariable("userId") Integer userId,
                             @RequestBody BettingRecordQueryDto queryDto) {
        return remoteBettingRecordQuery.queryLiveRecord(userId, queryDto, SecurityConstants.FROM_IN);
    }

    @PostMapping("/3rdsport/{userId}")
    public R queryTPSportRecord(@PathVariable("userId") Integer userId,
                                @RequestBody BettingRecordQueryDto queryDto) {
        return remoteBettingRecordQuery.queryTPSportRecord(userId, queryDto, SecurityConstants.FROM_IN);
    }

    @PostMapping("/crownsport/{userId}")
    public R queryCrownSportRecord(@PathVariable("userId") Integer userId,
                                   @RequestBody BettingRecordQueryDto queryDto) {
        return remoteBettingRecordQuery.queryCrownSportRecord(userId, queryDto, SecurityConstants.FROM_IN);
    }
}
