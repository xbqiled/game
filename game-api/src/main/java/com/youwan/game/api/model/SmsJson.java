package com.youwan.game.api.model;


import lombok.Data;

import java.io.Serializable;


@Data
public class SmsJson implements Serializable {

    private String channelId;

    private String phone;

    private String templateType;

    private String [] parameter;

}
