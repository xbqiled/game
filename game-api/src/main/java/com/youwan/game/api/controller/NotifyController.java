package com.youwan.game.api.controller;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.NotifyPayOrderDto;
import com.youwan.game.cac.api.feign.RemotePayOrderService;
import com.youwan.game.cac.api.vo.NotifyOrderVo;
import com.youwan.game.cac.api.vo.PayConfigVo;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.constant.UrlConstant;
import com.youwan.game.common.core.util.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.Base64;

import static com.youwan.game.common.core.constant.UrlConstant.FEONT_SYSTM_URL;


@Controller
@Slf4j
@AllArgsConstructor
@RequestMapping("/notify")
public class NotifyController {

    private final RemotePayOrderService remotePayOrderService;

    public SortedMap<String, String> getRequestData(HttpServletRequest request) {
        Enumeration<String> enums = request.getParameterNames();
        SortedMap<String, String> map = new TreeMap<>();

        while (enums.hasMoreElements()) {
            String paramName = enums.nextElement();
            String paramValue = request.getParameter(paramName);
            map.put(paramName, paramValue);
        }
        return map;
    }


    public SortedMap<String, Object> getRequestJsonData(HttpServletRequest request) {
        try {
            byte buffer[] = getRequestPostBytes(request);
            String charEncoding = request.getCharacterEncoding();
            if (charEncoding == null) {
                charEncoding = "UTF-8";
            }

            String data = new String(buffer, charEncoding);
            SortedMap<String, Object> map = new TreeMap<String, Object>();
            map = com.alibaba.fastjson.JSONObject.parseObject(data, map.getClass());

            return map;
        } catch (IOException e) {
            return null;
        }
    }


    public SortedMap<String, String> getStrRequestJsonData(HttpServletRequest request) {
        try {
            byte buffer[] = getRequestPostBytes(request);
            String charEncoding = request.getCharacterEncoding();
            if (charEncoding == null) {
                charEncoding = "UTF-8";
            }

            String data = new String(buffer, charEncoding);
            SortedMap<String, String> map = new TreeMap<>();
            map = com.alibaba.fastjson.JSONObject.parseObject(data, map.getClass());

            return map;
        } catch (IOException e) {
            return null;
        }
    }


    private static byte[] getRequestPostBytes(HttpServletRequest request) throws IOException {
        int contentLength = request.getContentLength();
        if (contentLength < 0) {
            return null;
        }
        byte buffer[] = new byte[contentLength];
        for (int i = 0; i < contentLength;) {

            int readlen = request.getInputStream().read(buffer, i, contentLength - i);
            if (readlen == -1) {
                break;
            }
            i += readlen;
        }
        return buffer;
    }


    public SortedMap<String, Object> getRequestObjData(HttpServletRequest request) {
        Enumeration<String> enums = request.getParameterNames();
        SortedMap<String, Object> map = new TreeMap<>();

        while (enums.hasMoreElements()) {
            String paramName = enums.nextElement();
            String paramValue = request.getParameter(paramName);
            map.put(paramName, paramValue);
        }
        return map;
    }


    @ResponseBody
    @RequestMapping(value = "/juBaoFu", method = {RequestMethod.POST, RequestMethod.GET})
    public String juBaoFuNotify(HttpServletRequest request, HttpServletResponse response) {
        String msg = "";
        try{
            SortedMap<String, String> map = getRequestData(request);
            log.warn("聚宝付支付，成功收到回调通知-->{}",map.toString());
            String orderNo = map.get("customerbillno");

            //获取秘钥信息
            R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
            if (key == null || key.getData() == null || key.getData().getStatus() == 1) {
                throw new Exception("聚宝付支付订单号不存在:"+orderNo);
            }
            if(map.containsKey("customerextinfo") && "".equalsIgnoreCase(map.get("customerextinfo"))){
                map.remove("customerextinfo");
            }
            String sign = map.get("sign");
            map.remove("sign");
            String str = SignUtil.formatBizQueryParaMap(map,false)+"&key="+key.getData().getMerchantKey();
            String newSign = SignUtil.encryption(str);
            //判断签名是否正确
            if(!newSign.equalsIgnoreCase(sign)){
                throw new Exception("聚宝付-回调过程签名验证错误");
            }
            if (!"SUCCESS".equals(map.get("paystatus"))) {
                throw new Exception("聚宝付-订单"+orderNo+"支付失败或超时!");
            }
            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(orderNo);
            order.setOrderStatus("1");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                throw new Exception("聚宝付支付更新订单信息失败:"+orderNo);
            } else {
                String resultStr = r.getData();
                if (resultStr.equals("0")) {
                    msg = "OK";
                } else {
                    throw new Exception("聚宝付支付订单处理失败:"+orderNo);
                }
            }
        }catch (Exception e){
            log.error(e.getMessage());
            msg = "FAIL";
        }
        return msg;
    }

    @ResponseBody
    @RequestMapping(value = "/jinZun", method = {RequestMethod.POST, RequestMethod.GET})
    public String jinZunNotify(HttpServletRequest request, HttpServletResponse response){
        String msg = "";
        try{
            SortedMap<String, String> map = getRequestData(request);
            log.warn("金樽支付，成功收到回调通知-->{}", map.toString());
            String orderNo = map.get("outTradeNo");

            //获取秘钥信息
            R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
            if (key == null || key.getData() == null || key.getData().getStatus() == 1) {
                throw new Exception("金樽支付订单号不存在:"+orderNo);
            }
            String sign = map.get("sign");
            map.remove("sign");
            map.put("secret",key.getData().getMerchantKey());
            String newSign = SignUtil.encryption(SignUtil.formatBizQueryParaMap(map,false));
            //判断签名是否正确
            if(!newSign.equalsIgnoreCase(sign)){
                throw new Exception("金樽支付-回调过程签名验证错误");
            }
            if (!"2".equals(map.get("orderState"))) {
                throw new Exception("金樽支付-订单"+orderNo+"支付失败或超时!");
            }
            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(orderNo);
            order.setOrderStatus("1");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                throw new Exception("金樽支付更新订单信息失败:"+orderNo);
            } else {
                String resultStr = r.getData();
                if (resultStr.equals("0")) {
                    msg = "SUCCESS";
                } else {
                    throw new Exception("金樽支付订单处理失败:"+orderNo);
                }
            }
        }catch (Exception e){
            log.error(e.getMessage());
            msg = "FAIL";
        }
        return msg;
    }

    @ResponseBody
    @RequestMapping(value = "/liBao", method = {RequestMethod.POST, RequestMethod.GET})
    public String liBaoNotify(HttpServletRequest request) {
        String msg = "";
        try{
            SortedMap<String, String> map = getRequestData(request);
            log.warn("丽宝支付，成功收到回调通知-->{}",map.toString());
            String orderNo = map.get("orderid");

            //获取秘钥信息
            R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
            if (key == null || key.getData() == null || key.getData().getStatus() == 1) {
                throw new Exception("丽宝支付订单号不存在:"+orderNo);
            }

            String sign = map.get("sign");
            map.remove("sign");
            if(map.containsKey("attach") && "".equalsIgnoreCase(map.get("attach"))){
                map.remove("attach");
            }
            String str = SignUtil.formatBizQueryParaMap(map,false)+"&key="+key.getData().getMerchantKey();
            String newSign = SignUtil.encryption(str).toUpperCase();
            //判断签名是否正确
            if(!newSign.equalsIgnoreCase(sign)){
                throw new Exception("丽宝支付-回调过程签名验证错误");
            }
            if (!"00".equals(map.get("returncode"))) {
                throw new Exception("丽宝支付-订单"+orderNo+"支付失败或超时!");
            }
            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(orderNo);
            order.setOrderStatus("1");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                throw new Exception("丽宝支付支付更新订单信息失败:"+orderNo);
            } else {
                String resultStr = r.getData();
                if (resultStr.equals("0")) {
                    msg = "OK";
                } else {
                    throw new Exception("丽宝支付支付订单处理失败:"+orderNo);
                }
            }
        }catch (Exception e){
            log.error(e.getMessage());
            msg = "FAIL";
        }
        return msg;
    }

    @ResponseBody
    @RequestMapping(value = "/hongYunTong", method = {RequestMethod.POST, RequestMethod.GET})
    public String hongYunTongNotify(HttpServletRequest request){
        String msg = "";
        try{
            SortedMap<String, String> map = getRequestData(request);
            log.warn("鸿运通支付，回调IP-->{}",IPUtils.getIpAddr(request));
            log.warn("鸿运通支付，收到回调通知-->{}", map.toString());
            String orderNo = map.get("MerchantUniqueOrderId");

            //获取秘钥信息
            R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
            if (key == null || key.getData() == null || key.getData().getStatus() == 1) {
                throw new Exception("鸿运通支付,订单号不存在或已处理:"+orderNo);
            }

            String sign = map.get("Sign");
            map.remove("Sign");
            String str = SignUtil.formatBizQueryParaMap(map,false)+key.getData().getMerchantKey();
            String newSign = SignUtil.encryption(str);
            //判断签名是否正确
            if(!newSign.equalsIgnoreCase(sign)){
                throw new Exception("鸿运通支付-回调过程签名验证错误，签名字符串--"+str);
            }
            BigDecimal amount = new BigDecimal(map.get("Amount"));
            if(key.getData().getPayFee().compareTo(amount) != 0){
                throw new Exception("鸿运通支付-订单"+orderNo+"实际支付金额与订单金额不匹配!，订单金额-"+key.getData().getPayFee()+",实际支付-"+amount);
            }
            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(orderNo);
            order.setOrderStatus("1");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                throw new Exception("鸿运通支付更新订单信息失败:"+orderNo);
            } else {
                String resultStr = r.getData();
                if (resultStr.equals("0")) {
                    log.warn("鸿运通支付，订单"+orderNo+"处理成功,订单状态已变更!");
                } else {
                    throw new Exception("鸿运通支付订单处理失败:"+orderNo);
                }
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return "SUCCESS";
    }

    @ResponseBody
    @RequestMapping(value = "/youYiFu", method = {RequestMethod.POST, RequestMethod.GET})
    public String youYiFuNotify(HttpServletRequest request){
        try{
            SortedMap<String, String> map = getRequestData(request);
            log.warn("优易付支付，回调IP-->{}",IPUtils.getIpAddr(request));
            log.warn("优易付支付，收到回调通知-->{}", map.toString());
            String orderNo = map.get("orderNo");
            String payResult = map.get("payResult");

            //获取秘钥信息
            R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
            if (key == null || key.getData() == null || key.getData().getStatus() == 1) {
                throw new Exception("优易付支付,订单号不存在或已处理:"+orderNo);
            }

            String sign = map.get("sign");
            map.remove("sign");
            TreeMap<String, String> treeMap = new TreeMap<>(map);
            String str = SignUtil.mapToUrlString(treeMap);
            String newSign = SignUtil.encryption(str+key.getData().getMerchantKey()).toUpperCase();
            //判断签名是否正确
            if(!newSign.equalsIgnoreCase(sign)){
                throw new Exception("优易付支付-回调过程签名验证错误，签名字符串--"+str);
            }
            BigDecimal amount = new BigDecimal(map.get("amount"));
            if(key.getData().getPayFee().compareTo(amount) != 0){
                throw new Exception("优易付支付-订单"+orderNo+"实际支付金额与订单金额不匹配!，订单金额-"+key.getData().getPayFee()+",实际支付-"+amount);
            }
            if(!"8".equalsIgnoreCase(payResult)){
                throw new Exception("优易付支付，回调通知-订单已取消:"+orderNo);
            }
            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(orderNo);
            order.setOrderStatus("1");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                throw new Exception("优易付支付更新订单信息失败:"+orderNo);
            } else {
                String resultStr = r.getData();
                if (resultStr.equals("0")) {
                    log.warn("优易付支付，订单"+orderNo+"处理成功,订单状态已变更!");
                } else {
                    throw new Exception("优易付支付订单处理失败:"+orderNo);
                }
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return "success";
    }

    @ResponseBody
    @RequestMapping(value = "/duoDuo", method = {RequestMethod.POST, RequestMethod.GET})
    public String duoDuo(HttpServletRequest request){
        try{
            SortedMap<String, String> map = getRequestData(request);
            log.warn("多多支付，回调IP-->{}",IPUtils.getIpAddr(request));
            log.warn("多多支付，收到回调通知-->{}", map.toString());
            String orderNo = map.get("mchOrderNo");
            String status = map.get("status");

            //获取秘钥信息
            R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
            if (key == null || key.getData() == null || key.getData().getStatus() == 1) {
                throw new Exception("多多支付,订单号不存在或已处理:"+orderNo);
            }

            String sign = map.get("sign");
            map.remove("sign");
            TreeMap<String, String> treeMap = new TreeMap<>(map);
            String str = SignUtil.mapToUrlString(treeMap)+"&key="+key.getData().getMerchantKey();
            String newSign = SignUtil.encryption(str).toUpperCase();
            //判断签名是否正确
            if(!newSign.equalsIgnoreCase(sign)){
                throw new Exception("多多支付-回调过程签名验证错误，签名字符串--"+str);
            }

            if(!"2".equalsIgnoreCase(status)){
                throw new Exception("多多支付，回调通知-订单已取消:"+orderNo);
            }
            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(orderNo);
            order.setOrderStatus("1");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                throw new Exception("多多支付，更新订单信息失败:"+orderNo);
            } else {
                String resultStr = r.getData();
                if (resultStr.equals("0")) {
                    log.warn("多多支付，订单"+orderNo+"处理成功,订单状态已变更!");
                } else {
                    throw new Exception("多多支付，订单处理失败:"+orderNo);
                }
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return "success";
    }

    @ResponseBody
    @RequestMapping(value = "/xinTianDi", method = {RequestMethod.POST, RequestMethod.GET})
    public String xinTianDi(HttpServletRequest request) {
        try{
            SortedMap<String, String> map = getRequestData(request);
            log.warn("新天地支付，回调IP-->{}",IPUtils.getIpAddr(request));
            log.warn("新天地支付，收到回调通知-->{}", map.toString());
            String orderNo = map.get("web_order");

            //获取秘钥信息
            R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
            if (key == null || key.getData() == null || key.getData().getStatus() == 1) {
                throw new Exception("新天地支付,订单号不存在或已处理:"+orderNo);
            }

            String reqResultData = "";
            String result = map.get("result");
            try {
                AesUtil aes = new AesUtil(result,key.getData().getMerchantKey(), key.getData().getMerchantKey(),128);
                //解密result
                reqResultData = aes.decryptToString();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(!JSONUtil.isJson(reqResultData)){
                throw new Exception("新天地支付,回调过程签名解密失败:"+orderNo);
            }

            Map paramMap = new Gson().fromJson(reqResultData, Map.class);
            String web_order = (String)paramMap.get("paramMap");
            String status = (String)paramMap.get("status");

            //判断签名是否正确
            if (status.equals("3")) {
                //调用类处理相关信息
                NotifyPayOrderDto notifyPayOrderDto = new NotifyPayOrderDto();
                notifyPayOrderDto.setOrderNo(web_order);
                notifyPayOrderDto.setOrderStatus("1");

                R<String> r = remotePayOrderService.callbackPayOrdery(notifyPayOrderDto, SecurityConstants.FROM_IN);
                if (r == null || r.getData() == null) {
                    throw new Exception("新天地支付更新订单信息失败:"+orderNo);
                } else {
                    String resultStr = r.getData();
                    if (resultStr.equals("0")) {
                        log.warn("新天地支付，订单"+orderNo+"处理成功,订单状态已变更!");
                    } else {
                        throw new Exception("新天地支付支付订单处理失败:"+orderNo);
                    }
                }

            } else {
                throw new Exception("新天地支付，订单已取消，未支付成功:"+orderNo);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return "success";
    }

    @ResponseBody
    @RequestMapping(value = "/yanYi", method = {RequestMethod.POST, RequestMethod.GET})
    public String yanYi(HttpServletRequest request){
        try{
            SortedMap<String, String> map = getRequestData(request);
            log.warn("炎易支付，回调IP-->{}",IPUtils.getIpAddr(request));
            log.warn("炎易支付，收到回调通知-->{}", map.toString());
            String orderNo = map.get("orderid");
            String returncode = map.get("returncode");

            //获取秘钥信息
            R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
            if (key == null || key.getData() == null || key.getData().getStatus() == 1) {
                throw new Exception("炎易支付,订单号不存在或已处理:"+orderNo);
            }

            String sign = map.get("sign");
            map.remove("sign");
            TreeMap<String, String> treeMap = new TreeMap<>(map);
            String str = SignUtil.mapToUrlString(treeMap)+"&key="+key.getData().getMerchantKey();
            String newSign = SignUtil.encryption(str).toUpperCase();
            //判断签名是否正确
            if(!newSign.equalsIgnoreCase(sign)){
                throw new Exception("炎易支付-回调过程签名验证错误，签名字符串--"+str);
            }

            if(!"00".equalsIgnoreCase(returncode)){
                throw new Exception("炎易支付，回调通知-订单已取消:"+orderNo);
            }
            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(orderNo);
            order.setOrderStatus("1");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                throw new Exception("炎易支付，更新订单信息失败:"+orderNo);
            } else {
                String resultStr = r.getData();
                if (resultStr.equals("0")) {
                    log.warn("炎易支付，订单"+orderNo+"处理成功,订单状态已变更!");
                } else {
                    throw new Exception("炎易支付，订单处理失败:"+orderNo);
                }
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return "OK";
    }

    @ResponseBody
    @RequestMapping(value = "/shanRuBao", method = {RequestMethod.POST, RequestMethod.GET})
    public String shanRuBao(HttpServletRequest request){
        try{
            SortedMap<String, String> map = getRequestData(request);
            log.warn("闪入宝支付，回调IP-->{}",IPUtils.getIpAddr(request));
            log.warn("闪入宝支付，收到回调通知-->{}", map.toString());
            String orderNo = map.get("orderid");

            //获取秘钥信息
            R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
            if (key == null || key.getData() == null || key.getData().getStatus() == 1) {
                throw new Exception("闪入宝支付,订单号不存在或已处理:"+orderNo);
            }

            String sign = map.get("key");
            map.remove("key");
            map.put("token",key.getData().getMerchantKey());
            TreeMap<String, String> treeMap = new TreeMap<>(map);
            String str = SignUtil.mapToUrlString(treeMap);
            String newSign = SignUtil.encryption(str).toLowerCase();
            //判断签名是否正确
            if(!newSign.equalsIgnoreCase(sign)){
                throw new Exception("闪入宝支付-回调过程签名验证错误，签名字符串--"+str);
            }
            BigDecimal amount = new BigDecimal(map.get("realprice"));
            if(key.getData().getPayFee().compareTo(amount) != 0){
                throw new Exception("闪入宝支付-订单"+orderNo+"实际支付金额与订单金额不匹配!，订单金额-"+key.getData().getPayFee()+",实际支付-"+amount);
            }
            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(orderNo);
            order.setOrderStatus("1");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                throw new Exception("闪入宝支付，更新订单信息失败:"+orderNo);
            } else {
                String resultStr = r.getData();
                if (resultStr.equals("0")) {
                    log.warn("闪入宝支付，订单"+orderNo+"处理成功,订单状态已变更!");
                } else {
                    throw new Exception("闪入宝支付，订单处理失败:"+orderNo);
                }
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return "success";
    }



    @RequestMapping(value = "/xinXing", method = {RequestMethod.POST, RequestMethod.GET})
    public void xinXingNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, String> map = getRequestData(request);
        log.debug("新星支付，成功收到回调通知，通知信息为:" + map.toString());
        String orderNo = map.get("agentOrderId");

        //获取秘钥信息
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null || key.getData().getStatus() == 1) {
            log.error("新星支付订单号不存在:", orderNo);
            return;
        }

        StringBuffer sb = new StringBuffer();
        sb.append("agentAcct=" + map.get("agentAcct"));
        sb.append("&agentOrderId=" +  map.get("agentOrderId"));
        sb.append("&amount=" +  map.get("amount"));
        sb.append("&payAmount=" +  map.get("payAmount"));
        sb.append("&status=" +  map.get("status"));
        sb.append("&key=" +  key.getData().getMerchantKey());

        String sign = MD5.encryption(sb.toString());
        //判断签名是否正确
        if (sign.equalsIgnoreCase(map.get("sign")) && "S".equals(map.get("status"))) {


            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(orderNo);
            order.setOrderStatus("1");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("新星支付更新订单信息失败:", orderNo);
                return;
            } else {
                String resultStr = r.getData();
                if (resultStr.equals("0")) {
                    response.getWriter().print("SUCCESSS");
                    return;
                } else {
                    response.getWriter().print("fail");
                    return;
                }
            }
        } else {
            log.error("新星支付验证签名错误:", orderNo);
            return;
        }
    }


    @RequestMapping(value = "/hongYun", method = {RequestMethod.POST, RequestMethod.GET})
    public void hongYunNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, String> map = getRequestData(request);
        log.debug("红运支付，成功收到回调通知，通知信息为:" + map.toString());
        String orderNo = map.get("orderid");

        //获取秘钥信息
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null || key.getData().getStatus() == 1) {
            log.error("红运支付订单号不存在:", orderNo);
            return;
        }

        Set<Map.Entry<String, String>> set = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();
        StringBuffer buffer = new StringBuffer();
        while (iterator.hasNext()) {
            Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
            if (!StrUtil.equals("sign", entry.getKey()) && !StrUtil.equals("attach", entry.getKey()) && StrUtil.isNotEmpty(entry.getValue())) {
                buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
        }
        String signSrc = buffer.append("key=").append(key.getData().getMerchantKey()).toString();
        String sign = MD5.encryption(signSrc).toUpperCase();

        //判断签名是否正确
        if (sign.equalsIgnoreCase(map.get("sign")) && "00".equals(map.get("returncode"))) {

            if(key.getData().getPayFee().compareTo(new BigDecimal(map.get("real_amount"))) != 0) {
                response.getWriter().print("订单金额和实际支付金额不一致。");
                log.error("订单金额和实际支付金额不一致:");
                return;
            }

            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(orderNo);
            order.setOrderStatus("1");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("红运支付更新订单信息失败:", orderNo);
                return;
            } else {
                String resultStr = r.getData();
                if (resultStr.equals("0")) {
                    response.getWriter().print("OK");
                    return;
                } else {
                    response.getWriter().print("fail");
                    return;
                }
            }
        } else {
            log.error("红运支付验证签名错误:", orderNo);
            return;
        }
    }


    @RequestMapping(value = "/sanYi", method = {RequestMethod.POST, RequestMethod.GET})
    public void sanYiNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, String> map = getRequestData(request);
        log.debug("三一支付，成功收到回调通知，通知信息为:" + map.toString());
        String orderNo = map.get("orderid").toString();

        //获取秘钥信息
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null || key.getData().getStatus() == 1) {
            log.error("三一支付订单号不存在:", orderNo);
            return;
        }

        StringBuffer sb = new StringBuffer();
        sb.append("amount=" + map.get("amount") + "&");
        sb.append("code=" + map.get("code") + "&");
        sb.append("crateTime=" + map.get("crateTime")  + "&");
        sb.append("orderid=" + map.get("orderid") + "&");
        sb.append("platformOrderid=" + map.get("platformOrderid") + "&");
        sb.append("userid=" + map.get("userid") + "&");
        sb.append("key=" + key.getData().getMerchantKey());
        String sign = MD5.encryption(sb.toString()).toUpperCase();

        //判断签名是否正确
        if (sign.equalsIgnoreCase(map.get("sign").toString()) && "0".equals(map.get("code").toString())) {
            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(orderNo);
            order.setOrderStatus("1");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("三一支付更新订单信息失败:", orderNo);
                return;
            } else {
                String resultStr = r.getData();
                if (resultStr.equals("0")) {
                    response.getWriter().print("OK");
                    return;
                } else {
                    response.getWriter().print("fail");
                    return;
                }
            }
        } else {
            log.error("三一支付验证签名错误:", orderNo);
            return;
        }
    }


    @RequestMapping(value = "/may", method = {RequestMethod.POST, RequestMethod.GET})
    public void mayNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, Object> map = getRequestJsonData(request);
        log.debug("May支付，成功收到回调通知，通知信息为:" + map.toString());
        String orderNo = map.get("mch_number").toString();

        //获取秘钥信息
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null || key.getData().getStatus() == 1) {
            log.error("May支付订单号不存在:", orderNo);
            return;
        }

        Set<Map.Entry<String, Object>> set = map.entrySet();
        Iterator<Map.Entry<String, Object>> iterator = set.iterator();
        StringBuffer buffer = new StringBuffer();
        while (iterator.hasNext()) {
            Map.Entry<java.lang.String, java.lang.Object> entry = (Map.Entry<java.lang.String, java.lang.Object>) iterator.next();
            if (!StrUtil.equals("sign", entry.getKey().toString())) {
                buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
        }
        String signSrc = buffer.append("key=").append(key.getData().getMerchantKey()).toString();
        String sign = MD5.encryption(signSrc).toUpperCase();

        //判断签名是否正确
        if (sign.equalsIgnoreCase(map.get("sign").toString()) && "3".equals(map.get("status").toString())) {
            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(orderNo);
            order.setOrderStatus("1");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("May支付更新订单信息失败:", orderNo);
                return;
            } else {
                String resultStr = r.getData();
                if (resultStr.equals("0")) {
                    JSONObject json = new JSONObject();
                    json.put("code","200");
                    response.getWriter().print(json.toJSONString());
                    return;
                } else {
                    response.getWriter().print("fail");
                    return;
                }
            }
        } else {
            log.error("May支付验证签名错误:", orderNo);
            return;
        }
    }


    @RequestMapping(value = "/yangGuang", method = {RequestMethod.POST, RequestMethod.GET})
    public void yangGuangNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, Object> map = getRequestJsonData(request);
        log.debug("阳光支付，成功收到回调通知，通知信息为:" + map.toString());
        String orderNo = (String)map.get("shop_no");
        //获取秘钥信息
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null || key.getData().getStatus() == 1) {
            log.error("阳光支付订单号不存在:", orderNo);
            return;
        }

        String signSrc = map.get("shopAccountId").toString() + "&" + map.get("status").toString() + "&" + map.get("trade_no").toString() + "&"
                + map.get("shop_no").toString() + "&" + map.get("money").toString() + "&" + map.get("type").toString() + "&" + key.getData().getMerchantKey();

        String sign = MD5.encryption(signSrc).toLowerCase();

        //判断签名是否正确
        if (sign.equalsIgnoreCase((String)map.get("sign")) && "0".equals(map.get("status").toString())) {
            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(orderNo);
            order.setOrderStatus("1");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("阳光支付更新订单信息失败:", orderNo);
                return;
            } else {
                String resultStr = r.getData();
                if (resultStr.equals("0")) {
                    response.getWriter().print("success");
                    return;
                } else {
                    response.getWriter().print("fail");
                    return;
                }
            }
        } else {
            log.error("阳光支付验证签名错误:", orderNo);
            return;
        }
    }


    @RequestMapping(value = "/longCheng", method = {RequestMethod.POST, RequestMethod.GET})
    public void longChengNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, String> map = getRequestData(request);

        log.debug("龙城支付，成功收到回调通知，通知信息为:" + map.toString());
        String postdata = map.get("postdata");
        com.alibaba.fastjson.JSONObject data = com.alibaba.fastjson.JSONObject.parseObject(postdata);
        String orderNo = data.get("sdorderno").toString();

        //获取秘钥信息
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null || key.getData().getStatus() == 1) {
            log.error("龙城支付订单号不存在:", orderNo);
            return;
        }

        String signSrc = String.format("customerid=%s&status=%s&sdpayno=%s&sdorderno=%s&total_fee=%s&paytype=%s&%s",
                data.get("customerid").toString(), data.get("status").toString(), data.get("sdpayno").toString(),
                data.get("sdorderno").toString(), data.get("total_fee").toString(), data.get("paytype").toString(), key.getData().getMerchantKey());

        String sign = MD5.encryption(signSrc).toLowerCase();
        String signStr = data.get("sign").toString();

        //判断签名是否正确
        if (sign.equalsIgnoreCase(signStr) && "1".equals(data.get("status").toString())) {
            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(orderNo);
            order.setOrderStatus("1");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("龙城支付更新订单信息失败:", orderNo);
                return;
            } else {
                String resultStr = r.getData();
                if (resultStr.equals("0")) {
                    response.getWriter().print("success");
                    return;
                } else {
                    response.getWriter().print("fail");
                    return;
                }
            }
        } else {
            log.error("龙城支付验证签名错误:", orderNo);
            return;
        }
    }


    @RequestMapping(value = "/feiFu", method = {RequestMethod.POST, RequestMethod.GET})
    public void feiFuNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, String> map = getRequestData(request);

        log.debug("飞付支付，成功收到回调通知，通知信息为:" + map.toString());
        String orderNo = map.get("out_trade_no");
        //获取秘钥信息
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null || key.getData().getStatus() == 1) {
            log.error("飞付支付订单号不存在:", orderNo);
            return;
        }

        StringBuffer buffer = new StringBuffer();
        Set<Map.Entry<String,String>> set = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
            if (!StrUtil.equals("sign", entry.getKey()) && StrUtil.isNotEmpty(entry.getValue())) {
                buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
        }

        String sginStr = buffer.substring(0, buffer.length() - 1) + key.getData().getMerchantKey();
        String sign = MD5.encryption(sginStr).toUpperCase();

        //判断签名是否正确
        if (sign.equalsIgnoreCase(map.get("sign")) && "SUCCESS".equals(map.get("trade_status"))) {
            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(orderNo);
            order.setOrderStatus(map.get("trade_status").equals("SUCCESS") ? "1" : "2");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("飞付支付更新订单信息失败:", orderNo);
                return;
            } else {
                String resultStr = r.getData();
                if (resultStr.equals("0")) {
                    response.getWriter().print("SUCCESS");
                    return;
                } else {
                    response.getWriter().print("FAIL");
                    return;
                }
            }
        } else {
            log.error("飞付支付验证签名错误:", orderNo);
            return;
        }
    }


    @RequestMapping(value = "/guaiShou", method = {RequestMethod.POST, RequestMethod.GET})
    public void guaiShouNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, String> map = getRequestData(request);

        log.debug("怪兽支付，成功收到回调通知，通知信息为:" + map.toString());
        String orderNo = map.get("order_sn");
        //获取秘钥信息
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null || key.getData().getStatus() == 1) {
            log.error("怪兽支付订单号不存在:", orderNo);
            return;
        }

        StringBuffer signSrc= new StringBuffer();
        signSrc.append("merchant_code=").append(map.get("merchant_code")).append("&");
        signSrc.append("money=").append(map.get("money")).append("&");
        signSrc.append("order_sn=").append(map.get("order_sn")).append("&");
        signSrc.append("payment_method=").append(map.get("payment_method")).append("&");
        signSrc.append("payment_time=").append(map.get("payment_time")).append("&");
        if (StrUtil.isNotBlank(map.get("remark"))) {
            signSrc.append("remark=").append(map.get("remark")).append("&");
        }
        signSrc.append("status=").append(map.get("status")).append("&");
        signSrc.append("submit_money=").append(map.get("submit_money")).append("&");
        signSrc.append("trade_sn=").append(map.get("trade_sn"));

        //平台公钥
        String syceepayPublicKey = key.getData().getPublicKey();
        byte[] publicKey= org.apache.commons.codec.binary.Base64.decodeBase64(syceepayPublicKey);
        boolean result = false;
        String sign = map.get("sign");

        try {
            result = RSACoder.verify(signSrc.toString().getBytes(), publicKey, org.apache.commons.codec.binary.Base64.decodeBase64(sign));
        } catch (Exception e) {
            e.printStackTrace();
        }
        //判断签名是否正确
        if (result && "1".equals(map.get("status").toString())) {
            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(orderNo);
            order.setOrderStatus("1");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("怪兽支付更新订单信息失败:", orderNo);
                return;
            } else {
                String resultStr = r.getData();
                if (resultStr.equals("0")) {
                    response.getWriter().print("SUCCESS");
                    return;
                } else {
                    response.getWriter().print("FAIL");
                    return;
                }
            }
        } else {
            log.error("怪兽支付验证签名错误:", orderNo);
            return;
        }
    }


    @RequestMapping(value = "/dingDing", method = {RequestMethod.POST, RequestMethod.GET})
    public void dingDingNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, String> map = getStrRequestJsonData(request);

        log.debug("钉钉支付，成功收到回调通知，通知信息为:" + map.toString());
        String orderNo = map.get("requestNo");
        String sign = map.get("sign");
        //获取秘钥信息
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null || key.getData().getStatus() == 1) {
            log.error("钉钉支付订单号不存在:", orderNo);
            return;
        }

        StringBuffer buffer = new StringBuffer();
        buffer.append("amount=" +  map.get("amount")).append("&").append("message=" +  map.get("message")).append("&")
                .append("orderNo=" +  map.get("orderNo")).append("&").append("payAmount=" +  map.get("payAmount")).append("&")
                .append("payTime=" +  map.get("payTime")).append("&").append("requestNo=" +  map.get("requestNo")).append("&")
                .append("status=" +  map.get("status")).append("&").append("userId=" +  map.get("userId")).append("&")
                .append("key=" +  key.getData().getMerchantKey());

        String sginStr = buffer.toString();
        String request_sign = MD5.encryption(sginStr).toUpperCase();
//        String request_sign = MerchSdkSign.getSign(signMap, key.getData().getMerchantKey());
        //判断签名是否正确
        if (sign.equalsIgnoreCase(request_sign) && "3".equals(map.get("status").toString())) {
            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(orderNo);
            order.setOrderStatus("1");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("钉钉支付更新订单信息失败:", orderNo);
                return;
            } else {
                String result = r.getData();
                if (result.equals("0")) {
                    response.getWriter().print("000000");
                    return;
                } else {
                    response.getWriter().print("fail");
                    return;
                }
            }
        } else {
            log.error("钉钉支付验证签名错误:", orderNo);
            return;
        }
    }


    @RequestMapping(value = "/shunDa", method = {RequestMethod.POST, RequestMethod.GET})
    public void shunDaNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, String> map = getRequestData(request);

        log.debug("顺达支付，成功收到回调通知，通知信息为:" + map.toString());
        String orderNo = map.get("mchOrderNo");
        //获取秘钥信息
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null) {
            log.error("顺达支付订单号不存在:", orderNo);
            return;
        }

        //验证签名信息
        StringBuffer buffer = new StringBuffer();
        Set<Map.Entry<String,String>> set = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
            if (!StrUtil.equals("sign", entry.getKey()) && !StrUtil.equals("reqAmount", entry.getKey()) && StrUtil.isNotEmpty(entry.getValue())) {
                buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
        }

        String signSrc = buffer.append("key=").append(key.getData().getMerchantKey()).toString();
        String sign = MD5.encryption(signSrc).toUpperCase();

        //判断签名是否正确
        if (map.get("sign").equalsIgnoreCase(sign) || "2".equals(map.get("status"))) {
            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(orderNo);
            order.setOrderStatus("1");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("顺达支付更新订单信息失败:", orderNo);
                return;
            } else {
                String result = r.getData();
                if (result.equals("0")) {
                    response.getWriter().print("success");
                    return;
                } else {
                    response.getWriter().print("fail");
                    return;
                }
            }
        } else {
            log.error("顺达支付验证签名错误:", orderNo);
            return;
        }
    }


    @RequestMapping(value = "/tianDi", method = {RequestMethod.POST, RequestMethod.GET})
    public void tianDiNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, String> map = getRequestData(request);

        log.debug("天地支付，成功收到回调通知，通知信息为:" + map.toString());
        String orderNo = map.get("out_trade_no");
        //获取秘钥信息
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null) {
            log.error("天地支付订单号不存在:", orderNo);
            return;
        }
        String keyStr = key.getData().getMerchantKey();

        //验证签名信息
        StringBuffer bufferSign = new StringBuffer();
        Set<Map.Entry<String, String>> set = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();

        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            if (!StrUtil.equals("sign", entry.getKey()) && StrUtil.isNotEmpty(entry.getValue())) {
                bufferSign.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
        }

        String bufferStr = bufferSign.substring(0, bufferSign.length() - 1) + keyStr;
        String sign = SignUtil.getSign(bufferStr);
        //判断签名是否正确
        if (map.get("sign").equalsIgnoreCase(sign)) {
            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(map.get("orderNo"));
            order.setOrderStatus(map.get("trade_status").equals("TRADE_SUCCESS") ? "1" : "2");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("天地支付更新订单信息失败:", orderNo);
                return;
            } else {
                String result = r.getData();
                if (result.equals("0")) {
                    response.getWriter().print("OK");
                    return;
                } else {
                    response.getWriter().print("fail");
                    return;
                }
            }
        } else {
            log.error("天地支付验证签名错误:", orderNo);
            return;
        }
    }


    /**
     * <B>快联支付</B>
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/kuaiLian", method = {RequestMethod.POST, RequestMethod.GET})
    public void kuaiLianNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, String> map = getRequestData(request);

        log.debug("快联钱包，成功收到回调通知，通知信息为:" + map.toString());
        String orderNo = map.get("orderNo");
        //获取秘钥信息
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null) {
            log.error("快联钱包订单号不存在:", orderNo);
            return;
        }
        String keyStr = key.getData().getMerchantKey();

        //验证签名信息
        StringBuffer bufferSign = new StringBuffer();
        Set<Map.Entry<String, String>> set = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();

        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            if (!StrUtil.equals("sign", entry.getKey()) && StrUtil.isNotEmpty(entry.getValue())) {
                bufferSign.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
        }

        String bufferStr = bufferSign.substring(0, bufferSign.length() - 1) + keyStr;
        String sign = SignUtil.getSign(bufferStr);
        //判断签名是否正确
        if (map.get("sign").equalsIgnoreCase(sign)) {
            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(map.get("orderNo"));
            order.setOrderStatus(map.get("orderStatus").equals("SUCCESS") ? "1" : "2");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("快联钱包更新订单信息失败:", orderNo);
                return;
            } else {
                String result = r.getData();
                if (result.equals("0")) {
                    response.getWriter().print("0000");
                    return;
                } else {
                    response.getWriter().print("0");
                    return;
                }
            }
        } else {
            log.error("快联钱包验证签名错误:", orderNo);
            return;
        }
    }


    /**
     * <B>猫支付</B>
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/mao", method = {RequestMethod.POST, RequestMethod.GET})
    public void maoNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, String> map = getRequestData(request);

        log.debug("猫支付，成功收到回调通知，通知信息为:" + map.toString());
        String orderNo = map.get("orderid");
        //获取秘钥信息
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null) {
            log.error("猫支付订单号不存在:", orderNo);
            return;
        }
        String keyStr = key.getData().getMerchantKey();

        //验证签名信息
        Set<Map.Entry<String, String>> set = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();
        StringBuffer buffer = new StringBuffer();
        while (iterator.hasNext()) {
            Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
            if (!StrUtil.equals("sign", entry.getKey()) && !StrUtil.equals("attach", entry.getKey()) && StrUtil.isNotEmpty(entry.getValue())) {
                buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
        }
        String signSrc = buffer.append("key=").append(keyStr).toString();
        String sign = MD5.encryption(signSrc).toUpperCase();

        //判断签名是否正确
        if (map.get("sign").equalsIgnoreCase(sign)) {
            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(orderNo);
            order.setOrderStatus(map.get("returncode").equals("00") ? "1" : "2");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("猫支付更新订单信息失败:", orderNo);
                return;
            } else {
                String result = r.getData();
                if (result.equals("0")) {
                    response.getWriter().print("OK");
                    return;
                } else {
                    response.getWriter().print("fail");
                    return;
                }
            }
        } else {
            log.error("猫支付验证签名错误:", orderNo);
            return;
        }
    }


    /**
     * <B>牛牛支付</B>
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/niuNiu", method = {RequestMethod.POST, RequestMethod.GET})
    public void niuNiuNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, String> map = getRequestData(request);

        log.debug("牛牛搬运支付，成功收到回调通知，通知信息为:" + map.toString());
        String orderNo = map.get("order_sn");
        //获取秘钥信息
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null) {
            log.error("牛牛搬运支付订单号不存在:", orderNo);
            return;
        }
        String keyStr = key.getData().getMerchantKey();

        //验证签名信息
        StringBuffer buffer = new StringBuffer();
        Set<Map.Entry<String,String>> set = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
            if (!StrUtil.equals("sign", entry.getKey())) {
                buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
        }

        String signSrc = buffer.append("api_secret=").append(keyStr).toString();
        String sign = MD5.encryption(signSrc).toLowerCase();

        //判断签名是否正确
        if (map.get("sign").equalsIgnoreCase(sign)) {
            //调用类处理相关信息
            NotifyPayOrderDto order = new NotifyPayOrderDto();
            order.setOrderNo(orderNo);
            order.setOrderStatus(map.get("pay_state").equals("1") ? "1" : "2");

            R<String> r = remotePayOrderService.callbackPayOrdery(order, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("牛牛搬运支付更新订单信息失败:", orderNo);
                return;
            } else {
                String result = r.getData();
                if (result.equals("0")) {
                    response.getWriter().print("success");
                    return;
                } else {
                    response.getWriter().print("fail");
                    return;
                }
            }
        } else {
            log.error("牛牛搬运支付验证签名错误:", orderNo);
            return;
        }
    }


    @RequestMapping(value = "/jidian", method = {RequestMethod.POST, RequestMethod.GET})
    public void jiDianNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, String> map = getRequestData(request);
        log.debug("极点支付，成功收到回调通知，通知信息为:" + map.toString());

        String orderNo = map.get("out_trade_no");
        //获取秘钥信息
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null) {
            log.error("极点支付订单号不存在:", orderNo);
            return;
        }

        NotifyOrderVo order = key.getData();
        StringBuffer buffer = new StringBuffer();
        Set<Map.Entry<String, String>> set = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();

        while (iterator.hasNext()) {
            Map.Entry<java.lang.String, java.lang.String> entry = iterator.next();
            if (!StrUtil.equals("sign", entry.getKey()) && StrUtil.isNotEmpty(entry.getValue())) {
                buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
        }

        String signSrc = buffer.append("key=").append(order.getMerchantKey()).toString();
        String sign = SignUtil.encryption(signSrc).toUpperCase();

        if (sign.equalsIgnoreCase(map.get("sign")) && "PAID".equalsIgnoreCase(map.get("status"))) {
            NotifyPayOrderDto notifyOrder = new NotifyPayOrderDto();
            notifyOrder.setOrderNo(map.get("orderNo"));
            notifyOrder.setOrderStatus(map.get("status").equals("PAID") ? "1" : "2");

            R<String> r = remotePayOrderService.callbackPayOrdery(notifyOrder, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("极点支付更新订单信息失败:", orderNo);
                return;
            } else {
                String result = r.getData();
                if (result.equals("0")) {
                    response.getWriter().print("SUCCESS");
                    return;
                } else {
                    response.getWriter().print("FAIL");
                    return;
                }
            }
        } else {
            log.error("极点支付验证签名错误:", orderNo);
            return;
        }
    }


    @RequestMapping(value = "/renRen", method = {RequestMethod.POST, RequestMethod.GET})
    public void renRenNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, Object> map = getRequestJsonData(request);
        log.debug("人人支付，成功收到回调通知，通知信息为:" + JSONObject.toJSONString(map));

        String merchantAccount = map.get("MerchantAccount").toString();
        String sign = map.get("Sign").toString();
        String result = map.get("Result").toString();

        //获取秘钥信息
        R<PayConfigVo> key = remotePayOrderService.getPayConfig(merchantAccount, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null) {
            log.error("人人支付配置不存在:", merchantAccount);
            return;
        }

        PayConfigVo order = key.getData();
        String reqResultData = "";
        try {
            //解密result
            reqResultData = RenRenAESUtil.decrypt(order.getPublicKey(), result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map paramMap = new Gson().fromJson(reqResultData, Map.class);
//        Map<String,Object> signMap = new HashMap<>();
//        signMap.put("MerchantOrderNo",paramMap.get("MerchantOrderNo"));
//        signMap.put("OrderState",paramMap.get("OrderState"));
//        signMap.put("OrderPrice",paramMap.get("OrderPrice"));
//        signMap.put("OrderRealPrice",paramMap.get("OrderRealPrice"));
//        int code = ((Double)paramMap.get("Code")).intValue();
//        signMap.put("Code", code);
//        signMap.put("Time",paramMap.get("Time"));
//        signMap.put("Message",paramMap.get("Message"));
//        int orderType = ((Double)paramMap.get("OrderType")).intValue();
//        signMap.put("OrderType",orderType);
//        signMap.put("MerchantFee",paramMap.get("MerchantFee"));
//        signMap.put("CompleteTime",paramMap.get("CompleteTime"));
//        signMap.put("DealTime",paramMap.get("DealTime"));
//        String encryptData = getReqEncryptData(signMap, order.getPublicKey());
        //获取sign
//        String qsign = RenRenMd5Util.md5Sign("MerchantAccount="+ merchantAccount +"&Result="+encryptData+" &Key="+order.getPublicKey());
//        if (sign.equalsIgnoreCase(qsign) && "6".equalsIgnoreCase(map.get("OrderState").toString())) {
        if (paramMap != null &&   ObjectUtil.isNotNull(paramMap.get("MerchantOrderNo")) &&  ObjectUtil.isNotNull(paramMap.get("OrderState"))  || "6".equalsIgnoreCase(paramMap.get("OrderState").toString())) {

            NotifyPayOrderDto notifyOrder = new NotifyPayOrderDto();
            notifyOrder.setOrderNo(paramMap.get("MerchantOrderNo").toString());
            notifyOrder.setOrderStatus(paramMap.get("OrderState").equals("6") ? "1" : "2");

            R<String> r = remotePayOrderService.callbackPayOrdery(notifyOrder, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("人人支付更新订单信息失败:", paramMap.get("MerchantOrderNo"));
                return;
            } else {
                String payResult = r.getData();
                if (payResult.equals("0")) {
                    response.getWriter().print("true");
                    return;
                } else {
                    response.getWriter().print("FAIL");
                    return;
                }
            }
        } else {
            log.error("人人支付验证签名错误:", paramMap.get("MerchantOrderNo"));
            return;
        }
    }


    /**
     * 得到加密业务参数的data
     *
     * @param paramMap
     * @return
     */
    public static String getReqEncryptData(Map<String, Object> paramMap, String key) {
        System.out.println("getReqEncryptData.paramMap to String:" + JSON.toJSONString(paramMap));
        String encryptData = "";
        try {
            encryptData = RenRenAESUtil.encodeHexStr(RenRenAESUtil.encrypt(key, JSON.toJSONString(paramMap)), false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encryptData;
    }


    @RequestMapping(value = "/beiYang", method = {RequestMethod.POST, RequestMethod.GET})
    public void beiYangNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, Object> map = getRequestJsonData(request);
        log.debug("北洋支付，成功收到回调通知，通知信息为:" + JSONObject.toJSONString(map));

        String orderNo = map.get("order_id").toString();
        //获取秘钥信息
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null) {
            log.error("北洋支付订单号不存在:", orderNo);
            return;
        }

        NotifyOrderVo order = key.getData();


        String signSrc = String.format("order_id=%s&amount=%s&verified_time=%s",
                map.get("order_id").toString(), map.get("amount").toString(), map.get("verified_time").toString());

        byte[] resultByte = null;
        String HMAC_SHA1_ALGORITHM = "HmacSHA1"; SecretKeySpec signingKey = new SecretKeySpec(order.getMerchantKey().getBytes(),HMAC_SHA1_ALGORITHM);
        Mac mac = null;
        try {
            mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
            mac.init(signingKey);
        } catch (InvalidKeyException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] rawHmac = mac.doFinal(signSrc.getBytes());
        resultByte = Base64Utils.encode(rawHmac);
        String sign = new String(resultByte);

        if (sign.equalsIgnoreCase(map.get("qsign").toString()) && "verified".equalsIgnoreCase(map.get("status").toString())) {
            NotifyPayOrderDto notifyOrder = new NotifyPayOrderDto();
            notifyOrder.setOrderNo(map.get("order_id").toString());
            notifyOrder.setOrderStatus(map.get("status").equals("verified") ? "1" : "2");

            R<String> r = remotePayOrderService.callbackPayOrdery(notifyOrder, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("北洋支付更新订单信息失败:", orderNo);
                return;
            } else {
                String result = r.getData();
                if (result.equals("0")) {
                    response.getWriter().print("true");
                    return;
                } else {
                    response.getWriter().print("FAIL");
                    return;
                }
            }
        } else {
            log.error("北洋支付验证签名错误:", orderNo);
            return;
        }
    }


    /**
     * <B>先锋支付回调</B>
     *
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "/xianFeng", method = {RequestMethod.POST, RequestMethod.GET})
    public void xianfengNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, String> map = getRequestData(request);
        log.debug("先锋支付，成功收到回调通知，通知信息为:" + map.toString());

        String orderNo = map.get("order_no");
        NotifyOrderVo vo;

        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null) {
            log.error("先锋支付订单号不存在:", orderNo);
            return;
        } else {
            vo = key.getData();
        }

        StringBuffer buffer = new StringBuffer();
        Set<Map.Entry<String, String>> set = map.entrySet();
        Iterator<Map.Entry<String, String>> iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
            if (!StrUtil.equals("sign", entry.getKey()) && !StrUtil.equals("remark", entry.getKey()) && StrUtil.isNotEmpty(entry.getValue())) {
                buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
        }

        String signSrc = buffer.append("key=").append(vo.getMerchantKey()).toString();
        String sign = SignUtil.encryption(signSrc).toUpperCase();

        if (sign.equalsIgnoreCase(map.get("sign"))) {
            NotifyPayOrderDto notifyOrder = new NotifyPayOrderDto();
            notifyOrder.setOrderNo(orderNo);
            notifyOrder.setOrderStatus(map.get("order_status").equals("1") ? "1" : "2");

            R<String> r = remotePayOrderService.callbackPayOrdery(notifyOrder, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("先锋更新订单信息失败:", orderNo);
                return;
            } else {
                String result = r.getData();
                if (result.equals("0")) {
                    response.getWriter().print("success");
                    return;
                } else {
                    response.getWriter().print("fail");
                    return;
                }
            }
        } else {
            response.getWriter().print("fail");
            log.error("先锋支付验证签名错误:", orderNo);
        }
    }


    @RequestMapping(value = "/paiYi", method = {RequestMethod.POST, RequestMethod.GET})
    public void paiYiNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, String> map = getRequestData(request);
        String orderNo = map.get("out_trade_no");

        NotifyOrderVo vo;
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null) {
            log.error("派易支付订单号不存在:", orderNo);
            return;
        } else {
            vo = key.getData();
        }

        String str = String.format("partner=%s&out_trade_no=%s&payment_type=%s&trade_status=%s&total_fee=%s&trade_no=%s%s",
                map.get("partner"), map.get("out_trade_no"), map.get("payment_type"), map.get("trade_status"), map.get("total_fee"), map.get("trade_no"), vo.getMerchantKey());
        String sign = SignUtil.encryption(str).toLowerCase();

        if (sign.equalsIgnoreCase(map.get("sign")) && "1".equals(map.get("trade_status"))) {
            NotifyPayOrderDto notifyOrder = new NotifyPayOrderDto();
            notifyOrder.setOrderNo(orderNo);
            notifyOrder.setOrderStatus(map.get("trade_status").equals("1") ? "1" : "2");

            R<String> r = remotePayOrderService.callbackPayOrdery(notifyOrder, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("易派更新订单信息失败:", orderNo);
                return;
            } else {
                String result = r.getData();
                if (result.equals("0")) {
                    response.getWriter().print("success");
                    return;
                } else {
                    response.getWriter().print("fail");
                    return;
                }
            }
        } else {
            response.getWriter().print("fail");
            log.error("易派支付验证签名错误:", orderNo);
        }
    }


    /**
     * <B>信付通</B>
     *
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "/xinFuTong", method = {RequestMethod.POST, RequestMethod.GET})
    public void xinFuTongNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, String> paramMap = getRequestData(request);
        log.debug("信付通支付，收到的加密前参数：" + JSONObject.toJSONString(paramMap) + "---map---" + paramMap);

        //通过订单号获取配置信息
        String orderNo = paramMap.get("order_no");
        String sign = paramMap.get("sign");
        //获取配置信息
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null) {
            log.error("信付通订单号不存在:", orderNo);
            return;
        }

        NotifyOrderVo order = key.getData();
        SortedMap<String, Object> reqMap = new TreeMap<>();
        Map<String, String[]> requestParams = request.getParameterMap();

        for (Iterator<?> iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            reqMap.put(name, valueStr);
        }
        log.error("信付通参数reqMap" + reqMap);

        Set<String> set = reqMap.keySet();
        Iterator<String> requestIterator = set.iterator();
        SortedMap<String, Object> sginMap = new TreeMap<>();

        while (requestIterator.hasNext()) {
            String mapKey = requestIterator.next();
            Object mapValue = reqMap.get(mapKey);

            if (mapValue != null && !"sign".equals(mapKey) && !"signType".equals(mapKey)) {
                sginMap.put(mapKey, mapValue);
            }
        }

        log.error("信付通参数sginMap" + sginMap);

        // 商户构建请求参数
        StringBuffer buffer = new StringBuffer();
        Iterator<String> iterator = sginMap.keySet().iterator();
        while (iterator.hasNext()) {
            String keyy = (String) iterator.next();
            Object value = sginMap.get(keyy);
            if (value != null) {
                buffer.append(keyy).append("=").append(value).append("&");
            }
        }

        String signStr = buffer.toString();
        signStr = signStr.substring(0, signStr.length() - 1);
        String msign = SignUtil.sign(signStr + order.getMerchantKey(), "UTF-8").toUpperCase();

        boolean verifyRst = sign.equalsIgnoreCase(msign);
        log.error("信付通签名参数" + signStr + "--第三方sign" + sign + "--验证sign" + msign);

        //验证签名是否一致
        if (verifyRst) {
            if ("TRADE_FINISHED".equals(paramMap.get("trade_status"))) {
                NotifyPayOrderDto notifyOrder = new NotifyPayOrderDto();
                notifyOrder.setOrderNo(orderNo);
                notifyOrder.setOrderStatus(paramMap.get("trade_status").equals("TRADE_FINISHED") ? "1" : "2");

                R<String> r = remotePayOrderService.callbackPayOrdery(notifyOrder, SecurityConstants.FROM_IN);
                if (r == null || r.getData() == null) {
                    log.error("信付通更新订单信息失败:", orderNo);
                    return;
                } else {
                    String result = r.getData();
                    if (result.equals("0")) {
                        response.getWriter().print("success");
                        return;
                    } else {
                        response.getWriter().print("fail");
                        return;
                    }
                }
            }
        } else {
            response.getWriter().print("fail");
            log.error("信付通支付验证签名错误:", orderNo);
        }
    }


    /**
     * <B>鑫发支付</B>
     *
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "/xinFa", method = {RequestMethod.POST, RequestMethod.GET})
    public void xinFaNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SortedMap<String, String> paramMap = getRequestData(request);

        log.debug("鑫发支付，收到的加密前参数：" + JSONObject.toJSONString(paramMap) + "---map---" + paramMap);
        String orderNo = paramMap.get("orderNo");

        //获取秘钥信息
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        if (key == null || key.getData() == null) {
            log.error("鑫发支付订单号不存在:", orderNo);
            return;
        }

        NotifyOrderVo order = key.getData();
        String data = paramMap.get("data");
        byte[] result = ToolKit.decryptByPrivateKey(Base64.getDecoder().decode(data), order.getAppKey());

        String resultData = new String(result, "UTF-8");// 解密数据
        SortedMap<String, String> map = new TreeMap<>();
        map = new Gson().fromJson(resultData, map.getClass());

        log.error("鑫发支付，收到的成功异步通知：" + JSONObject.toJSONString(map) + "---map---" + map);
        String sign = map.get("sign");
        map.remove("sign");
        String jsonStr = ToolKit.mapToJson(map);
        String signstr = ToolKit.MD5(jsonStr + order.getMerchantKey(), ToolKit.CHARSET);

        if (sign.equalsIgnoreCase(signstr)) {
            if ("00".equals(map.get("payStateCode"))) {
                //发送请求
                NotifyPayOrderDto notifyOrder = new NotifyPayOrderDto();
                notifyOrder.setOrderNo(orderNo);
                notifyOrder.setOrderStatus(map.get("payStateCode").equals("00") ? "1" : "2");

                R<String> r = remotePayOrderService.callbackPayOrdery(notifyOrder, SecurityConstants.FROM_IN);
                if (r == null || r.getData() == null) {
                    log.error("鑫发支付更新订单信息失败:", orderNo);
                    return;
                } else {
                    String resultStr = r.getData();
                    if (resultStr.equals("0")) {
                        response.getWriter().print("SUCCESS");
                        return;
                    } else {
                        response.getWriter().print("FAIL");
                    }
                }

            }
        } else {
            log.error("鑫发支付验证签名错误:", orderNo);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/qiTongBao", method = {RequestMethod.POST, RequestMethod.GET})
    public String qiTongBaoNotify(@RequestBody String body) throws Exception {
        log.debug("启通宝支付，收到的加密前参数：---json---" + body);

        com.alibaba.fastjson.JSONObject requestJson = com.alibaba.fastjson.JSONObject.parseObject(body);
        com.alibaba.fastjson.JSONObject content = requestJson.getJSONObject("content");
        String sign = requestJson.getString("sign");

        //订单号
        String orderNo = content.getString("memberOrderNumber");
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        //获取公钥
        String publicKey = key.getData().getAppKey();

        if (!QtbUtil.verify(content, sign, publicKey)) {
            log.error("启通宝验证签名错误:", orderNo);
            return "FAIL";
        } else {
            //验签成功
            //支付成功 订单处理 改变订单状态值
            NotifyPayOrderDto notifyOrder = new NotifyPayOrderDto();
            notifyOrder.setOrderNo(orderNo);
            notifyOrder.setOrderStatus("SUCCESS".equals(content.get("orderStatus")) ? "1" : "2");

            R<String> r = remotePayOrderService.callbackPayOrdery(notifyOrder, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("启通宝更新订单信息失败:", orderNo);
                return "FAIL";
            } else {
                String resultStr = r.getData();
                if (resultStr.equals("0")) {
                    return "SUC";
                } else {
                    return "FAIL";
                }
            }
        }
    }


    @ResponseBody
    @RequestMapping(value = "/banMa", method = {RequestMethod.POST, RequestMethod.GET})
    public String banMaNotify(@RequestBody String body) throws Exception {
        log.debug("斑马支付，收到的加密前参数：---json---" + body);

        com.alibaba.fastjson.JSONObject requestJson = com.alibaba.fastjson.JSONObject.parseObject(body);
        com.alibaba.fastjson.JSONObject content = requestJson.getJSONObject("body");
        String sign = requestJson.getString("sign");

        //订单号
        String orderNo = content.getString("orderId");
        R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
        //获取公钥
        String publicKey = key.getData().getPublicKey();

        Map<String, String> treeMap = new HashMap<>();
        treeMap.put("amount", (String)content.get("amount"));
        treeMap.put("biz",  (String)content.get("biz"));
        treeMap.put("chargeTime",  (String)content.get("chargeTime"));
        treeMap.put("mchtId",  (String)content.get("mchtId"));
        treeMap.put("orderId",  (String)content.get("orderId"));
        treeMap.put("payType",  (String)content.get("payType"));
        treeMap.put("seq",  (String)content.get("seq"));
        treeMap.put("status",  (String)content.get("status"));
        treeMap.put("tradeId",  (String)content.get("tradeId"));

        String signStr = BanMaSignUtil.md5Sign(treeMap, publicKey);

        if (!signStr.equals(sign)) {
            log.error("斑马支付验证签名错误:", orderNo);
            return "FAIL";
        } else {
            //验签成功
            //支付成功 订单处理 改变订单状态值
            NotifyPayOrderDto notifyOrder = new NotifyPayOrderDto();
            notifyOrder.setOrderNo(orderNo);
            String status =  content.get("status").equals("SUCCESS") ? "1" : "2";
            notifyOrder.setOrderStatus(status);

            R<String> r = remotePayOrderService.callbackPayOrdery(notifyOrder, SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                log.error("斑马支付更新订单信息失败:", orderNo);
                return "FAIL";
            } else {
                String resultStr = r.getData();
                if (resultStr.equals("0")) {
                    return "SUCCESS";
                } else {
                    return "FAIL";
                }
            }
        }
    }



    @RequestMapping(value = "/chengYiTong", method = {RequestMethod.POST, RequestMethod.GET})
    public void chengYiTongNotify(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            SortedMap<String, String> paramMap = getRequestData(request);
            log.error("诚易通，成功收到回调通知，通知信息为：" + JSONObject.toJSONString(paramMap));
            String reqdata = paramMap.get("reqdata");
            String orderNo = paramMap.get("ordernumber");

            NotifyOrderVo vo;
            R<NotifyOrderVo> key = remotePayOrderService.getMerchantKey(orderNo, SecurityConstants.FROM_IN);
            if (key == null || key.getData() == null) {
                log.error("诚易通支付订单号不存在:", orderNo);
                return;
            } else {
                vo = key.getData();
            }

            String dataResultStr = RsaChengYiTongUtil.decryptByPrivateKey(reqdata, vo.getMerchantKey());
            com.alibaba.fastjson.JSONObject data = com.alibaba.fastjson.JSONObject.parseObject(dataResultStr);
            String signSrc = String.format("partner=%s&ordernumber=%s&orderstatus=%s&paymoney=%s%s",
                    data.get("partner").toString(), data.get("ordernumber").toString(), data.get("orderstatus").toString(), data.get("paymoney").toString(), vo.getAppKey());
            String sign = MD5.encryption(signSrc).toLowerCase();

            if (sign.equalsIgnoreCase(data.get("sign").toString()) && "1".equals(data.get("orderstatus").toString())) {
                NotifyPayOrderDto notifyOrder = new NotifyPayOrderDto();
                notifyOrder.setOrderNo(orderNo);

                notifyOrder.setOrderStatus(data.get("orderstatus").toString().equals("1") ? "1" : "2");
                R<String> r = remotePayOrderService.callbackPayOrdery(notifyOrder, SecurityConstants.FROM_IN);
                if (r == null || r.getData() == null) {
                    log.error("诚易通更新订单信息失败:", orderNo);
                    return;
                } else {
                    String result = r.getData();
                    if (result.equals("0")) {
                        response.getWriter().print("ok");
                        return;
                    } else {
                        response.getWriter().print("fail");
                        return;
                    }
                }
            } else {
                response.getWriter().print("fail");
                log.error("诚易通支付验证签名错误:", orderNo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
