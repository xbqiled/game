package com.youwan.game.api.model;

import lombok.Data;

import java.io.Serializable;


@Data
public class WechatJson implements Serializable {

    private String authID;

    private String authToken;

}
