package com.youwan.game.api.model;

import lombok.Data;

import java.io.Serializable;


@Data
public class IpJson implements Serializable {

    private String ip;

    private String type;

    private String sessId;

}
