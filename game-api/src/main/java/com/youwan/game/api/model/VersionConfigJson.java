package com.youwan.game.api.model;


import lombok.Data;

import java.io.Serializable;

@Data
public class VersionConfigJson implements Serializable {

    private String channelId;
    private String version;


}
