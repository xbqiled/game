package com.youwan.game.cac;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.*;
import com.youwan.game.cac.api.entity.*;
import com.youwan.game.cac.api.vo.*;
import com.youwan.game.cac.mapper.recordlog.RecorddamachangeMapper;
import com.youwan.game.cac.mapper.xbqp.AccountinfoMapper;
import com.youwan.game.cac.service.*;
import com.youwan.game.common.core.jsonmodel.GsonSuccour;
import com.youwan.game.common.core.jsonmodel.GsonWxhhExp;
import org.bouncycastle.cms.PasswordRecipientId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-07-05.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {CacApplication.class})
public class WhitelistServiceTest {
    @Autowired
    private WhitelistService whitelistService;

    @Test
    public void test() {
        Page page = new Page(1, 10);
        IPage<Map<String, Object>> ret = whitelistService.queryPageList(page, new Whitelist());
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private UsermailService usermailService;

    @Test
    public void test2() {
        Page page = new Page(1, 10);
        MailDto dto = new MailDto();

        IPage<List<Map<String, Object>>> ret = usermailService.queryUserMailPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test3() {

        MailVo ret = usermailService.getUserMailInfo(33);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test4() {
        MailVo ret = usermailService.findById(350, 1025175);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private UsergamedataService usergamedataService;

    @Test
    public void test5() {
        Page page = new Page(1, 10);
        IPage<List<Map<String, Object>>> ret = usergamedataService.queryUserCtrlPage(page, "1000064", null, null);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test6() {
        List<Map<String, Object>> ret = usergamedataService.queryGameRoom();
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private StatisChannelService statisChannelService;

    @Test
    public void test7() {
        Page page = new Page(1, 10);
        ReportDto dto = new ReportDto();
        dto.setStartTime("2020-02-01");
        dto.setEndTime("2020-04-30");
        statisChannelService.getStatisDayReportPage(page, dto);
    }

    @Test
    public void test8() {
        Page page = new Page(1, 10);
        ReportDto dto = new ReportDto();
        dto.setMonthTime("2020-04");
        statisChannelService.getStatisMonthReportPage(page, dto);
    }

    @Autowired
    private StatisAccountService statisAccountService;

    @Test
    public void test9() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();
        dto.setStartTime("20200401");
        dto.setEndTime("20200430");
        statisAccountService.queryStatisDataPage(page, dto);
    }

    @Autowired
    private SigninchannelService signinchannelService;

    @Test
    public void test10() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();
        dto.setStartTime("2019-04-01");
        dto.setEndTime("2020-05-30");
        IPage<List<SigninchannelVo>> ret = signinchannelService.querySignCfgPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private OtherBgbetRecordService otherBgbetRecordService;

    @Test
    public void test11() {
        Page page = new Page(1, 10);
        BgBetDto dto = new BgBetDto();
        dto.setStartTime("2020-02-01 00:00:00");
        dto.setEndTime("2020-04-30 00:00:00");
        IPage<List<OtherBgbetRecord>> ret = otherBgbetRecordService.queryBgBetRecordPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private NickAwardService nickAwardService;

    @Test
    public void test12() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();
        dto.setChannelId("1003");
        IPage<List<NickAwardVo>> ret = nickAwardService.queryPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private LuckyRouletteService luckyRouletteService;

    @Test
    public void test13() {
        Page page = new Page(1, 10);
        LuckyRoulette dto = new LuckyRoulette();
        dto.setTChannel("1005");
        IPage<List<Map<String, Object>>> ret = luckyRouletteService.queryLuckyRoulettePage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private InviteDepositService inviteDepositService;

    @Test
    public void test14() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();

        IPage<List<InviteDepositChannelVo>> ret = inviteDepositService.queryInviteDepositPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test15() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();

        InviteDepositChannelVo ret = inviteDepositService.findByChannelId("1020001001");
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private GameglobalextinfoService gameglobalextinfoService;

    @Test
    public void test16() {
        Page page = new Page(1, 10);
        WxhhExpDto dto = new WxhhExpDto();

        List<GsonWxhhExp> ret = gameglobalextinfoService.getWxhhExpPage(dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private GamectrlService gamectrlService;

    @Test
    public void test17() {
        Page page = new Page(1, 10);
        WxhhExpDto dto = new WxhhExpDto();

        IPage<List<GameCtrlVo>> ret = gamectrlService.queryGameCtrlPage(page, null);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test18() {
        Page page = new Page(1, 10);
        WxhhExpDto dto = new WxhhExpDto();

        List<Map<String, Object>> ret = gamectrlService.getRoomTreeData();
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private GameCtrlCfgStrService gameCtrlCfgStrService;

    @Test
    public void test19() {
        Page page = new Page(1, 10);
        WxhhExpDto dto = new WxhhExpDto();

        IPage<Map<String, Object>> ret = gameCtrlCfgStrService.queryGameCtrlCfgStrPage(page, null, null);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private GamectrlcfgService gamectrlcfgService;

    @Test
    public void test20() {
        Page page = new Page(1, 10);
        WxhhExpDto dto = new WxhhExpDto();

        IPage<Map<String, Object>> ret = gamectrlcfgService.queryGameCtrlCfgPage(page, null, null);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private DepositchannelService depositchannelService;

    @Test
    public void test21() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();

        IPage<DepositchannelVo> ret = depositchannelService.queryDepositPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private DayShareService dayShareService;

    @Test
    public void test22() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();

        IPage<DayShareVo> ret = dayShareService.queryPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test23() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();

        DayShareEntity ret = dayShareService.findByChannelId("1005");
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private ChannelxinboonoffService channelxinboonoffService;

    @Test
    public void test24() {
        Page page = new Page(1, 10);
        XinBoSwitchDto dto = new XinBoSwitchDto();

        List<XinBoSwitchVo> ret = channelxinboonoffService.selectAllSwitch(dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private ChanneltaskService channeltaskService;

    @Test
    public void test25() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();

        IPage<TaskVo> ret = channeltaskService.queryPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test26() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();

        Map<String, Object> ret = channeltaskService.findByChannelId("1020001001");
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private ChannelsuccourService channelsuccourService;

    @Test
    public void test27() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();

        IPage<SuccourVo> ret = channelsuccourService.queryPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test28() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();

        GsonSuccour ret = channelsuccourService.findByChannelId("1020001001");
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private BulletinService bulletinService;

    @Test
    public void test29() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();

        IPage<Bulletin> ret = bulletinService.queryBulletinPage(page, null, null);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test30() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();

        List<Map<String, Object>> ret = bulletinService.queryBulletinChannel(null);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private BindUrlService bindUrlService;

    @Test
    public void test31() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();

        IPage<BindUrlVo> ret = bindUrlService.queryPage(page, dto);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private AiService aiService;

    @Test
    public void test32() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();

        IPage<Map<String, Object>> ret = aiService.queryBotPage(page, null, null);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private AccountinfoService accountinfoService;
    @Autowired
    private AccountinfoMapper accountinfoMapper;

    @Test
    public void test33() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();

        Map ret = accountinfoService.queryUserInfo("1025228");
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test34() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();

        Accountinfo ret = accountinfoMapper.getUserInfo(1000004);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test35() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();

        Accountinfo ret = accountinfoMapper.getCashUserInfo(1000004);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test36() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();

        Accountinfo ret = accountinfoMapper.getUserInfoById(1000004);
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test37() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();
        Accountinfo accountinfo = new Accountinfo();
        accountinfo.setTAccountid(1000004);
        IPage<Map<String, Object>> ret = accountinfoService.queryUserInfoPage(page, accountinfo);
        System.err.println(JSON.toJSONString(ret));
    }

    @Autowired
    private RecorddamachangeMapper recorddamachangeMapper;

    @Test
    public void test38() {
        Page page = new Page(1, 10);
        QueryDto dto = new QueryDto();
        Accountinfo accountinfo = new Accountinfo();
        accountinfo.setTAccountid(1000004);
        List<Map<String, Object>> ret = recorddamachangeMapper.queryDamaAgg(1585670400, 1585756800, "1020001001", Arrays.asList(1025742, 1025721,
                1025824));
        System.err.println(JSON.toJSONString(ret));
    }

    @Test
    public void test39() {
        List<DaMaValue> ret = accountinfoService.getDamaValueList("1020001001", new Integer[]{1025742, 1025721, 1025824});
        System.err.println(JSON.toJSONString(ret));
    }
}
