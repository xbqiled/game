package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.SourceUrl;
import com.youwan.game.cac.service.SourceUrlService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.common.security.annotation.Inner;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-04-19 13:18:35
 */
@RestController
@AllArgsConstructor
@RequestMapping("/sourceurl")
public class SourceUrlController {

  private final SourceUrlService sourceUrlService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param sourceUrl
   * @return
   */
  @GetMapping("/page")
  public R getSourceUrlPage(Page page, SourceUrl sourceUrl) {
    return new R<>(sourceUrlService.querySourceUrlPage(page, sourceUrl));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") String id) {
    return new R<>(sourceUrlService.findById(id));
  }

  /**
   * 新增
   * @param sourceUrl
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody SourceUrl sourceUrl){
    return new R<>(sourceUrlService.saveSourceUrl(sourceUrl));
  }

  /**
   * 修改
   * @param sourceUrl
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody SourceUrl sourceUrl){
    return new R<>(sourceUrlService.updateSourceById(sourceUrl));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable String id){
    return new R<>(sourceUrlService.deleteSourceById(id));
  }


  /**
   * <B>通过ID 获取对应的信息</B>
   * @return
   */
  @Inner
  @GetMapping("/getById/{id}")
  public R getJumpUrl(@PathVariable("id")  String id) {
    return  new R<>(sourceUrlService.getBySerial(id));
  }
}
