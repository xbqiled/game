package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.ChlRole;
import com.youwan.game.cac.mapper.ChlRoleMapper;
import com.youwan.game.cac.service.ChlRoleService;
import org.springframework.stereotype.Service;

/**
 * 角色
 *
 * @author code generator
 * @date 2019-02-09 17:17:39
 */
@Service("chlRoleService")
public class ChlRoleServiceImpl extends ServiceImpl<ChlRoleMapper, ChlRole> implements ChlRoleService {

}
