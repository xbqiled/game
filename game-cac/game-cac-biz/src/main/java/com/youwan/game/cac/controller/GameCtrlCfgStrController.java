package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.GameCtrlCfgStrDto;
import com.youwan.game.cac.api.dto.WxhhCtrlCfgDto;
import com.youwan.game.cac.service.GameCtrlCfgStrService;
import com.youwan.game.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * VIEW
 *
 * @author code generator
 * @date 2020-04-14 19:28:20
 */
@RestController
@AllArgsConstructor
@RequestMapping("/gamectrlcfgstr")
public class GameCtrlCfgStrController {

  private final GameCtrlCfgStrService gameCtrlCfgStrService;

  /**
   * 分页查询
   * @param page 分页对象
   * @return
   */
  @GetMapping("/page")
  public R getGameCtrlCfgStrPage(Page page, GameCtrlCfgStrDto gameCtrlCfgStrDto) {
    return new R<>(gameCtrlCfgStrService.queryGameCtrlCfgStrPage(page, gameCtrlCfgStrDto.getStartTime(), gameCtrlCfgStrDto.getEndTime()));
  }

  @GetMapping("/getGameList")
  public R getGameList(){
    List<Map<String, Object>> resultList = gameCtrlCfgStrService.getGameList();
    return new R<>(resultList);
  }

  @GetMapping("/info/{keyId}")
  public R info(@PathVariable String keyId) {
    return  new R<>(gameCtrlCfgStrService.getWxhhCtrlCfg(keyId));
  }

  @PostMapping("/doCfg")
  public R doCfg(@RequestBody WxhhCtrlCfgDto wxhhCtrlCfgDto) {
    int result = gameCtrlCfgStrService.doCfg(wxhhCtrlCfgDto);
    if (result == 0) {
      return new R<>();
    } else {
      return new R<>().setCode(result).setMsg("设置五星宏辉控制失败!");
    }
  }
}
