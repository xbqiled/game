package com.youwan.game.cac.service;

import com.youwan.game.cac.api.dto.BettingRecordQueryDto;
import com.youwan.game.common.core.betting.BettingRebateTakingRecord;
import com.youwan.game.common.core.betting.ScrubResult;
import com.youwan.game.common.data.core.Page;

import java.util.List;

/**
 * 投注返利
 * @author terry.jiang[taoj555@163.com] on 2020-10-10.
 */
public interface BettingRebateService {

    /**
     * 获取个人当前洗码记录
     * @param userId
     * @return
     */
    List<ScrubResult> getScrubRebateDetails(Long userId);

    /**
     * 领取洗码反水
     * @param userId
     * @return
     */
    boolean takeScrubRebate(Long userId);

    /**
     * 获取个人洗码领取记录
     * @param dto
     * @return
     */
    Page<BettingRebateTakingRecord> getPersonTakingRecords(BettingRecordQueryDto dto);

    /**
     * 查询个人已领取的洗码明细
     * @param userId
     * @param offset
     * @param limit
     * @return
     */
    Page<ScrubResult> queryPersonScrubRebateHistory(Long userId, Integer offset, Integer limit);
}
