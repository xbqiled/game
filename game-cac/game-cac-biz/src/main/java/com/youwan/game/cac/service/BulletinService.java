package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.BulletinDto;
import com.youwan.game.cac.api.entity.Bulletin;
import com.youwan.game.cac.api.vo.BulletinVo;

import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author code generator
 * @date 2018-12-29 13:18:03
 */
public interface BulletinService extends IService<Bulletin> {

    /**
     * <B>查询公告信息列表</B>
     * @param ttitle
     * @return
     */
    IPage<Bulletin> queryBulletinPage(Page page,  String tid, String ttitle);


    /**
     * <B>查询公告归属渠道</B>
     * @return
     */
    List<Map<String, Object>> queryBulletinChannel(String bulletinId);


    /**
     * <B>查询公告信息</B>
     * @param id
     * @return
     */
    BulletinVo getBulletinVoById(String id);


    int addBulletin(BulletinDto bulletinDto);


    int modifyBulletin(BulletinDto bulletinDto);


    int delBulletin(String bulletinId);
}
