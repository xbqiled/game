package com.youwan.game.cac.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.Accountinfo;
import com.youwan.game.cac.api.entity.Usergamedata;
import com.youwan.game.cac.mapper.xbqp.AccountinfoMapper;
import com.youwan.game.cac.mapper.xbqp.UsergamedataMapper;
import com.youwan.game.cac.service.UsergamedataService;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.HttpHandler;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import org.elasticsearch.common.Strings;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-10 21:03:48
 */
@AllArgsConstructor
@Service("usergamedataService")
public class UsergamedataServiceImpl extends ServiceImpl<UsergamedataMapper, Usergamedata> implements UsergamedataService {

    private final AccountinfoMapper accountinfoMapper;

    @Override
    public IPage<List<Map<String, Object>>> queryUserCtrlPage(Page page, String accountId, String gameId, String channelId) {
        return baseMapper.queryUserCtrlPage(page, channelId, Strings.hasText(accountId) ? Integer.parseInt(accountId) : null, gameId);
    }

    @Override
    public List<Map<String, Object>> queryGameRoom() {
        return baseMapper.queryGameRoom();
    }

    @Override
    public int doSet(String accountId, String type, String betLimit, String rate, String round, String roomList, String channelId) {
        Accountinfo account = accountinfoMapper.getUserInfo(Integer.parseInt(accountId));
        if (account == null) {
            return 1;
        }

        Integer[] roomArr = {};
        if (StrUtil.isNotBlank(roomList)) {
            if (roomList.contains(",")) {

                String[] strDesc = roomList.split(",");
                if (strDesc != null && strDesc.length > 0) {
                    roomArr = new Integer[strDesc.length];

                    for (int i = 0; i < strDesc.length; i++) {
                        roomArr[i] = Integer.parseInt(strDesc[i]);
                    }
                }
            } else {
                roomArr = new Integer[1];
                roomArr[0] = Integer.parseInt(roomList);
            }
        }

        //调用接口处理
        GsonResult result = HttpHandler.userCtrl(new Integer(accountId), new Integer(type), roomArr, new Integer(betLimit), new Integer(rate),
                new Integer(round));
        if (result != null && result.getRet() == 0) {
            return 0;
        } else {
            return 2;
        }
    }

    @Override
    public int cancelCtrl(String accountId, String roomId) {
        Integer[] roomArr = {Integer.parseInt(roomId)};
        GsonResult result = HttpHandler.cancelUserCtrl(new Integer(accountId), roomArr);
        if (result != null && result.getRet() == 0) {
            return 0;
        } else {
            return 2;
        }
    }
}
