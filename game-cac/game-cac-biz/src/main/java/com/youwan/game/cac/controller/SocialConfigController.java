package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.SocialConfig;
import com.youwan.game.cac.service.SocialConfigService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.common.security.annotation.Inner;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-03-26 15:43:34
 */
@RestController
@AllArgsConstructor
@RequestMapping("/socialconfig")
public class SocialConfigController {

  private final SocialConfigService socialConfigService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param socialConfig
   * @return
   */
  @GetMapping("/page")
  public R getSocialConfigPage(Page page, SocialConfig socialConfig) {
    return  new R<>(socialConfigService.page(page, Wrappers.query(socialConfig)));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(socialConfigService.getById(id));
  }

  /**
   * 新增
   * @param socialConfig
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  @PreAuthorize("@pms.hasPermission('cac_social_add')")
  public R save(@RequestBody SocialConfig socialConfig){
    return new R<>(socialConfigService.save(socialConfig));
  }

  /**
   * 修改
   * @param socialConfig
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  @PreAuthorize("@pms.hasPermission('cac_social_modify')")
  public R updateById(@RequestBody SocialConfig socialConfig){
    return new R<>(socialConfigService.updateById(socialConfig));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  @PreAuthorize("@pms.hasPermission('cac_social_del')")
  public R removeById(@PathVariable Integer id){
    return new R<>(socialConfigService.removeById(id));
  }


  /**
   * <B>获取配置信息</B>
   * @param channelId
   * @return
   */
  @Inner
  @PostMapping("/getConfig/{channelId}")
  public R getSocialConfigById(@PathVariable String channelId){
    return new R<>(socialConfigService.getSocialConfigById(channelId));
  }

}
