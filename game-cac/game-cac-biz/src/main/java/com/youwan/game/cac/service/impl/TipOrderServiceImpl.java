package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.TipOrder;
import com.youwan.game.cac.mapper.TipOrderMapper;
import com.youwan.game.cac.service.TipOrderService;
import org.springframework.stereotype.Service;

/**
 * 
 *
 * @author code generator
 * @date 2019-05-21 11:20:06
 */
@Service("tipOrderService")
public class TipOrderServiceImpl extends ServiceImpl<TipOrderMapper, TipOrder> implements TipOrderService {

}
