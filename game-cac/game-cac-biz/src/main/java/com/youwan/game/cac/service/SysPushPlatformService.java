package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.SysPushPlatform;

import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author code generator
 * @date 2020-05-23 15:14:24
 */
public interface SysPushPlatformService extends IService<SysPushPlatform> {

    IPage<List<Map<String, Object>>> queryPushPlatformPage(Page page, String platformName);

    Boolean addPushPlatform(SysPushPlatform sysPushPlatform );

    Boolean updatePushPlatform(SysPushPlatform sysPushPlatform );

    Boolean delPushPlatform(Integer pushPlatformId);

    List<Map<String, Object>> getPushPlatformList();
}
