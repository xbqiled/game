package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.ShortUrl;
import com.youwan.game.cac.mapper.ShortUrlMapper;
import com.youwan.game.cac.service.ShortUrlService;
import com.youwan.game.common.core.util.HttpHandler;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * @author code generator
 * @date 2019-04-19 13:17:59
 */
@Service("shortUrlService")
public class ShortUrlServiceImpl extends ServiceImpl<ShortUrlMapper, ShortUrl> implements ShortUrlService {

    @Override
    public IPage<List<ShortUrl>> queryShortUrlPage(Page page, ShortUrl url) {
        return baseMapper.queryShortUrlPage(page, url.getName());
    }

    /**
     * <B>创建一条短网址信息</B>
     * @param url
     * @return
     */
    @Override
    @CacheEvict(value = "short_url:details", key = "#url")
    public String createShortUrl(String url, ShortUrl shortUrl) {
        //首先判断短链接是否为第三方还是系统自生
        String resultUrl = HttpHandler.createShortUrl(shortUrl.getUrl(), url, shortUrl.getUrlPara(), shortUrl.getHeadJson(), shortUrl.getBodyJson(), shortUrl.getReqType(), shortUrl.getReplacePostion());
        int count = shortUrl.getReqCount() + 1;
        shortUrl.setReqCount(count);

        this.updateById(shortUrl);
        return resultUrl;
    }


    @Override
    @CacheEvict(value = "short_url:all_details")
    public  List<ShortUrl> getAllShortUrlConfig() {
      return   baseMapper.getShortUrlList();
    }

    @Override
    @CacheEvict(value = "short_url:details", allEntries = true)
    public Boolean updateShortById(ShortUrl shortUrl) {
        return this.updateById(shortUrl);
    }

    @Override
    @CacheEvict(value = "short_url:details", allEntries = true)
    public Boolean deleteShortById(Integer id) {
        return this.removeById(id);
    }

    @Override
    @CacheEvict(value = "short_url:details", allEntries = true)
    public Boolean saveShortUrl(ShortUrl shortUrl) {
        shortUrl.setCreateTime(LocalDateTime.now());
        shortUrl.setCreateBy(Objects.requireNonNull(getUsername()));

        this.save(shortUrl);
        return Boolean.TRUE;
    }

    /**
     * 获取用户名称
     *
     * @return username
     */
    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }

}
