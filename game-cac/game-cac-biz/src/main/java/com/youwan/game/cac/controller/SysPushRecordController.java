package com.youwan.game.cac.controller;

import cn.jpush.api.report.MessagesResult;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.PushFromDto;
import com.youwan.game.cac.api.dto.PushRecordDto;
import com.youwan.game.cac.service.SysPushRecordService;
import com.youwan.game.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author code generator
 * @date 2020-05-23 17:21:44
 */
@RestController
@AllArgsConstructor
@RequestMapping("/pushrecord")
public class SysPushRecordController {

  private final SysPushRecordService sysPushRecordService;

  @GetMapping("/page")
  public R getSysPushRecordPage(Page page, PushRecordDto pushRecordDto) {
    return  new R<>(sysPushRecordService.queryPushRecordPage(page, pushRecordDto.getChannelId(), pushRecordDto.getStartTime(), pushRecordDto.getEndTime()));
  }

  @PostMapping("/sendPush")
  public R sendPush(@RequestBody PushFromDto pushFromDto) {
    Boolean result = sysPushRecordService.sendPush(pushFromDto);
    if (result) {
      return  new R<>();
    } else {
      return new R<>().setCode(1).setMsg("推送失败,超过当前最大值");
    }
  }

  @GetMapping("/getById/{id}")
  public R getConfigById(@PathVariable("id") Integer id){
    Map<String, Object> pushRecord = sysPushRecordService.getObj(id);
    return new R<>(pushRecord);
  }

  @GetMapping("/getReportById/{id}")
  public R getReportById(@PathVariable("id") Integer id){
    return new R<>(sysPushRecordService.getReport(id));
  }
}
