package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.BgTransferDto;
import com.youwan.game.cac.api.entity.OtherBgTransfer;
import com.youwan.game.cac.mapper.OtherBgTransferMapper;
import com.youwan.game.cac.service.OtherBgTransferService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-05 15:57:02
 */
@Service("otherBgTransferService")
public class OtherBgTransferServiceImpl extends ServiceImpl<OtherBgTransferMapper, OtherBgTransfer> implements OtherBgTransferService {

    @Override
    public IPage<List<OtherBgTransfer>> queryBgTransferPage(Page page, BgTransferDto bgTransferDto) {
        return baseMapper.queryBgTransferPage(page, bgTransferDto.getUserId(), bgTransferDto.getStartTime(), bgTransferDto.getEndTime());
    }
}
