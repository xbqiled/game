package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.SysPushPlatform;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2020-05-23 15:14:24
 */
public interface SysPushPlatformMapper extends BaseMapper<SysPushPlatform> {

    IPage<List<Map<String, Object>>> queryPushPlatformPage(Page page, @Param("platformName") String platformName);

    List<Map<String, Object>> getPushPlatformList();

}
