package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.WxCashOrder;
import com.youwan.game.cac.mapper.WxCashOrderMapper;
import com.youwan.game.cac.service.WxCashOrderService;
import org.springframework.stereotype.Service;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-06 13:34:56
 */
@Service("wxCashOrderService")
public class WxCashOrderServiceImpl extends ServiceImpl<WxCashOrderMapper, WxCashOrder> implements WxCashOrderService {

}
