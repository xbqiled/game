package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.Usergamedata;

import java.util.List;
import java.util.Map;


/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-10 21:03:48
 */
public interface UsergamedataService extends IService<Usergamedata> {

    IPage<List<Map<String, Object>>> queryUserCtrlPage(Page page, String accountId, String gameId, String channelId);

    List<Map<String, Object>> queryGameRoom();

    int doSet(String accountId, String type, String betLimit, String rate, String round, String roomList, String channelId);

    int cancelCtrl(String accountId, String roomId);

}
