package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.ChlRole;

/**
 * 角色
 *
 * @author code generator
 * @date 2019-02-09 17:17:39
 */
public interface ChlRoleMapper extends BaseMapper<ChlRole> {

}
