package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.ChlUser;

/**
 * 系统用户
 *
 * @author code generator
 * @date 2019-02-09 17:16:48
 */
public interface ChlUserMapper extends BaseMapper<ChlUser> {

}
