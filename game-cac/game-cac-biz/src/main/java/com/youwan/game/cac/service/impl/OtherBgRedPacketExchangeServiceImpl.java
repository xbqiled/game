package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.OtherBgRedPacketExchange;
import com.youwan.game.cac.mapper.OtherBgRedPacketExchangeMapper;
import com.youwan.game.cac.service.OtherBgRedPacketExchangeService;
import org.springframework.stereotype.Service;

/**
 * 
 *
 * @author code generator
 * @date 2020-01-13 20:43:08
 */
@Service("bgRedPacketExchangeService")
public class OtherBgRedPacketExchangeServiceImpl extends ServiceImpl<OtherBgRedPacketExchangeMapper, OtherBgRedPacketExchange> implements OtherBgRedPacketExchangeService {

}
