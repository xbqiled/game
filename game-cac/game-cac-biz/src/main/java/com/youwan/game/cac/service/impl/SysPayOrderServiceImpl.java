package com.youwan.game.cac.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.AddPointDto;
import com.youwan.game.cac.api.dto.NotifyPayOrderDto;
import com.youwan.game.cac.api.dto.PayOrderDto;
import com.youwan.game.cac.api.entity.SysPayConfig;
import com.youwan.game.cac.api.entity.SysPayOrder;
import com.youwan.game.cac.api.entity.SysPayRebate;
import com.youwan.game.cac.api.entity.Accountinfo;
import com.youwan.game.cac.api.vo.*;
import com.youwan.game.cac.handler.PayHand;
import com.youwan.game.cac.mapper.SysPayConfigMapper;
import com.youwan.game.cac.mapper.SysPayOrderMapper;
import com.youwan.game.cac.mapper.SysPayRebateMapper;
import com.youwan.game.cac.mapper.xbqp.AccountinfoMapper;
import com.youwan.game.cac.service.SysPayConfigService;
import com.youwan.game.cac.service.SysPayOrderService;
import com.youwan.game.common.core.constant.CommonConstant;
import com.youwan.game.common.core.constant.ConfigConstant;
import com.youwan.game.common.core.constant.EnumConstant;
import com.youwan.game.common.core.jsonmodel.GsonReduceResult;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.HttpHandler;
import com.youwan.game.common.core.util.IPUtils;
import com.youwan.game.common.core.util.TableUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2019-02-20 18:03:40
 */
@Slf4j
@AllArgsConstructor
@Service("sysPayOrderService")
public class SysPayOrderServiceImpl extends ServiceImpl<SysPayOrderMapper, SysPayOrder> implements SysPayOrderService {

    private final SysPayConfigMapper sysPayConfigMapper;

    private final AccountinfoMapper accountinfoMapper;

    private final SysPayRebateMapper sysPayRebateMapper;

    private final SysPayConfigService sysPayConfigService;

    @Override
    public IPage<List<SysPayOrder>> queryPayOrderPage(Page page, String orderNo, String userId, String status) {
        return baseMapper.queryPayOrderPage(page, orderNo, userId, status);
    }

    @Override
    public PayConfigVo getPayInfo(PayConfigVo payConfigVo, PayOrderDto payOrderDto, HttpServletRequest request) {
        //生成订单编号
        String uuidStr = IdUtil.simpleUUID();
        //创建订单信息
        SysPayOrder sysPayOrder = PayHand.packagePayOrdery(payOrderDto, uuidStr, payConfigVo, ConfigConstant.HANDLE_TYPE.SYSTEM.getValue());

        //生成支付字符串
        if (payConfigVo != null && StrUtil.isNotBlank(payConfigVo.getPlatformKey())) {
            String[] parameterArray = PayHand.packagePayMap(payOrderDto,  uuidStr, payConfigVo, IPUtils.getIpAddr(request));
            Map<String, String> map = PayHand.getPayParameter(payConfigVo, parameterArray);

            if (CollUtil.isNotEmpty(map) && StrUtil.isNotEmpty(map.get("redirectUrl"))) {
                payConfigVo.setPayGetway(map.get("redirectUrl"));
                map.remove("redirectUrl");
            }

            payConfigVo.setParameterMap(map);
        }
        this.save(sysPayOrder);
        return  payConfigVo;
    }


    @Override
    public PayResult getPayUrl(PayConfigVo payConfigVo, PayOrderDto payOrderDto, HttpServletRequest request) {
        //生成订单编号
        String uuidStr = IdUtil.simpleUUID();

        //获取通道配置信息
        if (payConfigVo != null && StrUtil.isNotBlank(payConfigVo.getPlatformKey())) {
             //判断配置信息
             if (payConfigVo.getHandType().intValue() == EnumConstant.PAY_TYPE_HAND.BCL.getValue()) {
                 //不处理
                 return new PayResult();
             } else if (payConfigVo.getHandType().intValue() == EnumConstant.PAY_TYPE_HAND.PAYURL.getValue()) {
                 //创建订单
                 SysPayOrder sysPayOrder = PayHand.packagePayOrdery(payOrderDto, uuidStr, payConfigVo, ConfigConstant.HANDLE_TYPE.SYSTEM.getValue());
                 //转换成String []
                 String[] parameterArray = PayHand.packagePayMap(payOrderDto,  uuidStr, payConfigVo, IPUtils.getIpAddr(request));
                 //生成信息
                 PayResult payResult = PayHand.doOnLinePay(payConfigVo,  payOrderDto, uuidStr, parameterArray);
                 //判断类型如果是扫码支付生成二维码，并保存信息
                 if (payResult != null && StrUtil.isNotBlank(payResult.getPayUrl())) {
                     //判断处理
                     this.save(sysPayOrder);
                 }
                 return payResult;
             } else if (payConfigVo.getHandType().intValue() == EnumConstant.PAY_TYPE_HAND.PAGE.getValue()) {
                 //加工生成本地URL返回
                 return PayHand.createPayUrl(payOrderDto, uuidStr);
             } else if (payConfigVo.getHandType().intValue() == EnumConstant.PAY_TYPE_HAND.CASHIER.getValue()) {
                 //加工收银台本地URL返回

                 return new PayResult();
             } else if (payConfigVo.getHandType().intValue() == EnumConstant.PAY_TYPE_HAND.WEB.getValue()) {
                //加工WEBHTML代码返回
                 return new PayResult();
             } else {
                 return new PayResult();
             }
        }
        return new PayResult();
    }


    /**
     * <B>创建支付订单</B>
     *
     * @param payOrderDto
     * @return
     */
    @Override
    public PayResult createPayOrder(PayOrderDto payOrderDto) {
        PayResult payResult = new PayResult();
        //判断是否存在30秒前同样的充值信息
        List<SysPayOrder>  list = baseMapper.queryNowRechargeList(payOrderDto.getChannelId(), payOrderDto.getUserId(),  new BigDecimal(payOrderDto.getPayfee()), DateUtil.format(DateUtil.offsetSecond(new Date(), -60), ConfigConstant.FULL_DATE_TIME_FORMAT));
        if (CollUtil.isNotEmpty(list) && list.size() > 0) {
            payResult.setCode("1");
        } else {
            //首先获取配置信息
            PayChannelVo vo = sysPayConfigMapper.queryPayConfig(payOrderDto.getConfigId());
            //订单信息
            SysPayOrder sysPayOrder;
            //生成uuid
            String uuidStr = IdUtil.simpleUUID();
            sysPayOrder = packagePayOrder(payOrderDto, uuidStr, vo, ConfigConstant.HANDLE_TYPE.HAND.getValue());
            this.save(sysPayOrder);

            payResult.setCode("0");
            payResult.setStatus(sysPayOrder.getStatus().toString());
            payResult.setPayFee(sysPayOrder.getPayFee().toString());

            payResult.setPayName(sysPayOrder.getPayName());
            payResult.setOrderNo(sysPayOrder.getOrderNo());
        }
        return payResult;
    }


    @Override
    public NotifyOrderVo getMerchantKey(String orderId) {
        return baseMapper.getMerchantKey(orderId);
    }


    @Override
    public PayOrderVo getOrder(String orderId) {
        return baseMapper.getPayOrder(orderId);
    }

    /**
     * <B>组装map信息</B>
     * @return
     */
    String [] packagePayMap(PayOrderDto payOrderDto, String uuidStr, PayChannelVo vo) {
        List<String> list = new ArrayList<>();

        //商户号
        list.add(vo.getMerchantCode());
        //订单号
        list.add(uuidStr);
        //支付金额
        list.add(payOrderDto.getPayfee());
        //支付类型
        list.add(vo.getPlatformType().toString());
        //网关
        list.add(vo.getPayGetway());
        //产品名称
        list.add(CommonConstant.GOLD_COIN_BUY);
        //商户秘钥
        list.add(vo.getMerchantKey());
        //BankCode
        list.add(payOrderDto.getBankCode());

        String [] payParameter = new String [list.size()];
        return list.toArray(payParameter);
    }


    @Override
    public GsonReduceResult reduceFee(String userId, String note, String payFee, Integer force) {
        //判断用户是否为当前渠道的用户
        Accountinfo userInfo = accountinfoMapper.getUserInfo(Integer.parseInt(userId));

        if (userInfo == null) {
            return  null;
        }

        GsonReduceResult result = HttpHandler.reduceFee(Integer.parseInt(userId), new BigDecimal(payFee), force);
        //调用接口扣分
        if (result != null && result.getRet() == 0 && result.getRealCancel() > 0) {
            SysPayOrder insertOrder = new SysPayOrder();

            String oderId = IdUtil.fastSimpleUUID();
            insertOrder.setOrderNo(oderId);
            insertOrder.setUserId(Integer.valueOf(userId));

            insertOrder.setNote(note);
            insertOrder.setChannelId(userInfo.getTChannelkey());
            insertOrder.setStatus(4);

            insertOrder.setHandlerType(EnumConstant.HANDLE_TYPE.REQUEST.getValue());
            insertOrder.setPayFee(new BigDecimal(0 - result.getRealCancel()));
            insertOrder.setCreateBy(getUsername());

            insertOrder.setCreateTime(LocalDateTime.now());
            this.save(insertOrder);
            return result;
        } else {
            return new GsonReduceResult(result.getRet());
        }
    }

    /**
     * <B> 组装payOrder信息</B>
     * @return
     */
    SysPayOrder packagePayOrder(PayOrderDto payOrderDto, String uuidStr, PayChannelVo vo, int handType) {
        //创建支付订单
        SysPayOrder payOrder = new SysPayOrder();
        //银行卡转账支付
        payOrder.setOrderNo(uuidStr);
        payOrder.setUserId(Integer.parseInt(payOrderDto.getUserId()));
        BigDecimal payDecimal = new BigDecimal(payOrderDto.getPayfee());

        payOrder.setPayFee(payDecimal);
        BigDecimal poundageDecimal = new BigDecimal(0);
        payOrder.setPoundage(poundageDecimal);

        payOrder.setChannelId(payOrderDto.getChannelId());
        payOrder.setPayConfigId(Integer.parseInt(payOrderDto.getConfigId()));
        payOrder.setPayName(vo.getPayName());

        payOrder.setStatus(Integer.parseInt(ConfigConstant.PAYORDR_STATUS.WAIT.getValue()));
        payOrder.setDepositor(payOrderDto.getPayname());
        payOrder.setAccount(payOrderDto.getPayaccount());

        payOrder.setHandlerType(handType);
        payOrder.setCreateBy("SYSTEM");
        payOrder.setCreateTime(LocalDateTime.now());
        return payOrder;
    }


    /**
     * <B>回调支付订单</B>
     * @param payOrderDto
     * @return
     */
    @Override
    public String callbackPayOrdery(NotifyPayOrderDto payOrderDto) {
        //判断订单信息
        SysPayOrder payOrder = baseMapper.getPayOrderById(payOrderDto.getOrderNo());
        if (payOrder == null && StrUtil.isNotEmpty(payOrder.getOrderNo())) {
            //没有找到当前订单
            return  "1";
        }

        Integer rate = 0;
        //获取返利配置
        if (payOrder.getPayConfigId() != null && payOrder.getPayConfigId() > 0) {
           SysPayConfig config = sysPayConfigService.getById(payOrder.getPayConfigId());
           if (config != null && config.getReturnRate() != null && config.getReturnRate() > 0) {
               rate = config.getReturnRate();
           }
        }

        //判断订单是否为支付成功,
        if (payOrderDto.getOrderStatus().equals("1") && payOrder.getStatus() != 1) {
            //调用接口获取信息
            GsonResult result = HttpHandler.rechargeOrder(payOrder.getUserId(), payOrder.getOrderNo(), payOrder.getPayFee().intValue(), 0, 1, 0, payOrderDto.getDamaMulti(), rate);
            if (result.getRet() != 0) {
                return "2";
            } else {
                //更改订单状态
                payOrder.setStatus(Integer.parseInt(ConfigConstant.PAYORDR_STATUS.SUCCESS.getValue()));
                payOrder.setModifyBy("SYSTEM");
                payOrder.setModifyTime(LocalDateTime.now());
                this.updateById(payOrder);
                //判断是否有返利
                if (result.getRebateCoin() != null && result.getRebateCoin().longValue() > 0) {
                    SysPayRebate sysPayRebate = packagePayRebate(payOrder, new BigDecimal(result.getRebateCoin() / 100));
                    sysPayRebateMapper.savaRebate(sysPayRebate);
                }
                return "0";
            }
            //失败了
        } else if (payOrderDto.getOrderStatus().equals("2") && payOrder.getStatus().intValue() != 2) {
            //更改订单状态
            payOrder.setStatus(Integer.parseInt(ConfigConstant.PAYORDR_STATUS.FAIL.getValue()));
            payOrder.setModifyBy("SYSTEM");
            payOrder.setModifyTime(LocalDateTime.now());
            this.updateById(payOrder);
            return "0";
        }
        return "0";
    }

    @Override
    public AddPointVo addPoint(AddPointDto addPointDto) {
        String uuidStr = IdUtil.simpleUUID();
        //判断是否存在channelId
        Accountinfo userInfo = null;
        String tableName = TableUtils.getUserTableName(Integer.valueOf(addPointDto.getUserId()));
        if (addPointDto != null && StrUtil.isBlank(addPointDto.getChannelId())) {
            userInfo = accountinfoMapper.getUserInfoById(Integer.valueOf(addPointDto.getUserId()));
        } else {
            userInfo = accountinfoMapper.getChannelUserInfoById(tableName, addPointDto.getChannelId(), addPointDto.getUserId());
        }

        if (userInfo == null) {
            return null;
        }

        AddPointVo vo = new AddPointVo();
        vo.setChannelId(userInfo.getTChannelkey());
        vo.setUserId(addPointDto.getUserId());
        vo.setAmount(addPointDto.getAmount());

        SysPayOrder sysPayOrder = new SysPayOrder();
        sysPayOrder.setOrderNo(uuidStr);
        sysPayOrder.setUserId(Integer.parseInt(addPointDto.getUserId()));
        BigDecimal payDecimal = new BigDecimal(addPointDto.getAmount());

        sysPayOrder.setPayFee(payDecimal);
        BigDecimal poundageDecimal = new BigDecimal(0);
        sysPayOrder.setPoundage(poundageDecimal);

        sysPayOrder.setChannelId(userInfo.getTChannelkey());
        sysPayOrder.setPayConfigId(10000);
        sysPayOrder.setPayName("银商上分");

        sysPayOrder.setStatus(Integer.parseInt(ConfigConstant.PAYORDR_STATUS.SUCCESS.getValue()));
        sysPayOrder.setDepositor("银商orgId:" + addPointDto.getOrgId());
        sysPayOrder.setAccount("银商后台Id:" + addPointDto.getOperateUserId());

        sysPayOrder.setHandlerType(ConfigConstant.HANDLE_TYPE.RECHARGE.getValue());
        sysPayOrder.setCreateBy("SYSTEM");
        sysPayOrder.setCreateTime(LocalDateTime.now());
        GsonResult result = HttpHandler.rechargeOrder(sysPayOrder.getUserId(), sysPayOrder.getOrderNo(), sysPayOrder.getPayFee().intValue(), 0,  1, 1, 0.00, 0);
        if (result != null && result.getRet() == 0) {
            this.save(sysPayOrder);
            vo.setStatus(1);
        } else {
            vo.setStatus(2);
        }
        return vo;
    }


    @Override
    public Boolean recharge(String userId, String payFee, String depositor, String account, String remark, String payType, Double damaMulti, Integer rate) {
        Accountinfo userInfo = accountinfoMapper.getUserInfo(Integer.parseInt(userId));
        if (userInfo == null ||  StrUtil.isBlank(userInfo.getTNickname())) {
            return Boolean.FALSE;
        }


        SysPayOrder sysPayOrder = new SysPayOrder();
        sysPayOrder.setUserId(Integer.parseInt(userId));
        sysPayOrder.setStatus(1);

        sysPayOrder.setAccount(account);
        sysPayOrder.setChannelId(userInfo.getTChannelkey());
        sysPayOrder.setCreateBy(userInfo.getTAccountname());

        sysPayOrder.setCreateTime(LocalDateTime.now());
        sysPayOrder.setOrderNo(IdUtil.simpleUUID());
        sysPayOrder.setDepositor(depositor);

        sysPayOrder.setPoundage(new BigDecimal(0));
        Float fee = Float.parseFloat(payFee);
        sysPayOrder.setPayFee(new BigDecimal(fee));

        sysPayOrder.setHandlerType(EnumConstant.HANDLE_TYPE.RECHARGE.getValue());
        sysPayOrder.setPayName(EnumConstant.PayTypeValueOf(payType));
        sysPayOrder.setNote(remark);

        //请求接口
        GsonResult result = HttpHandler.rechargeOrder(sysPayOrder.getUserId(), sysPayOrder.getOrderNo(), sysPayOrder.getPayFee().intValue(), 0,  1, 1, damaMulti, rate);
        if (result != null && result.getRet() == 0) {
            //判断是否有返利
            if (result.getRebateCoin() != null && result.getRebateCoin().longValue() > 0) {
                SysPayRebate sysPayRebate  = packagePayRebate(sysPayOrder, new BigDecimal(result.getRebateCoin()/100));
                sysPayRebateMapper.savaRebate(sysPayRebate);
            }
            this.save(sysPayOrder);
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }


    SysPayRebate packagePayRebate(SysPayOrder sysPayOrder, BigDecimal grebateCoin) {
        SysPayRebate sysPayRebate = new SysPayRebate();
        sysPayRebate.setStatus(1);

        sysPayRebate.setUserId(sysPayOrder.getUserId());
        sysPayRebate.setChannelId(sysPayOrder.getChannelId());
        sysPayRebate.setCreateBy("SYSTME");

        sysPayRebate.setRebateType(1);
        //把分转换成元
        sysPayRebate.setRebateFee(grebateCoin);
        sysPayRebate.setOrderNo(sysPayOrder.getOrderNo());
        sysPayRebate.setCreateTime(LocalDateTime.now());
        return sysPayRebate;
    }


    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }

}

