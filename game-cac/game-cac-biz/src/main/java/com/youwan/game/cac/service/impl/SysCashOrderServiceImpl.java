package com.youwan.game.cac.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.CashOrderDto;
import com.youwan.game.cac.api.entity.SysCashOrder;
import com.youwan.game.cac.api.entity.Accountinfo;
import com.youwan.game.cac.mapper.SysCashOrderMapper;
import com.youwan.game.cac.mapper.xbqp.AccountinfoMapper;
import com.youwan.game.cac.mapper.xbqp.UserbaseattrMapper;
import com.youwan.game.cac.service.SysCashOrderService;
import com.youwan.game.common.core.constant.ConfigConstant;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.HttpHandler;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-02-20 18:19:04
 */
@Slf4j
@AllArgsConstructor
@Service("sysCashOrderService")
public class SysCashOrderServiceImpl extends ServiceImpl<SysCashOrderMapper, SysCashOrder> implements SysCashOrderService {


    private final UserbaseattrMapper userbaseattrMapper;
    private final AccountinfoMapper accountinfoMapper;


    @Override
    public IPage<List<SysCashOrder>> queryCashOrderPage(Page page, String cashNo, String userId, String status) {
        return baseMapper.queryCashOrderPage(page, cashNo, userId, status);
    }

    @Override
    public Boolean approval(String id, String status, String note) {
       //判断是审批通过还是审批打回
        String code = StrUtil.isNotBlank(status)  && new Integer(status) == ConfigConstant.ApproveStatus.FAIL_STATUS.getValue() ? "1" : "0";

        SysCashOrder order = this.getById(id);
        GsonResult result = HttpHandler.cashOrderResult(order.getUserId(), order.getCashNo(), Integer.parseInt(code), note);
        if (result != null && result.getRet() == 0) {
            //审批更改数据信息
            baseMapper.approval(getUsername(), id, status, note);
            return  Boolean.TRUE;
        } else {
            return  Boolean.FALSE;
        }
    }

    @Override
    public List<SysCashOrder> getCashOrderInfo(String userId, String cashType) {
        return baseMapper.getCashOrderInfo(userId, cashType);
    }

    @Override
    public SysCashOrder cretaeOrder(CashOrderDto cashOrderDto) {
        String channelId = "";

        if (cashOrderDto != null && StrUtil.isEmpty(cashOrderDto.getChannelId())) {
            Accountinfo accountinfo = accountinfoMapper.getCashUserInfo(Integer.parseInt(cashOrderDto.getAccountId()));
            if (accountinfo == null) {
                return null;
            }
            channelId = accountinfo.getTChannelkey();
        } else {
            channelId = cashOrderDto.getChannelId();
        }

        SysCashOrder order = new SysCashOrder();
        order.setCashNo(IdUtil.simpleUUID());
        order.setUserId(new Integer(cashOrderDto.getAccountId()));

        float fee = new Float(cashOrderDto.getExchangeMoney());
        order.setCashFee(new BigDecimal(fee/100) );
        //支付宝
        order.setCashType(Integer.parseInt(cashOrderDto.getType()));
        order.setAccount(cashOrderDto.getPayId());
        order.setAccountName(cashOrderDto.getRealName());
        order.setBank(cashOrderDto.getBank());

        order.setSubBank(cashOrderDto.getSubBank());
        if (StrUtil.isNotEmpty(cashOrderDto.getBankAddress())) {
            order.setBankAddress(cashOrderDto.getBankAddress());
        }

        order.setCreateBy(cashOrderDto.getAccountId());
        order.setCreateTime(LocalDateTime.now());
        order.setChannelId(channelId);
        order.setStatus(new Integer(ConfigConstant.CASHORDR_STATUS.APPLY.getValue()));

        this.save(order);
        return order;
    }

    /**
     * @param userId
     * @return
     */
    @Override
    public List<SysCashOrder> getOrderByUserId(String userId) {
        return baseMapper.getOrderByUserId(userId);
    }

    /**
     * 获取用户名称
     * @return username
     */
    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }
}
