package com.youwan.game.cac.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.ChanneltaskEntity;
import com.youwan.game.cac.api.entity.TaskForm;
import com.youwan.game.cac.api.vo.TaskVo;
import com.youwan.game.cac.mapper.xbqp.ChanneltaskMapper;
import com.youwan.game.cac.service.ChanneltaskService;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.jsonmodel.GsonTask;
import com.youwan.game.common.core.util.HttpHandler;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service("channeltaskService")
public class ChanneltaskServiceImpl extends ServiceImpl<ChanneltaskMapper, ChanneltaskEntity> implements ChanneltaskService {

    @Override
    public IPage<TaskVo> queryPage(Page page, QueryDto queryDto) {
        return this.baseMapper.queryPage(page, queryDto.getChannelId());
    }

    @Override
    public int channelOpenType(String channelId, String openType) {
        ChanneltaskEntity channeltask = baseMapper.getTaskByChannelId(channelId);
        if (channeltask != null && StrUtil.isNotEmpty(channeltask.getTChannel())) {

            Integer type = channeltask.getTActivityopentype() == 1 ? 0 : 1;
            String jsonStr = new String(channeltask.getTTaskinfo());
            if (StrUtil.isNotEmpty(jsonStr)) {

                GsonTask[] gsonTask = new Gson().fromJson(jsonStr, GsonTask[].class);
                GsonResult result = HttpHandler.cfgTask(channelId, type, channeltask.getTActivityopentype(), channeltask.getTBegintime(), channeltask.getTEndtime(), gsonTask);
                if (result != null) {
                    return result.getRet();
                }
            }
        }
        return 1;
    }

    @Override
    public Map<String, Object> findByChannelId(String channelId) {
        ChanneltaskEntity channelTask = baseMapper.getTaskByChannelId(channelId);
        List<Map<String, Object>> taskList = baseMapper.getTaskCfg(null);
        Map<String, Object> resultMap = new HashMap<>();

        List<Map<String, Object>> rsultList = new ArrayList<>();
        GsonTask [] gsonTask = {};
        if (channelTask != null && StrUtil.isNotEmpty(channelTask.getTChannel())) {

            String jsonStr = new String(channelTask.getTTaskinfo());
            gsonTask = new Gson().fromJson(jsonStr, GsonTask[].class);
            resultMap.put("taskInfo", channelTask);
        }

        if (CollUtil.isNotEmpty(taskList)) {
            for (Map<String, Object> map : taskList) {
                Map<String, Object> taskMap = new HashMap<>();

                taskMap.putAll(map);
                for (GsonTask task : gsonTask) {
                    if (map.get("taskId").toString().equals(task.getTaskId().toString())) {
                        taskMap.put("taskOpenType", task.getTaskOpenType());
                        taskMap.put("targetNum", task.getTargetNum());
                        taskMap.put("taskReward", task.getTaskReward());
                        taskMap.put("continueTaskCoinLimit", task.getContinueTaskCoinLimit());
                    }
                }
                rsultList.add(taskMap);
                resultMap.put("taskArr", rsultList);
            }
        }
        return resultMap;
    }


    @Override
    public int saveOrUpdateConfig(TaskForm taskForm) {
        Long beginDate = new Long(0);
        Long endDate = new Long(0);
        if (StrUtil.isNotEmpty(taskForm.getBeginTime()) && taskForm.getBeginTime().length() == 13) {
             beginDate = new Long(taskForm.getBeginTime()) /1000;
        }
        if (StrUtil.isNotEmpty(taskForm.getEndTime()) && taskForm.getEndTime().length() == 13) {
             endDate = new Long(taskForm.getEndTime()) /1000;
        }
        List<GsonTask> resultList = new ArrayList<>();
        GsonTask[] submitTaskArr = {};

        if (ArrayUtil.isNotEmpty(taskForm.getTaskArr())) {
            for (GsonTask task : taskForm.getTaskArr()) {
                if (task.getTargetNum() != null && task.getTaskReward() != null) {
                    if ( task.getTaskOpenType() == null) {
                        task.setTaskOpenType(0);
                    }
                    if ( task.getContinueTaskCoinLimit() == null) {
                        task.setContinueTaskCoinLimit(0);
                    }

                    resultList.add(task);
                }
            }
        }

        submitTaskArr = new GsonTask[resultList.size()];
        resultList.toArray(submitTaskArr);
        GsonResult result = HttpHandler.cfgTask(taskForm.getChannelId(), taskForm.getOpenType(), taskForm.getActivityTimeType(), beginDate, endDate, submitTaskArr);
        if (result != null) {
            return result.getRet();
        }
        return 1;
    }
}
