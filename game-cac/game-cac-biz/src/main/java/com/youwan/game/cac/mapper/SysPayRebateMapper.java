package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.SysPayRebate;
import org.apache.ibatis.annotations.Param;

/**
 * 
 *
 * @author code generator
 * @date 2019-06-05 14:11:47
 */
public interface SysPayRebateMapper extends BaseMapper<SysPayRebate> {

    SysPayRebate getPayRebateByChannel(@Param("channelId") String channelId);

    SysPayRebate getPayRebateByOrder(@Param("orderNo") String orderNo);

    Boolean savaRebate(@Param("model") SysPayRebate model);
}
