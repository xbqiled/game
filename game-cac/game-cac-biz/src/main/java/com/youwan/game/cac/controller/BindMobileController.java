package com.youwan.game.cac.controller;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.BindMobile;
import com.youwan.game.cac.api.entity.TChannel;
import com.youwan.game.cac.api.vo.BindVo;
import com.youwan.game.cac.service.BindMobileService;
import com.youwan.game.cac.service.TChannelService;
import com.youwan.game.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * @author code generator
 * @date 2019-04-10 21:03:25
 */
@RestController
@AllArgsConstructor
@RequestMapping("/bind")
public class BindMobileController {

    private final BindMobileService bindMobileService;

    private final TChannelService channelService;


    /**
     * 分页查询
     *
     * @param page      分页对象
     * @param channelId
     * @return
     */
    @GetMapping("/page")
    public R getBindMobilePage(Page page, String channelId) {
        return new R<>(bindMobileService.queryBindPage(page, channelId));
    }


    /**
     * 信息
     */
    @GetMapping("/{channelId}")
    public R getById(@PathVariable("channelId") String channelId) {
        BindMobile bindMobile = bindMobileService.getByChannelId(channelId);
        TChannel channel = channelService.getChannelInfo(channelId);
        BindVo vo = new BindVo();

        BeanUtil.copyProperties(bindMobile, vo);
        BeanUtil.copyProperties(channel, vo);
        return new R<>(vo);
    }

    /**
     * 修改
     */
    @PostMapping("/doSet")
    public R update(@RequestBody BindMobile bindMobile) {
        Boolean result = bindMobileService.saveOrUpdateBind(bindMobile);
        if (result) {
            return new R<>();
        } else {
            return new R<>().setCode(1).setMsg("绑定手机送金币配置失败");
        }
    }
}
