package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.DeviceInfoDto;
import com.youwan.game.cac.api.dto.VerifyDeviceInfo;
import com.youwan.game.cac.api.entity.DeviceInfo;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2019-05-15 15:24:30
 */
public interface DeviceInfoService extends IService<DeviceInfo> {


    IPage<List<DeviceInfo>> getDeviceInfoPage (Page page, DeviceInfo deviceInfo);

    /**
     * <B>添加设备信息</B>
     * @param deviceInfoDto
     * @return
     */
    Boolean addDeviceInfo(DeviceInfoDto deviceInfoDto);

    /**
     * <B>验证设备信息</B>
     * @param deviceInfoDto
     * @return
     */
    Map<String, String> verifyDeviceInfo(VerifyDeviceInfo deviceInfoDto);

}
