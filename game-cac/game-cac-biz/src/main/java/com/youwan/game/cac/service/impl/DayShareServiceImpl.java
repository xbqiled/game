package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.DayShareEntity;
import com.youwan.game.cac.api.entity.DayShareForm;
import com.youwan.game.cac.api.vo.DayShareVo;
import com.youwan.game.cac.mapper.xbqp.DayShareMapper;
import com.youwan.game.cac.service.DayShareService;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.HttpHandler;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("dayShareService")
public class DayShareServiceImpl extends ServiceImpl<DayShareMapper, DayShareEntity> implements DayShareService {

    @Override
    public IPage<DayShareVo> queryPage(Page page, QueryDto queryDto) {
        return this.baseMapper.queryPage(page, queryDto.getChannelId());
    }


    @Override
    public DayShareEntity findByChannelId(String channelId) {
        return baseMapper.getDayShareByChannelId(channelId);
    }


    @Override
    public int saveOrUpdateConfig(DayShareForm dayShareForm) {
        GsonResult result = HttpHandler.cfgDayShare(Long.valueOf(dayShareForm.getChannelId()), dayShareForm.getOpenType(), dayShareForm.getDayAward(), dayShareForm.getAwardDayShareCnt(), dayShareForm.getDataList());
        if (result != null) {
            return result.getRet();
        }
        return 1;
    }
}
