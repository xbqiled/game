package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.ReportDto;
import com.youwan.game.cac.api.entity.StatisGame;
import com.youwan.game.cac.api.vo.StatisGameVo;

import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author code generator
 * @date 2020-03-07 15:37:57
 */
public interface StatisGameService extends IService<StatisGame> {

    IPage<List<StatisGameVo>> queryStatisGamePage(Page page, ReportDto reportDto);

    IPage<List<StatisGameVo>>  queryStatisUserGamePage(Page page, ReportDto reportDto);

    List<Map<String, String>> queryGameList();

}
