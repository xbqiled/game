package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.Serverinfo;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-29 12:32:01
 */
public interface ServerinfoMapper extends BaseMapper<Serverinfo> {

}
