package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.SysPayCode;
import com.youwan.game.cac.api.vo.PayCodeVo;

/**
 * 
 *
 * @author code generator
 * @date 2019-05-10 11:23:04
 */
public interface SysPayCodeService extends IService<SysPayCode> {

    PayCodeVo getPayCodeByOrderNo(String orderNo);

}
