package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.Domain;

import java.util.List;


/**
 * 
 *
 * @author code generator
 * @date 2019-04-19 13:18:14
 */
public interface DomainService extends IService<Domain> {

    IPage<List<Domain>> queryDomainPage(Page page, Domain domain);

    Boolean saveDomain(Domain domain);

    Boolean updateDomainById(Domain domain);

    Boolean deleteDomainById(Integer id);

    List<Domain> quereyDomainByChannelId(String channelId);

}
