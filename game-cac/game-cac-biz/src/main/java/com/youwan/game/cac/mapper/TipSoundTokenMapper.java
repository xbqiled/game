package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.TipSoundToken;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-05-21 11:05:27
 */
public interface TipSoundTokenMapper extends BaseMapper<TipSoundToken> {

    IPage<List<TipSoundToken>> querySoundTokenPage(Page page, @Param("channelId") String channelId);

}
