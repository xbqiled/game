package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.Serverinfo;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-29 12:32:01
 */
public interface ServerinfoService extends IService<Serverinfo> {

}
