package com.youwan.game.cac.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.ChannelsuccourEntity;
import com.youwan.game.cac.api.entity.SuccourForm;
import com.youwan.game.cac.api.vo.SuccourVo;
import com.youwan.game.cac.mapper.xbqp.ChannelsuccourMapper;
import com.youwan.game.cac.service.ChannelsuccourService;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.jsonmodel.GsonSuccour;
import com.youwan.game.common.core.util.HttpHandler;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("channelsuccourService")
public class ChannelsuccourServiceImpl extends ServiceImpl<ChannelsuccourMapper, ChannelsuccourEntity> implements ChannelsuccourService {


    @Override
    public IPage<SuccourVo> queryPage(Page page, QueryDto queryDto) {
        return this.baseMapper.queryPage(page, queryDto.getChannelId());
    }


    @Override
    public int channelOpenType(String channelId, String openType) {
        ChannelsuccourEntity channelSuccour = baseMapper.getSuccourByChannelId(channelId);
        if (channelSuccour != null && StrUtil.isNotEmpty(channelSuccour.getTChannel())) {

            Integer type = channelSuccour.getTOpentype() == 1 ? 0 : 1;
            String jsonStr = new String(channelSuccour.getTSuccourinfo());
            if (StrUtil.isNotEmpty(jsonStr)) {
                GsonSuccour gsonSuccour = new Gson().fromJson(jsonStr, GsonSuccour.class);
                GsonResult result = HttpHandler.cfgSuccour(channelId, type, gsonSuccour.getCoinLimit(), gsonSuccour.getSuccourAward(),
                        gsonSuccour.getTotalSuccourCnt(), gsonSuccour.getShareConditionCnt());
                if (result != null) {
                    return result.getRet();
                }
            }
        }
        return 1;
    }


    @Override
    public GsonSuccour findByChannelId(String channelId) {
        ChannelsuccourEntity channelSuccour = baseMapper.getSuccourByChannelId(channelId);
        if (channelSuccour != null && StrUtil.isNotEmpty(channelSuccour.getTChannel())) {
            String jsonStr = new String(channelSuccour.getTSuccourinfo());
            if (StrUtil.isNotEmpty(jsonStr)) {
                GsonSuccour gsonSuccour = new Gson().fromJson(jsonStr, GsonSuccour.class);
                gsonSuccour.setOpenType(channelSuccour.getTOpentype());
                gsonSuccour.setChannel(channelSuccour.getTChannel());
                return gsonSuccour;
            }
        }
        return null;
    }


    @Override
    public int saveOrUpdateConfig(SuccourForm succourForm) {
        GsonResult result = HttpHandler.cfgSuccour(succourForm.getChannelId(), succourForm.getOpenType(), succourForm.getCoinLimit(), succourForm.getSuccourAward(), succourForm.getTotalSuccourCnt(), succourForm.getShareConditionCnt());
        if (result != null) {
            return result.getRet();
        }
        return 1;
    }
}
