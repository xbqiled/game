package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.ChannelAccount;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-19 14:07:23
 */
public interface OtherChannelAccountMapper extends BaseMapper<ChannelAccount> {

    IPage<List<ChannelAccount>> queryChannelAccountList(Page page, @Param("channelId") String channelId);

    ChannelAccount getByChannelId( @Param("channelId") String channelId);

    Integer updateIncreaseBgBalance( @Param("channelId") String channelId,  @Param("amount") BigDecimal amount);

    Integer updateReduceBgBalance( @Param("channelId") String channelId,  @Param("amount") BigDecimal amount);
}
