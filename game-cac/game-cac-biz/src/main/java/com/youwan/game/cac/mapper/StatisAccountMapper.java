package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.StatisAccount;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2019-06-16 12:32:18
 */
public interface StatisAccountMapper extends BaseMapper<StatisAccount> {

    @SqlParser(filter=true)
    IPage<List<Map<String, Object>>> queryStatisAccountDataPage(Page page,
                                                                @Param("table") String table,
                                                                @Param("channelId") String channelId,
                                                                @Param("userId") String userId,
                                                                @Param("startTime") String startTime,
                                                                @Param("endTime") String endTime);

    Map<String, Object> queryStatiAccountData(@Param("table") String table, @Param("channelId") String channelId, @Param("userId") String userId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    List<String> queryHaveTab(@Param("tableSql") String tableSql);

    IPage<List<StatisAccount>> queryStatisAccountPage(Page page, @Param("tableName") String tableName,
                                                     @Param("channelId") String channelId,
                                                     @Param("userId") String userId,
                                                     @Param("startTime") String startTime,
                                                     @Param("endTime") String endTime);
}
