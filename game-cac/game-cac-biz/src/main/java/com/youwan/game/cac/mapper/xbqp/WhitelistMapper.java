package com.youwan.game.cac.mapper.xbqp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.Whitelist;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-29 13:01:34
 */
public interface WhitelistMapper extends BaseMapper<Whitelist> {

    IPage<Map<String, Object>> queryPageList(Page page, @Param("userId")String userId);

    long queryCount(@Param("userId")String userId);

    Whitelist findById(@Param("userId")String userId);
}
