package com.youwan.game.cac.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.ChlAccountRecordDto;
import com.youwan.game.cac.service.OtherAccountRecordService;
import com.youwan.game.common.core.util.R;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;



/**
 * 
 *
 * @author code generator
 * @date 2019-11-20 13:02:18
 */
@RestController
@AllArgsConstructor
@RequestMapping("/accountrecord")
public class AccountRecordController {

  private final OtherAccountRecordService otherAccountRecordService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param chlAccountRecordDto
   * @return
   */
  @GetMapping("/page")
  public R getOtherAccountRecordPage(Page page, ChlAccountRecordDto chlAccountRecordDto) {
    return  new R<>(otherAccountRecordService.queryAccountRecordPage(page, chlAccountRecordDto.getChannelId(), chlAccountRecordDto.getStartTime(), chlAccountRecordDto.getEndTime()));
  }

}
