package com.youwan.game.cac.controller;


import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.InviteDepositDto;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.vo.InviteDepositChannelVo;
import com.youwan.game.cac.service.InviteDepositService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * VIEW
 *
 * @author code generator
 * @date 2019-08-03 15:31:04
 */
@RestController
@AllArgsConstructor
@RequestMapping("/invitedeposit")
public class InviteDepositController {

    private final InviteDepositService inviteDepositService;

    /**
     * 分页查询
     * @param page          分页对象
     * @param queryDto VIEW
     * @return
     */
    @GetMapping("/page")
    public R getInviteDepositPage(Page page, QueryDto queryDto) {
        return new R<>(inviteDepositService.queryInviteDepositPage(page, queryDto));
    }

    @RequestMapping("/info")
    @PreAuthorize("@pms.hasPermission('cac_invite_view')")
    public R info(@RequestParam Map<String, Object> params) {
        String channelId = ObjectUtil.isNotNull(params.get("channelId")) ? (String) params.get("channelId") : "";
        InviteDepositChannelVo inviteDeposit = inviteDepositService.findByChannelId(channelId);
        return new R<>(inviteDeposit);
    }

    @SysLog("代理累计充值配置信息保存")
    @RequestMapping("/doConfig")
    @PreAuthorize("@pms.hasPermission('cac_invite_add')")
    public R doConfig(@RequestBody InviteDepositDto inviteDepositDto){
        if (inviteDepositDto.getTimeType() == 1) {
            if (inviteDepositDto.getBeginTime() == 0) {
                return new R<>().setCode(1).setMsg("开始时间不能为空!");
            }
            if (inviteDepositDto.getEndTime() == 0) {
                return new R<>().setCode(1).setMsg("结束时间不能为空!");
            }
            if (inviteDepositDto.getBeginTime() == inviteDepositDto.getEndTime()) {
                return new R<>().setCode(1).setMsg("活动开始时间与活动结束时间不能相同!");
            }
        }

        int result = inviteDepositService.saveInviteDepositCfg(inviteDepositDto.getChannelId(), Long.valueOf(1), inviteDepositDto.getTimeType(), inviteDepositDto.getBeginTime(),
                inviteDepositDto.getEndTime(), inviteDepositDto.getFirstTotalDeposit(), inviteDepositDto.getSelfReward(), inviteDepositDto.getUpperReward());
        if (result == 0) {
            return new R<>();
        } else {
            return new R<>().setCode(1).setMsg("配置代理充值返利失败!");
        }
    }
}
