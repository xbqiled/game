package com.youwan.game.cac.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.TipSoundConfig;
import com.youwan.game.cac.service.TipSoundConfigService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-05-21 11:11:20
 */
@RestController
@AllArgsConstructor
@RequestMapping("/soundconfig")
public class TipSoundConfigController {

  private final TipSoundConfigService tipSoundConfigService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param tipSoundConfig 
   * @return
   */
  @GetMapping("/page")
  public R getTipSoundConfigPage(Page page, TipSoundConfig tipSoundConfig) {
    return  new R<>(tipSoundConfigService.querySoundConfigPage(page, tipSoundConfig.getChannelId()));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(tipSoundConfigService.getById(id));
  }

  /**
   * 新增
   * @param tipSoundConfig 
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody TipSoundConfig tipSoundConfig){
    TipSoundConfig result  = tipSoundConfigService.findByChannelId(tipSoundConfig.getChannelId());
    if (result != null && StrUtil.isNotBlank(result.getChannelId())) {
      return new R<>().setCode(1).setMsg("已经存在对应的渠道配置");
    } else {
      return new R<>(tipSoundConfigService.addSoundConfig(tipSoundConfig));
    }
  }

  /**
   * 修改
   * @param tipSoundConfig 
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody TipSoundConfig tipSoundConfig){
    return new R<>(tipSoundConfigService.updateById(tipSoundConfig));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(tipSoundConfigService.removeById(id));
  }

}
