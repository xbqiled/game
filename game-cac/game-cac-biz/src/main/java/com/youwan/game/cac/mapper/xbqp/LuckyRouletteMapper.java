package com.youwan.game.cac.mapper.xbqp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.LuckyRoulette;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-22 15:14:43
 */
public interface LuckyRouletteMapper extends BaseMapper<LuckyRoulette> {

    IPage<List<Map<String, Object>>> queryLuckyRoulettePage(Page page, @Param("channelId") String channelId , @Param("startTime") String startTime, @Param("endTime") String endTime);

}
