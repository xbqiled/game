package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.OtherBgUser;
import com.youwan.game.cac.api.entity.OtherPlatformConfig;
import com.youwan.game.cac.api.vo.InviteDepositChannelVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-05 15:57:07
 */
public interface OtherBgUserMapper extends BaseMapper<OtherBgUser> {

    OtherBgUser getBgUser(@Param("channelId") String channelId, @Param("userId") String userId);

    IPage<List<OtherBgUser>> queryBgUserPage(Page page, @Param("userId") String userId, @Param("channelId") String channelId , @Param("nickName")String nickName);

    Integer changeStatus(@Param("status") String status, @Param("userId") String userId);

}
