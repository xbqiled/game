package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.BgenterConfig;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-12 14:04:32
 */
public interface BgenterConfigMapper extends BaseMapper<BgenterConfig> {

    IPage<List<BgenterConfig>> queryEnterGameConfigPage(Page page, @Param("channelId") String channelId);

    BgenterConfig getConfigByChannelId(@Param("channelId") String channelId);

}
