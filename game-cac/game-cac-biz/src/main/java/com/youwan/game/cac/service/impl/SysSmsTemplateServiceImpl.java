package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.SysSmsTemplate;
import com.youwan.game.cac.mapper.SysSmsTemplateMapper;
import com.youwan.game.cac.service.SysSmsTemplateService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * 
 *
 * @author code generator
 * @date 2019-02-17 18:01:23
 */
@Service("sysSmsTemplateService")
public class SysSmsTemplateServiceImpl extends ServiceImpl<SysSmsTemplateMapper, SysSmsTemplate> implements SysSmsTemplateService {


    @Override
    @Caching(evict={@CacheEvict(value = "smsTemplate:details", allEntries = true), @CacheEvict(value = "smsTemplate:list", allEntries = true)})
    public Boolean saveSmsTemplate(SysSmsTemplate sysSmsTemplate) {
        sysSmsTemplate.setCreateTime(LocalDateTime.now());
        sysSmsTemplate.setCreateBy(Objects.requireNonNull(getUsername()));

        this.save(sysSmsTemplate);
        return Boolean.TRUE;
    }

    @Override
    @Caching(evict={@CacheEvict(value = "smsTemplate:details", allEntries = true), @CacheEvict(value = "smsTemplate:list", allEntries = true)})
    public Boolean updateSmsTemplate(SysSmsTemplate sysSmsTemplate) {
        this.updateById(sysSmsTemplate);
        return Boolean.TRUE;
    }

    @Override
    @Caching(evict={@CacheEvict(value = "smsTemplate:details", allEntries = true), @CacheEvict(value = "smsTemplate:list", allEntries = true)})
    public Boolean delSmsTemplate(Integer id) {
        this.removeById(id);
        return Boolean.TRUE;
    }

    @Override
    public IPage<List<SysSmsTemplate>> queryTemplatePage(Page page, String templateCode, String templateName, String channelId) {
        return baseMapper.queryTemplatePage(page, templateCode, templateName, channelId);
    }

    @Override
    @CacheEvict(value = "smsTemplate:details", key = "#channelId +'_'+ #serverCode +'_'+ #type")
    public SysSmsTemplate findByServerCode(String serverCode, Integer type, String channelId) {
        return baseMapper.findByServerCode(serverCode, type, channelId);
    }

    @Override
    @CacheEvict(value = "smsTemplate:list", key = "#channelId + '_'+#type")
    public List<SysSmsTemplate> queryTemplateByChannel(Integer type, String channelId) {
        return baseMapper.queryTemplateByChannel(type, channelId);
    }

    /**
     * 获取用户名称
     * @return username
     */
    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }
}
