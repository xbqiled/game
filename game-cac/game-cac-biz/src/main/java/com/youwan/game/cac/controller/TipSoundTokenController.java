package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.TipSoundToken;
import com.youwan.game.cac.service.TipSoundTokenService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-05-21 11:05:27
 */
@RestController
@AllArgsConstructor
@RequestMapping("/soundtoken")
public class TipSoundTokenController {

  private final TipSoundTokenService tipSoundTokenService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param tipSoundToken 
   * @return
   */
  @GetMapping("/page")
  public R getTipSoundTokenPage(Page page, TipSoundToken tipSoundToken) {
    return  new R<>(tipSoundTokenService.querySoundTokenPage(page, tipSoundToken.getChannelId()));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(tipSoundTokenService.getById(id));
  }

  /**
   * 新增
   * @param tipSoundToken 
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody TipSoundToken tipSoundToken){
    return new R<>(tipSoundTokenService.save(tipSoundToken));
  }

  /**
   * 修改
   * @param tipSoundToken 
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody TipSoundToken tipSoundToken){
    return new R<>(tipSoundTokenService.updateById(tipSoundToken));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(tipSoundTokenService.removeById(id));
  }

}
