package com.youwan.game.cac.mapper.xbqp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.GameCtrlCfgStr;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author code generator
 * @date 2020-04-14 19:28:20
 */
public interface GameCtrlCfgStrMapper extends BaseMapper<GameCtrlCfgStr> {

    IPage<Map<String, Object>> queryGameCtrlCfgStrPage(Page page, @Param("startTime") String channelId, @Param("endTime") String endTime);

    GameCtrlCfgStr getByKeyId(@Param("keyId") String keyId);

    List<Map<String, Object>> getGameList();
}
