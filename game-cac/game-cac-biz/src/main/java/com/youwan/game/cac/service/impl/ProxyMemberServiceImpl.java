package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.generator.config.IFileCreate;
import com.google.common.collect.ImmutableList;
import com.youwan.game.cac.api.dto.ProxyMemberQueryDto;
import com.youwan.game.cac.api.entity.*;
import com.youwan.game.cac.service.ProxyMemberService;
import com.youwan.game.common.data.core.Page;
import com.youwan.game.common.data.core.XSort;
import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import com.youwan.game.common.data.elasticsearch.operations.GetOperation;
import com.youwan.game.common.data.elasticsearch.operations.SearchOperation;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.collect.MapBuilder;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.script.Script;
import org.elasticsearch.search.aggregations.bucket.range.Range;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.Cardinality;
import org.elasticsearch.search.aggregations.metrics.Stats;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;
import static org.elasticsearch.script.Script.DEFAULT_SCRIPT_LANG;
import static org.elasticsearch.script.Script.DEFAULT_SCRIPT_TYPE;
import static org.elasticsearch.search.aggregations.AggregationBuilders.*;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-04-22.
 */
@Service
public class ProxyMemberServiceImpl implements ProxyMemberService {

    private ElasticsearchTemplate elasticsearchTemplate;

    public ProxyMemberServiceImpl(ElasticsearchTemplate elasticsearchTemplate) {
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    @Override
    public ProxyMemberProfile getMemberProfile(Long accountId) {
        ProxyMember me = elasticsearchTemplate.get(new GetOperation<>("account_info", "_doc", accountId.toString(), ProxyMember.class)
                .setFetchSourceContext(new FetchSourceContext(true, new String[]{"pid"}, null)));
        if (me == null)
            return null;
        ProxyMemberProfile profile = new ProxyMemberProfile();
        profile.setMyId(accountId);
        profile.setPid(me.getPid());
        //统计人数
        Long immediateMembers = elasticsearchTemplate.count(new SearchOperation<>("account_info", termQuery("pid", accountId)));
        Long allMembers = elasticsearchTemplate.count(new SearchOperation<>("account_info", termQuery("m_path.keyword", accountId)));
        profile.setImmediateMembers(immediateMembers);
        profile.setAllMembers(allMembers);
        DateTime toDay = DateTime.now().withTime(0, 0, 0, 0);
        long todayStart = toDay.toDate().getTime() / 1000;
        long todayEnd = toDay.plusDays(1).toDate().getTime() / 1000;

        long yestedayStart = toDay.minusDays(1).toDate().getTime() / 1000;
        //long yestedayEnd = todayStart;

        long todayImmMembers = elasticsearchTemplate.count(new SearchOperation<>("user_relation",
                boolQuery().must(termQuery("pid", accountId)).must(rangeQuery("promoteTime").gte(todayStart).lt(todayEnd))
        ));
        long todayMembers = elasticsearchTemplate.count(new SearchOperation<>("user_relation",
                boolQuery().must(termQuery("m_path.keyword", accountId)).must(rangeQuery("promoteTime").gte(todayStart).lt(todayEnd))
        ));
        profile.setTodayImmMembers(todayImmMembers);
        profile.setTodayMembers(todayMembers);
        //今日佣金
        BoolQueryBuilder toDayRebateQuery = addTimeRangeQueryIfNeed(boolQuery().must(termQuery("promoterID", accountId)),
                todayStart, todayEnd, "recordTime");
        Double toDayRebate = elasticsearchTemplate.sum(new SearchOperation<>("record_game_rebate_detail", toDayRebateQuery), "count");

        //昨日佣金
        BoolQueryBuilder yestedayRebateQuery = addTimeRangeQueryIfNeed(boolQuery().must(termQuery("promoterID", accountId)),
                yestedayStart, todayStart, "recordTime");
        Double yestedayRebate = elasticsearchTemplate.sum(new SearchOperation<>("record_game_rebate_detail", yestedayRebateQuery), "count");
        profile.setTodayRebate(toDayRebate);
        profile.setYesterdayRebate(yestedayRebate);

        //历史总佣金
        Double totalRebate = elasticsearchTemplate.sum(new SearchOperation<>("record_game_rebate_detail", termQuery("promoterID", accountId)),
                "count");
        profile.setTotalRebate(totalRebate);
        //已提取佣金
        Double balanceRebate = elasticsearchTemplate.sum(new SearchOperation<>("record_game_rebate_balance", termQuery("accountId", accountId)),
                "count");
        //可提取佣金
        profile.setWithdrawableRebate(new BigDecimal(totalRebate).subtract(new BigDecimal(balanceRebate)).setScale(2, RoundingMode.HALF_UP).doubleValue());

        return profile;
    }


    @Override
    public Page<ProxyMember> getDirectSubordinates(Long accountId, Long memberId, int offset, int limit) {
        BoolQueryBuilder query = QueryBuilders.boolQuery().must(termQuery("pid", accountId));
        if (!Objects.isNull(memberId))
            query.must(termQuery("_id", memberId));
        Page<ProxyMember> _page = elasticsearchTemplate.searchForPage(new SearchOperation<>("account_info", "_doc",
                query, ProxyMember.class).setFrom(offset).setSize(limit));
        _page.getRecords().forEach(member -> {
            member.setDeep(1);
            ProxyMemberQueryDto subDto = new ProxyMemberQueryDto();
            subDto.setUserId(Long.valueOf(member.getId()));
            ProxyMemberTeamMetrics metrics = queryTeamMetrics(subDto);
            member.setTeamMetrics(metrics);
        });
        return _page;
    }

    @Override
    public Page<ProxyMember> getAllSubordinates(Long accountId, int deep, int offset, int limit) {

        ProxyMember self = elasticsearchTemplate.get("account_info", "_doc", accountId + "", ProxyMember.class);
        if (Objects.isNull(self))
            return Page.empty();
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery().must(termQuery("m_path", accountId));
        if (deep > 0) {
            boolQueryBuilder.must(scriptQuery(new Script(DEFAULT_SCRIPT_TYPE, DEFAULT_SCRIPT_LANG,
                    "doc['m_path.keyword'].length == params.length",
                    MapBuilder.newMapBuilder(new HashMap<String, Object>()).put("length",
                            self.getM_path().size() + deep).immutableMap())));
        }
        Page<ProxyMember> _page = elasticsearchTemplate.searchForPage(new SearchOperation<>("account_info", "_doc",
                boolQueryBuilder, ProxyMember.class).setFrom(offset).setSize(limit));
        _page.getRecords().forEach(member -> member.setDeep(calculateDeep(accountId, member.getM_path())));
        return _page;
    }

    @Override
    public Page<ProxyMemberTreasureChange> queryProxyMemberTreasureChange(ProxyMemberQueryDto queryDto) {
        ProxyMember self = elasticsearchTemplate.get("account_info", "_doc", queryDto.getUserId() + "",
                ProxyMember.class);
        if (Objects.isNull(self))
            return Page.empty();
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery().must(termQuery("m_path.keyword", queryDto.getUserId()));
        if (queryDto.getBusinessType() != null) {
            boolQuery.must(termQuery("businessType", queryDto.getBusinessType()));
        }
        if (queryDto.getMemberId() != null) {
            boolQuery.must(termQuery("userID", queryDto.getMemberId()));
        }
        if (queryDto.getDeep() > 0) {
            boolQuery.must(scriptQuery(new Script(DEFAULT_SCRIPT_TYPE, DEFAULT_SCRIPT_LANG,
                    "doc['m_path.keyword'].length == params.length",
                    MapBuilder.newMapBuilder(new HashMap<String, Object>()).put("length",
                            self.getM_path().size() + queryDto.getDeep()).immutableMap())));
        }
        if (queryDto.getStart() != null) {
            RangeQueryBuilder rangeQuery = rangeQuery("recordTime").gte(queryDto.getStart());
            if (queryDto.getEnd() != null) {
                rangeQuery.lte(queryDto.getEnd());
            }
            boolQuery.must(rangeQuery);
        }
        Page<ProxyMemberTreasureChange> _page = elasticsearchTemplate.searchForPage(new SearchOperation<>(
                "user_treasure_change", "_doc", boolQuery, ProxyMemberTreasureChange.class)
                .setSort(XSort.desc("recordTime")).setFrom(queryDto.getOffset()).setSize(queryDto.getLimit()));
        _page.getRecords().forEach(treasureChange -> treasureChange.setDeep(calculateDeep(queryDto.getUserId(),
                treasureChange.getM_path())));
        return _page;
    }

    @Override
    public Page<ProxyMemberRechargeOrder> queryProxyMemberRechargeOrders(ProxyMemberQueryDto queryDto) {
        ProxyMember self = elasticsearchTemplate.get("account_info", "_doc", queryDto.getUserId() + "",
                ProxyMember.class);
        if (Objects.isNull(self))
            return Page.empty();
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery().must(termQuery("m_path.keyword", queryDto.getUserId()));
        if (queryDto.getDeep() > 0) {
            boolQueryBuilder.must(scriptQuery(new Script(DEFAULT_SCRIPT_TYPE, DEFAULT_SCRIPT_LANG,
                    "doc['m_path.keyword'].length == params.length",
                    MapBuilder.newMapBuilder(new HashMap<String, Object>()).put("length",
                            self.getM_path().size() + queryDto.getDeep()).immutableMap())));
        }
        if (queryDto.getStart() != null) {
            RangeQueryBuilder rangeQuery = rangeQuery("recordTime").gte(queryDto.getStart());
            if (queryDto.getEnd() != null) {
                rangeQuery.lte(queryDto.getEnd());
            }
            boolQueryBuilder.must(rangeQuery);
        }
        Page<ProxyMemberRechargeOrder> _page = elasticsearchTemplate.searchForPage(new SearchOperation<>(
                "recharge_platform_order", "_doc", boolQueryBuilder, ProxyMemberRechargeOrder.class)
                .setSort(XSort.desc("recordTime")).setFrom(queryDto.getOffset()).setSize(queryDto.getLimit()));
        _page.getRecords().forEach(rechargeOrder -> rechargeOrder.setDeep(calculateDeep(queryDto.getUserId(),
                rechargeOrder.getM_path())));
        return _page;
    }

    @Override
    public Page<ProxyMemberCashOrder> queryProxyMemberCashOrders(ProxyMemberQueryDto queryDto) {
        ProxyMember self = elasticsearchTemplate.get("account_info", "_doc", queryDto.getUserId() + "",
                ProxyMember.class);
        if (Objects.isNull(self))
            return Page.empty();
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery().must(termQuery("m_path.keyword", queryDto.getUserId()));
        if (queryDto.getDeep() > 0) {
            boolQueryBuilder.must(scriptQuery(new Script(DEFAULT_SCRIPT_TYPE, DEFAULT_SCRIPT_LANG,
                    "doc['m_path.keyword'].length == params.length",
                    MapBuilder.newMapBuilder(new HashMap<String, Object>()).put("length",
                            self.getM_path().size() + queryDto.getDeep()).immutableMap())));
        }
        if (queryDto.getStart() != null) {
            //DateUtil.format(new Date(queryDto.getStart() * 1000),"yyyy-MM-dd HH:mm:ss");
            RangeQueryBuilder rangeQuery = rangeQuery("create_time").gte(queryDto.getStart() * 1000);
            if (queryDto.getEnd() != null) {
                rangeQuery.lte(queryDto.getEnd() * 1000);
            }
            boolQueryBuilder.must(rangeQuery);
        }
        Page<ProxyMemberCashOrder> _page = elasticsearchTemplate.searchForPage(new SearchOperation<>(
                "sys_cash_order", "_doc", boolQueryBuilder, ProxyMemberCashOrder.class)
                .setSort(XSort.desc("create_time")).setFrom(queryDto.getOffset()).setSize(queryDto.getLimit()));
        _page.getRecords().forEach(rechargeOrder -> rechargeOrder.setDeep(calculateDeep(queryDto.getUserId(),
                rechargeOrder.getM_path())));
        return _page;
    }

    @Override
    public Page<ProxyMemberGameScore> queryProxyMemberGameScores(ProxyMemberQueryDto queryDto) {
        ProxyMember self = elasticsearchTemplate.get("account_info", "_doc", queryDto.getUserId() + "",
                ProxyMember.class);
        if (Objects.isNull(self))
            return Page.empty();
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        if (queryDto.getDeep() == -1) {
            boolQueryBuilder.must(termQuery("userID", queryDto.getUserId()));
        } else {
            boolQueryBuilder.must(termQuery("m_path.keyword", queryDto.getUserId()));
        }
        if (Strings.hasText(queryDto.getGame()))
            boolQueryBuilder.must(termQuery("game.keyword", queryDto.getGame()));
        if (queryDto.getDeep() > 0) {
            boolQueryBuilder.must(scriptQuery(new Script(DEFAULT_SCRIPT_TYPE, DEFAULT_SCRIPT_LANG,
                    "doc['m_path.keyword'].length == params.length",
                    MapBuilder.newMapBuilder(new HashMap<String, Object>()).put("length",
                            self.getM_path().size() + queryDto.getDeep()).immutableMap())));
        }
        if (queryDto.getMemberId() != null) {
            boolQueryBuilder.must(termQuery("userID", queryDto.getMemberId()));
        }
        if (queryDto.getStart() != null) {
            RangeQueryBuilder rangeQuery = rangeQuery("endTime").gte(queryDto.getStart());
            if (queryDto.getEnd() != null) {
                rangeQuery.lte(queryDto.getEnd());
            }
            boolQueryBuilder.must(rangeQuery);
        }
        Page<ProxyMemberGameScore> _page = elasticsearchTemplate.searchForPage(new SearchOperation<>(
                "record_game_score", "_doc", boolQueryBuilder, ProxyMemberGameScore.class)
                .setSort(XSort.desc("endTime")).setFrom(queryDto.getOffset()).setSize(queryDto.getLimit()));
        _page.getRecords().forEach(rechargeOrder ->
                rechargeOrder.setDeep(calculateDeep(queryDto.getUserId(), rechargeOrder.getM_path())));
        return _page;
    }


    @Override
    public List<ProxyMemberGameScoreStats> queryProxyMemberGameScoreStats(ProxyMemberQueryDto queryDto) {
        ProxyMember self = elasticsearchTemplate.get("account_info", "_doc", queryDto.getUserId().toString(),
                ProxyMember.class);
        if (Objects.isNull(self))
            return ImmutableList.of();
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        if (queryDto.getStart() != null) {
            RangeQueryBuilder rangeQuery = rangeQuery("endTime").gte(queryDto.getStart());
            if (queryDto.getEnd() != null) {
                rangeQuery.lte(queryDto.getEnd());
            }
            boolQueryBuilder.must(rangeQuery);
        }
        if (queryDto.getDeep() == -1) {
            boolQueryBuilder.must(termQuery("userID", queryDto.getUserId()));
        } else {
            boolQueryBuilder.must(termQuery("m_path.keyword", queryDto.getUserId()));
        }
        if (queryDto.getMemberId() != null) {
            boolQueryBuilder.must(termQuery("userID", queryDto.getMemberId()));
        }
        if (queryDto.getDeep() > 0) {
            boolQueryBuilder.must(scriptQuery(new Script(DEFAULT_SCRIPT_TYPE, DEFAULT_SCRIPT_LANG,
                    "doc['m_path.keyword'].length == params.length",
                    MapBuilder.newMapBuilder(new HashMap<String, Object>()).put("length",
                            self.getM_path().size() + queryDto.getDeep()).immutableMap())));
        }

        SearchRequest searchRequest = new SearchRequest("record_game_score");
        SearchSourceBuilder source = new SearchSourceBuilder().query(boolQueryBuilder)
                .size(0).aggregation(terms("games").field("game.keyword").size(200)
                        .subAggregation(stats("stats").field("score"))
                        .subAggregation(cardinality("players").field("userID"))
                        .subAggregation(range("loseOrWin").field("score").keyed(true)
                                .addUnboundedTo("lose", 0).addUnboundedFrom("win", 0))
                );

        try {
            SearchResponse response = elasticsearchTemplate.getClient().search(searchRequest.source(source), RequestOptions.DEFAULT);
            Terms gamesAgg = response.getAggregations().get("games");
            return gamesAgg.getBuckets().stream().map(bkt -> {
                ProxyMemberGameScoreStats stats = new ProxyMemberGameScoreStats();
                stats.setGame(bkt.getKey().toString());
                stats.setBoards(bkt.getDocCount());
                stats.setPlayers(((Cardinality) bkt.getAggregations().get("players")).getValue());
                Stats statsAgg = bkt.getAggregations().get("stats");
                Range rangeAgg = bkt.getAggregations().get("loseOrWin");
                String scores = new BigDecimal(statsAgg.getSum()).setScale(2, RoundingMode.HALF_UP).toPlainString();
                stats.setScores(Double.valueOf(scores));
                stats.setAvgScore(new BigDecimal(statsAgg.getAvg()).setScale(2, RoundingMode.HALF_UP).doubleValue());
                stats.setMaxScore(new BigDecimal(statsAgg.getMax()).setScale(2, RoundingMode.HALF_UP).doubleValue());
                stats.setMinScore(new BigDecimal(statsAgg.getMin()).setScale(2, RoundingMode.HALF_UP).doubleValue());
                stats.setWinBoards(rangeAgg.getBuckets().stream().filter(b -> b.getKeyAsString().equals("win")).findFirst().get().getDocCount());
                stats.setLoseBoards(rangeAgg.getBuckets().stream().filter(b -> b.getKeyAsString().equals("lose")).findFirst().get().getDocCount());
                return stats;
            }).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ImmutableList.of();
    }

    @Override
    public ProxyMemberTeamMetrics queryTeamMetrics(ProxyMemberQueryDto queryDto) {
        Long uid = queryDto.getUserId();
        ProxyMemberTeamMetrics teamMetrics = new ProxyMemberTeamMetrics();
        //统计人数
        Long immediateMembers = elasticsearchTemplate.count(new SearchOperation<>("account_info", termQuery("pid", uid)));
        Long allMembers = elasticsearchTemplate.count(new SearchOperation<>("account_info", termQuery("m_path.keyword", uid)));
        teamMetrics.setImmediateMembers(immediateMembers);
        teamMetrics.setAllMembers(allMembers);

        //充值金额
        BoolQueryBuilder rechargeQuery = addTimeRangeQueryIfNeed(boolQuery().must(termQuery("m_path.keyword", uid)),
                queryDto.getStart(), queryDto.getEnd(), "recordTime");
        Double totalRecharge = elasticsearchTemplate.sum(new SearchOperation<>("recharge_platform_order", rechargeQuery), "payAmount");

        //提现金额
        BoolQueryBuilder cashQuery = addTimeRangeQueryIfNeed(boolQuery().must(termQuery("m_path.keyword", uid)).must(termQuery("status", 1)),
                queryDto.getStart() == null ? null : queryDto.getStart() * 1000,
                queryDto.getEnd() == null ? null : queryDto.getEnd() * 1000, "create_time");
        Double totalCash = elasticsearchTemplate.sum(new SearchOperation<>("sys_cash_order", cashQuery), "cash_fee");

        //打码
        BoolQueryBuilder stakeQuery = addTimeRangeQueryIfNeed(boolQuery().must(termQuery("m_path.keyword", uid)),
                queryDto.getStart(), queryDto.getEnd(), "endTime");
        Double totalStake = elasticsearchTemplate.sum(new SearchOperation<>("record_game_score", stakeQuery), "stake");

        //返佣
        BoolQueryBuilder rebateQuery = addTimeRangeQueryIfNeed(boolQuery().must(termQuery("promoterID", uid)),
                queryDto.getStart(), queryDto.getEnd(), "recordTime");
        Double totalRebate = elasticsearchTemplate.sum(new SearchOperation<>("record_game_rebate_detail", rebateQuery), "count");

        teamMetrics.setTotalRecharge(totalRecharge);
        teamMetrics.setTotalCashFee(totalCash);
        teamMetrics.setTotalStake(totalStake);
        teamMetrics.setTotalRebate(totalRebate);
        return teamMetrics;
    }

    @Override
    public Page<ProxyMemberTeamMetrics> querySubordinateTeamMetrics(ProxyMemberQueryDto queryDto) {
        Long uid = queryDto.getUserId();
        ProxyMemberTeamMetrics teamMetrics = new ProxyMemberTeamMetrics();
        BoolQueryBuilder query = boolQuery().must(termQuery("m_path.keyword", uid)).must(rangeQuery("pid").gt(0));
        if (queryDto.getMemberId() != null) {
            query.must(termQuery("_id", queryDto.getMemberId()));
        }
        Page<ProxyMember> memberPage = elasticsearchTemplate.searchForPage(
                new SearchOperation<>("account_info", query, ProxyMember.class).setFrom(queryDto.getOffset()).setSize(queryDto.getLimit()));

        List<ProxyMemberTeamMetrics> metricsList = memberPage.getRecords().stream().map(m -> {
            ProxyMemberQueryDto subDto = new ProxyMemberQueryDto();
            subDto.setUserId(Long.valueOf(m.getId()));
            subDto.setStart(queryDto.getStart());
            subDto.setEnd(queryDto.getEnd());
            ProxyMemberTeamMetrics metrics = queryTeamMetrics(subDto);
            metrics.setCaptainId(Long.valueOf(m.getId()));
            metrics.setCaptainNickName(m.getT_nickName());
            metrics.setDeep(calculateDeep(queryDto.getUserId(), m.getM_path()));
            return metrics;
        }).collect(Collectors.toList());
        return Page.newPage(memberPage.getTotal(), metricsList);
    }

    @Override
    public Page<ProxyMemberRebateDetail> queryRebateDetails(ProxyMemberQueryDto queryDto) {
        BoolQueryBuilder query = addTimeRangeQueryIfNeed(boolQuery().must(termQuery("promoterID", queryDto.getUserId())), queryDto.getStart(),
                queryDto.getEnd(), "recordTime");
        if (queryDto.getMemberId() != null) {
            query.must(termQuery("accountId", queryDto.getMemberId()));
        }
        System.err.println(query);
        Page<ProxyMemberRebateDetail> _page = elasticsearchTemplate.searchForPage(new SearchOperation<>("record_game_rebate_detail", query,
                ProxyMemberRebateDetail.class).setSort(XSort.desc("recordTime")).setFrom(queryDto.getOffset()).setSize(queryDto.getLimit()));
        double totalRebate = elasticsearchTemplate.sum(new SearchOperation<>("record_game_rebate_detail", query), "count");
        _page.setAttach(MapBuilder.newMapBuilder(new HashMap<String, Object>()).put("totalRebate", totalRebate).immutableMap());
        return _page;
    }

    @Override
    public Page<ProxyMemberRebateBalance> queryRebateBalances(ProxyMemberQueryDto queryDto) {
        BoolQueryBuilder query = addTimeRangeQueryIfNeed(boolQuery().must(termQuery("accountId", queryDto.getUserId())), queryDto.getStart(),
                queryDto.getEnd(), "recordTime");
        Page<ProxyMemberRebateBalance> _page = elasticsearchTemplate.searchForPage(new SearchOperation<>("record_game_rebate_balance", query,
                ProxyMemberRebateBalance.class).setSort(XSort.desc("recordTime")).setFrom(queryDto.getOffset()).setSize(queryDto.getLimit()));
        double totalBalace = elasticsearchTemplate.sum(new SearchOperation<>("record_game_rebate_balance", query), "count");
        _page.setAttach(MapBuilder.newMapBuilder(new HashMap<String, Object>()).put("totalBalace", totalBalace).immutableMap());
        return _page;
    }

    private BoolQueryBuilder addTimeRangeQueryIfNeed(BoolQueryBuilder masterQuery, Long start, Long end, String timeField) {
        if (start != null) {
            RangeQueryBuilder rangeQuery = rangeQuery(timeField).gte(start);
            if (end != null) {
                rangeQuery.lte(end);
            }
            masterQuery.must(rangeQuery);
        }
        return masterQuery;
    }

    private int calculateDeep(Long accountId, List<String> m_path) {
        int index;
        for (index = 0; index < m_path.size(); index++) {
            if (m_path.get(index).equals(accountId.toString())) {
                break;
            }
        }
        return m_path.size() - index - 1;
    }
}
