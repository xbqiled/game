package com.youwan.game.cac.service;

import com.youwan.game.cac.api.dto.BettingRecordQueryDto;
import com.youwan.game.common.core.betting.entity.*;
import com.youwan.game.common.data.core.Page;

/**
 * 投注记录查询器
 * @author terry.jiang[taoj555@163.com] on 2020-09-28.
 */
public interface BettingRecordQuery {

    /**
     * 查询彩票投注记录
     * @param dto
     * @return
     */
    Page<BcLotteryOrder> queryLotteryRecord(BettingRecordQueryDto dto);

    /**
     * 查询皇冠体育投注记录
     * @param dto
     * @return
     */
    Page<CrownSportBettingOrder> queryCrownSportRecord(BettingRecordQueryDto dto);

    /**
     * 查询EGame投注记录
     * @param dto
     * @return
     */
    Page<ThirdPartyEGameBettingRecord> queryEGameRecord(BettingRecordQueryDto dto);

    /**
     * 查询真人投注记录
     * @param dto
     * @return
     */
    Page<ThirdPartyLiveBettingRecord> queryLiveRecord(BettingRecordQueryDto dto);

    /**
     *
     * @param dto
     * @return
     */
    Page<ThirdPartySportsBettingRecord> queryTPSportRecord(BettingRecordQueryDto dto);
}
