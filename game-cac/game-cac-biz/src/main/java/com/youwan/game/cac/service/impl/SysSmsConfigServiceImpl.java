package com.youwan.game.cac.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.youwan.game.cac.api.entity.SysSmsConfig;
import com.youwan.game.cac.api.vo.SmsVo;
import com.youwan.game.cac.mapper.SysSmsConfigMapper;
import com.youwan.game.cac.service.SysSmsConfigService;
import com.youwan.game.common.core.constant.ConfigConstant;
import com.youwan.game.common.core.sms.AliDayuConfig;
import com.youwan.game.common.core.sms.CloudLetterConfig;
import com.youwan.game.common.core.sms.RongLianConfig;
import com.youwan.game.common.core.sms.SmsConfig;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-24 13:41:07
 */
@Slf4j
@AllArgsConstructor
@Service("sysSmsConfigService")
public class SysSmsConfigServiceImpl extends ServiceImpl<SysSmsConfigMapper, SysSmsConfig> implements SysSmsConfigService {

    private final RedisTemplate redisTemplate;

    /**
     * <B>保存短信配置信息</B>
     * @param sysSmsConfig
     * @return
     */
    @Override
    public Boolean saveSmsConfig(SysSmsConfig sysSmsConfig) {
        sysSmsConfig.setCreateTime(LocalDateTime.now());
        sysSmsConfig.setCreateBy(Objects.requireNonNull(getUsername()));

        this.save(sysSmsConfig);
        return Boolean.TRUE;
    }


    /**
     * <B>激活短信配置信息</B>
     * @param id
     * @return
     */
    @Override
    public Boolean activateSmsConfig(Integer id) {
        //判断是否存在缓存
        SmsConfig  reidsConfig  = (SmsConfig)redisTemplate.opsForValue().get(ConfigConstant.ACTIVAT_SMS_CONFIG_KEY);
        if (reidsConfig != null) {
            redisTemplate.delete(ConfigConstant.ACTIVAT_SMS_CONFIG_KEY);
        }

        //更新所有配置为失效
        this.baseMapper.updateSmsConfigInvalid();
        //更新选择的配置为激活
        this.baseMapper.activateById(id);
        return Boolean.TRUE;
    }

    /**
     * <B> 获取当前激活的短信配置</B>
     */
    @Override
    public SmsConfig getActivateValue() {
        SmsConfig config  = (SmsConfig)redisTemplate.opsForValue().get(ConfigConstant.ACTIVAT_SMS_CONFIG_KEY);
        if (config == null) {
            SysSmsConfig smsConfig = baseMapper.queryActivateSmsConfig();

            if (smsConfig == null || StrUtil.isEmpty(smsConfig.getConfig())) {
                return  null;
            } else {
                redisTemplate.opsForValue().set(ConfigConstant.ACTIVAT_SMS_CONFIG_KEY, config);
                return new Gson().fromJson(smsConfig.getConfig(), SmsConfig.class);
            }
        } else {
            return config;
        }
    }

    /**
     * <B>获取配置信息</B>
     * @param id
     * @return
     */
    @Override
    public SmsConfig selectSmsSetting(Integer id) {
        SysSmsConfig sysSmsConfig = baseMapper.selectSmsSetting(id);
        //判断

        //阿里大鱼
        if (sysSmsConfig.getType() == 1) {
            AliDayuConfig smsAliyunConfig = null;

            if (sysSmsConfig != null && StrUtil.isNotBlank(sysSmsConfig.getConfig())) {
                smsAliyunConfig = new Gson().fromJson(sysSmsConfig.getConfig(), AliDayuConfig.class);
                return smsAliyunConfig;
            } else {
                return  new AliDayuConfig();
            }
        //云信互联
        } else if ( sysSmsConfig.getType() == 2){
            CloudLetterConfig  cloudLetterConfig;
            if (sysSmsConfig != null && StrUtil.isNotBlank(sysSmsConfig.getConfig())) {
                cloudLetterConfig = new Gson().fromJson(sysSmsConfig.getConfig(), CloudLetterConfig.class);
                return cloudLetterConfig;
            } else {
                return  new CloudLetterConfig();
            }
         //容联云
        }  else if ( sysSmsConfig.getType() == 3){
            RongLianConfig rongLianConfig;
            if (sysSmsConfig != null && StrUtil.isNotBlank(sysSmsConfig.getConfig())) {
                rongLianConfig = new Gson().fromJson(sysSmsConfig.getConfig(), RongLianConfig.class);
                return rongLianConfig;
            } else {
                return  new RongLianConfig();
            }
        } else {
            return  null;
        }
        //云信互联
    }

    /**
     * <B>更新短信配置信息</B>
     * @param id
     * @param
     * @return
     */
    @Override
    public Boolean updateSmsSetting(Integer id, SmsVo smsVo) {
        String strConfig = "";
        //阿里云
        if (smsVo.getType().equals("1")) {
            AliDayuConfig config = new AliDayuConfig();
            BeanUtil.copyProperties(smsVo, config);
            strConfig = new Gson().toJson(config);
        } else if (smsVo.getType().equals("2")) {
            CloudLetterConfig config = new CloudLetterConfig();
            BeanUtil.copyProperties(smsVo, config);
            strConfig = new Gson().toJson(config);
        } else if (smsVo.getType().equals("3")) {
            RongLianConfig config = new RongLianConfig();
            BeanUtil.copyProperties(smsVo, config);
            strConfig = new Gson().toJson(config);
        }

        //判断是否存在缓存
        SmsConfig  reidsConfig  = (SmsConfig)redisTemplate.opsForValue().get(ConfigConstant.ACTIVAT_SMS_CONFIG_KEY);
        if (reidsConfig != null) {
            redisTemplate.delete(ConfigConstant.ACTIVAT_SMS_CONFIG_KEY);
        }

        baseMapper.updateSmsSetting(id, strConfig);
        return Boolean.TRUE;
    }


    /**
     * <B>删除短信配置信息</B>
     * @return
     */
    @Override
    public Boolean deleteSmsSetting(Integer id) {
        //判断是否存在缓存
        SmsConfig  reidsConfig  = (SmsConfig)redisTemplate.opsForValue().get(ConfigConstant.ACTIVAT_SMS_CONFIG_KEY);
        if (reidsConfig != null) {
            redisTemplate.delete(ConfigConstant.ACTIVAT_SMS_CONFIG_KEY);
        }
        return this.removeById(id);
    }

    /**
     * 获取用户名称
     * @return username
     */
    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }
}
