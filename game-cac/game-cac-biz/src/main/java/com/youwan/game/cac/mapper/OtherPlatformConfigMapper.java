package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.OtherPlatformConfig;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;


import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-10-15 16:55:44
 */
public interface OtherPlatformConfigMapper extends BaseMapper<OtherPlatformConfig> {

    IPage<List<OtherPlatformConfig>> queryPlatformConfigPage(Page page, @Param("platformName") String platformName, @Param("platformCode") String platformCode);

    Integer updateConfigById(@Param("id") Integer id, @Param("platformConfig") String platformConfig);

    OtherPlatformConfig getPlatformConfigByCode(@Param("code") String code);
}
