package com.youwan.game.cac.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.OtherPlatformDto;
import com.youwan.game.cac.api.entity.OtherPlatformConfig;
import com.youwan.game.cac.mapper.OtherPlatformConfigMapper;
import com.youwan.game.cac.service.OtherPlatformConfigService;
import com.youwan.game.common.core.otherplatform.PlatformConfig;
import com.youwan.game.common.core.otherplatform.PlatformFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 
 *
 * @author code Lion
 * @date 2019-10-15 16:55:44
 */
@Service("otherPlatformConfigService")
public class OtherPlatformConfigServiceImpl extends ServiceImpl<OtherPlatformConfigMapper, OtherPlatformConfig> implements OtherPlatformConfigService {


    @Override
    public IPage<List<OtherPlatformConfig>> queryPlatformConfigPage(Page page, OtherPlatformDto otherPlatformDto) {
        return baseMapper.queryPlatformConfigPage(page,  otherPlatformDto.getPlatformCode(), otherPlatformDto.getPlatformName());
    }

    @Override
    @CacheEvict(value = "platformConfig:code:details", allEntries = true)
    public Boolean saveOtherPlatformConfig(OtherPlatformConfig otherPlatformConfig) {
        otherPlatformConfig.setCreateBy(getUsername());
        otherPlatformConfig.setCreateTime(LocalDateTime.now());
        this.save(otherPlatformConfig);
        return Boolean.TRUE;
    }

    @Override
    @CacheEvict(value = "platformConfig:code:details", allEntries = true)
    public Boolean updateOtherPlatformConfig(OtherPlatformConfig otherPlatformConfig) {
        otherPlatformConfig.setModifyBy(getUsername());
        otherPlatformConfig.setModifyTime(LocalDateTime.now());
        this.updateById(otherPlatformConfig);
        return Boolean.TRUE;
    }

    @Override
    @CacheEvict(value = "platformConfig:code:details", allEntries = true)
    public Integer updatePlatformConfig(Integer id, PlatformConfig platformConfig) {
        //转换成json
        String conifigStr = new Gson().toJson(platformConfig);
        return baseMapper.updateConfigById(id, conifigStr);
    }

    @Override
    public PlatformConfig getPlatformConfig(Integer id) {
        OtherPlatformConfig otherPlatformConfig = this.getById(id);
        if (otherPlatformConfig != null && StrUtil.isNotEmpty(otherPlatformConfig.getPlatformConfig())) {
           return  PlatformFactory.json2Model(otherPlatformConfig.getPlatformType(), otherPlatformConfig.getPlatformConfig());
        } else {
            return null;
        }
    }

    @Override
    @CacheEvict(value = "platformConfig:code:details", key = "#codeStr")
    public PlatformConfig getPlatformConfigByCode(String codeStr) {
        OtherPlatformConfig otherPlatformConfig =  baseMapper.getPlatformConfigByCode(codeStr);
        if (otherPlatformConfig != null && StrUtil.isNotEmpty(otherPlatformConfig.getPlatformConfig())) {
            return  PlatformFactory.json2Model(otherPlatformConfig.getPlatformType(), otherPlatformConfig.getPlatformConfig());
        } else {
            return null;
        }
    }

    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }
}
