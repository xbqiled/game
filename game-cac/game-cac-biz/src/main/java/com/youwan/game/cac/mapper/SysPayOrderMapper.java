package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.SysPayOrder;
import com.youwan.game.cac.api.vo.NotifyOrderVo;
import com.youwan.game.cac.api.vo.PayChannelVo;
import com.youwan.game.cac.api.vo.PayOrderVo;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-02-20 18:03:40
 */
public interface SysPayOrderMapper extends BaseMapper<SysPayOrder> {

    PayOrderVo getPayOrder(@Param("orderId") String orderId);

    SysPayOrder getPayOrderById(@Param("orderId") String orderId);

    NotifyOrderVo getMerchantKey(@Param("orderId") String orderId);

    IPage<List<SysPayOrder>> queryPayOrderPage(Page page, @Param("orderNo") String orderNo, @Param("userId") String userId, @Param("status") String status);

    List<SysPayOrder> queryNowRechargeList(@Param("channelId") String channelId, @Param("userId") String userId, @Param("payFee") BigDecimal payFee, @Param("queryTime") String queryTime);
}
