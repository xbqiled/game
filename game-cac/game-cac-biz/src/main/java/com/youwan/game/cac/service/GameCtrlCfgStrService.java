package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.WxhhCtrlCfgDto;
import com.youwan.game.cac.api.entity.GameCtrlCfgStr;

import java.util.List;
import java.util.Map;


/**
 * VIEW
 *
 * @author code generator
 * @date 2020-04-14 19:28:20
 */
public interface GameCtrlCfgStrService extends IService<GameCtrlCfgStr> {


    IPage<Map<String, Object>> queryGameCtrlCfgStrPage(Page page, String startTime, String endTime);

    int doCfg(WxhhCtrlCfgDto wxhhCtrlCfgDto);

    Object getWxhhCtrlCfg(String keyId);

    List<Map<String, Object>> getGameList();

}
