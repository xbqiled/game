package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.Mailtemplate;
import com.youwan.game.cac.mapper.MailtemplateMapper;
import com.youwan.game.cac.service.MailtemplateService;
import org.springframework.stereotype.Service;

/**
 * 
 *
 * @author code generator
 * @date 2019-03-19 13:02:27
 */
@Service("mailtemplateService")
public class MailtemplateServiceImpl extends ServiceImpl<MailtemplateMapper, Mailtemplate> implements MailtemplateService {

}
