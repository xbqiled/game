package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.SysSmsTemplate;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2019-02-17 18:01:23
 */
public interface SysSmsTemplateMapper extends BaseMapper<SysSmsTemplate> {

    IPage<List<SysSmsTemplate>> queryTemplatePage(Page page, @Param("templateCode") String templateCode, @Param("templateName") String templateName, @Param("channelId") String channelId);

    SysSmsTemplate findByServerCode(@Param("serverCode") String serverCode, @Param("type") Integer type, @Param("channelId") String channelId);

    SysSmsTemplate findByTemplateId(@Param("templateCode") String templateCode, @Param("type") Integer type, @Param("channelId") String channelId);

    List<SysSmsTemplate> queryTemplateByChannel(@Param("type") Integer type, @Param("channelId") String channelId);

}
