package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.ChlUser;
import com.youwan.game.cac.mapper.ChlUserMapper;
import com.youwan.game.cac.service.ChlUserService;
import org.springframework.stereotype.Service;

/**
 * 系统用户
 *
 * @author code generator
 * @date 2019-02-09 17:16:48
 */
@Service("chlUserService")
public class ChlUserServiceImpl extends ServiceImpl<ChlUserMapper, ChlUser> implements ChlUserService {

}
