package com.youwan.game.cac.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.BgBetDto;
import com.youwan.game.cac.api.entity.OtherBgbetRecord;
import com.youwan.game.cac.mapper.OtherBgbetRecordMapper;
import com.youwan.game.cac.service.OtherBgbetRecordService;
import com.youwan.game.cac.service.OtherPlatformConfigService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-05 15:56:55
 */
@AllArgsConstructor
@Service("otherBgbetRecordService")
public class OtherBgbetRecordServiceImpl extends ServiceImpl<OtherBgbetRecordMapper, OtherBgbetRecord> implements OtherBgbetRecordService {

    private final OtherPlatformConfigService otherPlatformConfigService;

    private static String BGBET_TABLE_NAME = "bgbet_record";

    @Override
    public IPage<List<OtherBgbetRecord>> queryBgBetRecordPage(Page page, BgBetDto bgBetDto) {
        List <OtherBgbetRecord> resultList = new ArrayList<>();
        if (bgBetDto.getStartTime() == null || bgBetDto.getEndTime() == null) {
            page.setTotal(new Long(0));
            page.setRecords(resultList);
            return page;
        }

        StringBuffer tableSql = new StringBuffer();
        List<String> list = getIntervalMonth(bgBetDto.getStartTime(), bgBetDto.getEndTime());

        String sql = spliceInSql(list);
        List<String> tableList = baseMapper.queryHaveTab(sql);

        tableSql.append(spliceQuerySql(tableList));
        IPage<List<OtherBgbetRecord>>  result = baseMapper.queryBgBetRecordPage(page, tableList, bgBetDto.getOrderId(), bgBetDto.getUserId(), bgBetDto.getIssueId(), bgBetDto.getOrderId(), bgBetDto.getStartTime(), bgBetDto.getEndTime());
        return result;
    }

    private List<String> getIntervalMonth(String startTime, String endTime) {
        List<String> list = new ArrayList<>();
        //转化成月份
        Date startMonthDate = DateUtil.parse(startTime, DatePattern.NORM_DATETIME_PATTERN);
        Date endMonthDate = DateUtil.parse(endTime, DatePattern.NORM_DATETIME_PATTERN);

        String endMonth = DateUtil.format(endMonthDate, "yyyyMM");
        Date nextDate = startMonthDate;

        //判断时间不等于结束时间
        while (!DateUtil.format(nextDate, "yyyyMM").equals(endMonth)) {
            list.add(BGBET_TABLE_NAME + DateUtil.format(nextDate, "yyyyMM"));
            nextDate = DateUtil.offsetMonth(nextDate, 1);
        }

        list.add(BGBET_TABLE_NAME + endMonth);
        return list;
    }

    /**
     * <B>拼接SQL语句</B>
     * @param list
     * @return
     */
    private String spliceQuerySql(List<String> list) {
        StringBuffer sb = new StringBuffer();
        sb.append("(");
        if (CollUtil.isNotEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                sb.append(" select * From " + list.get(i));
                if (i < list.size() - 1) {
                    sb.append("  union all ");
                }
            }
        }
        sb.append(")");
        return sb.toString();
    }

    private String spliceInSql(List<String> list) {
        StringBuffer sb = new StringBuffer();
        sb.append("(");
        if (CollUtil.isNotEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                sb.append("'" + list.get(i) + "'");
                if (i < list.size() - 1) {
                    sb.append(" , ");
                }
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
