package com.youwan.game.cac.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.DeviceInfoDto;
import com.youwan.game.cac.api.dto.VerifyDeviceInfo;
import com.youwan.game.cac.api.entity.DeviceInfo;
import com.youwan.game.cac.api.vo.DeviceInfoVo;
import com.youwan.game.cac.service.DeviceInfoService;
import com.youwan.game.common.core.jsonmodel.GsonDeviceInfo;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.common.security.annotation.Inner;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * @author code generator
 * @date 2019-05-15 15:24:30
 */
@RestController
@AllArgsConstructor
@RequestMapping("/deviceinfo")
public class DeviceInfoController {

    private final DeviceInfoService deviceInfoService;

    /**
     * 分页查询
     *
     * @param page       分页对象
     * @param deviceInfo
     * @return
     */
    @GetMapping("/page")
    public R getDeviceInfoPage(Page page, DeviceInfo deviceInfo) {
        return new R<>(deviceInfoService.getDeviceInfoPage(page, deviceInfo));
    }


    @GetMapping("/getDeviceInfo/{id}")
    public R getDeviceInfo(@PathVariable("id") Integer id) {
        DeviceInfoVo vo = new DeviceInfoVo();
        DeviceInfo deviceInfo = deviceInfoService.getById(id);

        if (deviceInfo != null && StrUtil.isNotBlank(deviceInfo.getDeviceInfo())) {
            GsonDeviceInfo info = new Gson().fromJson(deviceInfo.getDeviceInfo(), GsonDeviceInfo.class);

            if (info != null) {
                BeanUtil.copyProperties(info, vo);
            }
        }
        BeanUtil.copyProperties(deviceInfo, vo);
        return new R<>(vo);
    }


    /**
     * 通过id查询
     *
     * @param id id
     * @return R
     */
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") Integer id) {
        return new R<>(deviceInfoService.getById(id));
    }

    /**
     * 新增
     *
     * @param deviceInfoDto
     * @return R
     */
    @Inner
    @PostMapping("/addDeviceInfo")
    public R addDeviceInfo(@RequestBody DeviceInfoDto deviceInfoDto) {
        return new R<>(deviceInfoService.addDeviceInfo(deviceInfoDto));
    }


    @Inner
    @PostMapping("/verifyDeviceInfo")
    public R verifyDeviceInfo(@RequestBody VerifyDeviceInfo deviceInfoDto) {
        return new R<>(deviceInfoService.verifyDeviceInfo(deviceInfoDto));
    }


    /**
     * 修改
     *
     * @param deviceInfo
     * @return R
     */
    @SysLog("修改")
    @PutMapping
    public R updateById(@RequestBody DeviceInfo deviceInfo) {
        return new R<>(deviceInfoService.updateById(deviceInfo));
    }

    /**
     * 通过id删除
     *
     * @param id id
     * @return R
     */
    @SysLog("删除")
    @DeleteMapping("/{id}")
    public R removeById(@PathVariable Integer id) {
        return new R<>(deviceInfoService.removeById(id));
    }

}
