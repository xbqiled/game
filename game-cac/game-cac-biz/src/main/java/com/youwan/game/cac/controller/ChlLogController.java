package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.ChlLog;
import com.youwan.game.cac.service.ChlLogService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 系统日志
 *
 * @author code generator
 * @date 2019-05-23 13:09:56
 */
@RestController
@AllArgsConstructor
@RequestMapping("/chllog")
public class ChlLogController {

  private final ChlLogService chlLogService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param queryDto 系统日志
   * @return
   */
  @GetMapping("/page")
  public R getChlLogPage(Page page, QueryDto queryDto) {
    return  new R<>(chlLogService.queryLogPage(page, queryDto));
  }

}
