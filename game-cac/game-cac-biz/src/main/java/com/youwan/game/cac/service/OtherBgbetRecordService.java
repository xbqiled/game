package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.BgBetDto;
import com.youwan.game.cac.api.entity.OtherBgbetRecord;
import com.youwan.game.cac.api.vo.BgBetOrder;

import java.util.List;


/**
 * 
 *
 * @author code generator
 * @date 2019-11-05 15:56:55
 */
public interface OtherBgbetRecordService extends IService<OtherBgbetRecord> {

    IPage<List<OtherBgbetRecord>> queryBgBetRecordPage(Page page, BgBetDto bgBetDto);

}
