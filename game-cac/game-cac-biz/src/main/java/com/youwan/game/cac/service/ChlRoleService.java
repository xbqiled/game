package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.ChlRole;

/**
 * 角色
 *
 * @author code generator
 * @date 2019-02-09 17:17:39
 */
public interface ChlRoleService extends IService<ChlRole> {

}
