package com.youwan.game.cac.service.impl;

import com.youwan.game.cac.api.dto.BettingRecordQueryDto;
import com.youwan.game.cac.service.BettingRecordQuery;
import com.youwan.game.common.core.betting.entity.*;
import com.youwan.game.common.data.core.Page;
import com.youwan.game.common.data.core.XSort;
import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import com.youwan.game.common.data.elasticsearch.operations.SearchOperation;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.springframework.stereotype.Service;

import static org.elasticsearch.index.query.QueryBuilders.rangeQuery;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-28.
 */
@Service
public class BettingRecordQueryImpl implements BettingRecordQuery {

    private ElasticsearchTemplate elasticsearchTemplate;

    public BettingRecordQueryImpl(ElasticsearchTemplate elasticsearchTemplate) {
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    private BoolQueryBuilder getDefaultQuery(BettingRecordQueryDto dto) {
        return QueryBuilders.boolQuery().must(termQuery("userId", dto.getUserId()));
    }

    @Override
    public Page<BcLotteryOrder> queryLotteryRecord(BettingRecordQueryDto dto) {
        BoolQueryBuilder query = getDefaultQuery(dto);
        if (dto.getStart() != null) {
            RangeQueryBuilder rangeQuery = rangeQuery("createTime").gte(dto.getStart() * 1000);
            if (dto.getEnd() != null) {
                rangeQuery.lte(dto.getEnd() * 1000);
            }
            query.must(rangeQuery);
        }
        return elasticsearchTemplate.searchForPage(new SearchOperation<>(
                "bc_lottery_order", "_doc", query, BcLotteryOrder.class)
                .setFetchSource(new String[]{"createTime", "buyMoney", "status"}, null).setSort(XSort.desc("createTime"))
                .setFrom(dto.getOffset()).setSize(dto.getLimit()));
    }

    @Override
    public Page<CrownSportBettingOrder> queryCrownSportRecord(BettingRecordQueryDto dto) {
        BoolQueryBuilder query = getDefaultQuery(dto);
        if (dto.getStart() != null) {
            RangeQueryBuilder rangeQuery = rangeQuery("bettingDate").gte(dto.getStart() * 1000);
            if (dto.getEnd() != null) {
                rangeQuery.lte(dto.getEnd() * 1000);
            }
            query.must(rangeQuery);
        }
        return elasticsearchTemplate.searchForPage(new SearchOperation<>(
                "crown_sport_betting_order", "_doc", query, CrownSportBettingOrder.class)
                .setFetchSource(new String[]{"bettingDate", "bettingMoney", "resultStatus", "balance"}, null).setSort(XSort.desc("bettingDate"))
                .setFrom(dto.getOffset()).setSize(dto.getLimit()));
    }

    @Override
    public Page<ThirdPartyEGameBettingRecord> queryEGameRecord(BettingRecordQueryDto dto) {
        BoolQueryBuilder query = getDefaultQuery(dto);
        if (dto.getStart() != null) {
            RangeQueryBuilder rangeQuery = rangeQuery("bettingTime").gte(dto.getStart() * 1000);
            if (dto.getEnd() != null) {
                rangeQuery.lte(dto.getEnd() * 1000);
            }
            query.must(rangeQuery);
        }
        return elasticsearchTemplate.searchForPage(new SearchOperation<>(
                "3rd_egame_betting_record", "_doc", query, ThirdPartyEGameBettingRecord.class)
                .setFetchSource(new String[]{"bettingTime", "realBettingMoney", "bettingCode"}, null).setSort(XSort.desc("bettingTime"))
                .setFrom(dto.getOffset()).setSize(dto.getLimit()));
    }

    @Override
    public Page<ThirdPartyLiveBettingRecord> queryLiveRecord(BettingRecordQueryDto dto) {
        BoolQueryBuilder query = getDefaultQuery(dto);
        if (dto.getStart() != null) {
            RangeQueryBuilder rangeQuery = rangeQuery("bettingTime").gte(dto.getStart() * 1000);
            if (dto.getEnd() != null) {
                rangeQuery.lte(dto.getEnd() * 1000);
            }
            query.must(rangeQuery);
        }
        return elasticsearchTemplate.searchForPage(new SearchOperation<>(
                "3rd_live_betting_record", "_doc", query, ThirdPartyLiveBettingRecord.class)
                .setFetchSource(new String[]{"bettingTime", "realBettingMoney", "bettingCode"}, null).setSort(XSort.desc("bettingTime"))
                .setFrom(dto.getOffset()).setSize(dto.getLimit()));
    }

    @Override
    public Page<ThirdPartySportsBettingRecord> queryTPSportRecord(BettingRecordQueryDto dto) {
        BoolQueryBuilder query = getDefaultQuery(dto);
        if (dto.getStart() != null) {
            RangeQueryBuilder rangeQuery = rangeQuery("bettingTime").gte(dto.getStart() * 1000);
            if (dto.getEnd() != null) {
                rangeQuery.lte(dto.getEnd() * 1000);
            }
            query.must(rangeQuery);
        }
        return elasticsearchTemplate.searchForPage(new SearchOperation<>(
                "3rd_sport_betting_record", "_doc", query, ThirdPartySportsBettingRecord.class)
                .setFetchSource(new String[]{"bettingTime", "realBettingMoney", "bettingCode"}, null).setSort(XSort.desc("bettingTime"))
                .setFrom(dto.getOffset()).setSize(dto.getLimit()));
    }
}
