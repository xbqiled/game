package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.SysSmsConfig;
import com.youwan.game.cac.api.vo.SmsVo;
import com.youwan.game.common.core.sms.SmsConfig;


/**
 * 
 *
 * @author code lion
 * @date 2019-01-24 13:41:07
 */
public interface SysSmsConfigService extends IService<SysSmsConfig> {

    /**
     * <B>添加配置信息</B>
     * @param sysSmsConfig
     * @return
     */
    Boolean saveSmsConfig(SysSmsConfig sysSmsConfig);

    /**
     * <B>激活配置信息</B>
     * @param id
     * @return
     */
    Boolean activateSmsConfig( Integer id);


    /**
     * <B>获取当前激活配置信息</B>
     * @return
     */
    SmsConfig getActivateValue();


    /**
     * <B>获取配置信息</B>
     * @param id
     * @return
     */
    SmsConfig selectSmsSetting(Integer id);


    /**
     * <B>更新配置信息</B>
     * @return
     */
    Boolean updateSmsSetting(Integer id, SmsVo smsVo);


    /**
     * <B>删除短信配置信息</B>
     * @return
     */
    Boolean deleteSmsSetting(Integer id);
}
