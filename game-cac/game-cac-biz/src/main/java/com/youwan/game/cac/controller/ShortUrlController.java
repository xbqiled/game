package com.youwan.game.cac.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.ShareDto;
import com.youwan.game.cac.api.entity.Domain;
import com.youwan.game.cac.api.entity.ShortUrl;
import com.youwan.game.cac.api.entity.SourceUrl;
import com.youwan.game.cac.api.vo.ShortUrlVo;
import com.youwan.game.cac.handler.WechatHand;
import com.youwan.game.cac.service.DomainService;
import com.youwan.game.cac.service.ShortUrlService;
import com.youwan.game.cac.service.SourceUrlService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.common.security.annotation.Inner;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * @author code generator
 * @date 2019-04-19 13:17:59
 */
@RestController
@AllArgsConstructor
@RequestMapping("/shorturl")
public class ShortUrlController {

    private final ShortUrlService shortUrlService;
    private final SourceUrlService sourceUrlService;
    private final DomainService domainService;

    /**
     * 分页查询
     *
     * @param page     分页对象
     * @param shortUrl
     * @return
     */
    @GetMapping("/page")
    public R getShortUrlPage(Page page, ShortUrl shortUrl) {
        return new R<>(shortUrlService.queryShortUrlPage(page, shortUrl));
    }

    /**
     * 通过id查询
     *
     * @param id id
     * @return R
     */
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") Integer id) {
        return new R<>(shortUrlService.getById(id));
    }

    /**
     * 新增
     *
     * @param shortUrl
     * @return R
     */
    @SysLog("新增")
    @PostMapping
    public R save(@RequestBody ShortUrl shortUrl) {
        return new R<>(shortUrlService.saveShortUrl(shortUrl));
    }

    /**
     * 修改
     *
     * @param shortUrl
     * @return R
     */
    @SysLog("修改")
    @PutMapping
    public R updateById(@RequestBody ShortUrl shortUrl) {
        return new R<>(shortUrlService.updateShortById(shortUrl));
    }

    /**
     * 通过id删除
     *
     * @param id id
     * @return R
     */
    @SysLog("删除")
    @DeleteMapping("/{id}")
    public R removeById(@PathVariable Integer id) {
        return new R<>(shortUrlService.deleteShortById(id));
    }


    @Inner
    @PostMapping("/createShortUrl")
    public R createShortUrl(@Valid @RequestBody ShareDto shareDto) {
        //第一步查询数据库里是否存在对应的地址信息,如果存在直接返回
        SourceUrl sourceUrl = sourceUrlService.getJumpUrl(shareDto.getUrl());
        //判断是否要刷新
        if ((StrUtil.isBlank(shareDto.getIsRefresh()) || shareDto.getIsRefresh().equals("0"))  && (sourceUrl != null && StrUtil.equals(sourceUrl.getSourceUrl(), shareDto.getUrl()))) {
            //直接返回
            return new R<>(new ShortUrlVo(sourceUrl.getShortUrl(), sourceUrl.getJumpUrl(), sourceUrl.getSourceUrl()));
        } else {
            //第一步获取所有短链接信息
            List<ShortUrl> list = shortUrlService.getAllShortUrlConfig();
            ShortUrl shortUrl = RandomUtil.randomEle(list);
            String jumpUrl = "";
            String uuidStr = "";
            String shortStr = "";

            //第二步判断短链接是否什么类型
            if (shortUrl != null && shortUrl.getType() == 2) {
                //自己系统生成
                List<Domain> domainList = domainService.quereyDomainByChannelId(shareDto.getChannelId());
                if (CollUtil.isNotEmpty(domainList)) {
                    //随机选取一条域名
                    Domain domain = RandomUtil.randomEle(domainList);
                    uuidStr = IdUtil.fastSimpleUUID();
                    jumpUrl = WechatHand.createJumpUrl(uuidStr, domain);
                    shortStr = shortUrlService.createShortUrl(jumpUrl, shortUrl);
                }
            } else {
                shortStr = shortUrlService.createShortUrl(shareDto.getUrl(), shortUrl);
            }

            //随机生成一条短链接

            if (StrUtil.isNotEmpty(shortStr)) {
                //保存这些信息
                sourceUrlService.createSourceUrl(uuidStr, shareDto.getChannelId(), shortStr, jumpUrl, shareDto.getUrl());
                //判断是否删除
                if (sourceUrl != null && sourceUrl.getId() > 0) {
                    //sourceUrlService.deleteSourceById(sourceUrl.getId().toString());
                    sourceUrlService.deleteBySourceUrl(sourceUrl.getSourceUrl());
                }
                //判断返回
                return new R<>(new ShortUrlVo(shortStr, jumpUrl, shareDto.getUrl()));
            } else {
                return new R<>();
            }
        }
    }
}
