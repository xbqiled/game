package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.TChannel;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2019-02-08 18:30:36
 */
public interface ChannelMapper extends BaseMapper<TChannel> {

    TChannel getChannelInfo(@Param("channelId") String channelId);

    List<Map<String, Object>> getGameRoomInfo();

    IPage<List<TChannel>> queryChannelPage(Page page, @Param("channelId") String channelId, @Param("channelName") String channelName);
}
