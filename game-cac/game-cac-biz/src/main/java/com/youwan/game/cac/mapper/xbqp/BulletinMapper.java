package com.youwan.game.cac.mapper.xbqp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.Bulletin;
import com.youwan.game.cac.api.vo.BulletinVo;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-29 13:18:03
 */
public interface BulletinMapper extends BaseMapper<Bulletin> {

    IPage<Bulletin> queryBulletinPage(Page page,@Param("id") String id, @Param("title") String title);

    List<Map<String, Object>> queryBulletinChannel(@Param("bulletinId") String bulletinId);

    BulletinVo getBulletinVoById(@Param("bulletinId") String bulletinId);
}
