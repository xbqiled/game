package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.OtherBgRedPacket;

/**
 * 
 *
 * @author code generator
 * @date 2020-01-13 20:25:56
 */
public interface OtherBgRedPacketRecordMapper extends BaseMapper<OtherBgRedPacket> {

}
