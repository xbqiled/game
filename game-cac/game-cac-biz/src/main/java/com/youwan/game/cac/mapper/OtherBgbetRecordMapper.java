package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.OtherBgbetRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-05 15:56:55
 */
public interface OtherBgbetRecordMapper extends BaseMapper<OtherBgbetRecord> {

    IPage<List<OtherBgbetRecord>> queryBgBetRecordPage(Page page,@Param("tables") List<String> tables,
                                                                @Param("orderId") String channelId, @Param("userId") String userId,
                                                                @Param("issueId") String issueId,  @Param("gameName") String gameName,
                                                                @Param("startTime") String startTime, @Param("endTime") String endTime);

    List<String> queryHaveTab(@Param("tableSql") String tableSql);
}
