package com.youwan.game.cac.controller;

import com.youwan.game.cac.api.vo.PayCodeVo;
import com.youwan.game.cac.service.SysPayCodeService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.security.annotation.Inner;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 
 *
 * @author code generator
 * @date 2019-05-10 11:23:04
 */
@RestController
@AllArgsConstructor
@RequestMapping("/paycode")
public class SysPayCodeController {

  private final SysPayCodeService sysPayCodeService;


  @Inner
  @PostMapping("/getPayCode/{orderNo}")
  public R getPayCodeByOrderNo(@PathVariable String orderNo) {
      PayCodeVo vo = sysPayCodeService.getPayCodeByOrderNo(orderNo);
      return new R<>(vo);
  }

}
