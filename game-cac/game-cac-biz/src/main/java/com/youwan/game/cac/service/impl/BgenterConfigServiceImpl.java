package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.BgEnterGameDto;
import com.youwan.game.cac.api.entity.BgenterConfig;
import com.youwan.game.cac.mapper.BgenterConfigMapper;
import com.youwan.game.cac.service.BgenterConfigService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-12 14:04:32
 */
@Service("otherBgenterConfigService")
public class BgenterConfigServiceImpl extends ServiceImpl<BgenterConfigMapper, BgenterConfig> implements BgenterConfigService {

    @Override
    public IPage<List<BgenterConfig>> queryEnterGameConfigPage(Page page, BgEnterGameDto bgEnterGameDto) {
        return baseMapper.queryEnterGameConfigPage(page, bgEnterGameDto.getChannelId());
    }

    @Override
    public Boolean saveEnterConfig(BgenterConfig bgenterConfig) {
        BgenterConfig config = baseMapper.getConfigByChannelId(bgenterConfig.getChannelId());
        if (config != null) {
            return  Boolean.FALSE;
        }

        bgenterConfig.setCreateBy(getUsername());
        bgenterConfig.setCreateTime(LocalDateTime.now());
        this.save(bgenterConfig);
        return Boolean.TRUE;
    }

    @Override
    public Boolean modifyEnterConfig(BgenterConfig bgenterConfig) {
        bgenterConfig.setModifyBy(getUsername());
        bgenterConfig.setModifyTime(LocalDateTime.now());
        this.updateById(bgenterConfig);
        return Boolean.TRUE;
    }

    @Override
    public BgenterConfig getConfigByChannelId(String channleId) {
        return baseMapper.getConfigByChannelId(channleId);
    }

    /**
     * 获取用户名称
     *
     * @return username
     */
    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }
}
