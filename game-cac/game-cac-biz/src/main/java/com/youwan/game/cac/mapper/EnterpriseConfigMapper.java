package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.EnterpriseConfig;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-06 13:19:25
 */
public interface EnterpriseConfigMapper extends BaseMapper<EnterpriseConfig> {

}
