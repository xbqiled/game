package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.Domain;
import com.youwan.game.cac.mapper.DomainMapper;
import com.youwan.game.cac.service.DomainService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author code generator
 * @date 2019-04-19 13:18:14
 */
@Service("domainService")
public class DomainServiceImpl extends ServiceImpl<DomainMapper, Domain> implements DomainService {

    @Override
    public IPage<List<Domain>> queryDomainPage(Page page, Domain domain) {
        return baseMapper.queryDomainPage(page, domain.getChannelId(), domain.getDomainUrl());
    }

    @Override
    public Boolean saveDomain(Domain domain) {
        domain.setCreateTime(LocalDateTime.now());
        this.save(domain);
        return Boolean.TRUE;
    }

    @Override
    @CacheEvict(value = "domain:details", allEntries = true)
    public Boolean updateDomainById(Domain domain) {
        return this.updateById(domain);
    }

    @Override
    @CacheEvict(value = "domain:details", allEntries = true)
    public Boolean deleteDomainById(Integer id) {
        return this.removeById(id);
    }

    @Override
    @CacheEvict(value = "domain:details", key = "#channelId")
    public List<Domain> quereyDomainByChannelId(String channelId) {
        return baseMapper.queryDomainByChannelId(channelId);
    }
}
