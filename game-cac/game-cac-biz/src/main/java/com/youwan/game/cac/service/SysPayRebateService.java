package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.SysPayRebate;


/**
 * 
 *
 * @author code generator
 * @date 2019-06-05 14:11:47
 */
public interface SysPayRebateService extends IService<SysPayRebate> {

}
