package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.BindMobile;

import java.util.List;
import java.util.Map;


/**
 * @author code generator
 * @date 2019-04-10 21:03:25
 */
public interface BindMobileService extends IService<BindMobile> {


    IPage<List<Map<String, Object>>> queryBindPage(Page page, String channelId);

    BindMobile getByChannelId(String channelId);

    Boolean saveOrUpdateBind(BindMobile bindMobile);
}
