package com.youwan.game.cac.mapper.xbqp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.InviteDeposit;
import com.youwan.game.cac.api.vo.InviteDepositChannelVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-08-03 15:31:04
 */
public interface InviteDepositMapper extends BaseMapper<InviteDeposit> {

    IPage<List<InviteDepositChannelVo>> queryInviteDepositPage(Page page, @Param("channelId") String channelId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    InviteDepositChannelVo getInviteDepositByChannelId(@Param("channelId") String channelId);

}
