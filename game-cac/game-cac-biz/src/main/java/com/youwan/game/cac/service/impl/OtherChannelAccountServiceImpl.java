package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.ChannelAccountDto;
import com.youwan.game.cac.api.entity.AccountRecord;
import com.youwan.game.cac.api.entity.ChannelAccount;
import com.youwan.game.cac.api.entity.TChannel;
import com.youwan.game.cac.mapper.OtherChannelAccountMapper;
import com.youwan.game.cac.mapper.ChannelMapper;
import com.youwan.game.cac.service.OtherAccountRecordService;
import com.youwan.game.cac.service.OtherChannelAccountService;
import com.youwan.game.common.core.util.OrderIdUtil;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-19 14:07:23
 */
@AllArgsConstructor
@Service("otherChannelBalanceService")
public class OtherChannelAccountServiceImpl extends ServiceImpl<OtherChannelAccountMapper, ChannelAccount> implements OtherChannelAccountService {

    private final ChannelMapper channelMapper;

    private final OtherAccountRecordService otherAccountRecordService;

    @Override
    public IPage<List<ChannelAccount>> getChannelAccountPage(Page page, ChannelAccountDto channelBalanceDto) {
        return baseMapper.queryChannelAccountList(page, channelBalanceDto.getChannelId());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer updateChannelAccount(ChannelAccountDto channelAccountDto) {
        String amount = "";

        //获取渠道信息
        TChannel  channel = channelMapper.getChannelInfo(channelAccountDto.getChannelId());
        if (channel == null) {
            return  1;
        }

        amount = channelAccountDto.getOperateType() == 1? channelAccountDto.getAmount().toString() : "-" + channelAccountDto.getAmount();

        //获取账户信息
        ChannelAccount channelAccount = baseMapper.getByChannelId(channelAccountDto.getChannelId());


        if (channelAccount != null) {
            if (channelAccountDto.getOperateType() == 1) {
                baseMapper.updateIncreaseBgBalance(channelAccountDto.getChannelId(), channelAccountDto.getAmount());
            } else {
                baseMapper.updateReduceBgBalance(channelAccountDto.getChannelId(), channelAccountDto.getAmount());
            }
        } else {
            ChannelAccount cAccount  = new ChannelAccount();
            cAccount.setVideoBalance(new BigDecimal(amount));
            cAccount.setChannelId(channel.getChannelId());

            cAccount.setGameType(1);
            if (channelAccountDto.getOperateType() == 1) {
                cAccount.setSumIncrease(new BigDecimal(amount));
                cAccount.setSumReduce(new BigDecimal(0));
            } else {
                cAccount.setSumIncrease(new BigDecimal(0));
                cAccount.setSumReduce(new BigDecimal(amount));
            }

            cAccount.setChannelName(channel.getChannelName());
            cAccount.setModifyTime(LocalDateTime.now());
            cAccount.setModifyBy(getUsername());
            this.save(cAccount);
        }

        //记录账变信息
        AccountRecord accountOrder = packageAccountRecord(channelAccountDto.getOperateType(), channelAccount.getVideoBalance(),  new BigDecimal(amount),
                channelAccount.getVideoBalance().add(new BigDecimal(amount)), channelAccountDto.getChannelId(), 1);
        otherAccountRecordService.save(accountOrder);
        return 0;
    }


    public AccountRecord packageAccountRecord(Integer accountItem,  BigDecimal beforeAmount, BigDecimal changeAmount, BigDecimal afterAmount,
                                              String channelId, Integer gameType) {
        AccountRecord accountRecord  = new AccountRecord();
        accountRecord.setAfterAmount(afterAmount);
        accountRecord.setAccountItem(accountItem);

        accountRecord.setBeforeAmount(beforeAmount);
        accountRecord.setChangeAmount(changeAmount);

        accountRecord.setBizId(OrderIdUtil.getOrdereId());
        accountRecord.setChannelId(channelId);
        accountRecord.setStatus(1);
        //真人
        accountRecord.setGameType(gameType);
        accountRecord.setCreateBy(getUsername());
        accountRecord.setCreateTime(LocalDateTime.now());
        return  accountRecord;
    }


    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }
}
