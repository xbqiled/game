package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.SysPushPlatform;
import com.youwan.game.cac.mapper.SysPushPlatformMapper;
import com.youwan.game.cac.service.SysPushPlatformService;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 
 *
 * @author code generator
 * @date 2020-05-23 15:14:24
 */
@Service("sysPushPlatformService")
public class SysPushPlatformServiceImpl extends ServiceImpl<SysPushPlatformMapper, SysPushPlatform> implements SysPushPlatformService {

    @Override
    public IPage<List<Map<String, Object>>> queryPushPlatformPage(Page page, String platformName) {
        return baseMapper.queryPushPlatformPage(page, platformName);
    }

    @Override
    public Boolean addPushPlatform(SysPushPlatform sysPushPlatform) {
        sysPushPlatform.setCreateTime(LocalDateTime.now());
        sysPushPlatform.setCreateBy(Objects.requireNonNull(getUsername()));
        this.save(sysPushPlatform);
        return Boolean.TRUE;
    }

    @Override
    public Boolean updatePushPlatform(SysPushPlatform sysPushPlatform) {
        SysPushPlatform source = this.getById(sysPushPlatform.getId());
        if (source != null) {
            BeanUtils.copyProperties(sysPushPlatform, source);
            this.updateById(source);
        }
        return Boolean.TRUE;
    }


    @Override
    public Boolean delPushPlatform(Integer pushPlatformId) {
        this.removeById(pushPlatformId);
        return Boolean.TRUE;
    }


    @Override
    public List<Map<String, Object>> getPushPlatformList() {
        return baseMapper.getPushPlatformList();
    }


    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }


}
