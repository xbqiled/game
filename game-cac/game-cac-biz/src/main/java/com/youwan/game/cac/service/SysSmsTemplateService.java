package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.SysSmsTemplate;

import java.util.List;


/**
 * 
 *
 * @author code generator
 * @date 2019-02-17 18:01:23
 */
public interface SysSmsTemplateService extends IService<SysSmsTemplate> {

    Boolean saveSmsTemplate(SysSmsTemplate sysSmsTemplate);

    Boolean updateSmsTemplate(SysSmsTemplate sysSmsTemplate);

    Boolean delSmsTemplate(Integer id);

    SysSmsTemplate findByServerCode(String serverCode, Integer type, String channelId);

    List<SysSmsTemplate>  queryTemplateByChannel(Integer type, String channelId);

    IPage<List<SysSmsTemplate>> queryTemplatePage(Page page, String templateCode, String templateName, String channelId);
}
