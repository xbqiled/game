package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.AddPointDto;
import com.youwan.game.cac.api.dto.NotifyPayOrderDto;
import com.youwan.game.cac.api.dto.PayOrderDto;
import com.youwan.game.cac.api.entity.SysPayOrder;
import com.youwan.game.cac.api.vo.*;
import com.youwan.game.common.core.jsonmodel.GsonReduceResult;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * 
 *
 * @author code generator
 * @date 2019-02-20 18:03:40
 */
public interface SysPayOrderService extends IService<SysPayOrder> {

    PayResult createPayOrder(PayOrderDto payOrderDto);

    PayResult getPayUrl(PayConfigVo payConfigVo, PayOrderDto payOrderDto, HttpServletRequest request);

    PayConfigVo getPayInfo(PayConfigVo payConfigVo, PayOrderDto payOrderDto, HttpServletRequest request);

    String callbackPayOrdery(NotifyPayOrderDto payOrderDto);

    AddPointVo addPoint(AddPointDto addPointDto);

    PayOrderVo getOrder(String orderId);

    NotifyOrderVo getMerchantKey(String orderId);

    Boolean recharge(String userId, String payFee, String depositor, String account, String remark, String payType, Double damaMulti, Integer rate);

    IPage<List<SysPayOrder>> queryPayOrderPage(Page page, String orderNo, String userId, String status);

    GsonReduceResult reduceFee(String userId, String note, String payFee, Integer force);
}
