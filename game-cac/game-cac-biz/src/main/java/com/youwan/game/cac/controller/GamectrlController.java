package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.GameCtrlDto;
import com.youwan.game.cac.service.GamectrlService;
import com.youwan.game.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-11 17:01:03
 */
@RestController
@AllArgsConstructor
@RequestMapping("/gamectrl")
public class GamectrlController {

  private final GamectrlService gamectrlService;

  /**
   * 分页查询
   *
   * @param
   * @param
   * @return
   */
  @GetMapping("/page")
  public R getGamectrlInfo(Page page, String tGameid) {
    return new R<>(gamectrlService.queryGameCtrlPage(page, tGameid));
  }

  /**
   * <B>获取树结构的房间信息</B>
   *
   * @return
   */
  @PostMapping("/getRoomTree")
  public R getRoomTree() {
    return new R<>(gamectrlService.getRoomTreeData());
  }

  /**
   * 通过id查询VIEW
   *
   * @param gameCtrlDto
   * @return R
   */
  @PostMapping("/doSet")
  public R doSet(@RequestBody GameCtrlDto gameCtrlDto) {
    int result = gamectrlService.doSet(gameCtrlDto);
    if (result == 0) {
      return new R<>();
    } else {
      return new R<>().setCode(result).setMsg("设置游戏控制失败!");
    }
  }
}