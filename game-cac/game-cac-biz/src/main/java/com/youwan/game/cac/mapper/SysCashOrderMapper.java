package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.SysCashOrder;
import com.youwan.game.cac.api.entity.SysPayOrder;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-02-20 18:19:04
 */
public interface SysCashOrderMapper extends BaseMapper<SysCashOrder> {

    void approval( @Param("approveBy")String approveBy , @Param("id") String id, @Param("status") String status, @Param("note") String note);

    List<SysCashOrder> getOrderByUserId(@Param("userId")String userId);

    List<SysCashOrder> getCashOrderInfo(@Param("userId")String userId, @Param("cashType")String cashType);

    IPage<List<SysCashOrder>> queryCashOrderPage(Page page, @Param("cashNo") String cashNo, @Param("userId") String userId, @Param("status") String status);
}
