package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.TChannelRelation;


/**
 * 
 *
 * @author code generator
 * @date 2019-04-01 15:36:39
 */
public interface ChannelRelationService extends IService<TChannelRelation> {

     int doGameConfig(String channelId, String[] roomId);

}
