package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.OtherBgRedPacket;


/**
 * 
 *
 * @author code generator
 * @date 2020-01-13 20:25:56
 */
public interface OtherBgRedPacketService extends IService<OtherBgRedPacket> {

}
