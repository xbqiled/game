package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.TipSoundConfig;
import com.youwan.game.cac.mapper.TipSoundConfigMapper;
import com.youwan.game.cac.service.TipSoundConfigService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * 
 *
 * @author code generator
 * @date 2019-05-21 11:11:20
 */
@Service("tipSoundConfigService")
public class TipSoundConfigServiceImpl extends ServiceImpl<TipSoundConfigMapper, TipSoundConfig> implements TipSoundConfigService {


    @Override
    public Boolean addSoundConfig(TipSoundConfig tipSoundConfig) {
        tipSoundConfig.setCreateTime(LocalDateTime.now());
        tipSoundConfig.setCreateBy(Objects.requireNonNull(getUsername()));
        return this.save(tipSoundConfig);
    }

    @Override
    public TipSoundConfig findByChannelId(String channelId) {
        return baseMapper.findByChannelId(channelId);
    }

    @Override
    public IPage<List<TipSoundConfig>> querySoundConfigPage(Page page, String channelId) {
        return baseMapper.querySoundConfigPage(page, channelId);
    }

    /**
     * 获取用户名称
     *
     * @return username
     */
    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }
}
