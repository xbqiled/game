package com.youwan.game.cac.service.impl;

import com.youwan.game.cac.api.dto.BettingRecordQueryDto;
import com.youwan.game.cac.service.BettingRebateService;
import com.youwan.game.common.core.betting.BettingRebateTakingRecord;
import com.youwan.game.common.core.betting.ScrubResult;
import com.youwan.game.common.core.betting.entity.ThirdPartySportsBettingRecord;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.HttpHandler;
import com.youwan.game.common.data.core.Page;
import com.youwan.game.common.data.core.XSort;
import com.youwan.game.common.data.elasticsearch.ElasticsearchOperations;
import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import com.youwan.game.common.data.elasticsearch.operations.SearchOperation;
import com.youwan.game.common.data.elasticsearch.operations.UpdateByQueryOperation;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.common.collect.MapBuilder;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.script.Script;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.youwan.game.common.data.elasticsearch.ElasticsearchOperations.DEFAULT_TYPE;
import static org.elasticsearch.index.query.QueryBuilders.*;
import static org.elasticsearch.script.Script.DEFAULT_SCRIPT_LANG;
import static org.elasticsearch.script.Script.DEFAULT_SCRIPT_TYPE;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-10.
 */
@Service
@Slf4j
public class BettingRebateServiceImpl implements BettingRebateService {

    private ElasticsearchTemplate elasticsearchTemplate;

    public BettingRebateServiceImpl(ElasticsearchTemplate elasticsearchTemplate) {
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    @Override
    public List<ScrubResult> getScrubRebateDetails(Long userId) {
        String epoch = DateTime.now().toString(DateTimeFormat.forPattern("yyyyMMdd"));
        BoolQueryBuilder query = boolQuery().must(termQuery("epoch", epoch)).must(termQuery("took", false)).must(termQuery("uid", userId));
        return elasticsearchTemplate.search(new SearchOperation<>("scrub_result", query, ScrubResult.class));
    }

    @Override
    public boolean takeScrubRebate(Long userId) {
        String epoch = DateTime.now().toString(DateTimeFormat.forPattern("yyyyMMdd"));
        BoolQueryBuilder query = boolQuery().must(termQuery("epoch", epoch)).must(termQuery("took", false)).must(termQuery("uid", userId));
        BigDecimal totalRebate = BigDecimal.valueOf(elasticsearchTemplate.sum(new SearchOperation<>("scrub_result", query), "result")).setScale(2,
                RoundingMode.HALF_UP);
        BettingRebateTakingRecord takingRecord = new BettingRebateTakingRecord();
        takingRecord.setAmount(totalRebate);
        takingRecord.setEpoch(epoch);
        takingRecord.setUid(userId);
        takingRecord.setType(BettingRebateTakingRecord.TYPE_SCRUB);
        takingRecord.setTakeTime(new Date());
        String id = elasticsearchTemplate.index("betting_rebate_taking_record", ElasticsearchOperations.DEFAULT_TYPE, takingRecord);
        Script script = new Script(DEFAULT_SCRIPT_TYPE, DEFAULT_SCRIPT_LANG, "ctx._source.took = true", Collections.emptyMap());
        elasticsearchTemplate.updateByQueryAsync(new UpdateByQueryOperation("scrub_result", DEFAULT_TYPE, query, script));
        GsonResult result = HttpHandler.takeRebate(userId, totalRebate.doubleValue(), id);
        return result != null && result.getRet() == 0;
    }

    @Override
    public Page<BettingRebateTakingRecord> getPersonTakingRecords(BettingRecordQueryDto dto) {
        BoolQueryBuilder query = boolQuery().must(termQuery("uid", dto.getUserId()));
        if (dto.getStart() != null) {
            RangeQueryBuilder rangeQuery = rangeQuery("takeTime").gte(dto.getStart() * 1000);
            if (dto.getEnd() != null) {
                rangeQuery.lte(dto.getEnd() * 1000);
            }
            query.must(rangeQuery);
        }
        return elasticsearchTemplate.searchForPage(new SearchOperation<>(
                "betting_rebate_taking_record", DEFAULT_TYPE, query, BettingRebateTakingRecord.class).setSort(XSort.desc("takeTime"))
                .setFrom(dto.getOffset()).setSize(dto.getLimit()));
    }

    @Override
    public Page<ScrubResult> queryPersonScrubRebateHistory(Long userId, Integer offset, Integer limit) {
        BoolQueryBuilder query = boolQuery().must(termQuery("uid", userId)).must(termQuery("took",true));
        return elasticsearchTemplate.searchForPage(new SearchOperation<>(
                "scrub_result", DEFAULT_TYPE, query, ScrubResult.class).setSort(XSort.desc("epoch.keyword"))
                .setFrom(offset).setSize(limit));
    }
}
