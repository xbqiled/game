package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.SysSmsConfig;
import org.apache.ibatis.annotations.Param;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-24 13:41:07
 */
public interface SysSmsConfigMapper extends BaseMapper<SysSmsConfig> {

    /**
     * <B>修改所有的配置为失效</B>
     */
    void updateSmsConfigInvalid();


    /**
     * <B>激活对应ID的配置信息</B>
     * @param id
     * @return
     */
    int  activateById(@Param("id") Integer id);


    /**
     * <B>获取短信配置信息</B>
     * @return
     */
    SysSmsConfig  queryActivateSmsConfig();


    /**
     * <B>获取配置信息</B>
     * @param id
     * @return
     */
    SysSmsConfig selectSmsSetting(@Param("id") Integer id);


    /**
     * <B>修改</B>
     * @param id
     * @param strConfig
     * @return
     */
    int  updateSmsSetting(@Param("id")Integer id, @Param("strConfig")String  strConfig);

}
