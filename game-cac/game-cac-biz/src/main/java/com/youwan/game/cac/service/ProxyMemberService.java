package com.youwan.game.cac.service;

import com.youwan.game.cac.api.dto.ProxyMemberQueryDto;
import com.youwan.game.cac.api.entity.*;
import com.youwan.game.common.data.core.Page;

import java.util.List;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-04-22.
 */
public interface ProxyMemberService {


    /**
     * @param accountId
     * @return
     */
    ProxyMemberProfile getMemberProfile(Long accountId);

    /**
     * 获取直接下级
     *
     * @param accountId
     * @param offset
     * @param limit
     * @return
     */
    Page<ProxyMember> getDirectSubordinates(Long accountId, Long memberId, int offset, int limit);

    /**
     * 获取所有下级
     *
     * @param accountId
     * @param deep
     * @param offset
     * @param limit
     * @return
     */
    Page<ProxyMember> getAllSubordinates(Long accountId, int deep, int offset, int limit);

    /**
     * 团队账变记录
     *
     * @param queryDto
     * @return
     */
    Page<ProxyMemberTreasureChange> queryProxyMemberTreasureChange(ProxyMemberQueryDto queryDto);

    /**
     * 团队充值记录
     *
     * @param queryDto
     * @return
     */
    Page<ProxyMemberRechargeOrder> queryProxyMemberRechargeOrders(ProxyMemberQueryDto queryDto);

    /**
     * 团队提现记录
     *
     * @param queryDto
     * @return
     */
    Page<ProxyMemberCashOrder> queryProxyMemberCashOrders(ProxyMemberQueryDto queryDto);

    /**
     * 团队游戏得分统计
     * @param queryDto
     * @return
     */
    //List<ProxyMemberGameScoreStats> queryProxyMemberGameScoreStats(Long accountId, Long start, Long end, boolean onlyMe, Long memberId, int deep);
    List<ProxyMemberGameScoreStats> queryProxyMemberGameScoreStats(ProxyMemberQueryDto queryDto);

    /**
     * 团队游戏得分
     *
     * @param queryDto
     * @return
     */
    Page<ProxyMemberGameScore> queryProxyMemberGameScores(ProxyMemberQueryDto queryDto);

    /**
     * 我的团队指标统计
     *
     * @return
     */
    ProxyMemberTeamMetrics queryTeamMetrics(ProxyMemberQueryDto queryDto);

    /**
     * @param queryDto
     * @return
     */
    Page<ProxyMemberTeamMetrics> querySubordinateTeamMetrics(ProxyMemberQueryDto queryDto);

    /**
     * @param queryDto
     * @return
     */
    Page<ProxyMemberRebateDetail> queryRebateDetails(ProxyMemberQueryDto queryDto);

    /**
     * @param queryDto
     * @return
     */
    Page<ProxyMemberRebateBalance> queryRebateBalances(ProxyMemberQueryDto queryDto);
}
