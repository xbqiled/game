package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.WxhhExpDto;
import com.youwan.game.cac.service.GameglobalextinfoService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * VIEW
 *
 * @author code generator
 * @date 2020-05-20 16:19:40
 */
@RestController
@AllArgsConstructor
@RequestMapping("/gameglobalextinfo")
public class GameglobalextinfoController {

  private final GameglobalextinfoService gameglobalextinfoService;

  /**
   * 分页查询
   * @param wxhhExpDto VIEW
   * @return
   */
  @GetMapping("/page")
  public R getGameglobalextinfoPage(WxhhExpDto wxhhExpDto) {
    return  new R<>(gameglobalextinfoService.getWxhhExpPage(wxhhExpDto));
  }

}
