package com.youwan.game.cac.service.impl;

import cn.hutool.core.util.ArrayUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lorne.core.framework.utils.encode.MD5Util;
import com.youwan.game.cac.api.entity.Accountinfo;
import com.youwan.game.cac.api.vo.DaMaValue;
import com.youwan.game.cac.mapper.recordlog.RecorddamachangeMapper;
import com.youwan.game.cac.mapper.xbqp.AccountinfoMapper;
import com.youwan.game.cac.service.AccountinfoService;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.jsonmodel.GsonUserGame;
import com.youwan.game.common.core.util.HttpHandler;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.joda.time.LocalDateTime;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author code generator
 * @date 2018-12-27 15:11:02
 */
@Service("accountinfoService")
public class AccountinfoServiceImpl extends ServiceImpl<AccountinfoMapper, Accountinfo> implements AccountinfoService {

    //用户打码量记录表
    private static String domainDamaChangeName = "recordlog.recorddamachange";
    private static String damaChangeName = "recorddamachange";
    @Autowired
    private RecorddamachangeMapper recorddamachangeMapper;

    @Override
    public List<DaMaValue> getDamaValueList(String channel, Integer[] userIds) {
        /*//获取今日时间信息
        List<DaMaValue> resultList = new ArrayList<>();
        Date yesterDate = DateUtil.yesterday();
        String yesterStr = DateUtil.format(yesterDate, "yyyyMMdd");

        List<String> tableList = baseMapper.queryHaveTab(damaChangeName + yesterStr);
        if (tableList != null && tableList.size() > 0) {
            List<Integer> list = new ArrayList<>();
            Collections.addAll(list, userIds);
            resultList = baseMapper.queryUserDamaValue(domainDamaChangeName + yesterStr, channel, list);
            return resultList;
        } else {
            return null;
        }*/
        List<DaMaValue> resultList = baseMapper.queryUserDamaValue(channel, Arrays.asList(userIds));
        LocalDateTime yesterDay = LocalDateTime.now(DateTimeZone.forID("Asia/Shanghai")).minusDays(1).withTime(0, 0, 0, 0);
        Integer start = Math.toIntExact(yesterDay.toDate().getTime() / 1000);
        Integer end = Math.toIntExact(yesterDay.plusDays(1).toDate().getTime() / 1000);
        List<Map<String, Object>> aggs = recorddamachangeMapper.queryDamaAgg(start, end, channel, Arrays.asList(userIds));
        Map<Integer, Long> aggMap = new HashMap<>();
        aggs.forEach(agg -> aggMap.put((Integer) agg.get("UserID"), ((BigDecimal) agg.get("damaValue")).longValue()));
        resultList.forEach(daMaValue -> {
            daMaValue.setDamaValue(aggMap.get(daMaValue.getUserId()) == null ? 0l : aggMap.get(daMaValue.getUserId()));
            daMaValue.setPhoneNumber(phoneNumberDesensitization(daMaValue.getPhoneNumber()));
        });
        return resultList;
    }


    /**
     * <B>激活用户</B>
     *
     * @param userId
     * @param state
     * @return
     */
    @Override
    public GsonResult activateUser(String userId, String state) {
        return HttpHandler.operateAccount(Integer.parseInt(userId), Integer.parseInt(state), null);
    }

    /**
     * <B>查询用户信息</B>
     *
     * @param userId
     * @return
     */
    @Override
    public Map queryUserInfo(String userId) {
        return baseMapper.queryUserInfo(Integer.parseInt(userId));
    }

    /**
     * <B>修改用户密码</B>
     *
     * @param userId
     * @param password
     * @return
     */
    @Override
    public Boolean modifyPw(String userId, String password) {
        String md5Pw = MD5Util.md5(password.getBytes());
        //调用接口修改密码
        GsonResult result = HttpHandler.modifyAccountPassword(Integer.parseInt(userId), "", password, Boolean.FALSE);
        return checkResult(result, userId, md5Pw);
    }

    /**
     * <B>重置用户密码</B>
     *
     * @param userId
     * @return
     */
    @Override
    public Boolean resetPw(String userId) {
        String md5Pw = MD5Util.md5("123456".getBytes());
        //调用接口修改密码
        GsonResult result = HttpHandler.modifyAccountPassword(Integer.parseInt(userId), "", "123456", Boolean.FALSE);
        return checkResult(result, userId, md5Pw);
    }


    private Boolean checkResult(GsonResult result, String userId, String pw) {
        if (result.getRet() == -3) {
            //表示用户不在线
            baseMapper.modifyPw(userId, pw);
            return Boolean.TRUE;
        } else if (result.getRet() == 0) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }


    /**
     * <B>修改银行密码</B>
     *
     * @param userId
     * @param password
     * @return
     */
    @Override
    public Boolean modifyBankPw(String userId, String password) {
        String md5Pw = MD5Util.md5(password.getBytes());
        baseMapper.modifyBankPw(userId, md5Pw);
        return Boolean.TRUE;
    }

    @Override
    public Boolean resetBankPw(String userId) {
        String md5Pw = MD5Util.md5("123456".getBytes());
        baseMapper.modifyBankPw(userId, md5Pw);
        return Boolean.TRUE;
    }

    @Override
    public GsonResult kickUser(Integer userId) {
        //调用接口踢人
        Integer[] accountList = {userId};
        GsonResult result = HttpHandler.kickUser(accountList);
        return result;
    }


    @Override
    public Map isInGame(Integer userId) {
        GsonUserGame userGame = HttpHandler.getUserGameList(userId);
        if (userGame != null && ArrayUtil.isNotEmpty(userGame.getList())) {
            int[] gameList = userGame.getList();
            int roomId = gameList[0];

            return baseMapper.queryGameRoom(new Integer(roomId).toString());
        }

        return null;
    }

    @Override
    public GsonResult clearCommonDev(Integer userId) {
        return HttpHandler.clearCommonDev(userId);
    }


    @Override
    public GsonResult unbindPhoneNum(Integer userId) {
        return HttpHandler.unbindPhoneNum(userId);
    }


    @Override
    public IPage<Map<String, Object>> queryUserInfoPage(Page page, Accountinfo accountinfo) {
        String userId = accountinfo != null && accountinfo.getTAccountid() != null ? accountinfo.getTAccountid().toString() : "";
        String userName = accountinfo != null && accountinfo.getTAccountname() != null ? accountinfo.getTAccountname().toString() : "";
        String nickName = accountinfo != null && accountinfo.getTNickname() != null ? accountinfo.getTNickname().toString() : "";
        String channelId = accountinfo != null && accountinfo.getTChannelkey() != null ? accountinfo.getTChannelkey().toString() : "";
        IPage<Map<String, Object>> ret = baseMapper.queryUserInfo(page, StringUtils.isNumeric(userId) ? Integer.parseInt(userId) : null, userName,
                nickName, channelId);
        ret.getRecords().forEach(item -> item.put("tPhonenumber", phoneNumberDesensitization((String) item.get("tPhonenumber"))));
        return ret;
    }

    //desensitization
    private String phoneNumberDesensitization(String orgNumber) {
        if (StringUtils.isBlank(orgNumber))
            return orgNumber;
        if (orgNumber.length() >= 11) {
            return StringUtils.left(orgNumber, 3) + "*****" + StringUtils.right(orgNumber, 3);
        } else {
            return orgNumber;
        }

    }
}
