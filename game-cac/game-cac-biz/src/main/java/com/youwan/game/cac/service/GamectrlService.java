package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.GameCtrlDto;
import com.youwan.game.cac.api.entity.Gamectrl;
import com.youwan.game.cac.api.vo.GameCtrlVo;

import java.util.List;
import java.util.Map;


/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-11 17:01:03
 */
public interface GamectrlService extends IService<Gamectrl> {


     IPage<List<GameCtrlVo>> queryGameCtrlPage(Page page, String gameId);


     List<Map<String, Object>> getRoomTreeData();


     int doSet(GameCtrlDto gameCtrlDto);
}
