package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.BindUrlEntity;
import com.youwan.game.cac.api.entity.BindUrlForm;
import com.youwan.game.cac.service.BindUrlService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-18 18:58:08
 */
@RestController
@AllArgsConstructor
@RequestMapping("/bindUrl")
public class BindUrlController {

    private BindUrlService bindUrlService;

    /**
     * 列表
     */
    @RequestMapping("/page")
    public R page(Page page, QueryDto queryDto){
        return new R<>(bindUrlService.queryPage(page, queryDto));
    }

    @RequestMapping("/info/{channelId}")
    @PreAuthorize("@pms.hasPermission('cac_bindUrl_view')")
    public R info(@PathVariable("channelId") String channelId){
        BindUrlEntity entity = bindUrlService.getByChannelId(channelId);
        return new R<>(entity);
    }


    @SysLog("配置推广域名")
    @RequestMapping("/doConfig")
    @PreAuthorize("@pms.hasPermission('cac_bindUrl_config')")
    public R doConfig(@RequestBody BindUrlForm bindUrlForm) {
        int result = bindUrlService.saveEntity(bindUrlForm);
        if (result == 0) {
            return new R<>();
        } else {
            return new R<>(null,"配置推广域名失败!");
        }
    }

    /**
     * 删除
     */
    @SysLog("删除推广绑定域名配置")
    @RequestMapping("/delete")
    @PreAuthorize("@pms.hasPermission('cac_bindUrl_del')")
    public R delete(@RequestBody BindUrlForm bindUrlForm){
        int result = bindUrlService.deleteEntity(bindUrlForm);
        if (result == 0) {
            return new R<>();
        } else {
            return new R<>(null,"删除推广绑定域名失败!");
        }
    }
}
