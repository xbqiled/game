package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.DownloadConfigDto;
import com.youwan.game.cac.api.entity.CfgChlDownload;
import com.youwan.game.cac.api.vo.DownloadPanelConfigVo;

import java.util.List;


/**
 * 
 *
 * @author code generator
 * @date 2019-11-18 16:08:14
 */
public interface CfgChlDownloadService extends IService<CfgChlDownload> {

    IPage<List<CfgChlDownload>> queryDownloadCfgPage(Page page, DownloadConfigDto downloadConfigDto);

    DownloadPanelConfigVo getConfigByChannelId(String channelId);

    Boolean saveDownloadPanelCfg(CfgChlDownload cfgChlDownload);

    Boolean modifyDownloadPanelCfg(CfgChlDownload cfgChlDownload);
}
