package com.youwan.game.cac.controller;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.VersionConfigDto;
import com.youwan.game.cac.api.entity.SysVersionConfig;
import com.youwan.game.cac.service.SysVersionConfigService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.common.security.annotation.Inner;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * 
 *
 * @author code generator
 * @date 2019-02-08 16:36:30
 */
@RestController
@AllArgsConstructor
@RequestMapping("/sysversionconfig")
public class SysVersionConfigController {

  private final SysVersionConfigService sysVersionConfigService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param sysVersionConfig
   * @return
   */
  @GetMapping("/page")
  public R getSysVersionConfigPage(Page page, SysVersionConfig sysVersionConfig) {
    return new R<>(sysVersionConfigService.page(page, Wrappers.query(sysVersionConfig)));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(sysVersionConfigService.getById(id));
  }

  /**
   * 新增
   * @param sysVersionConfig 
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  @PreAuthorize("@pms.hasPermission('cac_sysversionconfig_add')")
  public R save(@RequestBody SysVersionConfig sysVersionConfig){
    return new R<>(sysVersionConfigService.saveVersionConfig(sysVersionConfig));
  }

  /**
   * 修改
   * @param sysVersionConfig 
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  @PreAuthorize("@pms.hasPermission('cac_sysversionconfig_edit')")
  public R updateById(@RequestBody SysVersionConfig sysVersionConfig){
    return new R<>(sysVersionConfigService.updateVersionConfigById(sysVersionConfig));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  @PreAuthorize("@pms.hasPermission('cac_sysversionconfig_del')")
  public R removeById(@PathVariable Integer id){
    return new R<>(sysVersionConfigService.removeById(id));
  }


  /**
   * <B>获取版本配置信息</B>
   * @param config
   * @return
   */
  @Inner
  @PostMapping("/config")
  public R getVersionConfig(@Valid @RequestBody VersionConfigDto config) {
    return new R<>(sysVersionConfigService.queryModelByChannelId(config));
  }

}
