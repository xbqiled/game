package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.Userbaseattr;
import com.youwan.game.cac.service.UserbaseattrService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-02-01 14:44:56
 */
@RestController
@AllArgsConstructor
@RequestMapping("/tuserbaseattr")
public class UserbaseattrController {

  private final UserbaseattrService userbaseattrService;

  /**
   * 分页查询
   *
   * @param page          分页对象
   * @param userbaseattr
   * @return
   */
  @GetMapping("/page")
  public R getTUserbaseattrPage(Page page, Userbaseattr userbaseattr) {
    return new R<>(userbaseattrService.page(page, Wrappers.query(userbaseattr)));
  }


  /**
   * 通过id查询
   * @param tAccountid id
   * @return R
   */
  @GetMapping("/{tAccountid}")
  public R getById(@PathVariable("tAccountid") Integer tAccountid){
    Userbaseattr userbaseattr = userbaseattrService.getById(tAccountid);
    return new R<>(userbaseattr);
  }

  /**
   * 新增
   * @param userbaseattr
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody Userbaseattr userbaseattr){
    return new R<>(userbaseattrService.save(userbaseattr));
  }

  /**
   * 修改
   * @param userbaseattr
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody Userbaseattr userbaseattr){
    return new R<>(userbaseattrService.updateById(userbaseattr));
  }

  /**
   * 通过id删除
   * @param tAccountid id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{tAccountid}")
  public R removeById(@PathVariable Integer tAccountid){
    return new R<>(userbaseattrService.removeById(tAccountid));
  }

}
