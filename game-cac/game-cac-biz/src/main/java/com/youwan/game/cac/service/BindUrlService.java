package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.BindUrlEntity;
import com.youwan.game.cac.api.entity.BindUrlForm;
import com.youwan.game.cac.api.vo.BindUrlVo;
import com.youwan.game.cac.api.vo.NickAwardVo;

import java.util.List;

public interface BindUrlService extends IService<BindUrlEntity> {

    IPage<BindUrlVo> queryPage(Page page, QueryDto queryDto);

    BindUrlEntity getByChannelId(String channelId);

    int deleteEntity(BindUrlForm bindUrlForm);

    int saveEntity(BindUrlForm bindUrlForm);

}
