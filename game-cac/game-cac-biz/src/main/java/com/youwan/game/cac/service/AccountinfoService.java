package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.Accountinfo;
import com.youwan.game.cac.api.vo.DaMaValue;
import com.youwan.game.common.core.jsonmodel.GsonResult;

import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author code generator
 * @date 2018-12-27 15:11:02
 */
public interface AccountinfoService extends IService<Accountinfo> {


    List<DaMaValue> getDamaValueList(String channel, Integer[] userIds);


    /**
     * <B>修改用户状态信息</B>
     * @param userId
     * @return
     */
    GsonResult activateUser( String userId, String state);


    /**
     * <B>查询用户信息</B>
     * @param userId
     * @return
     */
    Map queryUserInfo(String userId);


    /**
     * <B>用户密码修改</B>
     * @param userId
     * @param password
     * @return
     */
    Boolean modifyPw(String userId, String password);


    /**
     * <B>重置用户密码</B>
     * @param userId
     * @return
     */
    Boolean resetPw(String userId);


    /**
     * <B>银行密码修改</B>
     * @param userId
     * @param password
     * @return
     */
    Boolean modifyBankPw(String userId, String password);


    /**
     * <B>重置银行密码</B>
     * @param userId
     * @return
     */
    Boolean resetBankPw(String userId);


    /**
     * <B>踢人</B>
     * @param userId
     * @return
     */
    GsonResult kickUser(Integer userId);


    /**
     * <B> 查询用户所在游戏</B>
     * @return
     */
    Map isInGame(Integer userId);


    /**
     * <B>分页查询</B>
     * @param page
     * @param accountinfo
     * @return
     */
    IPage<Map<String, Object>> queryUserInfoPage(Page page, Accountinfo accountinfo);



    GsonResult  clearCommonDev(Integer userId);


    GsonResult  unbindPhoneNum(Integer userId);
}
