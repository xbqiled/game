package com.youwan.game.cac.mapper.xbqp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.Depositchannel;
import com.youwan.game.cac.api.vo.DepositchannelVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-05 14:15:52
 */
public interface DepositchannelMapper extends BaseMapper<Depositchannel> {

    IPage<DepositchannelVo> queryDepositPage(Page page, @Param("channelId") String channelId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    Depositchannel getDepositByChannelId( @Param("channelId") String channelId);

}
