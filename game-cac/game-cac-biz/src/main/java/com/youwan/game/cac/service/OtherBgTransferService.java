package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.BgBetDto;
import com.youwan.game.cac.api.dto.BgTransferDto;
import com.youwan.game.cac.api.entity.OtherBgTransfer;
import com.youwan.game.cac.api.entity.OtherBgbetRecord;

import java.util.List;


/**
 * 
 *
 * @author code generator
 * @date 2019-11-05 15:57:02
 */
public interface OtherBgTransferService extends IService<OtherBgTransfer> {


    IPage<List<OtherBgTransfer>> queryBgTransferPage(Page page, BgTransferDto bgTransferDto);

}
