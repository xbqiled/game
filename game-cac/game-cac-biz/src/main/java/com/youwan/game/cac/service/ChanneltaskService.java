package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.ChanneltaskEntity;
import com.youwan.game.cac.api.entity.TaskForm;
import com.youwan.game.cac.api.vo.TaskVo;
import com.youwan.game.common.core.jsonmodel.GsonTask;

import java.util.List;
import java.util.Map;

public interface ChanneltaskService extends IService<ChanneltaskEntity> {

    IPage<TaskVo> queryPage(Page page, QueryDto queryDto);

    int channelOpenType(String channelId, String openType);

    Map<String, Object> findByChannelId(String channelId);

    int saveOrUpdateConfig(TaskForm taskForm);

}
