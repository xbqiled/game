package com.youwan.game.cac.mapper.xbqp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.Gamectrlcfg;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2020-03-27 20:25:55
 */
public interface GamectrlcfgMapper extends BaseMapper<Gamectrlcfg> {

    IPage<Map<String, Object>> queryGameCtrlCfgPage(Page page, @Param("startTime") String channelId, @Param("endTime") String endTime);

    Gamectrlcfg getByGameId(@Param("gameId") String gameId);


}
