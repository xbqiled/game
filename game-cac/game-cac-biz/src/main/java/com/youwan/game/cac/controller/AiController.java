package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.Ai;
import com.youwan.game.cac.service.AiService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-01-21 19:13:24
 */
@RestController
@AllArgsConstructor
@RequestMapping("/bot")
public class AiController {

  private final AiService aiService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param tnick
   * @param tid
   * @return
   */
  @GetMapping("/page")
  public R getTAiPage(Page page, String tnick, String tid) {
    return  new R<>(aiService.queryBotPage(page, tnick, tid));
  }


  /**
   * 通过id查询
   * @param tId id
   * @return R
   */
  @GetMapping("/{tId}")
  public R getById(@PathVariable("tId") Integer tId){
    return new R<>(aiService.getById(tId));
  }

  /**
   * 新增
   * @param ai
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody Ai ai){
    return new R<>(aiService.save(ai));
  }

  /**
   * 修改
   * @param ai
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  @PreAuthorize("@pms.hasPermission('cac_bot_edit')")
  public R updateById(@RequestBody Ai ai){
    Boolean result = aiService.modifyBot(ai);
    return  result ? new R<>() :  new R<>().setCode(1).setMsg("修改BOT信息失败!");
  }

  /**
   * 通过id删除
   * @param tId id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{tId}")
  public R removeById(@PathVariable Integer tId){
    return new R<>(aiService.removeById(tId));
  }

}
