package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.TipOrder;

/**
 * 
 *
 * @author code generator
 * @date 2019-05-21 11:20:06
 */
public interface TipOrderMapper extends BaseMapper<TipOrder> {

}
