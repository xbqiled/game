package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.OtherBgRedPacketExchange;

/**
 * 
 *
 * @author code generator
 * @date 2020-01-13 20:43:08
 */
public interface OtherBgRedPacketExchangeMapper extends BaseMapper<OtherBgRedPacketExchange> {

}
