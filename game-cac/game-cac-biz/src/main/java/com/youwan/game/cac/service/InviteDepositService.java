package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.InviteDeposit;
import com.youwan.game.cac.api.vo.InviteDepositChannelVo;

import java.util.List;


/**
 * VIEW
 *
 * @author code generator
 * @date 2019-08-03 15:31:04
 */
public interface InviteDepositService extends IService<InviteDeposit> {

    InviteDepositChannelVo findByChannelId(String channelId);

    int channelOpenType(String channelId, String openType);

    IPage<List<InviteDepositChannelVo>> queryInviteDepositPage(Page page, QueryDto queryDto);

    int saveInviteDepositCfg(String channelId, Long openType, Long timeType, Long beginTime, Long endTime, Long firstTotalDeposit, Long selfReward, Long upperReward);

}
