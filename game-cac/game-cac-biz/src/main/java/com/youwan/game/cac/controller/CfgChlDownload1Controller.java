package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.DownloadConfigDto;
import com.youwan.game.cac.api.entity.CfgChlDownload1;
import com.youwan.game.cac.service.CfgChlDownload1Service;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.common.security.annotation.Inner;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 *
 *
 * @author code generator
 * @date 2019-12-30 15:25:40
 */
@RestController
@AllArgsConstructor
@RequestMapping("/downloadpanelcfg1")
public class CfgChlDownload1Controller {

  private final CfgChlDownload1Service cfgChlDownload1Service;

  /**
   * 分页查询
   * @param page 分页对象
   * @param downloadConfigDto
   * @return
   */
  @GetMapping("/page")
  public R getConfigChannelDownloadPage(Page page, DownloadConfigDto downloadConfigDto) {
    return new R<>(cfgChlDownload1Service.queryDownloadCfgPage(page, downloadConfigDto));
  }


  /**
   * 通过id查询
   * @param channelId
   * @return R
   */
  @GetMapping("/getConfig/{channelId}")
  public R getById(@PathVariable("channelId") String channelId){
    return new R<>(cfgChlDownload1Service.getConfigByChannelId(channelId));
  }

  /**
   * 新增
   * @param cfgChlDownload1
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody CfgChlDownload1 cfgChlDownload1){
    Boolean result = cfgChlDownload1Service.saveDownloadPanelCfg(cfgChlDownload1);
    if (result) {
      return new R<>();
    } else {
      return new R<>().setCode(1).setMsg("渠道配置重复");
    }
  }

  /**
   * 修改
   * @param cfgChlDownload1
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody  CfgChlDownload1 cfgChlDownload1){
    return new R<>(cfgChlDownload1Service.modifyDownloadPanelCfg(cfgChlDownload1));
  }


  /**
   *
   * @param
   * @return
   */
  @Inner
  @PostMapping("/getconfig")
  public R getDownloadPanelConfig(@RequestBody DownloadConfigDto downloadConfigDto){
    return new R<>(cfgChlDownload1Service.getConfigByChannelId(downloadConfigDto.getChannelId()));
  }
}
