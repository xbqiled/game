package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.TipSoundConfig;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-05-21 11:11:20
 */
public interface TipSoundConfigMapper extends BaseMapper<TipSoundConfig> {

    IPage<List<TipSoundConfig>> querySoundConfigPage(Page page, @Param("channelId") String channelId);

    TipSoundConfig findByChannelId(@Param("channelId") String channelId);

}
