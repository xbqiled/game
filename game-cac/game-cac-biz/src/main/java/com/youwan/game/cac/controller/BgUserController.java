package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.*;
import com.youwan.game.cac.api.entity.OtherBgUser;
import com.youwan.game.cac.service.BgenterConfigService;
import com.youwan.game.cac.service.OtherBgUserService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.common.security.annotation.Inner;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-11-05 15:57:07
 */
@RestController
@AllArgsConstructor
@RequestMapping("/bguser")
public class BgUserController {

  private final OtherBgUserService otherBgUserService;

  private final BgenterConfigService bgenterConfigService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param bgUserDto
   * @return
   */
  @GetMapping("/page")
  public R getOtherBgUserPage(Page page, BgUserDto bgUserDto) {
    return  new R<>(otherBgUserService.queryBgUserPage(page, bgUserDto));
  }


  @GetMapping("/{id}")
  public R getById(@PathVariable("id") String id){
    return new R<>(otherBgUserService.getUserDetails(id));
  }


  /**
   * 改变用户状态
   * @param
   * @return
   */
  @GetMapping("/changeStatus")
  public R changeStatus(ChannelBgUserStatusDto channelBgUserStatusDto){
    return new R<>(otherBgUserService.changeStatus(channelBgUserStatusDto));
  }


  /**
   * 通过id查询
   * @return R
   */
  @Inner
  @PostMapping("/finduser")
  public R findUser(@RequestBody BgUserDto bgUserDto){
    return new R<>(otherBgUserService.findUser(bgUserDto.getChannelId(), bgUserDto.getUserId()));
  }


  /**
   * <B>获取配置信息</B>
   * @return
   */
  @Inner
  @PostMapping("/getenterconfig")
  public R getEnerConfig(@RequestBody BgEnterConfig bgEnterConfig){
    return new R<>(bgenterConfigService.getConfigByChannelId(bgEnterConfig.getChannelId()));
  }

  /**
   * <B>创建用户</B>
   * @param bgUserDto
   * @return
   */
  @Inner
  @PostMapping("/createuser")
  public R createUser(@RequestBody BgUserDto bgUserDto){
    return new R<>(otherBgUserService.createUser(bgUserDto.getChannelId(), bgUserDto.getUserId(), bgUserDto.getNickName()));
  }

  /**
   * <B>打开游戏</B>
   * @param openBgGameDto
   * @return
   */
  @Inner
  @PostMapping("/opengame")
  public R openGame(@RequestBody OpenBgGameDto openBgGameDto){
    return new R<>(otherBgUserService.openGame(openBgGameDto.getIsWx(), openBgGameDto.getIsMobileUrl(), openBgGameDto.getIsHttpsUrl(),
            openBgGameDto.getChannelId(), openBgGameDto.getUserId(), openBgGameDto.getSysBalance(), openBgGameDto.getPassword()));
  }

  /**
   * <B>用户余额信息</B>
   * @param bgUserBalanceDto
   * @return
   */
  @Inner
  @PostMapping("/userbalance")
  public R userBalance(@RequestBody BgUserBalanceDto bgUserBalanceDto){
    return new R<>(otherBgUserService.getUserBalance(bgUserBalanceDto.getUserId()));
  }

  /**
   * <B>获取配置信息</B>
   * @return
   */
  @Inner
  @PostMapping("/getBgConfig")
  public R getBgConfig(){
    return new R<>(otherBgUserService.getBgConfig());
  }

  /**
   * <B>退出游戏</B>
   * @param gExitGameDto
   * @return
   */
  @Inner
  @PostMapping("/exitgame")
  public R exitGame(@RequestBody BgExitGameDto gExitGameDto){
    return new R<>(otherBgUserService.exitGame(gExitGameDto));
  }
  /**
   * <B>额度转换</B>
   * @param bgTransferBalanceDto
   * @return
   */
  @PostMapping("/transferbalance")
  public R transferBalance(@RequestBody BgTransferBalanceDto bgTransferBalanceDto){
    return new R<>(otherBgUserService.transferbalance(bgTransferBalanceDto.getUserId(), bgTransferBalanceDto.getAmount(), bgTransferBalanceDto.getType(), bgTransferBalanceDto.getChannelId(), bgTransferBalanceDto.getReason()));
  }

  /**
   * <B>获取系统余额</B>
   * @param bgUserBalanceDto
   * @return
   */
  @PostMapping("/sysbalance")
  public R sysBalance(@RequestBody BgUserBalanceDto bgUserBalanceDto){
    return new R<>(otherBgUserService.getSysBalance(bgUserBalanceDto.getUserId()));
  }

  /**
   * 新增
   * @param otherBgUser 
   * @return R
   */
  @SysLog("创建BG用户")
  @PostMapping
  public R save(@RequestBody OtherBgUser otherBgUser){
    return new R<>(otherBgUserService.save(otherBgUser));
  }

  /**
   * 修改
   * @param otherBgUser 
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody OtherBgUser otherBgUser){
    return new R<>(otherBgUserService.updateById(otherBgUser));
  }


  /**
   * <B>获取用户余额信息</B>
   * @param otherBgUser
   * @return
   */
  @PostMapping("/getUserBalance")
  public R getUserBalance(@RequestBody OtherBgUser otherBgUser){
    return new R<>(otherBgUserService.updateById(otherBgUser));
  }

  /**
   * <B>修改用户状态</B>
   * @return
   */
  @PostMapping("/modifyUserStatus")
  public R modifyUserStatus(@RequestBody ChannelBgUserStatusDto channelBgUserStatusDto){
    return new R<>(otherBgUserService.changeStatus(channelBgUserStatusDto));
  }

}
