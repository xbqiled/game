package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.LuckyRoulette;
import com.youwan.game.common.core.jsonmodel.GsonLuckRoulette;

import java.util.List;
import java.util.Map;


/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-22 15:14:43
 */
public interface LuckyRouletteService extends IService<LuckyRoulette> {

    IPage<List<Map<String, Object>>>  queryLuckyRoulettePage(Page page, LuckyRoulette luckyRoulette);

    int saveLuckRoulette(String channelId, List<GsonLuckRoulette> diamondcfg, List<GsonLuckRoulette> goldcfg, List<GsonLuckRoulette> silvercfg, String startTimeStr, String finishTimeStr, Integer goldcost, Integer diamondcost, Integer silvercost);

}
