package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.ChlLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统日志
 *
 * @author code generator
 * @date 2019-05-23 13:09:56
 */
public interface ChlLogMapper extends BaseMapper<ChlLog> {

    IPage<List<ChlLog>> queryLogPage(Page page, @Param("channelId") String channelId,
                                    @Param("userId") String userId, @Param("userName") String userName, @Param("operation") String operation,
                                    @Param("startTime") String startTime, @Param("endTime") String endTime);
}
