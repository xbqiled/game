package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.service.StatisAccountService;
import com.youwan.game.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 
 *
 * @author code generator
 * @date 2019-06-16 12:32:18
 */
@RestController
@AllArgsConstructor
@RequestMapping("/statisaccount")
public class StatisAccountController {

  private final StatisAccountService statisAccountService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param queryDto
   * @return
   */
  @GetMapping("/page")
  public R getStatisAccountPage(Page page, QueryDto queryDto) {
    return new R<>(statisAccountService.queryStatisDataPage(page, queryDto));
  }

  @RequestMapping("/statisList")
  public R statisList(Page page, QueryDto queryDto) {
    return new R<>(statisAccountService.queryStatisAccountPage(page, queryDto));
  }
}
