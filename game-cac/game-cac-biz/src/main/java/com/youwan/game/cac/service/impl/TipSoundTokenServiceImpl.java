package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.TipSoundToken;
import com.youwan.game.cac.mapper.TipSoundTokenMapper;
import com.youwan.game.cac.service.TipSoundTokenService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-05-21 11:05:27
 */
@Service("tipSoundTokenService")
public class TipSoundTokenServiceImpl extends ServiceImpl<TipSoundTokenMapper, TipSoundToken> implements TipSoundTokenService {

    @Override
    public IPage<List<TipSoundToken>> querySoundTokenPage(Page page, String channelId) {
        return baseMapper.querySoundTokenPage(page, channelId);
    }
}
