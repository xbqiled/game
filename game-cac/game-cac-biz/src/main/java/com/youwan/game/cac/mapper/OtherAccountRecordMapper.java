package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.AccountRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-20 13:02:18
 */
public interface OtherAccountRecordMapper extends BaseMapper<AccountRecord> {

    IPage<List<AccountRecord>> queryAccountRecordPage(Page page, @Param("channelId") String channelId, @Param("startTime") String startTime, @Param("endTime") String endTime);

}
