package com.youwan.game.cac.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.youwan.game.cac.api.dto.LuckyRouletteDto;
import com.youwan.game.cac.api.entity.LuckyRoulette;
import com.youwan.game.cac.api.vo.LuckyRouletteVo;
import com.youwan.game.cac.service.LuckyRouletteService;
import com.youwan.game.common.core.constant.ConfigConstant;
import com.youwan.game.common.core.jsonmodel.GsonLuckRouletteOptions;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.*;


/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-22 15:14:43
 */
@RestController
@AllArgsConstructor
@RequestMapping("/luckyroulette")
public class LuckyRouletteController {

    private final LuckyRouletteService luckyRouletteService;

    /**
     * 分页查询
     *
     * @param page          分页对象
     * @param luckyRoulette VIEW
     * @return
     */
    @GetMapping("/page")
    public R getLuckyroulettePage(Page page, LuckyRoulette luckyRoulette) {
        return new R<>(luckyRouletteService.queryLuckyRoulettePage(page, luckyRoulette));
    }


    /**
     * 通过id查询VIEW
     *
     * @param channelId
     * @return R
     */
    @GetMapping("/{channelId}")
    @PreAuthorize("@pms.hasPermission('cac_luckyroulette_view')")
    public R getById(@PathVariable("channelId") String channelId) {
        LuckyRoulette luckyroulette = luckyRouletteService.getById(channelId);

        if (luckyroulette != null && StrUtil.isNotEmpty(luckyroulette.getTChannel())) {

            String diamondcfg = new String(luckyroulette.getTDiamondcfg());
            String goldcfg = new String(luckyroulette.getTGoldcfg());
            String silvercfg = new String(luckyroulette.getTSilvercfg());

            LuckyRouletteVo vo = new LuckyRouletteVo();
            BeanUtils.copyProperties(luckyroulette, vo);
            Long startTime = luckyroulette.getTStart();

            Long finishTime = luckyroulette.getTFinish();
            vo.setStartTimeStr(DateUtil.format(new Date(startTime * 1000), ConfigConstant.FULL_DATE_TIME_FORMAT));
            vo.setFinishTimeStr(DateUtil.format(new Date(finishTime * 1000), ConfigConstant.FULL_DATE_TIME_FORMAT));

            List<GsonLuckRouletteOptions> diamondList = packLuckRoulette(diamondcfg);
            List<GsonLuckRouletteOptions> goldList = packLuckRoulette(goldcfg);
            List<GsonLuckRouletteOptions> silverList = packLuckRoulette(silvercfg);

            Map<String , Object> map = new HashMap<String , Object>();
            map.put("diamondList", diamondList);
            map.put("goldList", goldList);

            map.put("silverList", silverList);
            map.put("luckyroulette", vo);
            return new R<>().setData(map);
        }
        return new R<>(luckyRouletteService.getById(channelId));
    }


    private List<GsonLuckRouletteOptions> packLuckRoulette(String cfg) {
        List<GsonLuckRouletteOptions> list = new ArrayList<>();
        JsonParser parser = new JsonParser();
        Gson gson = new Gson();

        if (StrUtil.isNotEmpty(cfg)) {
            JsonArray jsonArray = parser.parse(cfg).getAsJsonArray();
            for (JsonElement user : jsonArray) {
                //使用GSON，直接转成Bean对象

                GsonLuckRouletteOptions gsonLuckRoulette = gson.fromJson(user, GsonLuckRouletteOptions.class);
                String str = getResName(gsonLuckRoulette.getRes());
                gsonLuckRoulette.setName(str);

                gsonLuckRoulette.setValue(gsonLuckRoulette.getRate());
                list.add(gsonLuckRoulette);
            }
        }
        return list;
    }


    private String getResName(Integer res) {
        String resultStr = "";
        switch (res) {
            case 1:
                resultStr = "1金币";
                break;

            case 2:
                resultStr = "2金币";
                break;

            case 3:
                resultStr = "红包";
                break;

            case 4:
                resultStr = "金色红包";
                break;

            case 5:
                resultStr = "钱包";
                break;

            case 6:
                resultStr = "钱袋";
                break;

            case 7:
                resultStr = "宝箱";
                break;
        }

        return resultStr;
    }


    /**
     * 新增VIEW
     *
     * @param luckyRouletteDto VIEW
     * @return R
     */
    @SysLog("配置幸运轮盘")
    @PostMapping
    @PreAuthorize("@pms.hasPermission('cac_luckyroulette_config')")
    public R doConfig(@RequestBody LuckyRouletteDto luckyRouletteDto) {
        if (StrUtil.isEmpty(luckyRouletteDto.getChannelId())) {
            return new R<>().setCode(1).setMsg("渠道编码不能为空!");
        }

        if (CollUtil.isEmpty(luckyRouletteDto.getDiamond())) {
            return new R<>().setCode(1).setMsg("钻石轮盘配置不能为空!");
        }

        if (CollUtil.isEmpty(luckyRouletteDto.getGold())) {
            return new R<>("黄金轮盘配置不能为空!").setCode(1).setMsg("钻石轮盘配置不能为空!");
        }

        if (CollUtil.isEmpty(luckyRouletteDto.getSilver())) {
            return new R<>("白银轮盘配置不能为空!").setCode(1).setMsg("钻石轮盘配置不能为空!");
        }

        if (StrUtil.isEmpty(luckyRouletteDto.getStart())) {
            return new R<>().setCode(1).setMsg("活动开始时间不能为空!");
        }

        if (StrUtil.isEmpty(luckyRouletteDto.getFinish())) {
            return new R<>().setCode(1).setMsg("活动结束时间不能为空!");
        }

        if (StrUtil.equals(luckyRouletteDto.getStart(), luckyRouletteDto.getFinish())) {
            return new R<>().setCode(1).setMsg("活动开始时间与活动结束时间不能相同!");
        }

        if (luckyRouletteDto.getSilverCost().intValue() == 0) {
            return new R<>().setCode(1).setMsg("白银轮盘消耗积分不能为空!");
        }

        if (luckyRouletteDto.getGoldCost().intValue() == 0) {
            return new R<>().setCode(1).setMsg("黄金轮盘消耗积分不能为空!");
        }

        if (luckyRouletteDto.getDiamondCost().intValue() == 0) {
            return new R<>().setCode(1).setMsg("钻石轮盘消耗积分不能为空!");
        }

        int result = luckyRouletteService.saveLuckRoulette(luckyRouletteDto.getChannelId(), luckyRouletteDto.getDiamond(), luckyRouletteDto.getGold(),
                luckyRouletteDto.getSilver(), luckyRouletteDto.getStart(), luckyRouletteDto.getFinish(), luckyRouletteDto.getGoldCost(),
                luckyRouletteDto.getDiamondCost(), luckyRouletteDto.getSilverCost());

        if (result == 0) {
            return new R<>("配置幸运轮盘成功!");
        } else {
            return new R<>("配置幸运轮盘失败!").setCode(1);
        }
    }


}
