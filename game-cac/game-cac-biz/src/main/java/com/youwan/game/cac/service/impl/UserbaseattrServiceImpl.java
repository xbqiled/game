package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.Userbaseattr;
import com.youwan.game.cac.mapper.xbqp.UserbaseattrMapper;
import com.youwan.game.cac.service.UserbaseattrService;
import org.springframework.stereotype.Service;

/**
 * 
 *
 * @author code generator
 * @date 2019-02-01 14:44:56
 */
@Service("userbaseattrService")
public class UserbaseattrServiceImpl extends ServiceImpl<UserbaseattrMapper, Userbaseattr> implements UserbaseattrService {

}
