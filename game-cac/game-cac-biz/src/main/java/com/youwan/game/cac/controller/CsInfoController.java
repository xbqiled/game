package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.CsInfoDto;
import com.youwan.game.cac.api.entity.CsInfo;
import com.youwan.game.cac.api.entity.SysSmsConfig;
import com.youwan.game.cac.service.CsInfoService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.common.security.annotation.Inner;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-03-29 15:44:57
 */
@RestController
@AllArgsConstructor
@RequestMapping("/csinfo")
public class CsInfoController {

  private final CsInfoService csInfoService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param csInfo
   * @return
   */
  @GetMapping("/page")
  public R getTCsInfoPage(Page page, CsInfo csInfo) {
    return new R<>(csInfoService.queryCsInfoPage(page, csInfo));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(csInfoService.getById(id));
  }

  /**
   * 新增
   * @param tCsInfo 
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody CsInfo tCsInfo){
    return new R<>(csInfoService.saveCsinfo(tCsInfo));
  }

  /**
   * 修改
   * @param tCsInfo 
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody CsInfo tCsInfo){
    return new R<>(csInfoService.updateById(tCsInfo));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(csInfoService.removeById(id));
  }


  @Inner
  @PostMapping("/getCsInfoById/{channelId}")
  public R getCsInfoByChannelId(@PathVariable String channelId){
    return new R<>(csInfoService.getCsInfoByChannelId(channelId));
  }

  @Inner
  @PostMapping("/getCsInfo")
  public R getCsInfo(@RequestBody CsInfoDto csInfoDto){
    return new R<>(csInfoService.getCsInfo(csInfoDto));
  }


}
