package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.ShortUrl;

import java.util.List;


/**
 * 
 *
 * @author code generator
 * @date 2019-04-19 13:17:59
 */
public interface ShortUrlService extends IService<ShortUrl> {

    IPage<List<ShortUrl>> queryShortUrlPage(Page page,  ShortUrl url);

    String createShortUrl(String url, ShortUrl shortUrl);

    Boolean saveShortUrl(ShortUrl shortUrl);

    Boolean updateShortById(ShortUrl shortUrl);

    Boolean deleteShortById(Integer id);

    List<ShortUrl> getAllShortUrlConfig();

}
