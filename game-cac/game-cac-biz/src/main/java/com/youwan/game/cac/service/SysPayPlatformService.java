package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.SysPayPlatform;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-27 16:32:02
 */
public interface SysPayPlatformService extends IService<SysPayPlatform> {

    /**
     * <B>添加信息</B>
     * @param sysPayPlatform
     * @return
     */
    Boolean savePlatform(SysPayPlatform sysPayPlatform);

    /**
     * <B>修改信息</B>
     * @param sysPayPlatform
     * @return
     */
    Boolean updatePlatformById(SysPayPlatform sysPayPlatform);


    /**
     * <B>删除信息</B>
     * @param id
     * @return
     */
    Boolean deletePlatformById(Integer id);

    /**
     * <B>分页查询</B>
     * @param page
     * @param company
     * @return
     */
    IPage<List<SysPayPlatform>> queryPayPlatformPage(Page page, String company);



    SysPayPlatform getPlatformById(Integer id);

}
