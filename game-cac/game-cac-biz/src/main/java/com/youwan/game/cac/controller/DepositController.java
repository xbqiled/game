package com.youwan.game.cac.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.DepositDto;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.Depositchannel;
import com.youwan.game.cac.service.DepositchannelService;
import com.youwan.game.common.core.constant.ConfigConstant;
import com.youwan.game.common.core.jsonmodel.GsonRechargeRebateCfg;
import com.youwan.game.common.core.jsonmodel.GsonSignAwardCfg;
import com.youwan.game.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.*;


/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-05 14:15:52
 */
@RestController
@AllArgsConstructor
@RequestMapping("/deposit")
public class DepositController {

  private final DepositchannelService depositchannelService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param queryDto VIEW
   * @return
   */
  @GetMapping("/page")
  public R getDepositPage(Page page, QueryDto queryDto) {
    return  new R<>(depositchannelService.queryDepositPage(page,  queryDto));
  }


  /**
   * 信息
   */
  @GetMapping("/{channelId}")
  public R info(@PathVariable("channelId") String channelId){
    Depositchannel depositchannel = depositchannelService.findByChannelId(channelId);
    List<GsonRechargeRebateCfg> cfg = new ArrayList<>();

    String str = new String(depositchannel.getTCfginfo());
    if (StrUtil.isNotBlank(str)) {
      GsonRechargeRebateCfg[] array = new Gson().fromJson(str, GsonRechargeRebateCfg[].class);
      cfg = Arrays.asList(array);
    }

    String startTime = DateUtil.format(new Date(depositchannel.getTStarttime() * 1000), ConfigConstant.FULL_DATE_TIME_FORMAT);
    String endTime = DateUtil.format(new Date(depositchannel.getTEndtime() * 1000), ConfigConstant.FULL_DATE_TIME_FORMAT);
    Map<String , Object> map = new HashMap<>();

    map.put("depositchannel", depositchannel);
    map.put("cfg", cfg);
    map.put("startTime", startTime);

    map.put("endTime", endTime);
    return new R<>(map);
  }



  @PreAuthorize("@pms.hasPermission('cac_rebate_add')")
  @PostMapping("/doConfig")
  public R save(@RequestBody DepositDto depositDto){
    if (StrUtil.isEmpty(depositDto.getChannelId())) {
      return new R<>().setCode(1).setMsg("渠道编码不能为空!");
    }

    if (StrUtil.isEmpty(depositDto.getStartTime())) {
      return new R<>().setCode(1).setMsg("开始时间不能为空!");
    }

    if (StrUtil.isEmpty(depositDto.getEndTime())) {
      return new R<>().setCode(1).setMsg("结束时间不能为空!");
    }

    if (StrUtil.equals(depositDto.getStartTime(), depositDto.getEndTime())) {
      return new R<>().setCode(1).setMsg("活动开始时间与活动结束时间不能相同!");
    }

    int result = depositchannelService.saveRechargeRebateCfg(depositDto.getChannelId(), depositDto.getStartTime(), depositDto.getEndTime(), 1, depositDto.getRabateCfg());
    if (result == 0) {
      return new R<>();
    } else {
      return new R<>().setCode(1).setMsg("配置充值返利失败!");
    }
  }
}
