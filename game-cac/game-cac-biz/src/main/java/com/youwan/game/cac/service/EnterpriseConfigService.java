package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.EnterpriseConfig;


/**
 * 
 *
 * @author code generator
 * @date 2019-11-06 13:19:25
 */
public interface EnterpriseConfigService extends IService<EnterpriseConfig> {

}
