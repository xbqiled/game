package com.youwan.game.cac.service;

import cn.jpush.api.report.MessagesResult;
import cn.jpush.api.report.ReceivedsResult;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.PushFromDto;
import com.youwan.game.cac.api.entity.SysPushRecord;

import java.util.List;
import java.util.Map;



/**
 * 
 *
 * @author code generator
 * @date 2020-05-23 17:21:44
 */
public interface SysPushRecordService extends IService<SysPushRecord> {

    IPage<List<SysPushRecord>> queryPushRecordPage(Page page, String channelId, String startTime, String endTime);

    Boolean sendPush(PushFromDto pushFromDto);

    Map<String, Object> getObj(Integer id);

    ReceivedsResult getReport(Integer id);
}
