package com.youwan.game.cac.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.SourceUrl;
import com.youwan.game.cac.mapper.SourceUrlMapper;
import com.youwan.game.cac.service.SourceUrlService;
import com.youwan.game.common.core.constant.EnumConstant;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-04-19 13:18:35
 */
@Service("sourceUrlService")
public class SourceUrlServiceImpl extends ServiceImpl<SourceUrlMapper, SourceUrl> implements SourceUrlService {

    @Override
    public IPage<List<SourceUrl>> querySourceUrlPage(Page page, SourceUrl sourceUrl) {
        return baseMapper.querySourceUrlPage(page, sourceUrl.getShortUrl(), sourceUrl.getChannelId(), sourceUrl.getStatus());
    }

    @Override
    public Boolean saveSourceUrl(SourceUrl sourceUrl) {
        String uuidStr = IdUtil.fastSimpleUUID();
        sourceUrl.setSerialNum(uuidStr);
        sourceUrl.setCreateTime(LocalDateTime.now());
        this.save(sourceUrl);
        return Boolean.TRUE;
    }

    @Override
    @CacheEvict(value = "source_url:details", allEntries = true)
    public Boolean updateSourceById(SourceUrl sourceUrl) {
        return this.updateById(sourceUrl);
    }

    @Override
    @CacheEvict(value = "source_url:details", allEntries = true)
    public Boolean deleteSourceById(String id) {
        return this.removeById(id);
    }

    /**
     * <B>
     *   通过落地地址获取对应的跳转地址信息
     * </B>
     * @param sorceUrl
     * @return
     */
    @Override
    @CacheEvict(value = "source_url:details", key = "#sorceUrl")
    public SourceUrl getJumpUrl(String sorceUrl) {
        //首先判断是否存在对应的
        return baseMapper.getJumpUrl(sorceUrl);
    }

    @Override
    @CacheEvict(value = "source_url:details", key = "#serialNum + '_serialNum'")
    public SourceUrl getBySerial(String serialNum) {
        return baseMapper.getBySerial(serialNum);
    }

    @Override
    @CacheEvict(value = "source_url:details", key = "#id + '_id'")
    public SourceUrl findById(String id) {
        return this.getById(id);
    }

    @Override
    public Integer deleteBySourceUrl(String url) {
        return baseMapper.delBySourceUrl(url);
    }

    /**
     * <B>创建新的地址对应关系信息</B>
     * @param channelId
     * @param jumpUrl
     * @param soureUrl
     * @return
     */
    @Override
    public Boolean createSourceUrl(String id, String channelId, String shortUrl, String jumpUrl, String soureUrl) {
        SourceUrl sourceUrl = new SourceUrl();

        sourceUrl.setSerialNum(id);
        sourceUrl.setCreateTime(LocalDateTime.now());
        sourceUrl.setJumpUrl(jumpUrl);

        sourceUrl.setChannelId(channelId);
        sourceUrl.setSourceUrl(soureUrl);
        sourceUrl.setShortUrl(shortUrl);
        sourceUrl.setStatus(EnumConstant.URL_STATUS.URL_ZC.getValue());

        this.save(sourceUrl);
        return Boolean.TRUE;
    }
}
