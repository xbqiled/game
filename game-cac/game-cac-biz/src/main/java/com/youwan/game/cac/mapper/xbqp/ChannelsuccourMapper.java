package com.youwan.game.cac.mapper.xbqp;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.ChannelsuccourEntity;
import com.youwan.game.cac.api.vo.SuccourVo;
import com.youwan.game.cac.api.vo.TaskVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-31 12:23:45
 */
public interface ChannelsuccourMapper extends BaseMapper<ChannelsuccourEntity> {
    IPage<SuccourVo> queryPage(Page page, @Param("channelId") String channelId);

    ChannelsuccourEntity getSuccourByChannelId(@Param("channelId") String channelId);

}
