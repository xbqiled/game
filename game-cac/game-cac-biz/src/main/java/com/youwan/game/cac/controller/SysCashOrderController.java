package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.CashOrderDto;
import com.youwan.game.cac.api.entity.SysCashOrder;
import com.youwan.game.cac.service.SysCashOrderService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.common.security.annotation.Inner;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * 
 *
 * @author code generator
 * @date 2019-02-20 18:19:04
 */
@RestController
@AllArgsConstructor
@RequestMapping("/cashorder")
public class SysCashOrderController {

  private final SysCashOrderService sysCashOrderService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param cashNo
   * @return
   */
  @GetMapping("/page")
  public R getSysCashOrderPage(Page page, String cashNo, String userId, String status) {
    return  new R<>(sysCashOrderService.queryCashOrderPage(page, cashNo, userId, status));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(sysCashOrderService.getById(id));
  }

  /**
   * 新增
   * @param sysCashOrder 
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody SysCashOrder sysCashOrder){
    return new R<>(sysCashOrderService.save(sysCashOrder));
  }

  /**
   * 修改
   * @param sysCashOrder 
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody SysCashOrder sysCashOrder){
    return new R<>(sysCashOrderService.updateById(sysCashOrder));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(sysCashOrderService.removeById(id));
  }



  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("审批")
  @GetMapping("/approval")
  public R approval(String id, String status, String note){
    Boolean result = sysCashOrderService.approval(id, status, note);
    return  result ? new R<>() :  new R<>().setCode(1).setMsg("审核失败失败!");
  }


  @Inner
  @PostMapping("/createOrder")
  public R createOrder(@Valid @RequestBody CashOrderDto cashOrderDto){
    return new R<>(sysCashOrderService.cretaeOrder(cashOrderDto));
  }


  @Inner
  @PostMapping("/getOrderByUserId/{userId}")
  public R getOrderByUserId (@PathVariable String userId){
    return new R<>(sysCashOrderService.getOrderByUserId(userId));
  }

  @Inner
  @PostMapping("/getCashOrderInfo")
  public R getOrderInfo(@Valid @RequestBody CashOrderDto cashOrderDto){
    return new R<>(sysCashOrderService.getCashOrderInfo(cashOrderDto.getAccountId(), cashOrderDto.getType()));
  }

}
