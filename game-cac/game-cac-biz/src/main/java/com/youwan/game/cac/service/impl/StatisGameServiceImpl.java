package com.youwan.game.cac.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.ReportDto;
import com.youwan.game.cac.api.entity.StatisGame;
import com.youwan.game.cac.api.vo.StatisGameVo;
import com.youwan.game.cac.mapper.StatisGameMapper;
import com.youwan.game.cac.service.StatisGameService;
import io.swagger.models.auth.In;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author code generator
 * @date 2020-03-07 15:37:57
 */
@Service("statisGameService")
public class StatisGameServiceImpl extends ServiceImpl<StatisGameMapper, StatisGame> implements StatisGameService {


    private static String TABLE_NAME = "statis_game";

    @Override
    public IPage<List<StatisGameVo>> queryStatisGamePage(Page page, ReportDto reportDto) {
        Date data = DateUtil.parse(reportDto.getStartTime(), "yyyy-MM-dd");
        String monthTable = DateUtil.format(data, "yyyyMM");
        return baseMapper.queryStatisGamePage(page, TABLE_NAME + monthTable,
                reportDto.getChannelNo(), reportDto.getGameId(), reportDto.getStartTime(), reportDto.getEndTime());
    }

    @Override
    public IPage<List<StatisGameVo>> queryStatisUserGamePage(Page page, ReportDto reportDto) {
        Date data = DateUtil.parse(reportDto.getStartTime(), "yyyy-MM-dd");
        String monthTable = DateUtil.format(data, "yyyyMM");
        Integer userId = StringUtils.isNumeric(reportDto.getUserId()) ? Integer.parseInt(reportDto.getUserId()) : null;
        return baseMapper.queryStatisUserGamePage(page, TABLE_NAME + monthTable,
                reportDto.getChannelNo(), userId, reportDto.getGameId(), reportDto.getStartTime(), reportDto.getEndTime());
    }

    @Override
    public List<Map<String, String>> queryGameList() {
        return baseMapper.queryGameList();
    }
}
