package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.Ai;
import com.youwan.game.cac.mapper.xbqp.AiMapper;
import com.youwan.game.cac.service.AiService;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.HttpHandler;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-21 19:13:24
 */
@Service("tAiService")
public class AiServiceImpl extends ServiceImpl<AiMapper, Ai> implements AiService {

    @Override
    public IPage<Map<String, Object>> queryBotPage(Page page, String tNick, String tId) {
        return baseMapper.queryBotPage(page, tNick, tId);
    }

    @Override
    public Boolean modifyBot(Ai ai) {
        GsonResult result = HttpHandler.modifyBotInfo(ai.getTId(), ai.getTNick());
        if (result != null && result.equals(0)) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }
}
