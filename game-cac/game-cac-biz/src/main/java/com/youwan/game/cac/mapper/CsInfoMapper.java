package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.CsInfo;
import com.youwan.game.cac.api.entity.SourceUrl;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-03-29 15:44:57
 */
public interface CsInfoMapper extends BaseMapper<CsInfo> {

    List<CsInfo> getCsInfoByChannelId(@Param("channelId") String channelId);


    List<CsInfo> getCsInfo(@Param("channelId") String channelId, @Param("type") String type);


    IPage<List<CsInfo>> queryCsInfoPage(Page page, @Param("channelId") String channelId, @Param("type") Integer type);

}
