package com.youwan.game.cac.mapper.recordlog;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-07-08.
 */
public interface RecorddamachangeMapper {

    List<Map<String, Object>> queryDamaAgg(@Param("start") Integer start, @Param("end") Integer end,
                                           @Param("channelId") String channelId, @Param("list") List<Integer> uids);
}
