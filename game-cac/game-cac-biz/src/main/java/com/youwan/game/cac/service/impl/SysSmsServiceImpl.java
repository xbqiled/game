package com.youwan.game.cac.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.SmsDto;
import com.youwan.game.cac.api.entity.SysSms;
import com.youwan.game.cac.api.entity.SysSmsConfig;
import com.youwan.game.cac.api.entity.SysSmsTemplate;
import com.youwan.game.cac.mapper.SysSmsConfigMapper;
import com.youwan.game.cac.mapper.SysSmsMapper;
import com.youwan.game.cac.mapper.SysSmsTemplateMapper;
import com.youwan.game.cac.service.SysSmsService;
import com.youwan.game.common.core.constant.ConfigConstant;
import com.youwan.game.common.core.constant.enums.SmsStatusEnum;
import com.youwan.game.common.core.sms.*;
import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-22 14:18:24
 */
@AllArgsConstructor
@Service("sysSmsService")
public class SysSmsServiceImpl extends ServiceImpl<SysSmsMapper, SysSms> implements SysSmsService {

    private final SysSmsConfigMapper sysSmsConfigMapper;

    private final SysSmsTemplateMapper sysSmsTemplateMapper;

    private final RedisTemplate redisTemplate;


    @Override
    public IPage<List<SysSms>> querySmsPage(Page page, SysSms sysSms) {
        return baseMapper.querySmsPage(page, sysSms.getPhoneNumber(), sysSms.getChannelId());
    }

    @Override
    public Boolean saveAndSendSms(SysSms sysSms) {
        //获取config信息
        SmsConfig smsConfig  = getActivatConfig();
        //组装内容信息
        MobileMsgTemplate mobileMsgTemplate = null;
        SysSmsTemplate sysSmsTemplate = sysSmsTemplateMapper.findByTemplateId(sysSms.getTemplateCode(), smsConfig.getType(), sysSms.getChannelId());
        if (sysSmsTemplate == null || StrUtil.isBlank(sysSmsTemplate.getTemplateName())) {
            return Boolean.FALSE;
        }

        //获取短信发送服务
        SmsMessageService smsMessageService = SMSFactory.build(smsConfig);
        Boolean result = false;
        String[] parameter = {};

        if (StrUtil.isNotEmpty(sysSms.getParameter()) && sysSms.getParameter().contains(",")) {
            String[] array = sysSms.getParameter().split(",");
            parameter = array;
        } else {
            parameter = new String [1];
            parameter[0] = sysSms.getParameter();
        }

        //发内容
        String content = StrUtil.format(sysSmsTemplate.getTemplateName(), parameter);
        if (smsConfig.getType() == 1) {
            AliDayuConfig aliDayuConfig = (AliDayuConfig) smsConfig;
            mobileMsgTemplate = new MobileMsgTemplate(smsConfig.getMode(), sysSms.getPhoneNumber(), content, sysSms.getChannelId(), aliDayuConfig.getAliSignName(), sysSms.getTemplateCode(), parameter);
            result = ((SmsAliyunMessageService) smsMessageService).process(mobileMsgTemplate, smsConfig);

        } else if (smsConfig.getType() == 2) {
            CloudLetterConfig cloudLetterConfig = (CloudLetterConfig) smsConfig;
            mobileMsgTemplate = new MobileMsgTemplate(smsConfig.getMode(), sysSms.getPhoneNumber(), content, sysSms.getChannelId(), cloudLetterConfig.getServerUrl(), sysSms.getTemplateCode(), parameter);
            result = ((SmsCloudLetterMessageService) smsMessageService).process(mobileMsgTemplate, smsConfig);

        } else if (smsConfig.getType() == 3) {
            RongLianConfig rongLianConfig = (RongLianConfig) smsConfig;
            mobileMsgTemplate = new MobileMsgTemplate(smsConfig.getMode(), sysSms.getPhoneNumber(), content, sysSms.getChannelId(), rongLianConfig.getRongLianServerIp(), sysSms.getTemplateCode(), parameter);
            result = ((SmsRongLianMessageService) smsMessageService).process(mobileMsgTemplate, smsConfig);

        } else {


        }
        //保存信息
        sysSms.setContent(content);
        sysSms.setCreateTime(LocalDateTime.now());
        sysSms.setCreateUser(getUsername());

        if (parameter != null && parameter.length > 0) {
            String str = new Gson().toJson(parameter, String[].class);
            sysSms.setParameter(str);
        }
        sysSms.setState(result ? SmsStatusEnum.SUCCESS.getStatus() : SmsStatusEnum.FAIL.getStatus());
        if (StrUtil.isBlank(sysSms.getTemplateCode())) {
            sysSms.setTemplateCode("SMS");
        }

        this.save(sysSms);
        return result;
    }

    private  SmsConfig  getActivatConfig() {
        SmsConfig smsConfig  = (SmsConfig)redisTemplate.opsForValue().get(ConfigConstant.ACTIVAT_SMS_CONFIG_KEY);
        if (smsConfig == null) {
            SysSmsConfig sysSmsConfig = sysSmsConfigMapper.queryActivateSmsConfig();
            if (sysSmsConfig != null || StrUtil.isNotEmpty(sysSmsConfig.getConfig())) {
                if (sysSmsConfig.getType() == 1) {
                    redisTemplate.opsForValue().set(ConfigConstant.ACTIVAT_SMS_CONFIG_KEY, new Gson().fromJson(sysSmsConfig.getConfig(), AliDayuConfig.class));
                    smsConfig = new Gson().fromJson(sysSmsConfig.getConfig(), AliDayuConfig.class);
                } else if (sysSmsConfig.getType() == 2) {
                    redisTemplate.opsForValue().set(ConfigConstant.ACTIVAT_SMS_CONFIG_KEY, new Gson().fromJson(sysSmsConfig.getConfig(), CloudLetterConfig.class));
                    smsConfig = new Gson().fromJson(sysSmsConfig.getConfig(), CloudLetterConfig.class);
                } else if (sysSmsConfig.getType() == 3) {
                    redisTemplate.opsForValue().set(ConfigConstant.ACTIVAT_SMS_CONFIG_KEY, new Gson().fromJson(sysSmsConfig.getConfig(), RongLianConfig.class));
                    smsConfig = new Gson().fromJson(sysSmsConfig.getConfig(), RongLianConfig.class);
                } else {

                }
            }
        }
        return smsConfig;
    }


    @Override
    public Boolean sendSms(SmsDto sms, SysSmsTemplate template) {
        MobileMsgTemplate mobileMsgTemplate = null;
        //获取config信息
        SmsConfig smsConfig = getActivatConfig();
        String context = StrUtil.format(template.getTemplateName(), sms.getParameter());

        if (smsConfig.getType() == 1) {
            mobileMsgTemplate = new MobileMsgTemplate(((AliDayuConfig) smsConfig).getMode(), sms.getPhone(), context, sms.getChannelId(), ((AliDayuConfig) smsConfig).getAliSignName(), template.getTemplateCode(), sms.getParameter());
        } else if (smsConfig.getType() == 2) {
            mobileMsgTemplate = new MobileMsgTemplate(((CloudLetterConfig) smsConfig).getMode(), sms.getPhone(), context, sms.getChannelId(), ((CloudLetterConfig) smsConfig).getServerUrl(), template.getTemplateCode(), sms.getParameter());
        } else if (smsConfig.getType() == 3) {
            mobileMsgTemplate = new MobileMsgTemplate(((RongLianConfig) smsConfig).getMode(), sms.getPhone(), context, sms.getChannelId(), ((RongLianConfig) smsConfig).getRongLianServerIp(), template.getTemplateCode(), sms.getParameter());
        } else {

        }

        //获取短信发送服务
        SmsMessageService smsMessageService = SMSFactory.build(smsConfig);
        boolean reuslt = smsMessageService.process(mobileMsgTemplate, smsConfig);

        //保存信息
        SysSms sysSms = new SysSms();
        sysSms.setCreateTime(LocalDateTime.now());
        sysSms.setPhoneNumber(sms.getPhone());
        sysSms.setTemplateCode(template.getTemplateCode());
        if (sms != null && sms.getParameter() != null && sms.getParameter().length > 0) {
            String str = new Gson().toJson(sms.getParameter(), String[].class);
            sysSms.setParameter(str);
        }

        sysSms.setContent(context);
        sysSms.setTemplateCode("SMS");
        sysSms.setCreateUser("API");
        sysSms.setChannelId(sms.getChannelId());
        sysSms.setState(reuslt ? SmsStatusEnum.SUCCESS.getStatus() : SmsStatusEnum.FAIL.getStatus());

        this.save(sysSms);
        return reuslt;
    }


    /**
     * 获取用户名称
     * @return username
     */
    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }
}
