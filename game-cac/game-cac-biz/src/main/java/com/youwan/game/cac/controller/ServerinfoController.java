package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.Serverinfo;
import com.youwan.game.cac.service.ServerinfoService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2018-12-29 12:32:01
 */
@RestController
@AllArgsConstructor
@RequestMapping("/tserverinfo")
public class ServerinfoController {

  private final ServerinfoService serverinfoService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param serverinfo
   * @return
   */
  @GetMapping("/page")
  public R getTServerinfoPage(Page page, Serverinfo serverinfo) {
    return new R<>(serverinfoService.page(page, Wrappers.query(serverinfo)));
  }


  /**
   * 通过id查询
   * @param tId id
   * @return R
   */
  @GetMapping("/{tId}")
  public R getById(@PathVariable("tId") Integer tId){
    return new R<>(serverinfoService.getById(tId));
  }

  /**
   * 新增
   * @param serverinfo
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody Serverinfo serverinfo){
    return new R<>(serverinfoService.save(serverinfo));
  }

  /**
   * 修改
   * @param serverinfo
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody Serverinfo serverinfo){
    return new R<>(serverinfoService.updateById(serverinfo));
  }

  /**
   * 通过id删除
   * @param tId id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{tId}")
  public R removeById(@PathVariable Integer tId){
    return new R<>(serverinfoService.removeById(tId));
  }

}
