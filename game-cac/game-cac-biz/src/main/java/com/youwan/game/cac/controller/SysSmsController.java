package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.SmsDto;
import com.youwan.game.cac.api.entity.SysSms;
import com.youwan.game.cac.api.entity.SysSmsTemplate;
import com.youwan.game.cac.service.SysSmsConfigService;
import com.youwan.game.cac.service.SysSmsService;
import com.youwan.game.cac.service.SysSmsTemplateService;
import com.youwan.game.common.core.sms.SmsConfig;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.common.security.annotation.Inner;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * 
 *
 * @author code lion
 * @date 2019-01-22 14:18:24
 */
@RestController
@AllArgsConstructor
@RequestMapping("/syssms")
public class SysSmsController {

  private final SysSmsService sysSmsService;

  private final SysSmsTemplateService sysSmsTemplateService;

  private final SysSmsConfigService sysSmsConfigService;

  /**
   * 分页查询
   *
   * @param page   分页对象
   * @param sysSms
   * @return
   */
  @GetMapping("/page")
  public R getSysSmsPage(Page page, SysSms sysSms) {
    return new R<>(sysSmsService.querySmsPage(page, sysSms));
  }


  /**
   * 通过id查询
   *
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id) {
    return new R<>(sysSmsService.getById(id));
  }

  /**
   * 新增
   *
   * @param sysSms
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody SysSms sysSms) {
    Boolean result = sysSmsService.saveAndSendSms(sysSms);
    if (result) {
      return new R<>();
    } else {
      return new R<>().setCode(1).setMsg("发送短信失败!");
    }
  }

  /**
   * 修改
   *
   * @param sysSms
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody SysSms sysSms) {
    return new R<>(sysSmsService.updateById(sysSms));
  }

  /**
   * 通过id删除
   *
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  @PreAuthorize("@pms.hasPermission('cac_syssms_del')")
  public R removeById(@PathVariable Integer id) {
    return new R<>(sysSmsService.removeById(id));
  }

  /**
   * 发送短信
   *
   * @param
   * @return
   */
  @Inner
  @PostMapping("/sendSms")
  public R sendSms(@Valid @RequestBody SmsDto sms) {
    SmsConfig config = sysSmsConfigService.getActivateValue();
    SysSmsTemplate template = sysSmsTemplateService.findByServerCode(sms.getTemplateType(), config.getType(), sms.getChannelId());
    return new R<>(sysSmsService.sendSms(sms, template));
  }


  @Inner
  @PostMapping("/getSmsTemplate")
  public R getSmsTemplate(@Valid @RequestBody SmsDto sms) {
    SmsConfig config = sysSmsConfigService.getActivateValue();
    List<SysSmsTemplate> list = sysSmsTemplateService.queryTemplateByChannel(config.getType(), sms.getChannelId());
    return new R<>(list);
  }


}
