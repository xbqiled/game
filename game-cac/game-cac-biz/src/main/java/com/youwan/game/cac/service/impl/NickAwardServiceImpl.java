package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.NickAwardEntity;
import com.youwan.game.cac.api.vo.NickAwardVo;
import com.youwan.game.cac.mapper.xbqp.NickAwardMapper;
import com.youwan.game.cac.service.NickAwardService;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.HttpHandler;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-08-03 15:31:04
 */
@Service("nickAwardService")
public class NickAwardServiceImpl extends ServiceImpl<NickAwardMapper, NickAwardEntity> implements NickAwardService {

    @Override
    public IPage<List<NickAwardVo>> queryPage(Page page, QueryDto queryDto) {
        return this.baseMapper.queryPage(page, queryDto.getChannelId());
    }

    @Override
    public NickAwardVo getByChannelId(String channelId) {
        return baseMapper.getByChannelId(channelId);
    }

    @Override
    public int saveNcikAwardCfg(String channelId, Integer openType, Integer award) {
        GsonResult result = HttpHandler.configNickAward(openType, channelId, award);
        if (result != null) {
            return result.getRet() ;
        }
        return 1;
    }
}
