package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.DownloadConfigDto;
import com.youwan.game.cac.api.entity.BgenterConfig;
import com.youwan.game.cac.api.entity.CfgChlDownload;
import com.youwan.game.cac.api.vo.DownloadPanelConfigVo;
import com.youwan.game.cac.mapper.CfgChlDownloadMapper;
import com.youwan.game.cac.service.CfgChlDownloadService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-18 16:08:14
 */
@Service("cfgChlDownloadService")
public class CfgChlDownloadServiceImpl extends ServiceImpl<CfgChlDownloadMapper, CfgChlDownload> implements CfgChlDownloadService {


    @Override
    public IPage<List<CfgChlDownload>> queryDownloadCfgPage(Page page, DownloadConfigDto downloadConfigDto) {
        return baseMapper.queryDownloadCfgPage(page, downloadConfigDto.getChannelId());
    }

    @Override
    @CacheEvict(value = "downloadPanelCfg:details", key = "#channelId")
    public DownloadPanelConfigVo getConfigByChannelId(String channelId) {
        return baseMapper.getConfigVoByChannelId(channelId);
    }

    @Override
    @CacheEvict(cacheNames = "downloadconfig:details",allEntries = true)
    public Boolean saveDownloadPanelCfg(CfgChlDownload cfgChlDownload) {
        CfgChlDownload config = baseMapper.getConfigByChannelId(cfgChlDownload.getChannelId());
        if (config != null) {
            return  Boolean.FALSE;
        }

        cfgChlDownload.setCreateBy(getUsername());
        cfgChlDownload.setCreateTime(LocalDateTime.now());
        this.save(cfgChlDownload);
        return Boolean.TRUE;
    }

    @Override
    @CacheEvict(cacheNames = "downloadconfig:details",allEntries = true)
    public Boolean modifyDownloadPanelCfg(CfgChlDownload cfgChlDownload) {
        CfgChlDownload config = baseMapper.getConfigByChannelId(cfgChlDownload.getChannelId());
        if (config == null) {
            return  Boolean.FALSE;
        }

        config.setModifyBy(getUsername());
        config.setModifyTime(LocalDateTime.now());
        config.setHeadUrl(cfgChlDownload.getHeadUrl());

        config.setBodyUrl(cfgChlDownload.getBodyUrl());
        config.setFootUrl(cfgChlDownload.getFootUrl());
        config.setBackgroundUrl(cfgChlDownload.getBackgroundUrl());
        this.updateById(config);
        return Boolean.TRUE;
    }

    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }

}
