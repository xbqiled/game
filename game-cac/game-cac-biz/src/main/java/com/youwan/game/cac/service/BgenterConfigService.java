package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.BgEnterGameDto;
import com.youwan.game.cac.api.entity.BgenterConfig;

import java.util.List;


/**
 * 
 *
 * @author code generator
 * @date 2019-11-12 14:04:32
 */
public interface BgenterConfigService extends IService<BgenterConfig> {


    IPage<List<BgenterConfig>> queryEnterGameConfigPage(Page page, BgEnterGameDto bgEnterGameDto);

    Boolean saveEnterConfig(BgenterConfig bgenterConfig);

    Boolean modifyEnterConfig(BgenterConfig bgenterConfig);

    BgenterConfig getConfigByChannelId(String channleId);
}
