package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.DayShareEntity;
import com.youwan.game.cac.api.entity.DayShareForm;
import com.youwan.game.cac.api.vo.BindUrlVo;
import com.youwan.game.cac.api.vo.DayShareVo;
import com.youwan.game.common.core.jsonmodel.GsonDayShare;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-15 16:02:59
 */
public interface DayShareService extends IService<DayShareEntity> {

    IPage<DayShareVo> queryPage(Page page, QueryDto queryDto);

    DayShareEntity findByChannelId(String channelId);

    int saveOrUpdateConfig(DayShareForm dayShareForm);

}

