package com.youwan.game.cac.mapper.xbqp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.Usergamedata;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-10 21:03:48
 */
public interface UsergamedataMapper extends BaseMapper<Usergamedata> {

    IPage<List<Map<String, Object>>> queryUserCtrlPage(Page page,
                                                       @Param("channelId") String channelId,
                                                       @Param("userId") Integer userId,
                                                       @Param("gameRoomId") String gameRoomId);

    List<Map<String, Object>> queryGameRoom();

}
