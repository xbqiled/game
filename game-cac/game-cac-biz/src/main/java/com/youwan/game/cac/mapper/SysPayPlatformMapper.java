package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.SysPayPlatform;
import com.youwan.game.cac.api.entity.SysSmsTemplate;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 
 *
 * @author code generator
 * @date 2018-12-27 16:32:02
 */
public interface SysPayPlatformMapper extends BaseMapper<SysPayPlatform> {

    IPage<List<SysPayPlatform>> queryPayPlatformPage(Page page, @Param("company") String company);
}
