package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.DownloadConfigDto;
import com.youwan.game.cac.api.entity.CfgChlDownload1;
import com.youwan.game.cac.api.vo.DownloadPanelConfigVo1;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-12-30 15:25:40
 */
public interface CfgChlDownload1Service extends IService<CfgChlDownload1> {

    IPage<List<CfgChlDownload1>> queryDownloadCfgPage(Page page, DownloadConfigDto downloadConfigDto);

    DownloadPanelConfigVo1 getConfigByChannelId(String channelId);

    Boolean saveDownloadPanelCfg(CfgChlDownload1 cfgChlDownload);

    Boolean modifyDownloadPanelCfg(CfgChlDownload1 cfgChlDownload);
}
