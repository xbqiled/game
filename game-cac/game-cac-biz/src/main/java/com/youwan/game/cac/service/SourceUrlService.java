package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.Domain;
import com.youwan.game.cac.api.entity.SourceUrl;

import java.time.LocalDateTime;
import java.util.List;


/**
 * 
 *
 * @author code generator
 * @date 2019-04-19 13:18:35
 */
public interface SourceUrlService extends IService<SourceUrl> {

    IPage<List<SourceUrl>> querySourceUrlPage(Page page, SourceUrl sourceUrl);

    Boolean saveSourceUrl(SourceUrl sourceUrl);

    Boolean createSourceUrl(String id, String channelId, String shortUrl, String jumpUrl, String soureUrl);

    Boolean updateSourceById(SourceUrl sourceUrl);

    SourceUrl findById(String id);

    Boolean deleteSourceById(String id);

    Integer deleteBySourceUrl(String url);

    SourceUrl getJumpUrl(String sorceUrl);

    SourceUrl getBySerial(String serialNum);
}
