package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.VersionConfigDto;
import com.youwan.game.cac.api.entity.SysVersionConfig;

import java.util.List;


/**
 * 
 *
 * @author code generator
 * @date 2019-02-08 16:36:30
 */
public interface SysVersionConfigService extends IService<SysVersionConfig> {

    /**
     * <B>添加配置信息</B>
     * @param sysVersionConfig
     * @return
     */
    Boolean saveVersionConfig(SysVersionConfig sysVersionConfig);


    /**
     * <B>修改配置信息</B>
     * @param sysVersionConfig
     * @return
     */
    Boolean updateVersionConfigById(SysVersionConfig sysVersionConfig);


    /**
     * <B>根据渠道和版本查询信息</B>
     * @param config
     * @return
     */
    List<SysVersionConfig> queryModelByChannelId(VersionConfigDto config);
}
