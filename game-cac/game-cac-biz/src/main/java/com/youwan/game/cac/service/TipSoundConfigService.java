package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.TipSoundConfig;

import java.util.List;


/**
 * 
 *
 * @author code generator
 * @date 2019-05-21 11:11:20
 */
public interface TipSoundConfigService extends IService<TipSoundConfig> {

    IPage<List<TipSoundConfig>> querySoundConfigPage(Page page, String channelId);

    Boolean addSoundConfig(TipSoundConfig tipSoundConfig);

    TipSoundConfig findByChannelId(String channelId);

}
