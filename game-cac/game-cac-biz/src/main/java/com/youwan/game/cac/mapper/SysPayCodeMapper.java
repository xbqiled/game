package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.SysPayCode;
import com.youwan.game.cac.api.vo.PayCodeVo;
import org.apache.ibatis.annotations.Param;

/**
 * 
 *
 * @author code generator
 * @date 2019-05-10 11:23:04
 */
public interface SysPayCodeMapper extends BaseMapper<SysPayCode> {

    PayCodeVo getPayCodeByOrderNo(@Param("orderNo") String orderNo);

}
