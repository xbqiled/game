package com.youwan.game.cac.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.Signinchannel;
import com.youwan.game.cac.api.vo.SigninchannelVo;
import com.youwan.game.cac.mapper.xbqp.SigninchannelMapper;
import com.youwan.game.cac.service.SigninchannelService;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.jsonmodel.GsonSignAwardCfg;
import com.youwan.game.common.core.util.HttpHandler;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-05 13:49:00
 */
@Service("signinService")
public class SigninchannelServiceImpl extends ServiceImpl<SigninchannelMapper, Signinchannel> implements SigninchannelService {


    @Override
    public IPage<List<SigninchannelVo>> querySignCfgPage(Page page, QueryDto queryDto) {
        return baseMapper.querySignCfgPage(page, queryDto.getChannelNo(), queryDto.getStartTime(), queryDto.getEndTime());
    }

    @Override
    public Signinchannel findByChannelId(String channelId) {
        return baseMapper.getSignByChannelId(channelId);
    }

    @Override
    public int channelOpenType(String channelId, String openType) {
        Signinchannel model  = baseMapper.getSignByChannelId(channelId);
        if (model != null && StrUtil.isNotEmpty(model.getTChannel())) {

            Integer type = model.getTOpenType() == 1 ? 0 : 1;
            String str = new String(model.getTCfginfo());
            List<GsonSignAwardCfg> rabateCfg = new ArrayList<>();

            if (StrUtil.isNotBlank(str)) {
                GsonSignAwardCfg[] array = new Gson().fromJson(str, GsonSignAwardCfg[].class);
                rabateCfg = Arrays.asList(array);
            }

            GsonResult result = HttpHandler.signConfig(model.getTChannel(), model.getTStarttime(), model.getTEndtime(), type, model.getTIsCirculate(), rabateCfg);
            if (result != null) {
                return result.getRet();
            }
        }
        return 1;
    }


    @Override
    public int channelLoop(String channelId, String openType) {
        Signinchannel model  = baseMapper.getSignByChannelId(channelId);
        if (model != null && StrUtil.isNotEmpty(model.getTChannel())) {

            Integer type = model.getTIsCirculate() == 1 ? 0 : 1;
            String str = new String(model.getTCfginfo());
            List<GsonSignAwardCfg> rabateCfg = new ArrayList<>();

            if (StrUtil.isNotBlank(str)) {
                GsonSignAwardCfg[] array = new Gson().fromJson(str, GsonSignAwardCfg[].class);
                rabateCfg = Arrays.asList(array);
            }

            GsonResult result = HttpHandler.signConfig(model.getTChannel(), model.getTStarttime(), model.getTEndtime(), model.getTOpenType(), type, rabateCfg);
            if (result != null) {
                return result.getRet();
            }
        }
        return 1;
    }


    @Override
    public int saveSignCfg(String channel, String beginTime, String endTime, Integer openType, Integer isCirculate, Integer num, List<GsonSignAwardCfg> rabateCfg) {
        Long start = new Long(beginTime) / 1000;
        Long finish = new Long(endTime) / 1000;

        GsonResult result = HttpHandler.signConfig(channel, start, finish, openType, isCirculate, rabateCfg);
        if (result != null) {
            return result.getRet();
        }
        return 1;
    }
}
