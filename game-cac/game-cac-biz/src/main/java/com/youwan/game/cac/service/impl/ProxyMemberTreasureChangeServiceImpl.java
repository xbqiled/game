package com.youwan.game.cac.service.impl;

import com.youwan.game.cac.api.dto.ProxyMemberQueryDto;
import com.youwan.game.cac.api.entity.ProxyMember;
import com.youwan.game.cac.api.entity.ProxyMemberTreasureChange;
import com.youwan.game.cac.service.ProxyMemberTreasureChangeService;
import com.youwan.game.common.data.core.Page;
import com.youwan.game.common.data.elasticsearch.ElasticsearchTemplate;
import com.youwan.game.common.data.elasticsearch.operations.SearchOperation;
import org.elasticsearch.common.collect.MapBuilder;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.script.Script;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import static org.elasticsearch.index.query.QueryBuilders.*;
import static org.elasticsearch.script.Script.DEFAULT_SCRIPT_LANG;
import static org.elasticsearch.script.Script.DEFAULT_SCRIPT_TYPE;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-04-25.
 */
@Service
public class ProxyMemberTreasureChangeServiceImpl implements ProxyMemberTreasureChangeService {

    private ElasticsearchTemplate elasticsearchTemplate;

    public ProxyMemberTreasureChangeServiceImpl(ElasticsearchTemplate elasticsearchTemplate) {
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    @Override
    public Page<ProxyMemberTreasureChange> queryProxyMemberTreasureChange(ProxyMemberQueryDto queryDto) {
        ProxyMember self = elasticsearchTemplate.get("account_info", "_doc", queryDto.getUserId() + "",
                ProxyMember.class);
        if (Objects.isNull(self))
            return Page.empty();
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery().must(termQuery("m_path", queryDto.getUserId()));
        if (queryDto.getDeep() > 0) {
            boolQueryBuilder.must(scriptQuery(new Script(DEFAULT_SCRIPT_TYPE, DEFAULT_SCRIPT_LANG,
                    "doc['m_path'].length == params.length",
                    MapBuilder.newMapBuilder(new HashMap<String, Object>()).put("length",
                            self.getM_path().size() + queryDto.getDeep()).immutableMap())));
        }
        if (queryDto.getStart() != null && queryDto.getStart() != null) {
            boolQueryBuilder.must(rangeQuery("recordTime").gte(queryDto.getStart()).lte(queryDto.getEnd()));
        }
        Page<ProxyMemberTreasureChange> _page = elasticsearchTemplate.searchForPage(new SearchOperation<>(
                "user_treasure_change", "_doc",
                boolQueryBuilder, ProxyMemberTreasureChange.class).setFrom(queryDto.getOffset()).setSize(queryDto.getLimit()));
        _page.getRecords().forEach(treasureChange -> treasureChange.setDeep(calculateDeep(queryDto.getUserId(),
                treasureChange.getM_path())));
        return _page;
    }

    private int calculateDeep(Long accountId, List<String> m_path) {
        int index;
        for (index = 0; index < m_path.size(); index++) {
            if (m_path.get(index).equals(accountId.toString())) {
                break;
            }
        }
        return m_path.size() - index - 1;
    }
}
