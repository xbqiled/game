package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.ChannelsuccourEntity;
import com.youwan.game.cac.api.entity.SuccourForm;
import com.youwan.game.cac.api.vo.SuccourVo;
import com.youwan.game.common.core.jsonmodel.GsonSuccour;

import java.util.List;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-31 12:23:45
 */
public interface ChannelsuccourService extends IService<ChannelsuccourEntity> {
    IPage<SuccourVo> queryPage(Page page, QueryDto queryDto);

    int channelOpenType(String channelId, String openType);

    GsonSuccour findByChannelId(String channelId);

    int saveOrUpdateConfig(SuccourForm succourForm);
}

