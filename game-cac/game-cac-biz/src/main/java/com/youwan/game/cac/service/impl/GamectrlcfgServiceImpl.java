package com.youwan.game.cac.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.GameFqzsCtrlCfg;
import com.youwan.game.cac.api.entity.Gamectrlcfg;
import com.youwan.game.cac.mapper.xbqp.GamectrlcfgMapper;
import com.youwan.game.cac.service.GamectrlcfgService;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.HttpHandler;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2020-03-27 20:25:55
 */
@Service("gamectrlcfgService")
public class GamectrlcfgServiceImpl extends ServiceImpl<GamectrlcfgMapper, Gamectrlcfg> implements GamectrlcfgService {

    @Override
    public IPage<Map<String, Object>> queryGameCtrlCfgPage(Page page, String startTime, String endTime) {
        IPage<Map<String, Object>> result = baseMapper.queryGameCtrlCfgPage(page, startTime, endTime);
        return result;
    }

    @Override
    public Map getGameCtrlCfg(String gameId) {
        Gamectrlcfg gamectrlcfg = baseMapper.getByGameId(gameId);
        if (gamectrlcfg != null && gamectrlcfg.getTCfginfo() != null) {
            String info = new String(gamectrlcfg.getTCfginfo());
            if (StrUtil.isNotEmpty(info)) {
                return new Gson().fromJson(info, Map.class);
            }
        }
        return null;
    }

    @Override
    public int doFqzsCfg(GameFqzsCtrlCfg gameFqzsCtrlCfg) {
        GsonResult result = HttpHandler.gameCtrlCfg(gameFqzsCtrlCfg.getSixInterval(), gameFqzsCtrlCfg.getEightInterval(),
                gameFqzsCtrlCfg.getTwelveInterval(), gameFqzsCtrlCfg.getGlobalInterval(), gameFqzsCtrlCfg.getKillAllInterval());

        if (result != null) {
            return result.getRet();
        }
        return 1;
    }
}
