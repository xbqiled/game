package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.Whitelist;
import com.youwan.game.cac.service.WhitelistService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


/**
 * @author code generator
 * @date 2018-12-29 13:01:34
 */
@RestController
@AllArgsConstructor
@RequestMapping("/whitelist")
public class WhitelistController {

    private final WhitelistService whitelistService;

    /**
     * 分页查询
     *
     * @param page      分页对象
     * @param whitelist
     * @return
     */
    @GetMapping("/page")
    public R getWhitelistPage(Page page, Whitelist whitelist) {
        return new R<>(whitelistService.queryPageList(page, whitelist));
    }

    /**
     * 新增
     *
     * @param whitelist
     * @return R
     */
    @SysLog("新增")
    @PostMapping
    @PreAuthorize("@pms.hasPermission('cac_white_add')")
    public R save(Whitelist whitelist) {
        Boolean result = whitelistService.addWhiteList(whitelist);
        return result ? new R<>() : new R<>().setCode(1).setMsg("添加白名单失败!");
    }

    /**
     * 通过id删除
     *
     * @param tAccountname id
     * @return R
     */
    @SysLog("删除")
    @DeleteMapping("/{tAccountname}")
    @PreAuthorize("@pms.hasPermission('cac_white_del')")
    public R removeById(@PathVariable String tAccountname) {
        Boolean result = whitelistService.delWhiteList(tAccountname);
        return result ? new R<>() : new R<>().setCode(1).setMsg("删除白名单失败!");
    }

}
