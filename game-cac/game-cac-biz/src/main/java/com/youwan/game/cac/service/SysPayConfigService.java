package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.SysPayConfig;
import com.youwan.game.cac.api.entity.SysPayPlatform;
import com.youwan.game.cac.api.vo.PayChannelVo;
import com.youwan.game.cac.api.vo.PayConfigVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-27 16:31:52
 */
public interface SysPayConfigService extends IService<SysPayConfig> {

    /**
     * <B>添加信息</B>
     * @param sysPayConfig
     * @return
     */
    Boolean saveConfig(SysPayConfig sysPayConfig);


    /**
     * <B>根据配置查询支付信息</B>
     * @param channelId
     * @return
     */
    List<PayChannelVo> queryPayChannel(String channelId);


    /**
     * <B>获取支付配置信息</B>
     * @param configId
     * @return
     */
    PayChannelVo queryPayConfig(String configId);



    PayConfigVo getPayConfig(String merchantCode);


    /**
     * <B>查询支付配置信息</B>
     * @param channelId
     * @param configId
     * @return
     */
    PayConfigVo queryPayConfigVo(String channelId, String configId);


    /**
     * <B>查询分页信息</B>
     * @param page
     * @param channelId
     * @param payName
     * @return
     */
    IPage<List<PayChannelVo>> queryPayConfigPage(Page page, String payName, String channelId);
}
