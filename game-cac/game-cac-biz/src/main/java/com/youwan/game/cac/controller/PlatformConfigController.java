package com.youwan.game.cac.controller;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.OtherPlatformDto;
import com.youwan.game.cac.api.dto.PlatformConfigDto;
import com.youwan.game.cac.api.entity.OtherPlatformConfig;
import com.youwan.game.cac.service.OtherPlatformConfigService;
import com.youwan.game.common.core.otherplatform.PlatformConfig;
import com.youwan.game.common.core.otherplatform.PlatformFactory;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author Lion
 * @date 2019-11-01 13:17:55
 */
@RestController
@AllArgsConstructor
@RequestMapping("/platformconfig")
public class PlatformConfigController {

  private final OtherPlatformConfigService otherPlatformConfigService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param otherPlatformDto
   * @return
   */
  @GetMapping("/page")
  public R getOtherPlatformConfigPage(Page page, OtherPlatformDto otherPlatformDto) {
    return  new R<>(otherPlatformConfigService.queryPlatformConfigPage(page, otherPlatformDto));
  }

  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(otherPlatformConfigService.getById(id));
  }

  @GetMapping("getPlatformConfig/{id}")
  public R getPlatformConfig(@PathVariable("id") Integer id){
    return new R<>(otherPlatformConfigService.getPlatformConfig(id));
  }

  /**
   * 新增
   * @param otherPlatformConfig 
   * @return R
   */
  @SysLog("新增第三方平台信息")
  @PostMapping
  public R save(@RequestBody OtherPlatformConfig otherPlatformConfig){
    return new R<>(otherPlatformConfigService.saveOtherPlatformConfig(otherPlatformConfig));
  }

  /**
   * 修改
   * @param otherPlatformConfig 
   * @return R
   */
  @SysLog("修改第三方平台信息")
  @PutMapping
  public R updateById(@RequestBody OtherPlatformConfig otherPlatformConfig){
    return new R<>(otherPlatformConfigService.updateOtherPlatformConfig(otherPlatformConfig));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除第三方平台信息")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(otherPlatformConfigService.removeById(id));
  }


  @SysLog("配置第三方平台信息")
  @PostMapping("/doConfig")
  public R doConfig(@RequestBody PlatformConfigDto platformConfigDto){
    PlatformConfig platformConfig = PlatformFactory.build(platformConfigDto.getType(), platformConfigDto.getPlatformCode());
    BeanUtil.copyProperties(platformConfigDto, platformConfig);
    return new R<>(otherPlatformConfigService.updatePlatformConfig(platformConfigDto.getId(), platformConfig));
  }
}
