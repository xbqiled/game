package com.youwan.game.cac.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.BgBetDto;
import com.youwan.game.cac.api.entity.OtherBgbetRecord;
import com.youwan.game.cac.service.OtherBgbetRecordService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-11-05 15:56:55
 */
@RestController
@AllArgsConstructor
@RequestMapping("/bgbetrecord")
public class BgbetRecordController {

  private final OtherBgbetRecordService otherBgbetRecordService;

  /**
   * 分页查询
   * @param page 分页对象
   * @return
   */
  @GetMapping("/page")
  public R getOtherBgbetRecordPage(Page page, BgBetDto bgBetDto) {
    return  new R<>(otherBgbetRecordService.queryBgBetRecordPage(page, bgBetDto));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(otherBgbetRecordService.getById(id));
  }

  /**
   * 新增
   * @param otherBgbetRecord 
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody OtherBgbetRecord otherBgbetRecord){
    return new R<>(otherBgbetRecordService.save(otherBgbetRecord));
  }

  /**
   * 修改
   * @param otherBgbetRecord 
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody OtherBgbetRecord otherBgbetRecord){
    return new R<>(otherBgbetRecordService.updateById(otherBgbetRecord));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(otherBgbetRecordService.removeById(id));
  }

}
