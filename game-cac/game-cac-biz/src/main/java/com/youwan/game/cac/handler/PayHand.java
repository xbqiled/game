package com.youwan.game.cac.handler;


import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.youwan.game.cac.api.dto.PayOrderDto;
import com.youwan.game.cac.api.entity.SysPayCode;
import com.youwan.game.cac.api.entity.SysPayOrder;
import com.youwan.game.cac.api.vo.PayConfigVo;
import com.youwan.game.cac.api.vo.PayResult;
import com.youwan.game.common.core.constant.CommonConstant;
import com.youwan.game.common.core.constant.ConfigConstant;
import com.youwan.game.common.core.pay.*;
import com.youwan.game.common.core.pay.json.*;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static com.youwan.game.common.core.constant.UrlConstant.FEONT_SYSTM_URL;

@Slf4j
public class PayHand {

    public static String PAY_URL = "/pay/autoSubmitPay";

    public static String CODE_PAY_URL = "/pay/showPayCode";

    public static Map<String, String> getPayParameter(PayConfigVo payConfigVo, String[] parameterArray) {
        Map<String, String> map = new TreeMap<>();
        if (payConfigVo != null && StrUtil.isNotBlank(payConfigVo.getPlatformKey())) {
            switch (payConfigVo.getPlatformKey()) {
                case "jidian": {
                    map = JiDianUtil.packaData(parameterArray);
                }
                break;

                case "xianfeng": {
                    map = XianFengUtil.packaData(parameterArray);
                }
                break;

                case "xinfutong": {
                    map = XinFuTongUtil.packaData(parameterArray);
                }
                break;

                case "yipai": {
                    map = YiPaiUtil.packaData(parameterArray);
                }
                break;

                case "banma": {
                    map = BanMaUtil.packaData(parameterArray);
                }
                break;

                case "beiyang": {
                    map = BeiYangUtil.packaData(parameterArray);
                }
                break;

                case "mao": {
                    map = MaoUtil.packaData(parameterArray);
                }
                break;

                case "tiandi": {
                    map = TianDiUtil.packaData(parameterArray);
                }
                break;

                case "yangguang": {
                    map = YangGuangUtil.packaData(parameterArray);
                }
                break;

                case "feifu": {
                    map = FeiFuUtil.packaData(parameterArray);
                }
                break;

                case "longcheng": {
                    map = LongChengUtil.packaData(parameterArray);
                }
                break;

                case "hongyun": {
                    map = HongYunUtil.packaData(parameterArray);
                }
                break;

                case "sanyi": {
                    map = SanYiUtil.packaData(parameterArray);
                }
                break;

                case "jubaofu": {
                    map = JuBaoFuUtil.packaData(parameterArray);
                }
                break;

                case "libao": {
                    map = LiBaoUtil.packaData(parameterArray);
                }
                break;

                case "xintiandi": {
                    map = XinTianDiUtil.packaData(parameterArray);
                }
                break;

                case "shanrubao": {
                    map = ShanRuBaoUtil.packaData(parameterArray);
                }
                break;

                case "yanyi": {
                    map = YanYiUtil.packaData(parameterArray);
                }
                break;

                default:{

                }
            }
        }
        return  map;
    }

    /**
     * <B>获取地址信息</B>
     * @param
     * @return
     */
    public static String getCodePayUrl(String orderNo) {
        StringBuffer sb = new StringBuffer();
        sb.append(FEONT_SYSTM_URL + CODE_PAY_URL);
        sb.append("?");
        sb.append("orderNo=" + orderNo);
        return sb.toString();
    }


    private static String WECHAR_ICON_URL = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/game/20190221/69f725b50bf0453aad3aa850874ed9df.jpg";
    private static String ALIPAY_ICON_URL = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/game/20190221/0ff41da4fa784febb67402c3f2bebeb9.jpg";
    private static String BANK_ICON_URL = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/game/20190324/eb7f2fd095684d3985e7b95a1c99cede.jpg";
    private static String QQ_ICON_URL = "https://youwanimage.oss-cn-shanghai.aliyuncs.com/game/20190511/54a6b80a20b0433d84c92156e6134268.jpg";


    public static String getIconUrl(Integer payType) {
        String imgUrl = "";
        switch (payType) {
            case 0: {
                //微信公众账号
                imgUrl = WECHAR_ICON_URL;
            }
            break;

            case 1: {
                //微信扫码支付
                imgUrl = WECHAR_ICON_URL;
            }
            break;

            case 2: {
                //微信网页支付
                imgUrl = WECHAR_ICON_URL;
            }
            break;

            case 3: {
                //微信app支付
                imgUrl = WECHAR_ICON_URL;
            }
            break;

            case 4: {
                //微信转账支付
                imgUrl = WECHAR_ICON_URL;
            }
            break;

            case 5: {
                //支付宝扫码
                imgUrl = ALIPAY_ICON_URL;
            }
            break;


            case 6: {
                //支付宝APP
                imgUrl = ALIPAY_ICON_URL;
            }
            break;

            case 7: {
                //支付宝网页
                imgUrl = ALIPAY_ICON_URL;
            }
            break;

            case 8: {
                //支付宝转账
                imgUrl = ALIPAY_ICON_URL;
            }
            break;

            case 9: {
                //QQ钱包Wap
                imgUrl = QQ_ICON_URL;
            }
            break;

            case 10: {
                //QQ钱包手机扫码
                imgUrl = QQ_ICON_URL;
            }
            break;

            case 11: {
                //QQ转账支付
                imgUrl = QQ_ICON_URL;
            }
            break;

            case 12: {
                //银联二维码
                imgUrl = BANK_ICON_URL;
            }
            break;

            case 13: {
                //网银B2B支付
                imgUrl = BANK_ICON_URL;
            }
            break;

            case 14: {
                //网银B2C支付
                imgUrl = BANK_ICON_URL;
            }
            break;

            case 15: {
                //无卡快捷支付
                imgUrl = BANK_ICON_URL;
            }
            break;

            case 16: {
                //银联H5快捷
                imgUrl = BANK_ICON_URL;
            }
            break;

            case 17: {
                //银行卡转账
                imgUrl = BANK_ICON_URL;
            }
            break;


            default:{
                imgUrl = QQ_ICON_URL;
            }
        }

        return  imgUrl;
    }

    /**
     * <B>创建支付URL</B>
     * @param payOrderDto
     * @param uuidStr
     * @return
     */
    public static PayResult createPayUrl(PayOrderDto payOrderDto, String uuidStr) {
        PayResult payResult  = new PayResult();
        payResult.setPayName(payOrderDto.getPayname());
        payResult.setPayFee(payOrderDto.getPayfee());
        payResult.setOrderNo(uuidStr);
        payResult.setStatus(ConfigConstant.PAYORDR_STATUS.WAIT.getValue());

        StringBuffer sb = new StringBuffer();
        sb.append(FEONT_SYSTM_URL + PAY_URL);
        sb.append("?");
        sb.append("configId=" + payOrderDto.getConfigId());
        sb.append("&channelId=" + payOrderDto.getChannelId());
        sb.append("&userId=" + payOrderDto.getUserId());
        sb.append("&payFee=" + payOrderDto.getPayfee());

        payResult.setPayUrl(sb.toString());
        payResult.setCode(ConfigConstant.PAY_RESULT.SUCCESS.getValue());
        return payResult;
    }


    /**
     *
     * @param configId
     * @param channelId
     * @return
     */
    public static SysPayCode createPayCode(BigDecimal payFee, String orderNo, String imgUrl, String payUrl, Integer configId, String channelId) {
        SysPayCode code = new SysPayCode();
        code.setChannelId(channelId);
        code.setConfigId(configId);

        code.setCreateTime(LocalDateTime.now());
        code.setImgUrl(imgUrl);

        code.setPayUrl(payUrl);
        code.setOrderNo(orderNo);

        code.setPayFee(payFee);
        return code;
    }



    /**
     * <B>根据配置组合调用信息</B>
     * @param payConfigVo
     * @param parameterArray
     * @return
     */
    public static PayResult doOnLinePay(PayConfigVo payConfigVo, PayOrderDto payOrderDto,String uuidStr, String[] parameterArray) {
        PayResult payResult  = new PayResult();
        payResult.setPayName(payOrderDto.getPayname());
        payResult.setPayFee(payOrderDto.getPayfee());
        payResult.setOrderNo(uuidStr);
        payResult.setStatus(ConfigConstant.PAYORDR_STATUS.WAIT.getValue());


        if (payConfigVo != null && StrUtil.isNotBlank(payConfigVo.getPlatformKey())) {
            switch (payConfigVo.getPlatformKey()) {
                //快联
                case "kuailian": {
                    Map<String, String> map = KuaiLianUtil.packaData(parameterArray);
                    ResultKuaiLian result = KuaiLianUtil.doPay(map);
                    if (result != null && result.getCode().equals("0000")) {
                        //发送成功
                        payResult.setCode(ConfigConstant.PAY_RESULT.SUCCESS.getValue());

                        //判断通道类型
                        if (payConfigVo.getPlatformType() != null && (payConfigVo.getPlatformType().equals("2"))) {
                            payResult.setType("1");
                        } else {
                            payResult.setType("0");
                        }
                        payResult.setPayUrl(result.getPayUrl());
                    } else {
                        //发送失败
                        payResult.setCode(ConfigConstant.PAY_RESULT.FAIL.getValue());
                    }
                }
                break;

                //鑫发
                case "xinfa": {
                    String bodyStr = XinFaUtil.packaData(parameterArray);
                    ResultXinFa result = XinFaUtil.doPay(bodyStr);

                    //获取成功
                    if (result != null && StrUtil.isNotBlank(result.getQrcodeUrl())  && result.getStateCode().equals("00")) {
                        payResult.setType("0");
                        payResult.setPayUrl(result.getQrcodeUrl());
                    } else {
                        payResult.setCode(ConfigConstant.PAY_RESULT.FAIL.getValue());
                    }
                }
                break;

                case "qitongbao" :{
                    // 这里拼接参数 map是一个参数map
                    Map<String, String> map = QiTongBaoUtil.packaData(parameterArray);
                    //发送post请求 map参数传入
                    ResultQiTongBao result = QiTongBaoUtil.doPay(map);
                    if (result != null && "200".equals(result.getRetCode())) {
                        payResult.setCode(ConfigConstant.PAY_RESULT.SUCCESS.getValue());
                        if (payConfigVo.getPlatformType() != null && (payConfigVo.getPlatformType().equals("WECHAT_NATIVE") || payConfigVo.getPlatformType().equals("WECHAT_JSAPI"))) {
                            payResult.setType("1");
                        } else {
                            payResult.setType("0");
                        }

                        //payResult放入成功后content content的参数 也就是要做成二维码的url
                        payResult.setPayUrl(result.getContent().getContent());
                    }else {
                        payResult.setCode(ConfigConstant.PAY_RESULT.FAIL.getValue());
                    }
                }
                break;

                case "chengyitong" :{
                    // 这里拼接参数 map是一个参数map
                    Map<String, String> map = ChengYiTongUtil.packaData(parameterArray);
                    //发送post请求 map参数传入
                    ResultChengFuTong result = ChengYiTongUtil.doPay(map);
                    if (result != null && result.getRspCode() == 1) {
                        payResult.setCode(ConfigConstant.PAY_RESULT.SUCCESS.getValue());
                        if (payConfigVo.getPlatformType() != null && (payConfigVo.getPlatformType().equals("UFALIPAY") || payConfigVo.getPlatformType().equals("UFWECHAT"))) {
                            payResult.setType("1");
                        } else {
                            payResult.setType("0");
                        }

                        //payResult放入成功后content content的参数 也就是要做成二维码的url
                        payResult.setPayUrl(result.getData().getR6_qrcode());
                    }else {
                        payResult.setCode(ConfigConstant.PAY_RESULT.FAIL.getValue());
                    }
                }
                break;

                case "renren" :{
                    // 这里拼接参数 map是一个参数map
                    Map<String, Object> map = RenRenUtil.packaData(parameterArray);
                    //发送post请求 map参数传入
                    UrlResultRenRen result = RenRenUtil.doPay(map, parameterArray[9]);
                    if (result != null && StrUtil.isNotBlank(result.getURL())) {
                        payResult.setCode(ConfigConstant.PAY_RESULT.SUCCESS.getValue());
                        payResult.setType("0");

                        //payResult放入成功后content content的参数 也就是要做成二维码的url
                        payResult.setPayUrl(result.getURL());
                    }else {
                        payResult.setCode(ConfigConstant.PAY_RESULT.FAIL.getValue());
                    }
                }
                break;

                case "niuniu" :{
                    // 这里拼接参数 map是一个参数map
                    Map<String, String> map = NiuNiuUtil.packaData(parameterArray);
                    //发送post请求 map参数传入
                    ResultNiuNiu result = NiuNiuUtil.doPay(map);
                    if (result != null && StrUtil.isNotBlank(result.getHttp_url())) {
                        payResult.setCode(ConfigConstant.PAY_RESULT.SUCCESS.getValue());
                        payResult.setType("0");
                        //payResult放入成功后content content的参数 也就是要做成二维码的url
                        payResult.setPayUrl(result.getHttp_url());
                    }else {
                        payResult.setCode(ConfigConstant.PAY_RESULT.FAIL.getValue());
                    }
                }
                break;

                case "mao" :{
                    // 这里拼接参数 map是一个参数map
                    Map<String, String> map = MaoUtil.packaData(parameterArray);
                    //发送post请求 map参数传入
                    ResultMao result = MaoUtil.doPay(map);
                    if (result != null && StrUtil.isNotBlank(result.getData().getPayUrl())) {
                        payResult.setCode(ConfigConstant.PAY_RESULT.SUCCESS.getValue());
                        payResult.setType("0");
                        //payResult放入成功后content content的参数 也就是要做成二维码的url
                        payResult.setPayUrl(result.getData().getPayUrl());
                    }else {
                        payResult.setCode(ConfigConstant.PAY_RESULT.FAIL.getValue());
                    }
                }
                break;

                case "shunda" :{
                    // 这里拼接参数 map是一个参数map
                    Map<String, String> map = ShunDaUtil.packaData(parameterArray);
                    //发送post请求 map参数传入
                    ResultShunDa result = ShunDaUtil.doPay(map);
                    if (result != null && StrUtil.isNotBlank(result.getPayParams().getUrl())) {
                        payResult.setCode(ConfigConstant.PAY_RESULT.SUCCESS.getValue());
                        payResult.setType("0");
                        //payResult放入成功后content content的参数 也就是要做成二维码的url
                        payResult.setPayUrl(result.getPayParams().getUrl());
                    }else {
                        payResult.setCode(ConfigConstant.PAY_RESULT.FAIL.getValue());
                    }
                }
                break;

                case "dingding" :{
                    // 这里拼接参数 map是一个参数map
                    Map<String, String> map = DingDingUtil.packaData(parameterArray);
                    //发送post请求 map参数传入
                    ResultDingDing result = DingDingUtil.doPay(map);
                    if (result != null && result.getStatus().equalsIgnoreCase("1") && StrUtil.isNotBlank(result.getPayUrl())) {
                        payResult.setCode(ConfigConstant.PAY_RESULT.SUCCESS.getValue());
                        payResult.setType("0");
                        //payResult放入成功后content content的参数 也就是要做成二维码的url
                        payResult.setPayUrl(result.getPayUrl());
                    }else {
                        payResult.setCode(ConfigConstant.PAY_RESULT.FAIL.getValue());
                    }
                }
                break;

                case "guaishou": {
                    Map<String, String> map = GuaiShouUtil.packaData(parameterArray);
                    //发送post请求 map参数传入
                    ResultGuaiShou result = GuaiShouUtil.doPay(map);
                    if (result != null && result.getResult() && StrUtil.isNotBlank(result.getData().getPayment_code())) {
                        payResult.setCode(ConfigConstant.PAY_RESULT.SUCCESS.getValue());
                        payResult.setType("0");
                        //payResult放入成功后content content的参数 也就是要做成二维码的url
                        payResult.setPayUrl(result.getData().getPayment_code());
                    }else {
                        payResult.setCode(ConfigConstant.PAY_RESULT.FAIL.getValue());
                    }
                }
                break;

                case "may": {
                    Map<String, String> map = MayUtil.packaData(parameterArray);
                    //发送post请求 map参数传入
                    ResultMay result = MayUtil.doPay(map);
                    if (result != null && result.getData() != null && StrUtil.isNotBlank(result.getData().getPay_url())) {
                        payResult.setCode(ConfigConstant.PAY_RESULT.SUCCESS.getValue());
                        payResult.setType("0");
                        //payResult放入成功后content content的参数 也就是要做成二维码的url
                        payResult.setPayUrl(result.getData().getPay_url());
                    }else {
                        payResult.setCode(ConfigConstant.PAY_RESULT.FAIL.getValue());
                    }
                }
                break;


                case "xinxing": {
                    Map<String, String> map = XinXingUtil.packaData(parameterArray);
                    //发送post请求 map参数传入
                    ResultXinXing result = XinXingUtil.doPay(map);
                    if (result != null && result.getPayUrl() != null && result.getStatusCode().equalsIgnoreCase("SUCCESS")) {
                        payResult.setCode(ConfigConstant.PAY_RESULT.SUCCESS.getValue());
                        payResult.setType("0");
                        //payResult放入成功后content content的参数 也就是要做成二维码的url
                        payResult.setPayUrl(result.getPayUrl());
                    }else {
                        payResult.setCode(ConfigConstant.PAY_RESULT.FAIL.getValue());
                    }
                }
                break;

                case "jinzun": {
                    Map<String, String> map = JinZunUtil.packaData(parameterArray);
                    //发送post请求 map参数传入
                    Map<String,String> resMap = JinZunUtil.doPay(map);
                    if (resMap.containsKey("payUrl")) {
                        payResult.setCode(ConfigConstant.PAY_RESULT.SUCCESS.getValue());
                        payResult.setType("0");
                        //payResult放入成功后content content的参数 也就是要做成二维码的url
                        payResult.setPayUrl(resMap.get("payUrl"));
                    }else {
                        log.debug("金樽支付接口返回-->{}",resMap.get("msg"));
                        payResult.setCode(ConfigConstant.PAY_RESULT.FAIL.getValue());
                    }
                }
                break;

                case "hongyuntong": {
                    Map<String, String> map = HongYunTongUtil.packaData(parameterArray);
                    //发送post请求 map参数传入
                    String result = HongYunTongUtil.doPay(map);
                    JSONObject json = JSONObject.parseObject(result);
                    if ("0".equalsIgnoreCase(json.getString("Code"))) {
                        payResult.setCode(ConfigConstant.PAY_RESULT.SUCCESS.getValue());
                        payResult.setType("0");
                        payResult.setPayUrl(json.getString("Url"));
                    }else {
                        log.warn("鸿运通支付接口返回-->{}",json.toJSONString());
                        payResult.setCode(ConfigConstant.PAY_RESULT.FAIL.getValue());
                    }
                }
                break;

                case "youyifu": {
                    Map<String, String> map = YouYiFuUtil.packaData(parameterArray);
                    //发送post请求 map参数传入
                    String result = YouYiFuUtil.doPay(map);
                    if (JSONUtil.isJson(result) && JSONObject.parseObject(result).containsKey("data")) {
                        payResult.setCode(ConfigConstant.PAY_RESULT.SUCCESS.getValue());
                        payResult.setType("0");
                        payResult.setPayUrl(JSONObject.parseObject(result).getJSONObject("data").getString("url"));
                    }else {
                        log.debug("优易付支付接口返回-->{}",result);
                        payResult.setCode(ConfigConstant.PAY_RESULT.FAIL.getValue());
                    }
                }
                break;

                case "duoduo": {
                    Map<String, String> map = DuoDuoUtil.packaData(parameterArray);
                    //发送post请求 map参数传入
                    String result = DuoDuoUtil.doPay(map);
                    if (JSONUtil.isJson(result) && JSONObject.parseObject(result).containsKey("retCode") && "SUCCESS".equalsIgnoreCase(JSONObject.parseObject(result).getString("retCode"))) {
                        payResult.setCode(ConfigConstant.PAY_RESULT.SUCCESS.getValue());
                        payResult.setType("0");
                        payResult.setPayUrl(JSONObject.parseObject(result).getJSONObject("payParams").getString("payUrl"));
                    }else {
                        log.debug("多多支付接口返回-->{}",result);
                        payResult.setCode(ConfigConstant.PAY_RESULT.FAIL.getValue());
                    }
                }
                break;

                default:{

                }
            }
        }
        return payResult;
    }


    /**
     * <B>组装map信息</B>
     * @return
     */
    public static String [] packagePayMap(PayOrderDto payOrderDto, String uuidStr, PayConfigVo vo,String ip) {
        List<String> list = new ArrayList<>();
        //商户号
        list.add(vo.getMerchantCode());
        //订单号
        list.add(uuidStr);
        //支付金额
        list.add(payOrderDto.getPayfee());
        //支付类型
        list.add(vo.getPlatformType().toString());
        //网关
        list.add(vo.getPayGetway());
        //产品名称
        list.add(CommonConstant.GOLD_COIN_BUY);
        //商户秘钥
        list.add(vo.getMerchantKey());
        //BankCode
        list.add(payOrderDto.getBankCode());
        //用戶ID
        list.add(payOrderDto.getUserId());
        //商户公钥
        list.add(vo.getPublicKey());
        //appID
        list.add(vo.getAppKey());
        //ip
        list.add(ip);

        String [] payParameter = new String [list.size()];
        return list.toArray(payParameter);
    }


    public static SysPayOrder packagePayOrdery(PayOrderDto payOrderDto, String uuidStr, PayConfigVo vo, int handType) {
        //创建支付订单
        SysPayOrder sysPayOrder = new SysPayOrder();
        //银行卡转账支付
        sysPayOrder.setOrderNo(uuidStr);
        sysPayOrder.setUserId(Integer.parseInt(payOrderDto.getUserId()));
        BigDecimal payDecimal = new BigDecimal(payOrderDto.getPayfee());

        sysPayOrder.setPayFee(payDecimal);
        BigDecimal poundageDecimal = new BigDecimal(0);
        sysPayOrder.setPoundage(poundageDecimal);

        sysPayOrder.setChannelId(payOrderDto.getChannelId());
        sysPayOrder.setPayConfigId(Integer.parseInt(payOrderDto.getConfigId()));
        sysPayOrder.setPayName(vo.getPayName());

        sysPayOrder.setStatus(Integer.parseInt(ConfigConstant.PAYORDR_STATUS.WAIT.getValue()));
        sysPayOrder.setDepositor(payOrderDto.getPayname());
        sysPayOrder.setAccount(payOrderDto.getPayaccount());

        sysPayOrder.setHandlerType(handType);
        sysPayOrder.setCreateBy("SYSTEM");
        sysPayOrder.setCreateTime(LocalDateTime.now());
        return sysPayOrder;
    }
}
