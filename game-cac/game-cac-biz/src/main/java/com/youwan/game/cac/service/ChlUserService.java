package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.ChlUser;


/**
 * 系统用户
 *
 * @author code generator
 * @date 2019-02-09 17:16:48
 */
public interface ChlUserService extends IService<ChlUser> {
    




}
