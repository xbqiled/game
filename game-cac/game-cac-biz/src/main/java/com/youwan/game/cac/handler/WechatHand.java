package com.youwan.game.cac.handler;


import cn.hutool.core.util.ArrayUtil;
import com.youwan.game.cac.api.entity.Domain;

public class WechatHand {

    public static String DOMAIN_HEAD = "https://front.";

    /**
     * <B>生成跳转的中间链接,根据域名动态生成</B>
     *
     * @param domain
     * @return
     */
    public static String createJumpUrl(String uuid, Domain domain) {
        //默认为三段式域名
        String domainUrl = domain.getDomainUrl();
        StringBuffer sb  = new StringBuffer();

        //本地测试先注释掉
        sb.append(DOMAIN_HEAD);
        String [] domainStr = domainUrl.split("\\.");

        if (ArrayUtil.isNotEmpty(domainStr)  && domainStr.length == 3) {
            sb.append(domainStr[1]);
            sb.append(".");
            sb.append(domainStr[2]);
        }

        //sb.append(domainUrl);
        sb.append("/front/download/shareJump?doId=");
        sb.append(uuid);
        return sb.toString();
    }



    public  String getJumpUrl(String url) {
        StringBuffer sb  = new StringBuffer();
        sb.append(DOMAIN_HEAD);
        String [] domainStr = url.split("\\.");

        if (ArrayUtil.isNotEmpty(domainStr)  && domainStr.length == 3) {
            sb.append(domainStr[1]);
            sb.append(".");
            sb.append(domainStr[2]);
        }
        return sb.toString();
    }

}
