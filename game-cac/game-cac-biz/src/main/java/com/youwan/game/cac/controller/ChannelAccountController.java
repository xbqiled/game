package com.youwan.game.cac.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.ChannelAccountDto;
import com.youwan.game.cac.api.entity.ChannelAccount;
import com.youwan.game.cac.service.OtherChannelAccountService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-11-19 14:07:23
 */
@RestController
@AllArgsConstructor
@RequestMapping("/channelaccount")
public class ChannelAccountController {

  private final OtherChannelAccountService otherChannelBalanceService;

  /**
   * 分页查询
   * @param
   * @param channelBalanceDto
   * @return
   */
  @GetMapping("/page")
  public R getOtherChannelBalancePage(Page page, ChannelAccountDto channelBalanceDto) {
    return  new R<>(otherChannelBalanceService.getChannelAccountPage(page, channelBalanceDto));
  }

  /**
   * 新增
   * @param channelAccountDto
   * @return R
   */
  @SysLog("修改账户余额")
  @PutMapping
  public R updateChannelAccount(@RequestBody ChannelAccountDto channelAccountDto){
    Integer result = otherChannelBalanceService.updateChannelAccount(channelAccountDto);
    if (result == 0) {
      return new R<>();
    } else {
      return new R<>().setCode(1).setMsg("渠道配置错误");
    }
  }
}
