package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.SysDownConfig;
import com.youwan.game.cac.api.entity.SysPayPlatform;
import com.youwan.game.cac.api.vo.DownloadConfigVo;
import com.youwan.game.cac.api.vo.DownloadConfigVo1;

import java.util.List;


/**
 * 
 *
 * @author code generator
 * @date 2019-01-26 14:00:25
 */
public interface SysDownConfigService extends IService<SysDownConfig> {


    IPage<List<SysDownConfig>> queryDownConfigPage(Page page, String channelId);

    /**
     * <B>添加信息</B>
     * @param sysDownConfig
     * @return
     */
    Boolean saveDownConfig(SysDownConfig sysDownConfig);


    /**
     * <B>修改信息</B>
     * @param sysDownConfig
     * @return
     */
    Boolean updateDownConfig(SysDownConfig sysDownConfig);



    /**
     * <B>激活配置信息</B>
     * @param id
     * @return
     */
     Boolean activateDownConfig(Integer id, Integer channelId);


    /**
     * <B>删除下载配置信息</B>
     * @param id
     * @return
     */
    Boolean deleteDownConfig(Integer id);

    /**
     * <B>通过channelId 获取配置信息</B>
     * @param channelId
     * @return
     */
    DownloadConfigVo getConfigByChannelId(String channelId);


    DownloadConfigVo1 getConfig1ByChannelId(String channelId);

}
