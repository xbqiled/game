package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.CsInfoDto;
import com.youwan.game.cac.api.entity.CsInfo;
import com.youwan.game.cac.mapper.CsInfoMapper;
import com.youwan.game.cac.service.CsInfoService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * 
 *
 * @author code generator
 * @date 2019-03-29 15:44:57
 */
@Service("csInfoService")
public class CsInfoServiceImpl extends ServiceImpl<CsInfoMapper, CsInfo> implements CsInfoService {

    public Boolean saveCsinfo(CsInfo ssInfo) {
        ssInfo.setCreateTime(LocalDateTime.now());
        ssInfo.setCreateBy(Objects.requireNonNull(getUsername()));

        this.save(ssInfo);
        return Boolean.TRUE;
    }

    @Override
    public IPage<List<CsInfo>> queryCsInfoPage(Page page, CsInfo csInfo) {
        return baseMapper.queryCsInfoPage(page, csInfo.getChannelId(), csInfo.getType());
    }

    @Override
    public List<CsInfo> getCsInfoByChannelId(String channelId) {
        return baseMapper.getCsInfoByChannelId(channelId);
    }

    @Override
    public List<CsInfo> getCsInfo(CsInfoDto csInfoDto) {
        return baseMapper.getCsInfo(csInfoDto.getChannelId(), csInfoDto.getType());
    }

    /**
     * 获取用户名称
     *
     * @return username
     */
    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }
}
