package com.youwan.game.cac.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.ReportDto;
import com.youwan.game.cac.api.entity.StatisChannel;
import com.youwan.game.cac.api.vo.StatisChannelVo;
import com.youwan.game.cac.mapper.StatisChannelMapper;
import com.youwan.game.cac.service.StatisChannelService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2020-02-28 11:22:35
 */
@Service("statisChannelService")
public class StatisChannelServiceImpl extends ServiceImpl<StatisChannelMapper, StatisChannel> implements StatisChannelService {

    private static final String TABLE_NAME = "statis_channel";

    /**
     * <B>拼接SQL语句</B>
     * @param list
     * @return
     */
    private String spliceQuerySql(List<String> list) {
        StringBuffer sb = new StringBuffer();
        sb.append("(");
        if (CollUtil.isNotEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                sb.append(" select * From " + list.get(i));
                if (i < list.size() - 1) {
                    sb.append("  union all ");
                }
            }
        }
        sb.append(")");
        return sb.toString();
    }


    private String spliceInSql(List<String> list) {
        StringBuffer sb = new StringBuffer();
        sb.append("(");
        if (CollUtil.isNotEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                sb.append("'" + list.get(i) + "'");
                if (i < list.size() - 1) {
                    sb.append(" , ");
                }
            }
        }
        sb.append(")");
        return sb.toString();
    }

    private List<String> getIntervalMonth(String startTime, String endTime) {
        List<String> list = new ArrayList<>();
        //转化成月份
        Date startMonthDate = DateUtil.parse(startTime, "yyyy-MM-dd");
        Date endMonthDate = DateUtil.parse(endTime, "yyyy-MM-dd");

        String endMonth = DateUtil.format(endMonthDate, "yyyyMM");
        Date nextDate = startMonthDate;

        //判断时间不等于结束时间
        while (!DateUtil.format(nextDate, "yyyyMM").equals(endMonth)) {
            list.add(TABLE_NAME + DateUtil.format(nextDate, "yyyyMM"));
            //获取下一个月
            nextDate = DateUtil.offsetMonth(nextDate, 1);
        }

        list.add(TABLE_NAME + endMonth);
        return list;
    }


    @Override
    public IPage<List<StatisChannelVo>> getStatisDayReportPage(Page page, ReportDto reportDto) {
        StringBuffer tableSql = new StringBuffer();
        //获取当前时间
        List<String> list = getIntervalMonth(reportDto.getStartTime(), reportDto.getEndTime());
        String sql = spliceInSql(list);
        List<String> tableList = baseMapper.queryHaveTab(sql);

        //组成查询SQL
        tableSql.append(spliceQuerySql(tableList));
        return baseMapper.getDayReportPage(page, tableList, reportDto.getChannelNo(), reportDto.getStartTime(), reportDto.getEndTime());
    }


    @Override
    public IPage<List<StatisChannelVo>> getStatisMonthReportPage(Page page, ReportDto reportDto) {
        Date queryMonth = DateUtil.parse(reportDto.getMonthTime(), "yyyy-MM");
        String month = DateUtil.format(queryMonth, "yyyyMM");
        return baseMapper.getMonthReportPage(page, TABLE_NAME + month, reportDto.getChannelNo(), reportDto.getMonthTime());
    }
}
