package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.SocialConfig;

/**
 * 
 *
 * @author code generator
 * @date 2019-03-26 15:43:34
 */
public interface SocialConfigService extends IService<SocialConfig> {


    Boolean addSocialConfig(SocialConfig socialConfig);


    SocialConfig getSocialConfigById(String channelId);

}
