package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.CfgChlDownload;
import com.youwan.game.cac.api.vo.DownloadPanelConfigVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-18 16:08:14
 */
public interface CfgChlDownloadMapper extends BaseMapper<CfgChlDownload> {

    IPage<List<CfgChlDownload>> queryDownloadCfgPage(Page page, @Param("channelId") String channelId);

    DownloadPanelConfigVo getConfigVoByChannelId(@Param("channelId") String channelId);

    CfgChlDownload getConfigByChannelId(@Param("channelId") String channelId);
}
