package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.SmsDto;
import com.youwan.game.cac.api.entity.SysSms;
import com.youwan.game.cac.api.entity.SysSmsTemplate;
import com.youwan.game.cac.api.entity.TipSoundToken;

import java.util.List;


/**
 * 
 *
 * @author code generator
 * @date 2019-01-22 14:18:24
 */
public interface SysSmsService extends IService<SysSms> {

    IPage<List<SysSms>> querySmsPage(Page page, SysSms sysSms);

    Boolean saveAndSendSms(SysSms sysSms);

    Boolean sendSms(SmsDto sms, SysSmsTemplate template);

}
