package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.BindMobile;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2019-04-10 21:03:25
 */
public interface BindMobileMapper extends BaseMapper<BindMobile> {

    IPage<List<Map<String, Object>>> queryBindPage(Page page, @Param("channelId") String channelId);

    BindMobile getByChannleId( @Param("channelId") String channelId);

    void updateBind(@Param("channelId") String channelId, @Param("modifyTime") LocalDateTime modifyTime,
                    @Param("modifyBy") String modifyBy, @Param("giveValue") BigDecimal giveValue,
                    @Param("rebate") Integer rebate,  @Param("exchange") Integer exchange,
                    @Param("initCoin")Integer initCoin, @Param("minExchange") Integer minExchange,
                    @Param("damaMulti") Integer damaMulti, @Param("rebateDamaMulti") Integer rebateDamaMulti,
                    @Param("exchangeCount") String exchangeCount, @Param("promoteType") String promoteType,
                    @Param("resetDamaLeftCoin")String resetDamaLeftCoin, @Param("cpStationId")String cpStationId,@Param("cpLimit") Integer cpLimit);

}
