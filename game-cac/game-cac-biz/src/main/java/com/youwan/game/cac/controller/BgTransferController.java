package com.youwan.game.cac.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.BgTransferDto;
import com.youwan.game.cac.api.entity.OtherBgTransfer;
import com.youwan.game.cac.service.OtherBgTransferService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-11-05 15:57:02
 */
@RestController
@AllArgsConstructor
@RequestMapping("/bgtransfer")
public class BgTransferController {

  private final OtherBgTransferService otherBgTransferService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param bgTransferDto
   * @return
   */
  @GetMapping("/page")
  public R getOtherBgTransferPage(Page page, BgTransferDto bgTransferDto) {
    return  new R<>(otherBgTransferService.queryBgTransferPage(page, bgTransferDto));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(otherBgTransferService.getById(id));
  }

  /**
   * 新增
   * @param otherBgTransfer 
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody OtherBgTransfer otherBgTransfer){
    return new R<>(otherBgTransferService.save(otherBgTransfer));
  }

  /**
   * 修改
   * @param otherBgTransfer 
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody OtherBgTransfer otherBgTransfer){
    return new R<>(otherBgTransferService.updateById(otherBgTransfer));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(otherBgTransferService.removeById(id));
  }

}
