package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.SysDownConfig;
import com.youwan.game.cac.api.vo.DownloadConfigVo;
import com.youwan.game.cac.api.vo.DownloadConfigVo1;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-26 14:00:25
 */
public interface SysDownConfigMapper extends BaseMapper<SysDownConfig> {


    IPage<List<SysDownConfig>> queryDownConfigPage(Page page, @Param("channelId") String channelId);

    /**
     * <B>修改所有的配置为失效</B>
     */
    void updateDownConfigInvalid(@Param("channelId") Integer channelId);


    /**
     * <B>激活对应ID的配置信息</B>
     * @param id
     * @return
     */
    int  activateById(@Param("id") Integer id);


    /**
     * <B>获取下载配置信息</B>
     * @param channelId
     * @return
     */
    DownloadConfigVo getConfigByChannelId(@Param("channelId") String channelId);



    DownloadConfigVo1 getConfig1ByChannelId(@Param("channelId") String channelId);
}
