package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.LuckyRoulette;
import com.youwan.game.cac.mapper.xbqp.LuckyRouletteMapper;
import com.youwan.game.cac.service.LuckyRouletteService;
import com.youwan.game.common.core.jsonmodel.GsonLuckRoulette;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.HttpHandler;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-22 15:14:43
 */
@Service("luckyrouletteService")
public class LuckyRouletteServiceImpl extends ServiceImpl<LuckyRouletteMapper, LuckyRoulette> implements LuckyRouletteService {

    @Override
    public IPage<List<Map<String, Object>>> queryLuckyRoulettePage(Page page, LuckyRoulette luckyRoulette) {
        return baseMapper.queryLuckyRoulettePage(page, luckyRoulette.getTChannel(), "", "");
    }

    @Override
    public int saveLuckRoulette(String channelId, List<GsonLuckRoulette> diamondcfg, List<GsonLuckRoulette> goldcfg, List<GsonLuckRoulette> silvercfg,
                                String startTimeStr, String finishTimeStr, Integer goldcost, Integer diamondcost, Integer silvercost) {
        //时间转换
        Long start = Long.valueOf(startTimeStr) / 1000;
        Long finish = Long.valueOf(finishTimeStr) / 1000;

        GsonResult result = HttpHandler.luckyRoulette(Long.valueOf(channelId), diamondcfg, goldcfg, silvercfg, start, finish, diamondcost, goldcost, silvercost);
        if (result != null) {
            return result.getRet();
        }
        return 1;
    }
}
