package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.TipOrder;
import com.youwan.game.cac.service.TipOrderService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-05-21 11:20:06
 */
@RestController
@AllArgsConstructor
@RequestMapping("/tiporder")
public class TipOrderController {

  private final TipOrderService tipOrderService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param tipOrder 
   * @return
   */
  @GetMapping("/page")
  public R getTipOrderPage(Page page, TipOrder tipOrder) {
    return  new R<>(tipOrderService.page(page, Wrappers.query(tipOrder)));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(tipOrderService.getById(id));
  }

  /**
   * 新增
   * @param tipOrder 
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody TipOrder tipOrder){
    return new R<>(tipOrderService.save(tipOrder));
  }

  /**
   * 修改
   * @param tipOrder 
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody TipOrder tipOrder){
    return new R<>(tipOrderService.updateById(tipOrder));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(tipOrderService.removeById(id));
  }

}
