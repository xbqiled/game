package com.youwan.game.cac.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.BulletinDto;
import com.youwan.game.cac.api.entity.Bulletin;
import com.youwan.game.cac.api.entity.TChannel;
import com.youwan.game.cac.api.vo.BulletinVo;
import com.youwan.game.cac.mapper.xbqp.BulletinMapper;
import com.youwan.game.cac.mapper.ChannelMapper;
import com.youwan.game.cac.service.BulletinService;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.HttpHandler;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-29 13:18:03
 */
@Slf4j
@AllArgsConstructor
@Service("bulletinService")
public class BulletinServiceImpl extends ServiceImpl<BulletinMapper, Bulletin> implements BulletinService {

    private final ChannelMapper channelMapper;

    @Override
    public IPage<Bulletin> queryBulletinPage(Page page, String tid, String ttitle) {
        return baseMapper.queryBulletinPage(page, tid, ttitle);
    }

    @Override
    public List<Map<String, Object>> queryBulletinChannel(String bulletinId) {
        return baseMapper.queryBulletinChannel(bulletinId);
    }

    @Override
    public BulletinVo getBulletinVoById(String id) {
        return baseMapper.getBulletinVoById(id);
    }

    /**
     * <B>添加公告信息</B>
     * @param bulletinDto
     * @return
     */
    @Override
    public int addBulletin(BulletinDto bulletinDto) {
        String guid = IdUtil.simpleUUID();
        //String hyperlink = UrlConstant.FEONT_SYSTM_URL +  UrlConstant.BULLETIN_URL + guid;
        String hyperlink = bulletinDto.getImgUrl();

        //转换成数组
        String [] channleList = {};
        if (StrUtil.isNotBlank(bulletinDto.getChannelList())) {
            String channleStr = bulletinDto.getChannelList();

            if (channleStr.contains(",")) {
                String[] channelDesc = channleStr.split(",");
                if (channelDesc != null && channelDesc.length > 0) {

                    channleList = new String [channelDesc.length];
                    for (int i = 0; i < channelDesc.length; i++) {
                        channleList[i] = channelDesc[i];
                    }
                }
            } else {
                TChannel channel = channelMapper.getChannelInfo(bulletinDto.getChannelList());
                if (channel != null && StrUtil.isNotBlank(channel.getChannelName())) {
                    channleList = new String[1];
                    channleList[0] = channleStr;
                } else {
                    return 1;
                }
            }
        }

        Integer start = Integer.parseInt(bulletinDto.getStart().substring(0, 10));
        Integer finish = Integer.parseInt(bulletinDto.getFinish().substring(0, 10));

        GsonResult result = HttpHandler.addBulletin(guid, start, finish, bulletinDto.getTitle(), bulletinDto.getContent(), hyperlink , channleList);
        if (result != null && result.getRet() == 0) {
            return  0;
        } else {
            return  2;
        }
    }

    /**
     * <B>修改公告信息</B>
     * @param bulletinDto
     * @return
     */
    @Override
    public int modifyBulletin(BulletinDto bulletinDto) {
        //String hyperlink = UrlConstant.FEONT_SYSTM_URL +  UrlConstant.BULLETIN_URL + bulletinDto.getTid();
        String hyperlink = bulletinDto.getImgUrl();

        //转换成数组
        String [] channleList = {};
        //Boolean resultB = filterBulletin(channleList, bulletinDto.getChannelList());
        if (StrUtil.isNotBlank(bulletinDto.getChannelList())) {
            String channleStr = bulletinDto.getChannelList();

            if (channleStr.contains(",")) {
                String[] channelDesc = channleStr.split(",");
                if (channelDesc != null && channelDesc.length > 0) {

                    channleList = new String [channelDesc.length];
                    for (int i = 0; i < channelDesc.length; i++) {
                        channleList[i] = channelDesc[i];
                    }
                }
            } else {
                TChannel channel = channelMapper.getChannelInfo(bulletinDto.getChannelList());
                if (channel != null && StrUtil.isNotBlank(channel.getChannelName())) {
                    channleList = new String[1];
                    channleList[0] = channleStr;
                } else {
                    return 1;
                }
            }
        }

        Integer start = Integer.parseInt(bulletinDto.getStart().substring(0, 10));
        Integer finish = Integer.parseInt(bulletinDto.getFinish().substring(0, 10));

        GsonResult result = HttpHandler.modifyBulletin(bulletinDto.getTid(), start, finish, bulletinDto.getTitle(), bulletinDto.getContent(), hyperlink , channleList);
        if (result != null && result.getRet() == 0) {
            return  0;
        } else {
            return  2;
        }
    }

    /**
     * <B>删除公告信息</B>
     * @param bulletinId
     * @return
     */
    @Override
        public int delBulletin(String bulletinId) {
        GsonResult result = HttpHandler.batchDelBulletin(new String[]{bulletinId});
        if (result != null && result.getRet() == 0) {
            return  0;
        } else {
            return  1;
        }
    }
}
