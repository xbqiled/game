package com.youwan.game.cac.mapper.xbqp;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.youwan.game.cac.api.entity.Gamectrl;
import com.youwan.game.cac.api.vo.GameCtrlVo;
import com.youwan.game.cac.api.vo.GameRoomVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-11 17:01:03
 */
public interface GamectrlMapper extends BaseMapper<Gamectrl> {

    IPage<List<GameCtrlVo>> queryGameCtrlPage(Page page, @Param("gameRoomId") String gameRoomId);

    List<GameRoomVo> queryGameRoomList();

    List<GameRoomVo> queryGameList();
}
