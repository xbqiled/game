package com.youwan.game.cac.config;

import com.youwan.game.common.data.datasource.DataSourceContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;

import static com.youwan.game.cac.config.DS.*;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-05-07.
 */
@Aspect
@Slf4j
@Order(-10)
public class DataSourceExchange {

    @Around("execution(* com.youwan.game.cac.mapper.platform.*.*(..))")
    public Object switchToPlatform(ProceedingJoinPoint pjp) throws Throwable {
        log.info("切换到[{}]数据源...", PLATFORM);
        DataSourceContextHolder.setDSKey(PLATFORM);
        Object result = pjp.proceed();
        DataSourceContextHolder.clear();
        return result;
    }

    @Around("execution(* com.youwan.game.cac.mapper.xbqp..*.*(..))")
    public Object switchToXbqp(ProceedingJoinPoint pjp) throws Throwable {
        log.info("切换到[{}]数据源...", XBQP);
        DataSourceContextHolder.setDSKey(XBQP);
        Object result = pjp.proceed();
        DataSourceContextHolder.clear();
        return result;
    }

    @Around("execution(* com.youwan.game.cac.mapper.recordlog..*.*(..))")
    public Object switchToRecordLog(ProceedingJoinPoint pjp) throws Throwable {
        log.info("切换到[{}]数据源...", RECORD_LOG);
        DataSourceContextHolder.setDSKey(RECORD_LOG);
        Object result = pjp.proceed();
        DataSourceContextHolder.clear();
        return result;
    }

}
