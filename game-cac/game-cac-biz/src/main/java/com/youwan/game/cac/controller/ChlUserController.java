package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.ChlUser;
import com.youwan.game.cac.service.ChlRoleService;
import com.youwan.game.cac.service.ChlUserService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 系统用户
 *
 * @author code generator
 * @date 2019-02-09 17:16:48
 */
@RestController
@AllArgsConstructor
@RequestMapping("/chluser")
public class ChlUserController {

  private final ChlUserService chlUserService;

  private final ChlRoleService chlRoleService;


  /**
   * 分页查询
   * @param page 分页对象
   * @param chlUser 系统用户
   * @return
   */
  @GetMapping("/page")
  public R getChlUserPage(Page page, ChlUser chlUser) {
    return  new R<>(chlUserService.page(page, Wrappers.query(chlUser)));
  }

  /**
   * 通过id查询系统用户
   * @param userId id
   * @return R
   */
  @GetMapping("/{userId}")
  public R getById(@PathVariable("userId") Long userId){
    return new R<>(chlUserService.getById(userId));
  }

  /**
   * 新增系统用户
   * @param chlUser 系统用户
   * @return R
   */
  @SysLog("新增系统用户")
  @PostMapping
  public R save(@RequestBody ChlUser chlUser){
    return new R<>(chlUserService.save(chlUser));
  }

  /**
   * 修改系统用户
   * @param chlUser 系统用户
   * @return R
   */
  @SysLog("修改系统用户")
  @PutMapping
  public R updateById(@RequestBody ChlUser chlUser){
    return new R<>(chlUserService.updateById(chlUser));
  }

  /**
   * 通过id删除系统用户
   * @param userId id
   * @return R
   */
  @SysLog("删除系统用户")
  @DeleteMapping("/{userId}")
  public R removeById(@PathVariable Long userId){
    return new R<>(chlUserService.removeById(userId));
  }

}
