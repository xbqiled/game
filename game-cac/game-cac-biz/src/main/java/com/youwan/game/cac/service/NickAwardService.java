package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.NickAwardEntity;
import com.youwan.game.cac.api.vo.NickAwardVo;

import java.util.List;

public interface NickAwardService extends IService<NickAwardEntity> {

    IPage<List<NickAwardVo>> queryPage(Page page, QueryDto queryDto);

    NickAwardVo getByChannelId(String channelId);

    int saveNcikAwardCfg(String channelId, Integer openType, Integer award);

}
