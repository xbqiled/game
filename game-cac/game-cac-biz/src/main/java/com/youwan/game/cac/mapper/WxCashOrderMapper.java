package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.WxCashOrder;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-06 13:34:56
 */
public interface WxCashOrderMapper extends BaseMapper<WxCashOrder> {

}
