package com.youwan.game.cac.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.DaMaValueDto;
import com.youwan.game.cac.api.entity.Accountinfo;
import com.youwan.game.cac.api.entity.Userbaseattr;
import com.youwan.game.cac.api.vo.UserCashInfoVo;
import com.youwan.game.cac.api.vo.UserInfoVo;
import com.youwan.game.cac.service.AccountinfoService;
import com.youwan.game.cac.service.UserbaseattrService;
import com.youwan.game.common.core.jsonmodel.GsonCashAli;
import com.youwan.game.common.core.jsonmodel.GsonCashBank;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.common.security.annotation.Inner;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;


/**
 * @author code lion
 * @date 2018-12-27 15:11:02
 */
@RestController
@AllArgsConstructor
@RequestMapping("/accountinfo")
public class AccountinfoController {

    private final AccountinfoService accountinfoService;
    private final UserbaseattrService userbaseattrService;

    @Inner
    @PostMapping("/getUserDamaValue")
    public R getUserDamaValue(@Valid @RequestBody DaMaValueDto daMaValueDto) {
        R result = new R<>(accountinfoService.getDamaValueList(daMaValueDto.getChannelId(), daMaValueDto.getUserId()));
        return result;
    }

    /**
     * 分页查询
     *
     * @param page         分页对象
     * @param accountinfo
     * @return
     */
    @GetMapping("/page")
    public R getTAccountinfoPage(Page page, Accountinfo accountinfo) {
        R result = new R<>(accountinfoService.queryUserInfoPage(page, accountinfo));
        return result;
    }


    /**
     * 通过id查询
     *
     * @param accountid
     * @return R
     */
    @GetMapping("/{tAccountid}")
    public R getById(@PathVariable("tAccountid") Integer accountid) {
        Accountinfo accountinfo = accountinfoService.getById(accountid);
        UserCashInfoVo vo = new UserCashInfoVo();
        if (accountinfo != null && StrUtil.isNotBlank(accountinfo.getTBankinfo()) &&  JSONUtil.isJson(accountinfo.getTBankinfo())) {

            GsonCashBank gsonCashBank = new Gson().fromJson(accountinfo.getTBankinfo(), GsonCashBank.class);
            vo.setBankPayId(gsonCashBank.getPayId());
            vo.setBank(gsonCashBank.getBank());

            vo.setSubBank(gsonCashBank.getSubBank());
            vo.setBankRealName(gsonCashBank.getRealName());
        }

        if (accountinfo != null && StrUtil.isNotBlank(accountinfo.getTBankinfo()) && JSONUtil.isJson(accountinfo.getTAlipayinfo())) {
            GsonCashAli ali = new Gson().fromJson(accountinfo.getTAlipayinfo(), GsonCashAli.class);

            vo.setAliPayId(ali.getPayId());
            vo.setAliRealName(ali.getRealName());
        }
        return new R<>(vo);
    }

    /**
     * 新增
     *
     * @param accountinfo
     * @return R
     */
    @SysLog("新增")
    @PostMapping
    @PreAuthorize("@pms.hasPermission('cac_useaccount_add')")
    public R save(@RequestBody Accountinfo accountinfo) {
        return new R<>(accountinfoService.save(accountinfo));
    }

    /**
     * 修改
     *
     * @param accountinfo
     * @return R
     */
    @SysLog("修改")
    @PutMapping
    @PreAuthorize("@pms.hasPermission('cac_useaccount_edit')")
    public R updateById(@RequestBody Accountinfo accountinfo) {
        return new R<>(accountinfoService.updateById(accountinfo));
    }

    /**
     * 通过id删除
     *
     * @param tAccountid id
     * @return R
     */
    @SysLog("删除")
    @DeleteMapping("/{tAccountid}")
    @PreAuthorize("@pms.hasPermission('cac_useaccount_del')")
    public R removeById(@PathVariable Integer tAccountid) {
        accountinfoService.removeById(tAccountid);
        userbaseattrService.removeById(tAccountid);
        return new R<>(Boolean.TRUE);
    }

    /**
     * 通过id查询账户信息
     *
     * @param accountid
     * @return R
     */
    @GetMapping("/getUserInfo/{accountid}")
    public R getUserInfoById(@PathVariable("accountid") Integer accountid) {
        Accountinfo accountinfo = accountinfoService.getById(accountid);
        Userbaseattr userbaseattr = userbaseattrService.getById(accountid);

        UserInfoVo userInfo = UserInfoVo.installBean(accountinfo, userbaseattr);
        return new R<>(userInfo);
    }

    /**
     * <B>冻结/解冻</B>
     *
     * @param userId
     * @return
     */
    @SysLog("修改用户状态")
    @GetMapping("/activate")
    public R activate(String userId, String state) {
        GsonResult result = accountinfoService.activateUser(userId, state);
        if (result.getRet() == 0) {
            return  new R<>(result.getRet());
        } else if (result.getRet() == -3) {
            return  new R<>().setCode(result.getRet()).setMsg("修改状态失败,用户不在线!");
        } else {
            return  new R<>().setCode(result.getRet()).setMsg("修改状态失败!");
        }
    }

    @SysLog("修改用户密码")
    @GetMapping("/modifyPw")
    public R modifyPw(String userId, String password) {
        Boolean result = accountinfoService.modifyPw(userId, password);
        return  result ? new R<>() :  new R<>().setCode(1).setMsg("修改用户密码失败!");
    }

    @SysLog("重置用户密码")
    @GetMapping("/resetPw")
    public R resetPw(String userId) {
        Boolean result = accountinfoService.resetPw(userId);
        return  result ? new R<>() :  new R<>().setCode(1).setMsg("重置用户密码失败!");
    }

    @SysLog("查看玩家是否在游戏")
    @GetMapping("/isOnline/{accountid}")
    public R isInGame(@PathVariable("accountid") Integer accountid) {
        Map map = accountinfoService.isInGame(accountid);
        return new R<>(map);
    }


    @SysLog("修改银行密码")
    @GetMapping("/modifyBankPw")
    public R modifyBankPw(String userId, String password) {
        return new R<>(accountinfoService.modifyBankPw(userId, password));
    }

    @SysLog("解除手机号码绑定")
    @GetMapping("/unbindPhoneNum/{accountid}")
    public R unbindPhoneNum(@PathVariable("accountid") Integer accountid) {
        GsonResult result = accountinfoService.unbindPhoneNum(accountid);
        if (result.getRet() == 0) {
            return new R<>();
        } else if (result.getRet() == -1) {
            return new R<>().setCode(1).setMsg("解除手机号码绑定失败!");
        } else {
            return new R<>().setCode(2).setMsg("解除手机号码绑定失败!");
        }
    }

    @SysLog("清空设备信息")
    @GetMapping("/clearCommonDev/{accountid}")
    public R clearCommonDev(@PathVariable("accountid") Integer accountid) {
        GsonResult result = accountinfoService.clearCommonDev(accountid);
        if (result.getRet() == 0) {
            return new R<>();
        } else if (result.getRet() == -1) {
            return new R<>().setCode(1).setMsg("清空设备信息失败!");
        } else {
            return new R<>().setCode(2).setMsg("清空设备信息失败!");
        }
    }

    @SysLog("踢用户下线")
    @GetMapping("/kickUser/{accountid}")
    public R kickUer(@PathVariable("accountid") Integer accountid) {
        GsonResult result = accountinfoService.kickUser(accountid);
        if (result.getRet() == 0) {
            return new R<>();
        } else if (result.getRet() == -1) {
            return new R<>().setCode(1).setMsg("当前用户不在线!");
        } else {
            return new R<>().setCode(2).setMsg("踢人失败!");
        }
    }
}
