package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.BgDamaGameDto;
import com.youwan.game.cac.api.dto.BgExitGameDto;
import com.youwan.game.cac.api.dto.BgUserDto;
import com.youwan.game.cac.api.dto.ChannelBgUserStatusDto;
import com.youwan.game.cac.api.entity.OtherBgUser;
import com.youwan.game.common.core.otherplatform.PlatformConfig;
import com.youwan.game.common.core.otherplatform.bg.BGPlatfromConfig;
import com.youwan.game.common.core.otherplatform.bg.json.resp.UserDetailsResp;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-05 15:57:07
 */
public interface OtherBgUserService extends IService<OtherBgUser> {

    OtherBgUser findUser(String channelId, String userId);

    OtherBgUser createUser(String channelId, String userId, String nickName);

    IPage<List<OtherBgUser>> queryBgUserPage(Page page, BgUserDto bgUserDto);

    String openGame(Integer isWx, Integer isMobileUrl, Integer isHttpsUrl, String channlId, String userId, String sysBalance, String password);

//    String openGame(Integer isMobileUrl, Integer isHttpsUrl, String channlId, String userId, String sysBalance, String password);

    BgDamaGameDto exitGame(BgExitGameDto gExitGameDto);

    Map<String, BigDecimal> getUserBalance(String userId);

    BigDecimal getSysBalance(String userId);

    Integer transferbalance(String userId, String amount, Integer type, String channelId, Integer reason);

    BGPlatfromConfig getBgConfig();

    UserDetailsResp getUserDetails(String userId);

    Boolean changeStatus(ChannelBgUserStatusDto channelBgUserStatusDto);

}
