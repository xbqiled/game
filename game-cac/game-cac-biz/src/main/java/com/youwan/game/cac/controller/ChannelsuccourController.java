package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.OpenTypeForm;
import com.youwan.game.cac.api.entity.SuccourForm;
import com.youwan.game.cac.api.vo.SuccourVo;
import com.youwan.game.cac.api.vo.TaskVo;
import com.youwan.game.cac.service.ChannelsuccourService;
import com.youwan.game.common.core.jsonmodel.GsonSuccour;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-31 12:23:45
 */
@RestController
@AllArgsConstructor
@RequestMapping("/channelsuccour")
public class ChannelsuccourController {

    private ChannelsuccourService channelsuccourService;

    /**
     * 列表
     */
    @RequestMapping("/page")
    public R page(Page page, QueryDto queryDto){
        IPage<SuccourVo> dataList = channelsuccourService.queryPage(page, queryDto);
        return new R<>(dataList);
    }

    /**
     * 信息g
     */
    @RequestMapping("/info/{channelId}")
    @PreAuthorize("@pms.hasPermission('cac_channelsuccour_info')")
    public R info(@PathVariable("channelId") String channelId){
        GsonSuccour gsonSuccour = channelsuccourService.findByChannelId(channelId);
        return new R<>(gsonSuccour);
    }

    @SysLog("救济金修改状态")
    @RequestMapping("/channelOpenType")
    @PreAuthorize("@pms.hasPermission('cac_channelsuccour_opentype')")
    public R channelOpenType(@RequestBody OpenTypeForm openTypeForm){
        int result = channelsuccourService.channelOpenType(openTypeForm.getChannelId(), openTypeForm.getOpenType());
        if (result == 0) {
            return new R<>();
        } else {
            return new R<>(null,"修改救济金配置状态失败!");
        }
    }

    @SysLog("救济金配置信息保存")
    @RequestMapping("/doConfig")
    @PreAuthorize("@pms.hasPermission('cac_channelsuccour_config')")
    public R doConfig(@RequestBody SuccourForm succourForm){
        int result = channelsuccourService.saveOrUpdateConfig(succourForm);
        if (result == 0) {
            return new R<>();
        } else {
            return new R<>(null,"配置救济金信息失败!");
        }
    }
}
