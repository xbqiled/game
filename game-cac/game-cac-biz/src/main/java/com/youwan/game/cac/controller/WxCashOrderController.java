package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.WxCashOrder;
import com.youwan.game.cac.service.WxCashOrderService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-11-06 13:34:56
 */
@RestController
@AllArgsConstructor
@RequestMapping("/wxcashorder")
public class WxCashOrderController {

  private final WxCashOrderService wxCashOrderService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param wxCashOrder 
   * @return
   */
  @GetMapping("/page")
  public R getWxCashOrderPage(Page page, WxCashOrder wxCashOrder) {
    return  new R<>(wxCashOrderService.page(page, Wrappers.query(wxCashOrder)));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(wxCashOrderService.getById(id));
  }

  /**
   * 新增
   * @param wxCashOrder 
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody WxCashOrder wxCashOrder){
    return new R<>(wxCashOrderService.save(wxCashOrder));
  }

  /**
   * 修改
   * @param wxCashOrder 
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody WxCashOrder wxCashOrder){
    return new R<>(wxCashOrderService.updateById(wxCashOrder));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(wxCashOrderService.removeById(id));
  }

}
