package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.DownloadConfigDto;
import com.youwan.game.cac.api.entity.CfgChlDownload;
import com.youwan.game.cac.api.vo.DownloadPanelConfigVo;
import com.youwan.game.cac.service.CfgChlDownloadService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.common.security.annotation.Inner;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-11-18 16:08:14
 */
@RestController
@AllArgsConstructor
@RequestMapping("/downloadpanelcfg")
public class CfgChlDownloadController {

  private final CfgChlDownloadService cfgChlDownloadService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param
   * @return
   */
  @GetMapping("/page")
  public R getConfigChannelDownloadPage(Page page, DownloadConfigDto downloadConfigDto) {
    return  new R<>(cfgChlDownloadService.queryDownloadCfgPage(page, downloadConfigDto));
  }


  /**
   * 通过id查询
   * @param channelId
   * @return R
   */
  @GetMapping("/getConfig/{channelId}")
  public R getById(@PathVariable("channelId") String channelId){
    return new R<>(cfgChlDownloadService.getConfigByChannelId(channelId));
  }

  /**
   * 新增
   * @param cfgChlDownload
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody CfgChlDownload cfgChlDownload){
    Boolean result = cfgChlDownloadService.saveDownloadPanelCfg(cfgChlDownload);
    if (result) {
      return new R<>();
    } else {
      return new R<>().setCode(1).setMsg("渠道配置重复");
    }
  }

  /**
   * 修改
   * @param cfgChlDownload
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody CfgChlDownload cfgChlDownload){
    return new R<>(cfgChlDownloadService.modifyDownloadPanelCfg(cfgChlDownload));
  }


  /**
   *
   * @param
   * @return
   */
  @Inner
  @PostMapping("/getconfig")
  public R getDownloadPanelConfig(@RequestBody DownloadConfigDto downloadConfigDto){
    return new R<>(cfgChlDownloadService.getConfigByChannelId(downloadConfigDto.getChannelId()));
  }

}
