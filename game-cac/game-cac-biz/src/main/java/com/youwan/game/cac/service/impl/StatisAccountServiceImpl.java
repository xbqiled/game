package com.youwan.game.cac.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.StatisAccount;
import com.youwan.game.cac.mapper.StatisAccountMapper;
import com.youwan.game.cac.service.StatisAccountService;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 
 *
 * @author code generator
 * @date 2019-06-16 12:32:18
 */
@Service("statisAccountService")
public class StatisAccountServiceImpl extends ServiceImpl<StatisAccountMapper, StatisAccount> implements StatisAccountService {

    private static String TABLE_NAME = "statis_account";
    private static String TIME_TABLE_NAME = "statis_date";

    @Override
    public Map<String, Object> queryStatisDataPage(Page page, QueryDto dto) {
        StringBuffer tableSql = new StringBuffer();

        //获取当前时间
        List<String> list = getIntervalMonth(dto.getStartTime(), dto.getEndTime());
        String sql = spliceInSql(list);
        List<String> tableList = baseMapper.queryHaveTab(sql);

        //组成查询SQL
        tableSql.append(spliceQuerySql(tableList));
        IPage<List<Map<String, Object>>> resultList = baseMapper.queryStatisAccountDataPage(page, tableSql.toString(), dto.getChannelNo(), dto.getUserId(), dto.getStartTime(), dto.getEndTime());

        Map<String, Object>  statis =  baseMapper.queryStatiAccountData(tableSql.toString(), dto.getChannelNo(), dto.getUserId(), dto.getStartTime(), dto.getEndTime());
        Map<String, Object> map = new HashMap<>();
        map.put("page", resultList);
        map.put("statis", statis);
        return  map;
    }


    @Override
    public IPage<List<StatisAccount>> queryStatisAccountPage(Page page, QueryDto dto) {
        Date data = DateUtil.parse(dto.getStartTime(), "yyyy-MM-dd");
        String monthTable = DateUtil.format(data, "yyyyMM");
        return baseMapper.queryStatisAccountPage(page, TABLE_NAME + monthTable, dto.getChannelId(), dto.getUserId(), dto.getStartTime(), dto.getEndTime());
    }

    /**
     * <B>获取间隔时间</B>
     * @param startTime
     * @param endTime
     * @return
     */
    private List<String> getIntervalMonth(String startTime, String endTime) {
        List<String> list = new ArrayList<>();
        //转化成月份
        Date startMonthDate = DateUtil.parse(startTime, "yyyyMMdd");
        Date endMonthDate = DateUtil.parse(endTime, "yyyyMMdd");

        String endMonth = DateUtil.format(endMonthDate, "yyyyMM");
        Date nextDate = startMonthDate;

        //判断时间不等于结束时间
        while (!DateUtil.format(nextDate, "yyyyMM").equals(endMonth)) {
            list.add(TIME_TABLE_NAME + DateUtil.format(nextDate, "yyyyMM"));
            //获取下一个月
            nextDate = DateUtil.offsetMonth(nextDate, 1);
        }

        list.add(TIME_TABLE_NAME + endMonth);
        return list;
    }


    /**
     * <B>拼接SQL语句</B>
     * @param list
     * @return
     */
    private String spliceQuerySql(List<String> list) {
        StringBuffer sb = new StringBuffer();
        sb.append("(");
        if (CollUtil.isNotEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                sb.append(" select * From " + list.get(i));
                if (i < list.size() - 1) {
                    sb.append("  union all ");
                }
            }
        }
        sb.append(")");
        return sb.toString();
    }


    private String spliceInSql(List<String> list) {
        StringBuffer sb = new StringBuffer();
        sb.append("(");
        if (CollUtil.isNotEmpty(list)) {
            for (int i = 0; i < list.size(); i++) {
                sb.append("'" + list.get(i) + "'");
                if (i < list.size() - 1) {
                    sb.append(" , ");
                }
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
