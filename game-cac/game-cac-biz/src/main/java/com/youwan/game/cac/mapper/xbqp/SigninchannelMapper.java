package com.youwan.game.cac.mapper.xbqp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.Signinchannel;
import com.youwan.game.cac.api.vo.DepositchannelVo;
import com.youwan.game.cac.api.vo.SigninchannelVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-05 13:49:00
 */
public interface SigninchannelMapper extends BaseMapper<Signinchannel> {

    IPage<List<SigninchannelVo>>  querySignCfgPage(Page page, @Param("channelId") String channelId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    Signinchannel getSignByChannelId(@Param("channelId") String channelId);

}
