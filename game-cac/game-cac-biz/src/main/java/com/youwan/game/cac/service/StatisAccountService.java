package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.StatisAccount;
import com.youwan.game.cac.api.vo.StatisGameVo;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2019-06-16 12:32:18
 */
public interface StatisAccountService extends IService<StatisAccount> {

    Map<String, Object> queryStatisDataPage(Page page, QueryDto dto);

    IPage<List<StatisAccount>> queryStatisAccountPage(Page page, QueryDto dto);

}
