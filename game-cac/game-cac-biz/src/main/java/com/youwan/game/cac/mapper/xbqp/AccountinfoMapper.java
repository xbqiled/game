package com.youwan.game.cac.mapper.xbqp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.Accountinfo;
import com.youwan.game.cac.api.vo.DaMaValue;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-27 15:11:02
 */
public interface AccountinfoMapper extends BaseMapper<Accountinfo> {

    void activateUser(@Param("userId")String userId, @Param("state")String state);

    List<String> queryHaveTab(@Param("tableSql") String tableSql);

    List<DaMaValue> queryUserDamaValue(@Param("channelId") String channelId, @Param("list")List<Integer> list);

    Map queryUserInfo(@Param("userId") Integer userId);

    Accountinfo getCashUserInfo(@Param("userId")Integer userId);
    Accountinfo getUserInfo(@Param("userId")Integer userId);

    Accountinfo getUserInfoById(@Param("userId") Integer userId);

    Accountinfo getChannelUserInfoById(@Param("tableName") String tableName, @Param("channelId") String channelId, @Param("userId") String userId);

    void modifyPw(@Param("userId") String userId, @Param("password") String password);

    void modifyBankPw(@Param("userId")String userId, @Param("password")String password);

    IPage<Map<String, Object>> queryUserInfo(Page page, @Param("userId") Integer userId, @Param("userName") String userName,
                                             @Param("nickName") String nickName, @Param("channelId") String channelId);

    Map queryGameRoom(@Param("gameRoomId")String gameRoomId);
}
