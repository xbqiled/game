package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.OtherBgRedPacketExchange;


/**
 * 
 *
 * @author code generator
 * @date 2020-01-13 20:43:08
 */
public interface OtherBgRedPacketExchangeService extends IService<OtherBgRedPacketExchange> {

}
