package com.youwan.game.cac.controller;

import com.youwan.game.cac.api.dto.BettingRecordQueryDto;
import com.youwan.game.cac.service.BettingRebateService;
import com.youwan.game.common.core.betting.ScrubResult;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.security.annotation.Inner;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-10.
 */
@RestController
@RequestMapping("/bettingRebate")
public class BettingRebateController {

    private BettingRebateService bettingRebateService;

    public BettingRebateController(BettingRebateService bettingRebateService) {
        this.bettingRebateService = bettingRebateService;
    }

    @GetMapping("/scrub/{userId}/details")
    @Inner
    public R<List<ScrubResult>> getScrubRebateDetails(@PathVariable("userId") Long userId) {
        return new R<>(bettingRebateService.getScrubRebateDetails(userId));
    }

    @GetMapping("/scrub/{userId}/history")
    @Inner
    public R<?> getScrubRebateHistory(@PathVariable("userId") Long userId,
                                      @RequestParam(value = "offset", required = false, defaultValue = "0") Integer offset,
                                      @RequestParam(value = "limit", required = false, defaultValue = "10") Integer limit) {
        return new R<>(bettingRebateService.queryPersonScrubRebateHistory(userId, offset, limit));
    }

    @PostMapping("/scrub/{userId}/_take")
    @Inner
    public R<?> takeScrubRebate(@PathVariable("userId") Long userId) {
        boolean ret = bettingRebateService.takeScrubRebate(userId);
        return ret ? new R<>() : new R<>().setCode(1).setMsg("领取失败");
    }

    @PostMapping("/scrub/{userId}/takingRecords")
    @Inner
    public R<?> getPersonTakingRecords(@PathVariable("userId") Integer userId, BettingRecordQueryDto dto) {
        dto.setUserId(userId);
        return new R(bettingRebateService.getPersonTakingRecords(dto));
    }
}
