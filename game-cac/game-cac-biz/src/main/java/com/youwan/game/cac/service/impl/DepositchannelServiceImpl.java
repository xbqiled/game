package com.youwan.game.cac.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.Depositchannel;
import com.youwan.game.cac.api.vo.DepositchannelVo;
import com.youwan.game.cac.mapper.xbqp.DepositchannelMapper;
import com.youwan.game.cac.service.DepositchannelService;
import com.youwan.game.common.core.jsonmodel.GsonRechargeRebateCfg;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.HttpHandler;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-05 14:15:52
 */
@Service("depositchannelService")
public class DepositchannelServiceImpl extends ServiceImpl<DepositchannelMapper, Depositchannel> implements DepositchannelService {


    @Override
    public Depositchannel findByChannelId(String channelId) {
        return baseMapper.getDepositByChannelId(channelId);
    }

    @Override
    public IPage<DepositchannelVo> queryDepositPage(Page page, QueryDto queryDto) {
        return baseMapper.queryDepositPage(page, queryDto.getChannelNo(), queryDto.getStartTime(), queryDto.getEndTime());
    }

    @Override
    public int channelOpenType(String channelId, String openType) {
        Depositchannel model  = baseMapper.getDepositByChannelId(channelId);
        if (model != null && StrUtil.isNotEmpty(model.getTChannel())) {

            Integer type = model.getTOpentype() == 1 ? 0 : 1;
            String str = new String(model.getTCfginfo());
            List<GsonRechargeRebateCfg> rabateCfg = new ArrayList<>();

            if (StrUtil.isNotBlank(str)) {
                GsonRechargeRebateCfg[] array = new Gson().fromJson(str, GsonRechargeRebateCfg[].class);
                rabateCfg = Arrays.asList(array);
            }

            GsonResult result = HttpHandler.rechargeRebate(channelId, model.getTStarttime(), model.getTEndtime(), type, rabateCfg);
            if (result != null) {
                return result.getRet();
            }
        }
        return 1;
    }

    @Override
    public int saveRechargeRebateCfg(String channel, String beginTime, String endTime, Integer openType, List<GsonRechargeRebateCfg> rabateCfg) {
        //时间转换
        Long start = new Long(beginTime) / 1000;
        Long finish = new Long(endTime) / 1000;

        GsonResult result = HttpHandler.rechargeRebate(channel, start, finish, openType, rabateCfg);
        if (result != null) {
            return result.getRet();
        }

        return 1;
    }
}
