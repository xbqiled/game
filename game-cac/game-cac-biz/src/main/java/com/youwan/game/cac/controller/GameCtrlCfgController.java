package com.youwan.game.cac.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.GameCtrlCfgDto;
import com.youwan.game.cac.api.dto.GameFqzsCtrlCfg;
import com.youwan.game.cac.service.GamectrlcfgService;
import com.youwan.game.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * VIEW
 *
 * @author code generator
 * @date 2020-03-27 20:25:55
 */
@RestController
@AllArgsConstructor
@RequestMapping("/gamectrlcfg")
public class GameCtrlCfgController {

  private final GamectrlcfgService gamectrlcfgService;

  /**
   * 分页查询
   * @param page 分页对象
   * @return
   */
  @GetMapping("/page")
  public R getGameCtrlCfgPage(Page page, GameCtrlCfgDto gameCtrlCfgDto) {
    return  new R<>(gamectrlcfgService.queryGameCtrlCfgPage(page, gameCtrlCfgDto.getStartTime(), gameCtrlCfgDto.getEndTime()));
  }


  @RequestMapping("/info")
  public R info(@RequestParam Map<String, Object> params) {
    String gameId = ObjectUtil.isNotNull(params.get("gameId")) ? (String) params.get("gameId") : "";
    return  new R<>(gamectrlcfgService.getGameCtrlCfg(gameId));
  }


  @PostMapping("/doFqzsCfg")
  public R doFqzsCfg(@RequestBody GameFqzsCtrlCfg gameFqzsCtrlCfg) {
    int result = gamectrlcfgService.doFqzsCfg(gameFqzsCtrlCfg);
    if (result == 0) {
      return new R<>();
    } else {
      return new R<>().setCode(result).setMsg("设置飞禽走兽控制失败!");
    }
  }

}
