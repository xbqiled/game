package com.youwan.game.cac.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.WxhhExpDto;
import com.youwan.game.cac.api.entity.Gameglobalextinfo;
import com.youwan.game.cac.api.vo.GameglobalextinfoVo;
import com.youwan.game.cac.mapper.xbqp.GameglobalextinfoMapper;
import com.youwan.game.cac.service.GameglobalextinfoService;
import com.youwan.game.common.core.constant.ConfigConstant;
import com.youwan.game.common.core.jsonmodel.GsonWxhhExp;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * VIEW
 *
 * @author code generator
 * @date 2020-05-20 16:19:40
 */
@Service("gameglobalextinfoService")
public class GameglobalextinfoServiceImpl extends ServiceImpl<GameglobalextinfoMapper, Gameglobalextinfo> implements GameglobalextinfoService {

    @Override
    public List<GsonWxhhExp> getWxhhExpPage(WxhhExpDto wxhhExpDto) {
        List<GsonWxhhExp> list = new ArrayList<>();
        List<GameglobalextinfoVo> resultList = baseMapper.getWxhhExpList(wxhhExpDto.getStartTime(), wxhhExpDto.getEndTime());

        if (resultList != null && resultList.size() > 0) {
            for (GameglobalextinfoVo gameglobalextinfoVo : resultList) {

                if(gameglobalextinfoVo != null && gameglobalextinfoVo.getExtinfo() != null  && gameglobalextinfoVo.getExtinfo().length > 0) {
                    String infoStr = new String (gameglobalextinfoVo.getExtinfo());

                    GsonWxhhExp gsonWxhhExp = new Gson().fromJson(infoStr, GsonWxhhExp.class);
                    gsonWxhhExp.setGameId(gameglobalextinfoVo.getGameatomtypeid());
                    gsonWxhhExp.setGameKindName(gameglobalextinfoVo.getGameKindName());

                    gsonWxhhExp.setPhoneGameName(gameglobalextinfoVo.getPhoneGameName());
                    if (gameglobalextinfoVo.getUpdatetime() != null) {
                        gsonWxhhExp.setUpdateTime(DateUtil.format(DateUtil.date(gameglobalextinfoVo.getUpdatetime() * 1000), ConfigConstant.FULL_DATE_TIME_FORMAT));
                    }
                    list.add(gsonWxhhExp);
                }
            }
        }

        return list;
    }

}
