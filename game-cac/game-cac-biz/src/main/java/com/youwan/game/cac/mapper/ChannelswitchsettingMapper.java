package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.TChannelswitchsetting;
import org.apache.ibatis.annotations.Param;


/**
 * 
 *
 * @author code generator
 * @date 2019-02-08 18:43:43
 */
public interface ChannelswitchsettingMapper extends BaseMapper<TChannelswitchsetting> {


    void deleteByChannelId(@Param("channelId")String channelId);


    void saveSetting(@Param("tChannelkey")String tChannelkey, @Param("tNormalsetting")String tNormalsetting, @Param("tAuditsetting")String tAuditsetting);


    TChannelswitchsetting queryByChannelId(@Param("channelId")String channelId);

}
