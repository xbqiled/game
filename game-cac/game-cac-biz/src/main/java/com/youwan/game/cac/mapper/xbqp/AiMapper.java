package com.youwan.game.cac.mapper.xbqp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.Ai;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author code generator
 * @date 2019-01-21 19:13:24
 */
public interface AiMapper extends BaseMapper<Ai> {

    IPage<Map<String, Object>> queryBotPage(Page page, @Param("tnick") String tNick, @Param("tid") String tId);

}
