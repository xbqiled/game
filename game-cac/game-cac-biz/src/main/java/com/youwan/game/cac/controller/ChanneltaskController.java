package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.OpenTypeForm;
import com.youwan.game.cac.api.entity.TaskForm;
import com.youwan.game.cac.api.vo.DayShareVo;
import com.youwan.game.cac.api.vo.TaskVo;
import com.youwan.game.cac.service.ChanneltaskService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-31 12:30:32
 */
@RestController
@AllArgsConstructor
@RequestMapping("/channeltask")
public class ChanneltaskController {

    private ChanneltaskService channeltaskService;

    /**
     * 列表
     */
    @RequestMapping("/page")
    public R page(Page page, QueryDto queryDto){
        IPage<TaskVo> dataList = channeltaskService.queryPage(page, queryDto);
        return new R<>(dataList);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{channelId}")
    @PreAuthorize("@pms.hasPermission('cac_channeltask_info')")
    public R info(@PathVariable("channelId") String channelId){
        Map<String, Object> taskMap = channeltaskService.findByChannelId(channelId);
        return new R<>(taskMap);
    }


    @SysLog("任务修改状态")
    @RequestMapping("/channelOpenType")
    @PreAuthorize("@pms.hasPermission('cac_channeltask_opentype')")
    public R channelOpenType(@RequestBody OpenTypeForm openTypeForm){
        int result = channeltaskService.channelOpenType(openTypeForm.getChannelId(), openTypeForm.getOpenType());
        if (result == 0) {
            return new R<>();
        } else {
            return new R<>(null,"修改任务配置状态失败!");
        }
    }

    @SysLog("任务配置信息保存")
    @RequestMapping("/doConfig")
    @PreAuthorize("@pms.hasPermission('cac_channeltask_config')")
    public R doConfig(@RequestBody TaskForm taskForm){
        int result = channeltaskService.saveOrUpdateConfig(taskForm);
        if (result == 0) {
            return new R<>();
        } else {
            return new R<>(null,"配置任务信息失败!");
        }
    }
}
