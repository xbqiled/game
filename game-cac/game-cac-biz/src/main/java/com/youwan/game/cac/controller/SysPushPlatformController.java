package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.PushPlatformDto;
import com.youwan.game.cac.api.entity.SysPushPlatform;
import com.youwan.game.cac.service.SysPushPlatformService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;



/**
 * 
 *
 * @author code generator
 * @date 2020-05-23 15:14:24
 */
@RestController
@AllArgsConstructor
@RequestMapping("/pushplatform")
public class SysPushPlatformController {

  private final SysPushPlatformService sysPushPlatformService;

  @GetMapping("/page")
  public R getSysPushPlatformPage(Page page, PushPlatformDto pushPlatformDto) {
    return  new R<>(sysPushPlatformService.queryPushPlatformPage(page, pushPlatformDto.getPlatformName()));
  }

  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(sysPushPlatformService.getById(id));
  }


  @GetMapping("/getPushPlatformData")
  public R getPushPlatformData(){
    return new R<>(sysPushPlatformService.getPushPlatformList());
  }

  /**
   * 新增
   * @param sysPushPlatform 
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody SysPushPlatform sysPushPlatform){
    return new R<>(sysPushPlatformService.addPushPlatform(sysPushPlatform));
  }

  /**
   * 修改
   * @param sysPushPlatform 
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody SysPushPlatform sysPushPlatform){
    return new R<>(sysPushPlatformService.updatePushPlatform(sysPushPlatform));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(sysPushPlatformService.delPushPlatform(id));
  }

}
