package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.ChlLog;
import com.youwan.game.cac.api.entity.DeviceInfo;

import java.util.List;


/**
 * 系统日志
 *
 * @author code generator
 * @date 2019-05-23 13:09:56
 */
public interface ChlLogService extends IService<ChlLog> {

    IPage<List<ChlLog>>  queryLogPage(Page page, QueryDto queryDto);

}
