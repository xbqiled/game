package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.ShortUrl;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2019-04-19 13:17:59
 */
public interface ShortUrlMapper extends BaseMapper<ShortUrl> {
    IPage<List<ShortUrl>> queryShortUrlPage(Page page, @Param("name") String name);

    List<ShortUrl> getShortUrlList();
}
