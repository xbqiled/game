package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.Mailtemplate;


/**
 * 
 *
 * @author code generator
 * @date 2019-03-19 13:02:27
 */
public interface MailtemplateService extends IService<Mailtemplate> {

}
