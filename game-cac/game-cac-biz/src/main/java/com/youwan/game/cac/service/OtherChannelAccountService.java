package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.ChannelAccountDto;
import com.youwan.game.cac.api.entity.ChannelAccount;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-19 14:07:23
 */
public interface OtherChannelAccountService extends IService<ChannelAccount> {

    IPage<List<ChannelAccount>> getChannelAccountPage (Page page, ChannelAccountDto channelBalanceDto);

    Integer updateChannelAccount(ChannelAccountDto channelAccountDto);

}
