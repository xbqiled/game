package com.youwan.game.cac.mapper.xbqp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.ChanneltaskEntity;
import com.youwan.game.cac.api.vo.BindUrlVo;
import com.youwan.game.cac.api.vo.TaskVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-31 12:30:32
 */
public interface ChanneltaskMapper extends BaseMapper<ChanneltaskEntity> {

    IPage<TaskVo> queryPage(Page page, @Param("channelId") String channelId);

    ChanneltaskEntity getTaskByChannelId(@Param("channelId") String channelId);

    List<Map<String, Object>> getTaskCfg(@Param("taskId") String taskId);

}
