package com.youwan.game.cac.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.WxhhCtrlCfgDto;
import com.youwan.game.cac.api.entity.GameCtrlCfgStr;
import com.youwan.game.cac.mapper.xbqp.GameCtrlCfgStrMapper;
import com.youwan.game.cac.service.GameCtrlCfgStrService;
import com.youwan.game.common.core.jsonmodel.*;
import com.youwan.game.common.core.util.HttpHandler;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2020-04-14 19:28:20
 */
@Service("gameCtrlCfgStrService")
public class GameCtrlCfgStrServiceImpl extends ServiceImpl<GameCtrlCfgStrMapper, GameCtrlCfgStr> implements GameCtrlCfgStrService {

    private static final Map<String, String> CFG_TYPES = new HashMap<>();

    /**
     * case when a.t_key = 'dantiaoSingleProtect' THEN '单人保护机制'
     *         when a.t_key = 'dantiaoSingleDrawLots' THEN '单人抽签机制'
     *         when a.t_key = 'dantiaoSingleMaxLose' THEN '单人奖金池最高可输'
     *         when a.t_key = 'dantiaoSingleMaxWin' THEN '单人奖金池最高可赢'
     *         when a.t_key = 'dantiaoMultiplyMaxLose' THEN '多人奖金池最高可输'
     *         when a.t_key = 'dantiaoMultiplyMaxWin' THEN '多人奖金池最高可赢'
     *         when a.t_key = 'dantiaoMultiplyBetPeopleCnt' THEN '多人人数基数'
     *         when a.t_key = 'dantiaoMultiplyKillInning' THEN '吃分局数基数'
     *         when a.t_key = 'dantiaoNewDataBetLimit' THEN '房间花色基数' END cfgType
     */
    static {
        CFG_TYPES.put("dantiaoSingleProtect", "单人保护机制");
        CFG_TYPES.put("dantiaoSingleDrawLots", "单人抽签机制");
        CFG_TYPES.put("dantiaoSingleMaxLose", "单人奖金池最高可输");
        CFG_TYPES.put("dantiaoSingleMaxWin", "单人奖金池最高可赢");
        CFG_TYPES.put("dantiaoMultiplyMaxLose", "多人奖金池最高可输");
        CFG_TYPES.put("dantiaoMultiplyMaxWin", "多人奖金池最高可赢");
        CFG_TYPES.put("dantiaoMultiplyBetPeopleCnt", "多人人数基数");
        CFG_TYPES.put("dantiaoMultiplyKillInning", "吃分局数基数");
        CFG_TYPES.put("dantiaoNewDataBetLimit", "房间花色基数");
    }

    @Override
    public IPage<Map<String, Object>> queryGameCtrlCfgStrPage(Page page, String startTime, String endTime) {
        IPage<Map<String, Object>> ret = baseMapper.queryGameCtrlCfgStrPage(page, startTime, endTime);
        ret.getRecords().forEach(item -> item.put("cfgType", CFG_TYPES.get(item.get("keyId"))));
        return ret;
    }

    public List<Map<String, Object>> getGameList() {
        return baseMapper.getGameList();
    }

    @Override
    public int doCfg(WxhhCtrlCfgDto wxhhCtrlCfgDto) {
        List<GsonWxDanrbh> danrbhList = new ArrayList<>();
        List<GsonWxDanrcq> danrcqList = new ArrayList<>();
        List<GsonWxDanrks> danrksList = new ArrayList<>();

        List<GsonWxDanrky> danrkyList = new ArrayList<>();
        List<GsonWxDuorks> duorksList = new ArrayList<>();
        List<GsonWxDuorky> duorkyList = new ArrayList<>();
        List<GsonWxDuorjs> duorjsList = new ArrayList<>();
        List<GsonWxFjhs> fjhsList = new ArrayList<>();

        if (wxhhCtrlCfgDto.getCtrlArr() != null && wxhhCtrlCfgDto.getCtrlArr().size() > 0) {
            for (Map<String, Object> jsonMap : wxhhCtrlCfgDto.getCtrlArr()) {
                GsonWxDanrbh gsonWxDanrbh = new GsonWxDanrbh();
                GsonWxDanrcq gsonWxDanrcq = new GsonWxDanrcq();
                GsonWxDanrks gsonWxDanrks = new GsonWxDanrks();

                GsonWxDanrky gsonWxDanrky = new GsonWxDanrky();
                GsonWxDuorks gsonWxDuorks = new GsonWxDuorks();
                GsonWxDuorky gsonWxDuorky = new GsonWxDuorky();

                GsonWxDuorjs gsonWxDuorjs = new GsonWxDuorjs();
                GsonWxFjhs gsonWxFjhs = new GsonWxFjhs();

                for (Map.Entry<String, Object> entry : jsonMap.entrySet()) {
                    if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoSingleProtect")) {

                        if (entry.getKey().equals("drbh_maxLose")) {
                            gsonWxDanrbh.setMaxLose((int) entry.getValue());
                        } else if (entry.getKey().equals("drbh_min")) {
                            gsonWxDanrbh.setMin((int) entry.getValue());
                        } else if (entry.getKey().equals("drbh_max")) {
                            gsonWxDanrbh.setMax((int) entry.getValue());
                        }
                    } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoSingleDrawLots")) {

                        if (entry.getKey().equals("drcq_id")) {
                            gsonWxDanrcq.setId((int) entry.getValue());
                        } else if (entry.getKey().equals("drcq_maxWin")) {
                            gsonWxDanrcq.setMaxWin((int) entry.getValue());
                        } else if (entry.getKey().equals("drcq_chance")) {
                            gsonWxDanrcq.setChance((int) entry.getValue());
                        } else if (entry.getKey().equals("drcq_isProtect")) {
                            if (entry.getValue() instanceof String) {
                                gsonWxDanrcq.setIsProtect(Integer.valueOf((String) entry.getValue()));
                            } else if (entry.getValue() instanceof Integer) {
                                gsonWxDanrcq.setIsProtect((int) entry.getValue());
                            }
                        }
                    } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoSingleMaxLose")) {

                        if (entry.getKey().equals("darks_maxLose")) {
                            gsonWxDanrks.setMaxLose((int) entry.getValue());
                        } else if (entry.getKey().equals("darks_min")) {
                            gsonWxDanrks.setMin((int) entry.getValue());
                        } else if (entry.getKey().equals("darks_max")) {
                            gsonWxDanrks.setMax((int) entry.getValue());
                        }
                    } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoSingleMaxWin")) {

                        if (entry.getKey().equals("darky_maxWin")) {
                            gsonWxDanrky.setMaxWin((int) entry.getValue());
                        } else if (entry.getKey().equals("darky_min")) {
                            gsonWxDanrky.setMin((int) entry.getValue());
                        } else if (entry.getKey().equals("darky_max")) {
                            gsonWxDanrky.setMax((int) entry.getValue());
                        }
                    } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoMultiplyMaxLose")) {

                        if (entry.getKey().equals("dorks_maxLose")) {
                            gsonWxDuorks.setMaxLose((int) entry.getValue());
                        } else if (entry.getKey().equals("dorks_min")) {
                            gsonWxDuorks.setMin((int) entry.getValue());
                        } else if (entry.getKey().equals("dorks_max")) {
                            gsonWxDuorks.setMax((int) entry.getValue());
                        }
                    } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoMultiplyMaxWin")) {

                        if (entry.getKey().equals("dorky_maxWin")) {
                            gsonWxDuorky.setMaxWin((int) entry.getValue());
                        } else if (entry.getKey().equals("dorky_min")) {
                            gsonWxDuorky.setMin((int) entry.getValue());
                        } else if (entry.getKey().equals("dorky_max")) {
                            gsonWxDuorky.setMax((int) entry.getValue());
                        }
                    } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoMultiplyBetPeopleCnt")) {

                        if (entry.getKey().equals("drrs_betPeopleCnt")) {
                            gsonWxDuorjs.setBetPeopleCnt((int) entry.getValue());
                        } else if (entry.getKey().equals("drrs_totalBet")) {
                            gsonWxDuorjs.setTotalBet((int) entry.getValue());
                        }
                    } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoNewDataBetLimit")) {

                        if (entry.getKey().equals("game_id")) {
                            gsonWxFjhs.setGameId((int) entry.getValue());
                        } else if (entry.getKey().equals("four_color_min_bet")) {
                            gsonWxFjhs.setFourColorMinBet((int) entry.getValue());
                        } else if (entry.getKey().equals("four_color_max_bet")) {
                            gsonWxFjhs.setFourColorMaxBet((int) entry.getValue());
                        } else if (entry.getKey().equals("king_max_bet")) {
                            gsonWxFjhs.setKingMaxBet((int) entry.getValue());
                        }
                    }
                }
                if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoSingleProtect")) {
                    danrbhList.add(gsonWxDanrbh);
                } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoSingleDrawLots")) {
                    danrcqList.add(gsonWxDanrcq);
                } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoSingleMaxLose")) {
                    danrksList.add(gsonWxDanrks);
                } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoSingleMaxWin")) {
                    danrkyList.add(gsonWxDanrky);
                } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoMultiplyMaxLose")) {
                    duorksList.add(gsonWxDuorks);
                } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoMultiplyMaxWin")) {
                    duorkyList.add(gsonWxDuorky);
                } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoMultiplyBetPeopleCnt")) {
                    duorjsList.add(gsonWxDuorjs);
                } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoNewDataBetLimit")) {
                    fjhsList.add(gsonWxFjhs);
                }
            }
        }

        if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoSingleProtect")) {
            GsonResult result = HttpHandler.wxDanrbh(danrbhList);
            return result.getRet();
        } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoSingleDrawLots")) {
            GsonResult result = HttpHandler.wxDanrcq(danrcqList);
            return result.getRet();
        } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoSingleMaxLose")) {
            GsonResult result = HttpHandler.wxDanrks(danrksList);
            return result.getRet();
        } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoSingleMaxWin")) {
            GsonResult result = HttpHandler.wxDanrky(danrkyList);
            return result.getRet();
        } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoMultiplyMaxLose")) {
            GsonResult result = HttpHandler.wxDuoks(duorksList);
            return result.getRet();
        } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoMultiplyMaxWin")) {
            GsonResult result = HttpHandler.wxDuoky(duorkyList);
            return result.getRet();
        } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoMultiplyBetPeopleCnt")) {
            GsonResult result = HttpHandler.wxDuorjs(duorjsList);
            return result.getRet();
        } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoMultiplyKillInning")) {
            GsonResult result = HttpHandler.wxDuorgl(wxhhCtrlCfgDto.getKillInning());
            return result.getRet();
        } else if (wxhhCtrlCfgDto.getKeyId().equals("dantiaoNewDataBetLimit")) {
            GsonResult result = HttpHandler.wxFjhs(fjhsList);
            return result.getRet();
        }
        return 1;
    }

    @Override
    public Object getWxhhCtrlCfg(String keyId) {
        List<Map<String, Object>> resultList = new ArrayList<>();
        GameCtrlCfgStr cfg = baseMapper.getByKeyId(keyId);

        if (cfg != null && cfg.getTCfginfo() != null) {
            String info = new String(cfg.getTCfginfo());

            if (keyId.equals("dantiaoMultiplyKillInning")) {
                GsonWxDuorgl gsonWxDuorgl = new Gson().fromJson(info, GsonWxDuorgl.class);
                return gsonWxDuorgl.getKillInning();
            } else {
                List<Map<String, Object>> jsonList = new Gson().fromJson(info, List.class);
                if (StrUtil.isNotEmpty(info) && jsonList != null && jsonList.size() > 0) {
                    for (Map<String, Object> jsonMap : jsonList) {
                        Map<String, Object> resultMap = new HashMap<>();

                        for (Map.Entry<String, Object> entry : jsonMap.entrySet()) {
                            if (keyId.equals("dantiaoSingleProtect")) {
                                if (entry.getKey().equals("maxLose")) {
                                    resultMap.put("drbh_maxLose", entry.getValue());
                                } else if (entry.getKey().equals("min")) {
                                    resultMap.put("drbh_min", entry.getValue());
                                } else if (entry.getKey().equals("max")) {
                                    resultMap.put("drbh_max", entry.getValue());
                                }
                            } else if (keyId.equals("dantiaoSingleDrawLots")) {
                                if (entry.getKey().equals("id")) {
                                    resultMap.put("drcq_id", entry.getValue());
                                } else if (entry.getKey().equals("maxWin")) {
                                    resultMap.put("drcq_maxWin", entry.getValue());
                                } else if (entry.getKey().equals("chance")) {
                                    resultMap.put("drcq_chance", entry.getValue());
                                } else if (entry.getKey().equals("isProtect")) {
                                    resultMap.put("drcq_isProtect", entry.getValue());
                                }
                            } else if (keyId.equals("dantiaoSingleMaxLose")) {
                                if (entry.getKey().equals("maxLose")) {
                                    resultMap.put("darks_maxLose", entry.getValue());
                                } else if (entry.getKey().equals("min")) {
                                    resultMap.put("darks_min", entry.getValue());
                                } else if (entry.getKey().equals("max")) {
                                    resultMap.put("darks_max", entry.getValue());
                                }
                            } else if (keyId.equals("dantiaoSingleMaxWin")) {
                                if (entry.getKey().equals("maxWin")) {
                                    resultMap.put("darky_maxWin", entry.getValue());
                                } else if (entry.getKey().equals("min")) {
                                    resultMap.put("darky_min", entry.getValue());
                                } else if (entry.getKey().equals("max")) {
                                    resultMap.put("darky_max", entry.getValue());
                                }
                            } else if (keyId.equals("dantiaoMultiplyMaxLose")) {
                                if (entry.getKey().equals("maxLose")) {
                                    resultMap.put("dorks_maxLose", entry.getValue());
                                } else if (entry.getKey().equals("min")) {
                                    resultMap.put("dorks_min", entry.getValue());
                                } else if (entry.getKey().equals("max")) {
                                    resultMap.put("dorks_max", entry.getValue());
                                }
                            } else if (keyId.equals("dantiaoMultiplyMaxWin")) {
                                if (entry.getKey().equals("maxWin")) {
                                    resultMap.put("dorky_maxWin", entry.getValue());
                                } else if (entry.getKey().equals("min")) {
                                    resultMap.put("dorky_min", entry.getValue());
                                } else if (entry.getKey().equals("max")) {
                                    resultMap.put("dorky_max", entry.getValue());
                                }
                            } else if (keyId.equals("dantiaoMultiplyBetPeopleCnt")) {
                                if (entry.getKey().equals("betPeopleCnt")) {
                                    resultMap.put("drrs_betPeopleCnt", entry.getValue());
                                } else if (entry.getKey().equals("totalBet")) {
                                    resultMap.put("drrs_totalBet", entry.getValue());
                                }
                            } else if (keyId.equals("dantiaoNewDataBetLimit")) {
                                if (entry.getKey().equals("gameId")) {
                                    resultMap.put("game_id", entry.getValue());
                                } else if (entry.getKey().equals("fourColorMaxBet")) {
                                    resultMap.put("four_color_max_bet", entry.getValue());
                                } else if (entry.getKey().equals("fourColorMinBet")) {
                                    resultMap.put("four_color_min_bet", entry.getValue());
                                } else if (entry.getKey().equals("kingMaxBet")) {
                                    resultMap.put("king_max_bet", entry.getValue());
                                }
                            }
                        }
                        resultList.add(resultMap);
                    }
                }
            }
        }
        return resultList;
    }
}
