package com.youwan.game.cac.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.PushConfigDto;
import com.youwan.game.cac.api.dto.PushConfigFromDto;
import com.youwan.game.cac.api.entity.SysPushConfig;
import com.youwan.game.cac.service.SysPushConfigService;
import com.youwan.game.common.core.push.jpush.JPushConfig;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;



/**
 * @date 2020-05-23 15:14:12
 */
@RestController
@AllArgsConstructor
@RequestMapping("/pushconfig")
public class SysPushConfigController {

  private final SysPushConfigService sysPushConfigService;

  /**
   * 分页查询
   * @param page 分页对象
   * @return
   */
  @GetMapping("/page")
  public R getSysPushConfigPage(Page page, PushConfigDto pushConfigDto) {
    return  new R<>(sysPushConfigService.queryPushConfigPage(page, pushConfigDto.getChannelId(), pushConfigDto.getName(), pushConfigDto.getStatus()));
  }

  @GetMapping("/getObj/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(sysPushConfigService.getById(id));
  }

  @GetMapping("/getConfigById/{id}")
  public R getConfigById(@PathVariable("id") Integer id){
    JPushConfig config = sysPushConfigService.getPushConfig(id);
    return new R<>(config);
  }

  @GetMapping("/activate/{id}")
  public R activate(@PathVariable("id") Integer id){
    return new R<>(sysPushConfigService.activatePushCofing(id));
  }

  @PostMapping("/doconfig")
  public R doConfig(@RequestBody PushConfigFromDto pushConfigFromDto){
    return new R<>(sysPushConfigService.doConfig(pushConfigFromDto.getPushConfig(), pushConfigFromDto.getId()));
  }

  @SysLog("新增")
  @PostMapping("/add")
  public R save(@RequestBody SysPushConfig sysPushConfig){
    return new R<>(sysPushConfigService.addPushConfig(sysPushConfig));
  }

  /**
   * 修改
   * @param sysPushConfig 
   * @return R
   */
  @SysLog("修改")
  @PostMapping("/update")
  public R updateById(@RequestBody SysPushConfig sysPushConfig){
    return new R<>(sysPushConfigService.updatePushConfig(sysPushConfig));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(sysPushConfigService.delPushConfig(id));
  }

}
