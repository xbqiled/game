package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.SysVersionConfig;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-02-08 16:36:30
 */
public interface SysVersionConfigMapper extends BaseMapper<SysVersionConfig> {

   List<SysVersionConfig> queryModelByChannelId(@Param("channelId")String channelId, @Param("version")String version);

}
