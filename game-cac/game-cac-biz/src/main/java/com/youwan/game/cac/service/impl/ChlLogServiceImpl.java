package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.ChlLog;
import com.youwan.game.cac.mapper.ChlLogMapper;
import com.youwan.game.cac.service.ChlLogService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 系统日志
 *
 * @author code generator
 * @date 2019-05-23 13:09:56
 */
@Service("chlLogService")
public class ChlLogServiceImpl extends ServiceImpl<ChlLogMapper, ChlLog> implements ChlLogService {

    @Override
    public IPage<List<ChlLog>> queryLogPage(Page page, QueryDto queryDto) {
        return baseMapper.queryLogPage(page, queryDto.getChannelNo(), queryDto.getUserId(), queryDto.getUserName(), queryDto.getOperation(),  queryDto.getStartTime(), queryDto.getEndTime() );
    }
}
