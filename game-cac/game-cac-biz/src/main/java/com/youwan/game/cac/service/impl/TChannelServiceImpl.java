package com.youwan.game.cac.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.TChannel;
import com.youwan.game.cac.api.entity.TChannelRelation;
import com.youwan.game.cac.mapper.ChannelMapper;
import com.youwan.game.cac.mapper.ChannelRelationMapper;
import com.youwan.game.cac.service.TChannelService;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.HttpHandler;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 
 *
 * @author code generator
 * @date 2019-02-08 18:30:36
 */
@Slf4j
@AllArgsConstructor
@Service("channelService")
public class TChannelServiceImpl extends ServiceImpl<ChannelMapper, TChannel> implements TChannelService {

    private final ChannelRelationMapper channelRelationMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int saveChannel(TChannel tChannel) {

        //先查询对应的是否存在
        TChannel channel = baseMapper.getChannelInfo(tChannel.getChannelId());
        if (channel != null && StrUtil.isNotBlank(channel.getChannelId())) {
             return 1;
        }

        //调用接口处理
        GsonResult result =  HttpHandler.synChannel(tChannel.getChannelId(), tChannel.getChannelName(), tChannel.getState());
        if (result.getRet() == 0) {
            tChannel.setCreateTime(LocalDateTime.now());
            tChannel.setCreateBy(Objects.requireNonNull(getUsername()));

            this.save(tChannel);
            return 0;
        } else {
            return 2;
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateChannel(TChannel tChannel) {
        GsonResult result = HttpHandler.synChannel(tChannel.getChannelId(), tChannel.getChannelName(), tChannel.getState());
        if (result.getRet() == 0) {
            this.updateById(tChannel);
            return 0;
        } else {
            return 2;
        }
    }

    @Override
    public TChannel getChannelInfo(String channelId) {
        return baseMapper.getChannelInfo(channelId);
    }



    @Override
    public List<Map<String, Object>> getConfigGame(String channelId) {
        //查询所有游戏配置
        List<Map<String, Object>> list = baseMapper.getGameRoomInfo();
        List<TChannelRelation> channelList = channelRelationMapper.getGameConfigByChannelId(channelId);
        TChannel channel = baseMapper.getChannelInfo(channelId);

        if (list != null && list.size() > 0) {
            for (int i = 0 ; i < list.size() ; i ++) {
                Map<String, Object> map = list.get(i);

                Integer gameAtomTypeId = (Integer) map.get("gameAtomTypeId");
                map.put("channelName",  channel.getChannelName());
                map.put("status", false);

                for (TChannelRelation channelRelation : channelList) {
                    if ((channelRelation.getAtomTypeId().intValue() == gameAtomTypeId.intValue()) && channelRelation.getStatus().intValue() == 1) {
                        map.put("status", true);
                    }
                }
            }
        }
        return list;
    }


    @Override
    public IPage<List<TChannel>> queryChannelPage(Page page, String channelId, String channelName) {
        return baseMapper.queryChannelPage(page, channelId, channelName);
    }

    /**
     * 获取用户名称
     * @return username
     */
    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }
}
