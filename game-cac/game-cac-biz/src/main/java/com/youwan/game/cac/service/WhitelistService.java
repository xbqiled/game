package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.Whitelist;

import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author code generator
 * @date 2018-12-29 13:01:34
 */
public interface WhitelistService extends IService<Whitelist> {

    IPage<Map<String, Object>> queryPageList(Page page, Whitelist whitelist);

    Boolean addWhiteList(Whitelist whitelist);

    Boolean delWhiteList(String tAccountname);
}
