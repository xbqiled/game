package com.youwan.game.cac.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.MailDto;
import com.youwan.game.cac.api.dto.SendMailDto;
import com.youwan.game.cac.api.entity.Accountinfo;
import com.youwan.game.cac.api.entity.TChannel;
import com.youwan.game.cac.api.entity.Usermail;
import com.youwan.game.cac.api.vo.MailVo;
import com.youwan.game.cac.mapper.xbqp.AccountinfoMapper;
import com.youwan.game.cac.mapper.ChannelMapper;
import com.youwan.game.cac.mapper.xbqp.UsermailMapper;
import com.youwan.game.cac.service.UsermailService;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.HttpHandler;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-29 12:31:06
 */
@Slf4j
@AllArgsConstructor
@Service("usermailService")
public class UsermailServiceImpl extends ServiceImpl<UsermailMapper, Usermail> implements UsermailService {

    private final AccountinfoMapper accountinfoMapper;

    private final ChannelMapper channelMapper;

    @Override
    public MailVo getUserMailInfo(Integer mailId) {
        MailVo mailVo = baseMapper.getUserMailInfo(mailId);
        mailVo.setContent(new String (mailVo.getTContent()));

        return mailVo;
    }

    @Override
    public IPage<List<Map<String, Object>>> queryUserMailPage(Page page, MailDto mailDto) {
        return baseMapper.queryUserMailPage(page, mailDto.getTitle(), mailDto.getAccountId());
    }

    /**
     * <B>发送邮件</B>
     * @param sendMailDto
     * @return
     */
    @Override
    public int sendMail(SendMailDto sendMailDto) {
        //调用接口发送邮件
        String title = sendMailDto.getTitle();
        String content = sendMailDto.getContent();

        String rewardId = sendMailDto.getRewardId();
        String rewardNum = sendMailDto.getRewardNum();

        int [] channelList = {};
        int [] userList = {};
        if (StrUtil.isNotBlank(sendMailDto.getChannelList())) {
            String channelStr = sendMailDto.getChannelList();
            if (channelStr.contains(",")) {
                String[] strDesc = channelStr.split(",");
                if (strDesc != null && strDesc.length > 0) {
                    channelList = new int [strDesc.length];
                    for (int i = 0; i < strDesc.length; i++) {
                        channelList[i] = Integer.parseInt(strDesc[i]);
                    }
                }
            } else {
                //用户格式错误

                //判断是否存在这个用户
                TChannel channel = channelMapper.getChannelInfo(channelStr);
                if (channel != null && StrUtil.isNotBlank(channel.getChannelName())) {
                    channelList = new int[1];
                    channelList[0] = Integer.parseInt(channelStr);
                } else {
                    return 1;
                }
            }
        }

        if (StrUtil.isNotBlank(sendMailDto.getUserList())) {
            String userStr = sendMailDto.getUserList();
            if (userStr.contains(",")) {
                String[] strDesc = userStr.split(",");
                if (strDesc != null && strDesc.length > 0) {
                    userList = new int [strDesc.length];
                    for (int i = 0; i < strDesc.length; i++) {
                        userList[i] = Integer.parseInt(strDesc[i]);
                    }
                }
            } else {
                Accountinfo account = accountinfoMapper.getUserInfo(Integer.parseInt(userStr));
                if (account != null && StrUtil.isNotBlank(account.getTAccountname())) {
                    userList = new int[1];
                    userList[0] = Integer.parseInt(userStr);
                } else {
                    return 2;
                }
            }
        }

        GsonResult result = HttpHandler.sendMail(title, content, rewardId, rewardNum, channelList, userList, sendMailDto.getSendType());
        if (result != null && result.getRet() == 0) {
            return result.getRet();
        } else {
            return 3;
        }
    }


    @Override
    public MailVo findById(Integer id, Integer userId) {
        return baseMapper.findById(id, userId);
    }
}
