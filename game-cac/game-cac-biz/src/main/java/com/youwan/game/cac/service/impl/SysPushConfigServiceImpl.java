package com.youwan.game.cac.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.youwan.game.cac.api.entity.SysPushConfig;
import com.youwan.game.cac.mapper.SysPushConfigMapper;
import com.youwan.game.cac.service.SysPushConfigService;
import com.youwan.game.common.core.push.jpush.JPushConfig;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 
 *
 * @author code generator
 * @date 2020-05-23 15:14:12
 */
@Service("sysPushConfigService")
public class SysPushConfigServiceImpl extends ServiceImpl<SysPushConfigMapper, SysPushConfig> implements SysPushConfigService {

    @Override
    public IPage<List<Map<String, Object>>> queryPushConfigPage(Page page, String channelId, String name, String status) {
        return baseMapper.queryPushConfigPage(page, channelId, name,  status);
    }

    @Override
    public JPushConfig getPushConfig(Integer id) {
        SysPushConfig config = this.getById(id);
        if (config != null && StrUtil.isNotEmpty(config.getConfig())) {
            JPushConfig pushConfig = new Gson().fromJson(config.getConfig(), JPushConfig.class);
            return pushConfig;
        }
        return null;
    }

    @Override
    public Boolean doConfig(JPushConfig pushConfig, Integer id) {
        SysPushConfig config = this.getById(id);
        config.setConfig(new Gson().toJson(pushConfig));
        this.updateById(config);
        return Boolean.TRUE;
    }

    @Override
    public Boolean addPushConfig(SysPushConfig sysPushConfig) {
        sysPushConfig.setCreateTime(LocalDateTime.now());
        sysPushConfig.setIsActivate(0);
        sysPushConfig.setCreateBy(Objects.requireNonNull(getUsername()));
        this.save(sysPushConfig);
        return Boolean.TRUE;
    }

    @Override
    public Boolean updatePushConfig(SysPushConfig sysPushConfig) {
        sysPushConfig.setLastUpdateTime(LocalDateTime.now());
        this.updateById(sysPushConfig);
        return Boolean.TRUE;
    }

    @Override
    public Boolean delPushConfig(Integer pushConfigId) {
        this.removeById(pushConfigId);
        return Boolean.TRUE;
    }

    @Override
    public Boolean activatePushCofing(Integer pushConfigId) {
        SysPushConfig config = this.getById(pushConfigId);
        baseMapper.updateInvalidAll(config.getChannelId(), pushConfigId);
        baseMapper.activatePushCofing(pushConfigId);
        return Boolean.TRUE;
    }

    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }
}
