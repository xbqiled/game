package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.Ai;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-21 19:13:24
 */
public interface AiService extends IService<Ai> {

    Boolean modifyBot(Ai ai);

    IPage<Map<String, Object>> queryBotPage(Page page, String tNick,  String tId);
}
