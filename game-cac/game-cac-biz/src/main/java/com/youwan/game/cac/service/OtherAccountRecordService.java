package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.AccountRecord;

import java.util.List;


/**
 * 
 *
 * @author code generator
 * @date 2019-11-20 13:02:18
 */
public interface OtherAccountRecordService extends IService<AccountRecord> {

    IPage<List<AccountRecord>> queryAccountRecordPage(Page page, String channelId, String startTime, String endTime);

}
