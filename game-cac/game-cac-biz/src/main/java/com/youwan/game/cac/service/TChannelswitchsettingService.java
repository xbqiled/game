package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.TChannelswitchsetting;


/**
 * 
 *
 * @author code generator
 * @date 2019-02-08 18:43:43
 */
public interface TChannelswitchsettingService extends IService<TChannelswitchsetting> {

    TChannelswitchsetting queryByChannelId(String tChannelkey);

    Boolean  saveSetting(String tChannelkey, String tNormalsetting, String tAuditsetting);

}
