package com.youwan.game.cac.controller;

import com.youwan.game.cac.api.dto.ProxyMemberQueryDto;
import com.youwan.game.cac.api.entity.*;
import com.youwan.game.cac.service.ProxyMemberService;
import com.youwan.game.cac.service.ProxyMemberTreasureChangeService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.data.core.Page;
import com.youwan.game.common.security.annotation.Inner;
import io.swagger.models.auth.In;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-04-22.
 */
@RestController
@RequestMapping("/proxyMember")
public class ProxyMemberController {

    private ProxyMemberService proxyMemberService;
    private ProxyMemberTreasureChangeService treasureChangeService;

    public ProxyMemberController(ProxyMemberService proxyMemberService,
                                 ProxyMemberTreasureChangeService treasureChangeService) {
        this.proxyMemberService = proxyMemberService;
        this.treasureChangeService = treasureChangeService;
    }

    @GetMapping("/getMemberProfile/{accountId}")
    @Inner
    public R<ProxyMemberProfile> getMemberProfile(@PathVariable("accountId") Long accountId) {
        return new R<>(proxyMemberService.getMemberProfile(accountId));
    }

    @PostMapping("/getDirectSubordinates/{accountId}")
    @Inner
    public R<Page<ProxyMember>> getDirectSubordinates(@PathVariable("accountId") Long accountId,
                                                      @RequestBody ProxyMemberQueryDto queryDto) {
        return new R<>(proxyMemberService.getDirectSubordinates(accountId, queryDto.getMemberId(), queryDto.getOffset(), queryDto.getLimit()));
    }

    @PostMapping("/getAllSubordinates/{accountId}")
    @Inner
    public R<Page<ProxyMember>> getAllSubordinates(@PathVariable("accountId") Long accountId,
                                                   @RequestBody ProxyMemberQueryDto queryDto) {
        return new R<>(proxyMemberService.getAllSubordinates(accountId, queryDto.getDeep(), queryDto.getOffset(),
                queryDto.getLimit()));
    }

    @PostMapping("/getTreasureChanges/{accountId}")
    @Inner
    public R<Page<ProxyMemberTreasureChange>> getTreasureChanges(@PathVariable("accountId") Long accountId,
                                                                 @RequestBody ProxyMemberQueryDto queryDto) {
        queryDto.setUserId(accountId);
        return new R<>(proxyMemberService.queryProxyMemberTreasureChange(queryDto));
    }

    @PostMapping("/getRechargeOrders/{accountId}")
    @Inner
    public R<Page<ProxyMemberRechargeOrder>> getRechargeOrders(@PathVariable("accountId") Long accountId,
                                                               @RequestBody ProxyMemberQueryDto queryDto) {
        queryDto.setUserId(accountId);
        return new R<>(proxyMemberService.queryProxyMemberRechargeOrders(queryDto));
    }

    @PostMapping("/getCashOrders/{accountId}")
    @Inner
    public R<Page<ProxyMemberCashOrder>> getCashOrders(@PathVariable("accountId") Long accountId,
                                                       @RequestBody ProxyMemberQueryDto queryDto) {
        queryDto.setUserId(accountId);
        return new R<>(proxyMemberService.queryProxyMemberCashOrders(queryDto));
    }

    @PostMapping("/getGameScoreDetails/{accountId}")
    @Inner
    public R<Page<ProxyMemberGameScore>> getGameScores(@PathVariable("accountId") Long accountId,
                                                       @RequestBody ProxyMemberQueryDto queryDto) {
        queryDto.setUserId(accountId);
        return new R<>(proxyMemberService.queryProxyMemberGameScores(queryDto));
    }

    @PostMapping("/getGameScoreStats/{accountId}")
    @Inner
    public R<List<ProxyMemberGameScoreStats>> getGameScoreStats(@PathVariable("accountId") Long accountId,
                                                                @RequestBody ProxyMemberQueryDto queryDto) {
        queryDto.setUserId(accountId);
        return new R<>(proxyMemberService.queryProxyMemberGameScoreStats(queryDto));
    }

    @PostMapping("/getTeamMetrics/{accountId}")
    @Inner
    public R<ProxyMemberTeamMetrics> getTeamMetrics(@PathVariable("accountId") Long accountId,
                                                    @RequestBody ProxyMemberQueryDto queryDto) {
        queryDto.setUserId(accountId);
        return new R<>(proxyMemberService.queryTeamMetrics(queryDto));
    }

    @PostMapping("/getSubordinateTeamMetrics/{accountId}")
    @Inner
    public R<Page<ProxyMemberTeamMetrics>> getSubordinateTeamMetrics(@PathVariable("accountId") Long accountId,
                                                                     @RequestBody ProxyMemberQueryDto queryDto) {
        queryDto.setUserId(accountId);
        return new R<>(proxyMemberService.querySubordinateTeamMetrics(queryDto));
    }

    @PostMapping("/getRebateDetails/{accountId}")
    @Inner
    public R<Page<ProxyMemberRebateDetail>> getRebateDetails(@PathVariable("accountId") Long accountId,
                                                             @RequestBody ProxyMemberQueryDto queryDto) {
        queryDto.setUserId(accountId);
        return new R<>(proxyMemberService.queryRebateDetails(queryDto));
    }

    @PostMapping("/getRebateBalances/{accountId}")
    @Inner
    public R<Page<ProxyMemberRebateBalance>> getRebateBalances(@PathVariable("accountId") Long accountId,
                                                               @RequestBody ProxyMemberQueryDto queryDto) {
        queryDto.setUserId(accountId);
        return new R<>(proxyMemberService.queryRebateBalances(queryDto));
    }
}
