package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.CfgChlDownload;
import com.youwan.game.cac.api.entity.CfgChlDownload1;
import com.youwan.game.cac.api.vo.DownloadPanelConfigVo;
import com.youwan.game.cac.api.vo.DownloadPanelConfigVo1;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 
 *
 * @author code generator
 * @date 2019-12-30 15:25:40
 */
public interface CfgChlDownload1Mapper extends BaseMapper<CfgChlDownload1> {

    IPage<List<CfgChlDownload1>> queryDownloadCfgPage(Page page, @Param("channelId") String channelId);

    DownloadPanelConfigVo1 getConfigVoByChannelId(@Param("channelId") String channelId);

    CfgChlDownload1 getConfigByChannelId(@Param("channelId") String channelId);

}
