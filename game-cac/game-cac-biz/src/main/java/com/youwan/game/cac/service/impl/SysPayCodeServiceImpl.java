package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.SysPayCode;
import com.youwan.game.cac.api.vo.PayCodeVo;
import com.youwan.game.cac.mapper.SysPayCodeMapper;
import com.youwan.game.cac.service.SysPayCodeService;
import org.springframework.stereotype.Service;

/**
 * 
 *
 * @author code generator
 * @date 2019-05-10 11:23:04
 */
@Service("sysPayCodeService")
public class SysPayCodeServiceImpl extends ServiceImpl<SysPayCodeMapper, SysPayCode> implements SysPayCodeService {

    @Override
    public PayCodeVo getPayCodeByOrderNo(String orderNo) {
        return baseMapper.getPayCodeByOrderNo(orderNo);
    }
}
