package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.WxhhExpDto;
import com.youwan.game.cac.api.entity.Gameglobalextinfo;
import com.youwan.game.common.core.jsonmodel.GsonWxhhExp;

import java.util.List;


/**
 * VIEW
 *
 * @author code generator
 * @date 2020-05-20 16:19:40
 */
public interface GameglobalextinfoService extends IService<Gameglobalextinfo> {

    List<GsonWxhhExp> getWxhhExpPage(WxhhExpDto wxhhExpDto);
}
