package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.GameCtrlDto;
import com.youwan.game.cac.api.entity.Gamectrl;
import com.youwan.game.cac.api.vo.GameCtrlVo;
import com.youwan.game.cac.api.vo.GameRoomVo;
import com.youwan.game.cac.mapper.xbqp.GamectrlMapper;
import com.youwan.game.cac.service.GamectrlService;
import com.youwan.game.common.core.jsonmodel.GsonGameCtrl;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.HttpHandler;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-11 17:01:03
 */
@Service("gamectrlService")
public class GamectrlServiceImpl extends ServiceImpl<GamectrlMapper, Gamectrl> implements GamectrlService {

    @Override
    public IPage<List<GameCtrlVo>> queryGameCtrlPage(Page page, String gameId) {
        IPage<List<GameCtrlVo>> resultPage = baseMapper.queryGameCtrlPage(page, gameId);
        List list = resultPage.getRecords();

        for (int i = 0; i < list.size(); i++) {
            GameCtrlVo gameCtrlVo = (GameCtrlVo) list.get(i);
            String str = new String(gameCtrlVo.getTExtend());

            GsonGameCtrl game = new Gson().fromJson(str, GsonGameCtrl.class);
            gameCtrlVo.setDangerDown(game.getDangerDown());
            gameCtrlVo.setDangerUp(game.getDangerUp());

            gameCtrlVo.setSafeDown(game.getSafeDown());
            gameCtrlVo.setSafeUp(game.getSafeUp());
        }
        return resultPage;
    }


    @Override
    public int doSet(GameCtrlDto gameCtrlDto) {
       GsonResult result = HttpHandler.gameCtrl(Integer.parseInt(gameCtrlDto.getRoomId()), gameCtrlDto.getDangerDown(), gameCtrlDto.getSafeDown(),  gameCtrlDto.getSafeUp(),  gameCtrlDto.getDangerUp());
       if (result != null && result.getRet() == 0) {
           return 0;
       } else {
           return 1;
       }
    }


    @Override
    public List<Map<String, Object>> getRoomTreeData() {
        List<Map<String, Object>> reusltList = new ArrayList<>();

        List<GameRoomVo> gameList = baseMapper.queryGameList();
        List<GameRoomVo> roomList = baseMapper.queryGameRoomList();

        for (GameRoomVo  game :  gameList) {
            Map<String, Object> map = new HashMap<>();
            List<Map<String, Object>> childrenList = new ArrayList<>();

            map.put("value", game.getGameKindType());
            map.put("label", game.getGameKindName());

            for (GameRoomVo room : roomList) {
                if (game.getGameKindType().intValue() == room.getGameKindType().intValue()) {
                    Map<String, Object> childrenMap = new HashMap<>();

                    childrenMap.put("value", room.getGameAtomTypeId());
                    childrenMap.put("label", room.getPhoneGameName());
                    childrenList.add(childrenMap);
                }
            }

            map.put("children", childrenList);
            reusltList.add(map);
        }
        return reusltList;
    }
}
