package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.StatisGame;
import com.youwan.game.cac.api.vo.StatisGameVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2020-03-07 15:37:57
 */
public interface StatisGameMapper extends BaseMapper<StatisGame> {

    IPage<List<StatisGameVo>> queryStatisGamePage(Page page, @Param("tabelName")String tabelName,
                                                  @Param("channelId")String channelId,
                                                  @Param("gameId")String gameId,
                                                  @Param("startTime") String startTime,
                                                  @Param("endTime") String endTime);


    IPage<List<StatisGameVo>> queryStatisUserGamePage(Page page, @Param("tabelName")String tabelName,
                                               @Param("channelId")String channelId,
                                               @Param("userId")Integer userId,
                                               @Param("gameId")String gameId,
                                               @Param("startTime") String startTime,
                                               @Param("endTime") String endTime);


    List<Map<String, String>> queryGameList();

}
