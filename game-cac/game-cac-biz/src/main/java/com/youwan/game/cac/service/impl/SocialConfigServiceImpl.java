package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.SocialConfig;
import com.youwan.game.cac.mapper.SocialConfigMapper;
import com.youwan.game.cac.service.SocialConfigService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * 
 *
 * @author code generator
 * @date 2019-03-26 15:43:34
 */
@Service("socialConfigService")
public class SocialConfigServiceImpl extends ServiceImpl<SocialConfigMapper, SocialConfig> implements SocialConfigService {


    @Override
    public SocialConfig getSocialConfigById(String channelId) {
        return baseMapper.getSocialConfigById(channelId);
    }


    @Override
    public Boolean addSocialConfig(SocialConfig socialConfig) {
        socialConfig.setCreateTime(LocalDateTime.now());
        socialConfig.setCreateBy(Objects.requireNonNull(getUsername()));

        this.save(socialConfig);
        return Boolean.TRUE;
    }


    /**
     * 获取用户名称
     * @return username
     */
    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }
}
