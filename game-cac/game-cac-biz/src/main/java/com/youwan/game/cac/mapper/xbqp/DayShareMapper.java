package com.youwan.game.cac.mapper.xbqp;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.DayShareEntity;
import com.youwan.game.cac.api.vo.BindUrlVo;
import com.youwan.game.cac.api.vo.DayShareVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-15 16:02:59
 */
public interface DayShareMapper extends BaseMapper<DayShareEntity> {

   IPage<DayShareVo> queryPage(Page page, @Param("channelId") String channelId);

   DayShareEntity getDayShareByChannelId(@Param("channelId") String channelId);


}
