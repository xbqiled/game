package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.BindUrlEntity;
import com.youwan.game.cac.api.entity.BindUrlForm;
import com.youwan.game.cac.api.vo.BindUrlVo;
import com.youwan.game.cac.mapper.xbqp.BindUrlMapper;
import com.youwan.game.cac.service.BindUrlService;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.HttpHandler;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-08-03 15:31:04
 */
@Service("bindUrlService")
public class BindUrlServiceImpl extends ServiceImpl<BindUrlMapper, BindUrlEntity> implements BindUrlService {

    @Override
    public IPage<BindUrlVo> queryPage(Page page, QueryDto queryDto) {
        return this.baseMapper.queryPage(page, queryDto.getChannelId());
    }

    @Override
    public BindUrlEntity getByChannelId(String channelId) {
        return baseMapper.getByChannelId(channelId);
    }

    @Override
    public int deleteEntity(BindUrlForm bindUrlForm) {
        GsonResult result = HttpHandler.cfgBindUrl(bindUrlForm.getUrl(), 2, bindUrlForm.getChannelId(), Long.valueOf(bindUrlForm.getAccountId()));
        return result.getRet();
    }

    @Override
    public int saveEntity(BindUrlForm bindUrlForm) {
        GsonResult result = HttpHandler.cfgBindUrl(bindUrlForm.getUrl(), 1, bindUrlForm.getChannelId(), Long.valueOf(bindUrlForm.getAccountId()));
        return result.getRet();
    }

}
