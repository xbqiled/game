package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.EnterpriseConfig;
import com.youwan.game.cac.service.EnterpriseConfigService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-11-06 13:19:25
 */
@RestController
@AllArgsConstructor
@RequestMapping("/wxenterpriseconfig")
public class EnterpriseConfigController {

  private final EnterpriseConfigService enterpriseConfigService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param wxEnterpriseConfig 
   * @return
   */
  @GetMapping("/page")
  public R getWxEnterpriseConfigPage(Page page, EnterpriseConfig wxEnterpriseConfig) {
    return  new R<>(enterpriseConfigService.page(page, Wrappers.query(wxEnterpriseConfig)));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(enterpriseConfigService.getById(id));
  }

  /**
   * 新增
   * @param wxEnterpriseConfig 
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody EnterpriseConfig wxEnterpriseConfig){
    return new R<>(enterpriseConfigService.save(wxEnterpriseConfig));
  }

  /**
   * 修改
   * @param wxEnterpriseConfig 
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody EnterpriseConfig wxEnterpriseConfig){
    return new R<>(enterpriseConfigService.updateById(wxEnterpriseConfig));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(enterpriseConfigService.removeById(id));
  }

}
