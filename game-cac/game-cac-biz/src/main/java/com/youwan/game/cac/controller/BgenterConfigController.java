package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.BgEnterGameDto;
import com.youwan.game.cac.api.entity.BgenterConfig;
import com.youwan.game.cac.service.BgenterConfigService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-11-12 14:04:32
 */
@RestController
@AllArgsConstructor
@RequestMapping("/bgenterconfig")
public class BgenterConfigController {

  private final BgenterConfigService bgenterConfigService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param bgEnterGameDto
   * @return
   */
  @GetMapping("/page")
  public R getOtherBgenterConfigPage(Page page, BgEnterGameDto bgEnterGameDto) {
    return  new R<>(bgenterConfigService.queryEnterGameConfigPage(page, bgEnterGameDto));
  }


  /**
   * 通过id查询
   * @param channelId
   * @return R
   */
  @GetMapping("/getConfig/{channelId}")
  public R getById(@PathVariable("channelId") String channelId){
    return new R<>(bgenterConfigService.getConfigByChannelId(channelId));
  }

  /**
   * 新增
   * @param bgenterConfig
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody BgenterConfig bgenterConfig){
    Boolean result = bgenterConfigService.saveEnterConfig(bgenterConfig);
    if (result) {
      return new R<>();
    } else {
      return new R<>().setCode(1).setMsg("渠道配置重复");
    }
  }

  /**
   * 修改
   * @param bgenterConfig
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R moidfy(@RequestBody BgenterConfig bgenterConfig){
    Boolean result = bgenterConfigService.modifyEnterConfig(bgenterConfig);
    if (result) {
      return new R<>();
    } else {
      return new R<>().setCode(1).setMsg("渠道配置重复");
    }
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(bgenterConfigService.removeById(id));
  }

}
