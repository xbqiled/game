package com.youwan.game.cac.service.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.AccountRecord;
import com.youwan.game.cac.mapper.OtherAccountRecordMapper;
import com.youwan.game.cac.service.OtherAccountRecordService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-20 13:02:18
 */
@Service("otherAccountRecordService")
public class OtherAccountRecordServiceImpl extends ServiceImpl<OtherAccountRecordMapper, AccountRecord> implements OtherAccountRecordService {

    @Override
    public IPage<List<AccountRecord>> queryAccountRecordPage(Page page, String channelId, String startTime, String endTime) {
        return baseMapper.queryAccountRecordPage(page, channelId, startTime, endTime);
    }
}
