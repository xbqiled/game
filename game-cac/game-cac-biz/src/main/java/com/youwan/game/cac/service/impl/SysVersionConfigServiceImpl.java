package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.VersionConfigDto;
import com.youwan.game.cac.api.entity.SysVersionConfig;
import com.youwan.game.cac.mapper.SysVersionConfigMapper;
import com.youwan.game.cac.service.SysVersionConfigService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * 
 *
 * @author code generator
 * @date 2019-02-08 16:36:30
 */
@Service("sysVersionConfigService")
public class SysVersionConfigServiceImpl extends ServiceImpl<SysVersionConfigMapper, SysVersionConfig> implements SysVersionConfigService {


    @Override
    public Boolean saveVersionConfig(SysVersionConfig sysVersionConfig) {
        sysVersionConfig.setCreateTime(LocalDateTime.now());
        sysVersionConfig.setCreateBy(Objects.requireNonNull(getUsername()));

        this.save(sysVersionConfig);
        return Boolean.TRUE;
    }

    @Override
    public Boolean updateVersionConfigById(SysVersionConfig sysVersionConfig) {
        sysVersionConfig.setModifyTime(LocalDateTime.now());
        sysVersionConfig.setModifyBy(getUsername());

        this.updateById(sysVersionConfig);
        return Boolean.TRUE;
    }


    /**
     * 获取用户名称
     * @return username
     */
    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }

    /**
     * <B>通过渠道ID 和版本查询对应的信息</B>
     * @return
     */

    @Override
    public List<SysVersionConfig> queryModelByChannelId(VersionConfigDto config) {
        return baseMapper.queryModelByChannelId(config.getChannelId(), config.getVersion());
    }
}

