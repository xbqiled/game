package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.SysPushConfig;
import com.youwan.game.common.core.push.jpush.JPushConfig;

import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author code generator
 * @date 2020-05-23 15:14:12
 */
public interface SysPushConfigService extends IService<SysPushConfig> {

    IPage<List<Map<String, Object>>> queryPushConfigPage(Page page, String channelId,  String name, String status);

    JPushConfig getPushConfig(Integer id);

    Boolean addPushConfig(SysPushConfig sysPushConfig);

    Boolean doConfig(JPushConfig pushConfig, Integer id);

    Boolean updatePushConfig(SysPushConfig sysPushConfig);

    Boolean delPushConfig(Integer pushConfigId);

    Boolean activatePushCofing(Integer pushConfigId);
}
