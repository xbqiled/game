package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.SysPayPlatform;
import com.youwan.game.cac.mapper.SysPayPlatformMapper;
import com.youwan.game.cac.service.SysPayPlatformService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-27 16:32:02
 */
@Service("sysPayPlatformService")
public class SysPayPlatformServiceImpl extends ServiceImpl<SysPayPlatformMapper, SysPayPlatform> implements SysPayPlatformService {

    @Override
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(value = "domain:details", allEntries = true)
    public Boolean savePlatform(SysPayPlatform sysPayPlatform) {
        sysPayPlatform.setCreateTime(LocalDateTime.now());
        sysPayPlatform.setCreateBy(Objects.requireNonNull(getUsername()));

        this.save(sysPayPlatform);
        return Boolean.TRUE;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(value = "domain:details", allEntries = true)
    public Boolean updatePlatformById(SysPayPlatform sysPayPlatform) {
        sysPayPlatform.setModifyTime(LocalDateTime.now());
        sysPayPlatform.setModifyBy(getUsername());

        this.updateById(sysPayPlatform);
        return Boolean.TRUE;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(value = "domain:details", allEntries = true)
    public Boolean deletePlatformById(Integer id) {
        return this.removeById(id);
    }

    /**
     * 用户分页查询
     * @param page
     * @param company
     * @return
     */
    @Override
    public IPage<List<SysPayPlatform>> queryPayPlatformPage(Page page, String company) {
        return  baseMapper.queryPayPlatformPage(page, company);
    }

    @Override
    @CacheEvict(value = "pay:platform:details", key = "#id")
    public SysPayPlatform getPlatformById(Integer id) {
        return this.getById(id);
    }

    /**
     * 获取用户名称
     *
     * @return username
     */
    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }
}
