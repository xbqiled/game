package com.youwan.game.cac.controller;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.admin.api.dto.QrCodeDto;
import com.youwan.game.admin.api.feign.RemoteOssService;
import com.youwan.game.cac.api.dto.*;
import com.youwan.game.cac.api.entity.SysPayCode;
import com.youwan.game.cac.api.entity.SysPayOrder;
import com.youwan.game.cac.api.vo.PayConfigVo;
import com.youwan.game.cac.api.vo.PayResult;
import com.youwan.game.cac.handler.PayHand;
import com.youwan.game.cac.service.SysPayCodeService;
import com.youwan.game.cac.service.SysPayConfigService;
import com.youwan.game.cac.service.SysPayOrderService;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.jsonmodel.GsonReduceResult;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.common.security.annotation.Inner;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.math.BigDecimal;


/**
 * @author code generator
 * @date 2019-02-20 18:03:40
 */
@RestController
@AllArgsConstructor
@RequestMapping("/payorder")
public class SysPayOrderController {

    private final SysPayOrderService sysPayOrderService;

    private final SysPayConfigService sysPayConfigService;

    private final SysPayCodeService sysPayCodeService;

    private final RemoteOssService remoteOssService;

    /**
     * 分页查询
     *
     * @param page    分页对象
     * @param orderNo
     * @return
     */
    @GetMapping("/page")
    public R getSysPayOrderPage(Page page, String orderNo, String userId, String status) {
        return new R<>(sysPayOrderService.queryPayOrderPage(page, orderNo, userId, status));
    }

    /**
     * 通过id查询
     *
     * @param id id
     * @return R
     */
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") Integer id) {
        return new R<>(sysPayOrderService.getById(id));
    }

    /**
     * 新增
     *
     * @param sysPayOrder
     * @return R
     */
    @SysLog("新增")
    @PostMapping
    public R save(@RequestBody SysPayOrder sysPayOrder) {
        return new R<>(sysPayOrderService.save(sysPayOrder));
    }

    /**
     * 修改
     *
     * @param sysPayOrder
     * @return R
     */
    @SysLog("修改")
    @PutMapping
    public R updateById(@RequestBody SysPayOrder sysPayOrder) {
        return new R<>(sysPayOrderService.updateById(sysPayOrder));
    }

    /**
     * 通过id删除
     *
     * @param id id
     * @return R
     */
    @SysLog("删除")
    @DeleteMapping("/{id}")
    public R removeById(@PathVariable Integer id) {
        return new R<>(sysPayOrderService.removeById(id));
    }


    /**
     * <B>手动上分</B>
     *
     * @param rechargeDto
     * @return
     */
    @SysLog("手动上分")
    @RequestMapping("/recharge")
    @PreAuthorize("@pms.hasPermission('cac_syspayorder_add')")
    public R recharge(RechargeDto rechargeDto) {
        Boolean reuslt = sysPayOrderService.recharge(rechargeDto.getUserId(), rechargeDto.getPayFee(), rechargeDto.getDepositor(),
                rechargeDto.getAccount(), rechargeDto.getRemark(), rechargeDto.getPayType(), rechargeDto.getDamaMulti(), rechargeDto.getRate());
        if (reuslt) {
            return new R<>();
        } else {
            return new R<>().setCode(1).setMsg("上分失败!");
        }
    }

    /**
     * <B>银商上分</B>
     *
     * @param
     * @return
     */
    @SysLog("银商上分")
    @Inner
    @RequestMapping("/addPoint")
    public R addPoint(@Valid  @RequestBody AddPointDto addPointDto) {
        return new R<>(sysPayOrderService.addPoint(addPointDto));
    }


    @SysLog("手动扣款")
    @RequestMapping("/reduce")
    public R reduce(ReduceDto reduceDto) {
        GsonReduceResult resulet = sysPayOrderService.reduceFee(reduceDto.getUserId(), reduceDto.getNote(), reduceDto.getPayFee(), reduceDto.getForce());
        if (resulet != null && resulet.getRet() == 0 && resulet.getRealCancel() != null && resulet.getRealCancel() > 0) {
            return new R<>(resulet);
        } else if (resulet == null) {
            return new R<>().setCode(1).setMsg("找不到到对应的用户!");
        } else {
            return new R<>().setCode(1).setMsg("处理手动扣分失败!");
        }
    }

    /**
     * <B>创建支付订单</B>
     *
     * @param payOrderDto
     * @return
     */
    @Inner
    @PostMapping("/createPayOrder")
    public R createPayOrder(@Valid @RequestBody PayOrderDto payOrderDto) {
        return new R<>(sysPayOrderService.createPayOrder(payOrderDto));
    }

    /**
     * <B>回调订单信息,支付成功</B>
     *
     * @return
     */
    @Inner
    @PostMapping("/callbackPayOrder")
    public R callbackPayOrdery(@Valid @RequestBody NotifyPayOrderDto notifyPayOrderDto) {
        return new R<>(sysPayOrderService.callbackPayOrdery(notifyPayOrderDto));
    }

    /**
     * <B>获取订单详情</B>
     *
     * @param orderId
     * @return
     */
    @Inner
    @PostMapping("/getPayOrder/{orderId}")
    public R getOrdery(@PathVariable String orderId) {
        return new R<>(sysPayOrderService.getOrder(orderId));
    }

    /**
     * <B>获取配置密钥</B>
     *
     * @param orderId
     * @return
     */
    @Inner
    @PostMapping("/getMerchantKey/{orderId}")
    public R getMerchantKey(@PathVariable String orderId) {
        return new R<>(sysPayOrderService.getMerchantKey(orderId));
    }


    /**
     * <B>获取支付链接</B>
     *
     * @param payOrderDto
     * @return
     */
    @Inner
    @PostMapping("/getPayUrl")
    public R getPayUrl(@Valid @RequestBody PayOrderDto payOrderDto,HttpServletRequest request) {
        //首先获取配置信息
        PayConfigVo payConfigVo = sysPayConfigService.queryPayConfigVo(payOrderDto.getChannelId(), payOrderDto.getConfigId());
        //根据类型判断是否生成订单
        PayResult payResult = sysPayOrderService.getPayUrl(payConfigVo, payOrderDto,request);

        if (payResult != null && StrUtil.isNotBlank(payResult.getPayUrl()) && StrUtil.isNotBlank(payResult.getType()) && payResult.getType().equals("1")) {
            //生成二维码
            R<String> r = remoteOssService.createQrcode(new QrCodeDto(0, 0, 0, payResult.getPayUrl(), ""), SecurityConstants.FROM_IN);
            if (r == null || r.getData() == null) {
                return new R<>(new PayResult());
            }

            String orCodeUrl = r.getData();
            BigDecimal payDecimal = new BigDecimal(payOrderDto.getPayfee());
            SysPayCode payCode = PayHand.createPayCode(payDecimal, payResult.getOrderNo(), orCodeUrl, payResult.getPayUrl(), Integer.parseInt(payOrderDto.getConfigId()), payOrderDto.getChannelId());

            payResult.setPayUrl(PayHand.getCodePayUrl(payResult.getOrderNo()));
            sysPayCodeService.save(payCode);
        }

        return new R<>(payResult);
    }


    /**
     * <B>获取支付信息</B>
     *
     * @param payOrderDto
     * @return
     */
    @Inner
    @PostMapping("/getPayInfo")
    public R getPayInfo(@Valid @RequestBody PayOrderDto payOrderDto, HttpServletRequest request) {
        //获取配置信息
        PayConfigVo payConfigVo = sysPayConfigService.queryPayConfigVo(payOrderDto.getChannelId(), payOrderDto.getConfigId());
        //获取支付信息
        return new R<>(sysPayOrderService.getPayInfo(payConfigVo, payOrderDto,request));
    }


}
