package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.XinBoSwitchDto;
import com.youwan.game.cac.api.entity.Channelxinboonoff;
import com.youwan.game.cac.api.vo.XinBoSwitchVo;

import java.util.List;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-12-15 13:50:26
 */
public interface ChannelxinboonoffService extends IService<Channelxinboonoff> {

    List<XinBoSwitchVo> selectAllSwitch(XinBoSwitchDto xinBoSwitchDto);

    Integer doConfig(XinBoSwitchDto xinBoSwitchDto);

}
