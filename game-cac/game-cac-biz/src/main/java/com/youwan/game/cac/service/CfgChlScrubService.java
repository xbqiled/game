package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.CfgChlScrub;
import com.youwan.game.common.core.betting.ScrubConfig;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-03.
 */
public interface CfgChlScrubService extends IService<CfgChlScrub> {

    ScrubConfig getScrubConfig(String channelId, Integer vipLevel);

}
