package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.WxCashOrder;


/**
 * 
 *
 * @author code generator
 * @date 2019-11-06 13:34:56
 */
public interface WxCashOrderService extends IService<WxCashOrder> {

}
