package com.youwan.game.cac.service.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.report.MessagesResult;
import cn.jpush.api.report.ReceivedsResult;
import cn.jpush.api.schedule.ScheduleMsgIdsResult;
import cn.jpush.api.schedule.ScheduleResult;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.PushFromDto;
import com.youwan.game.cac.api.entity.SysPushConfig;
import com.youwan.game.cac.api.entity.SysPushRecord;
import com.youwan.game.cac.mapper.SysPushConfigMapper;
import com.youwan.game.cac.mapper.SysPushRecordMapper;
import com.youwan.game.cac.service.SysPushRecordService;
import com.youwan.game.common.core.push.jpush.JPushConfig;
import com.youwan.game.common.core.push.jpush.JPushServer;
import com.youwan.game.common.core.push.jpush.model.JPushRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * alias
 *
 * @author code generator
 * @date 2020-05-23 17:21:44
 */
@Slf4j
@AllArgsConstructor
@Service("sysPushRecordService")
public class SysPushRecordServiceImpl extends ServiceImpl<SysPushRecordMapper, SysPushRecord> implements SysPushRecordService {


    private final SysPushConfigMapper pushConfigMapper;

    @Override
    public IPage<List<SysPushRecord>> queryPushRecordPage(Page page, String channelId, String startTime, String endTime) {
        return baseMapper.queryPushRecordPage(page, channelId, startTime, endTime);
    }

    @Override
    public Map<String, Object> getObj(Integer id) {
        return baseMapper.getPushRecordById(id);
    }

    @Override
    public Boolean sendPush(PushFromDto pushFromDto) {
        try {
            SysPushConfig pushConfig = pushConfigMapper.getActivateConfig(pushFromDto.getChannelId());
            if (pushConfig == null) {
                return null;
            }

            JPushConfig config = new Gson().fromJson(pushConfig.getConfig(), JPushConfig.class);
            JPushRequest request = new JPushRequest();
            String[] aliasArray;
            String retainTimeStr = "";

            Audience audience = null;
            SysPushRecord push = new SysPushRecord();
            push.setChannelId(pushFromDto.getChannelId());

            push.setPlatform(pushFromDto.getDevicePlatform());
            push.setTitle(pushFromDto.getTitle());
            push.setContent(pushFromDto.getContent());

            push.setPushTime(LocalDateTime.now());
            push.setSendType(pushFromDto.getSendType());
            if (StrUtil.isNotBlank(pushFromDto.getAlias()) && pushFromDto.getSendType() != null && pushFromDto.getSendType() == 1) {

                push.setAudience(pushFromDto.getAlias());
                aliasArray = getAlias(pushFromDto.getAlias());
                if (aliasArray == null || aliasArray.length == 0) {
                    return null;
                }
                audience = Audience.alias(aliasArray);
            }

            push.setTimeType(pushFromDto.getTimeType());
            if (pushFromDto.getRetainTime() != null && pushFromDto.getTimeType() != null && pushFromDto.getTimeType() == 1) {
                retainTimeStr = DateUtil.format(new Date(new Long(pushFromDto.getRetainTime())), DatePattern.NORM_DATETIME_FORMAT);
                push.setRetainTime(new Date(new Long(pushFromDto.getRetainTime())));
            }

            push.setTimeLong(pushFromDto.getTimeLong());
            push.setPlatformId(pushConfig.getPlatformId());
            push.setConfigId(pushConfig.getId());

            push.setSendSys(0);
            push.setPushBy(Objects.requireNonNull(getUsername()));
            //发送
            JPushServer server = new JPushServer(config);
            request.setTitle(pushFromDto.getTitle());
            request.setContent(pushFromDto.getContent());

            request.setDevicePlatform(pushFromDto.getDevicePlatform());
            request.setTimeToAlive(pushFromDto.getTimeLong());
            request.setApnsProduction(Boolean.TRUE);

            if (pushFromDto.getTimeType() == 0) {
                PushResult result = server.sendPush(request, audience == null ? Audience.all() : audience);
                if (result.statusCode == 0) {
                    //保存信息
                    if (result != null) {
                        push.setMsgId(new Long (result.msg_id).toString());
                    }
                    this.save(push);
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            } else {
                ScheduleResult result = server.createSingleSchedule("推送" + IdUtil.simpleUUID(), retainTimeStr, request, audience == null ? Audience.all() : audience);
                if (StrUtil.isNotBlank(result.getSchedule_id())) {
                    //保存信息
                    if (result != null) {
                        push.setMsgId(result.getSchedule_id());
                    }
                    this.save(push);
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
        } catch (Exception e) {
            return null;
        }
    }


    private String [] getAlias(String aliasStr) {
        if (aliasStr != null && aliasStr.contains(",")) {
            return aliasStr.split(",");
        } else {
            return new String[] {aliasStr};
        }
    }


    @Override
    public ReceivedsResult getReport(Integer id) {
        try {
            SysPushRecord push = this.getById(id);
            SysPushConfig pushConfig = pushConfigMapper.getActivateConfig(push.getChannelId());
            if (pushConfig == null) {
                return null;
            }
            JPushConfig config = new Gson().fromJson(pushConfig.getConfig(), JPushConfig.class);
            JPushServer server = new JPushServer(config);
            String msgId = "";

            if (push.getTimeType() != null && push.getTimeType() == 1) {
                ScheduleMsgIdsResult scheduleMsgIdsResult = server.getScheduleMsgIds(push.getMsgId());
                if (scheduleMsgIdsResult != null &&  scheduleMsgIdsResult.getMsgids() != null) {

                }
            } else {
                msgId = push.getMsgId();
            }

            ReceivedsResult result = server.getReportReceiveds(msgId);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }

}
