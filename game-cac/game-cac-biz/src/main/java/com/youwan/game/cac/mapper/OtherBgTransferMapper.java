package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.OtherBgTransfer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-05 15:57:02
 */
public interface OtherBgTransferMapper extends BaseMapper<OtherBgTransfer> {

    IPage<List<OtherBgTransfer>> queryBgTransferPage(Page page,  @Param("userId") String userId, @Param("startTime") String startTime, @Param("endTime") String endTime);

}
