package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.SysPayPlatform;
import com.youwan.game.cac.api.entity.TChannel;

import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author code generator
 * @date 2019-02-08 18:30:36
 */
public interface TChannelService extends IService<TChannel> {


    /**
     * <B>添加渠道信息信息</B>
     * @param tChannel
     * @return
     */
    int saveChannel(TChannel tChannel);

    /**
     * <B>修改渠道信息</B>
     * @param tChannel
     * @return
     */
    int updateChannel(TChannel tChannel);


    TChannel getChannelInfo(String channelId);


    List<Map<String, Object>> getConfigGame(String channelId);


    IPage<List<TChannel>> queryChannelPage(Page page, String channelId, String channelName);
}
