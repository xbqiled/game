package com.youwan.game.cac.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.dto.SigninDto;
import com.youwan.game.cac.api.entity.Signinchannel;
import com.youwan.game.cac.service.SigninchannelService;
import com.youwan.game.common.core.constant.ConfigConstant;
import com.youwan.game.common.core.jsonmodel.GsonSignAwardCfg;
import com.youwan.game.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.*;


/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-05 13:49:00
 */
@RestController
@AllArgsConstructor
@RequestMapping("/signin")
public class SigninController {

  private final SigninchannelService signinchannelService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param queryDto VIEW
   * @return
   */
  @GetMapping("/page")
  public R getSignPage(Page page, QueryDto queryDto) {
    return new R<>(signinchannelService.querySignCfgPage(page, queryDto));
  }


  @GetMapping("/{channelId}")
  public R info(@PathVariable("channelId") String tChannel){
    Signinchannel signinchannel = signinchannelService.findByChannelId(tChannel);
    List<GsonSignAwardCfg> cfg = new ArrayList<>();

    String str = new String(signinchannel.getTCfginfo());
    if (StrUtil.isNotBlank(str)) {
      GsonSignAwardCfg[] array = new Gson().fromJson(str, GsonSignAwardCfg[].class);
      cfg = Arrays.asList(array);
    }

    String startTime = DateUtil.format(new Date(signinchannel.getTStarttime() * 1000), ConfigConstant.FULL_DATE_TIME_FORMAT);
    String endTime = DateUtil.format(new Date(signinchannel.getTEndtime() * 1000), ConfigConstant.FULL_DATE_TIME_FORMAT);
    Map<String , Object> map = new HashMap<String , Object>();

    map.put("signinchannel", signinchannel);
    map.put("cfg", cfg);
    map.put("startTime", startTime);

    map.put("endTime", endTime);
    return new R<>(map);
  }


  @PreAuthorize("@pms.hasPermission('cac_signin_add')")
  @RequestMapping("/doConfig")
  public R save(@RequestBody SigninDto signinDto){
    if (StrUtil.isEmpty(signinDto.getChannelId())) {
      return new R<>().setCode(1).setMsg("渠道编码不能为空!");
    }

    if (StrUtil.isEmpty(signinDto.getStartTime())) {
      return new R<>().setCode(1).setMsg("开始时间不能为空!");
    }

    if (StrUtil.isEmpty(signinDto.getEndTime())) {
      return new R<>().setCode(1).setMsg("结束时间不能为空!");
    }

    if (StrUtil.equals(signinDto.getStartTime(), signinDto.getEndTime())) {
      return new R<>().setCode(1).setMsg("活动开始时间与活动结束时间不能相同!");
    }

    int result = signinchannelService.saveSignCfg(signinDto.getChannelId(), signinDto.getStartTime(), signinDto.getEndTime(), 1,
            signinDto.getIsCirculate(), signinDto.getNum(), signinDto.getSignCfg());
    if (result == 0) {
      return new R<>();
    } else {
      return new R<>().setCode(1).setMsg("配置充值返利失败!");
    }
  }


}
