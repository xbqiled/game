package com.youwan.game.cac.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.BgRoundDto;
import com.youwan.game.cac.api.dto.BgTransferDto;
import com.youwan.game.cac.api.entity.OtherBgTransfer;
import com.youwan.game.cac.api.vo.BgRoundVo;
import com.youwan.game.cac.service.OtherBgTransferService;
import com.youwan.game.cac.service.OtherPlatformConfigService;
import com.youwan.game.common.core.otherplatform.PlatformConfig;
import com.youwan.game.common.core.otherplatform.bg.BGApi;
import com.youwan.game.common.core.otherplatform.bg.BGPlatfromConfig;
import com.youwan.game.common.core.otherplatform.bg.json.resp.UserRoundResp;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 
 *
 * @author
 * @date 2019-11-05 15:57:02
 */
@RestController
@AllArgsConstructor
@RequestMapping("/bground")
public class BgRoundController {

  private final OtherPlatformConfigService otherPlatformConfigService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param bgRoundDto
   * @return
   */
  @GetMapping("/page")
  public R getOtherBgTransferPage(Page page, BgRoundDto bgRoundDto) {
    if (StrUtil.isEmpty(bgRoundDto.getStartTime()) || StrUtil.isEmpty(bgRoundDto.getEndTime())) {
      page.setRecords(new ArrayList<BgRoundVo>());
    } else {
      PlatformConfig platformConfig = otherPlatformConfigService.getPlatformConfigByCode("bg");
      if (platformConfig instanceof BGPlatfromConfig) {
        //查询时间转换成美东时间

        BGPlatfromConfig config = (BGPlatfromConfig) platformConfig;
        UserRoundResp resp = BGApi.getUserRound(config, bgRoundDto.getRound(),
                channelMdTime(bgRoundDto.getStartTime()), channelMdTime(bgRoundDto.getEndTime()), page.getCurrent(), page.getSize());
        if (resp != null && resp.getResult() != null) {

          List<BgRoundVo> resultList = new ArrayList<>();
          page.setTotal(resp.getResult().getTotal());
          //美东时间转换成本地时间
          if (resp.getResult().getItems() != null && resp.getResult().getItems().size() > 0) {
            for (UserRoundResp.Result.RoundOrder round : resp.getResult().getItems()) {
              String openTime = channelBjTime(round.getOpenTime());

              BgRoundVo vo = new BgRoundVo();
              BeanUtil.copyProperties(round, vo);

              if (StrUtil.isNotEmpty(round.getCalcTime())) {
                vo.setCalcTime(channelBjTime(round.getCalcTime()));
              }

              vo.setOpenTime(openTime);
              resultList.add(vo);
            }
          }
          page.setRecords(resultList);
        } else {
          page.setRecords(new ArrayList<BgRoundVo>());
        }
      }
    }
    return new R<>(page);
  }

  /**
   * <B>转换成美东时间</B>
   * @param dataTime
   * @return
   */
  public String channelMdTime(String dataTime) {
    Date date = DateUtil.parse(dataTime, DatePattern.NORM_DATETIME_FORMAT);
    Date mdDate = DateUtil.offsetHour(date, -12);
    return DateUtil.format(mdDate, DatePattern.NORM_DATETIME_FORMAT);
  }

  public String channelBjTime(String dataTime) {
    Date date = DateUtil.parse(dataTime, DatePattern.NORM_DATETIME_FORMAT);
    Date mdDate = DateUtil.offsetHour(date, 12);
    return DateUtil.format(mdDate, DatePattern.NORM_DATETIME_FORMAT);
  }

}
