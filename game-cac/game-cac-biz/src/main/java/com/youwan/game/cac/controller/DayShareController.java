package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.DayShareEntity;
import com.youwan.game.cac.api.entity.DayShareForm;
import com.youwan.game.cac.api.vo.DayShareVo;
import com.youwan.game.cac.service.DayShareService;
import com.youwan.game.common.core.jsonmodel.RequestDayShare;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * VIEW
 * @email ismyjuliet@gmail.com
 * @date 2019-12-15 16:02:59
 */
@RestController
@AllArgsConstructor
@RequestMapping("/dayShare")
public class DayShareController {

    private DayShareService dayShareService;

    @RequestMapping("/page")
    public R page(Page page, QueryDto queryDto){
        IPage<DayShareVo> dataList = dayShareService.queryPage(page, queryDto);
        return new R<>(dataList);
    }

    @RequestMapping("/info/{channelId}")
    @PreAuthorize("@pms.hasPermission('cac_dayShare_view')")
    public R info(@PathVariable("channelId") String channelId){
        DayShareEntity dayShareEntity = dayShareService.findByChannelId(channelId);
        String jsonStr = new String(dayShareEntity.getTDayshareinfo());
        RequestDayShare requestDayShare  = new Gson().fromJson(jsonStr, RequestDayShare.class);
        if (requestDayShare != null) {
            requestDayShare.setChannel(Long.parseLong(dayShareEntity.getTChannel()));
            return new R<>(requestDayShare);
        }
        return  null;
    }


    @SysLog("每日分享信息保存")
    @RequestMapping("/doConfig")
    @PreAuthorize("@pms.hasPermission('cac_dayShare_config')")
    public R doConfig(@RequestBody DayShareForm dayShareForm){
        int result = dayShareService.saveOrUpdateConfig(dayShareForm);
        if (result == 0) {
            return new R<>();
        } else {
            return new R<>(null,"配置每日分享失败!");
        }
    }

}
