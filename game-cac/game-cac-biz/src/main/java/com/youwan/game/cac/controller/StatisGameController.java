package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.ReportDto;
import com.youwan.game.cac.api.vo.StatisGameVo;
import com.youwan.game.cac.service.StatisGameService;
import com.youwan.game.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author code generator
 * @date 2020-03-07 15:37:57
 */
@RestController
@AllArgsConstructor
@RequestMapping("/statisgame")
public class StatisGameController {

  private final StatisGameService statisGameService;
  /**
   * 列表
   */
  @RequestMapping("/gameList")
  @PreAuthorize("@pms.hasPermission('cac:statisgame:list')")
  public R statisGameList(Page page, ReportDto reportDto){
    IPage<List<StatisGameVo>> resultList = statisGameService.queryStatisGamePage(page, reportDto);
    return new R<>(resultList);
  }

  @RequestMapping("/usergameList")
  @PreAuthorize("@pms.hasPermission('cac:statisusergame:list')")
  public R statisUserGameList(Page page, ReportDto reportDto){
    IPage<List<StatisGameVo>> resultList = statisGameService.queryStatisUserGamePage(page, reportDto);
    return new R<>(resultList);
  }

  @RequestMapping("/allGame")
  public R gamelist() {
    List<Map<String, String>> list = statisGameService.queryGameList();
    return new R<>(list);
  }
}
