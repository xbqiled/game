package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.Domain;
import com.youwan.game.cac.api.entity.SourceUrl;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 *
 * @author
 * @date 2019-04-19 13:18:35
 */
public interface SourceUrlMapper extends BaseMapper<SourceUrl> {

    SourceUrl getJumpUrl(@Param("url")String url);

    SourceUrl getBySerial(@Param("serialNum")String serialNum);

    Integer delBySourceUrl(@Param("sourceUrl")String sourceUrl);

    IPage<List<SourceUrl>> querySourceUrlPage(Page page, @Param("shortUrl") String shortUrl, @Param("channelId") String channelId, @Param("status") Integer status);
}
