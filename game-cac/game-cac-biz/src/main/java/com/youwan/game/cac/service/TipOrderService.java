package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.TipOrder;


/**
 * 
 *
 * @author code generator
 * @date 2019-05-21 11:20:06
 */
public interface TipOrderService extends IService<TipOrder> {

}
