package com.youwan.game.cac.mapper.xbqp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.Gameglobalextinfo;
import com.youwan.game.cac.api.vo.GameglobalextinfoVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * VIEW
 *
 * @author code generator
 * @date 2020-05-20 16:19:40
 */
public interface GameglobalextinfoMapper extends BaseMapper<Gameglobalextinfo> {

    List<GameglobalextinfoVo> getWxhhExpList(@Param("startTime") String startTime, @Param("endTime") String endTime);

}
