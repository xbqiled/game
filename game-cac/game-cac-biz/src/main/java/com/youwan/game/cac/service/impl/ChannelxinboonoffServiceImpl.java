package com.youwan.game.cac.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.XinBoSwitchDto;
import com.youwan.game.cac.api.entity.Channelxinboonoff;
import com.youwan.game.cac.api.vo.XinBoSwitchVo;
import com.youwan.game.cac.mapper.xbqp.ChannelxinboonoffMapper;
import com.youwan.game.cac.service.ChannelxinboonoffService;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.jsonmodel.GsonSwitch;
import com.youwan.game.common.core.util.HttpHandler;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-12-15 13:50:26
 */
@Service("xinboonoffService")
public class ChannelxinboonoffServiceImpl extends ServiceImpl<ChannelxinboonoffMapper, Channelxinboonoff> implements ChannelxinboonoffService {


    @Override
    public List<XinBoSwitchVo> selectAllSwitch(XinBoSwitchDto xinBoSwitchDto) {
        List<XinBoSwitchVo> list = baseMapper.selectAllSwitch();
        List<XinBoSwitchVo> allList = new ArrayList<>();

        if (CollUtil.isNotEmpty(list)) {
            for (XinBoSwitchVo vo : list) {
                if (vo.getOnOffInfo() != null) {

                    //转化成String
                    String cfg = new String(vo.getOnOffInfo());
                    if (StrUtil.isNotBlank(cfg)) {
                        GsonSwitch[] gsonSwitchArr = new Gson().fromJson(cfg, GsonSwitch[].class);
                        if (gsonSwitchArr != null && gsonSwitchArr.length > 0) {

                            for (GsonSwitch gsonSwitch : gsonSwitchArr) {
                                if (xinBoSwitchDto.getType() == gsonSwitch.getFunctionType()) {
                                    vo.setFunctionType(gsonSwitch.getFunctionType());
                                    vo.setOpenTypeBoolean(gsonSwitch.getOpenType() == 1 ? Boolean.TRUE : Boolean.FALSE);
                                }
                            }
                        }
                    }
                } else {
                    vo.setOpenTypeBoolean(Boolean.FALSE);
                }
                allList.add(vo);
            }
        }
        return allList;
    }


    @Override
    public Integer doConfig(XinBoSwitchDto xinBoSwitchDto) {
        GsonResult result = HttpHandler.cfgXinboSw(xinBoSwitchDto.getCfgList(), xinBoSwitchDto.getChannelId());
        if (result != null) {
            return result.getRet();
        }
        return 1;
    }
}
