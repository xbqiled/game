package com.youwan.game.cac.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.BgDamaGameDto;
import com.youwan.game.cac.api.dto.BgExitGameDto;
import com.youwan.game.cac.api.dto.BgUserDto;
import com.youwan.game.cac.api.dto.ChannelBgUserStatusDto;
import com.youwan.game.cac.api.entity.*;
import com.youwan.game.cac.mapper.OtherBgUserMapper;
import com.youwan.game.cac.mapper.OtherChannelAccountMapper;
import com.youwan.game.cac.mapper.xbqp.AccountinfoMapper;
import com.youwan.game.cac.service.BgenterConfigService;
import com.youwan.game.cac.service.OtherBgTransferService;
import com.youwan.game.cac.service.OtherBgUserService;
import com.youwan.game.cac.service.OtherPlatformConfigService;
import com.youwan.game.common.core.jsonmodel.GsonOutGameOp;
import com.youwan.game.common.core.jsonmodel.GsonUserBalance;
import com.youwan.game.common.core.otherplatform.PlatformConfig;
import com.youwan.game.common.core.otherplatform.bg.BGApi;
import com.youwan.game.common.core.otherplatform.bg.BGPlatfromConfig;
import com.youwan.game.common.core.otherplatform.bg.json.resp.*;
import com.youwan.game.common.core.util.HttpHandler;
import com.youwan.game.common.core.util.MD5;
import com.youwan.game.common.core.util.OrderIdUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author code generator
 * @date 2019-11-05 15:57:07
 */
@Slf4j
@AllArgsConstructor
@Service("otherBgUserService")
public class OtherBgUserServiceImpl extends ServiceImpl<OtherBgUserMapper, OtherBgUser> implements OtherBgUserService {

    private final OtherPlatformConfigService otherPlatformConfigService;

    private final OtherBgTransferService otherBgTransferService;

    private final BgenterConfigService bgenterConfigService;

    private final OtherChannelAccountMapper otherChannelAccountMapper;

    private final AccountinfoMapper accountinfoMapper;

    private static final Long pageIndex =  new Long(1);

    private static final Long pageSize =  new Long(10000);

    @Override
    public OtherBgUser findUser(String channelId, String userId) {
        return baseMapper.getBgUser(channelId, userId);
    }

    @Override
    public IPage<List<OtherBgUser>> queryBgUserPage(Page page, BgUserDto bgUserDto) {
        return baseMapper.queryBgUserPage(page, bgUserDto.getUserId(), bgUserDto.getChannelId(), bgUserDto.getNickName());
    }

    @Override
    public UserDetailsResp getUserDetails(String userId) {
        PlatformConfig platformConfig = otherPlatformConfigService.getPlatformConfigByCode("bg");
        if (platformConfig instanceof BGPlatfromConfig) {

            BGPlatfromConfig config = (BGPlatfromConfig)platformConfig;
            UserDetailsResp userDetailsResp = BGApi.getUserDetails(config, getPrefixName(config.getPrefix(), userId));
            if (userDetailsResp != null && userDetailsResp.getError() == null) {
                return userDetailsResp;
            }
        }
        return null;
    }

    @Override
    public OtherBgUser createUser(String channelId, String userId, String nickName) {
        //创建用户
        PlatformConfig platformConfig = otherPlatformConfigService.getPlatformConfigByCode("bg");
        if (platformConfig instanceof BGPlatfromConfig) {
            BGPlatfromConfig config = (BGPlatfromConfig)platformConfig;

            //判断是否存在用户
            OtherBgUser user = baseMapper.getBgUser(channelId, userId);
            if (user != null) {
                return user;
            }

            //通信创建用户
            CreateUserResp userResp = BGApi.createUser(config, getPrefixName(config.getPrefix(), userId), userId);
            if (userResp != null && userResp.getResult().getSuccess() == Boolean.TRUE) {
                OtherBgUser otherBgUser = packageOtherBgUser(config.getAgentAccount(), userResp.getResult().getNickname(), userResp.getResult().getUserId(), channelId,
                nickName, userResp.getResult().getRegType(), Integer.valueOf(userId), getPrefixName(config.getPrefix(), userId), config.getSn());
                this.save(otherBgUser);
                return otherBgUser;
            } else {
                return null;
            }
        }
        return null;
    }

    @Override
    public BgDamaGameDto exitGame(BgExitGameDto gExitGameDto) {
        //判断是否有起止时间
        try {
            PlatformConfig platformConfig = otherPlatformConfigService.getPlatformConfigByCode("bg");
            if (platformConfig instanceof BGPlatfromConfig) {
                BGPlatfromConfig config = (BGPlatfromConfig) platformConfig;

                AgentQueryResp order = BGApi.agentQuery(config, getPrefixName(config.getPrefix(), gExitGameDto.getUserId()),
                        channelMdTime(gExitGameDto.getStartTime() * 1000),  channelMdTime(gExitGameDto.getEndTime() * 1000),
                        pageIndex, pageSize, null , null, null, null);
                Float validBet = 0f;

                if (CollUtil.isNotEmpty(order.getResult().getItems())) {
                    for (AgentQueryResp.OrderData.BgBetOrder bgBetOrder : order.getResult().getItems()) {
                        validBet += bgBetOrder.getValidBet();
                    }
                    return new BgDamaGameDto(gExitGameDto.getUserId(), gExitGameDto.getStartTime(), gExitGameDto.getEndTime(), (int)(validBet * 100));
                } else {
                    return new BgDamaGameDto(gExitGameDto.getUserId(), gExitGameDto.getStartTime(), gExitGameDto.getEndTime(), 0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public String openGame(Integer isWx, Integer isMobileUrl, Integer isHttpsUrl, String channelId, String userId, String sysBalance, String password) {
        PlatformConfig platformConfig = otherPlatformConfigService.getPlatformConfigByCode("bg");
        BgenterConfig bgenterConfig = bgenterConfigService.getConfigByChannelId(channelId);

        //判断是否为微信
        if (isWx == 0) {
            Accountinfo userInfo = accountinfoMapper.getUserInfo(Integer.parseInt(userId));
            if (userInfo != null && ObjectUtil.isNotNull(userInfo.getTAccountid()) && StrUtil.isNotEmpty(userInfo.getTAccountkey())) {
                String pw = MD5.encryption(password);
                if (!StrUtil.equals(pw, userInfo.getTAccountkey())) {
                    return "1";
                }
            } else {
                return "2";
            }
        }

        //判断密码是否正确
       if (platformConfig instanceof BGPlatfromConfig) {
           BGPlatfromConfig config = (BGPlatfromConfig) platformConfig;
           if (bgenterConfig != null && ! bgenterConfig.getEnterBalance().equals(BigDecimal.ZERO)) {
               //获取bg余额度
               UserBalanceResp userBalanceResp = BGApi.userBalance(config, getPrefixName(config.getPrefix(), userId));
               Float userBalance = null;
               if (StrUtil.isNotEmpty(sysBalance)) {
                   userBalance = Float.valueOf(userBalanceResp.getResult()) + Float.valueOf(sysBalance);
               } else {
                   userBalance = Float.valueOf(userBalanceResp.getResult());
               }

               if (userBalance != null) {
                   if ((new BigDecimal(userBalance)).compareTo(bgenterConfig.getEnterBalance()) == -1) {
                       return "3";
                   }
               } else {
                   return "4";
               }
           }

           OpenGameResp openGameResp = BGApi.openGame(config, channelId, getPrefixName(config.getPrefix(), userId), isMobileUrl, isHttpsUrl);
           if (openGameResp != null && StrUtil.isNotEmpty(openGameResp.getResult())) {
               return openGameResp.getResult();
           }
       }

       return "4";
    }


//    @Override
//    public String openGame(Integer isMobileUrl, Integer isHttpsUrl, String channelId, String userId, String sysBalance, String password) {
//        PlatformConfig platformConfig = otherPlatformConfigService.getPlatformConfigByCode("bg");
//        BgenterConfig bgenterConfig = bgenterConfigService.getConfigByChannelId(channelId);
//        TAccountinfo userInfo = accountinfoMapper.getUserInfo(userId);
//
//        //判断密码是否正确
//        if (userInfo != null && ObjectUtil.isNotNull(userInfo.getTAccountid()) && StrUtil.isNotEmpty(userInfo.getTAccountkey())) {
//            String pw = MD5.encryption(password);
//            if (StrUtil.equals(pw, userInfo.getTAccountkey())) {
//                if (platformConfig instanceof BGPlatfromConfig) {
//
//                    BGPlatfromConfig config = (BGPlatfromConfig) platformConfig;
//                    if (bgenterConfig != null && ! bgenterConfig.getEnterBalance().equals(BigDecimal.ZERO)) {
//                        //获取bg余额度
//                        UserBalanceResp userBalanceResp = BGApi.userBalance(config, getPrefixName(config.getPrefix(), userId));
//                        Float userBalance = null;
//                        if (StrUtil.isNotEmpty(sysBalance)) {
//                            userBalance = Float.valueOf(userBalanceResp.getResult()) + Float.valueOf(sysBalance);
//                        } else {
//                            userBalance = Float.valueOf(userBalanceResp.getResult());
//                        }
//
//                        if (userBalance != null) {
//                            if (bgenterConfig.getEnterBalance().compareTo(new BigDecimal(userBalance)) == 1) {
//                                return "1";
//                            }
//                        }
//                    }
//
//                    OpenGameResp openGameResp = BGApi.openGame(config, channelId, getPrefixName(config.getPrefix(), userId), isMobileUrl, isHttpsUrl);
//                    if (openGameResp != null && StrUtil.isNotEmpty(openGameResp.getResult())) {
//                        return openGameResp.getResult();
//                    }
//                }
//            }
//        }
//        return "2";
//    }


    @Override
    public Map<String, BigDecimal> getUserBalance(String userId) {
        PlatformConfig platformConfig = otherPlatformConfigService.getPlatformConfigByCode("bg");
        Map<String, BigDecimal> resultMap = new HashMap<>();

        if (platformConfig instanceof BGPlatfromConfig) {
            BGPlatfromConfig config = (BGPlatfromConfig)platformConfig;

            UserBalanceResp userBalanceResp = BGApi.userBalance(config, getPrefixName(config.getPrefix(), userId));
            GsonUserBalance userBalance = HttpHandler.getUserBalance(1, Integer.valueOf(userId));
            if (userBalanceResp != null) {
                resultMap.put("bg",  new BigDecimal(userBalanceResp.getResult()));
            }

            if (userBalance != null) {
                Double balance = (double)userBalance.getCurCoin()/100;
                DecimalFormat df =new DecimalFormat("#.00");
                String str = df.format(balance);
                resultMap.put("sys",  new BigDecimal(str));
            }
            return resultMap;
        }
        return null;
    }

    @Override
    public BigDecimal getSysBalance(String userId) {
        GsonUserBalance userBalance = HttpHandler.getUserBalance(1, Integer.valueOf(userId));
        Integer balance = userBalance.getCurCoin() ;

        DecimalFormat df =new DecimalFormat("#.00");
        String str = df.format(balance/100);
        BigDecimal bd = new BigDecimal(str);

        return bd;
    }

    @Override
    //@Transactional(rollbackFor = Exception.class)
    public Integer transferbalance(String userId, String amount, Integer type, String channelId, Integer reason) {
        try {
            PlatformConfig platformConfig = otherPlatformConfigService.getPlatformConfigByCode("bg");
            if (platformConfig instanceof BGPlatfromConfig) {
                String bgAmount = amount;
                String retrunAmount = amount;

                BGPlatfromConfig config = (BGPlatfromConfig) platformConfig;
                Long orderId = OrderIdUtil.getOrdereId();
                int transferbaAmount = 0;

                //转入BG
                if (ObjectUtil.isNotNull(type) && type == 1) {
                    ChannelAccount channelAccount = otherChannelAccountMapper.getByChannelId(channelId);
                    if (channelAccount.getVideoBalance().compareTo(new BigDecimal(amount)) == -1) {
                        return 3;
                    }
                    BigDecimal value = new BigDecimal(100);
                    transferbaAmount = value.multiply(new BigDecimal(amount)).intValue();
                }

                //转出BG
                if (ObjectUtil.isNotNull(type) && type == 2) {
                    //获取用户最大余
                    UserBalanceResp userBalanceResp = BGApi.userBalance(config, getPrefixName(config.getPrefix(), userId));
                    if (userBalanceResp != null && userBalanceResp.getResult() != null) {
                        Float bgBalance = new Float(userBalanceResp.getResult());
                        if (new Float(userBalanceResp.getResult()) > 0) {
                            if (bgBalance > 3000000) {
                                bgAmount = "-3000000";
                                transferbaAmount = (3000000 * 100);
                                retrunAmount = "3000000";
                            } else {
                                bgAmount = "-" + userBalanceResp.getResult();
                                transferbaAmount = (int) (bgBalance * 100);
                                retrunAmount = userBalanceResp.getResult();
                            }
                        } else {
                            return 4;
                        }
                    }
                }

                //判断额度是否够用
                GsonOutGameOp result = HttpHandler.opGameCoin(orderId.toString(), reason, 1, type, transferbaAmount, Integer.valueOf(userId));
                if (result != null && result.getRet() == 0) {
                    UserTransferResp userTransferResp = BGApi.userBalanceTransfer(config, getPrefixName(config.getPrefix(), userId), bgAmount, orderId.toString());

                    //正数表示从系统到BG, 负数表示从BG到系统
                    if (userTransferResp != null && userTransferResp.getResult() >= 0 && (userTransferResp.getError() == null || userTransferResp.getError().size() == 0)) {
                        //扣除额度信息
                        if (ObjectUtil.isNotNull(type) && type == 2) { //转出应该给代理商加钱
                            otherChannelAccountMapper.updateIncreaseBgBalance(channelId, new BigDecimal(retrunAmount));
                        } else {   //转入给代理商减钱
                            otherChannelAccountMapper.updateReduceBgBalance(channelId, new BigDecimal(amount));
                        }

                        //判断是否成功，然后从系统扣款
                        OtherBgTransfer otherBgTransfer = packageOtherBgTransfer(retrunAmount, userTransferResp.getResult(), result.getLeftCoin(), orderId, userId, type, config.getSn(), channelId);
                        otherBgTransferService.save(otherBgTransfer);
                        return 0;
                    } else {
                        HttpHandler.cancelOpGameCoin(orderId.toString(), 1, type, transferbaAmount, Integer.valueOf(userId));
                    }
                }
                return 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 2;
    }

    @Override
    public BGPlatfromConfig getBgConfig() {
        PlatformConfig platformConfig = otherPlatformConfigService.getPlatformConfigByCode("bg");
        BGPlatfromConfig config = (BGPlatfromConfig)platformConfig;
        return config;
    }

    @Override
    public Boolean changeStatus(ChannelBgUserStatusDto channelBgUserStatusDto) {
        PlatformConfig platformConfig = otherPlatformConfigService.getPlatformConfigByCode("bg");
        if (platformConfig instanceof BGPlatfromConfig) {
            BGPlatfromConfig config = (BGPlatfromConfig)platformConfig;
            String statust = "";

            if (Integer.parseInt(channelBgUserStatusDto.getStatus()) == 1) {
                UserDisableResp userDisableResp = BGApi.userDisable(config, getPrefixName(config.getPrefix(), channelBgUserStatusDto.getUserId()));
                if (userDisableResp != null && userDisableResp.getError() == null) {
                    statust = "4";
                }
            } else {
                UserEnableResp userEnableResp = BGApi.userEnable(config, getPrefixName(config.getPrefix(), channelBgUserStatusDto.getUserId()));
                if (userEnableResp != null  && userEnableResp.getError() == null) {
                    statust = "1";
                }
            }

            baseMapper.changeStatus(statust, channelBgUserStatusDto.getUserId());
        }
        return Boolean.TRUE;
    }



    public OtherBgTransfer packageOtherBgTransfer(String amount, Float result, Long leftCoin, Long orderId, String userId, Integer type, String sn, String channelId) {
        OtherBgTransfer otherBgTransfer  = new OtherBgTransfer();
        otherBgTransfer.setAmount(new BigDecimal(amount));

        otherBgTransfer.setBgBalance(new BigDecimal(result));
        Double sysBalanece = (double) leftCoin/100;

        DecimalFormat df = new DecimalFormat("0.00");
        String sb = df.format(sysBalanece);
        otherBgTransfer.setSysBalance(new BigDecimal(sb));

        otherBgTransfer.setBizId(orderId);
        otherBgTransfer.setUserId(userId);
        otherBgTransfer.setChannelId(channelId);

        otherBgTransfer.setAccountItem(type);
        otherBgTransfer.setSn(sn);
        otherBgTransfer.setStatus(1); //1成功。2失败

        otherBgTransfer.setOperateTime(new Date());
        return  otherBgTransfer;
    }



    public OtherBgUser packageOtherBgUser (String agentId, String bgNickName, Long bgUserId, String channelId, String nickName, String regType, Integer userId, String prefixUserId, String sn) {
        OtherBgUser otherBgUser = new OtherBgUser();
        otherBgUser.setPrefixUserId(prefixUserId);
        otherBgUser.setAgentId(agentId);

        otherBgUser.setBgNickname(bgNickName);
        otherBgUser.setBgUserId(bgUserId);
        otherBgUser.setChannelId(channelId);

        otherBgUser.setNickName(nickName);
        otherBgUser.setRegType(regType);
        otherBgUser.setCreateTime(LocalDateTime.now());

        otherBgUser.setUserId(userId);
        otherBgUser.setStatus(1);
        otherBgUser.setSn(sn);
        return  otherBgUser;
    }


    public Date channelMdTime(Long dataTime) {
        Date date = new Date(dataTime);
        return DateUtil.offsetHour(date, -12);
    }



    public String getPrefixName(String prefix, String userId) {
        return prefix + userId;
    }

}
