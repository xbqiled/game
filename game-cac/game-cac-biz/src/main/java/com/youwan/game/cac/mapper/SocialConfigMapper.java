package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.SocialConfig;
import org.apache.ibatis.annotations.Param;

/**
 * 
 *
 * @author code generator
 * @date 2019-03-26 15:43:34
 */
public interface SocialConfigMapper extends BaseMapper<SocialConfig> {

    SocialConfig getSocialConfigById(@Param("channelId") String channelId);

}
