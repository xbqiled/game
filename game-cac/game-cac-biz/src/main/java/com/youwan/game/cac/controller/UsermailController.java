package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.MailDto;
import com.youwan.game.cac.api.dto.SendMailDto;
import com.youwan.game.cac.api.entity.Usermail;
import com.youwan.game.cac.api.vo.MailVo;
import com.youwan.game.cac.service.UsermailService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author code generator
 * @date 2018-12-29 12:31:06
 */
@RestController
@AllArgsConstructor
@RequestMapping("/usermail")
public class UsermailController {

  private final UsermailService usermailService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param mailDto
   * @return
   */
  @GetMapping("/page")
  public R getUsermailPage(Page page, MailDto mailDto) {
    IPage<List<Map<String, Object>>> resultPage = usermailService.queryUserMailPage(page, mailDto);
    return  new R<>(resultPage);
  }


  /**
   * 通过id查询
   * @param id
   * @return R
   */
  @GetMapping("/findById")
  @PreAuthorize("@pms.hasPermission('cac_mail_view')")
  public R findById(String id, String userId) {
    MailVo usermail = usermailService.findById(Integer.parseInt(id), Integer.parseInt(userId));
    String content = new String(usermail.getTContent());
    usermail.setContent(content);
    return new R<>(usermail);
  }

  /**
   * 新增
   * @param sendMailDto
   * @return R
   */
  @SysLog("发送邮件")
  @PostMapping
  @PreAuthorize("@pms.hasPermission('cac_mail_add')")
  public R save(@RequestBody SendMailDto sendMailDto){
    int result = usermailService.sendMail(sendMailDto);
    if (result == 0) {
      return  new R<>();
    } else if (result == 1) {
      return new R<>().setCode(1).setMsg("用户ID格式错误!");
    } else if (result == 2) {
      return new R<>().setCode(2).setMsg("渠道ID格式错误!");
    } else if (result == 3) {
      return new R<>().setCode(3).setMsg("发送邮件信息失败!");
    } else {
      return  new R<>();
    }
  }

  /**
   * 修改
   * @param usermail
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody Usermail usermail){
    return new R<>(usermailService.updateById(usermail));
  }

  /**
   * 通过id删除
   * @param  id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(usermailService.removeById(id));
  }

}
