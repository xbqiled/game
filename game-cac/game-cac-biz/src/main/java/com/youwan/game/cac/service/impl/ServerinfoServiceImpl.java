package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.Serverinfo;
import com.youwan.game.cac.mapper.ServerinfoMapper;
import com.youwan.game.cac.service.ServerinfoService;
import org.springframework.stereotype.Service;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-29 12:32:01
 */
@Service("serverinfoService")
public class ServerinfoServiceImpl extends ServiceImpl<ServerinfoMapper, Serverinfo> implements ServerinfoService {

}
