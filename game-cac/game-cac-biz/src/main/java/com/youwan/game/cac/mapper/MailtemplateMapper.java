package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.Mailtemplate;

/**
 * 
 *
 * @author code generator
 * @date 2019-03-19 13:02:27
 */
public interface MailtemplateMapper extends BaseMapper<Mailtemplate> {

}
