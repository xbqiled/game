package com.youwan.game.cac.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.InviteDeposit;
import com.youwan.game.cac.api.vo.InviteDepositChannelVo;
import com.youwan.game.cac.mapper.xbqp.InviteDepositMapper;
import com.youwan.game.cac.service.InviteDepositService;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.HttpHandler;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-08-03 15:31:04
 */
@Service("inviteDepositService")
public class InviteDepositServiceImpl extends ServiceImpl<InviteDepositMapper, InviteDeposit> implements InviteDepositService {


    @Override
    public InviteDepositChannelVo findByChannelId(String channelId) {
        return baseMapper.getInviteDepositByChannelId(channelId);
    }

    @Override
    public int saveInviteDepositCfg(String channelId, Long openType, Long timeType, Long beginTime, Long endTime,
                                    Long firstTotalDeposit, Long selfReward, Long upperReward) {
        Long start = new Long(0);
        Long finish = new Long(0);

        if (timeType == 1) {
            start = new Long(beginTime) / 1000;
            finish = new Long(endTime) / 1000;
        }

        GsonResult result = HttpHandler.inviteDeposit(channelId, openType, timeType, start, finish, firstTotalDeposit, selfReward, upperReward);
        if (result != null) {
            return result.getRet();
        }
        return 1;
    }

    @Override
    public int channelOpenType(String channelId, String openType) {
        InviteDepositChannelVo model  = baseMapper.getInviteDepositByChannelId(channelId);
        if (model != null && StrUtil.isNotEmpty(model.getChannelId())) {

            Integer type = model.getDepositType() == 1 ? 0 : 1;
            GsonResult result = HttpHandler.inviteDeposit(channelId, Long.valueOf(type), model.getTimeType(), model.getBeginTime(), model.getEndTime(),
                    model.getFirstTotalDeposit(), model.getSelfReward(), model.getUpperReward());
            if (result != null) {
                return result.getRet();
            }
        }
        return 1;
    }

    @Override
    public IPage<List<InviteDepositChannelVo>> queryInviteDepositPage(Page page, QueryDto queryDto) {
        return this.baseMapper.queryInviteDepositPage(page, queryDto.getChannelId(), queryDto.getStartTime(), queryDto.getEndTime());
    }
}
