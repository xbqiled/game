package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.EnterpriseConfig;
import com.youwan.game.cac.mapper.EnterpriseConfigMapper;
import com.youwan.game.cac.service.EnterpriseConfigService;
import org.springframework.stereotype.Service;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-06 13:19:25
 */
@Service("wxEnterpriseConfigService")
public class EnterpriseConfigServiceImpl extends ServiceImpl<EnterpriseConfigMapper, EnterpriseConfig> implements EnterpriseConfigService {

}
