package com.youwan.game.cac.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.DeviceInfoDto;
import com.youwan.game.cac.api.dto.VerifyDeviceInfo;
import com.youwan.game.cac.api.entity.DeviceInfo;
import com.youwan.game.cac.mapper.DeviceInfoMapper;
import com.youwan.game.cac.service.DeviceInfoService;
import com.youwan.game.common.core.jsonmodel.GsonDeviceInfo;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2019-05-15 15:24:30
 */
@Service("deviceInfoService")
public class DeviceInfoServiceImpl extends ServiceImpl<DeviceInfoMapper, DeviceInfo> implements DeviceInfoService {

    @Override
    public IPage<List<DeviceInfo>> getDeviceInfoPage(Page page, DeviceInfo deviceInfo) {
        return baseMapper.getDeviceInfoPage(page, deviceInfo.getChannelId(), deviceInfo.getIp(),  deviceInfo.getOs(), deviceInfo.getPromoterId());
    }

    @Override
    public Boolean addDeviceInfo(DeviceInfoDto deviceInfoDto) {
        StringBuffer sb = new StringBuffer();
        DeviceInfo info = new DeviceInfo();
        info.setChannelId(deviceInfoDto.getChannelId());

        info.setCompareCount(0);
        info.setCreateTime(LocalDateTime.now());
        info.setDeviceInfo(deviceInfoDto.getDeviceInfo());

        info.setIp(deviceInfoDto.getIp());
        info.setOs(deviceInfoDto.getOs());
        info.setStatus(1);
        info.setPromoterId(deviceInfoDto.getPromoterId());
        info.setParaInfo(sb.append("channleId=").append(deviceInfoDto.getChannelId()).append("&promoterId=").append(deviceInfoDto.getPromoterId()).toString());

        this.save(info);
        return Boolean.TRUE;
    }

    /**
     * <B>获取设备信息并验证</B>
     * @param deviceInfoDto
     * @return
     */
    @Override
    public Map<String, String> verifyDeviceInfo(VerifyDeviceInfo deviceInfoDto) {
        List<DeviceInfo> resultList = baseMapper.getDeviceList(deviceInfoDto.getChannelId(), deviceInfoDto.getIp(), deviceInfoDto.getOs());
        if (resultList != null && resultList.size() > 0) {
            //对比验证
            DeviceInfo info = resultList.get(0);

            //获取信息
            if (StrUtil.isNotBlank(info.getDeviceInfo())) {
                GsonDeviceInfo deviceInfo = new Gson().fromJson(info.getDeviceInfo(), GsonDeviceInfo.class);

                if (deviceInfo.getColorDepth().equals(deviceInfoDto.getColorDepth()) &&
                    //deviceInfo.getDeviceMemory().equals(deviceInfoDto.getDeviceMemory()) &&
                    deviceInfo.getDeviceType().equals(deviceInfoDto.getDeviceType()) &&
                    deviceInfo.getOsVersion().equals(deviceInfoDto.getOsVersion()) &&
                    deviceInfo.getHardwareConcurrency().equals(deviceInfoDto.getHardwareConcurrency()) &&
                    //deviceInfo.getLanguage().equals(deviceInfoDto.getLanguage()) &&
                    //deviceInfo.getNetWork().equals(deviceInfoDto.getNetWork()) &&
                    deviceInfo.getScreenHeight().equals(deviceInfoDto.getScreenHeight()) &&
                    deviceInfo.getScreenWidth().equals(deviceInfoDto.getScreenWidth()) &&
                    deviceInfo.getUserAgent().indexOf(deviceInfoDto.getDeviceModel()) > 0) {

                    Map<String, String> map = new HashMap<>();
                    map.put("promoterId", info.getPromoterId());
                    map.put("paraInfo", info.getParaInfo());
                    return map;

                } else {
                    return null;
                }
            }
        }
        return null;
    }
}
