package com.youwan.game.cac.controller;

import com.youwan.game.cac.api.dto.BettingRecordQueryDto;
import com.youwan.game.cac.service.BettingRecordQuery;
import com.youwan.game.common.core.betting.entity.*;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.data.core.Page;
import com.youwan.game.common.security.annotation.Inner;
import org.springframework.web.bind.annotation.*;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-28.
 */
@RestController
@RequestMapping("/bettingRecord")
public class BettingRecordController {

    private BettingRecordQuery bettingRecordQuery;

    public BettingRecordController(BettingRecordQuery bettingRecordQuery) {
        this.bettingRecordQuery = bettingRecordQuery;
    }

    @PostMapping("/lotteries/{userId}")
    @Inner
    public R<Page<BcLotteryOrder>> queryLotteryRecord(@PathVariable("userId") Integer userId,
                                                      @RequestBody BettingRecordQueryDto queryDto) {
        queryDto.setUserId(userId);
        return new R<>(bettingRecordQuery.queryLotteryRecord(queryDto));
    }

    @PostMapping("/egame/{userId}")
    @Inner
    public R<Page<ThirdPartyEGameBettingRecord>> queryEGameRecord(@PathVariable("userId") Integer userId,
                                                                  @RequestBody BettingRecordQueryDto queryDto) {
        queryDto.setUserId(userId);
        return new R<>(bettingRecordQuery.queryEGameRecord(queryDto));
    }

    @PostMapping("/live/{userId}")
    @Inner
    public R<Page<ThirdPartyLiveBettingRecord>> queryLiveRecord(@PathVariable("userId") Integer userId,
                                                                @RequestBody BettingRecordQueryDto queryDto) {
        queryDto.setUserId(userId);
        return new R<>(bettingRecordQuery.queryLiveRecord(queryDto));
    }

    @PostMapping("/3rdsport/{userId}")
    @Inner
    public R<Page<ThirdPartySportsBettingRecord>> queryTPSportRecord(@PathVariable("userId") Integer userId,
                                                                     @RequestBody BettingRecordQueryDto queryDto) {
        queryDto.setUserId(userId);
        return new R<>(bettingRecordQuery.queryTPSportRecord(queryDto));
    }

    @PostMapping("/crownsport/{userId}")
    @Inner
    public R<Page<CrownSportBettingOrder>> queryCrownSportRecord(@PathVariable("userId") Integer userId,
                                                              @RequestBody BettingRecordQueryDto queryDto) {
        queryDto.setUserId(userId);
        return new R<>(bettingRecordQuery.queryCrownSportRecord(queryDto));
    }
}
