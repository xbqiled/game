package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.SysPayPlatform;
import com.youwan.game.cac.service.SysPayPlatformService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2018-12-27 16:32:02
 */
@RestController
@AllArgsConstructor
@RequestMapping("/syspayplatform")
public class SysPayPlatformController {

  private final SysPayPlatformService sysPayPlatformService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param company
   * @return
   */
  @GetMapping("/page")
  public R getSysPayPlatformPage(Page page, String company) {
    return  new R<>(sysPayPlatformService.queryPayPlatformPage(page, company));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(sysPayPlatformService.getById(id));
  }

  /**
   * 新增
   * @param sysPayPlatform 
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody SysPayPlatform sysPayPlatform){
    return new R<>(sysPayPlatformService.savePlatform(sysPayPlatform));
  }

  /**
   * 修改
   * @param sysPayPlatform 
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody SysPayPlatform sysPayPlatform){
    return new R<>(sysPayPlatformService.updatePlatformById(sysPayPlatform));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(sysPayPlatformService.deletePlatformById(id));
  }

}
