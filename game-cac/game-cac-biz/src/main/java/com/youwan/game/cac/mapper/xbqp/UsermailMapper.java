package com.youwan.game.cac.mapper.xbqp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.Usermail;
import com.youwan.game.cac.api.vo.MailVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-29 12:31:06
 */
public interface UsermailMapper extends BaseMapper<Usermail> {

    //获取邮件列表
    IPage<List<Map<String, Object>>> queryUserMailPage(Page page, @Param("title") String title, @Param("accountId") String accountId);

    //获取邮件信息
    MailVo getUserMailInfo(@Param("mailId") Integer mailId);

    //通过userId查询
    MailVo findById(@Param("mailId") Integer mailId, @Param("userId") Integer userId);
}
