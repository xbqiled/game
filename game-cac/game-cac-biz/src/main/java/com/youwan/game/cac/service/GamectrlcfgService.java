package com.youwan.game.cac.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.GameFqzsCtrlCfg;
import com.youwan.game.cac.api.entity.Gamectrlcfg;

import java.util.List;
import java.util.Map;

/**
 * VIEW
 *
 * @author code generator
 * @date 2020-03-27 20:25:55
 */
public interface GamectrlcfgService extends IService<Gamectrlcfg> {

    IPage<Map<String, Object>> queryGameCtrlCfgPage(Page page, String startTime, String endTime);

    int doFqzsCfg(GameFqzsCtrlCfg gameFqzsCtrlCfg);

    Map getGameCtrlCfg(String gameId);
}
