package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.SysPushConfig;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2020-05-23 15:14:12
 */
public interface SysPushConfigMapper extends BaseMapper<SysPushConfig> {

    IPage<List<Map<String, Object>>> queryPushConfigPage(Page page, @Param("channelId") String channelId, @Param("name") String name, @Param("status") String status);

    int updateInvalidAll(@Param("channelId") String channelId, @Param("id") Integer id);

    int activatePushCofing(@Param("id") Integer id);

    SysPushConfig getActivateConfig(@Param("channelId") String channelId);
}
