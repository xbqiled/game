package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.Domain;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-04-19 13:18:14
 */
public interface DomainMapper extends BaseMapper<Domain> {
    IPage<List<Domain>> queryDomainPage(Page page, @Param("channelId") String channelId, @Param("domainUrl") String domainUrl);

    List<Domain> queryDomainByChannelId(@Param("channelId") String channelId);
}
