package com.youwan.game.cac.controller;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.BulletinDto;
import com.youwan.game.cac.api.entity.Bulletin;
import com.youwan.game.cac.api.vo.BulletinVo;
import com.youwan.game.cac.service.BulletinService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.common.security.annotation.Inner;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * @author code generator
 * @date 2018-12-29 13:18:03
 */
@RestController
@AllArgsConstructor
@RequestMapping("/bulletin")
public class BulletinController {

    private final BulletinService bulletinService;

    /**
     * 分页查询
     *
     * @param page      分页对象
     * @param ttitle
     * @return
     */
    @GetMapping("/page")
    public R getTBulletinPage(Page page, String tid, String ttitle) {
        return new R<>(bulletinService.queryBulletinPage(page, tid, ttitle));
    }

    /**
     * 通过id查询
     *
     * @param tId id
     * @return R
     */
    @GetMapping("/{tId}")
    @PreAuthorize("@pms.hasPermission('cac_bulletin_view')")
    public R getById(@PathVariable("tId") String tId) {
        BulletinVo bulletin = bulletinService.getBulletinVoById(tId);
        bulletin.setContent(new String(bulletin.getTContent()));
        List<Map<String, Object>> list = bulletinService.queryBulletinChannel(tId);

        StringBuffer  sb = new StringBuffer();

        if (CollUtil.isNotEmpty(list)) {
          for (int i = 0 ;  i < list.size() ; i ++) {
              Map<String, Object> map =  list.get(i);

              sb.append(map.get("channelId"));
              if (i < list.size() - 1) {
                  sb.append(",");
              }
          }
        }

        bulletin.setChannelList(sb.toString());
        return new R<>(bulletin);
    }

    @Inner
    @PostMapping("/getBulletinById/{bullitinId}")
    public R getBulletinId(@PathVariable("bullitinId") String bullitinId) {
        BulletinVo bulletin = bulletinService.getBulletinVoById(bullitinId);
        bulletin.setContent(new String(bulletin.getTContent()));
        return new R<>(bulletin);
    }

    /**
     * <B>查询公告归属的渠道</B>
     * @param tId
     * @return
     */
    @GetMapping("/getChannel/{tId}")
    public R getChannle(@PathVariable("tId") String tId) {
        List<Map<String, Object>> list = bulletinService.queryBulletinChannel(tId);
        return new R<>(list);
    }

    /**
     * 新增
     *
     * @param bulletinDto
     * @return R
     */
    @SysLog("新增")
    @PostMapping
    @PreAuthorize("@pms.hasPermission('cac_bulletin_add')")
    public R save(@RequestBody BulletinDto bulletinDto) {
        int result = bulletinService.addBulletin(bulletinDto);
        if (result == 0) {
            return new R<>();
        } else if (result == 1) {
            return new R<>().setCode(1).setMsg("渠道编码错误!");
        } else if (result == 2) {
            return new R<>().setCode(1).setMsg("创建公告失败!");
        } else {
            return new R<>();
        }
    }

    /**
     * 修改
     *
     * @param bulletinDto
     * @return R
     */
    @SysLog("修改")
    @PutMapping
    @PreAuthorize("@pms.hasPermission('cac_bulletin_edit')")
    public R updateById(@RequestBody BulletinDto bulletinDto) {
        int result = bulletinService.modifyBulletin(bulletinDto);
        if (result == 0) {
            return new R<>();
        } else if (result == 1) {
            return new R<>().setCode(1).setMsg("渠道编码错误!");
        } else if (result == 2) {
            return new R<>().setCode(1).setMsg("修改公告失败!");
        } else {
            return new R<>();
        }
    }

    /**
     * 通过id删除
     *
     * @param tId id
     * @return R
     */
    @SysLog("删除")
    @PreAuthorize("@pms.hasPermission('cac_bulletin_del')")
    @DeleteMapping("/{tId}")
    public R removeById(@PathVariable String tId) {
        return new R<>(bulletinService.delBulletin(tId));
    }

}
