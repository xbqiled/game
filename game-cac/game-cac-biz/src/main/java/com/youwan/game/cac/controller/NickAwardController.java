package com.youwan.game.cac.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.GsonNickRward;
import com.youwan.game.cac.api.entity.NickAwardForm;
import com.youwan.game.cac.api.vo.NickAwardVo;
import com.youwan.game.cac.service.NickAwardService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;


/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-18 18:58:08
 */
@RestController
@AllArgsConstructor
@RequestMapping("/nickAward")
public class NickAwardController {

    private NickAwardService nickAwardService;

    /**
     * 列表
     */
    @RequestMapping("/page")
    public R page(Page page, QueryDto queryDto){
        return new R<>(nickAwardService.queryPage(page, queryDto));
    }

    @RequestMapping("/info/{channelId}")
    @PreAuthorize("@pms.hasPermission('cac_nickAward_view')")
    public R info(@PathVariable("channelId") String channelId){
        NickAwardVo vo = nickAwardService.getByChannelId(channelId);
        String cfg = new String(vo.getStrJsonAward());
        if (StrUtil.isNotEmpty(cfg)) {
            GsonNickRward nickRward = new Gson().fromJson(cfg, GsonNickRward.class);
            vo.setAward(nickRward.getAward());
        }
        return new R<>(vo);
    }


    @SysLog("代理修改昵称返利信息保存")
    @RequestMapping("/doConfig")
    @PreAuthorize("@pms.hasPermission('cac_nickAward_config')")
    public R doConfig(@RequestBody NickAwardForm nickAwardForm) {
        int result = nickAwardService.saveNcikAwardCfg(nickAwardForm.getChannelId(), nickAwardForm.getOpenType(), nickAwardForm.getReward());
        if (result == 0) {
            return new R<>();
        } else {
            return new R<>(null,"配置修改昵称返利失败!");
        }
    }
}
