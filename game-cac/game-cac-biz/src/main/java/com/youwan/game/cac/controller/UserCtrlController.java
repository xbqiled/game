package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.UserCtrlDto;
import com.youwan.game.cac.service.UsergamedataService;
import com.youwan.game.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-10 21:03:48
 */
@RestController
@AllArgsConstructor
@RequestMapping("/userctrl")
public class UserCtrlController {

  private final UsergamedataService usergamedataService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param
   * @return
   */
  @GetMapping("/page")
  public R getUserCtrlPage(Page page, String accountId, String gameId, String channelId) {
    return new R<>(usergamedataService.queryUserCtrlPage(page, accountId, gameId, channelId));
  }


  @GetMapping("/cancelCtrl")
  public R cancelCtrl(String accountId, String roomList) {
    int result = usergamedataService.cancelCtrl(accountId, roomList);
    if (result == 0) {
      return new R<>();
    } else {
      return new R<>().setMsg("设置玩家控制失败!");
    }
  }


  @GetMapping("/roomList")
  public R roomList() {
    List<Map<String, Object>> roomList = usergamedataService.queryGameRoom();
    if (roomList != null && roomList.size() > 0) {
      for (int i = 0; i < roomList.size(); i++) {

        Integer key = (Integer)roomList.get(i).get("gameAtomTypeId");
        String gameKindName = (String)roomList.get(i).get("gameKindName");
        String phoneGameName = (String)roomList.get(i).get("phoneGameName");

        roomList.get(i).put("phoneGameName", gameKindName + "-" + phoneGameName);
        roomList.get(i).put("key", key);
        roomList.get(i).put("label", gameKindName + "-" + phoneGameName);
      }
    }
    return new R<>(roomList);
  }

  /**
   * 保存
   */
  @RequestMapping("/doSet")
  public R doSet(@RequestBody UserCtrlDto userCtrlDto){
    int result = usergamedataService.doSet(userCtrlDto.getAccountId(), userCtrlDto.getType(), userCtrlDto.getBetLimit(), userCtrlDto.getRate(), userCtrlDto.getRound(), userCtrlDto.getRoomList(), userCtrlDto.getChannelId());
    if (result == 0) {
      return new R<>().setCode(result);
    } else if (result == 1) {
      return new R<>().setCode(result).setMsg("用户ID不正确!");
    } else if (result == 2) {
      return new R<>().setCode(result).setMsg("设置玩家控制失败!");
    } else {
      return new R<>().setCode(result).setMsg("设置玩家控制失败!");
    }
  }

}
