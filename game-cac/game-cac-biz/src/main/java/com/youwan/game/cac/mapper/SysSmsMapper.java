package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.SysSms;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-22 14:18:24
 */
public interface SysSmsMapper extends BaseMapper<SysSms> {

    IPage<List<SysSms>> querySmsPage(Page page, @Param("phoneNumber") String phoneNumber, @Param("channelId") String channelId);

}
