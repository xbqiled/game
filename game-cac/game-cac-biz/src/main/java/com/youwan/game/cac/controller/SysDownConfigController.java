package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.SysDownConfig;
import com.youwan.game.cac.service.SysDownConfigService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-01-26 14:00:25
 */
@RestController
@AllArgsConstructor
@Api(value = "downconfig", description = "下载管理模块")
@RequestMapping("/downconfig")
public class SysDownConfigController {

  private final SysDownConfigService sysDownConfigService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param channelId
   * @return
   */
  @GetMapping("/page")
  public R getSysDownConfigPage(Page page, String channelId) {
    return  new R<>(sysDownConfigService.queryDownConfigPage(page, channelId));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(sysDownConfigService.getById(id));
  }


  /**
   * 新增
   * @param sysDownConfig 
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  @PreAuthorize("@pms.hasPermission('cac_sysdownconfig_add')")
  public R save(@RequestBody SysDownConfig sysDownConfig){
    return new R<>(sysDownConfigService.saveDownConfig(sysDownConfig));
  }


  /**
   * 修改
   * @param sysDownConfig 
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  @PreAuthorize("@pms.hasPermission('cac_sysdownconfig_activate')")
  public R updateById(@RequestBody SysDownConfig sysDownConfig){
    return new R<>(sysDownConfigService.updateDownConfig(sysDownConfig));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  @PreAuthorize("@pms.hasPermission('cac_sysdownconfig_del')")
  public R removeById(@PathVariable Integer id){
    return new R<>(sysDownConfigService.removeById(id));
  }


  @SysLog("激活下载配置")
  @GetMapping("/activate")
  @PreAuthorize("@pms.hasPermission('cac_sysdownconfig_activate')")
  public R activate(@PathVariable("id")Integer id, @PathVariable("channelId")Integer channelId) {
    return new R<>(sysDownConfigService.activateDownConfig(id, channelId));
  }


  @Inner
  @GetMapping("/configByChannelId/{channelId}")
  public R configByChannelId(@PathVariable("channelId")String channelId) {
    return new R<>(sysDownConfigService.getConfigByChannelId(channelId));
  }

  @Inner
  @GetMapping("/config1ByChannelId/{channelId}")
  public R config1ByChannelId(@PathVariable("channelId")String channelId) {
    return new R<>(sysDownConfigService.getConfig1ByChannelId(channelId));
  }
}
