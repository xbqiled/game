package com.youwan.game.cac.service;

import com.youwan.game.cac.api.dto.ProxyMemberQueryDto;
import com.youwan.game.cac.api.entity.ProxyMemberTreasureChange;
import com.youwan.game.common.data.core.Page;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-04-25.
 */
public interface ProxyMemberTreasureChangeService {

    Page<ProxyMemberTreasureChange> queryProxyMemberTreasureChange(ProxyMemberQueryDto queryDto);

}
