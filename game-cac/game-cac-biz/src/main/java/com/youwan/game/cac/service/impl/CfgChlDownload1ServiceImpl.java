package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.dto.DownloadConfigDto;
import com.youwan.game.cac.api.entity.CfgChlDownload;
import com.youwan.game.cac.api.entity.CfgChlDownload1;
import com.youwan.game.cac.api.vo.DownloadPanelConfigVo1;
import com.youwan.game.cac.mapper.CfgChlDownload1Mapper;
import com.youwan.game.cac.service.CfgChlDownload1Service;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-12-30 15:25:40
 */
@Service("cfgChlDownload1Service")
public class CfgChlDownload1ServiceImpl extends ServiceImpl<CfgChlDownload1Mapper, CfgChlDownload1> implements CfgChlDownload1Service {


    @Override
    public IPage<List<CfgChlDownload1>> queryDownloadCfgPage(Page page, DownloadConfigDto downloadConfigDto) {
        return baseMapper.queryDownloadCfgPage(page, downloadConfigDto.getChannelId());
    }

    @Override
    public DownloadPanelConfigVo1 getConfigByChannelId(String channelId) {
        return baseMapper.getConfigVoByChannelId(channelId);
    }

    @Override
    public Boolean saveDownloadPanelCfg(CfgChlDownload1 cfgChlDownload) {
        CfgChlDownload1 config = baseMapper.getConfigByChannelId(cfgChlDownload.getChannelId());
        if (config != null) {
            return  Boolean.FALSE;
        }

        cfgChlDownload.setCreateBy(getUsername());
        cfgChlDownload.setCreateTime(LocalDateTime.now());
        this.save(cfgChlDownload);
        return Boolean.TRUE;
    }

    @Override
    public Boolean modifyDownloadPanelCfg(CfgChlDownload1 cfgChlDownload) {
        CfgChlDownload1 config = baseMapper.getConfigByChannelId(cfgChlDownload.getChannelId());
        if (config == null) {
            return  Boolean.FALSE;
        }

        config.setModifyBy(getUsername());
        config.setModifyTime(LocalDateTime.now());
        config.setBackgroundUrl(cfgChlDownload.getBackgroundUrl());

        config.setCsTips(cfgChlDownload.getCsTips());
        config.setBackgroundUrl(cfgChlDownload.getBackgroundUrl());
        config.setActivityMsg(cfgChlDownload.getActivityMsg());

        config.setCsUrl(cfgChlDownload.getCsUrl());
        this.updateById(config);
        return Boolean.TRUE;
    }



    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }
}
