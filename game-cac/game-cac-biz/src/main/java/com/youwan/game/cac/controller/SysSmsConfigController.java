package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.SysSmsConfig;
import com.youwan.game.cac.api.vo.SmsVo;
import com.youwan.game.cac.service.SysSmsConfigService;
import com.youwan.game.common.core.sms.SmsConfig;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-01-24 13:41:07
 */
@RestController
@AllArgsConstructor
@RequestMapping("/syssmsconfig")
public class SysSmsConfigController {

  private final SysSmsConfigService sysSmsConfigService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param sysSmsConfig 
   * @return
   */
  @GetMapping("/page")
  public R getSysSmsConfigPage(Page page, SysSmsConfig sysSmsConfig) {
    return new R<>(sysSmsConfigService.page(page, Wrappers.query(sysSmsConfig).orderByDesc(SysSmsConfig::getCreateTime)));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(sysSmsConfigService.getById(id));
  }


  /**
   * <B>通过ID获取配置信息</B>
   * @param id
   * @return
   */
  @GetMapping("/getSmsConfig/{id}")
  public R getSmsConfigById(@PathVariable("id") Long id){
    return new R<>(sysSmsConfigService.getById(id));
  }


  /**
   * 新增
   * @param sysSmsConfig 
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  @PreAuthorize("@pms.hasPermission('cac_syssmsconfig_add')")
  public R save(@RequestBody SysSmsConfig sysSmsConfig){
    return new R<>(sysSmsConfigService.saveSmsConfig(sysSmsConfig));
  }


  @SysLog("激活短信配置")
  @GetMapping("/activate/{id}")
  @PreAuthorize("@pms.hasPermission('cac_syssmsconfig_activate')")
  public R activate(@PathVariable Integer id) {
    return new R<>(sysSmsConfigService.activateSmsConfig(id));
  }


  /**
   * 修改
   * @param sysSmsConfig 
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  @PreAuthorize("@pms.hasPermission('cac_syssmsconfig_edit')")
  public R updateById(@RequestBody SysSmsConfig sysSmsConfig){
    return new R<>(sysSmsConfigService.updateById(sysSmsConfig));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  @PreAuthorize("@pms.hasPermission('cac_syssmsconfig_del')")
  public R removeById(@PathVariable Integer id){
    return new R<>(sysSmsConfigService.deleteSmsSetting(id));
  }


  /**
   * 云存储配置信息
   */
  @GetMapping("/setting/{id}")
  public R setting(@PathVariable Integer id){
    SmsConfig config = sysSmsConfigService.selectSmsSetting(id);
    return new R<>(config);
  }


  /**
   * 保存云存储配置信息
   */
  @SysLog("保存云存储配置")
  @PostMapping("/saveSetting/{id}")
  @PreAuthorize("@pms.hasPermission('cac_syssmsconfig_setting')")
  public R saveSetting(@PathVariable Integer id , @RequestBody SmsVo smsVo){
    Boolean result = sysSmsConfigService.updateSmsSetting(id , smsVo);
    return new R<>(result);
  }
}
