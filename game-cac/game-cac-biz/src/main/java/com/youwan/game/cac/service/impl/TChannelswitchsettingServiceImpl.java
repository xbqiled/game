package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.TChannelswitchsetting;
import com.youwan.game.cac.mapper.ChannelswitchsettingMapper;
import com.youwan.game.cac.service.TChannelswitchsettingService;
import org.springframework.stereotype.Service;

/**
 * 
 *
 * @author code generator
 * @date 2019-02-08 18:43:43
 */
@Service("tChannelswitchsettingService")
public class TChannelswitchsettingServiceImpl extends ServiceImpl<ChannelswitchsettingMapper, TChannelswitchsetting> implements TChannelswitchsettingService {


    @Override
    public TChannelswitchsetting queryByChannelId(String tChannelkey) {
        TChannelswitchsetting channelswitchsetting =  baseMapper.queryByChannelId(tChannelkey);
        return channelswitchsetting;
    }


    @Override
    public Boolean saveSetting(String tChannelkey, String tNormalsetting, String tAuditsetting) {
        baseMapper.deleteByChannelId(tChannelkey);
        baseMapper.saveSetting(tChannelkey, tNormalsetting, tAuditsetting);
        return Boolean.TRUE;
    }
}
