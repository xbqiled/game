package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.TChannelRelation;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-04-01 15:36:39
 */
public interface ChannelRelationMapper extends BaseMapper<TChannelRelation> {

    List<TChannelRelation> getGameConfigByChannelId(@Param("channelId")String channelId);

    void  delGameConfigByChannelId(@Param("channelId")String channelId);
}
