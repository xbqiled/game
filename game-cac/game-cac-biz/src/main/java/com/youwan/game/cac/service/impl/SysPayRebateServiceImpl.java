package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.SysPayRebate;
import com.youwan.game.cac.mapper.SysPayRebateMapper;
import com.youwan.game.cac.service.SysPayRebateService;
import org.springframework.stereotype.Service;

/**
 *
 *
 * @author code generator
 * @date 2019-06-05 14:11:47
 */
@Service("sysPayRebateService")
public class SysPayRebateServiceImpl extends ServiceImpl<SysPayRebateMapper, SysPayRebate> implements SysPayRebateService {

}
