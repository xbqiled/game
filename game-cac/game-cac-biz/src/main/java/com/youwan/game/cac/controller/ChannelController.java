package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.TChannel;
import com.youwan.game.cac.service.ChannelRelationService;
import com.youwan.game.cac.service.TChannelService;
import com.youwan.game.cac.service.TChannelswitchsettingService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.common.security.annotation.Inner;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-02-08 18:30:36
 */
@RestController
@AllArgsConstructor
@RequestMapping("/tchannel")
public class ChannelController {

  private final TChannelService channelService;
  private final TChannelswitchsettingService channelswitchsettingService;
  private final ChannelRelationService channelRelationService;


  /**
   * 分页查询
   * @param page 分页对象
   * @param tChannel 
   * @return
   */
  @GetMapping("/page")
  public R getChannelPage(Page page, TChannel tChannel) {
    return  new R<>(channelService.queryChannelPage(page, tChannel.getChannelId(), tChannel.getChannelName()));
  }

  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(channelService.getById(id));
  }

  /**
   * 新增
   * @param tChannel 
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  @PreAuthorize("@pms.hasPermission('cac_channel_add')")
  public R save(@RequestBody TChannel tChannel){
    int result = channelService.saveChannel(tChannel);
    if (result == 1) {
      return new R<>().setCode(result).setMsg("渠道编码不能重复!");
    } else if (result == 0) {
      return new R<>().setCode(result).setMsg("添加渠道成功!");
    } else if (result == 2) {
      return new R<>().setCode(result).setMsg("调用接口失败!");
    } else {
      return new R<>().setCode(result).setMsg("添加渠道失败!");
    }
  }

  /**
   * 修改
   * @param tChannel 
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  @PreAuthorize("@pms.hasPermission('cac_channel_edit')")
  public R updateById(@RequestBody TChannel tChannel){
      int result = channelService.updateChannel(tChannel);
      if (result == 1) {
          return new R<>().setCode(result).setMsg("渠道编码不能重复!");
      } else if (result == 0) {
          return new R<>().setCode(result).setMsg("修改渠道成功!");
      } else if (result == 2) {
          return new R<>().setCode(result).setMsg("调用接口失败!");
      } else {
          return new R<>().setCode(result).setMsg("修改渠道成功!");
      }
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  @PreAuthorize("@pms.hasPermission('cac_channel_del')")
  public R removeById(@PathVariable Integer id){
    return new R<>(channelService.removeById(id));
  }


  @GetMapping("/config/{id}")
  public R getConfigById(@PathVariable("id") Integer id){
    return new R<>(channelswitchsettingService.queryByChannelId(id.toString()));
  }


  @SysLog("配置渠道信息")
  @GetMapping("/saveConfig")
  public R channelConfig(String tChannelkey, String tNormalsetting, String tAuditsetting) {
    return new R<>(channelswitchsettingService.saveSetting(tChannelkey, tNormalsetting, tAuditsetting));
  }


  @Inner
  @GetMapping("/channelInfo/{channleId}")
  public R getChannleById(@PathVariable("channleId") String channleId){
    return new R<>(channelService.getChannelInfo(channleId));
  }


  @GetMapping("/gameConfig/{channleId}")
  public R  getConfigGame(@PathVariable("channleId") Integer channleId){
    return new R<>(channelService.getConfigGame(channleId.toString()));
  }


  @SysLog("配置渠道游戏")
  @PostMapping("/doGameConfig")
  public R doConfigGame(String channelId, String noteId) {
    String [] notes  = noteId.substring(0, noteId.length() -1).split(",");
    int result = channelRelationService.doGameConfig(channelId, notes);
    if (result == 0) {
      return new R<>().setCode(result).setMsg("修改游戏配置成功!");
    } else {
      return new R<>().setCode(result).setMsg("修改游戏配置失败!");
    }
  }
}
