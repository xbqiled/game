package com.youwan.game.cac.mapper.xbqp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.Userbaseattr;
import org.apache.ibatis.annotations.Param;

/**
 * 
 *
 * @author code generator
 * @date 2019-02-01 14:44:56
 */
public interface UserbaseattrMapper extends BaseMapper<Userbaseattr> {

    void addGoldCoinById(@Param("accountId") String accountId, @Param("goldCoin") Integer goldCoin);

    void reduceGoldCoinById(@Param("accountId") String accountId, @Param("goldCoin") Integer goldCoin);
}
