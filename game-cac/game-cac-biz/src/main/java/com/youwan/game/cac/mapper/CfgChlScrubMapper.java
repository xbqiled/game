package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.CfgChlScrub;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-03.
 */
public interface CfgChlScrubMapper extends BaseMapper<CfgChlScrub> {

    CfgChlScrub getConfigByChannelId(@Param("channelId") String channelId);

    IPage<List<CfgChlScrub>> queryScrubCfgPage(Page page, @Param("channelId") String channelId);
}
