package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.StatisChannel;
import com.youwan.game.cac.api.vo.StatisChannelVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2020-02-28 11:22:35
 */
public interface StatisChannelMapper extends BaseMapper<StatisChannel> {


    List<String> queryHaveTab(@Param("tableSql") String tableSql);


    IPage<List<StatisChannelVo>> getDayReportPage(Page page, @Param("tables")List<String> tables, @Param("channelId")String channelId,
                                                  @Param("startTime")String startTime, @Param("endTime")String endTime);


    IPage<List<StatisChannelVo>> getMonthReportPage(Page page,  @Param("tableName")String tableName, @Param("channelId")String channelId, @Param("monthTime")String monthTime);
}
