package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.TChannelRelation;
import com.youwan.game.cac.mapper.ChannelRelationMapper;
import com.youwan.game.cac.service.ChannelRelationService;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.HttpHandler;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-04-01 15:36:39
 */
@Service("channelRelationService")
public class ChannelRelationServiceImpl extends ServiceImpl<ChannelRelationMapper, TChannelRelation> implements ChannelRelationService {

    @Override
    public int doGameConfig(String channelId, String[] roomId) {
        List<TChannelRelation> list = new ArrayList<>();

        Integer[] notes = new Integer[roomId.length];
        for (int i = 0; i < roomId.length; i++) {
            notes[i] = Integer.parseInt(roomId[i]);

            TChannelRelation model  = new TChannelRelation();
            model.setStatus(1);
            model.setChannelId(channelId);
            model.setAtomTypeId(notes[i]);
            list.add(model);
        }

        GsonResult result = HttpHandler.synGameConfig(channelId, notes);
        if (result != null && result.getRet() == 0) {
            baseMapper.delGameConfigByChannelId(channelId);
            this.saveBatch(list);
            return 0;
        } else {
            return 1;
        }
    }

}
