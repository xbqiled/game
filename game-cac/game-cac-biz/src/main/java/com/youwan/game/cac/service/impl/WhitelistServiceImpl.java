package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.Whitelist;
import com.youwan.game.cac.mapper.xbqp.WhitelistMapper;
import com.youwan.game.cac.service.WhitelistService;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.HttpHandler;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author code generator
 * @date 2018-12-29 13:01:34
 */
@Service("tWhitelistService")
public class WhitelistServiceImpl extends ServiceImpl<WhitelistMapper, Whitelist> implements WhitelistService {

    @Override
    public IPage<Map<String, Object>> queryPageList(Page page, Whitelist whitelist) {

        return baseMapper.queryPageList(page, whitelist.getTAccountname());
    }


    @Override
    public Boolean addWhiteList(Whitelist whitelist) {
        //调用接口保存
        List<String> list = new ArrayList<>();
        list.add(whitelist.getTAccountname());
        String accountList[] = new String[list.size()];

        GsonResult result = HttpHandler.synWhiteList(0, list.toArray(accountList));
        if (result != null && result.getRet() == 0) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }


    @Override
    public Boolean delWhiteList(String tAccountname) {
        Whitelist whitelist = baseMapper.findById(tAccountname);

        //调用接口保存
        List<String> list = new ArrayList<>();
        list.add(tAccountname);
        String accountList[] = new String[list.size()];

        int doType = whitelist.getTState().intValue() == 0 ? 1 : 0;
        GsonResult result = HttpHandler.synWhiteList(doType, list.toArray(accountList));
        if (result != null && result.getRet() == 0) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}
