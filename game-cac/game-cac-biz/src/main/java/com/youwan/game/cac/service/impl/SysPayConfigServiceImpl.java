package com.youwan.game.cac.service.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.SysPayConfig;
import com.youwan.game.cac.api.vo.PayChannelVo;
import com.youwan.game.cac.api.vo.PayConfigVo;
import com.youwan.game.cac.handler.PayHand;
import com.youwan.game.cac.mapper.SysPayConfigMapper;
import com.youwan.game.cac.service.SysPayConfigService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-27 16:31:52
 */
@Service("sysPayConfigService")
public class SysPayConfigServiceImpl extends ServiceImpl<SysPayConfigMapper, SysPayConfig> implements SysPayConfigService {

    @Override
    public Boolean saveConfig(SysPayConfig sysPayConfig) {
        sysPayConfig.setCreateTime(LocalDateTime.now());
        sysPayConfig.setCreateBy(Objects.requireNonNull(getUsername()));

        //根据类型判断显示IMG信息
        sysPayConfig.setIcon(PayHand.getIconUrl(sysPayConfig.getPayType()));
        this.save(sysPayConfig);
        return Boolean.TRUE;
    }


    @Override
    public PayChannelVo queryPayConfig(String configId) {
        return baseMapper.queryPayConfig(configId);
    }


    @Override
    public IPage<List<PayChannelVo>> queryPayConfigPage(Page page, String payName, String channelId) {
        return baseMapper.queryPayConfigPage(page, payName, channelId);
    }


    @Override
    public PayConfigVo queryPayConfigVo(String channelId, String configId) {
        return baseMapper.queryPayConfigVo(channelId, configId);
    }


    @Override
    public PayConfigVo getPayConfig(String merchantCode) {
        return baseMapper.getPayConfig(merchantCode);
    }

    /**
     * 获取用户名称
     *
     * @return username
     */
    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }

    @Override
    public List<PayChannelVo> queryPayChannel(String channelId) {
        return baseMapper.queryPayChannel(channelId);
    }
}
