package com.youwan.game.cac.controller;

import com.youwan.game.cac.api.dto.XinBoSwitchDto;
import com.youwan.game.cac.service.ChannelxinboonoffService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * VIEW
 *
 * @author code generator
 * @date 2019-12-15 13:50:26
 */
@RestController
@AllArgsConstructor
@RequestMapping("/xinboonoff")
public class XinBoOnOffController {

  private final ChannelxinboonoffService channelxinboonoffService;

  /**
   * 分页查询
   * @return
   */
  @GetMapping("/all")
  public R getAllXinBoOnOff(XinBoSwitchDto xinBoSwitchDto) {
    return  new R<>(channelxinboonoffService.selectAllSwitch(xinBoSwitchDto));
  }


  /**
   * @param
   * @return R
   */
  @SysLog("配置开关信息")
  @PostMapping
  public R doSwitchConfig(@RequestBody XinBoSwitchDto xinBoSwitchDto){
    Integer result = channelxinboonoffService.doConfig(xinBoSwitchDto);
    if (result == 0) {
      return new R<>().setCode(result);
    } else if (result == 1) {
      return new R<>().setCode(result).setMsg("处理开关失败!");
    } else {
      return new R<>().setCode(result).setMsg("处理开关失败!");
    }
  }
}
