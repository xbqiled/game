package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.OtherBgRedPacket;
import com.youwan.game.cac.mapper.OtherBgRedPacketRecordMapper;
import com.youwan.game.cac.service.OtherBgRedPacketService;
import org.springframework.stereotype.Service;

/**
 * 
 *
 * @author code generator
 * @date 2020-01-13 20:25:56
 */
@Service("bgRedPacketRecordService")
public class OtherBgRedPacketRecordServiceImpl extends ServiceImpl<OtherBgRedPacketRecordMapper, OtherBgRedPacket> implements OtherBgRedPacketService {

}
