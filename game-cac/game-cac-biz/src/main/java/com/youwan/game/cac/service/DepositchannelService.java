package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.Depositchannel;
import com.youwan.game.cac.api.vo.DepositchannelVo;
import com.youwan.game.common.core.jsonmodel.GsonRechargeRebateCfg;

import java.util.List;


/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-05 14:15:52
 */
public interface DepositchannelService extends IService<Depositchannel> {

    Depositchannel findByChannelId(String channelId);

    IPage<DepositchannelVo> queryDepositPage(Page page, QueryDto queryDto);

    int channelOpenType(String channelId, String openType);

    int saveRechargeRebateCfg(String channel, String beginTime, String endTime, Integer openType, List<GsonRechargeRebateCfg> rabateCfg);

}
