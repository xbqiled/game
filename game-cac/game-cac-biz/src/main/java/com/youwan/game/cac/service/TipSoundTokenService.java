package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.entity.TipSoundToken;

import java.util.List;


/**
 * 
 *
 * @author code generator
 * @date 2019-05-21 11:05:27
 */
public interface TipSoundTokenService extends IService<TipSoundToken> {

    IPage<List<TipSoundToken>> querySoundTokenPage(Page page, String channelId);

}
