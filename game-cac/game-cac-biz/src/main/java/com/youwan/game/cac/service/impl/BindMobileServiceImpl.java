package com.youwan.game.cac.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.BindMobile;
import com.youwan.game.cac.api.entity.TChannel;
import com.youwan.game.cac.mapper.BindMobileMapper;
import com.youwan.game.cac.mapper.ChannelMapper;
import com.youwan.game.cac.service.BindMobileService;
import com.youwan.game.common.core.jsonmodel.GsonResult;
import com.youwan.game.common.core.util.HttpHandler;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2019-04-10 21:03:25
 */
@AllArgsConstructor
@Service("bindMobileService")
public class BindMobileServiceImpl extends ServiceImpl<BindMobileMapper, BindMobile> implements BindMobileService {

    private final ChannelMapper channelMapper;

    @Override
    public IPage<List<Map<String, Object>>> queryBindPage(Page page, String channelId) {
        return baseMapper.queryBindPage(page, channelId);
    }

    @Override
    public BindMobile getByChannelId(String channelId) {
        return baseMapper.getByChannleId(channelId);
    }

    @Override
    public Boolean saveOrUpdateBind(BindMobile bindMobile) {
        //判断渠道是否存在
        TChannel channel = channelMapper.getChannelInfo(bindMobile.getChannelId());
        if (channel == null) {
            return Boolean.FALSE;
        }

        GsonResult result = HttpHandler.bindGive(bindMobile.getChannelId(), bindMobile.getGiveValue().intValue(), bindMobile.getRebate(),
                bindMobile.getMinExchange(), bindMobile.getExchange(), bindMobile.getInitCoin(), bindMobile.getDamaMulti(),
                bindMobile.getRebateDamaMulti(), bindMobile.getExchangeCount(), bindMobile.getPromoteType(), bindMobile.getResetDamaLeftCoin(),
                bindMobile.getCpStationId(),bindMobile.getCpLimit());
        if (result != null && result.getRet() == 0) {
            //判断是否有这条记录
            BindMobile bind = baseMapper.getByChannleId(bindMobile.getChannelId());

            if(bind != null && StrUtil.isNotBlank(bind.getChannelId())) {
                bindMobile.setModifyTime(LocalDateTime.now());
                bindMobile.setModifyBy(getUsername());
                baseMapper.updateBind(bindMobile.getChannelId(), bindMobile.getModifyTime(), bindMobile.getModifyBy(), bindMobile.getGiveValue(),
                        bindMobile.getRebate(), bindMobile.getExchange(), bindMobile.getInitCoin(), bindMobile.getMinExchange(),
                        bindMobile.getDamaMulti(), bindMobile.getRebateDamaMulti(), bindMobile.getExchangeCount().toString(), bindMobile.getPromoteType().toString(),
                        bindMobile.getResetDamaLeftCoin().toString(), bindMobile.getCpStationId().toString(),bindMobile.getCpLimit());
                return Boolean.TRUE;
            } else {
                bindMobile.setCreateTime(LocalDateTime.now());
                bindMobile.setCreateBy(getUsername());
                return this.save(bindMobile);
            }
        } else {
            return Boolean.FALSE;
        }
    }


    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }
}
