package com.youwan.game.cac.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.SysPayConfig;
import com.youwan.game.cac.service.SysPayConfigService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import com.youwan.game.common.security.annotation.Inner;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;



/**
 * 
 *
 * @author code lion
 * @date 2018-12-27 16:31:52
 */
@RestController
@AllArgsConstructor
@RequestMapping("/payconfig")
public class SysPayConfigController {

  private final SysPayConfigService sysPayConfigService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param payName
   * @param channelId
   * @return
   */
  @GetMapping("/page")
  public R getSysPayConfigPage(Page page, String payName, String channelId) {
    return  new R<>(sysPayConfigService.queryPayConfigPage(page, payName, channelId));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Long id){
    return new R<>(sysPayConfigService.getById(id));
  }


  /**
   * 新增
   * @param sysPayConfig 
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody SysPayConfig sysPayConfig){
    return new R<>(sysPayConfigService.saveConfig(sysPayConfig));
  }

  /**
   * 修改
   * @param sysPayConfig 
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody SysPayConfig sysPayConfig){
    return new R<>(sysPayConfigService.updateById(sysPayConfig));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Long id){
    return new R<>(sysPayConfigService.removeById(id));
  }


  /**
   * <B>获取充值通道</B>
   * @param channelId
   * @return
   */
  @Inner
  @GetMapping("/payChannel/{channelId}")
  public R queryPayChannel(@PathVariable("channelId")String channelId) {
    return new R<>(sysPayConfigService.queryPayChannel(channelId));
  }


  @Inner
  @PostMapping("/getPayConfig/{merchantCode}")
  public R getPayConfig(@PathVariable("merchantCode")String merchantCode) {
    return new R<>(sysPayConfigService.getPayConfig(merchantCode));
  }


  /**
   * <B>获取充值通道</B>
   * @param configId
   * @return
   */
  @Inner
  @GetMapping("/info/{configId}")
  public R queryPayConfig(@PathVariable("configId")String configId) {
    return new R<>(sysPayConfigService.queryPayConfig(configId));
  }
}
