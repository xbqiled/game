package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.dto.ReportDto;
import com.youwan.game.cac.api.entity.StatisChannel;
import com.youwan.game.cac.api.vo.StatisChannelVo;
import com.youwan.game.cac.service.StatisChannelService;
import com.youwan.game.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * 
 *
 * @author code generator
 * @date 2020-02-28 11:22:35
 */
@RestController
@AllArgsConstructor
@RequestMapping("/statischannel")
public class StatisChannelController {

  private final StatisChannelService statisChannelService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param reportDto
   * @return
   */
  @GetMapping("/dayReport/page")
  public R getStatisDayReportPage(Page page, ReportDto reportDto) {
    IPage<List<StatisChannelVo>> list = statisChannelService.getStatisDayReportPage(page, reportDto);
    return new R<>(list);
  }

  @GetMapping("/monthReport/page")
  public R getStatisMonthReportPage(Page page, ReportDto reportDto) {
    IPage<List<StatisChannelVo>> list = statisChannelService.getStatisMonthReportPage(page, reportDto);
    return new R<>(list);
  }
}
