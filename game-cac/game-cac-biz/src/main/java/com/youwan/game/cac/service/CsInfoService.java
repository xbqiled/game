package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.CsInfoDto;
import com.youwan.game.cac.api.entity.CsInfo;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-03-29 15:44:57
 */
public interface CsInfoService extends IService<CsInfo> {

    IPage<List<CsInfo>> queryCsInfoPage(Page page, CsInfo csInfo);

    Boolean saveCsinfo(CsInfo ssInfo);

    List<CsInfo> getCsInfoByChannelId(String channelId);

    List<CsInfo> getCsInfo(CsInfoDto csInfoDto);
}
