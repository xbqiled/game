package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.SysPayConfig;
import com.youwan.game.cac.api.entity.SysPayPlatform;
import com.youwan.game.cac.api.vo.PayChannelVo;
import com.youwan.game.cac.api.vo.PayConfigVo;
import com.youwan.game.cac.api.vo.PayOrderVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 
 *
 * @author code generator
 * @date 2018-12-27 16:31:52
 */
public interface SysPayConfigMapper extends BaseMapper<SysPayConfig> {

    List<PayChannelVo> queryPayChannel(@Param("channelId") String channelId);

    PayChannelVo queryPayConfig(@Param("configId") String configId);

    PayConfigVo queryPayConfigVo(@Param("channelId") String channelId, @Param("configId") String configId);

    PayConfigVo getPayConfig(@Param("merchantCode") String merchantCode);

    IPage<List<PayChannelVo>> queryPayConfigPage(Page page, @Param("payName") String payName, @Param("channelId") String channelId);

    Boolean updateStatus(@Param("status") String status, @Param("id") String id);

}
