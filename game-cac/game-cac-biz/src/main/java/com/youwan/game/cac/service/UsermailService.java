package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.MailDto;
import com.youwan.game.cac.api.dto.SendMailDto;
import com.youwan.game.cac.api.entity.Usermail;
import com.youwan.game.cac.api.vo.MailVo;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-29 12:31:06
 */
public interface UsermailService extends IService<Usermail> {

    IPage<List<Map<String, Object>>> queryUserMailPage(Page page, MailDto mailDto);

    int sendMail(SendMailDto sendMailDto);

    MailVo getUserMailInfo(Integer mailId);

    MailVo findById(Integer id , Integer userId);
}
