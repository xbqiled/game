package com.youwan.game.cac.mapper.xbqp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youwan.game.cac.api.entity.Channelxinboonoff;
import com.youwan.game.cac.api.vo.XinBoSwitchVo;

import java.util.List;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-12-15 13:50:26
 */
public interface ChannelxinboonoffMapper extends BaseMapper<Channelxinboonoff> {

    List<XinBoSwitchVo> selectAllSwitch();

}
