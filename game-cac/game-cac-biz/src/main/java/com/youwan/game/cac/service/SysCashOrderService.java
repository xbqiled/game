package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.CashOrderDto;
import com.youwan.game.cac.api.entity.SysCashOrder;
import com.youwan.game.cac.api.entity.SysPayOrder;

import java.util.List;


/**
 * 
 *
 * @author code generator
 * @date 2019-02-20 18:19:04
 */
public interface SysCashOrderService extends IService<SysCashOrder> {

    Boolean approval(String id, String status, String note);

    SysCashOrder cretaeOrder(CashOrderDto cashOrderDto);

    List<SysCashOrder> getOrderByUserId(String userId);

    List<SysCashOrder> getCashOrderInfo(String userId, String cashType);

    IPage<List<SysCashOrder>> queryCashOrderPage(Page page, String cashNo, String userId, String status);

}
