package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.SysPushRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author code generator
 * @date 2020-05-23 17:21:44
 */
public interface SysPushRecordMapper extends BaseMapper<SysPushRecord> {

    IPage<List<SysPushRecord>> queryPushRecordPage(Page page,  @Param("channelId") String channelId, @Param("startTime") String startTime , @Param("endTime") String endTime);

    Map<String, Object> getPushRecordById(@Param("id") Integer id);
}
