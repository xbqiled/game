package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.OtherPlatformDto;
import com.youwan.game.cac.api.entity.OtherPlatformConfig;
import com.youwan.game.common.core.otherplatform.PlatformConfig;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-10-15 16:55:44
 */
public interface OtherPlatformConfigService extends IService<OtherPlatformConfig> {

    IPage<List<OtherPlatformConfig>> queryPlatformConfigPage(Page page, OtherPlatformDto otherPlatformDto);

    Boolean saveOtherPlatformConfig(OtherPlatformConfig otherPlatformConfig);

    Boolean updateOtherPlatformConfig(OtherPlatformConfig otherPlatformConfig);

    Integer updatePlatformConfig(Integer id, PlatformConfig platformConfig);

    PlatformConfig getPlatformConfig(Integer id);

    PlatformConfig getPlatformConfigByCode(String codeStr);
}
