package com.youwan.game.cac.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youwan.game.cac.api.entity.SysDownConfig;
import com.youwan.game.cac.api.vo.DownloadConfigVo;
import com.youwan.game.cac.api.vo.DownloadConfigVo1;
import com.youwan.game.cac.mapper.SysDownConfigMapper;
import com.youwan.game.cac.service.SysDownConfigService;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-26 14:00:25
 */
@AllArgsConstructor
@Service("sysDownConfigService")
public class SysDownConfigServiceImpl extends ServiceImpl<SysDownConfigMapper, SysDownConfig> implements SysDownConfigService {

    @Override
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(cacheNames = "downloadconfig:details",allEntries = true)
    public Boolean saveDownConfig(SysDownConfig sysDownConfig) {
        sysDownConfig.setCreateTime(LocalDateTime.now());
        sysDownConfig.setCreateBy(Objects.requireNonNull(getUsername()));

        this.save(sysDownConfig);
        return Boolean.TRUE;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(cacheNames = "downloadconfig:details",allEntries = true)
    public Boolean updateDownConfig(SysDownConfig sysDownConfig) {
        sysDownConfig.setModifyTime(LocalDateTime.now());
        sysDownConfig.setModifyBy(getUsername());

        this.updateById(sysDownConfig);
        return Boolean.TRUE;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(cacheNames = "downloadconfig:details",allEntries = true)
    public Boolean activateDownConfig(Integer id, Integer channelId) {
        //更新所有配置为失效
        this.baseMapper.updateDownConfigInvalid(channelId);
        //更新选择的配置为激活
        this.baseMapper.activateById(id);
        return Boolean.TRUE;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(cacheNames = "downloadconfig:details",allEntries = true)
    public Boolean deleteDownConfig(Integer id) {
        return this.removeById(id);
    }

    /**
     * 获取用户名称
     *
     * @return username
     */
    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }


    /**
     * <B>通过渠道ID获取配置信息</B>
     * @param channelId
     * @return
     */
    @Override
    @CacheEvict(value = "downloadconfig:details", key = "#channelId")
    public DownloadConfigVo getConfigByChannelId(String channelId) {
        //如果没有就从数据库里取得
        return baseMapper.getConfigByChannelId(channelId);
    }


    @Override
    @CacheEvict(value = "downloadconfig1:details", key = "#channelId")
    public DownloadConfigVo1 getConfig1ByChannelId(String channelId) {
        //如果没有就从数据库里取得
        return baseMapper.getConfig1ByChannelId(channelId);
    }

    /**
     * <B>下载配置分页信息</B>
     * @param page
     * @param channelId
     * @return
     */
    @Override
    public IPage<List<SysDownConfig>> queryDownConfigPage(Page page, String channelId) {
        return baseMapper.queryDownConfigPage(page, channelId);
    }
}
