package com.youwan.game.cac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.DeviceInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2019-05-15 15:24:30
 */
public interface DeviceInfoMapper extends BaseMapper<DeviceInfo> {

    IPage<List<DeviceInfo>> getDeviceInfoPage(Page page, @Param("channelId") String channelId, @Param("ip") String ip, @Param("os") String os, @Param("promoterId") String promoterId);

    List<DeviceInfo> getDeviceList(@Param("channelId") String channelId, @Param("ip") String ip, @Param("os") String os);
}
