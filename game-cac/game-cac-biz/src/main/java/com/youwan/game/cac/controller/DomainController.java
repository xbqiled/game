package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.Domain;
import com.youwan.game.cac.service.DomainService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code generator
 * @date 2019-04-19 13:18:14
 */
@RestController
@AllArgsConstructor
@RequestMapping("/domain")
public class DomainController {

  private final DomainService domainService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param domain
   * @return
   */
  @GetMapping("/page")
  public R getDomainPage(Page page, Domain domain) {
    return new R<>(domainService.queryDomainPage(page, domain));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(domainService.getById(id));
  }

  /**
   * 新增
   * @param domain
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  public R save(@RequestBody Domain domain){
    return new R<>(domainService.saveDomain(domain));
  }

  /**
   * 修改
   * @param domain
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  public R updateById(@RequestBody Domain domain){
    return new R<>(domainService.updateById(domain));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  public R removeById(@PathVariable Integer id){
    return new R<>(domainService.removeById(id));
  }

}
