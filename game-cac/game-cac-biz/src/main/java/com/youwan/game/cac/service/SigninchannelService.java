package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.QueryDto;
import com.youwan.game.cac.api.entity.Signinchannel;
import com.youwan.game.cac.api.vo.SigninchannelVo;
import com.youwan.game.common.core.jsonmodel.GsonSignAwardCfg;

import java.util.List;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-05 13:49:00
 */
public interface SigninchannelService extends IService<Signinchannel> {

    IPage<List<SigninchannelVo>> querySignCfgPage(Page page, QueryDto queryDto);

    Signinchannel findByChannelId (String channelId);

    int channelOpenType(String channelId, String openType);

    int channelLoop(String channelId, String isCirculate);

    int saveSignCfg(String channel, String beginTime, String endTime, Integer openType, Integer isCirculate, Integer num, List<GsonSignAwardCfg> rabateCfg);

}
