package com.youwan.game.cac.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youwan.game.cac.api.entity.SysSmsTemplate;
import com.youwan.game.cac.service.SysSmsTemplateService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.log.annotation.SysLog;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author code lion
 * @date 2019-02-17 18:01:23
 */
@RestController
@AllArgsConstructor
@RequestMapping("/syssmstemplate")
public class SysSmsTemplateController {

  private final SysSmsTemplateService sysSmsTemplateService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param templateName
   * @param templateCode
   * @return
   */
  @GetMapping("/page")
  public R getSysSmsTemplatePage(Page page, String templateCode, String templateName, String channelId) {
    return new R<>(sysSmsTemplateService.queryTemplatePage(page, templateCode, templateName, channelId));
  }


  /**
   * 通过id查询
   * @param id id
   * @return R
   */
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") Integer id){
    return new R<>(sysSmsTemplateService.getById(id));
  }


  /**
   * 新增
   * @param sysSmsTemplate 
   * @return R
   */
  @SysLog("新增")
  @PostMapping
  @PreAuthorize("@pms.hasPermission('cac_syssmstemplate_add')")
  public R save(@RequestBody SysSmsTemplate sysSmsTemplate){
    return new R<>(sysSmsTemplateService.saveSmsTemplate(sysSmsTemplate));
  }

  /**
   * 修改
   * @param sysSmsTemplate 
   * @return R
   */
  @SysLog("修改")
  @PutMapping
  @PreAuthorize("@pms.hasPermission('cac_syssmstemplate_edit')")
  public R updateById(@RequestBody SysSmsTemplate sysSmsTemplate){
    return new R<>(sysSmsTemplateService.updateSmsTemplate(sysSmsTemplate));
  }

  /**
   * 通过id删除
   * @param id id
   * @return R
   */
  @SysLog("删除")
  @DeleteMapping("/{id}")
  @PreAuthorize("@pms.hasPermission('cac_syssmstemplate_del')")
  public R removeById(@PathVariable Integer id){
    return new R<>(sysSmsTemplateService.delSmsTemplate(id));
  }

}
