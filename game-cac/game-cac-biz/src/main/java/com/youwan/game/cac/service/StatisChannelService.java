package com.youwan.game.cac.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.youwan.game.cac.api.dto.ReportDto;
import com.youwan.game.cac.api.entity.StatisChannel;
import com.youwan.game.cac.api.vo.StatisChannelVo;

import java.util.List;

/**
 * 
 *
 * @author code generator
 * @date 2020-02-28 11:22:35
 */
public interface StatisChannelService extends IService<StatisChannel> {

    IPage<List<StatisChannelVo>> getStatisDayReportPage(Page page, ReportDto reportDto);

    IPage<List<StatisChannelVo>> getStatisMonthReportPage(Page page, ReportDto reportDto);
}
