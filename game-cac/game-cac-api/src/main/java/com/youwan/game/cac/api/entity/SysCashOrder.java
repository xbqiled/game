package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-02-20 18:19:04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_cash_order")
public class SysCashOrder extends Model<SysCashOrder> {
private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId
    private Integer id;
    /**
     * 订单号
     */
    private String cashNo;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     *充值金额
     */
    private BigDecimal cashFee;
    /**
     * 渠道编码
     */
    private String channelId;
    /**
     *  状态: 0:提现申请 1,审批通过 2,审批失败
     */
    private Integer status;
    /**
     * 提现类型: 0支付宝,1微信,2银行卡
     */
    private Integer cashType;
    /**
     * 用户账号
     */
    private String account;
    /**
     * 账户名称
     */
    private String accountName;
    /**
     * 银行名称
     */
    private String bank;
    /**
     * 分行名称
     */
    private String subBank;
    /**
     * 开户行地址
     */
    private String bankAddress;
    /**
     *
     */
    private LocalDateTime createTime;
    /**
     * SYSTME
     */
    private String createBy;
    /**
     *
     */
    private LocalDateTime approveTime;
    /**
     * 手动处理记录编码
     */
    private String approveBy;
    /**
     * 备注
     */
    private String note;
}
