package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-05 15:57:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("other_bg_user")
public class OtherBgUser extends Model<OtherBgUser> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId
    private Integer id;
    /**
   * 信博棋牌用户ID
   */
    private Integer userId;

    /**
     * 前缀USERID
     */
    private String prefixUserId;
    /**
   * 信博用户昵称
   */
    private String nickName;
    /**
   * 渠道编码
   */
    private String channelId;
    /**
   * 注册用户类型
   */
    private String regType;
    /**
   * 厅代码
   */
    private String sn;
    /**
   * 玩家状态
   */
    private Integer status;
    /**
   * 在bg的用户id
   */
    private Long bgUserId;
    /**
   * 在bg的用户昵称
   */
    private String bgNickname;
    /**
   * bg代理id
   */
    private String agentId;
    /**
   * 创建时间
   */
    private LocalDateTime createTime;
  
}
