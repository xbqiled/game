package com.youwan.game.cac.api.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class StatisGameVo implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 游戏ID
     */
    private Long gameId;
    /**
     * 用户Id
     */
    private Long userId;
    /**
     * 游戏次数
     */
    private Long gameCount;
    /**
     * 渠道编码
     */
    private String channelId;
    /**
     * 赢取金额
     */
    private BigDecimal gameFee;


    private BigDecimal channelFee;
    /**
     * 统计时间
     */
    private Date statisTime;


    private String statisMonthTime;
    /**
     * 下注金额
     */
    private BigDecimal betFee;
    /**
     * 税手金额
     */
    private BigDecimal revenueFee;
    /**
     * 是否比赛
     */
    private Integer isMatch;
    /**
     * 游戏时长
     */
    private Long gameTimeLength;

    private String gameKindName;
}