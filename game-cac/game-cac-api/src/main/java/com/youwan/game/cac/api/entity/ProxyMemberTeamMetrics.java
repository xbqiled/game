package com.youwan.game.cac.api.entity;

import lombok.Data;

/**
 * 团队指标
 * TeamMetrics
 *
 * @author terry.jiang[taoj555@163.com] on 2020-05-06.
 */
@Data
public class ProxyMemberTeamMetrics {

    /**
     * 队长id
     */
    private Long captainId;

    /**
     * 队长昵称
     */
    private String captainNickName;

    /**
     * 直属成员数量
     */
    private Long immediateMembers = 0l;

    /**
     * 所有成员数量
     */
    private Long allMembers = 0l;

    /**
     * 总充值
     */
    private Double totalRecharge = 0d;

    /**
     * 总提现
     */
    private Double totalCashFee = 0d;

    /**
     * 总打码
     */
    private Double totalStake = 0d;

    /**
     * 总返佣
     */
    private Double totalRebate = 0d;

    private int deep = 0;

}
