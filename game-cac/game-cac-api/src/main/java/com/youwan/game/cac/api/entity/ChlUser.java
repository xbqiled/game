package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统用户
 *
 * @author code generator
 * @date 2019-02-09 17:16:48
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("chl_user")
public class ChlUser extends Model<ChlUser> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private Long userId;
    /**
   * 用户名
   */
    private String username;
    /**
   * 密码
   */
    private String password;
    /**
   * 盐
   */
    private String salt;
    /**
   * 邮箱
   */
    private String email;
    /**
   * 手机号
   */
    private String mobile;
    /**
   * 状态  0：禁用   1：正常
   */
    private Integer status;

    /**
     * 类型 0:管理账号 2;渠道账号
     */
    private Integer type;
    /**
   * 创建者ID
   */
    private Long createUserId;
    /**
   * 创建时间
   */
    private LocalDateTime createTime;
    /**
   * 渠道ID
   */
    private Integer channelId;
  
}
