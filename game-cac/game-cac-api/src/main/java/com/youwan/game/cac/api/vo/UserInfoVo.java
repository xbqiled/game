package com.youwan.game.cac.api.vo;


import cn.hutool.core.bean.BeanUtil;
import com.youwan.game.cac.api.entity.Accountinfo;
import com.youwan.game.cac.api.entity.Userbaseattr;
import lombok.Data;

@Data
public class UserInfoVo {

    private Integer tAccountid;
    /**
     *
     */
    private String tAccountname;
    /**
     *
     */
    private String tPhonenumber;
    /**
     *
     */
    private String tChannelkey;
    /**
     *
     */
    private String tAccountkey;
    /**
     *
     */
    private String tNickname;
    /**
     *
     */
    private Integer tBisregularaccount;
    /**
     *
     */
    private String tIdcard;
    /**
     *
     */
    private String tLockmachine;
    /**
     *
     */
    private Long tLastlogintime;
    /**
     *
     */
    private Long tLastlogouttime;
    /**
     *
     */
    private String tCommondevices;
    /**
     *
     */
    private String tCommonips;
    /**
     *
     */
    private Integer tAccountstate;
    /**
     *
     */
    private Integer tAuthpwdfailtimes;
    /**
     *
     */
    private String tExcepverifyinfo;
    /**
     *
     */
    private Long tRegtime;
    /**
     *
     */
    private Integer tRegterminaltype;
    /**
     *
     */
    private String tRegip;
    /**
     *
     */
    private String tRegdevice;
    /**
     *
     */
    private Integer tIsuniqdevicecode;
    /**
     *
     */
    private Integer tSharetype;
    /**
     *
     */
    private Integer tSharetriggerfuncid;
    /**
     *
     */
    private String tAlipayinfo;
    /**
     *
     */
    private String tBankinfo;

    /**
     *
     */
    private Integer tSex;
    /**
     *
     */
    private Integer tViplevel;
    /**
     *
     */
    private Integer tGoldcoin;
    /**
     *
     */
    private Integer tAvatarid;
    /**
     *
     */
    private Integer tAvatarframeid;
    /**
     *
     */
    private Integer tTryavatarid;
    /**
     *
     */
    private Integer tAuditingavatarid;
    /**
     *
     */
    private Integer tCustommaxavatarid;
    /**
     *
     */
    private String tUnlocksystemavatarlist;
    /**
     *
     */
    private String tCustomavatarlist;
    /**
     *
     */
    private String tBankpassword;
    /**
     *
     */
    private Integer tBankgoldcoin;
    /**
     *
     */
    private Integer tIsmodifiednick;
    /**
     *
     */
    private byte [] tModulescattereddata;
    /**
     *
     */
    private Integer tTotalgametime;
    /**
     *
     */
    private Integer tTodaygametime;
    /**
     *
     */
    private Long tTodaygametimerefreshtime;
    /**
     *
     */
    private Long tTotalcoinget;
    /**
     *
     */
    private Long tTodaycoinget;
    /**
     *
     */
    private Long tTodaycoinrefreshtime;
    /**
     *
     */
    private Integer tRechargemoney;
    /**
     *
     */
    private  byte []  tGameinfo;
    /**
     *
     */
    private  byte []  tGamehistory;
    /**
     *
     */
    private  byte []  tGameexdata;

    private Long tLastgamebetscore;

    private Long tTodaygamebetscore;

    private Long tBetscorersftime;

    public static  UserInfoVo installBean(Accountinfo accountinfo, Userbaseattr userbaseattr) {
        UserInfoVo userInfo = new UserInfoVo();
        BeanUtil.copyProperties(accountinfo, userInfo);
        BeanUtil.copyProperties(userbaseattr, userInfo);

        Long tRegtime = userInfo.getTRegtime() *1000;
        userInfo.setTRegtime(tRegtime);

        Long tLastlogintime = userInfo.getTLastlogintime() * 1000;
        userInfo.setTLastlogintime(tLastlogintime);

        Long tLastlogouttime = userInfo.getTLastlogouttime() * 1000;
        userInfo.setTLastlogouttime(tLastlogouttime);

        Long tTodaygametimerefreshtime = userInfo.getTTodaygametimerefreshtime() * 1000;
        userInfo.setTTodaygametimerefreshtime(tTodaygametimerefreshtime);

        Long tTodaycoinrefreshtime = userInfo.getTTodaycoinrefreshtime() * 1000;
        userInfo.setTTodaycoinrefreshtime(tTodaycoinrefreshtime);

        return userInfo;
    }
}
