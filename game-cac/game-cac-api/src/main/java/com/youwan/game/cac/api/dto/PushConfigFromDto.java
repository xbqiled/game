package com.youwan.game.cac.api.dto;


import com.youwan.game.common.core.push.jpush.JPushConfig;
import lombok.Data;

import java.io.Serializable;

@Data
public class PushConfigFromDto implements Serializable {

    private JPushConfig pushConfig;

    private Integer id;
}
