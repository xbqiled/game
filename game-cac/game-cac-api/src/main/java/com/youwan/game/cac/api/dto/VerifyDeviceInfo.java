package com.youwan.game.cac.api.dto;


import lombok.Data;

import java.io.Serializable;


@Data
public class VerifyDeviceInfo implements Serializable {

    private String channelId;
    private String ip;
    private String os;
    private String osVersion;
    private String colorDepth;

    private String deviceMemory;
    private String deviceType;

    private String hardwareConcurrency;
    private String language;

    private String netWork;
    private String screenHeight;
    private String screenWidth;


    private String deviceModel;
}
