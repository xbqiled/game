package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2020-05-23 15:14:12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_push_config")
public class SysPushConfig extends Model<SysPushConfig> {
private static final long serialVersionUID = 1L;

    /**
   * ID
   */
    @TableId
    private Integer id;
    /**
   * 名称
   */
    private String name;
    /**
   * 通道编码
   */
    private Integer platformId;
    /**
   * 配置
   */
    private String config;
    /**
   * 有效性标识,只有一个有效
   */
    private Integer isActivate;
    /**
   * 描述
   */
    private String description;
    /**
   * 渠道编码
   */
    private String channelId;
    /**
   * 当日推送次数
   */
    private Long dayCount;
    /**
   * 当日最大推送次数
   */
    private Long dayMaxCount;
    /**
   * 总计推送次数
   */
    private Long totalCount;
    /**
   * 创建时间
   */
    private LocalDateTime createTime;
    /**
   * 创建人
   */
    private String createBy;
    /**
   * 最后一次更新时间
   */
    private LocalDateTime lastUpdateTime;
  
}
