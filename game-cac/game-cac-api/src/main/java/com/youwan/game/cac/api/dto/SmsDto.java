package com.youwan.game.cac.api.dto;


import lombok.Data;


@Data
public class SmsDto {

    private String channelId;

    private String phone;

    private String templateType;

    private String[] parameter;

    public SmsDto(String channelId, String phone, String templateType, String[] parameter) {
        this.channelId = channelId;
        this.phone = phone;
        this.templateType = templateType;
        this.parameter = parameter;
    }

}
