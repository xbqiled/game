package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-04-01 15:36:39
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_channel_relation")
public class TChannelRelation extends Model<TChannelRelation> {
private static final long serialVersionUID = 1L;

    /**
   * 渠道编码
   */
    private String channelId;
    /**
   * 房间编码
   */
    private Integer atomTypeId;
    /**
   * 状态
   */
    private Integer status;
  
}
