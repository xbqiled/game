package com.youwan.game.cac.api.dto;


import lombok.Data;

import java.io.Serializable;

@Data
public class OpenBgGameDto implements Serializable {
    private Integer isMobileUrl;
    private Integer isHttpsUrl;
    private String userId;
    private String password;
    private String channelId;
    private String sysBalance;
    private Integer isWx;
    private String table;


    public OpenBgGameDto(Integer isMobileUrl, Integer isHttpsUrl, String userId, String password, String channelId, String sysBalance) {
        this.isMobileUrl = isMobileUrl;
        this.isHttpsUrl = isHttpsUrl;
        this.userId = userId;
        this.password = password;
        this.channelId = channelId;
        this.sysBalance = sysBalance;
    }
}
