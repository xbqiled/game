package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-03.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cfg_channel_scrub")
public class CfgChlScrub extends Model<CfgChlScrub> {

    @TableId
    private Integer id;

    /**
     * 渠道id
     */
    private String channelId;

    /**
     * 配置json
     */
    private String scrubConfig;

    /**
     * 修改时间
     */
    private LocalDateTime modifyAt;

    /**
     * 修改人
     */
    private String modifyBy;

    /**
     * 创建时间
     */
    private LocalDateTime createAt;

    /**
     * 创建人
     */
    private String createBy;
}
