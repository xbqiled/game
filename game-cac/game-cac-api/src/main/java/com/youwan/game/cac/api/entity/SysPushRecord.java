package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 
 *
 * @author code generator
 * @date 2020-05-23 17:21:44
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_push_record")
public class SysPushRecord extends Model<SysPushRecord> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId
    private Integer id;
    /**
   * 消息id
   */
    private String msgId;
    /**
   * 渠道编码
   */
    private String channelId;
    /**
   * 平台： android,ios,winphone,all
   */
    private String platform;
    /**
   * 指定推送设备
   */
    private String audience;
    /**
   * 通知内容体
   */
    private String title;
    /**
   * 消息内容体
   */
    private String content;
    /**
   * 参数
   */
    private String options;
    /**
   * 回调参数
   */
    private String callback;


    private Integer configId;

    private Integer sendSys;

    private Integer platformId;

    private Integer timeType;

    private Date retainTime;
    /**
   * 推送时间
   */
    private LocalDateTime pushTime;
    /**
   * 推送操作人
   */
    private String pushBy;


    private LocalDateTime callbackTime;


    private Long timeLong;


    private Integer sendType;
}
