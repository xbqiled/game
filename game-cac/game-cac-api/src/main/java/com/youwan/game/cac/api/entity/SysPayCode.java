package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-05-10 11:23:04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_pay_code")
public class SysPayCode extends Model<SysPayCode> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId
    private Integer id;
    /**
   * 订单号
   */
    private String orderNo;
    /**
   * 二维码地址
   */
    private String imgUrl;
    /**
     * 支付地址
     */
    private String payUrl;
    /**
   * 渠道編碼
   */
    private String channelId;
    /**
   * 配置信息
   */
    private Integer configId;
    /**
   * 支付金额度
   */
    private BigDecimal payFee;
    /**
   * 支付时间
   */
    private LocalDateTime createTime;
  
}
