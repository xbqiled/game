package com.youwan.game.cac.api.dto;


import lombok.Data;




@Data
public class PayOrderDto {

    private String channelId;

    private String userId;

    private String configId;

    private String payfee;

    private String payaccount;

    private String payname;

    private String bankCode;

}
