package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-21 19:13:24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_ai")
public class Ai extends Model<Ai> {
private static final long serialVersionUID = 1L;

    /**
   * 机器人账号id
   */
    private Integer tId;
    /**
   * 机器人昵称
   */
    private String tNick;
    /**
   * 机器人头像
   */
    private Integer tAvatarid;
  
}
