package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-04-19 13:17:59
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wx_short_url")
public class ShortUrl extends Model<ShortUrl> {
private static final long serialVersionUID = 1L;

  /**
   * 
   */
    @TableId
    private Integer id;
    /**
     * 类型短链接类型
     */
    private Integer type;
    /**
     * 短网址名字
     */
    private String name;
    /**
     * 请求URL
     */
    private String url;
    /**
     * 请求类型
     */
    private Integer reqType;

    /**
     * 参数位置
     */
    private Integer replacePostion;
    /**
     * URL参数
     */
    private String urlPara;
    /**
     * 头部json
     */
    private String headJson;
    /**
     * 请求字符串信息
     */
    private String bodyJson;
    /**
     * 1,正常 2,失效
     */
    private Integer status;
    /**
     * 请求次数
     */
    private Integer reqCount;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
  
}
