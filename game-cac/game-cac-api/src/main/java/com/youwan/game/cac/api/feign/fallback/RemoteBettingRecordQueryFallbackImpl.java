package com.youwan.game.cac.api.feign.fallback;

import com.youwan.game.cac.api.dto.BettingRecordQueryDto;
import com.youwan.game.cac.api.feign.RemoteBettingRecordQuery;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.data.core.Page;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-28.
 */
@Slf4j
@Component
public class RemoteBettingRecordQueryFallbackImpl implements RemoteBettingRecordQuery {

    @Setter
    private Throwable cause;

    @Override
    public R queryLotteryRecord(Integer userId, BettingRecordQueryDto dto, String from) {
        log.error("feign query Lottery betting records fail", cause);
        return new R(Page.empty());
    }

    @Override
    public R queryEGameRecord(Integer userId, BettingRecordQueryDto dto, String from) {
        log.error("feign query EGame betting records fail", cause);
        return new R(Page.empty());
    }

    @Override
    public R queryLiveRecord(Integer userId, BettingRecordQueryDto dto, String from) {
        log.error("feign query Live betting records fail", cause);
        return new R(Page.empty());
    }

    @Override
    public R queryTPSportRecord(Integer userId, BettingRecordQueryDto dto, String from) {
        log.error("feign query 3rd sport betting records fail", cause);
        return new R(Page.empty());
    }

    @Override
    public R queryCrownSportRecord(Integer userId, BettingRecordQueryDto dto, String from) {
        log.error("feign query crown sport betting records fail", cause);
        return new R(Page.empty());
    }
}
