package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-06 13:34:56
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wx_cash_order")
public class WxCashOrder extends Model<WxCashOrder> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId
    private Integer id;
    /**
   * 订单号
   */
    private String cashOrder;
    /**
   * 渠道编码
   */
    private String channelId;
    /**
   * 用户id
   */
    private Integer userId;
    /**
   * 微信账号
   */
    private String wechatAccount;
    /**
   * 状态，0:等待审核, 1,审核成功, 2,审核失败
   */
    private Integer status;
    /**
   * 提现金额
   */
    private BigDecimal cashFee;
    /**
   * 创建人
   */
    private String createBy;
    /**
   * 创建时间
   */
    private LocalDateTime createTime;
    /**
   * 审批人
   */
    private LocalDateTime modifyTime;
    /**
   * 如果系统处理，那么记录SYSTME, 手动处理记录编码
   */
    private String modifyBy;
    /**
   * 备注
   */
    private String note;
  
}
