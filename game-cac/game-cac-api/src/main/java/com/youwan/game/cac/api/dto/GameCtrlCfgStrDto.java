package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class GameCtrlCfgStrDto implements Serializable {

    private String startTime;

    private String endTime;

}
