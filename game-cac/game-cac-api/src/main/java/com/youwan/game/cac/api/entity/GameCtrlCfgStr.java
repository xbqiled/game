package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * VIEW
 *
 * @author code generator
 * @date 2020-04-14 19:28:20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_gamectrlcfgstr")
public class GameCtrlCfgStr extends Model<GameCtrlCfgStr> {
private static final long serialVersionUID = 1L;

    /**
   * 唯一key
   */
    @TableId
    private String tKey;
    /**
   * 配置时间
   */
    private Integer tCfgtime;
    /**
   * 游戏控制配置信息
   */
    private byte[] tCfginfo;
  
}
