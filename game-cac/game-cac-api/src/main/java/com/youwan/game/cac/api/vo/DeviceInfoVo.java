package com.youwan.game.cac.api.vo;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-05-15 15:24:30
 */
@Data
public class DeviceInfoVo implements Serializable  {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    private Integer id;
    /**
   * 渠道编码
   */
    private String channelId;
    /**
   * IP地址
   */
    private String ip;
    /**
   * 推广人信息
   */
    private String promoterId;
    /**
   * 参数信息
   */
    private String paraInfo;
    /**
   * 比较次数
   */
    private Integer compareCount;
    private  String userAgent;
    private  String os;
    private  String osVersion;
    private  String appVersion;

    private  String colorDepth;
    private  String deviceMemory;
    private  String deviceType;

    private  String fingerprint;
    private  String hardwareConcurrency;
    private  String language;

    private  String netWork;
    private  String screenHeight;
    private  String screenWidth;
    /**
   * 状态:  正常1， 拉黑0
   */
    private Integer status;
    /**
   * 创建时间
   */
    private LocalDateTime createTime;
  
}
