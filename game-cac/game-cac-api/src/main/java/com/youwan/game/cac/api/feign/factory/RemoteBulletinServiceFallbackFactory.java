package com.youwan.game.cac.api.feign.factory;


import com.youwan.game.cac.api.feign.RemoteBulletinService;
import com.youwan.game.cac.api.feign.fallback.RemoteBulletinServiceFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author lion
 * @date 2019/2/14
 */
@Component
public class RemoteBulletinServiceFallbackFactory implements FallbackFactory<RemoteBulletinService> {



    @Override
    public RemoteBulletinService create(Throwable throwable) {
        RemoteBulletinServiceFallbackImpl remoteBulletinServiceFallbackImpl = new RemoteBulletinServiceFallbackImpl();
        remoteBulletinServiceFallbackImpl.setCause(throwable);
        return remoteBulletinServiceFallbackImpl;
    }
}
