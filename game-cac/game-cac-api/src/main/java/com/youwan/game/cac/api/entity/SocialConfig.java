package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-03-26 15:43:34
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_social_config")
public class SocialConfig extends Model<SocialConfig> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId
    private Integer id;
    /**
   * 类型
   */
    private String type;
    /**
   * 微信app_id
   */
    private String appId;
    /**
   * 微信app_secret
   */
    private String appSecret;
    /**
   * 渠道编码
   */
    private String channelId;
    /**
   * bundleId
   */
    private String bundleId;
    /**
   * 签名
   */
    private String androidSign;
    /**
   * 包名
   */
    private String packageName;
    /**
   * 状态， 1，正常， 0失效
   */
    private Integer status;
    /**
   * 创建人
   */
    private String createBy;
    /**
   * 创建时间
   */
    private LocalDateTime createTime;
  
}
