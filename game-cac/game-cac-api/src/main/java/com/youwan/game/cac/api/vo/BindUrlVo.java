package com.youwan.game.cac.api.vo;

import lombok.Data;

@Data
public class BindUrlVo {
    private String url;

    private Long accountId;

    private String channelId;

    private String channelName;

    private String iconUrl;
}
