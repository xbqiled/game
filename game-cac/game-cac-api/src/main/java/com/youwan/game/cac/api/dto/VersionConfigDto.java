package com.youwan.game.cac.api.dto;


import lombok.Data;



@Data
public class VersionConfigDto {

    private String channelId;

    private String version;

    public VersionConfigDto(String channelId, String version) {
        this.channelId = channelId;
        this.version = version;
    }

}
