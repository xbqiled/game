package com.youwan.game.cac.api.feign.factory;


import com.youwan.game.cac.api.feign.RemoteSmsService;
import com.youwan.game.cac.api.feign.fallback.RemoteSmsServiceFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author lion
 * @date 2019/2/14
 */
@Component
public class RemoteSmsServiceFallbackFactory implements FallbackFactory<RemoteSmsService> {



    @Override
    public RemoteSmsService create(Throwable throwable) {
        RemoteSmsServiceFallbackImpl remoteSmsServiceFallbackImpl = new RemoteSmsServiceFallbackImpl();
        remoteSmsServiceFallbackImpl.setCause(throwable);
        return remoteSmsServiceFallbackImpl;
    }
}
