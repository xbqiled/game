package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-19 14:07:23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("other_channel_account")
public class ChannelAccount extends Model<ChannelAccount> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId
    private Integer id;
    /**
   * 
   */
    private String channelId;
    /**
   * 
   */
    private String channelName;
    /**
   * 渠道余额
   */
    private BigDecimal videoBalance;
    /**
   * 类型编码
   */
    private BigDecimal sumIncrease;
    /**
   * 第三方平台编码
   */
    private BigDecimal sumReduce;
    /**
     * 类型: 真人
     */
    private Integer gameType;
    /**
   * 修改时间
   */
    private LocalDateTime modifyTime;
    /**
   * 修改人
   */
    private String modifyBy;
  
}
