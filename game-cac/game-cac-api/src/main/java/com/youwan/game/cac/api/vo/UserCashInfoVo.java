package com.youwan.game.cac.api.vo;

import lombok.Data;

import java.io.Serializable;


@Data
public class UserCashInfoVo implements Serializable {

    private String bank;
    private String bankRealName;
    private String bankPayId;
    private String subBank;
    private String aliPayId;
    private String aliRealName;

}
