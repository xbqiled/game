package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class GameFqzsCtrlCfg implements Serializable {

    Integer sixInterval;

    Integer eightInterval;

    Integer twelveInterval;

    Integer globalInterval;

    Integer killAllInterval;

}
