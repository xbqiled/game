package com.youwan.game.cac.api.entity;

import lombok.Data;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-05-11.
 */
@Data
public class ProxyMemberProfile {

    private Long myId;
    private Long pid;
    /**
     * 直属成员数量
     */
    private Long immediateMembers = 0l;

    /**
     * 所有成员数量
     */
    private Long allMembers = 0l;
    /**
     * 今日新增会员
     */
    private Long todayMembers;
    /**
     * 今日新增直属会员
     */
    private Long todayImmMembers;

    /**
     * 今日佣金
     */
    private Double todayRebate = 0d;

    /**
     * 昨日佣金
     */
    private Double yesterdayRebate = 0d;

    /**
     * 总佣金
     */
    private Double totalRebate = 0d;

    /**
     *
     */
    private Double withdrawableRebate = 0d;
}
