package com.youwan.game.cac.api.feign;

import com.youwan.game.cac.api.dto.BettingRecordQueryDto;
import com.youwan.game.cac.api.feign.factory.RemoteBettingRecordQueryFallbackFactory;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.constant.ServiceNameConstant;
import com.youwan.game.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-28.
 */
@FeignClient(value = ServiceNameConstant.CAC_SERVICE, fallbackFactory = RemoteBettingRecordQueryFallbackFactory.class)
public interface RemoteBettingRecordQuery {

    @PostMapping("/bettingRecord/lotteries/{userId}")
    R queryLotteryRecord(@PathVariable("userId") Integer userId, @RequestBody BettingRecordQueryDto dto,
                                               @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/bettingRecord/egame/{userId}")
    R queryEGameRecord(@PathVariable("userId") Integer userId, @RequestBody BettingRecordQueryDto dto,
                                                           @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/bettingRecord/live/{userId}")
    R queryLiveRecord(@PathVariable("userId") Integer userId, @RequestBody BettingRecordQueryDto dto,
                                                         @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/bettingRecord/3rdsport/{userId}")
    R queryTPSportRecord(@PathVariable("userId") Integer userId, @RequestBody BettingRecordQueryDto dto,
                                                              @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/bettingRecord/crownsport/{userId}")
    R queryCrownSportRecord(@PathVariable("userId") Integer userId, @RequestBody BettingRecordQueryDto dto,
                                                          @RequestHeader(SecurityConstants.FROM) String from);
}
