package com.youwan.game.cac.api.vo;

import lombok.Data;

import java.io.Serializable;


@Data
public class DownloadPanelConfigVo1 implements Serializable {

    /**
     * 渠道编码
     */
    private Integer channelId;
    /**
     * 渠道名称
     */
    private String channelName;

    /**
     * iconUrl地址
     */
    private String iconUrl;
    /**
     * logURL
     */
    private String logoUrl;
    /**
     * 客服url
     */
    private String csUrl;
    /**
     * 客服提示
     */
    private String csTips;
    /**
     * 活动信息
     */
    private String activityMsg;
    /**
     * 背景url
     */
    private String backgroundUrl;

}
