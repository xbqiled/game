package com.youwan.game.cac.api.dto;


import lombok.Data;

import java.io.Serializable;

@Data
public class BgExitGameDto implements Serializable {

    private String userId;

    private Long startTime;

    private Long endTime;

    public BgExitGameDto(String userId, Long startTime, Long endTime) {
        this.userId = userId;
        this.startTime = startTime;
        this.endTime = endTime;
    }
}
