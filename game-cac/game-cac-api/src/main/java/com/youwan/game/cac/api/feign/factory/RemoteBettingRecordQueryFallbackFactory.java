package com.youwan.game.cac.api.feign.factory;

import com.youwan.game.cac.api.feign.RemoteBettingRecordQuery;
import com.youwan.game.cac.api.feign.fallback.RemoteBettingRecordQueryFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-28.
 */
@Component
public class RemoteBettingRecordQueryFallbackFactory implements FallbackFactory<RemoteBettingRecordQuery> {

    @Override
    public RemoteBettingRecordQuery create(Throwable throwable) {
        RemoteBettingRecordQueryFallbackImpl remoteBettingRecordQueryFallback = new RemoteBettingRecordQueryFallbackImpl();
        remoteBettingRecordQueryFallback.setCause(throwable);
        return remoteBettingRecordQueryFallback;
    }
}
