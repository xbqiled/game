package com.youwan.game.cac.api.dto;


import lombok.Data;

import java.io.Serializable;

@Data
public class BgUserDto implements Serializable {
    private String channelId;
    private String userId;
    private String nickName;


    public BgUserDto (String channelId, String userId, String nickName) {
        this.channelId = channelId;
        this.userId = userId;
        this.nickName = nickName;
    }

    public BgUserDto (String channelId, String userId) {
        this.channelId = channelId;
        this.userId = userId;
    }
}
