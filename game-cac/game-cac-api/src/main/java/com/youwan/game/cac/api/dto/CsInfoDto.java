package com.youwan.game.cac.api.dto;


import lombok.Data;

import java.io.Serializable;

@Data
public class CsInfoDto implements Serializable {
    private String channelId;
    private String type;

    public CsInfoDto(String channelId, String type) {
        this.channelId = channelId;
        this.type = type;
    }
}
