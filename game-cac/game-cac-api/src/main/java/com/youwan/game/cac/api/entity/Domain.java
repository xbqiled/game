package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * @author code generator
 * @date 2019-04-19 13:18:14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wx_domain")
public class Domain extends Model<Domain> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private Integer id;
    /**
     * 渠道ID
     */
    private String channelId;
    /**
     * 域名信息
     */
    private String domainUrl;
    /**
     * 对应子域名的个数
     */
    private Integer num;
    /**
     * 状态: 1,正常2，失效3，被拉黑
     */
    private Integer status;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

}
