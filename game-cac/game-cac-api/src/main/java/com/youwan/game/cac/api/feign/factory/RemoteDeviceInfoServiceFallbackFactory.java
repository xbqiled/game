package com.youwan.game.cac.api.feign.factory;


import com.youwan.game.cac.api.feign.RemoteDeviceInfoService;
import com.youwan.game.cac.api.feign.fallback.RemoteDeviceInfoServiceFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author lion
 * @date 2019/2/14
 */
@Component
public class RemoteDeviceInfoServiceFallbackFactory implements FallbackFactory<RemoteDeviceInfoService> {


    @Override
    public RemoteDeviceInfoService create(Throwable throwable) {
        RemoteDeviceInfoServiceFallbackImpl remoteDeviceInfoServiceFallbackImpl = new RemoteDeviceInfoServiceFallbackImpl();
        remoteDeviceInfoServiceFallbackImpl.setCause(throwable);
        return remoteDeviceInfoServiceFallbackImpl;
    }
}
