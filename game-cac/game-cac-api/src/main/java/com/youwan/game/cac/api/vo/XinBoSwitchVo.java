package com.youwan.game.cac.api.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class XinBoSwitchVo implements Serializable {

    private String channelId;

    private String channelName;

    private String iconUrl;

    private Integer cfgTime;

    private byte[] onOffInfo;

    private Integer functionType;

    private Integer openType;

    private Boolean openTypeBoolean;

}
