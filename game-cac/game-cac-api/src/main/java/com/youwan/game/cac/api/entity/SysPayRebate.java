package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-06-05 14:11:47
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_pay_rebate")
public class SysPayRebate extends Model<SysPayRebate> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private Integer id;
    /**
   * 订单号
   */
    private String orderNo;
    /**
   * 用户ID
   */
    private Integer userId;
    /**
   * 1,支付成功
   */
    private Integer status;
    /**
   * 渠道编码
   */
    private String channelId;
    /**
   * 返利金额
   */
    private BigDecimal rebateFee;
    /**
   * 返利类型
   */
    private Integer rebateType;
    /**
   * 创建时间， 1,创建时间
   */
    private LocalDateTime createTime;
    /**
   * 创建人
   */
    private String createBy;
  
}
