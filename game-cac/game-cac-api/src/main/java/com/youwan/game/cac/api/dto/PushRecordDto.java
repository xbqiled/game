package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class PushRecordDto implements Serializable {


    private String channelId;
    private String startTime;
    private String endTime;

}