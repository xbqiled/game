package com.youwan.game.cac.api.feign;


import com.youwan.game.cac.api.dto.*;
import com.youwan.game.cac.api.entity.BgenterConfig;
import com.youwan.game.cac.api.entity.OtherBgUser;
import com.youwan.game.cac.api.entity.TChannel;
import com.youwan.game.cac.api.feign.factory.RemoteChannelServiceFallbackFactory;
import com.youwan.game.cac.api.vo.DaMaValue;
import com.youwan.game.cac.api.vo.DownloadPanelConfigVo;
import com.youwan.game.cac.api.vo.DownloadPanelConfigVo1;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.constant.ServiceNameConstant;
import com.youwan.game.common.core.otherplatform.bg.BGPlatfromConfig;
import com.youwan.game.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * <B>渠道服务feign</B>
 */
@FeignClient(value = ServiceNameConstant.CAC_SERVICE, fallbackFactory = RemoteChannelServiceFallbackFactory.class)
public interface RemoteChannelService {

    @GetMapping("/tchannel/channelInfo/{channleId}")
    R<TChannel> getChannelInfo(@PathVariable("channleId") String channleId, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/bguser/finduser")
    R<OtherBgUser> findBgUser(@RequestBody BgUserDto bgUserDto, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/bguser/createuser")
    R<OtherBgUser> createBgUser(@RequestBody BgUserDto bgUserDto, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/bguser/opengame")
    R<String> openBgGame(@RequestBody OpenBgGameDto openBgGameDto, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/bguser/userbalance")
    R<Map<String, BigDecimal>> bgUserBalance(@RequestBody BgUserBalanceDto bgUserBalanceDto, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/bguser/transferbalance")
    R<Integer> bgTransferbalance(@RequestBody BgTransferBalanceDto bgTransferBalanceDto, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/bguser/getBgConfig")
    R<BGPlatfromConfig> getbgConfig(@RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/bguser/exitgame")
    R<BgDamaGameDto> exitGame(@RequestBody BgExitGameDto bgExitGameDto, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/bguser/getenterconfig")
    R<BgenterConfig> getBgEnterConfig(@RequestBody BgEnterConfig bgEnterConfig, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/downloadpanelcfg/getconfig")
    R<DownloadPanelConfigVo> getDownloadPanelConfig(@RequestBody DownloadConfigDto downloadConfigDto, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/downloadpanelcfg1/getconfig")
    R<DownloadPanelConfigVo1> getDownloadPanelConfig1(@RequestBody DownloadConfigDto downloadConfigDto, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/accountinfo/getUserDamaValue")
    R<List<DaMaValue>> getUserDamaValue(@RequestBody DaMaValueDto daMaValueDto, @RequestHeader(SecurityConstants.FROM) String from);

}
