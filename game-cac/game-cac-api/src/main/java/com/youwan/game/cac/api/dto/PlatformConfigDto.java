package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class PlatformConfigDto implements Serializable {

    private Integer id;

    private String platformCode;

    private Integer type;

    //请求url
    private String apiUrl;

    //厅代码
    private String sn;

    //代理账号
    private String agentAccount;

    //代理密码
    private String agentPassword;

    //代理秘钥
    private String secretKey;

    //前缀
    private String prefix;
}
