package com.youwan.game.cac.api.feign.factory;

import com.youwan.game.cac.api.feign.RemoteProxyMemberService;
import com.youwan.game.cac.api.feign.fallback.RemoteProxyMemberServiceFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-04-27.
 */
@Component
public class RemoteProxyMemberServiceFallbackFactory implements FallbackFactory<RemoteProxyMemberService> {


    @Override
    public RemoteProxyMemberService create(Throwable throwable) {
        RemoteProxyMemberServiceFallbackImpl remoteProxyMemberServiceFallback = new RemoteProxyMemberServiceFallbackImpl();
        remoteProxyMemberServiceFallback.setCause(throwable);
        return remoteProxyMemberServiceFallback;
    }
}
