package com.youwan.game.cac.api.feign;


import com.youwan.game.cac.api.feign.factory.RemoteDownloadServiceFallbackFactory;
import com.youwan.game.cac.api.vo.DownloadConfigVo;
import com.youwan.game.cac.api.vo.DownloadConfigVo1;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.constant.ServiceNameConstant;
import com.youwan.game.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;


/**
 * <B>添加服务用来处理下载相关信息</B>
 * @author lion
 * @date 2019/02/14
 * feign token
 */
@FeignClient(value = ServiceNameConstant.CAC_SERVICE, fallbackFactory = RemoteDownloadServiceFallbackFactory.class)
public interface RemoteDownloadService {


    /**
     * 通过渠道编码查询渠道对应下载信息
     *
     * @param channelId 渠道编码
     * @return R
     */
    @GetMapping("/downconfig/configByChannelId/{channelId}")
    R<DownloadConfigVo> downConfigByChannelId(@PathVariable("channelId") String channelId, @RequestHeader(SecurityConstants.FROM) String from);


    @GetMapping("/downconfig/config1ByChannelId/{channelId}")
    R<DownloadConfigVo1> downConfig1ByChannelId(@PathVariable("channelId") String channelId, @RequestHeader(SecurityConstants.FROM) String from);
}
