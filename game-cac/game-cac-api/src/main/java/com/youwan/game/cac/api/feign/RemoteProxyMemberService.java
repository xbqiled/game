package com.youwan.game.cac.api.feign;

import com.youwan.game.cac.api.dto.ProxyMemberQueryDto;
import com.youwan.game.cac.api.entity.ProxyMember;
import com.youwan.game.cac.api.feign.factory.RemoteProxyMemberServiceFallbackFactory;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.constant.ServiceNameConstant;
import com.youwan.game.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-04-27.
 */
@FeignClient(value = ServiceNameConstant.CAC_SERVICE, fallbackFactory = RemoteProxyMemberServiceFallbackFactory.class)
public interface RemoteProxyMemberService {

    @GetMapping("/proxyMember/getMemberProfile/{accountId}")
    R getMemberProfile(@PathVariable("accountId") Long accountId, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/proxyMember/getDirectSubordinates/{accountId}")
    R getDirectSubordinates(@PathVariable("accountId") Long accountId, @RequestBody ProxyMemberQueryDto dto,
                            @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/proxyMember/getAllSubordinates/{accountId}")
    R getAllSubordinates(@PathVariable("accountId") Long accountId, @RequestBody ProxyMemberQueryDto dto,
                         @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/proxyMember/getTreasureChanges/{accountId}")
    R getTreasureChanges(@PathVariable("accountId") Long accountId, @RequestBody ProxyMemberQueryDto queryDto,
                         @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/proxyMember/getRechargeOrders/{accountId}")
    R getRechargeOrders(@PathVariable("accountId") Long accountId, @RequestBody ProxyMemberQueryDto queryDto,
                        @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/proxyMember/getCashOrders/{accountId}")
    R getCashOrders(@PathVariable("accountId") Long accountId, @RequestBody ProxyMemberQueryDto queryDto,
                    @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/proxyMember/getGameScoreStats/{accountId}")
    R getGameScoreStats(@PathVariable("accountId") Long accountId,
                        @RequestBody ProxyMemberQueryDto queryDto,
                        @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/proxyMember/getGameScoreDetails/{accountId}")
    R getGameScores(@PathVariable("accountId") Long accountId, @RequestBody ProxyMemberQueryDto queryDto,
                    @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/proxyMember/getTeamMetrics/{accountId}")
    R getTeamMetrics(@PathVariable("accountId") Long accountId, @RequestBody ProxyMemberQueryDto queryDto,
                     @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/proxyMember/getSubordinateTeamMetrics/{accountId}")
    R getSubordinateTeamMetrics(@PathVariable("accountId") Long accountId, @RequestBody ProxyMemberQueryDto queryDto,
                                @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/proxyMember/getRebateDetails/{accountId}")
    R getRebateDetails(@PathVariable("accountId") Long accountId, @RequestBody ProxyMemberQueryDto queryDto,
                       @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/proxyMember/getRebateBalances/{accountId}")
    R getRebateBalances(@PathVariable("accountId") Long accountId, @RequestBody ProxyMemberQueryDto queryDto,
                        @RequestHeader(SecurityConstants.FROM) String from);
}
