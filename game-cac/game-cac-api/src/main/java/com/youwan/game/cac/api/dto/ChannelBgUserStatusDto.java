package com.youwan.game.cac.api.dto;


import lombok.Data;

import java.io.Serializable;

@Data
public class ChannelBgUserStatusDto implements Serializable {

    private String userId;
    private String status;

}
