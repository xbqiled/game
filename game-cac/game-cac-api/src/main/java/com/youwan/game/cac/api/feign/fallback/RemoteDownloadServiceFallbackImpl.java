package com.youwan.game.cac.api.feign.fallback;


import com.youwan.game.cac.api.feign.RemoteDownloadService;
import com.youwan.game.cac.api.vo.DownloadConfigVo;
import com.youwan.game.cac.api.vo.DownloadConfigVo1;
import com.youwan.game.common.core.util.R;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


/**
 * @author lion
 * @date 2019/02/14
 * feign token  fallback
 */
@Slf4j
@Component
public class RemoteDownloadServiceFallbackImpl implements RemoteDownloadService {

    @Setter
    private Throwable cause;

    /**
     * <B> 获取下载信息</B>
     * @param channelId
     * @return
     */
    public R<DownloadConfigVo> downConfigByChannelId(String channelId, String from) {
        log.error("feign 获取下载配置信息失败", cause);
        return null;
    }

    @Override
    public R<DownloadConfigVo1> downConfig1ByChannelId(String channelId, String from) {
        log.error("feign 获取下载配置信息失败", cause);
        return null;
    }
}
