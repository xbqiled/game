package com.youwan.game.cac.api.dto;


import lombok.Data;

import java.io.Serializable;

@Data
public class UserCtrlDto implements Serializable {

    private String accountId;

    private String type;

    private String betLimit;

    private String rate;

    private String round;

    private String roomList;

    private String channelId;


}
