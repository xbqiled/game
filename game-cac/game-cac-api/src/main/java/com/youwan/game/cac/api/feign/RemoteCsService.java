package com.youwan.game.cac.api.feign;


import com.youwan.game.cac.api.dto.CsInfoDto;
import com.youwan.game.cac.api.dto.NotifyPayOrderDto;
import com.youwan.game.cac.api.entity.CsInfo;
import com.youwan.game.cac.api.feign.factory.RemoteCsServiceFallbackFactory;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.constant.ServiceNameConstant;
import com.youwan.game.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;


/**
 * <B>客服信息接口</B>
 */
@FeignClient(value = ServiceNameConstant.CAC_SERVICE, fallbackFactory = RemoteCsServiceFallbackFactory.class)
public interface RemoteCsService {

    @PostMapping("/csinfo/getCsInfoById/{channelId}")
    R<List<CsInfo>> queryCsInfoById(@PathVariable("channelId") String channelId, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/csinfo/getCsInfo/")
    R queryCsInfo(@RequestBody CsInfoDto csInfoDto, @RequestHeader(SecurityConstants.FROM) String from);
}
