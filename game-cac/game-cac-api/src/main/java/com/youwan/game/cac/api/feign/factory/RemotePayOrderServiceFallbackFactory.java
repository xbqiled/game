package com.youwan.game.cac.api.feign.factory;


import com.youwan.game.cac.api.feign.RemotePayOrderService;
import com.youwan.game.cac.api.feign.RemotePayService;
import com.youwan.game.cac.api.feign.fallback.RemotePayOrderServiceFallbackImpl;
import com.youwan.game.cac.api.feign.fallback.RemotePayServiceFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author lion
 * @date 2019/2/14
 */
@Component
public class RemotePayOrderServiceFallbackFactory implements FallbackFactory<RemotePayOrderService> {



    @Override
    public RemotePayOrderService create(Throwable throwable) {
        RemotePayOrderServiceFallbackImpl remotePayOrderServiceFallbackImpl = new RemotePayOrderServiceFallbackImpl();
        remotePayOrderServiceFallbackImpl.setCause(throwable);
        return remotePayOrderServiceFallbackImpl;
    }
}
