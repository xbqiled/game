package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 *
 * @author code generator
 * @date 2019-03-19 13:02:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_mailTemplate")
public class Mailtemplate extends Model<Mailtemplate> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId
    private Integer id;
    /**
   * 
   */
    private String idKey;
    /**
   * 
   */
    private String title;
    /**
   * 
   */
    private String content;
    /**
   * 
   */
    private String sender;
  
}
