package com.youwan.game.cac.api.feign;


import com.youwan.game.cac.api.feign.factory.RemoteSocialConfigServiceFallbackFactory;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.constant.ServiceNameConstant;
import com.youwan.game.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;



/**
 * <B> 支付订单feign</B>
 */
@FeignClient(value = ServiceNameConstant.CAC_SERVICE, fallbackFactory = RemoteSocialConfigServiceFallbackFactory.class)
public interface RemoteSocialConfigService {

    @PostMapping("/socialconfig/getConfig/{channelId}")
    R getSocialConfigById(@PathVariable("channelId") String channelId, @RequestHeader(SecurityConstants.FROM) String from);
}
