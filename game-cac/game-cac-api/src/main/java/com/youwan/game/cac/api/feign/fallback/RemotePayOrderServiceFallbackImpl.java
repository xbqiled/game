package com.youwan.game.cac.api.feign.fallback;


import com.youwan.game.cac.api.dto.AddPointDto;
import com.youwan.game.cac.api.dto.NotifyPayOrderDto;
import com.youwan.game.cac.api.dto.PayOrderDto;
import com.youwan.game.cac.api.feign.RemotePayOrderService;
import com.youwan.game.cac.api.vo.NotifyOrderVo;
import com.youwan.game.cac.api.vo.PayCodeVo;
import com.youwan.game.cac.api.vo.PayConfigVo;
import com.youwan.game.cac.api.vo.PayResult;
import com.youwan.game.common.core.util.R;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author lion
 * @date 2019/02/14
 * feign token  fallback
 */
@Slf4j
@Component
public class RemotePayOrderServiceFallbackImpl implements RemotePayOrderService {

    @Setter
    private Throwable cause;

    @Override
    public R<PayResult> createPayOrder(PayOrderDto payOrderDto, String from) {
        log.error("feign 获取支付订单信息失败", cause);
        return null;
    }

    @Override
    public R callbackPayOrdery(NotifyPayOrderDto payOrderDto, String from) {
        log.error("feign 回调支付订单信息失败", cause);
        return null;
    }

    @Override
    public R addPoint(AddPointDto addPointDto, String from) {
        log.error("feign 银商上分失败", cause);
        return null;
    }

    @Override
    public R<PayConfigVo> getPayConfig(String merchantCode, String from) {
        log.error("feign 回调支付配置信息失败", cause);
        return null;
    }

    @Override
    public R getPayOrder(String orderId, String from) {
        log.error("feign 获取支付订单信息失败", cause);
        return null;
    }

    @Override
    public R<NotifyOrderVo> getMerchantKey(String orderId, String from) {
        log.error("feign 获取秘钥信息失败", cause);
        return null;
    }

    @Override
    public R getPayUrl(PayOrderDto payOrderDto, String from) {
        log.error("feign 获取支付URL信息失败", cause);
        return null;
    }

    @Override
    public R<PayConfigVo> getPayInfo(PayOrderDto payOrderDto, String from) {
        log.error("feign 获取支付详情失败", cause);
        return null;
    }

    @Override
    public R<PayCodeVo> getPayCode(String orderNo, String from) {
        log.error("feign 获取支付二维码信息失败", cause);
        return null;
    }
}
