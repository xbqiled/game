package com.youwan.game.cac.api.entity;

import lombok.Data;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-05-12.
 */
@Data
public class ProxyMemberRebateDetail {

    private Long accountId;
    private String nickName;
    private Long promoterID;
    private String promoterNickName;
    private Integer level;
    private Integer count;
    private Long recordTime;
    private String channelNO;

}
