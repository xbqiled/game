package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * sharded table
 *
 * @author code generator
 * @date 2019-06-05 14:15:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_depositChannel")
public class Depositchannel extends Model<Depositchannel> {
private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId
    private String tChannel;
    /**
     *
     */
    private byte[] tCfginfo;
    /**
     *
     */
    private Long tStarttime;
    /**
     *
     */
    private Long tEndtime;
    /**
     *
     */
    private Integer tOpentype;
  
}
