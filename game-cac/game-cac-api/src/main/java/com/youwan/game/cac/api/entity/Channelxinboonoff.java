package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * sharded table
 *
 * @author code generator
 * @date 2019-12-15 13:50:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_channelxinboonoff")
public class Channelxinboonoff extends Model<Channelxinboonoff> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private String tChannel;
    /**
   * 
   */
    private Integer tCfgtime;
    /**
   * 
   */
    private byte[] tOnoffinfo;
  
}
