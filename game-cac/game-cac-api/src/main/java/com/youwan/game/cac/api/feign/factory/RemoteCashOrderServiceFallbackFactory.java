package com.youwan.game.cac.api.feign.factory;


import com.youwan.game.cac.api.feign.RemoteCashOrderService;
import com.youwan.game.cac.api.feign.RemoteSmsService;
import com.youwan.game.cac.api.feign.fallback.RemoteCashOrderServiceFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author lion
 * @date 2019/2/14
 */
@Component
public class RemoteCashOrderServiceFallbackFactory implements FallbackFactory<RemoteCashOrderService> {



    @Override
    public RemoteCashOrderService create(Throwable throwable) {
        RemoteCashOrderServiceFallbackImpl remoteCashOrderServiceFallbackImpl = new RemoteCashOrderServiceFallbackImpl();
        remoteCashOrderServiceFallbackImpl.setCause(throwable);
        return remoteCashOrderServiceFallbackImpl;
    }
}
