package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;


@Data
public class RechargeDto implements Serializable {

    private String userId;
    private String payFee;
    private String depositor;

    private String account;
    private String remark;
    private String payType;

    private Double damaMulti;
    private Integer rate;
}
