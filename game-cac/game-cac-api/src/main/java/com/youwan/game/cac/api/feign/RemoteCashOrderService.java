package com.youwan.game.cac.api.feign;


import com.youwan.game.cac.api.dto.CashOrderDto;
import com.youwan.game.cac.api.feign.factory.RemoteCashOrderServiceFallbackFactory;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.constant.ServiceNameConstant;
import com.youwan.game.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * <B> 提现申请feign</B>
 */
@FeignClient(value = ServiceNameConstant.CAC_SERVICE, fallbackFactory = RemoteCashOrderServiceFallbackFactory.class)
public interface RemoteCashOrderService {

    @PostMapping("/cashorder/createOrder/")
    R createCashOrder(@RequestBody CashOrderDto cashOrderDto, @RequestHeader(SecurityConstants.FROM) String from);


    @PostMapping("/cashorder/getOrderByUserId/{userId}")
    R getOrderByUserId(@PathVariable("userId")  String userId, @RequestHeader(SecurityConstants.FROM) String from);


    @PostMapping("/cashorder/getCashOrderInfo/")
    R getCashOrderInfo(@RequestBody CashOrderDto cashOrderDto, @RequestHeader(SecurityConstants.FROM) String from);
}
