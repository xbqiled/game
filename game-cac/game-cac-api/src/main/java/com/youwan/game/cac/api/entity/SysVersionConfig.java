package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-02-08 16:36:30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_version_config")
public class SysVersionConfig extends Model<SysVersionConfig> {
private static final long serialVersionUID = 1L;

    /**
   * ID
   */
    @TableId
    private Integer id;
    /**
   * 渠道ID
   */
    private Integer channelId;
    /**
   * 更新地址
   */
    private String updateUrl;
    /**
   * 是否删除
   */
    private String isClear;
    /**
   * 版本
   */
    private String version;
    /**
   * 创建时间
   */
    private LocalDateTime createTime;
    /**
   * 创建人
   */
    private String createBy;
    /**
   * 修改时间
   */
    private LocalDateTime modifyTime;
    /**
   * 修改人
   */
    private String modifyBy;
  
}
