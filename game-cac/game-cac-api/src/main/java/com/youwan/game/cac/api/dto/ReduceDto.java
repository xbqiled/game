package com.youwan.game.cac.api.dto;


import lombok.Data;

import java.io.Serializable;


@Data
public class ReduceDto implements Serializable {

    private String userId;
    private String payFee;
    private Integer force;
    private String note;

}
