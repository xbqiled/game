package com.youwan.game.cac.api.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class SuccourForm implements Serializable {
    private String channelId;
    private Integer openType;
    private Long coinLimit;
    private Long succourAward;
    private Integer totalSuccourCnt;
    private Integer shareConditionCnt;
}
