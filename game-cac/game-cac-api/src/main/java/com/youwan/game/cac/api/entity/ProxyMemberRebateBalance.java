package com.youwan.game.cac.api.entity;

import lombok.Data;

import javax.persistence.Id;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-05-12.
 */
@Data
public class ProxyMemberRebateBalance {

    private Integer count;
    private Long recordTime;
    @Id
    private String id;
    private String channelNO;
}
