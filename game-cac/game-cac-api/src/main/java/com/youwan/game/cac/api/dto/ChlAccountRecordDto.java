package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ChlAccountRecordDto implements Serializable {

    private String channelId;

    private String startTime;

    private String endTime;

}
