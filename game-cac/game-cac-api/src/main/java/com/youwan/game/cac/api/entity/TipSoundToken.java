package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-05-21 11:05:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("tip_sound_token")
public class TipSoundToken extends Model<TipSoundToken> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId
    private Integer id;
    /**
   * 渠道编码
   */
    private String channelId;
    /**
   * 
   */
    private String accessToken;
    /**
   * 
   */
    private String sessionKey;
    /**
   * 
   */
    private String scope;
    /**
   * 
   */
    private String refreshToken;
    /**
   * 
   */
    private String sessionSecret;
    /**
   * 
   */
    private LocalDateTime expiresTime;
    /**
   * 创建时间
   */
    private LocalDateTime createTime;
  
}
