package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;


@Data
public class AddPointDto implements Serializable {

    private String amount;
    private String userId;
    private String operateUserId;
    private String orgId;
    private String channelId;

    public AddPointDto(String amount, String userId, String operateUserId, String orgId, String channelId) {
        this.amount = amount;
        this.userId = userId;
        this.operateUserId = operateUserId;
        this.orgId = orgId;
        this.channelId = channelId;
    }


}
