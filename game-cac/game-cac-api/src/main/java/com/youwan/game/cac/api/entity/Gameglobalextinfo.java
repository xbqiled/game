package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * VIEW
 *
 * @author code generator
 * @date 2020-05-20 16:19:40
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_gameglobalextinfo")
public class Gameglobalextinfo extends Model<Gameglobalextinfo> {
private static final long serialVersionUID = 1L;

    /**
   * 游戏id
   */
    @TableId
    private Integer tGameatomtypeid;
    /**
   * 扩展信息
   */
    private byte[] tExtinfo;
    /**
   * 更新时间
   */
    private Integer tUpdatetime;
  
}
