package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author code generator
 * @date 2018-12-29 13:18:03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_bulletin")
public class Bulletin extends Model<Bulletin> {
    private static final long serialVersionUID = 1L;

    /**
     * 公告ID
     */
    @TableId
    private String tId;
    /**
     * 公告类型 1-公告栏 2-跑马灯
     */
    private Long tType;
    /**
     * 公告标题
     */
    private String tTitle;
    /**
     * 公告内容, 其中包括公告作用时间, 内容
     */
    private byte[] tContent;
    /**
     * 公告超链接,没有则为空,指向图片等url地址
     */
    private String tHyperlink;
    /**
     * 公告开始时间
     */
    private Integer tStart;
    /**
     * 公告结束时间
     */
    private Integer tFinish;
    /**
     * 影响渠道,json格式,记录所有渠道, 如果之所有渠道这里为空
     */
    private byte[] tChannellist;
    /**
     * 创建时间
     */
    private Integer tCreatetime;

}
