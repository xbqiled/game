package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;


@Data
public class WxhhExpDto implements Serializable {

    private Integer gameId;

    private String startTime;

    private String endTime;
}
