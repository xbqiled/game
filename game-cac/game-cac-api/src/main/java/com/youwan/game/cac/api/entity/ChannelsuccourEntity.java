package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * sharded table
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-31 12:23:45
 */
@TableName("t_channelsuccour")
public class ChannelsuccourEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String tChannel;
	/**
	 * 
	 */
	private Integer tCfgtime;
	/**
	 * 
	 */
	private Integer tOpentype;
	/**
	 * 
	 */
	private byte[] tSuccourinfo;

	/**
	 * 设置：
	 */
	public void setTChannel(String tChannel) {
		this.tChannel = tChannel;
	}
	/**
	 * 获取：
	 */
	public String getTChannel() {
		return tChannel;
	}
	/**
	 * 设置：
	 */
	public void setTCfgtime(Integer tCfgtime) {
		this.tCfgtime = tCfgtime;
	}
	/**
	 * 获取：
	 */
	public Integer getTCfgtime() {
		return tCfgtime;
	}
	/**
	 * 设置：
	 */
	public void setTOpentype(Integer tOpentype) {
		this.tOpentype = tOpentype;
	}
	/**
	 * 获取：
	 */
	public Integer getTOpentype() {
		return tOpentype;
	}
	/**
	 * 设置：
	 */
	public void setTSuccourinfo(byte[]  tSuccourinfo) {
		this.tSuccourinfo = tSuccourinfo;
	}
	/**
	 * 获取：
	 */
	public byte[]  getTSuccourinfo() {
		return tSuccourinfo;
	}
}
