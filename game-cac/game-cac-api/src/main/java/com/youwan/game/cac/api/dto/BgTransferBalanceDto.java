package com.youwan.game.cac.api.dto;


import lombok.Data;

import java.io.Serializable;

@Data
public class BgTransferBalanceDto implements Serializable {
    private String amount;
    private String userId;
    private Integer type;
    private String channelId;
    private Integer reason;


    public BgTransferBalanceDto(String userId, String amount, Integer type, String channelId, Integer reason) {
        this.userId = userId;
        this.amount = amount;
        this.type = type;
        this.channelId  = channelId;
        this.reason = reason;
    }
}
