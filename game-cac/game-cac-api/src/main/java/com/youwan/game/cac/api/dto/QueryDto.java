package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;


@Data
public class QueryDto implements Serializable {

    private String channelId;
    private String channelNo;
    private String userId;

    private String userName;
    private String operation;
    private String startTime;
    private String endTime;
}
