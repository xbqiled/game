package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-27 16:31:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_pay_config")
public class SysPayConfig extends Model<SysPayConfig> {
private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private Long id;
    /**
     * 支付名称
     */
    private String payName;
    /**
     * 1	微信公众账号支付
     2	微信扫码支付
     3	微信网页支付(微信H5)
     4	微信app支付
     5 微信转账支付
     6	支付宝扫码支付
     7 支付宝APP支付
     8 支付宝网页支付(手机H5)
     9 支付宝转账支付
     10	QQ钱包Wap支付
     11	QQ钱包手机扫码支付
     12  QQ转账支付
     13	京东扫码支付
     14	苏宁扫码支付
     15	银联二维码支付
     16	网银B2B支付
     17	网银B2C支付
     18	无卡快捷支付(不需要银行卡号的WAP快捷)
     19	WAP快捷(银联H5,需要正确银行卡号)
     20  银行卡转账支付
     */
    private Integer payType;
    /**
     * 平台通道类型:根据第三方提供的来
     */
    private String platformType;
    /**
     * 支付大类型:1，在线支付， 2，银行打款,  3,支付宝打款,   4,微信打款
     */
    private Integer type;
    /**
     * 收款人/商户号
     */
    private String merchantCode;
    /**
     * 收款银行/秘钥
     */
    private String merchantKey;
    /**
     * 开户网点/公钥
     */
    private String publicKey;
    /**
     *
     */
    private String appKey;
    /**
     * 收款账号/账号
     */
    private String account;
    /**
     *  最小支付金额
     */
    private Long min;
    /**
     *  最大支付金额
     */
    private Long max;
    /**
     * 是否默认
     */
    private Integer def;
    /**
     * 状态: 1, 有效， 0无效
     */
    private Integer status;
    /**
     * 渠道编码
     */
    private Long channelId;
    /**
     * 图片地址
     */
    private String icon;
    /**
     * 通道ID
     */
    private Long payPlatformId;
    /**
     * 支付网关
     */
    private String payGetway;
    /**
     * 排序
     */
    private Integer sortNo;
    /**
     * 支付说明
     */
    private String payDesc;

    /**
     * 返利值
     */
    private Integer returnRate;

    /**
     * 按钮配置
     */
    private String payValueConfig;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 创建人
     */
    private String createBy;


    private String remark;
  
}
