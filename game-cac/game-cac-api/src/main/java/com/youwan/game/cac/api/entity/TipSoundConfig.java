package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-05-21 11:11:20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("tip_sound_config")
public class TipSoundConfig extends Model<TipSoundConfig> {
private static final long serialVersionUID = 1L;

    /**
   * 主键Id
   */
    @TableId
    private Integer id;
    /**
   * 类型: 1,百度
   */
    private Integer type;
    /**
   * client_id
   */
    private String clientId;
    /**
   * client_secret
   */
    private String clientSecret;
    /**
   * grant_type
   */
    private String grantType;
    /**
   * 渠道编码
   */
    private String channelId;
    /**
   * 创建时间
   */
    private LocalDateTime createTime;
    /**
   * 创建人
   */
    private String createBy;
  
}
