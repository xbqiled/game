package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-22 15:14:43
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("V_luckyRoulette")
public class LuckyRoulette extends Model<LuckyRoulette> {
private static final long serialVersionUID = 1L;

    /**
   * 渠道ID
   */
    @TableId
    private String tChannel;
    /**
   * 白银转盘数据
   */
    private byte[] tSilvercfg;
    /**
   * 白银转盘单次抽奖消耗金币
   */
    private Long tSilvercost;
    /**
   * 黄金转盘数据
   */
    private byte[] tGoldcfg;
    /**
   * 黄金转盘单次抽奖消耗金币
   */
    private Long tGoldcost;
    /**
   * 钻石转盘数据
   */
    private byte[] tDiamondcfg;
    /**
   * 钻石转盘单次抽奖消耗金币
   */
    private Long tDiamondcost;
    /**
   * 活动开始时间
   */
    private Long tStart;
    /**
   * 活动结束时间
   */
    private Long tFinish;
  
}
