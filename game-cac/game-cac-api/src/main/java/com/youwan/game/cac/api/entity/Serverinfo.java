package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-29 12:32:01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_serverinfo")
public class Serverinfo extends Model<Serverinfo> {
private static final long serialVersionUID = 1L;

    /**
   * 主键1   此表是全局变量表  只会有一行记录  
   */
    @TableId
    private Integer tId;
    /**
   * 服务状态  1-维护 2-正常
   */
    private Integer tSrvstate;
    /**
   * 维护开始时间
   */
    private Integer tSrvmaintstart;
    /**
   * 维护结束时间
   */
    private Integer tSrvmaintend;
    /**
   * 账号名idx
   */
    private Integer tAccnameidx;
    /**
   * 昵称idx
   */
    private Integer tNickidx;
  
}
