package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BatchSynBgBetDto implements Serializable {
    private String startTime;
    private String endTime;

    private Long pageIndex;
    private Long pageSize;
}
