package com.youwan.game.cac.api.vo;

import lombok.Data;

import java.io.Serializable;


@Data
public class AddPointVo implements Serializable {

     private String channelId;

     private String userId;

     private String amount;

     private Integer status;

}
