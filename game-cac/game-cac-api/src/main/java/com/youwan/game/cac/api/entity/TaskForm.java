package com.youwan.game.cac.api.entity;

import com.youwan.game.common.core.jsonmodel.GsonTask;
import lombok.Data;

import java.io.Serializable;

@Data
public class TaskForm implements Serializable {
    private String channelId;

    private Integer openType;

    private Integer activityTimeType;

    private Integer activityOpenType;

    private String beginTime;

    private String endTime;

    private GsonTask[] taskArr;
}
