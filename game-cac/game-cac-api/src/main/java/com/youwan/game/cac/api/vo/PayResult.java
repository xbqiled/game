package com.youwan.game.cac.api.vo;


import lombok.Data;

import java.io.Serializable;


@Data
public class PayResult implements Serializable {

    private String code;
    private String orderNo;
    private String payUrl;

    private String payName;
    private String status;
    private String payFee;

    //0不处理，1保存code
    private String type;
}
