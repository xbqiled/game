package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 *
 * @author code generator
 * @date 2019-02-08 18:43:43
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_channelSwitchSetting")
public class TChannelswitchsetting extends Model<TChannelswitchsetting> {
private static final long serialVersionUID = 1L;

    /**
   * 渠道号
   */
    private String tChannelkey;
    /**
   * 正式版本配置
   */
    private String tNormalsetting;
    /**
   * 提审配置
   */
    private String tAuditsetting;
  
}
