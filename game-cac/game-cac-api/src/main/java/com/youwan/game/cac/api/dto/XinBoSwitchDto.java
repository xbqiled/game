package com.youwan.game.cac.api.dto;

import com.youwan.game.common.core.jsonmodel.GsonSwitch;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


@Data
public class XinBoSwitchDto implements Serializable {

     private List<GsonSwitch> cfgList;

     private Integer type;

     private String channelId;

}
