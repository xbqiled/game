package com.youwan.game.cac.api.feign;

import com.youwan.game.cac.api.dto.BettingRecordQueryDto;
import com.youwan.game.cac.api.feign.factory.RemoteBettingRebateServiceFallbackFactory;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.constant.ServiceNameConstant;
import com.youwan.game.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-10.
 */
@FeignClient(value = ServiceNameConstant.CAC_SERVICE, fallbackFactory = RemoteBettingRebateServiceFallbackFactory.class)
public interface RemoteBettingRebateService {

    @GetMapping("/bettingRebate/scrub/{userId}/details")
    R getScrubRebateDetails(@PathVariable("userId") Long userId, @RequestHeader(SecurityConstants.FROM) String from);

    @GetMapping("/bettingRebate/scrub/{userId}/history")
    R getScrubRebateHistory(@PathVariable("userId") Long userId,
                             @RequestParam(value = "offset", required = false, defaultValue = "0") Integer offset,
                             @RequestParam(value = "limit", required = false, defaultValue = "10") Integer limit,
                             @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/bettingRebate/scrub/{userId}/_take")
    R takeScrubRebate(@PathVariable("userId") Long userId, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/bettingRebate/scrub/{userId}/takingRecords")
    R getPersonTakingRecords(@PathVariable("userId") Long userId, @RequestBody BettingRecordQueryDto dto,
                             @RequestHeader(SecurityConstants.FROM) String from);
}
