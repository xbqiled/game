package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 
 *
 * @author code generator
 * @date 2019-06-16 12:32:18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("tmp_statis_account")
public class StatisAccount extends Model<StatisAccount> {
private static final long serialVersionUID = 1L;

    @TableId
    private String userId;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 用户昵称
     */
    private String nickName;
    /**
     * 渠道编码
     */
    private String channelId;
    /**
     * 统计时间
     */
    private Date statisTime;
    /**
     * 充值金额
     */
    private BigDecimal rechargeFee;
    /**
     * 充值次数
     */
    private Long rechargeCount;
    /**
     * 提现金额
     */
    private BigDecimal cashFee;
    /**
     * 提现次数
     */
    private Long cashCount;
    /**
     * 下注金额
     */
    private BigDecimal betFee;
    /**
     * 下注次数
     */
    private Long betCount;
    /**
     * 赢取金额
     */
    private BigDecimal winFee;
    /**
     * 游戏时长
     */
    private Long gameTimeLangth;
    /**
     * 总进账
     */
    private BigDecimal allFee;
    /**
     * 赠送金额
     */
    private BigDecimal giveFee;
    /**
     * 当日盈亏
     */
    private BigDecimal lossFee;
    /**
     * 是否比赛
     */
    private Integer match;
    /**
     * 步骤
     */
    private Integer step;


}
