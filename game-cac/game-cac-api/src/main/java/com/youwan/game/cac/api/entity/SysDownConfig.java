package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-26 14:00:25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_down_config")
public class SysDownConfig extends Model<SysDownConfig> {
private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId
    private Integer id;
    /**
     * 渠道ID
     */
    private Integer channelId;

    /**
     * android下载地址
     */
    private String androidUrl;
    /**
     * ios下载地址
     */
    private String iosUrl;
    /**
     * android版本号
     */
    private String androidVersion;
    /**
     * ios版本号
     */
    private String iosVersion;
    /**
     * android的APPKEY
     */
    private String androidAppKey;
    /**
     * ios的APPKEY
     */
    private String iosAppKey;
    /**
     * 安卓的pkgID
     */
    private String androidPkgid;
    /**
     * 苹果的pkgID
     */
    private String iosPkgid;
    /**
     * scheme名称
     */
    private String scheme;
    /**
     * host地址
     */
    private String host;
    /**
     * 后缀
     */
    private String pathprefix;
    /**
     * 是否激活
     */
    private String flagActive;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 修改时间
     */
    private LocalDateTime modifyTime;
    /**
     * 修改人
     */
    private String modifyBy;
  
}
