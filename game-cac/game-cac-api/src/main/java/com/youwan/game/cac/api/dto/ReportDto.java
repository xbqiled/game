package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;


@Data
public class ReportDto implements Serializable {
    String channelNo;

    String startTime;

    String endTime;

    String monthTime;

    String userId;

    String gameId;
}
