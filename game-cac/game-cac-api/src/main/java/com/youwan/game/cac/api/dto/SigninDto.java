package com.youwan.game.cac.api.dto;

import com.youwan.game.common.core.jsonmodel.GsonSignAwardCfg;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SigninDto implements Serializable {

    private String channelId;

    private List<GsonSignAwardCfg> signCfg;

    private String startTime;

    private String endTime;

    private Integer isCirculate;

    private Integer num;



}


