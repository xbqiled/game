package com.youwan.game.cac.api.vo;


import lombok.Data;

@Data
public class GameglobalextinfoVo {

    private Integer gameatomtypeid;

    private byte[] extinfo;

    private Long updatetime;

    private String gameKindName;

    private String phoneGameName;

}
