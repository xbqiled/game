package com.youwan.game.cac.api.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class BgRoundVo implements Serializable {

    public Integer payoutAmount;
    public String betResult;

    public Integer gameType;
    public String baccarat;
    public String baccarat64;

    public Integer dealerId;
    public String serialNo;
    public String result;

    public Integer betAmount;
    public String gameName;
    public String calcTime;

    public String tableId;
    public String dealer;
    public Integer bettings;

    public String openTime;
    public Integer status;
}
