package com.youwan.game.cac.api.dto;

import com.youwan.game.common.core.jsonmodel.GsonRechargeRebateCfg;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


@Data
public class DepositDto implements Serializable {

    private String channelId;

    private List<GsonRechargeRebateCfg> rabateCfg;

    private String startTime;

    private String endTime;

}
