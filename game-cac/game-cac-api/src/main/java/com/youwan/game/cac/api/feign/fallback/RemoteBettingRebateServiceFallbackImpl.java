package com.youwan.game.cac.api.feign.fallback;

import com.youwan.game.cac.api.dto.BettingRecordQueryDto;
import com.youwan.game.cac.api.feign.RemoteBettingRebateService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.data.core.Page;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-10.
 */
@Slf4j
@Component
public class RemoteBettingRebateServiceFallbackImpl implements RemoteBettingRebateService {

    @Setter
    private Throwable cause;

    @Override
    public R getScrubRebateDetails(Long userId, String from) {
        log.error("feign get current betting rebate fail", cause);
        return new R(Collections.EMPTY_LIST);
    }

    @Override
    public R getScrubRebateHistory(Long userId, Integer offset, Integer limit, String from) {
        log.error("feign get current betting rebate history fail", cause);
        return new R(Page.empty());
    }

    @Override
    public R takeScrubRebate(Long userId, String from) {
        log.error("feign take current betting rebate fail", cause);
        return new R();
    }

    @Override
    public R getPersonTakingRecords(Long userId, BettingRecordQueryDto dto, String from) {
        log.error("feign get person betting rebate taking records fail", cause);
        return new R(Page.empty());
    }
}
