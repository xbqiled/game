package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-20 13:02:18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("other_account_record")
public class AccountRecord extends Model<AccountRecord> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId
    private Integer id;
    /**
   * 业务标识
   */
    private Long bizId;
    /**
   * 渠道编码
   */
    private String channelId;
    /**
   * 会计项目
   */
    private Integer accountItem;
    /**
   * 状态信息 1成功
   */
    private Integer status;
    /**
   * 类型编码 1真人
   */
    private Integer gameType;
    /**
   * 变化前金额
   */
    private BigDecimal beforeAmount;
    /**
     * 金额
     */
    private BigDecimal changeAmount;
    /**
   * 变化后金额
   */
    private BigDecimal afterAmount;
    /**
   * 创建时间
   */
    private LocalDateTime createTime;
    /**
   * 创建人
   */
    private String createBy;
  
}
