package com.youwan.game.cac.api.vo;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


@Data
public class SmsVo implements Serializable {
    @Getter
    @Setter
    private String type;

    @Getter
    @Setter
    private String mode;

    @Getter
    @Setter
    private String account;

    @Getter
    @Setter
    private String clientPw;

    @Getter
    @Setter
    private String intfacePw;

    @Getter
    @Setter
    private String code;

    @Getter
    @Setter
    private String serverUrl;

    /**
     * 应用ID
     */
    @Getter
    @Setter
    private String aliAccessKey;

    /**
     * 应用秘钥
     */
    @Getter
    @Setter
    private String aliSecretKey;

    /**
     * 签名
     */
    @Getter
    @Setter
    private String aliSignName;


    @Getter
    @Setter
    private String rongLianServerIp;

    @Getter
    @Setter
    private String rongLianServerPort;

    @Getter
    @Setter
    private String rongLianAccount;

    @Getter
    @Setter
    private String rongLianToken;

    @Getter
    @Setter
    private String rongLianAppId;
}
