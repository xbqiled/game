package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-18 16:08:14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cfg_channel_download")
public class CfgChlDownload extends Model<CfgChlDownload> {
private static final long serialVersionUID = 1L;

   /**
   * 
   */
    @TableId
    private Integer id;
    /**
   * 渠道编码
   */
    private String channelId;
    /**
   * 头部URL
   */
    private String headUrl;
    /**
   * 中部URL
   */
    private String bodyUrl;
    /**
   * 脚部URL
   */
    private String footUrl;
  /**
   * 背景色
   */
   private String backgroundUrl;
  /**
   * 修改时间
   */
    private LocalDateTime modifyTime;
    /**
   * 修改人
   */
    private String modifyBy;
    /**
   * 创建时间
   */
    private LocalDateTime createTime;
    /**
   * 创建人
   */
    private String createBy;
  
}
