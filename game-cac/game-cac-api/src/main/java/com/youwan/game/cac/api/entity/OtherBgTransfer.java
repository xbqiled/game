package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-05 15:57:02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("other_bg_transfer")
public class OtherBgTransfer extends Model<OtherBgTransfer> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId
    private Integer id;

    /**
   * 业务单据id
   */
    private Long bizId;
    /**
   * 厅号码
   */
    private String sn;
    /**
   * 用户ID
   */
    private String userId;
    /**
     * 渠道ID
     */
    private String channelId;
    /**
   * 会计项目
   */
    private Integer accountItem;
    /**
   * 状态信息
   */
    private Integer status;
    /**
   * 金额
   */
    private BigDecimal amount;
    /**
   * 余额
   */
    private BigDecimal bgBalance;
    /**
     * 系统余额
     */
    private BigDecimal sysBalance;
    /**
   * 操作时间
   */
    private Date operateTime;
  
}
