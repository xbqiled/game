package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class OtherPlatformDto implements Serializable {

    private String platformName;

    private String platformCode;

}
