package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2020-05-23 15:14:24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_push_platform")
public class SysPushPlatform extends Model<SysPushPlatform> {
private static final long serialVersionUID = 1L;

    /**
   * ID
   */
    @TableId
    private Integer id;
    /**
   * 名称
   */
    private String name;
    /**
   * 类型
   */
    private Integer type;
    /**
   * 类名
   */
    private String className;
    /**
   * 有效性标识,只有一个有效
   */
    private Integer status;
    /**
   * 描述
   */
    private String description;
    /**
   * 创建时间
   */
    private LocalDateTime createTime;
    /**
   * 创建人
   */
    private String createBy;
  
}
