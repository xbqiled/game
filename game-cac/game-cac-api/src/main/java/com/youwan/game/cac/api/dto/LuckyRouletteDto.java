package com.youwan.game.cac.api.dto;

import com.youwan.game.common.core.jsonmodel.GsonLuckRoulette;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


@Data
public class LuckyRouletteDto implements Serializable {
    private String channelId;

    private List<GsonLuckRoulette> diamond;
    private List<GsonLuckRoulette> gold;
    private List<GsonLuckRoulette> silver;

    private String start;
    private String finish;

    private Integer goldCost;
    private Integer diamondCost;
    private Integer silverCost;
}
