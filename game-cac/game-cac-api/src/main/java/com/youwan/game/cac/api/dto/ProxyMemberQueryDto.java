package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-04-25.
 */
@Data
public class ProxyMemberQueryDto implements Serializable {
    private String channelId;
    private String channelNo;
    private Long userId;
    private Long start;
    private Long end;
    private int deep;
    private int offset;
    private int limit = 10;
    private String game;
    private Long memberId;
    private Integer businessType;
}
