package com.youwan.game.cac.api.vo;


import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class BindVo implements Serializable {

    /**
     * 渠道编码
     */
    private String channelId;

    /**
     * 渠道名称
     */
    private String channelName;
    /**
     * 赠送值
     */
    private BigDecimal giveValue;
    /**
     * 第一级返利返利比例(万分比)

     */
    private Integer rebate;
    /**
     * 是否开放兑换 1-开放 0-关闭
     */
    private Integer exchange;
    /**
     * 版本号
     */
    private String versionNo;
    /**
     * 最低兑换额度, 单位分, 即100元

     */
    private Integer minExchange;
    /**
     * 初始化信息
     */
    private Integer initCoin;

    /**
     * 打码量
     */
    private  Integer damaMulti;


    private Integer rebateDamaMulti;


    private Integer promoteType;


    private Integer exchangeCount;


    private Integer resetDamaLeftCoin;


    private Integer cpStationId;

    private Integer cpLimit;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改人
     */
    private String createBy;
    /**
     * 修改时间
     */
    private Date modifyTime;
    /**
     * 修改人
     */
    private String modifyBy;

}
