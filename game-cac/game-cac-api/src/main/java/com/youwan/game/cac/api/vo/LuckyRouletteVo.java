package com.youwan.game.cac.api.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class LuckyRouletteVo implements Serializable {

    private String tChannel;

    private byte[] tSilvercfg;

    private Long tSilvercost;

    private byte[] tGoldcfg;

    private Long tGoldcost;

    private byte[] tDiamondcfg;

    private Long tDiamondcost;

    private Integer tStart;

    private Integer tFinish;

    private String startTimeStr;

    private String finishTimeStr;
}
