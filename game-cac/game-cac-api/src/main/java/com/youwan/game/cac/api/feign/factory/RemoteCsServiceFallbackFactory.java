package com.youwan.game.cac.api.feign.factory;


import com.youwan.game.cac.api.feign.RemoteCsService;
import com.youwan.game.cac.api.feign.fallback.RemoteCsServiceFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author lion
 * @date 2019/2/14
 */
@Component
public class RemoteCsServiceFallbackFactory implements FallbackFactory<RemoteCsService> {

    @Override
    public RemoteCsService create(Throwable throwable) {
        RemoteCsServiceFallbackImpl remoteCsServiceFallbackImpl = new RemoteCsServiceFallbackImpl();
        remoteCsServiceFallbackImpl.setCause(throwable);
        return remoteCsServiceFallbackImpl;
    }
}
