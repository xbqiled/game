package com.youwan.game.cac.api.entity;

import com.youwan.game.common.data.core.BaseEntity;
import lombok.Data;

import javax.persistence.Id;
import java.util.List;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-04-25.
 */
@Data
public class ProxyMemberTreasureChange extends BaseEntity {
    @Id
    private String id;
    private Integer beforeCount;
    private Integer gameTypeID;
    private String channelNO;
    private Integer itemValidValue;
    private Long pid;
    private Long userID;
    private String nickName;
    private Integer terminalType;
    private Integer itemID;
    private Integer afterCount;
    private Integer gainCount;
    private Integer afterValidValue;
    private Long recordTime;
    private Integer beforeValidValue;
    private String keyGuid;
    private Integer userType;
    private Integer businessType;
    //private String businessGuid;
    private String matchGuid;
    private List<String> m_path;
    private int deep;
}
