package com.youwan.game.cac.api.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * VIEW
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-05-31 11:20:19
 */
@Data
public class DepositchannelVo implements Serializable {
	private static final long serialVersionUID = 1L;

	private String channelId;

	private byte[] cfginfo;

	private Long startTime;

	private Long endTime;

	private String startTimeStr;

	private String endTimeStr;

	private Boolean openType;

	private Integer depositType;

	private String channelName;

	private String iconUrl;

}
