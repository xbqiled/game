package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * VIEW
 *
 * @author code generator
 * @date 2020-03-27 20:25:55
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_gamectrlcfg")
public class Gamectrlcfg extends Model<Gamectrlcfg> {
private static final long serialVersionUID = 1L;

    /**
   * 游戏id
   */
    private Integer tGameatomtypeid;
    /**
   * 配置时间
   */
    private Integer tCfgtime;
    /**
   * 游戏控制配置信息
   */
    private byte[] tCfginfo;
  
}
