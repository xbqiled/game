package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * shared table
 *
 * @email ismyjuliet@gmail.com
 * @date 2019-12-15 16:02:59
 */
@TableName("t_channeldayshare")
public class DayShareEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    private String tChannel;
    /**
     *
     */
    private Integer tCfgtime;
    /**
     *
     */
    private Integer tOpentype;
    /**
     *
     */
    private byte[] tDayshareinfo;

    /**
     * 设置：
     */
    public void setTChannel(String tChannel) {
        this.tChannel = tChannel;
    }

    /**
     * 获取：
     */
    public String getTChannel() {
        return tChannel;
    }

    /**
     * 设置：
     */
    public void setTCfgtime(Integer tCfgtime) {
        this.tCfgtime = tCfgtime;
    }

    /**
     * 获取：
     */
    public Integer getTCfgtime() {
        return tCfgtime;
    }

    /**
     * 设置：
     */
    public void setTOpentype(Integer tOpentype) {
        this.tOpentype = tOpentype;
    }

    /**
     * 获取：
     */
    public Integer getTOpentype() {
        return tOpentype;
    }

    /**
     * 设置：
     */
    public void setTDayshareinfo(byte[] tDayshareinfo) {
        this.tDayshareinfo = tDayshareinfo;
    }

    /**
     * 获取：
     */
    public byte[] getTDayshareinfo() {
        return tDayshareinfo;
    }
}
