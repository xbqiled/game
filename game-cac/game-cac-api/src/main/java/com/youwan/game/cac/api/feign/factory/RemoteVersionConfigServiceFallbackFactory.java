package com.youwan.game.cac.api.feign.factory;


import com.youwan.game.cac.api.feign.RemoteVersionConfigService;
import com.youwan.game.cac.api.feign.fallback.RemoteVersionConfigServiceFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author lion
 * @date 2019/2/14
 */
@Component
public class RemoteVersionConfigServiceFallbackFactory implements FallbackFactory<RemoteVersionConfigService> {



    @Override
    public RemoteVersionConfigService create(Throwable throwable) {
        RemoteVersionConfigServiceFallbackImpl remoteVersionConfigServiceFallbackImpl = new RemoteVersionConfigServiceFallbackImpl();
        remoteVersionConfigServiceFallbackImpl.setCause(throwable);
        return remoteVersionConfigServiceFallbackImpl;
    }
}
