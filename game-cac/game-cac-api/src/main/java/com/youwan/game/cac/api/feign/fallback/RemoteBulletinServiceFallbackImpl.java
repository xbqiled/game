package com.youwan.game.cac.api.feign.fallback;


import com.youwan.game.cac.api.feign.RemoteBulletinService;
import com.youwan.game.common.core.util.R;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author lion
 * @date 2019/02/14
 * feign token  fallback
 */
@Slf4j
@Component
public class RemoteBulletinServiceFallbackImpl implements RemoteBulletinService {

    @Setter
    private Throwable cause;

    /**
     *
     * @param bullitinId
     * @param from
     * @return
     */
    @Override
    public R getBulletinInfo(String bullitinId, String from) {
        log.error("feign 获取公告信息失败", cause);
        return null;
    }
}
