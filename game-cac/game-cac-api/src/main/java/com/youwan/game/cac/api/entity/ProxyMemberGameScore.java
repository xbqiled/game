package com.youwan.game.cac.api.entity;

import com.youwan.game.common.data.core.BaseEntity;
import lombok.Data;

import javax.persistence.Id;
import java.util.List;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-04-30.
 */
@Data
public class ProxyMemberGameScore extends BaseEntity {

    @Id
    private String id;
    private Long userID;
    private String nickName;
    private Integer score;
    private String game;
    private String room;
    private Integer gameTypeID;
    private Long startTime;
    private Long endTime;
    private String recordID;
    private String channelNO;
    private Integer terminalType;
    private Integer beforeScore;
    private Integer afterScore;
    private Double stake;
    private List<String> m_path;
    private int deep;
    private Long pid;
    private Integer settlementType;
}
