package com.youwan.game.cac.api.vo;

import lombok.Data;

import java.io.Serializable;


@Data
public class GameRoomVo implements Serializable {

    private  Integer gameAtomTypeId;

    private  Integer gameKindType;

    private  String gameKindName;

    private  String phoneGameName;

}
