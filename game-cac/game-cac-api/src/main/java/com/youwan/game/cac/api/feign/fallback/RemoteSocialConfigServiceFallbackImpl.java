package com.youwan.game.cac.api.feign.fallback;


import com.youwan.game.cac.api.feign.RemoteSocialConfigService;
import com.youwan.game.common.core.util.R;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author lion
 * @date 2019/02/14
 * feign token  fallback
 */
@Slf4j
@Component
public class RemoteSocialConfigServiceFallbackImpl implements RemoteSocialConfigService {

    @Setter
    private Throwable cause;

    @Override
    public R getSocialConfigById(String channelId, String from) {
        log.error("feign 获取微信分享配置信息失败", cause);
        return null;
    }
}
