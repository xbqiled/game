package com.youwan.game.cac.api.dto;


import lombok.Data;

import java.io.Serializable;

@Data
public class BgDamaGameDto implements Serializable {

    private String userId;

    private Long startTime;

    private Long endTime;

    private Integer damaValue;

    public BgDamaGameDto(String userId, Long startTime, Long endTime, Integer damaValue) {
        this.userId = userId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.damaValue = damaValue;
    }
}
