package com.youwan.game.cac.api.feign.fallback;


import com.youwan.game.cac.api.dto.CsInfoDto;
import com.youwan.game.cac.api.entity.CsInfo;
import com.youwan.game.cac.api.feign.RemoteCsService;
import com.youwan.game.common.core.util.R;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lion
 * @date 2019/02/14
 * feign token  fallback
 */
@Slf4j
@Component
public class RemoteCsServiceFallbackImpl implements RemoteCsService {

    @Setter
    private Throwable cause;

    @Override
    public R<List<CsInfo>> queryCsInfo(CsInfoDto csInfoDto, String from) {
        log.error("feign 获取客服信息失败", cause);
        return null;
    }

    @Override
    public R<List<CsInfo>> queryCsInfoById(String channelId, String from) {
        log.error("feign 获取客服信息失败", cause);
        return null;
    }
}
