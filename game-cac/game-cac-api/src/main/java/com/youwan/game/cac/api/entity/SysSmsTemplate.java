package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code lion
 * @date 2019-02-17 18:01:23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_sms_template")
public class SysSmsTemplate extends Model<SysSmsTemplate> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private Integer id;
    /**
   * 
   */
    private Integer configId;
    /**
   * 
   */
    private String templateCode;
    /**
   * 
   */
    private String templateName;
    /**
   * 
   */
    private Integer templateType;
    /**
   * 
   */
    private Integer state;
    /**
     *
     */
    private String channelId;

    /**
     *
     */
    private String serverCode;
    /**
   * 
   */
    private LocalDateTime createTime;
    /**
   * 
   */
    private String createBy;
  
}
