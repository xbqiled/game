package com.youwan.game.cac.api.vo;


import lombok.Data;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class  DownloadConfigVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 渠道ID
     */
    private Integer channelId;

    /**
     * 渠道名称
     */
    private String channelName;

    /**
     * iconUrl地址
     */
    private String iconUrl;

    private Integer id;
    /**
     * android下载地址
     */
    private String androidUrl;
    /**
     * ios下载地址
     */
    private String iosUrl;
    /**
     * android版本号
     */
    private String androidVersion;
    /**
     * ios版本号
     */
    private String iosVersion;
    /**
     * android的APPKEY
     */
    private String androidAppKey;
    /**
     * ios的APPKEY
     */
    private String iosAppKey;
    /**
     * 安卓的pkgID
     */
    private String androidPkgid;
    /**
     * 苹果的pkgID
     */
    private String iosPkgid;
    /**
     * scheme名称
     */
    private String scheme;
    /**
     * host地址
     */
    private String host;
    /**
     * 后缀
     */
    private String pathprefix;
    /**
     * 是否激活
     */
    private String flagActive;

    private String headUrl;

    private String bodyUrl;

    private String footUrl;

    private String backgroundUrl;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 修改时间
     */
    private LocalDateTime modifyTime;
    /**
     * 修改人
     */
    private String modifyBy;

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
