package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-05-15 15:24:30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pop_device_info")
public class DeviceInfo extends Model<DeviceInfo> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId
    private Integer id;
    /**
   * 渠道编码
   */
    private String channelId;
    /**
   * IP地址
   */
    private String ip;

    /**
     * 系统
     */
    private String os;
    /**
   * 推广人信息
   */
    private String promoterId;
    /**
   * 参数信息
   */
    private String paraInfo;
    /**
   * 比较次数
   */
    private Integer compareCount;
    /**
   * 设备信息
   */
    private String deviceInfo;
    /**
   * 状态:  正常1， 拉黑0
   */
    private Integer status;
    /**
   * 创建时间
   */
    private LocalDateTime createTime;
  
}
