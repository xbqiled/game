package com.youwan.game.cac.api.dto;


import lombok.Data;

import java.io.Serializable;


@Data
public class NotifyPayOrderDto implements Serializable {
    private String productDesc;
    private String orderAmount;
    private String orderNo;
    private String wtfOrderNo;
    private String payTime;
    private String sign;
    private String orderStatus;
    private String remark;
    private String productName;
    private String merchantNo;
    private Double damaMulti;
    private Integer rate;
    private Integer isManual;
}
