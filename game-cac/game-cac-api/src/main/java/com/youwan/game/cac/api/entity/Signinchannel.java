package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-06-05 13:49:00
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_signInChannel")
public class Signinchannel extends Model<Signinchannel> {
    private static final long serialVersionUID = 1L;

    @TableId
    private String tChannel;
    /**
     *
     */
    private byte[] tCfginfo;
    /**
     *
     */
    private Long tStarttime;
    /**
     *
     */
    private Long tEndtime;
    /**
     *
     */
    private Integer tIsCirculate;

    /**
     *
     */
    private Integer tOpenType;

  
}
