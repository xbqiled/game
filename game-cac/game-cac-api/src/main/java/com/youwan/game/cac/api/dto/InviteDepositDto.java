package com.youwan.game.cac.api.dto;


import lombok.Data;

import java.io.Serializable;

@Data
public class InviteDepositDto implements Serializable {

    private String channelId;
    private Long openType;

    private Long timeType;
    private Long beginTime;
    private Long endTime;

    private Long firstTotalDeposit;
    private Long selfReward;
    private Long upperReward;


}
