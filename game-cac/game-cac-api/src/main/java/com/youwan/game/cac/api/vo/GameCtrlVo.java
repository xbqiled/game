package com.youwan.game.cac.api.vo;


import lombok.Data;

import java.io.Serializable;

@Data
public class GameCtrlVo implements Serializable {

    private Integer tGameid;

    private byte[] tExtend;

    private String gameAtomTypeId;

    private String gameKindType;

    private String gameKindName;

    private String phoneGameName;

    private Integer dangerDown;

    private Integer safeDown;

    private  Integer safeUp;

    private Integer dangerUp;
}
