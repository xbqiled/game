package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;


@Data
public class DeviceInfoDto implements Serializable {

    private String channelId;
    /**
     * IP地址
     */
    private String ip;
    /**
     *系统信息
     */
    private String os;
    /**
     * 推广人信息
     */
    private String promoterId;
    /**
     * 参数信息
     */
    private String paraInfo;
    /**
     * <B>设备信息</B>
     */
    private String deviceInfo;

}
