package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-29 13:01:34
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_whiteList")
public class Whitelist extends Model<Whitelist> {
private static final long serialVersionUID = 1L;

    //@TableId
    private String tAccountname;
    /**
   * 0--已删除 1--正常状态
   */
    private Integer tState;
  
}
