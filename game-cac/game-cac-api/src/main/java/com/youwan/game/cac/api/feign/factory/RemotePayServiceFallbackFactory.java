package com.youwan.game.cac.api.feign.factory;


import com.youwan.game.cac.api.feign.RemotePayService;
import com.youwan.game.cac.api.feign.fallback.RemotePayServiceFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author lion
 * @date 2019/2/14
 */
@Component
public class RemotePayServiceFallbackFactory implements FallbackFactory<RemotePayService> {



    @Override
    public RemotePayService create(Throwable throwable) {
        RemotePayServiceFallbackImpl remotePayServiceFallbackImpl = new RemotePayServiceFallbackImpl();
        remotePayServiceFallbackImpl.setCause(throwable);
        return remotePayServiceFallbackImpl;
    }
}
