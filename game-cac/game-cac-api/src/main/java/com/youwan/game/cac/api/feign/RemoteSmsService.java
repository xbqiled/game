package com.youwan.game.cac.api.feign;


import com.youwan.game.cac.api.dto.SmsDto;
import com.youwan.game.cac.api.feign.factory.RemoteCashOrderServiceFallbackFactory;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.constant.ServiceNameConstant;
import com.youwan.game.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * <B> 短信服务feign</B>
 */
@FeignClient(value = ServiceNameConstant.CAC_SERVICE, fallbackFactory = RemoteCashOrderServiceFallbackFactory.class)
public interface RemoteSmsService {

    @PostMapping("/syssms/sendSms/")
    R sendSmsInfo(@RequestBody SmsDto sms, @RequestHeader(SecurityConstants.FROM) String from);


    @PostMapping("/syssms/getSmsTemplate/")
    R getSmsTemplate(@RequestBody SmsDto sms, @RequestHeader(SecurityConstants.FROM) String from);
}
