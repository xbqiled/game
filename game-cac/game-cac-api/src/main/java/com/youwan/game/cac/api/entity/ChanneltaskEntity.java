package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * sharded table
 * 
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-12-31 12:30:32
 */
@TableName("t_channeltask")
public class ChanneltaskEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String tChannel;
	/**
	 * 
	 */
	private Integer tActivityopentype;
	/**
	 * 
	 */
	private Integer tActivitytimetype;
	/**
	 * 
	 */
	private Long tBegintime;
	/**
	 * 
	 */
	private Long tEndtime;
	/**
	 * 
	 */
	private Integer tCfgtime;
	/**
	 * 
	 */
	private byte[] tTaskinfo;

	/**
	 * 设置：
	 */
	public void setTChannel(String tChannel) {
		this.tChannel = tChannel;
	}
	/**
	 * 获取：
	 */
	public String getTChannel() {
		return tChannel;
	}
	/**
	 * 设置：
	 */
	public void setTActivityopentype(Integer tActivityopentype) {
		this.tActivityopentype = tActivityopentype;
	}
	/**
	 * 获取：
	 */
	public Integer getTActivityopentype() {
		return tActivityopentype;
	}
	/**
	 * 设置：
	 */
	public void setTActivitytimetype(Integer tActivitytimetype) {
		this.tActivitytimetype = tActivitytimetype;
	}
	/**
	 * 获取：
	 */
	public Integer getTActivitytimetype() {
		return tActivitytimetype;
	}
	/**
	 * 设置：
	 */
	public void setTBegintime(Long tBegintime) {
		this.tBegintime = tBegintime;
	}
	/**
	 * 获取：
	 */
	public Long getTBegintime() {
		return tBegintime;
	}
	/**
	 * 设置：
	 */
	public void setTEndtime(Long tEndtime) {
		this.tEndtime = tEndtime;
	}
	/**
	 * 获取：
	 */
	public Long getTEndtime() {
		return tEndtime;
	}
	/**
	 * 设置：
	 */
	public void setTCfgtime(Integer tCfgtime) {
		this.tCfgtime = tCfgtime;
	}
	/**
	 * 获取：
	 */
	public Integer getTCfgtime() {
		return tCfgtime;
	}
	/**
	 * 设置：
	 */
	public void setTTaskinfo( byte[]  tTaskinfo) {
		this.tTaskinfo = tTaskinfo;
	}
	/**
	 * 获取：
	 */
	public  byte[]  getTTaskinfo() {
		return tTaskinfo;
	}
}
