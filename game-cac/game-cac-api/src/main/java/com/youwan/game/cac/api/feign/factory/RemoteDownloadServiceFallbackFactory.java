package com.youwan.game.cac.api.feign.factory;


import com.youwan.game.cac.api.feign.RemoteDownloadService;
import com.youwan.game.cac.api.feign.fallback.RemoteDownloadServiceFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author lion
 * @date 2019/2/14
 */
@Component
public class RemoteDownloadServiceFallbackFactory implements FallbackFactory<RemoteDownloadService> {


    @Override
    public RemoteDownloadService create(Throwable throwable) {
        RemoteDownloadServiceFallbackImpl remoteDownloadServiceFallbackImpl = new RemoteDownloadServiceFallbackImpl();
        remoteDownloadServiceFallbackImpl.setCause(throwable);
        return remoteDownloadServiceFallbackImpl;
    }
}
