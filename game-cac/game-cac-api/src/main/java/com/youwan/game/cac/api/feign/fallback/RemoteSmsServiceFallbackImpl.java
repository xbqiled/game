package com.youwan.game.cac.api.feign.fallback;


import com.youwan.game.cac.api.dto.SmsDto;
import com.youwan.game.cac.api.feign.RemoteSmsService;
import com.youwan.game.common.core.util.R;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author lion
 * @date 2019/02/14
 * feign token  fallback
 */
@Slf4j
@Component
public class RemoteSmsServiceFallbackImpl implements RemoteSmsService {

    @Setter
    private Throwable cause;

    /**
     * <B>发送短信信息</B>
     * @param sms
     * @param from
     * @return
     */
    @Override
    public R sendSmsInfo(SmsDto sms, String from) {
        log.error("feign 获取创建SMS消息失败", cause);
        return null;
    }

    @Override
    public R getSmsTemplate(SmsDto sms, String from) {
        log.error("feign 获取创建SMS模板失败", cause);
        return null;
    }
}
