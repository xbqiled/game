package com.youwan.game.cac.api.feign.fallback;


import com.youwan.game.cac.api.dto.DeviceInfoDto;
import com.youwan.game.cac.api.dto.SmsDto;
import com.youwan.game.cac.api.dto.VerifyDeviceInfo;
import com.youwan.game.cac.api.entity.DeviceInfo;
import com.youwan.game.cac.api.feign.RemoteDeviceInfoService;
import com.youwan.game.cac.api.feign.RemoteSmsService;
import com.youwan.game.common.core.util.R;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author lion
 * @date 2019/02/14
 * feign token  fallback
 */
@Slf4j
@Component
public class RemoteDeviceInfoServiceFallbackImpl implements RemoteDeviceInfoService {

    @Setter
    private Throwable cause;

    @Override
    public R addDeviceInfo(DeviceInfoDto deviceInfoDto, String from) {
        log.error("feign 提交设备信息失败", cause);
        return null;
    }

    @Override
    public R<Map<String,String>> verifyDeviceInfo(VerifyDeviceInfo verifyDeviceInfo, String from) {
        log.error("feign 验证设备信息失败", cause);
        return null;
    }
}
