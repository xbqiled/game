package com.youwan.game.cac.api.feign.fallback;


import com.youwan.game.cac.api.entity.SysPayConfig;
import com.youwan.game.cac.api.feign.RemotePayService;
import com.youwan.game.cac.api.vo.PayChannelVo;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.util.R;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

/**
 * @author lion
 * @date 2019/02/14
 * feign token  fallback
 */
@Slf4j
@Component
public class RemotePayServiceFallbackImpl implements RemotePayService {

    @Setter
    private Throwable cause;

    /**
     * <B> 获取支付配置信息</B>
     * @param channelId
     * @return
     */
    @Override
    public R<List<PayChannelVo>> queryPayChannel(String channelId, String from) {
        log.error("feign 获取支付配置信息失败", cause);
        return null;
    }


    @Override
    public R<PayChannelVo> queryPayConfig(String configId, String from) {
        log.error("feign 获取支付配置信息失败", cause);
        return null;
    }
}
