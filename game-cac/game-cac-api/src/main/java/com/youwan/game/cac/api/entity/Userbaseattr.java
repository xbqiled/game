package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author code generator
 * @date 2019-02-01 14:44:56
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_userBaseAttr")
public class Userbaseattr extends Model<Userbaseattr> {
    private static final long serialVersionUID = 1L;

    @TableId
    private Integer tAccountid;

    private Integer tSex;

    private Integer tViplevel;

    private Integer tGoldcoin;

    private Integer tAvatarid;

    private String tBankpassword;

    private Long tBankgoldcoin;

    private Integer tIsmodifiednick;

    private byte[] tModulescattereddata;

    private Integer tTotalgametime;

    private Integer tTodaygametime;

    private Integer tTodaygametimerefreshtime;

    private Long tTotalcoinget;

    private Long tTodaycoinget;

    private Integer tTodaycoinrefreshtime;

    private Long tRechargemoney;

    private byte[] tGameinfo;

    private Long tLastgamebetscore;

    private Long tTodaygamebetscore;

    private Long tBetscorersftime;

}
