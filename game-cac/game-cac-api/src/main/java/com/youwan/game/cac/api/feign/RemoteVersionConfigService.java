package com.youwan.game.cac.api.feign;


import com.youwan.game.cac.api.dto.VersionConfigDto;
import com.youwan.game.cac.api.entity.SysVersionConfig;
import com.youwan.game.cac.api.feign.factory.RemoteVersionConfigServiceFallbackFactory;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.constant.ServiceNameConstant;
import com.youwan.game.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;


/**
 * <B>版本信息配置相关服务</B>
 * @author lion
 * @date 2019/02/14
 * feign token
 */
@FeignClient(value = ServiceNameConstant.CAC_SERVICE, fallbackFactory = RemoteVersionConfigServiceFallbackFactory.class)
public interface RemoteVersionConfigService {

    @PostMapping("/sysversionconfig/config/")
    R<List<SysVersionConfig>> queryVersionConfig(@RequestBody VersionConfigDto config , @RequestHeader(SecurityConstants.FROM) String from);

}
