package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-22 14:18:24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_sms")
public class SysSms extends Model<SysSms> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId
    private Integer id;
    /**
     *
     */
    private String templateCode;
    /**
   * 
   */
    private String phoneNumber;
    /**
   * 
   */
    private String content;
    /**
   * 
   */
    private String createUser;
    /**
   * 
   */
    private LocalDateTime createTime;
    /**
   * 
   */
    private String state;
    /**
     *
     */
    private String parameter;
    /**
     *
     */
    private String channelId;
  
}
