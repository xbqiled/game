package com.youwan.game.cac.api.dto;


import lombok.Data;



@Data
public class CashOrderDto {
    private String accountId;

    private String exchangeMoney;

    private String channelId;

    private String type;

    private String realName;

    private String payId;

    private String bank;

    private String subBank;

    private String bankAddress;

    public CashOrderDto(String accountId, String type) {
        this.accountId = accountId;
        this.type = type;
    }


    public CashOrderDto(String accountId, String exchangeMoney, String type, String realName, String payId) {
        this.accountId = accountId;
        this.type = type;

        this.exchangeMoney = exchangeMoney;
        this.realName = realName;
        this.payId = payId;
    }


    public CashOrderDto(String accountId, String exchangeMoney, String type, String realName, String payId, String channelId) {
        this.accountId = accountId;
        this.type = type;

        this.exchangeMoney = exchangeMoney;
        this.realName = realName;
        this.payId = payId;
        this.channelId = channelId;
    }



    public CashOrderDto(String accountId, String exchangeMoney, String type, String realName, String payId, String bank, String subBank, String bankAddress) {
        this.accountId = accountId;
        this.type = type;

        this.exchangeMoney = exchangeMoney;
        this.realName = realName;
        this.payId = payId;

        this.bank = bank;
        this.subBank = subBank;
        this.bankAddress = bankAddress;
    }


    public CashOrderDto(String accountId, String channelId , String exchangeMoney, String type, String realName, String payId, String bank, String subBank, String bankAddress) {
        this.accountId = accountId;
        this.type = type;

        this.exchangeMoney = exchangeMoney;
        this.realName = realName;
        this.payId = payId;

        this.bank = bank;
        this.subBank = subBank;
        this.bankAddress = bankAddress;

        this.channelId = channelId;
    }
}
