package com.youwan.game.cac.api.entity;

import lombok.Data;

@Data
public class BindUrlForm {
    private String url;

    private String accountId;

    private String channelId;


}
