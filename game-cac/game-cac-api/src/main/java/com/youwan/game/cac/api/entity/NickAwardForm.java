package com.youwan.game.cac.api.entity;

import lombok.Data;

@Data
public class NickAwardForm {
    private String channelId;
    private String channel;
    private Integer openType;
    private Integer reward;
}
