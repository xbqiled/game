package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-18 18:58:08
 */
@Data

public class BindUrlEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    private String tUrl;

    private String tAccountid;

    private String tChannel;

}
