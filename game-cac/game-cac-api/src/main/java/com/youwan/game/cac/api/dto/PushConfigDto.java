package com.youwan.game.cac.api.dto;


import lombok.Data;

import java.io.Serializable;

@Data
public class PushConfigDto implements Serializable {

    private String channelId;

    private String name;

    private String status;

}
