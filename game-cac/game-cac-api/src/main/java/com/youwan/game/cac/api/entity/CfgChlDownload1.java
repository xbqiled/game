package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-12-30 15:25:40
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cfg_channel_download1")
public class CfgChlDownload1 extends Model<CfgChlDownload1> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private Integer id;
    /**
   * 渠道编码
   */
    private String channelId;
    /**
   * logoUrl
   */
    private String logoUrl;
    /**
   * 客服url
   */
    private String csUrl;
    /**
   * 客服提示
   */
    private String csTips;
    /**
   * 活动信息
   */
    private String activityMsg;
    /**
   * 背景url
   */
    private String backgroundUrl;
    /**
   * 修改时间
   */
    private LocalDateTime modifyTime;
    /**
   * 修改人
   */
    private String modifyBy;
    /**
   * 创建时间
   */
    private LocalDateTime createTime;
    /**
   * 创建人
   */
    private String createBy;
  
}
