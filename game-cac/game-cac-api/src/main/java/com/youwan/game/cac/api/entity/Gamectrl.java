package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author code generator
 * @date 2019-04-11 17:01:03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_gameCtrl")
public class Gamectrl extends Model<Gamectrl> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId
    private Integer tGameid;
    /**
     *
     */
    private byte[] tExtend;

}
