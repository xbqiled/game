package com.youwan.game.cac.api.feign.factory;

import com.youwan.game.cac.api.feign.RemoteBettingRebateService;
import com.youwan.game.cac.api.feign.RemoteBettingRecordQuery;
import com.youwan.game.cac.api.feign.fallback.RemoteBettingRebateServiceFallbackImpl;
import com.youwan.game.cac.api.feign.fallback.RemoteBettingRecordQueryFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-10-10.
 */
@Component
public class RemoteBettingRebateServiceFallbackFactory implements FallbackFactory<RemoteBettingRebateService> {
    @Override
    public RemoteBettingRebateService create(Throwable throwable) {
        RemoteBettingRebateServiceFallbackImpl remoteBettingRebateServiceFallback = new RemoteBettingRebateServiceFallbackImpl();
        remoteBettingRebateServiceFallback.setCause(throwable);
        return remoteBettingRebateServiceFallback;
    }
}
