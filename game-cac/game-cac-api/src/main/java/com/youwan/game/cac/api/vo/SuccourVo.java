package com.youwan.game.cac.api.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class SuccourVo implements Serializable {

    private String channelId;

    private byte[] succourInfo;

    private Boolean openType;

    private Integer depositType;

    private String iconUrl;

    private String channelName;

    private String cfgTime;

}
