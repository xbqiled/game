package com.youwan.game.cac.api.feign.fallback;


import com.youwan.game.cac.api.dto.CashOrderDto;
import com.youwan.game.cac.api.feign.RemoteCashOrderService;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.util.R;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * @author lion
 * @date 2019/02/14
 * feign token  fallback
 */
@Slf4j
@Component
public class RemoteCashOrderServiceFallbackImpl implements RemoteCashOrderService {

    @Setter
    private Throwable cause;


    /**
     * <B>生成提现订单信息</B>
     * @param cashOrderDto
     * @param from
     * @return
     */
    public R createCashOrder(CashOrderDto cashOrderDto, String from) {
        log.error("feign 生成提现订单信息失败", cause);
        return null;
    }


    @Override
    public R getOrderByUserId(String userId, String from) {
        log.error("feign 生成用户提现信息失败", cause);
        return null;
    }


    @Override
    public R getCashOrderInfo(@RequestBody CashOrderDto cashOrderDto, String from) {
        log.error("feign 生成用户提现信息失败", cause);
        return null;
    }
}
