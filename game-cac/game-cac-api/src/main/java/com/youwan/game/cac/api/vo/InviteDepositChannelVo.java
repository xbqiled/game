package com.youwan.game.cac.api.vo;

import lombok.Data;

import java.io.Serializable;


@Data
public class InviteDepositChannelVo implements Serializable {

    private String channelId;
    /**
     *
     */
    private Long timeType;
    /**
     *
     */
    private Long firstTotalDeposit;
    /**
     *
     */
    private String beginTimeStr;
    /**
     *
     */
    private String endTimeStr;
    /**
     *
     */
    private Long beginTime;
    /**
     *
     */
    private Long endTime;
    /**
     *
     */
    private Long selfReward;
    /**
     *
     */
    private Long upperReward;
    /**
     *
     */
    private Long cfgTime;
    /**
     *
     */
    private Boolean openType;
    /**
     *
     */
    private Integer depositType;
    /**
     *
     */
    private String channelName;

    /**
     *
     */
    private String iconUrl;


}
