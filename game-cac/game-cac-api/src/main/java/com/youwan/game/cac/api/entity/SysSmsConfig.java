package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-01-24 13:41:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_sms_config")
public class SysSmsConfig extends Model<SysSmsConfig> {
private static final long serialVersionUID = 1L;

    /**
   * ID
   */
    @TableId
    private Integer id;

    /**
     *
     */
    private Integer type;
    /**
   * 名称
   */
    private String name;
    /**
   * 描述
   */
    private String description;
    /**
   * 类名
   */
    private String className;
    /**
   * 配置
   */
    private String config;
    /**
   * 有效性标识,只有一个有效
   */
    private String isActivate;
    /**
   * 创建时间
   */
    private LocalDateTime createTime;
    /**
   * 创建人
   */
    private String createBy;
  
}
