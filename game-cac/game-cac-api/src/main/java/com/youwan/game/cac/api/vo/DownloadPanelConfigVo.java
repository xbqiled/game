package com.youwan.game.cac.api.vo;

import lombok.Data;

import java.io.Serializable;


@Data
public class DownloadPanelConfigVo implements Serializable {

    /**
     * 渠道编码
     */
    private Integer channelId;
    /**
     * 渠道名称
     */
    private String channelName;

    /**
     * iconUrl地址
     */
    private String iconUrl;

    /**
     * 头部URL
     */
    private String headUrl;
    /**
     * 中部URL
     */
    private String bodyUrl;
    /**
     * 脚部URL
     */
    private String footUrl;
    /**
     * 背景色
     */
    private String backgroundUrl;
}
