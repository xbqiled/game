package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-04-10 21:03:25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_bind_mobile")
public class BindMobile extends Model<BindMobile> {
private static final long serialVersionUID = 1L;

    /**
   * 渠道编码
   */
    private String channelId;
    /**
   * 赠送值
   */
    private BigDecimal giveValue;
    /**
     * 第一级返利返利比例(万分比)

     */
    private Integer rebate;
    /**
     * 是否开放兑换 1-开放 0-关闭
     */
    private Integer exchange;
    /**
     * 版本号
     */
    private String versionNo;
    /**
     * 最低兑换额度, 单位分, 即100元

     */
    private Integer minExchange;
    /**
     * 初始化信息
     */
    private Integer initCoin;

    /**
     * 打码量倍率
     */
    private Integer damaMulti;


    private Integer rebateDamaMulti;


    private Integer promoteType;


    private Integer exchangeCount;


    private Integer resetDamaLeftCoin;

    /**
     * 第三方站点Id
     */
    private Integer cpStationId;

    /**
     * 第三方站点金币限制，单位分
     */
    private Integer cpLimit;

    /**
   * 创建时间
   */
    private LocalDateTime createTime;
    /**
   * 修改人
   */
    private String createBy;
    /**
   * 修改时间
   */
    private LocalDateTime modifyTime;
    /**
   * 修改人
   */
    private String modifyBy;
  
}
