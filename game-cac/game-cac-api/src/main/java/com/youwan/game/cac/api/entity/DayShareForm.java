package com.youwan.game.cac.api.entity;

import com.youwan.game.common.core.jsonmodel.GsonDayShare;
import lombok.Data;

import java.util.List;
@Data
public class DayShareForm {
    private String channelId;
    private Integer openType;
    private Long dayAward;
    private Long awardDayShareCnt;
    private List<GsonDayShare> dataList;

    //子参数
    private String totalDay;
    private String additionAward;
}
