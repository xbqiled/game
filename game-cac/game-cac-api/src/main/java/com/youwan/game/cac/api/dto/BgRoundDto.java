package com.youwan.game.cac.api.dto;


import lombok.Data;

import java.io.Serializable;

@Data
public class BgRoundDto implements Serializable {

    private String round;
    private String startTime;
    private String endTime;

}
