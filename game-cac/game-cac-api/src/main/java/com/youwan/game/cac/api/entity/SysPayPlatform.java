package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-27 16:32:02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_pay_platform")
public class SysPayPlatform extends Model<SysPayPlatform> {
private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private Integer id;
    /**
     *
     */
    private String platformKey;
    /**
     *
     */
    private Integer handType;
    /**
     *
     */
    private Integer method;
    /**
     * 公司名称
     */
    private String company;
    /**
     * 状态 1有效  0无效
     */
    private Integer status;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    private LocalDateTime modifyTime;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 修改人
     */
    private String modifyBy;
    /**
     * 排序
     */
    private Integer sortNo;
    /**
     * 说明
     */
    private String note;
  
}
