package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;


@Data
public class GameCtrlDto implements Serializable {
    private String roomId;
    private Integer safeDown;

    private Integer safeUp;
    private Integer dangerDown;
    private Integer dangerUp;
}
