package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;


@Data
public class DaMaValueDto implements Serializable {
    private String channelId;
    private Integer [] userId;
}
