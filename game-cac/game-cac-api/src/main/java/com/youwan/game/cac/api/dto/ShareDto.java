package com.youwan.game.cac.api.dto;
import lombok.Data;

import java.io.Serializable;

@Data
public class ShareDto implements Serializable {
    private String channelId;
    private String url;
    private String isRefresh;

    public ShareDto(String url, String channelId, String isRefresh) {
        this.channelId = channelId;
        this.url = url;
        this.isRefresh = isRefresh;
    }
}

