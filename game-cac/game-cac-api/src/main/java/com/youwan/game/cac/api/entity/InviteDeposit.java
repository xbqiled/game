package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-08-03 15:31:04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_invite_Deposit")
public class InviteDeposit extends Model<InviteDeposit> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private String tChannel;
    /**
   * 
   */
    private Integer tOpentype;
    /**
   * 
   */
    private Integer tTimetype;
    /**
   * 
   */
    private Integer tBegintime;
    /**
   * 
   */
    private Integer tEndtime;
    /**
   * 
   */
    private Integer tFirsttotaldeposit;
    /**
   * 
   */
    private Integer tSelfreward;
    /**
   * 
   */
    private Integer tUpperreward;
    /**
   * 
   */
    private Integer tCfgtime;
  
}
