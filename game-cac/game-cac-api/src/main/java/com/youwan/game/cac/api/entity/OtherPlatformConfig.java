package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-10-15 16:55:44
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("other_platform_config")
public class OtherPlatformConfig extends Model<OtherPlatformConfig> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId
    private Integer id;
    /**
   * 平台编码
   */
    private String platformCode;
    /**
   * 平台名称
   */
    private String platformName;
    /**
   * 接口地址
   */
    private Integer platformType;
    /**
   * 平台配置
   */
    private String platformConfig;
    /**
   * 状态 0,正常；1，维护 2,失效
   */
    private Integer status;
    /**
   * 创建时间
   */
    private LocalDateTime createTime;
    /**
   * 创建人
   */
    private String createBy;
    /**
   * 
   */
    private LocalDateTime modifyTime;
    /**
   * 如果系统处理，那么记录SYSTME, 手动处理记录编码
   */
    private String modifyBy;
    /**
   * 备注
   */
    private String note;
  
}
