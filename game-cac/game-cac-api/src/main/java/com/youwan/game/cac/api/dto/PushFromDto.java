package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Data
public class PushFromDto implements Serializable {

    private String channelId;
    private String devicePlatform;
    private Integer timeType;

    private String retainTime;
    private Long timeLong;
    private Integer sendType;

    private String alias;
    private String title;
    private String content;

}