package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 
 *
 * @author code generator
 * @date 2020-01-13 20:25:56
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("tmp_bgredpacket_record")
public class OtherBgRedPacket extends Model<OtherBgRedPacket> {
private static final long serialVersionUID = 1L;

    /**
   * 记录ID
   */
    @TableId
    private Long pid;
    /**
   * 厅代码
   */
    private String sn;
    /**
   * 用户ID
   */
    private Long uid;
    /**
   * 中奖时间
   */
    private Date time;
    /**
   * 用户登录ID
   */
    private String account;
    /**
   * 余额
   */
    private Float balance;
    /**
   * 中奖金额
   */
    private Float amount;
  
}
