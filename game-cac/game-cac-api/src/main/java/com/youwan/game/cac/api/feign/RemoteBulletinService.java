package com.youwan.game.cac.api.feign;


import com.youwan.game.cac.api.feign.factory.RemoteBulletinServiceFallbackFactory;
import com.youwan.game.cac.api.vo.BulletinVo;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.constant.ServiceNameConstant;
import com.youwan.game.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * <B>公告服务feign</B>
 */
@FeignClient(value = ServiceNameConstant.CAC_SERVICE, fallbackFactory = RemoteBulletinServiceFallbackFactory.class)
public interface RemoteBulletinService {

    @PostMapping("/bulletin/getBulletinById/{bullitinId}")
    R<BulletinVo>  getBulletinInfo(@PathVariable("bullitinId") String bullitinId, @RequestHeader(SecurityConstants.FROM) String from);

}
