package com.youwan.game.cac.api.dto;


import lombok.Data;

import java.io.Serializable;

@Data
public class BgEnterGameDto implements Serializable {

    private String channelId;
}
