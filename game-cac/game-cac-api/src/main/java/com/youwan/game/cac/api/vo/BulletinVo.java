package com.youwan.game.cac.api.vo;


import lombok.Data;

import java.io.Serializable;


@Data
public class BulletinVo implements Serializable {

    private String tId;
    /**
     * 公告类型 1-公告栏 2-跑马灯
     */
    private Long tType;
    /**
     * 公告标题
     */
    private String tTitle;
    /**
     * 公告内容, 其中包括公告作用时间, 内容
     */
    private byte[] tContent;
    /**
     * 公告内容
     */
    private String content;
    /**
     * 公告超链接,没有则为空,指向图片等url地址
     */
    private String tHyperlink;
    /**
     * 公告开始时间
     */
    private String start;
    /**
     * 公告结束时间
     */
    private String finish;

    /**
     * 公告开始时间
     */
    private String tStart;

    /**
     * 公告结束时间
     */
    private String tFinish;

    /**
     * 影响渠道,json格式,记录所有渠道
     */
    private byte[] tChannellist;
    /**
     * 创建时间
     */
    private String tCreatetime;

    /**
     * 创建时间
     */
    private Integer bCreatetime;

    private String createtime;

    private String createTime;

    private String channelList;

}
