package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 角色
 *
 * @author code generator
 * @date 2019-02-09 17:17:39
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("chl_role")
public class ChlRole extends Model<ChlRole> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private Long roleId;
    /**
   * 角色名称
   */
    private String roleName;
    /**
   * 备注
   */
    private String remark;
    /**
   * 创建者ID
   */
    private Long createUserId;
    /**
   * 创建时间
   */
    private LocalDateTime createTime;
  
}
