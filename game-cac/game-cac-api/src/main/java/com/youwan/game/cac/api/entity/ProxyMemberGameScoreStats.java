package com.youwan.game.cac.api.entity;

import lombok.Data;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-04-29.
 * 牌局得分统计结果
 */
@Data
public class ProxyMemberGameScoreStats {

    /**
     * 游戏类型 subordinate
     */
    private String game;

    /**
     * 总局数
     */
    private Long boards;

    /**
     * 总分数
     */
    private Double scores;

    /**
     * 玩家数
     */
    private Long players;

    /**
     * 获胜局数
     */
    private Long winBoards;

    /**
     * 失败局数
     */
    private Long loseBoards;

    /**
     * 最小单局得分
     */
    private Double minScore;

    /**
     * 最大单局得分
     */
    private Double maxScore;

    /**
     * 平均单局得分
     */
    private Double avgScore;
}
