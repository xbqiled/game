package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code Lion
 * @date 2020-03-07 15:37:57
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("tmp_statis_game")
public class StatisGame extends Model<StatisGame> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId
    private Long id;
    /**
   * 游戏ID
   */
    private Long gameId;
    /**
   * 用户Id
   */
    private Long userId;
    /**
   * 游戏次数
   */
    private Long gameCount;
    /**
   * 渠道编码
   */
    private String channelId;
    /**
   * 赢取金额
   */
    private BigDecimal gameFee;
    /**
   * 统计时间
   */
    private LocalDateTime statisTime;
    /**
   * 下注金额
   */
    private BigDecimal betFee;
    /**
   * 税手金额
   */
    private BigDecimal revenueFee;
    /**
   * 是否比赛
   */
    private Integer isMatch;
    /**
   * 游戏时长
   */
    private Long gameTimeLength;
  
}
