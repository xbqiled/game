package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class GameCtrlCfgDto  implements Serializable {

    private String startTime;

    private String endTime;

}
