package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * VIEW
 *
 * @author pengbin
 * @email ismyjuliet@gmail.com
 * @date 2019-09-18 18:58:08
 */
@TableName("t_editnickaward")
public class NickAwardEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId
    private String tChannel;
    /**
     *
     */
    private Integer tOpentype;
    /**
     *
     */
    private byte[] tStrjsonaward;
    /**
     *
     */
    private Integer tCfgtime;

    /**
     * 设置：
     */
    public void setTChannel(String tChannel) {
        this.tChannel = tChannel;
    }

    /**
     * 获取：
     */
    public String getTChannel() {
        return tChannel;
    }

    /**
     * 设置：
     */
    public void setTOpentype(Integer tOpentype) {
        this.tOpentype = tOpentype;
    }

    /**
     * 获取：
     */
    public Integer getTOpentype() {
        return tOpentype;
    }

    /**
     * 设置：
     */
    public void setTStrjsonaward(byte[] tStrjsonaward) {
        this.tStrjsonaward = tStrjsonaward;
    }

    /**
     * 获取：
     */
    public byte[] getTStrjsonaward() {
        return tStrjsonaward;
    }

    /**
     * 设置：
     */
    public void setTCfgtime(Integer tCfgtime) {
        this.tCfgtime = tCfgtime;
    }

    /**
     * 获取：
     */
    public Integer getTCfgtime() {
        return tCfgtime;
    }
}
