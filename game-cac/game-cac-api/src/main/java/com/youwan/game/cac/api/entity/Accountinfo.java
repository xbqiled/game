package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-27 15:11:02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_accountInfo")
public class Accountinfo extends Model<Accountinfo> {
private static final long serialVersionUID = 1L;

    @TableId
    private Integer tAccountid;
    /**
     *
     */
    private String tAccountname;
    /**
     *
     */
    private String tPhonenumber;
    /**
     *
     */
    private String tChannelkey;
    /**
     *
     */
    private String tAccountkey;
    /**
     *
     */
    private String tNickname;
    /**
     *
     */
    private Integer tBisregularaccount;
    /**
     *
     */
    private String tIdcard;
    /**
     *
     */
    private String tLockmachine;
    /**
     *
     */
    private Integer tLastlogintime;
    /**
     *
     */
    private Integer tLastlogouttime;
    /**
     *
     */
    private String tCommondevices;
    /**
     *
     */
    private String tCommonips;
    /**
     *
     */
    private Integer tAccountstate;
    /**
     *
     */
    private Integer tAuthpwdfailtimes;
    /**
     *
     */
    private String tExcepverifyinfo;
    /**
     *
     */
    private Integer tRegtime;
    /**
     *
     */
    private Integer tRegterminaltype;
    /**
     *
     */
    private String tRegip;
    /**
     *
     */
    private String tRegdevice;
    /**
     *
     */
    private Integer tIsuniqdevicecode;
    /**
     *
     */
    private Integer tSharetype;
    /**
     *
     */
    private Integer tSharetriggerfuncid;
    /**
     *
     */
    private String tAlipayinfo;
    /**
     *
     */
    private String tBankinfo;
  
}
