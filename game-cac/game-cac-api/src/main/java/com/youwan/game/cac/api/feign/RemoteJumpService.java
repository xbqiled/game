package com.youwan.game.cac.api.feign;


import com.youwan.game.cac.api.dto.ShareDto;
import com.youwan.game.cac.api.entity.SourceUrl;
import com.youwan.game.cac.api.feign.factory.RemoteJumpServiceFallbackFactory;
import com.youwan.game.cac.api.vo.ShortUrlVo;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.constant.ServiceNameConstant;
import com.youwan.game.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;


/**
 * <B>微信信息接口</B>
 */
@FeignClient(value = ServiceNameConstant.CAC_SERVICE, fallbackFactory = RemoteJumpServiceFallbackFactory.class)
public interface RemoteJumpService {

    @PostMapping("/shorturl/createShortUrl/")
    R<ShortUrlVo> createShortUrl(@RequestBody ShareDto shareDto, @RequestHeader(SecurityConstants.FROM) String from);


    @GetMapping("/sourceurl/getById/{id}")
    R<SourceUrl> getSourceUrl(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

}
