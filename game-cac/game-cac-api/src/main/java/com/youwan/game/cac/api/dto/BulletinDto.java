package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;



@Data
public class BulletinDto implements Serializable {
    private  String tid;
    private  String start;
    private  String finish;
    private  String title;
    private  String content;
    private  String imgUrl;
    private  String channelList;
}
