package com.youwan.game.cac.api.entity;

import com.youwan.game.common.data.core.BaseEntity;
import lombok.Data;

import javax.persistence.Id;
import java.util.List;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-04-22.
 */
@Data
public class ProxyMember extends BaseEntity {

    @Id
    private String id;
    private String t_phoneNumber;
    private String t_nickName;
    private String t_origin;
    private Long t_regTime;
    private Integer t_regTerminalType;
    private String t_regIP;
    private String t_regDevice;
    private Long t_lastLoginTime;
    private Long t_lastLogoutTime;
    private String t_channelKey;
    private Integer t_accountState;
    private List<String> m_path;
    private Long pid;
    private int deep;

    private ProxyMemberTeamMetrics teamMetrics;
}
