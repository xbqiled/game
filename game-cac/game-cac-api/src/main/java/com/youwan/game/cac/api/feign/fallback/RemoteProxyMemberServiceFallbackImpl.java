package com.youwan.game.cac.api.feign.fallback;

import com.google.common.collect.ImmutableList;
import com.youwan.game.cac.api.dto.ProxyMemberQueryDto;
import com.youwan.game.cac.api.entity.ProxyMember;
import com.youwan.game.cac.api.entity.ProxyMemberProfile;
import com.youwan.game.cac.api.entity.ProxyMemberTeamMetrics;
import com.youwan.game.cac.api.feign.RemoteProxyMemberService;
import com.youwan.game.common.core.util.R;
import com.youwan.game.common.data.core.Page;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-04-27.
 */
@Slf4j
@Component
public class RemoteProxyMemberServiceFallbackImpl implements RemoteProxyMemberService {

    @Setter
    private Throwable cause;

    @Override
    public R getMemberProfile(Long accountId, String from) {
        log.error("feign get member profile fail", cause);
        ProxyMemberProfile ret = new ProxyMemberProfile();
        ret.setMyId(accountId);
        return new R(ret);
    }

    @Override
    public R<Page<ProxyMember>> getDirectSubordinates(Long accountId, ProxyMemberQueryDto dto, String from) {
        log.error("feign get direct Subordinates fail", cause);
        return new R(Page.empty());
    }

    @Override
    public R getAllSubordinates(Long accountId, ProxyMemberQueryDto dto, String from) {
        log.error("feign get all Subordinates fail", cause);
        return new R(Page.empty());
    }

    @Override
    public R getTreasureChanges(Long accountId, ProxyMemberQueryDto queryDto, String from) {
        log.error("feign get treasureChanges fail", cause);
        return new R(Page.empty());
    }

    @Override
    public R getRechargeOrders(Long accountId, ProxyMemberQueryDto queryDto, String from) {
        log.error("feign get rechargeOrders fail", cause);
        return new R(Page.empty());
    }

    @Override
    public R getCashOrders(Long accountId, ProxyMemberQueryDto queryDto, String from) {
        log.error("feign get cashOrders fail", cause);
        return new R(Page.empty());
    }

    @Override
    public R getGameScoreStats(Long accountId, ProxyMemberQueryDto queryDto, String from) {
        log.error("feign get gameScoreStats fail", cause);
        return new R(ImmutableList.of());
    }

    @Override
    public R getGameScores(Long accountId, ProxyMemberQueryDto queryDto, String from) {
        log.error("feign get gameScores fail", cause);
        return new R(Page.empty());
    }

    @Override
    public R getTeamMetrics(Long accountId, ProxyMemberQueryDto queryDto, String from) {
        return new R(new ProxyMemberTeamMetrics());
    }

    @Override
    public R getSubordinateTeamMetrics(Long accountId, ProxyMemberQueryDto queryDto, String from) {
        return new R(Page.empty());
    }

    @Override
    public R getRebateDetails(Long accountId, ProxyMemberQueryDto queryDto, String from) {
        log.error("feign get rebate details", cause);
        return new R(Page.empty());
    }

    @Override
    public R getRebateBalances(Long accountId, ProxyMemberQueryDto queryDto, String from) {
        log.error("feign get rebate balances", cause);
        return new R(Page.empty());
    }
}
