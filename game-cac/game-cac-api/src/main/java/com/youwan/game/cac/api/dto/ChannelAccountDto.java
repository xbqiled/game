package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ChannelAccountDto implements Serializable {

    private String channelId;

    private BigDecimal amount;

    private Integer platformType;

    private Integer operateType;

}
