package com.youwan.game.cac.api.vo;

import java.io.Serializable;

public class NickAwardVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    private String channelId;

    private Integer depositType;

    private Boolean openType;

    private byte[] strJsonAward;

    private Integer cfgTime;

    private String cfgTimeStr;

    private String channelName;

    private String iconUrl;

    private Integer award;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Integer getDepositType() {
        return depositType;
    }

    public void setDepositType(Integer depositType) {
        this.depositType = depositType;
    }

    public Boolean getOpenType() {
        return openType;
    }

    public void setOpenType(Boolean openType) {
        this.openType = openType;
    }

    public byte[] getStrJsonAward() {
        return strJsonAward;
    }

    public void setStrJsonAward(byte[] strJsonAward) {
        this.strJsonAward = strJsonAward;
    }

    public Integer getCfgTime() {
        return cfgTime;
    }

    public void setCfgTime(Integer cfgTime) {
        this.cfgTime = cfgTime;
    }

    public String getCfgTimeStr() {
        return cfgTimeStr;
    }

    public void setCfgTimeStr(String cfgTimeStr) {
        this.cfgTimeStr = cfgTimeStr;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Integer getAward() {
        return award;
    }

    public void setAward(Integer award) {
        this.award = award;
    }
}
