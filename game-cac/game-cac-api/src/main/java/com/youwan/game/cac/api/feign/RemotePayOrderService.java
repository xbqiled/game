package com.youwan.game.cac.api.feign;


import com.youwan.game.cac.api.dto.AddPointDto;
import com.youwan.game.cac.api.dto.NotifyPayOrderDto;
import com.youwan.game.cac.api.dto.PayOrderDto;
import com.youwan.game.cac.api.feign.factory.RemoteCashOrderServiceFallbackFactory;
import com.youwan.game.cac.api.vo.*;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.constant.ServiceNameConstant;
import com.youwan.game.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * <B> 支付订单feign</B>
 */
@FeignClient(value = ServiceNameConstant.CAC_SERVICE, fallbackFactory = RemoteCashOrderServiceFallbackFactory.class)
public interface RemotePayOrderService {

    @PostMapping("/payorder/createPayOrder/")
    R<PayResult> createPayOrder(@RequestBody PayOrderDto payOrderDto, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/payorder/callbackPayOrder/")
    R callbackPayOrdery(@RequestBody NotifyPayOrderDto payOrderDto, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/payorder/addPoint/")
    R<AddPointVo> addPoint(@RequestBody AddPointDto addPointDto, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/payorder/getPayOrder/{orderId}")
    R getPayOrder(@PathVariable("orderId") String orderId, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/payorder/getMerchantKey/{orderId}")
    R<NotifyOrderVo> getMerchantKey(@PathVariable("orderId") String orderId, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/payconfig/getPayConfig/{merchantCode}")
    R<PayConfigVo> getPayConfig(@PathVariable("merchantCode") String merchantCode, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/payorder/getPayUrl/")
    R getPayUrl(@RequestBody PayOrderDto payOrderDto, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/payorder/getPayInfo/")
    R<PayConfigVo> getPayInfo(@RequestBody PayOrderDto payOrderDto, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/paycode/getPayCode/{orderNo}")
    R<PayCodeVo> getPayCode(@PathVariable("orderNo") String orderNo, @RequestHeader(SecurityConstants.FROM) String from);

}
