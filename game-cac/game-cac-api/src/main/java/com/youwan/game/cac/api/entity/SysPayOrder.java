package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-02-20 18:03:40
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_pay_order")
public class SysPayOrder extends Model<SysPayOrder> {
private static final long serialVersionUID = 1L;

    @TableId
    private Integer id;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 充值金额
     */
    private BigDecimal payFee;
    /**
     * 手续费
     */
    private BigDecimal poundage;
    /**
     * 渠道ID
     */
    private String channelId;
    /**
     * 状态，0:等待支付, 1,支付成功, 2,支付失败
     */
    private Integer status;
    /**
     * 支付方式编码
     */
    private Integer payConfigId;
    /**
     * 支付方式名称
     */
    private String payName;
    /**
     * 姓名
     */
    private String depositor;
    /**
     * 用户账号
     */
    private String account;
    /**
     * 处理方式（1、 手动处理,2、系统处理）
     */
    private Integer handlerType;
    /**
     *
     */
    private LocalDateTime createTime;
    /**
     * 如果系统处理，那么记录SYSTME, 手动处理记录编码
     */
    private String createBy;
    /**
     *
     */
    private LocalDateTime modifyTime;
    /**
     * 如果系统处理，那么记录SYSTME, 手动处理记录编码
     */
    private String modifyBy;
    /**
     * 备注
     */
    private String note;
  
}
