package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 *
 * @author code generator
 * @date 2018-12-29 12:30:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_bag")
public class Bag extends Model<Bag> {
private static final long serialVersionUID = 1L;

    /**
   * 格子ID
   */
    @TableId
    private Integer tGridid;
    /**
   * 用户ID
   */
    private Integer tAccountid;
    /**
   * 物品ID
   */
    private Integer tItemid;
    /**
   * 物品数量
   */
    private Integer tCount;
    /**
   * 拓展参数
   */
    private String tExtendparams;
  
}
