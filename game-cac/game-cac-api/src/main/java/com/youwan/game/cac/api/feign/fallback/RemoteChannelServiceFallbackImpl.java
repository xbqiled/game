package com.youwan.game.cac.api.feign.fallback;


import com.youwan.game.cac.api.dto.*;
import com.youwan.game.cac.api.entity.BgenterConfig;
import com.youwan.game.cac.api.entity.OtherBgUser;
import com.youwan.game.cac.api.feign.RemoteChannelService;
import com.youwan.game.cac.api.vo.DaMaValue;
import com.youwan.game.cac.api.vo.DownloadPanelConfigVo;
import com.youwan.game.cac.api.vo.DownloadPanelConfigVo1;
import com.youwan.game.common.core.otherplatform.bg.BGPlatfromConfig;
import com.youwan.game.common.core.util.R;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author lion
 * @date 2019/02/14
 * feign token  fallback
 */
@Slf4j
@Component
public class RemoteChannelServiceFallbackImpl implements RemoteChannelService {

    @Setter
    private Throwable cause;


    /**
     * <B>获取渠道信息</B>
     * @param id
     * @param from
     * @return
     */
    @Override
    public R getChannelInfo(String id, String from) {
        log.error("feign 获取支付配置信息失败", cause);
        return null;
    }

    @Override
    public R<OtherBgUser> findBgUser(BgUserDto bgUserDto, String from) {
        log.error("feign 获取用户信息失败", cause);
        return null;
    }

    @Override
    public R<OtherBgUser> createBgUser(BgUserDto bgUserDto, String from) {
        log.error("feign 创建用户信息失败", cause);
        return null;
    }

    @Override
    public R<String> openBgGame(OpenBgGameDto openBgGameDto, String from) {
        log.error("feign 获取游戏信息失败", cause);
        return null;
    }

    @Override
    public R<Map<String, BigDecimal>> bgUserBalance(BgUserBalanceDto bgUserBalanceDto, String from) {
        log.error("feign 获取用户余额信息失败", cause);
        return null;
    }

    @Override
    public R<Integer> bgTransferbalance(BgTransferBalanceDto bgTransferBalanceDto, String from) {
        log.error("feign 额度转换失败", cause);
        return null;
    }

    @Override
    public R<BGPlatfromConfig> getbgConfig(String from) {
        log.error("feign 获取bg默认配置失败", cause);
        return null;
    }

    @Override
    public R<BgenterConfig> getBgEnterConfig(BgEnterConfig bgEnterConfig, String from) {
        log.error("feign 获取bg进入配置失败", cause);
        return null;
    }

    @Override
    public R<BgDamaGameDto> exitGame(BgExitGameDto bgExitGameDto, String from) {
        log.error("feign 用户退出处理失败", cause);
        return null;
    }

    @Override
    public R<DownloadPanelConfigVo> getDownloadPanelConfig(DownloadConfigDto downloadConfigDto, String from) {
        log.error("feign 获取下载界面配置失败", cause);
        return null;
    }

    @Override
    public R<DownloadPanelConfigVo1> getDownloadPanelConfig1(DownloadConfigDto downloadConfigDto, String from) {
        log.error("feign 获取下载界面配置失败", cause);
        return null;
    }


    @Override
    public R<List<DaMaValue>> getUserDamaValue(DaMaValueDto daMaValueDto, String from) {
        log.error("feign 获取用户打码量信息失败", cause);
        return null;
    }
}
