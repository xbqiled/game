package com.youwan.game.cac.api.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;


@Data
public class StatisChannelVo implements Serializable {

    private Long id;
    /**
     * 统计时间
     */
    private String statisTime;
    /**
     * 渠道编码
     */
    private String channelId;
    /**
     * 充值金额
     */
    private BigDecimal rechargeFee;
    /**
     * 充值人数
     */
    private Long rechargeUserCount;
    /**
     * 充值笔数
     */
    private Long rechargeCount;
    /**
     * 提现金额
     */
    private BigDecimal cashFee;
    /**
     * 提现人数
     */
    private Long cashUserCount;
    /**
     * 提现笔数
     */
    private Long cashCount;
    /**
     * 盈亏统计
     */
    private BigDecimal loss;
    /**
     * 新增注册人数
     */
    private Long regUserCount;
    /**
     * 新增游戏人数
     */
    private Long newGameUserCount;
    /**
     * 游戏次数
     */
    private Long gameCount;
    /**
     * 游戏人数
     */
    private Long gameUserCount;
    /**
     * 登录次数
     */
    private Long loginCount;
    /**
     * 登录人数
     */
    private Long loginUserCount;
    /**
     * 日活跃用户
     */
    private Long dailyActiveCount;
    /**
     * 首冲金额
     */
    private BigDecimal firstRechargeFee;
    /**
     * 首次充值人数
     */
    private Long firstRechargeCount;
    /**
     * 用户游戏总时长
     */
    private Long timeLength;
    /**
     * 用户平均时长
     */
    private Long userAvgGametime;
    /**
     * 充值率
     */
    private Long rechargeRate;
    /**
     * 步骤
     */
    private Integer step;


}
