package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-02-08 18:30:36
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_channel")
public class TChannel extends Model<TChannel> {
private static final long serialVersionUID = 1L;

    @TableId
    private Integer id;
    /**
     * 渠道标识
     */
    private String channelId;
    /**
     * 渠道名称
     */
    private String channelName;
    /**
     * 渠道类型 (0自有渠道, 1社会渠道)
     */
    private Integer type;
    /**
     * 1,正常， 0，关闭
     */
    private Integer state;
    /**
     * 图标URL
     */
    private String iconUrl;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 描述
     */
    private String note;
  
}
