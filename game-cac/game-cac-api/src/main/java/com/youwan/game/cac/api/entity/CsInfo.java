package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * @author code generator
 * @date 2019-03-29 15:44:57
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_cs_info")
public class CsInfo extends Model<CsInfo> {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId
    private Integer id;
    /**
     * 客服名称
     */
    private String csName;
    /**
     * 渠道编码
     */
    private String channelId;
    /**
     * 类型: 1,微信， 2，QQ， 3,URL
     */
    private Integer connetType;
    /**
     * 客服类型: 1,推广客服 2,游戏客服
     */
    private Integer type;
    /**
     * 账号或URL
     */
    private String accountOrUrl;
    /**
     * 状态 1, 正常， 0，失效
     */
    private Integer status;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 创建人
     */
    private String createBy;

}
