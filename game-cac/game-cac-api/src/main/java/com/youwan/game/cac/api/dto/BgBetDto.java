package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class BgBetDto implements Serializable {

    private String userId;
    private String startTime;
    private String endTime;

    private String gameName;
    private String issueId;
    private String orderId;
}
