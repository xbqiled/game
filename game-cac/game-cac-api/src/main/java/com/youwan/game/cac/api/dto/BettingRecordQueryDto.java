package com.youwan.game.cac.api.dto;

import lombok.Data;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-09-28.
 */
@Data
public class BettingRecordQueryDto {

    private Integer offset = 0;
    private Integer limit = 20;
    private String channelId;
    private Integer userId;
    private Long start;
    private Long end;

}
