package com.youwan.game.cac.api.vo;


import lombok.Data;

import java.io.Serializable;

@Data
public class DaMaValue implements Serializable {

    private Integer userId;

    private String phoneNumber;

    private Long damaValue = 0l;

    private Integer vipGrade;
}
