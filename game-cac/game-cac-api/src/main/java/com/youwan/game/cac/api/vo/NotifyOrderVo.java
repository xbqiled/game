package com.youwan.game.cac.api.vo;


import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;


@Data
public class NotifyOrderVo implements Serializable {


    private String orderNo;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 充值金额
     */
    private BigDecimal payFee;
    /**
     * 渠道ID
     */
    private String channelId;
    /**
     * 状态，0:等待支付, 1,支付成功, 2,支付失败
     */
    private Integer status;
    /**
     * 支付方式编码
     */
    private Integer payConfigId;
    /**
     * 支付方式名称
     */
    private String payName;
    /**
     * 处理方式（1、 手动处理,2、系统处理）
     */
    private Integer handlerType;
    /**
     * 收款人/商户号
     */
    private String merchantCode;
    /**
     * 收款银行/秘钥
     */
    private String merchantKey;
    /**
     * 开户网点/公钥
     */
    private String publicKey;
    /**
     * 支付网关
     */
    private String payGetway;


    private String appKey;

}
