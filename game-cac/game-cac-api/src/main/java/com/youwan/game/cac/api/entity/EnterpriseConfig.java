package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-06 13:19:25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wx_enterprise_config")
public class EnterpriseConfig extends Model<EnterpriseConfig> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId
    private Integer id;
    /**
   * 渠道编码
   */
    private String channelId;
    /**
   * appID
   */
    private String appId;
    /**
   * 创建人
   */
    private String createBy;
    /**
   * 创建时间
   */
    private LocalDateTime createTime;
  
}
