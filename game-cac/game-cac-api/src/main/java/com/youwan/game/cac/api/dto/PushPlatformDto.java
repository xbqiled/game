package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class PushPlatformDto implements Serializable {

    private String platformName;

}
