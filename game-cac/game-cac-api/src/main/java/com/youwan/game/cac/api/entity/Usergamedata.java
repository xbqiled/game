package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * VIEW
 *
 * @author code generator
 * @date 2019-04-10 21:03:48
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_userGameData")
public class Usergamedata extends Model<Usergamedata> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private Integer tAccountid;
    /**
   * 
   */
    private Integer tGameid;
    /**
   * 
   */
    private Integer tTotalround;
    /**
   * 
   */
    private Integer tHistwin;
    /**
   * 
   */
    private Integer tTodaywin;
    /**
   * 
   */
    private Integer tTodayrsftime;
    /**
   * 
   */
    private Integer tTotalplaycount;
    /**
   * 
   */
    private Integer tFirstentertime;
    /**
   * 
   */
    private Integer tLastentertime;
    /**
   * 
   */
    private byte[] tExtend;
    /**
   * 
   */
    private Integer tType;
    /**
   * 
   */
    private Integer tSettime;
    /**
   * 
   */
    private Integer tBetlimit;
    /**
   * 
   */
    private Integer tRound;
    /**
   * 
   */
    private Integer tRate;
    /**
   * 
   */
    private Integer tCtrlround;
    /**
   * 
   */
    private Integer tCtlscore;
    /**
     *
     */
    private Integer tCtltotalscore;
    /**
   * 
   */
    private Integer tCurscore;
    /**
   * 
   */
    private String tChannel;
  
}
