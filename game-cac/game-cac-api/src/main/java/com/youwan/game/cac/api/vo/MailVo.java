package com.youwan.game.cac.api.vo;


import lombok.Data;

import java.io.Serializable;


@Data
public class MailVo implements Serializable {

    private Integer tId;
    /**
     * 用户ID
     */
    private Integer tAccountid;
    /**
     * 邮件标题
     */
    private String tTitle;
    /**
     * 邮件内容
     */
    private byte[] tContent;

    /**
     * 邮件内容
     */
    private String content;
    /**
     * 邮件附件奖励列表
     */
    private String tRewardlist;
    /**
     * 发送人
     */
    private String tSender;
    /**
     * 邮件状态 1-未读 2-已读 3-领取
     */
    private Integer tState;
    /**
     * 是否文本邮件 0-否 1-是
     */
    private Integer tIstextmail;
    /**
     * 是否群发邮件 0-否 1-是
     */
    private Integer tIsgroupmail;
    /**
     * 是否模板邮件 0-否 1-是
     */
    private Integer tIstemplatemail;
    /**
     * 模板邮件参数
     */
    private String tTemplatemailinfo;
    /**
     * 发送日期
     */
    private String tSenddate;
    /**
     * 接收日期
     */
    private String tRecvdate;
    /**
     * 触发邮件游戏类型
     */
    private Integer tTriggergametype;
    /**
     * 业务类型
     */
    private Integer tBusinesstype;
    /**
     * 业务关联标识
     */
    private String tBusinessguid;


    private String tAccountname;


    private String tNickname;

}
