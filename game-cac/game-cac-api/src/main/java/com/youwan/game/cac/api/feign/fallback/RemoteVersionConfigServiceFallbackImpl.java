package com.youwan.game.cac.api.feign.fallback;


import com.youwan.game.cac.api.dto.VersionConfigDto;
import com.youwan.game.cac.api.entity.SysVersionConfig;
import com.youwan.game.cac.api.feign.RemoteVersionConfigService;
import com.youwan.game.common.core.util.R;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lion
 * @date 2019/02/14
 * feign token  fallback
 */
@Slf4j
@Component
public class RemoteVersionConfigServiceFallbackImpl implements RemoteVersionConfigService {

    @Setter
    private Throwable cause;

    /**
     * <B>获取版本信息</B>
     * @param config
     * @param from
     * @return
     */
    @Override
    public R<List<SysVersionConfig>> queryVersionConfig(VersionConfigDto config, String from) {
        log.error("feign 获取版本配置信息失败", cause);
        return null;
    }
}
