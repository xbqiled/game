package com.youwan.game.cac.api.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class TaskVo implements Serializable {

    private String channelId;

    private Boolean openType;

    private Integer activityOpenType;

    private byte[] taskInfo;

    private Integer activityTimeType;

    private String beginTime;

    private String endTime;

    private String cfgTime;

    private String iconUrl;

    private String channelName;
}
