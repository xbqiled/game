package com.youwan.game.cac.api.dto;


import lombok.Data;

import java.io.Serializable;

@Data
public class BgUserBalanceDto implements Serializable {
    private String userId;


    public BgUserBalanceDto(String userId) {
        this.userId = userId;
    }
}
