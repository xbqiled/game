package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统日志
 *
 * @author code generator
 * @date 2019-05-23 13:09:56
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("chl_log")
public class ChlLog extends Model<ChlLog> {
private static final long serialVersionUID = 1L;

    /**
   * 
   */
    @TableId
    private Long id;
    /**
   * 用户id
   */
    private Long userId;
    /**
   * 用户名
   */
    private String username;
    /**
   * 用户操作
   */
    private String operation;
    /**
   * 请求方法
   */
    private String method;
    /**
   * 请求参数
   */
    private String params;
    /**
   * 渠道编码
   */
    private String channelId;
    /**
   * 执行时长(毫秒)
   */
    private Long time;
    /**
   * IP地址
   */
    private String ip;
    /**
   * 创建时间
   */
    private LocalDateTime createDate;
  
}
