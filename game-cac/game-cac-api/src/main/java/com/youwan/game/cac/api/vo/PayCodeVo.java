package com.youwan.game.cac.api.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class PayCodeVo implements Serializable {

    private Integer id;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 二维码地址
     */
    private String imgUrl;

    /**
     * 支付地址
     */
    private String payUrl;
    /**
     * 渠道編碼
     */
    private String channelId;
    /**
     * 渠道名稱
     */
    private String channelName;
    /**
     * 图标URL
     */
    private String iconUrl;
    /**
     * 配置信息
     */
    private Integer configId;
    /**
     * 支付名称
     */
    private String payName;
    /**
     * 支付金额度
     */
    private BigDecimal payFee;
    /**
     * 支付时间
     */
    private LocalDateTime createTime;


}
