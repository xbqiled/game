package com.youwan.game.cac.api.dto;


import lombok.Data;

import java.io.Serializable;

@Data
public class MailDto implements Serializable  {

    private String accountId;
    private String title;

}
