package com.youwan.game.cac.api.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class BgBetOrder implements Serializable {

    private long orderId;
    private long tranId;

    private String sn;
    private long uid;
    private String loginId;

    private int moduleId;
    private String moduleName;
    private int gameId;

    private String gameName;
    private int orderStatus;
    private float bAmount;

    private float aAmount;
    private int orderFrom;
    private Date orderTime;

    private Date lastUpdateTime;
    private String fromIp;
    private String issueId;

    private String  playId;
    private String playName;
    private String playNameEn;

    private float validBet;
    private float payment;
}
