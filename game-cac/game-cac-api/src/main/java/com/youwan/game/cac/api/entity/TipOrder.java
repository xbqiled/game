package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 *
 * @author code generator
 * @date 2019-05-21 11:20:06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("tip_order")
public class TipOrder extends Model<TipOrder> {
private static final long serialVersionUID = 1L;

    /**
   * 主键编码
   */
    @TableId
    private Integer id;
    /**
   * 订单编号
   */
    private String orderNo;
    /**
   * 类型: 1, 支付订单 2,提现订单
   */
    private Integer type;
    /**
   * 状态: 1, 已提醒
   */
    private Integer status;
    /**
   * 已提醒次数
   */
    private Integer tipCount;
    /**
   * 创建时间
   */
    private LocalDateTime createTime;
  
}
