package com.youwan.game.cac.api.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class OpenTypeForm implements Serializable {

    String channelId;

    String openType;

    String isCirculate;

    String optionType;
}
