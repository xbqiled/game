package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 
 *
 * @author code generator
 * @date 2020-01-13 20:43:08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("tmp_bgredpacket_exchange")
public class OtherBgRedPacketExchange extends Model<OtherBgRedPacketExchange> {
private static final long serialVersionUID = 1L;

    /**
   * 记录ID
   */
    @TableId
    private Long pid;
    /**
   * 厅代码
   */
    private String sn;
    /**
   * 用户ID
   */
    private Long uid;
    /**
   * 兑换时间
   */
    private Date time;
    /**
   * 用户登录ID
   */
    private String account;
    /**
   * 红包余额
   */
    private Float balance;
    /**
   * 兑换金额
   */
    private Float amount;
  
}
