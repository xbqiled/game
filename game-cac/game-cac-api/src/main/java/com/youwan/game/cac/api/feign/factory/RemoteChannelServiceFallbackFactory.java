package com.youwan.game.cac.api.feign.factory;


import com.youwan.game.cac.api.feign.RemoteChannelService;
import com.youwan.game.cac.api.feign.fallback.RemoteChannelServiceFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author lion
 * @date 2019/2/14
 */
@Component
public class RemoteChannelServiceFallbackFactory implements FallbackFactory<RemoteChannelService> {


    @Override
    public RemoteChannelService create(Throwable throwable) {
        RemoteChannelServiceFallbackImpl remoteChannelServiceFallbackImpl = new RemoteChannelServiceFallbackImpl();
        remoteChannelServiceFallbackImpl.setCause(throwable);
        return remoteChannelServiceFallbackImpl;
    }
}
