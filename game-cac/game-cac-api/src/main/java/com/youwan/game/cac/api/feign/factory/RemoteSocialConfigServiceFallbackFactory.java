package com.youwan.game.cac.api.feign.factory;


import com.youwan.game.cac.api.feign.RemoteSocialConfigService;
import com.youwan.game.cac.api.feign.fallback.RemoteSocialConfigServiceFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;


/**
 * @author lion
 * @date 2019/2/14
 */
@Component
public class RemoteSocialConfigServiceFallbackFactory implements FallbackFactory<RemoteSocialConfigService> {


    @Override
    public RemoteSocialConfigService create(Throwable throwable) {
        RemoteSocialConfigServiceFallbackImpl remoteSocialConfigServiceFallbackImpl = new RemoteSocialConfigServiceFallbackImpl();
        remoteSocialConfigServiceFallbackImpl.setCause(throwable);
        return remoteSocialConfigServiceFallbackImpl;
    }
}
