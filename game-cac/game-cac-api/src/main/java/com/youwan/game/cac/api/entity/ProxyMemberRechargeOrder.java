package com.youwan.game.cac.api.entity;

import com.youwan.game.common.data.core.BaseEntity;
import lombok.Data;

import javax.persistence.Id;
import java.util.List;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-04-25.
 */
@Data
public class ProxyMemberRechargeOrder extends BaseEntity {

    @Id
    private String id;
    private String channelNO;
    private Long pid;
    private Long userID;
    private String nickName;
    private Integer payType;
    private Integer payAmount;
    private Long recordTime;
    private Integer payStatus;
    private String account;
    private List<String> m_path;
    private int deep;

}
