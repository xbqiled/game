package com.youwan.game.cac.api.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
public class WxhhCtrlCfgDto implements Serializable {

    private String keyId;

    private List<Map<String, Object>> ctrlArr;

    private Integer killInning;
}
