package com.youwan.game.cac.api.feign.fallback;


import com.youwan.game.cac.api.dto.ShareDto;
import com.youwan.game.cac.api.entity.SourceUrl;
import com.youwan.game.cac.api.feign.RemoteJumpService;
import com.youwan.game.cac.api.vo.ShortUrlVo;
import com.youwan.game.common.core.util.R;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lion
 * @date 2019/02/14
 * feign token  fallback
 */
@Slf4j
@Component
public class RemoteJumpServiceFallbackImpl implements RemoteJumpService {

    @Setter
    private Throwable cause;

    @Override
    public R<ShortUrlVo> createShortUrl(ShareDto shareDto, String from) {
        log.error("feign 获取短链接信息失败", cause);
        return null;
    }

    @Override
    public R<SourceUrl> getSourceUrl(String id, String from) {
        log.error("feign 获取落地地址信息失败", cause);
        return null;
    }
}
