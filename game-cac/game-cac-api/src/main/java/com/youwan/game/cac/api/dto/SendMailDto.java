package com.youwan.game.cac.api.dto;


import lombok.Data;

import java.io.Serializable;

@Data
public class SendMailDto implements Serializable {

    private  int sendType;
    private  String channelList;
    private  String userList;
    private  String title;
    private  String content;
    private  String rewardId;
    private  String rewardNum;
}
