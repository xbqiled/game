package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * @author code generator
 * @date 2019-04-19 13:18:35
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wx_source_url")
public class SourceUrl extends Model<SourceUrl> {
    private static final long serialVersionUID = 1L;
    /**
     * 主键自定义
     */
    @TableId
    private Integer id;
    /**
     * 唯一值
     */
    private String serialNum;
    /**
     * 短网址地址
     */
    private String shortUrl;
    /**
     * 源网址地址
     */
    private String sourceUrl;
    /**
     * 跳转地址， 做判断跳出微信浏览器
     */
    private String jumpUrl;
    /**
     * <B>申请的渠道</B>
     */
    private String channelId;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 1,正常2,失效3,拉黑
     */
    private Integer status;

}
