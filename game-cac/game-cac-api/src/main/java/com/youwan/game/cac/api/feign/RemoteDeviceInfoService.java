package com.youwan.game.cac.api.feign;


import com.youwan.game.cac.api.dto.DeviceInfoDto;
import com.youwan.game.cac.api.dto.VerifyDeviceInfo;
import com.youwan.game.cac.api.feign.factory.RemoteDeviceInfoServiceFallbackFactory;
import com.youwan.game.common.core.constant.SecurityConstants;
import com.youwan.game.common.core.constant.ServiceNameConstant;
import com.youwan.game.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.Map;

/**
 * <B> 设备信息服务feign</B>
 */
@FeignClient(value = ServiceNameConstant.CAC_SERVICE, fallbackFactory = RemoteDeviceInfoServiceFallbackFactory.class)
public interface RemoteDeviceInfoService {

    @PostMapping("/deviceinfo/addDeviceInfo/")
    R addDeviceInfo(@RequestBody DeviceInfoDto deviceInfoDto, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/deviceinfo/verifyDeviceInfo/")
    R<Map<String,String>> verifyDeviceInfo(@RequestBody VerifyDeviceInfo verifyDeviceInfo, @RequestHeader(SecurityConstants.FROM) String from);
}
