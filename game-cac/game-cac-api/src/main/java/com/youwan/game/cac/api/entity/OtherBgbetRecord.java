package com.youwan.game.cac.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 
 *
 * @author code generator
 * @date 2019-11-05 15:56:55
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("tmp_bgbet_record")
public class OtherBgbetRecord extends Model<OtherBgbetRecord> {
private static final long serialVersionUID = 1L;

    /**
     * 订单ID
     */
    @TableId
    private Long orderId;
    /**
     * 订单ID
     */
    private Long tranId;
    /**
     * 厅代码
     */
    private String sn;
    /**
     * 用户ID
     */
    private Long uid;
    /**
     * 用户登录ID
     */
    private String loginId;
    /**
     * 模块ID
     */
    private Long moduleId;
    /**
     * 模块名称
     */
    private String moduleName;
    /**
     * 游戏ID
     */
    private Long gameId;
    /**
     * 玩法名称
     */
    private String gameName;
    /**
     * 注单状态
     */
    private Long orderStatus;
    /**
     * 下注额
     */
    private Float bAmount;
    /**
     * 结算额
     */
    private Float aAmount;
    /**
     * 订单来源
     */
    private Long orderFrom;
    /**
     * 订单时间
     */
    private Date orderTime;
    /**
     * 最后修改时间
     */
    private Date lastUpdateTime;
    /**
     * 下注来源IP
     */
    private String fromIp;
    /**
     * 下注来源IP归属地
     */
    private String fromIpAddr;
    /**
     * 下注期数
     */
    private String issueId;
    /**
     * 玩法ID
     */
    private String playId;
    /**
     * 玩法名称
     */
    private String playName;
    /**
     * 玩法名称(En)
     */
    private String playNameEn;
    /**
     * 打码量
     */
    private Float validBet;
    /**
     * 派彩(输赢)
     */
    private Float payment;
    /**
     * 是否同步打码量
     */
    private Integer isSynMulti;


    private String detailUrl;
  
}
