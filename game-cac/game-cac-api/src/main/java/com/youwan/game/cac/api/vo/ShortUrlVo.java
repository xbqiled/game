package com.youwan.game.cac.api.vo;

import lombok.Data;

import java.io.Serializable;


@Data
public class ShortUrlVo implements Serializable {

    private String shortUrl;

    private String jumpUrl;

    private String sourceUrl;

    public ShortUrlVo(String shortUrl, String jumpUrl, String sourceUrl) {
        this.sourceUrl = sourceUrl;
        this.jumpUrl = jumpUrl;
        this.shortUrl = shortUrl;
    }

}
