package com.youwan.game.cac.api.feign.factory;


import com.youwan.game.cac.api.feign.RemoteJumpService;
import com.youwan.game.cac.api.feign.fallback.RemoteJumpServiceFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author lion
 * @date 2019/2/14
 */
@Component
public class RemoteJumpServiceFallbackFactory implements FallbackFactory<RemoteJumpService> {

    @Override
    public RemoteJumpService create(Throwable throwable) {
        RemoteJumpServiceFallbackImpl remoteJumpServiceFallbackImpl = new RemoteJumpServiceFallbackImpl();
        remoteJumpServiceFallbackImpl.setCause(throwable);
        return remoteJumpServiceFallbackImpl;
    }
}
