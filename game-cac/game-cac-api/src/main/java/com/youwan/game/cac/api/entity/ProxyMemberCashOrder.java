package com.youwan.game.cac.api.entity;

import com.youwan.game.common.data.core.BaseEntity;
import lombok.Data;

import javax.persistence.Id;
import java.util.Date;
import java.util.List;

/**
 * @author terry.jiang[taoj555@163.com] on 2020-04-25.
 */
@Data
public class ProxyMemberCashOrder extends BaseEntity {

    @Id
    private String id;
    private String note;
    private Date create_time;
    private String bank_address;
    private Long pid;
    private Integer cash_fee;
    private String approve_by;
    private String create_by;
    private String bank;
    private Long user_id;
    private String nickName;
    private String account_name;
    private Integer cash_type;
    private String sub_bank;
    private Date approve_time;
    private Long channel_id;
    private String account;
    private Integer status;
    private List<String> m_path;
    private int deep;

}
